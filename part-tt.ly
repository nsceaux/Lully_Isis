\version "2.19.80"
\include "common.ily"

\paper {
  %% plus d'espace entre titre et musique
  markup-system-spacing.padding = #2.5 % default: 0.5
  system-system-spacing.padding = #2.5 % default: 1
  %% de la place entre dernier system et copyright
  last-bottom-spacing.padding = #3
}
\layout {
  \context {
    \Lyrics
    %% de la place sous les paroles (basse continue)
    \override VerticalAxisGroup.nonstaff-unrelatedstaff-spacing.padding = #3
  }
}

%% Title page
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header { title = "Isis" }
  \markup\null
  \partPageBreak #'(dessus) \markup\null
}

%% Table of contents
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \markuplist
  \abs-fontsize-lines #7
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 2)
  \table-of-contents
}

%% Musique
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%% Prologue
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\actn "Prologue"
%% 0-1
\pieceToc "Ouverture"
\includeScore "AAAouverture"

\scene "Scene Premiere" "Scene I"
%% 0-2
\pieceToc\markup\wordwrap {
  Chœur : \italic { Publions en tous lieux }
}
\includeScore "AABchoeur"
%% 0-3
\pieceToc\markup\wordwrap {
  La Renommée, chœur : \italic { C’est luy dont les Dieux ont fait choix }
}
\includeScore "AACchoeur"

\scene "Scene II" "Scene I"
%% 0-4
\pieceToc\markup\wordwrap { Premier air des Tritons }
\includeScore "ABAair"
%% 0-5
\pieceToc\markup\wordwrap {
  Deux Tritons : \italic { C’est le Dieu des Eaux qui va paraistre }
}
\includeScore "ABBtritons"
%% 0-6
\pieceToc\markup\wordwrap { Deuxième air des Tritons }
\includeScore "ABCair"
%% 0-7
\pieceToc\markup\wordwrap {
  Netpune, la Renommée : \italic { Mon Empire a servy de Theatre à la Guerre }
}
\includeScore "ABDrecit"
%% 0-8
\pieceToc\markup\wordwrap {
  Chœur : \italic { Celebrons son grand Nom sur la Terre & sur l’Onde }
}
\includeScore "ABEchoeur"

\scene "Scene III" "Scene III"
%% 0-9
\pieceToc "Prélude des Muses"
\includeScore "ACAprelude"
%% 0-10
\pieceToc\markup\wordwrap {
  Calliope, Thalie, Apollon, Melpomene :
  \italic { Cessez pour quelque temps, bruit terrible des Armes }
}
\includeScore "ACBrecit"
%% 0-11
\pieceToc "Premier air pour les Muses"
\includeScore "ACCair"
%% 0-12
\pieceToc "Deuxième air pour les Muses"
\includeScore "ACDair"
%% 0-13
\pieceToc\markup\wordwrap {
  Apollon : \italic { Ne parlons pas toujours de la Guerre cruelle }
}
\includeScore "ACErecit"
%% 0-14
\pieceToc\markup\wordwrap {
  Chœur : \italic { Ne parlons pas toujours de la Guerre cruelle }
}
\includeScore "ACFchoeur"
%% 0-15
\pieceToc "Air [pour les Trompettes]"
\includeScore "ACGair"
%% 0-16
\pieceToc\markup\wordwrap {
  La Renommée, Apollon, Neptune, chœur : \italic { Hastez-vous, Plaisirs, hastez-vous }
}
\includeScore "ACHchoeur"
\markup\wordwrap { 
  [Matériel 1677] \italic {
    On joüe l’Air des Trompettes [page \page-refII #'ACGair "]," & sur la
    derniere note on recommence le Chœur
    \line { \normal-text Hastez-vous. \raise#1 \musicglyph #"scripts.segno" }
  }
}
%% 0-17
\pieceToc "Ouverture"
\includeScore "ACIouverture"
\actEnd "FIN DU PROLOGUE"
