\version "2.19.80"
\include "common.ily"

\paper {
  %% plus d'espace entre titre et musique
  markup-system-spacing.padding = #2.5 % default: 0.5
  system-system-spacing.padding = #2.5 % default: 1
  %% de la place entre dernier system et copyright
  last-bottom-spacing.padding = #3
}
\layout {
  \context {
    \Lyrics
    %% de la place sous les paroles (basse continue)
    \override VerticalAxisGroup.nonstaff-unrelatedstaff-spacing.padding = #3
  }
}

%% Title page
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header { title = "Isis" }
  \markup\null
  \partPageBreak #'(dessus haute-contre taille quinte basse) \markup\null
}

%% Table of contents
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \markuplist
  \abs-fontsize-lines #7
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 2)
  \table-of-contents
}

%% Musique
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%% Prologue
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\actn "Prologue"
%% 0-1
\pieceToc "Ouverture"
\includeScore "AAAouverture"

\scene "Scene Premiere" "Scene I"
%% 0-2
\pieceToc\markup\wordwrap {
  Chœur : \italic { Publions en tous lieux }
}
\includeScore "AABchoeur"
%% 0-3
\pieceToc\markup\wordwrap {
  La Renommée, chœur : \italic { C’est luy dont les Dieux ont fait choix }
}
\includeScore "AACchoeur"

\scene "Scene II" "Scene I"
%% 0-4
\pieceToc\markup\wordwrap { Premier air des Tritons }
\includeScore "ABAair"
%% 0-5
\pieceToc\markup\wordwrap {
  Deux Tritons : \italic { C’est le Dieu des Eaux qui va paraistre }
}
\includeScore "ABBtritons"
%% 0-6
\pieceToc\markup\wordwrap { Deuxième air des Tritons }
\includeScore "ABCair"
%% 0-7
\pieceToc\markup\wordwrap {
  Netpune, la Renommée : \italic { Mon Empire a servy de Theatre à la Guerre }
}
\includeScore "ABDrecit"
%% 0-8
\pieceToc\markup\wordwrap {
  Chœur : \italic { Celebrons son grand Nom sur la Terre & sur l’Onde }
}
\includeScore "ABEchoeur"

\scene "Scene III" "Scene III"
%% 0-9
\pieceToc "Prélude des Muses"
\includeScore "ACAprelude"
%% 0-10
\pieceToc\markup\wordwrap {
  Calliope, Thalie, Apollon, Melpomene :
  \italic { Cessez pour quelque temps, bruit terrible des Armes }
}
\includeScore "ACBrecit"
%% 0-11
\pieceToc "Premier air pour les Muses"
\includeScore "ACCair"
%% 0-12
\pieceToc "Deuxième air pour les Muses"
\includeScore "ACDair"
%% 0-13
\pieceToc\markup\wordwrap {
  Apollon : \italic { Ne parlons pas toujours de la Guerre cruelle }
}
\includeScore "ACErecit"
%% 0-14
\pieceToc\markup\wordwrap {
  Chœur : \italic { Ne parlons pas toujours de la Guerre cruelle }
}
\includeScore "ACFchoeur"
%% 0-15
\pieceToc "Air [pour les Trompettes]"
\includeScore "ACGair"
%% 0-16
\pieceToc\markup\wordwrap {
  La Renommée, Apollon, Neptune, chœur : \italic { Hastez-vous, Plaisirs, hastez-vous }
}
\includeScore "ACHchoeur"
\markup\wordwrap { 
  [Matériel 1677] \italic {
    On joüe l’Air des Trompettes [page \page-refII #'ACGair "]," & sur la
    derniere note on recommence le Chœur
    \line { \normal-text Hastez-vous. \raise#1 \musicglyph #"scripts.segno" }
  }
}
%% 0-17
\pieceToc "Ouverture"
\includeScore "ACIouverture"
\actEnd "FIN DU PROLOGUE"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%% Acte I
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newBookPart #'()
\act "Acte Premier"
\scene "Scene Premiere" "Scene I"
%% 1-1
\pieceToc "Ritournelle"
\includeScore "BAAritournelle"
%% 1-2
\pieceToc\markup\wordwrap {
  Hierax : \italic { Cessons d’aimer une Infidelle }
}
\includeScore "BABrecit"

\scene "Scene II" "Scene II"
%% 1-3
\pieceToc\markup\wordwrap {
  Pirante, Hierax : \italic { C’est trop entretenir vos tristes resveries }
}
\includeScore "BBArecit"

\scene "Scene III" "Scene III"
%% 1-4
\pieceToc\markup\wordwrap {
  Io, Hierax : \italic { M’aimez-vous ? puis-je m’en flater ? }
}
\includeScore "BCArecit"

\scene "Scene IV" "Scene IV"
%% 1-5
\pieceToc\markup\wordwrap {
  Micene, Io :
  \italic { Ce Prince trop longtemps dans ses chagrins s’obstine. }
}
\includeScore "BDArecit"

\scene "Scene V" "Scene V"
%% 1-6
\pieceToc\markup\wordwrap {
  Mercure, chœur :
  \italic { Le Dieu puissant qui lance le Tonnerre }
}
\includeScore "BEAchoeur"
%% 1-7
\pieceToc\markup\wordwrap {
  Mercure, Io :
  \italic { C'est ainsi que Mercure }
}
\includeScore "BEBrecit"

\scene "Scene VI" "Scene VI"
%% 1-8
\pieceToc\markup\wordwrap {
  Chœur : \italic { Que tout l’Univers se pare }
}
\includeScore "BFAchoeur"
%% 1-9
\pieceToc\markup\wordwrap {
  Premier air [pour l’Entrée des Divinitez de la Terre]
}
\includeScore "BFBair"
%% 1-10
\pieceToc\markup Deuxième air
\includeScore "BFCair"
%% 1-11
\pieceToc\markup\wordwrap {
  Jupiter, chœur :
  \italic { Les armes que je tiens protegent l’innocence }
}
\includeScore "BFDchoeur"
%% 1-12
\pieceToc\markup Entr’acte
\includeScore "BFEair"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%% Acte II
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newBookPart #'()
\act "Acte Deuxiéme"
\scene "Scene Premiere" "Scene I"
%% 2-1
\pieceToc "Ritournelle"
\includeScore "CAAritournelle"
\partNoPageBreak#'(dessus)
%% 2-2
\pieceToc\markup\wordwrap {
  Io : \italic { Où suis-je, d’où vient ce Nuage }
}
\includeScore "CABrecit"

\scene "Scene II" "Scene II"
%% 2-3
\pieceToc\markup\wordwrap {
  Jupiter, Io : \italic { Vous voyez Jupiter, que rien ne vous étonne }
}
\includeScore "CBArecit"
\partPageTurn#'(dessus)
\scene "Scene III" "Scene III"
%% 2-4
\pieceToc\markup\wordwrap {
  Mercure, Jupiter : \italic { Iris est icy-bas, & Junon elle-mesme }
}
\includeScore "CCArecit"

\scene "Scene IV" "Scene IV"
%% 2-5
\pieceToc\markup\wordwrap {
  Mercure, Iris : \italic { Arrestez, belle Iris, différez un moment }
}
\includeScore "CDArecit"
\partPageTurn#'(dessus)
\scene "Scene V" "Scene V"
%% 2-6
\pieceToc\markup Prélude
\includeScore "CEAprelude"
%% 2-7
\pieceToc\markup\wordwrap {
  Iris, Junon : \italic { J’ay cherché vainement le Fille d’Inachus }
}
\includeScore "CEBrecit"
\partNoPageTurn #'(basse-continue)

\scene "Scene VI" "Scene VI"
%% 2-8
\pieceToc\markup\wordwrap {
  Récit : \italic { Dans les Jardins d’Hébé vous deviez en ce jour }
}
\includeScore "CFArecit"

\scene "Scene VII" "Scene VII"
%% 2-9
\pieceToc\markup\wordwrap { Entrée pour le Jeunesse }
\includeScore "CGAentree"
%% 2-10
\pieceToc\markup\wordwrap {
  Hebé, chœur : \italic { Les Plaisirs les plus doux }
}
\includeScore "CGBchoeur"
%% 2-11
\pieceToc\markup\wordwrap { Premier Air }
\includeScore "CGCair"
%% 2-12
\pieceToc\markup\wordwrap {
  Deux Nymphes : \italic { Aymez, profitez du temps }
}
\includeScore "CGDnymphes"
%% 2-13
\pieceToc\markup\wordwrap { Deuxième Air }
\includeScore "CGEair"
\partNoPageTurn #'(basse-continue)
%% 2-14
\pieceToc\markup\wordwrap {
  Chœur : \italic { Que ces Lieux ont d’attraits }
}
\includeScore "CGFchoeur"

\scene "Scene VIII" "Scene VIII"
%% 2-15
\pieceToc\markup\wordwrap {
  Mercure, Iris, Hebé, chœur : \italic {
    Servez, Nymphe, servez, avec un soin fidelle
  }
}
\includeScore "CHAchoeur"
\actEnd FIN DU DEUXIÈME ACTE

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%% Acte III
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newBookPart #'()
\act "Acte Troisiéme"
\scene "Scene Premiere" "Scene I"
%% 3-1
\pieceToc "Ritournelle"
\includeScore "DAAritournelle"

%% 3-2
\pieceToc\markup\wordwrap {
  Argus, Io : \italic { Dans ce solitaire séjour }
}
\includeScore "DABrecit"

\scene "Scene II" "Scene II"
%% 3-3
\pieceToc\markup\wordwrap {
  Hierax, Argus : \italic { La perfide craint ma presence }
}
\includeScore "DBArecit"

\scene "Scene III" "Scene III"
%% 3-4
\pieceToc\markup\wordwrap {
  Hierax, Argus, chœur : \italic { Liberté, liberté. }
}
\includeScore "DCAchoeur"

\scene "Scene IV" "Scene IV"
\sceneDescription\markup\wordwrap-center {
  Argus, Hierax, Syrinx, Troupe de Nymphes, Mercure déguisé en Berger,
  Troupe de Bergers, Troupe de Satyres & de Sylvains.
}
%% 3-5
\pieceToc\markup\wordwrap {
  Mercure, chœur : \italic { Liberté, liberté. }
}
\includeScore "DDAchoeur"
%% 3-6
\pieceToc\markup\wordwrap {
  Mercure, Argus : \italic { De la nymphe Syrinx, Pan chérit la mémoire }
}
\includeScore "DDBrecit"

\scene "Scene V" "Scene V"
%% 3-7
\pieceToc\markup\wordwrap {
  Syrinx, chœur : \italic { Liberté, liberté. }
}
\includeScore "DEAchoeur"
%% 3-8
\pieceToc\markup { Air de Sylvains et des Satyres }
\includeScore "DEBair"
%% 3-9
\pieceToc\markup\wordwrap {
  Chœur : \italic { Liberté, liberté. }
}
\includeScore "DECchoeur"
%% 3-10
\pieceToc\markup\wordwrap {
  Marche [des Bergers & Satyres qui apportent des presents à Syrinx]
}
\includeScore "DEDmarche"

\scene "Scene VI" "Scene VI"
%% 3-11
\pieceToc "Air"
\markup {
  [Matériel 1677 :
  \italic {
    Les Violons, les Fluttes & les hautbois joüent cét Air
    alternativement avec les \concat { voix. \normal-text ] }
  }
}
\includeScore "DFAair"
%% 3-12
\pieceToc\markup\wordwrap {
  Deux Bergers : \italic { Quel bien devez-vous attendre }
}
\includeScore "DFBbergers"
%% 3-13
\pieceToc\markup\wordwrap { Troisième Air }
\includeScore "DFCair"
%% 3-14
\pieceToc\markup\wordwrap {
  Pan, Syrinx : \italic { Je vous aime, nymphe charmante }
}
\includeScore "DFDrecit"
%% 3-15
\pieceToc\markup\wordwrap {
  Chœurs, Syrinx, Pan : \italic { Aymons sans cesse }
}
\includeScore "DFEchoeur"
%% 3-16
\pieceToc\markup\wordwrap {
  Syrinx, Pan, chœur : \italic { Faut-il qu’en vains discours un si beau jour se passe }
}
\includeScore "DFFchoeur"
%% 3-17
\pieceToc\markup\wordwrap {
  Pan, Syrinx, chœur : \italic { Je ne puis vous quitter, mon cœur s’attache à vous }
}
\includeScore "DFGchoeur"
%% 3-18
\pieceToc\markup\wordwrap { Plainte du Dieu Pan }
\includeScore "DFHplainte"

\scene "Scene VII" "Scene VII"
%% 3-19
\pieceToc\markup\wordwrap {
  Mercure, Hierax, Io, Argus, chœur :
  \italic { Reconnoissez Mercure, & fuyez avec nous }
}
\includeScore "DGArecit"

\scene "Scene VIII" "Scene VIII"
%% 3-20
\pieceToc\markup\wordwrap {
  Junon, Io :
  \italic { Revoy le jour, Argus, que ta figure change }
}
\includeScore "DHArecit"
\actEnd FIN DU TROISIÈME ACTE
%% 3-21
\pieceToc\markup Entr’acte
\reIncludeScore "DFAair" "DFBentracte"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%% Acte IV
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newBookPart #'()
\act "Acte Quatriéme"
\sceneDescription\markup\wordwrap-center {
  Le Théâtre change, & représente l’endroit le plus glacé de la Scythie.
}
\scene "Scene Premiere" "Scene I"
\sceneDescription\markup\wordwrap-center {
  Des Peuples paroissent transis de froid.
}
%% 4-1
\pieceToc\markup\wordwrap { [Entrée des Peuples des Climats glacez] }
\includeScore "EAAentree"
%% 4-2
\pieceToc\markup\wordwrap {
  Chœur : \italic { L’hyver qui nous tourmente }
}
\includeScore "EABchoeur"

\scene "Scene II" "Scene II"
%% 4-3
\pieceToc\markup\wordwrap {
  Io, la Furie, chœur : \italic { Laissez-moy, Cruelle Furie }
}
\includeScore "EBArecit"
%% 4-4
\pieceToc\markup\wordwrap { [Entrée des Peuples des Climats glacez] }
\markup\wordwrap {
  [Matériel 1677]
  \italic { On jouë encore une fois l’Entrée des Peuples des Climats glacez. }
}
\includeScore "EBCentree"

\scene "Scene III" "Scene III"
%% 4-5
\pieceToc\markup\wordwrap {
  Chœur des Chalybes : \italic { Que le feu des forges s’allume }
}
\includeScore "ECAchoeur"
%% 4-6
\pieceToc\markup\wordwrap { Entrée des Forgerons }
\includeScore "ECBentree"

\scene "Scene IV" "Scene IV"
%% 4-7
\pieceToc\markup\wordwrap {
  Io, la Furie, chœur : \italic { Quel déluge de feux vient sur moy se répandre }
}
\includeScore "EDAchoeur"

\scene "Scene V" "Scene V"
%% 4-8
\pieceToc\markup\wordwrap {
  Chœur : \italic { Executons l’arrest du sort }
}
\includeScore "EEAchoeur"

\scene "Scene VI" "Scene VI"
%% 4-9
\pieceToc\markup\wordwrap { Premier air des Parques }
\includeScore "EFAair"
%% 4-10
\pieceToc\markup\wordwrap { Deuxiéme air des Parques }
\includeScore "EFBair"
%% 4-11
\pieceToc\markup\wordwrap {
  Io, chœur : \italic { C’est contre moy qu’il faut tourner }
}
\includeScore "EFCrecit"

\scene "Scene VII" "Scene VII"
%% 4-12
\pieceToc "Ritournelle"
\includeScore "EGAritournelle"
%% 4-13
\pieceToc\markup\wordwrap {
  Les Trois Parques, Io, la Furie : \italic { Le fil de la vie }
}
\includeScore "EGBchoeur"
%% 4-14
\pieceToc "Entr’acte"
\reIncludeScore "DEBair" "EGCentracte"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%% Acte V
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newBookPart #'()
\act "Acte Cinquième"
\scene "Scene Premiere" "Scene I"
%% 5-1
\pieceToc "Ritournelle"
\includeScore "FAAritournelle"
%% 5-2
\pieceToc\markup\wordwrap {
  Io : \italic { Terminez mes tourments, puissant Maître du monde }
}
\includeScore "FABrecit"

\scene "Scene II" "Scene II"
%% 5-3
\pieceToc "Prélude"
\includeScore "FBAprelude"
%% 5-4
\pieceToc\markup\wordwrap {
  Jupiter, Io : \italic { Il ne m’est pas permis de finir vôtre peine }
}
\includeScore "FBBrecit"
\partPageTurn#'(dessus)
\scene "Scene III" "Scene III"
%% 5-5
\pieceToc\markup\wordwrap {
  Jupiter, Junon, Io : \italic { Venez Déesse impitoyable }
}
\includeScore "FCArecit"
%% 5-6
\pieceToc\markup\wordwrap {
  Chœurs, Jupiter, Junon : \italic { Venez, Divinité nouvelle }
}
\includeScore "FCBchoeur"
%% 5-7
\pieceToc\markup\wordwrap {
  Premier air pour les Egyptiens. Canaries.
}
\includeScore "FCCcanaries"
%% 5-8
\pieceToc\markup\wordwrap {
  Deuxième air pour les Egyptiens.
}
\includeScore "FCDair"
%% 5-9
\pieceToc\markup\wordwrap {
  Chœurs : \italic { Isis est immortelle }
}
\includeScore "FCEchoeur"
\actEnd "FIN DU CINQUIÉME ET DERNIER ACTE"
