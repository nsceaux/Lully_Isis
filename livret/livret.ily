\notesSection "Livret"
\markuplist\abs-fontsize-lines #8 \page-columns-title \act\line { LIVRET } {
\livretAct PROLOGUE
\livretDescAtt\justify {
  Le Theatre represente le Palais de la Renommée.  Il est ouvert de
  tous costez pour recevoir les nouvelles de ce qui se fait de
  conséderable sur la Terre, & de ce qui se passe de memorable sur la
  Mer, que l’on découvre dans l’enfoncement.  La Divinité qui préside
  dans ce Palais y paroist accompagnée de sa Suite ordinaire : Les
  Rumeurs & les Bruits qui portent comme elle chacun une Trompette à
  la main, y viennent en foule de divers endroits du monde.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center {
  La Renommée, Suite de la Renommée, les Rumeurs, et les Bruits.
}
\livretPers\wordwrap\smallCaps {
  Chœur de la Suite de la Renommée, des Rumeurs, & des Bruits.
}
\livretRef#'AABchoeur
\livretVerse#6 { Publions en tous lieux }
\livretVerse#12 { Du plus grand des Heros la valeur triomphante, }
\livretVerse#6 { Que la Terre, & les Cieux }
\livretVerse#12 { Retentissent du bruit de sa gloire éclatante. }
\livretPers La Renommée
\livretRef#'AACchoeur
\livretVerse#8 { C’est luy dont les Dieux ont fait choix }
\livretVerse#12 { Pour combler le bon-heur de l’Empire François ; }
\livretVerse#12 { En vain, pour le troubler, tout s’unit, tout conspire, }
\livretVerse#12 { C’est en vain que l’Envie a ligué tant de Rois, }
\livretVerse#4 { Heureux l’Empire }
\livretVerse#4 { Qui suit ses Loix ! }
\livretPers Le Chœur
\livretVerse#4 { Heureux l’Empire }
\livretVerse#4 { Qui suit ses Loix ! }
\livretPers La Renommée
\livretVerse#8 { Il faut que par tout on l’admire, }
\livretVerse#12 { Parlons de ses Vertus, racontons ses Exploits, }
\livretVerse#8 { A peine y pourrons-nous suffire }
\livretVerse#6 { Avec toutes nos voix. }
\livretPers La Renommée et le chœur
\livretVerse#4 { Heureux l’Empire }
\livretVerse#4 { Qui suit ses Loix ! }
\livretVerse#4 { Il faut le dire }
\livretVerse#4 { Cent & cent fois. }
\livretVerse#4 { Heureux l’Empire }
\livretVerse#4 { Qui suit ses Loix ! }

\livretScene SCENE SECONDE
\livretDescAtt\wordwrap-center {
  Deux Tritons chantants, Trouppe de Dieux Marins joüant des
  instruments et daçants.  Neptune, la Renommée, Chœur de la Suite de
  la Renommée.
}
\livretPersDidas Deux Tritons chantants
\livretRef#'ABBtritons
\livretVerse#9 { C’est le Dieu des Eaux qui va paraistre, }
\livretVerse#8 { Rangeons-nous prés de nostre Maistre : }
\livretVerse#5 { Enchaînons les Vents }
\livretVerse#4 { Les plus terribles, }
\livretVerse#9 { Que le bruit des Flots céde à nos Chants ; }
\livretVerse#6 { Regnez, Zephirs paisibles, }
\livretVerse#7 { Ramenez le doux Printemps. }
\livretVerse#9 { Fuyez loin d’icy, cruels orages, }
\livretVerse#8 { Rien ne doit troubler ces Rivages. }
\livretVerse#5 { Enchaînons les Vents }
\livretVerse#4 { Les plus terribles, &c. }
\livretPersDidas Neptune parlant à la Renommée
\livretRef#'ABDrecit
\livretVerse#12 { Mon Empire a servy de Theatre à la Guerre ; }
\livretVerse#8 { Publiez des Exploits nouveaux : }
\livretVerse#12 { C’est le mesme Vainqueur si fameux sur la Terre }
\livretVerse#8 { Qui triomphe encor sur les Eaux. }
\livretPers Neptune et la Renommée
\livretVerse#12 { \raise#0.7 { \left-brace#20 \raise#1 \column { Celebrez Celebrons } \right-brace#20 } son grand Nom sur la Terre & sur l’Onde. }
\livretVerse#12 { Qu’il ne soit pas borné par les plus vastes Mers : }
\livretVerse#8 { Qu’il vole jusqu’au bout du Monde, }
\livretVerse#8 { Qu’il dure autant que l’Univers. }
\livretDidasP\wordwrap {
  Le Chœur repete ces quatre derniers Vers.
}
\livretRef#'ABEchoeur
\livretVerse#12 { Celebrons son grand Nom sur la Terre & sur l’Onde, &c. }

\livretScene SCENE TROISIESME
\livretDescAtt\wordwrap-center {
  Les neuf Muses, les Arts Libéraux, Apollon, Neptune,
  Suite de Neptune, la Renommée, Suite de la Renommée
}
\livretPers Calliope
\livretRef#'ACBrecit
\livretVerse#12 { Cessez pour quelque temps, bruit terrible des Armes, }
\livretVerse#12 { Qui troublez le repos de cent Climats divers ; }
\livretPers Calliope, Clio, Melpomene, Thalie, & Vranie
\livretVerse#6 { Ne troublez pas les charmes }
\livretVerse#6 { De nos divins Concerts. }
\livretDidasP\justify {
  Erato, Euterpe, Terpsichore, & Polymnie, forment un Concert d’Instruments.
}
\livretPers Melpomene
\livretVerse#12 { Recommençons nos Chants, allons les faire entendre }
\livretVerse#6 { Dans une Auguste Cour. }
\livretPers Thalie et Calliope
\livretVerse#12 { La Paix, la douce Paix n’ose encore descendre }
\livretVerse#6 { Du celeste Séjour ; }
\livretPers Calliope, Clio, Melpomene, Thalie, & Vranie
\livretVerse#8 { Prés du Vainqueur, allons attendre }
\livretVerse#6 { Son bien-heureux retour. }
\livretRef#'ACCair
\livretDidasPPage\justify {
  Les Arts accompagnent Apollon, & se réjoüissent du bon-heur que ce Dieu
  qui les conduit, leur fait esperer.
}
\livretPersDidas Apollon parlant à la Renommée
\livretRef#'ACErecit
\livretVerse#12 { Ne parlez pas toujours de la Guerre cruelle, }
\livretVerse#8 { Parlons des Plaisirs, & des Jeux. }
\livretVerse#12 { Les Muses, & les Arts vont signaler leur zele, }
\livretVerse#8 { Je vais favoriser leurs vœux ; }
\livretVerse#10 { Nous preparons une Feste nouvelle, }
\livretVerse#8 { Pour le Heros qui les appelle, }
\livretVerse#6 { Dans cet Azile heureux. }
\livretVerse#12 { Ne parlez pas toujours de la Guerre cruelle, }
\livretVerse#8 { Parlons des Plaisirs, & des Jeux. }
\livretPers La Renommée, Neptune, Apollon, les Muses, & Chœur
\livretRef#'ACFchoeur
\livretVerse#12 { Ne parlons pas toujours de la Guerre cruelle, }
\livretVerse#8 { Parlons des Plaisirs, & des Jeux. }
\livretPers\wordwrap\smallCaps {
  La Renommée, Neptune, Apollon, les Muses, les Tritons,
  & le Chœur de la Suite de la Renommée
}
\livretRef#'ACHchoeur
\livretVerse#8 { Hastez-vous, Plaisirs, hastez-vous, }
\livretVerse#12 { Hastez-vous de montrer vos charmes les plus doux. }
\livretPers La Renommée
\livretVerse#8 { Il n’est pas encor temps de croire }
\livretVerse#12 { Que les paisibles Jeux ne seront plus troublez ; }
\livretVerse#12 { Rien ne plaist au Heros qui les a rassemblez }
\livretVerse#12 { A l’égal des Exploits d’éternelle mémoire. }
\livretVerse#8 { Ennemis de la Paix, tremblez ; }
\livretVerse#12 { Vous le verrez bien-tost courir à la Victoire, }
\livretVerse#6 { Vos efforts redoublez }
\livretVerse#10 { Ne serviront qu’à redoubler sa gloire. }
\livretPers\wordwrap\smallCaps {
  La Renommée, Neptune, Apollon, les Muses, les Tritons,
  & le Chœur de la Suite de la Renommée
}
\livretVerse#8 { Hastez-vous, Plaisirs, hastez-vous, }
\livretVerse#12 { Hastez-vous de montrer vos charmes les plus doux. }
\livretDidasP\justify {
  Dans le temps que le Chœur chante, & que les Instruments joüent,
  la Suite de Neptune dance avec celle d’Apollon, & toutes ces Divinitez
  vont ensemble prendre part à la nouvelle Feste que le Dieux du Parnasse
  a préparée avec les Muses, & les arts.
}
\livretFinAct Fin du Prologue
\sep
\livretAct ACTE PREMIER
\livretDescAtt\justify {
  Le Theatre represente des Prairies agreables,
  où le Fleuve Inachus serpente.
}
\livretScene SCENE PREMIERE
\livretPers Hierax
\livretRef#'BABrecit
\livretVerse#8 { Cessons d’aimer une Infidelle, }
\livretVerse#8 { Evitons la honte cruelle }
\livretVerse#12 { De servir, d’adorer qui ne nous aime plus, }
\livretVerse#12 { Achevons de briser les nœuds qu’elle a rompus : }
\livretVerse#12 { Dégageons-nous, sortons d’un si funeste empire. }
\livretVerse#8 { Helas ! malgré moy je soupire, }
\livretVerse#8 { Ah, mon Cœur, quelle lacheté ! }
\livretVerse#12 { Quel charme te retient dans un honteux martire ? }
\livretVerse#12 { Tu n’as pas craint des fers qui nous ont tant cousté, }
\livretVerse#8 { As-tu peur de la Liberté ? }
\livretVerse#8 { Revenez, Liberté charmante, }
\livretVerse#8 { Vous n’estes que trop diligente, }
\livretVerse#12 { Lors qu’il faut dans un Cœur faire place à l’Amour, }
\livretVerse#6 { Mais que vous estes lente, }
\livretVerse#12 { Lors qu’un juste Dépit presse vostre retour. }

\livretScene SCENE SECONDE
\livretDescAtt\wordwrap-center {
  Pirante, Hierax
}
\livretPers Pirante
\livretRef#'BBArecit
\livretVerse#12 { C’est trop entretenir ces tristes resveries ; }
\livretVerse#12 { Venez, tournez vos pas vers ces Rives fleuries ; }
\livretVerse#8 { Regardez ces flots argentez, }
\livretVerse#8 { Qui dans ces Vallons écartez, }
\livretVerse#8 { Font briller l’émail des Prairies. }
\livretVerse#7 { Interrompez vos soûpirs, }
\livretVerse#7 { Tout doit estre icy tranquile ; }
\livretVerse#7 { Ce beau séjour est l’Azile }
\livretVerse#7 { Du Repos, & des Plaisirs. }
\livretPers Hierax
\livretVerse#8 { Depuis qu’une Nimphe inconstante }
\livretVerse#12 { A trahy mon amour, & m’a manqué de foy : }
\livretVerse#12 { Ces lieux, jadis si beaux, n’ont plus rien qui m’enchante, }
\livretVerse#12 { Ce que j’aime a changé, tout est changé pour moy. }
\livretPers Pirante
\livretVerse#12 { La Fille d’Inachus hautement vous prefere }
\livretVerse#12 { A mille autres Amants de vostre sort jaloux ; }
\livretVerse#8 { Vous avez l’aveu de son Pere, }
\livretVerse#8 { En faveur d’Argus, vostre Frere, }
\livretVerse#12 { La puissante Junon se declare pour vous. }
\livretPers Hierax
\livretVerse#12 { Si l’Ingrate m’aimoit, je serois son Espoux. }
\livretVerse#6 { Cette Nymphe legere }
\livretVerse#6 { De jour en jour differe }
\livretVerse#12 { Un Hymen qu’autrefois elle avoit crû si doux. }
\livretVerse#12 { L’inconstante n’a plus l’empressement extréme }
\livretVerse#12 { De cét Amour naissant qui répondoit au mien, }
\livretVerse#12 { Son changement paroist en dépit d’elle-mesme, }
\livretVerse#8 { Je ne le connois que trop bien ; }
\livretVerse#12 { Sa bouche quelquefois dit encor qu’elle m’aime, }
\livretVerse#12 { Mais son cœur, ny ses yeux ne m’en disent plus rien. }
\livretPers Pirante
\livretVerse#8 { Se peut-il qu’elle dissimule ? }
\livretVerse#12 { Aprés tant de serments, ne la croyez-vous pas ? }
\livretPers Hierax
\livretVerse#8 { Je ne le crûs que trop, helas ? }
\livretVerse#12 { Ces serments qui trompoient mon cœur tendre & credule. }
\livretVerse#12 { Ce fut dans ces Vallons, où par mille détours }
\livretVerse#12 { Inachus prend plaisir à prolonger son cours ; }
\livretVerse#8 { Ce fut sur son charmant rivage, }
\livretVerse#6 { Que la Fille volage }
\livretVerse#8 { Me promit de m’aimer toûjours. }
\livretVerse#12 { Le Zephir fut témoin, l’Onde fut attentive, }
\livretVerse#12 { Quand la Nymphe jura de ne changer jamais ; }
\livretVerse#12 { Mais le Zephir leger, & l’Onde fugitive, }
\livretVerse#12 { Ont enfin emporté les serments qu’elle a faits. }
\livretVerse#6 { Je la voy l’Infidelle. }
\livretPers Pirante
\livretVerse#8 { Eclaircissez-vous avec elle. }

\livretScene SCENE TROISIESME
\livretDescAtt\wordwrap-center {
  La Nymphe Io, Mycene, Hierax, Pirante.
}
\livretPers Io
\livretRef#'BCArecit
\livretVerse#8 { M’aimez-vous ? puis-je m’en flater ? }
\livretPers Hierax
\livretVerse#8 { Cruelle, en voulez-vous douter ? }
\livretVerse#8 { En vain vostre inconstance éclate, }
\livretVerse#12 { En vain elle m’anime à briser tous les nœuds, }
\livretVerse#8 { Je vous aime toûjours, Ingrate, }
\livretVerse#12 { Plus que vous ne voulez ; & plus que je ne veux. }
\livretPers Io
\livretVerse#8 { Je crains un funeste présage. }
\livretVerse#12 { Un aigle dévorant vient de fondre à mes yeux, }
\livretVerse#8 { Sur un Oyseau qui dans ces lieux, }
\livretVerse#8 { M’entretenoit d’un doux ramage. }
\livretVerse#12 { Differez nostre Hymen, suivons l’avis des Cieux. }
\livretPers Hierax
\livretVerse#12 { Nostre Hymen ne déplaist qu’à vostre cœur volage, }
\livretVerse#12 { Répondez-moy de vous, je vous répons des Dieux. }
\livretVerse#12 { Vous juriez autrefois que cette Onde rebelle, }
\livretVerse#12 { Se feroit vers sa source une route nouvelle, }
\livretVerse#12 { Plûtost qu’on ne verroit vostre Cœur dégagé : }
\livretVerse#12 { Voyez couler ces flots dans cette vaste Plaine, }
\livretVerse#12 { C’est le mesme penchant qui toûjours les entraîne, }
\livretVerse#12 { Leur cours ne change point, & vous avez changé. }
\livretPers Io
\livretVerse#12 { Laissez-moy revenir de mes frayeurs secretes ; }
\livretVerse#12 { J’attens de vostre amour cét effort genereux. }
\livretPers Hierax
\livretVerse#12 { Je veux ce qui vous plaist, Cruelle que vous estes, }
\livretVerse#12 { Vous n’abusez que trop d’un amour mal-heureux. }
\livretPers Io
\livretVerse#12 { Non, je vous aime encor. }
\livretPers Hierax
\livretVerse#12 { \transparent { Non, je vous aime encor. } Qu’elle froideur extréme ! }
\livretVerse#12 { Inconstante, est-ce ainsi qu’on doit dire qu’on aime ? }
\livretPers Io
\livretVerse#8 { C’est à tord que vous m’accusez, }
\livretVerse#12 { Vous avez veu toûjours vos Rivaux méprisez. }
\livretPers Hierax
\livretVerse#12 { Le mal de mes Rivaux n’égale point ma peine, }
\livretVerse#12 { La douce illusion d’une esperance vaine }
\livretVerse#12 { Ne les fait point tomber du faiste du bon-heur, }
\livretVerse#12 { Aucun d’eux, comme moy, n’a perdu vostre Cœur, }
\livretVerse#8 { Comme eux, à vostre humeur severe, }
\livretVerse#8 { Je ne suis point accoûtumé : }
\livretVerse#8 { Quel tourment de cesser de plaire, }
\livretVerse#12 { Lors qu’on a fait l’effay du plaisir d’estre aimé ! }
\livretVerse#12 { Je ne le sens que trop, vostre Cœur se détache, }
\livretVerse#8 { Et je ne sçay qui me l’arrache. }
\livretVerse#8 { Je cherche en vain l’heureux Amant }
\livretVerse#8 { Qui me dérobe un bien charmant, }
\livretVerse#8 { Où j’ay crû devoir seul pretendre ; }
\livretVerse#8 { Je sentirois moins mon tourment }
\livretVerse#8 { Si je trouvois à qui m’en prendre. }
\livretVerse#12 { Vous fuyez mes regards, vous ne me dîtes rien. }
\livretVerse#12 { Il faut vous délivrer d’un fâcheux entretien, }
\livretVerse#12 { Ma presence vous blesse, & c’est trop vous contraindre. }
\livretPers Io
\livretVerse#12 { Jaloux, sombre ; & chagrin, partout où je vous voy, }
\livretVerse#8 { Vous ne cessez point de vous plaindre ; }
\livretVerse#12 { Je voudrois vous aimer autant que je le doy, }
\livretVerse#8 { Et vous me forcez à vous craindre. }
\livretPers Io & Hierax
\livretVerse#6 { Non, il ne tient qu’à vous }
\livretVerse#8 { De rendre nostre sort plus doux. }
\livretPers Io
\livretVerse#6 { Non, il ne tient qu’à vous }
\livretVerse#2 { De rendre }
\livretVerse#4 { Mon cœur plus tendre. }
\livretPers Hierax
\livretVerse#6 { Non, il ne tient qu’à vous }
\livretVerse#8 { De rendre mon cœur moins jaloux. }
\livretPers Io & Hierax
\livretVerse#6 { Non, il ne tient qu’à vous }
\livretVerse#8 { De rendre nostre sort plus doux. }

\livretScene SCENE QUATRIESME
\livretDescAtt\wordwrap-center {
  Io, Mycene.
}
\livretPers Mycene
\livretRef#'BDArecit
\livretVerse#12 { Ce Prince trop long-temps dans ses chagrins s’obstine. }
\livretVerse#8 { On pardonne au premier transport }
\livretVerse#8 { D’un Amour qui se plaint à tort, }
\livretVerse#8 { Et qui sans raison se mutine ; }
\livretVerse#4 { Mais à la fin }
\livretVerse#4 { On se chagrine, }
\livretVerse#6 { Contre un Amour chagrin. }
\livretPers Io
\livretVerse#12 { Je veux bien te parler enfin sans artifice, }
\livretVerse#12 { Ce Prince infortuné s’allarme avec Justice, }
\livretVerse#12 { Le Maistre souverain de la Terre & des Cieux }
\livretVerse#8 { Entreprend de plaire à mes yeux, }
\livretVerse#12 { Du cœur de Jupiter l’Amour m’offre l’Empire ; }
\livretVerse#8 { Mercure est venu me le dire : }
\livretVerse#12 { Je le voy chaque jour descendre dans ces lieux. }
\livretVerse#12 { Mon cœur, autant qu’il peut, fait toûjours resistance, }
\livretVerse#8 { Et pour attaquer ma constance, }
\livretVerse#12 { Il ne falloit pas moins que le plus grand des Dieux. }
\livretPers Mycene
\livretVerse#12 { On écoute aisément Jupiter qui soûpire, }
\livretVerse#10 { C’est un Amant qu’on n’ose mépriser ; }
\livretVerse#12 { Et du plus grand des Cœurs le glorieux Empire }
\livretVerse#8 { Est difficile à refuser. }
\livretPers Io
\livretVerse#8 { Lors qu’on me presse de me rendre }
\livretVerse#8 { Aux attraits d’un Amour nouveau ; }
\livretVerse#12 { Plus le charme est puissant, & plus il seroit beau }
\livretVerse#6 { De pouvoir m’en deffendre. }
\livretVerse#12 { Quoy, tu veux me quitter. d’où vient ce soin pressant ? }
\livretPers Mycene
\livretVerse#12 { C’est pour vous seule, icy, que Mercure descend. }

\livretScene SCENE CINQUIESME
\livretDescAtt\wordwrap-center {
  Mercure, Io, chœur des Divinitez de la Terre, et chœur des Echos.
}
\livretPersDidas Mercure sur un Nüage
\livretRef#'BEAchoeur
\livretVerse#10 { Le Dieu puissant qui lance le Tonnerre, }
\livretVerse#10 { Et qui des Cieux tient le Sceptre en ses mains, }
\livretVerse#10 { A resolu de venir sur la Terre }
\livretVerse#10 { Chasser les maux qui troûblent les Humains. }
\livretVerse#12 { Que la Terre avec soin à cét honneur réponde, }
\livretVerse#12 { Echos, retentissez dans ces lieux pleins d’appas ; }
\livretVerse#12 { Annoncez qu’aujourd’huy pour le bon-heur du Monde, }
\livretVerse#8 { Jupiter descend icy bas. }
\livretDidasP\wordwrap {
  Les Chœurs repetent ces quatre derniers Vers dans le temps
  que Mercure descend sur la Terre.
}
\livretPersDidas Mercure parlant à Io
\livretRef#'BEBrecit
\livretVerse#6 { C’est ainsi que Mercure }
\livretVerse#8 { Pour abuser les Dieux jaloux }
\livretVerse#12 { Doit parler hautement à toute la Nature, }
\livretVerse#12 { Mais il doit s’expliquer autrement avec vous. }
\livretVerse#8 { C’est pour vous voir, c’est pour vous plaire, }
\livretVerse#12 { Que Jupiter descend du celeste Séjour ; }
\livretVerse#12 { Et les biens qu’icy-bas sa presence va faire, }
\livretVerse#8 { Ne seront dus qu’à son amour. }
\livretPers Io
\livretVerse#12 { Pourquoy du haut des Cieux, ce Dieu veut-il descendre ? }
\livretVerse#12 { Mes vœux sont engagés, mon cœur a fait un choix, }
\livretVerse#8 { L’Amour tost ou tard doit pretendre, }
\livretVerse#10 { Que tous les Cœurs se rangent sous ses Loix : }
\livretVerse#8 { C’est un hommage qu’il faut rendre, }
\livretVerse#10 { Mais c’est assés de le rendre une fois. }
\livretPers Mercure
\livretVerse#12 { Ce seroit en aimant une contrainte étrange, }
\livretVerse#12 { Qu’un Cœur pour mieux choisir n’osast se dégager : }
\livretVerse#8 { Quand c’est pour Jupiter qu’on change, }
\livretVerse#8 { Il n’est pas honteux de changer. }
\livretVerse#7 { Que tout l’Univers se pare }
\livretVerse#7 { De ce qu’il a de plus rare, }
\livretVerse#7 { Que tout brille dans ces lieux. }
\livretVerse#6 { Que la Terre partage }
\livretVerse#8 { L’éclat & la gloire des Cieux ; }
\livretVerse#5 { Que tout rende hommage }
\livretVerse#5 { Au plus grand des Dieux. }

\livretScene SCENE SIXIESME
\livretDescAtt\justify {
  Les divinitez de la Terre, des Eaux, & des Richesses soûterraines,
  viennent magnifiquement parées pour recevoir Jupiter, & pour luy rendre
  hommage.
}
\livretPers Chœur des Divinitez
\livretRef#'BFAchoeur
\livretVerse#6 { Que la Terre partage }
\livretVerse#8 { L’éclat & la gloire des Cieux ; }
\livretVerse#5 { Que tout rende hommage }
\livretVerse#5 { Au plus grand des Dieux. }
\livretDescAtt\wordwrap-center { Vingt-quatre Divinitez chantantes }
\livretDidasP\justify {
  Huit Divinitez de la Terre. Huit Divinitez des Eaux. Huit Divinitez
  des Richesses soûterraines.
}
\livretDescAtt\wordwrap-center { Douze Divinitez dançantes }
\livretDidasP\justify {
  Quatre Divinitez de la Terre. Quatre Divinitez des Eaux. Quatre Divinitez
  des Richesses soûterraines.
}
\livretPersDidas Jupiter descendant de Ciel
\livretRef#'BFDchoeur
\livretVerse#12 { Les armes que je tiens protegent l’Innocence, }
\livretVerse#12 { L’effort n’en est fatal qu’à l’orgueil des Titans. }
\livretVerse#12 { Vous qui suivez mes Loix, vivez sous ma puissance, }
\livretVerse#8 { Toûjours heureux, toûjours contents. }
\livretVerse#7 { Jupiter vient sur la Terre, }
\livretVerse#7 { Pour la combler de bien-faits, }
\livretVerse#7 { Il est armé du Tonnere, }
\livretVerse#7 { Mais c’est pour donner la paix. }
\livretDidasP\justify {
  Le Chœur des Divinitez repete ces quatre derniers Vers dans le temps
  que Jupiter descend.
}
\livretFinAct Fin du premier Acte
\sep
\livretAct ACTE SECOND
\livretDescAtt\justify {
  Le Theatre devient obscurcy par des Nuages épais qui l’environnent
  de tous costez.
}
\livretScene SCENE PREMIERE
\livretPers Io
\livretRef#'CABrecit
\livretVerse#8 { Où suis-je, d’où vient ce Nuage ! }
\livretVerse#12 { Les Ondes de mon Pere, & son charmant Rivage, }
\livretVerse#10 { Ont disparu tout à coup à mes yeux ! }
\livretVerse#8 { Où puis-je trouvez un passage ? }
\livretVerse#8 { La jalouse Reine des Cieux }
\livretVerse#12 { Me fait-elle si tost acheter l’avantage }
\livretVerse#8 { De plaire au plus puissant des Dieux ? }
\livretVerse#12 { Que vois-je ! quel éclat se répand dans ces lieux ? }
\livretDidasP\justify {
  Jupiter paroist, & les Nuages qui obscurcissent le Theatre sont illuminez
  & peints de couleurs les plus brillantes & les plus agreables.
}

\livretScene SCENE SECONDE
\livretDescAtt\wordwrap-center { Jupiter, Io. }
\livretPers Jupiter
\livretRef#'CBArecit
\livretVerse#12 { Vous voyez Jupiter, que rien ne vous étonne. }
\livretVerse#12 { C’est pour tromper Junon & ses regards jaloux }
\livretVerse#8 { Qu’un nuage vous environne, }
\livretVerse#8 { Belle Nymphe r’assurez-vous. }
\livretVerse#8 { Je vous aime, & pour vous le dire }
\livretVerse#12 { Je sors avec plaisir de mon suprême Empire. }
\livretVerse#12 { La foudre est dans mes mains, les Dieux me font la cour, }
\livretVerse#12 { Je tiens tout l’Univers sous mon obeïssance ; }
\livretVerse#8 { Mais si je pretens en ce jour }
\livretVerse#12 { Engager vostre cœur à m’aimer à son tour, }
\livretVerse#8 { Je fonde moins mon esperance }
\livretVerse#8 { Sur la grandeur de ma puissance, }
\livretVerse#8 { Que sur l’excés de mon amour. }
\livretPers Io
\livretVerse#12 { Que sert-il qu’icy-bas vostre amour me choisisse ? }
\livretVerse#12 { L’honneur m’en vient trop tard, j’ay formé d’autres nœuds : }
\livretVerse#12 { Il falloit que ce bien pour combler tous mes vœux, }
\livretVerse#8 { Ne me coûtast point d’injustice, }
\livretVerse#8 { Et ne fit point de mal-heureux. }
\livretPers Jupiter
\livretVerse#7 { C’est une assez grande gloire }
\livretVerse#7 { Pour vostre premier Vainqueur, }
\livretVerse#8 { D’estre encor dans vostre memoire, }
\livretVerse#12 { Et de me disputer si long-temps vostre Cœur. }
\livretPers Io
\livretVerse#12 { La Gloire doit forcer mon cœur à se défendre. }
\livretVerse#12 { Si vous sortez du Ciel pour chercher les douceurs }
\livretVerse#4 { D’une amour tendre, }
\livretVerse#12 { Vous pourrez aisément attaquer d’autres Cœurs, }
\livretVerse#8 { Qui feront gloire de se rendre. }
\livretPers Jupiter
\livretVerse#12 { Il n’est rien dans les Cieux, il n’est rien icy-bas, }
\livretVerse#8 { De si charmant que vos appas ; }
\livretVerse#12 { Rien ne peut me toucher d’une flame si forte ; }
\livretVerse#8 { Belle Nymphe vous l’emportez }
\livretVerse#6 { Sur les autres Beautez, }
\livretVerse#8 { Autant que Jupiter l’emporte }
\livretVerse#8 { Sur les autres Divinitez. }
\livretVerse#12 { Verrez-vous tant d’amour avec indifference ? }
\livretVerse#12 { Quel trouble vous saisit ? où tournez-vous vos pas ? }
\livretPers Io
\livretVerse#7 { Mon Cœur en vostre presence }
\livretVerse#7 { Fait trop peu de resistance ; }
\livretVerse#6 { Contentez-vous, helas ! }
\livretVerse#6 { D’étonner ma constance, }
\livretVerse#6 { Et n’en triomphez pas. }
\livretPers Jupiter
\livretVerse#12 { Et pourquoy craignez-vous Jupiter qui vous aime ? }
\livretPers Io
\livretVerse#8 { Je crains tout, je me crains moy-mesme. }
\livretPers Jupiter
\livretVerse#12 { Quoy, voulez-vous me fuïr ? }
\livretPers Io
\livretVerse#12 { \transparent { Quoy, voulez-vous me fuïr ? } C’est mon dernier espoir. }
\livretPers Jupiter
\livretVerse#12 { Ecoutez mon amour. }
\livretPers Io
\livretVerse#12 { \transparent { Ecoutez mon amour. } Ecoutez mon Devoir. }
\livretPers Jupiter
\livretVerse#12 { Vous avez un cœur libre, & qui peut se défendre. }
\livretPers Io
\livretVerse#12 { Non, vous ne laissez pas mon cœur en mon pouvoir. }
\livretPers Jupiter
\livretVerse#8 { Quoy, vous ne voulez pas m’entendre ? }
\livretPers Io
\livretVerse#12 { Je n’ay que trop de peine à ne le pas vouloir. }
\livretVerse#6 { Laissez-moy. }
\livretPers Jupiter
\livretVerse#6 { \transparent { Laissez-moy. } Quoy, si tost ? }
\livretPers Io
\livretVerse#6 { Je devois moins attendre ; }
\livretVerse#12 { Que ne fuyois-je, helas ! avant que de vous voir ! }
\livretPers Jupiter
\livretVerse#8 { L’Amour pour moy vous sollicite, }
\livretVerse#8 { Et je voy que vous me quittez. }
\livretPers Io
\livretVerse#8 { Le Devoir veut que je vous quitte, }
\livretVerse#8 { Et je sens que vous m’arrestez. }

\livretScene SCENE TROISIESME
\livretDescAtt\wordwrap-center {
  Mercure, Jupiter, Io.
}
\livretPers Mercure
\livretRef#'CCArecit
\livretVerse#12 { Iris est icy-bas, & Junon elle-mesme, }
\livretVerse#8 { Pourroit vous suivre dans ces lieux. }
\livretPers Jupiter
\livretVerse#6 { Pour la Nymphe que j’aime, }
\livretVerse#8 { Je crains ses transports furieux. }
\livretPers Mercure
\livretVerse#8 { Sa vengeance seroit funeste }
\livretVerse#8 { Si vostre amour estoit surpris. }
\livretPers Jupiter
\livretVerse#8 { Va, prens soin d’arrester Iris, }
\livretVerse#8 { Mon amour prendra soin du reste. }
\livretDidasP\justify {
  Io tâche à fuïr Jupiter qui la suit.
}

\livretScene SCENE QUATRIESME
\livretDescAtt\wordwrap-center {
  Mercure, Iris.
}
\livretPers Mercure
\livretRef#'CDArecit
\livretVerse#12 { Arrestez, belle Iris, différez un moment }
\livretVerse#12 { D’accomplir en ces lieux ce que Junon desire. }
\livretPers Iris
\livretVerse#8 { Vous m’arresterez vainement, }
\livretVerse#8 { Et vous n’aurez rien à me dire. }
\livretPers Mercure
\livretVerse#12 { Mais, si je vous disois que je veux vous choisir }
\livretVerse#12 { Pour attacher mon cœur d’une éternelle chaîne ? }
\livretPers Iris
\livretVerse#12 { Je vous écouterois peut-estre avec plaisir, }
\livretVerse#8 { Mais je vous croirois avec peine. }
\livretPers Mercure
\livretVerse#12 { Refusez-vous d’unir vostre cœur & le mien ? }
\livretPers Iris
\livretVerse#12 { Jupiter & Junon nous occupent sans cesse, }
\livretVerse#12 { Nos soins sont assez grands sans que l’Amour nous blesse, }
\livretVerse#12 { Nous n’avons pas tous deux le loisir d’aimer bien. }
\livretPers Mercure
\livretVerse#8 { Si je fais ma premiere affaire, }
\livretVerse#7 { De vous voir, & de vous plaire ? }
\livretPers Iris
\livretVerse#8 { Je feray mon premier devoir }
\livretVerse#7 { De vous plaire, & de vous voir. }
\livretPers Mercure
\livretVerse#4 { Un cœur fidelle }
\livretVerse#8 { A pour moy de charmants appas : }
\livretVerse#12 { Vous avez mille attraits, vous n’estes que trop belle, }
\livretVerse#8 { Mais je crains que vous n’ayez pas }
\livretVerse#4 { Un Cœur fidelle. }
\livretPers Iris
\livretVerse#6 { Pourquoy craignez-vous tant }
\livretVerse#6 { Que mon cœur se dégage ? }
\livretVerse#8 { Je vous permets d’estre inconstant, }
\livretVerse#8 { Si-tost que je seray volage. }
\livretPers Mercure, & Iris
\livretVerse#10 { Promettez-moy de constantes amours ; }
\livretVerse#10 { Je vous promets de vous aimer toûjours. }
\livretPers Mercure
\livretVerse#8 { Que la feinte entre nous finisse ; }
\livretPers Iris
\livretVerse#8 { Parlons sans mystere en ce jour. }
\livretPers Mercure, & Iris
\livretVerse#5 { Le moindre artifice }
\livretVerse#5 { Offense l’Amour. }
\livretPers Iris
\livretVerse#12 { Quel soin presse icy-bas Jupiter de descendre ? }
\livretPers Mercure
\livretVerse#12 { Le seul bien des Mortels luy fait quitter les Cieux. }
\livretVerse#12 { Mais quel soupçon nouveau Junon peut-elle prendre ? }
\livretVerse#12 { Ne suivroit-elle point Jupiter en ces lieux ? }
\livretPers Iris
\livretVerse#12 { Dans les Jardins d’Hébé Junon vient de se rendre. }
\livretDidasP\justify {
  Junon paroist au milieu d’un Nuage qui s’avance.
}
\livretPers Mercure
\livretVerse#12 { Un Nuage entr’ouvert la découvre à mes yeux. }
\livretVerse#8 { Iris parle ainsi sans mystere ? }
\livretVerse#12 { C’est ainsi que je puis me fier à sa foy ? }
\livretPers Iris
\livretVerse#12 { Ne me reprochez-pas que je suis peu sincere, }
\livretVerse#8 { Vous ne l’estes pas plus que moy. }
\livretPers Mercure, & Iris
\livretVerse#5 { Gardez pour quelqu’autre }
\livretVerse#5 { Vostre amour trompeur ; }
\livretVerse#5 { Je reprens mon cœur, }
\livretVerse#5 { Reprenez le vostre. }
\livretDidasP\justify {
  Le Nuage s’approche de Terre, & Junon descend.
}

\livretScene SCENE CINQUIESME
\livretDescAtt\wordwrap-center {
  Junon, Iris.
}
\livretPers Iris
\livretRef#'CEBrecit
\livretVerse#12 { J’ay cherché vainement la Fille d’Inachus. }
\livretPers Junon
\livretVerse#12 { Ah, je n’ay pas besoin d’en sçavoir davantage, }
\livretVerse#8 { Non, Iris, ne la cherchons plus. }
\livretVerse#12 { Jupiter, dans ces lieux, m’a donné de l’ombrage, }
\livretVerse#12 { J’ay traversé les Airs, j’ay percé le Nuage }
\livretVerse#8 { Qu’il opposoit à mes regards : }
\livretVerse#12 { Mais en vain j’ay tourné les yeux de toutes parts, }
\livretVerse#8 { Ce Dieu par son pouvoir suprême }
\livretVerse#8 { M’a caché la Nymphe qu’il aime, }
\livretVerse#12 { Et ne m’a laissé voir que des Troupeaux épars, }
\livretVerse#12 { Non, non, je ne suis point une incredule Epouse }
\livretVerse#8 { Qu’on puisse tromper aisément, }
\livretVerse#12 { Voyons qui feindra mieux de Jupiter Amant, }
\livretVerse#6 { Ou de Junon jalouse. }
\livretVerse#12 { Il est maistre des Cieux, la Terre suit sa loy, }
\livretVerse#12 { Sous sa toute-puissance il faut que tout fléchisse, }
\livretVerse#12 { Mais puisqu’il ne pretend s’armer que d’artifice, }
\livretVerse#12 { Tout Jupiter qu’il est, il est moins fort que moy. }
\livretVerse#12 { Dans ces lieux écartez, voy que la Terre est belle. }
\livretPers Iris
\livretVerse#12 { Elle honore son Maistre, & brille sous ses pas. }
\livretPers Junon
\livretVerse#8 { L’Amour, cét Amour infidelle, }
\livretVerse#8 { Qui du plus haut des Cieux l’appelle, }
\livretVerse#8 { Fait que tout luy rit icy-bas. }
\livretVerse#8 { Prés d’une Maistresse nouvelle }
\livretVerse#12 { Dans le fond des Deserts on trouve des appas, }
\livretVerse#8 { Et le Ciel mesme ne plaist pas }
\livretVerse#8 { Avec une Epouse immortelle. }

\livretScene SCENE SIXIESME
\livretDescAtt\wordwrap-center {
  Jupiter, Junon, Mercure, Iris.
}
\livretPers Jupiter
\livretRef#'CFArecit
\livretVerse#12 { Dans les Jardins d’Hébé vous deviez en ce jour }
\livretVerse#12 { D’une nouvelle Nymphe augmenter vostre Cour ; }
\livretVerse#12 { Quel dessein si pressant dans ces lieux vous ameine ? }
\livretPers Junon
\livretVerse#8 { Je ne vous suivray pas plus loin. }
\livretVerse#12 { Je viens de vostre amour attendre un nouveau soin : }
\livretVerse#12 { Ne vous étonnez pas qu’on vous quitte avec peine, }
\livretVerse#12 { Et que de Jupiter on ait toûjours besoin. }
\livretVerse#8 { Vous m’aimez, & j’en suis certaine. }
\livretPers Jupiter
\livretVerse#6 { Souhaitez, je promets }
\livretVerse#8 { Que vos vœux seront satisfaits. }
\livretPers Junon
\livretVerse#12 { J’ay fait choix d’une Nymphe, & déja la Déesse, }
\livretVerse#6 { De l’aimable Jeunesse }
\livretVerse#8 { Se prepare à la recevoir ; }
\livretVerse#12 { Mais je n’ose sans vous disposer de personne }
\livretVerse#6 { Si j’ay quelque pouvoir, }
\livretVerse#6 { Je n’en pretens avoir }
\livretVerse#8 { Qu’autant que vostre amour d’en donne. }
\livretVerse#12 { Ce don de vostre main me sera precieux. }
\livretPers Jupiter
\livretVerse#12 { J’approuve vos desirs, que rien n’y soit contraire. }
\livretVerse#8 { Mercure, ayez soin de luy plaire, }
\livretVerse#12 { Et portez à son gré mes ordres en tous lieux. }
\livretVerse#12 { Que tout suive les loix de la Reine des Cieux. }
\livretPers Mercure, & Iris
\livretVerse#12 { Que tout suive les loix de la Reine des Cieux. }
\livretPers Jupiter
\livretVerse#12 { Parlez, que vostre choix hautement se declare. }
\livretPers Junon
\livretVerse#12 { La Nymphe qui me plaist ne vous déplaira pas. }
\livretVerse#8 { Vous ne verrez point icy bas }
\livretVerse#12 { De merite plus grand, ny de Beauté plus rare : }
\livretVerse#8 { Les honneurs que je luy prepare }
\livretVerse#6 { Ne luy sont que trop dûs ; }
\livretVerse#12 { Enfin, Junon choisit la fille d’Inachus. }
\livretPers Jupiter
\livretVerse#12 { La fille d’Inachus ! }
\livretPers Junon
\livretVerse#12 { \transparent { La fille d’Inachus ! } Declarez-vous pour elle. }
\livretVerse#12 { Peut-on voir à ma suite une Nymphe plus belle, }
\livretVerse#8 { Plus capable d’orner ma Cour, }
\livretVerse#12 { Et de marquer pour moy le soin de vostre amour ? }
\livretVerse#12 { Vous me l’avez promise, & je vous la demande. }
\livretPers Jupiter
\livretVerse#12 { Vous ne sçauriez combler d’une gloire trop grande }
\livretVerse#8 { La Nymphe que vous choisissez, }
\livretVerse#4 { Junon commande, }
\livretVerse#8 { Allez, Mercure, obeïssez. }
\livretPers Iris
\livretVerse#4 { Junon commande, }
\livretVerse#8 { Allez, Mercure, obeïssez. }

\livretScene SCENE VII
\livretDescAtt\wordwrap-center {
  Le Theatre change, & represente les Jardins d’Hebé, Déesse de la Jeunesse.
}
\livretDidasP\wordwrap-center {
  Hebé, Troupe des Jeux et de Plaisirs. Troupe de Nymphes de la Suite de Junon.
}
\livretDidasP\justify {
  Hebé, Déesse de la Jeunesse. Six Nymphes de Junon suivantes.
  Vingt-quatre Jeux & Plaisirs chantants.
  Neuf Jeux & Plaisirs dançants.
}
\livretDidasP\justify {
  Des Jeux & des Plaisirs s’avancent en dançant devant la Déesse Hebé.
}
\livretPers Hebé
\livretRef#'CGBchoeur
\livretVerse#6 { Les Plaisirs les plus doux }
\livretVerse#6 { Sont faits pour la Jeunesse. }
\livretVerse#8 { Venez Jeux charmants, venez-tous ; }
\livretVerse#10 { Gardez-vous bien d’amener avec vous }
\livretVerse#6 { La severe Sagesse : }
\livretVerse#6 { Les plaisirs les plus doux }
\livretVerse#6 { Sont faits pour la Jeunesse. }
\livretVerse#8 { Fuyez, fuyez, sombre Tristesse, }
\livretVerse#8 { Noirs chagrins, fuyez loin de nous, }
\livretVerse#12 { Vous estes destinez pour l’affreuse veillesse ! }
\livretVerse#6 { Les plaisirs les plus doux }
\livretVerse#6 { Sont faits pour la Jeunesse. }
\livretDidasP\justify {
  Le Chœur repete ces deux derniers Vers.
}
\livretDidasP\justify {
  Les Jeux, les Plaisirs, & les Nymphes de Junon se divertissent par des Dances
  & par des Chansons, en attendant la nouvelle Nymphe dont Junon veut faire
  choix.
}
\livretPersDidas Deux Nymphes chantent ensemble
\livretRef#'CGDnymphes
\livretVerse#7 { Aymez, profitez du temps, }
\livretVerse#5 { Jeunesse charmante, }
\livretVerse#7 { Rendez vos desirs contents. }
\livretVerse#5 { Tout rit, tout enchante }
\livretVerse#5 { Dans les plus beaux ans. }
\livretVerse#5 { L’Amour vous éclaire, }
\livretVerse#5 { Marchez sur ses pas ; }
\livretVerse#5 { Cherchez à vous faire }
\livretVerse#5 { Des nœuds pleins d’appas, }
\livretVerse#5 { Que vous sert de plaire, }
\livretVerse#5 { Si vous n’aimez pas ? }
\null
\livretVerse#7 { Pourquoy craignez-vous d’aimer, }
\livretVerse#5 { Beautez inhumaines, }
\livretVerse#7 { Cessez de vous allarmer ; }
\livretVerse#5 { L’Amour a des peines, }
\livretVerse#5 { Qui doivent charmer. }
\livretVerse#5 { Ce Dieu vous éclaire, }
\livretVerse#5 { Marchez sur ses pas. }
\livretVerse#5 { Cherchez à vous faire }
\livretVerse#5 { Des nœuds pleins d’appas, }
\livretVerse#5 { Que vous sert de plaire, }
\livretVerse#5 { Si vous n’aimez pas ? }
\livretPers Chœur
\livretRef#'CGFchoeur
\livretVerse#6 { Que ces Lieux ont d’attraits, }
\livretVerse#6 { Goûtons-en bien les charmes ; }
\livretVerse#6 { L’Amour n’y fait jamais }
\livretVerse#6 { Verser de tristes larmes, }
\livretVerse#6 { Les soins, & les allarmes, }
\livretVerse#6 { N’en troublent point la paix, }
\livretVerse#7 { Joüissons dans ces Retraites, }
\livretVerse#7 { Des douceurs les plus parfaites, }
\livretVerse#7 { Suivez-nous charmants Plaisirs, }
\livretVerse#6 { Comblez tous nos desirs. }
\null
\livretVerse#6 { Voyons couler ces Eaux }
\livretVerse#6 { Dans ces riants Boccages ; }
\livretVerse#6 { Chantez petits Oyseaux, }
\livretVerse#6 { Chantez sur ces feuillages ; }
\livretVerse#6 { Joignez vos doux ramages }
\livretVerse#6 { A nos Concerts nouveaux. }
\livretVerse#7 { Joüissons dans ces Retraites, }
\livretVerse#7 { Des douceurs les plus parfaites, }
\livretVerse#7 { Suivez-nous charmants Plaisirs, }
\livretVerse#6 { Comblez tous nos desirs. }

\livretScene SCENE VIII
\livretDescAtt\wordwrap-center {
  Io, Mercure, Iris, Hebé, les Jeux, les Plaisirs,
  Trouppe de Nymphes de la Suite de Junon.
}
\livretPersDidas Mercure, & Iris conduisant Io
\livretRef#'CHAchoeur
\livretVerse#12 { Servez, Nymphe, servez, avec un soin fidelle, }
\livretVerse#8 { La puissante Reine des Cieux : }
\livretVerse#8 { Suivez dans ces aimables lieux, }
\livretVerse#6 { La Jeunesse immortelle ; }
\livretVerse#8 { Tout plaist, & tout rit avec elle. }
\livretDidasP\justify {
  Hebé, & les Nymphes reçoivent Io.
}
\livretPers Hebé, & le Chœur des Nymphes
\livretVerse#7 { Que c’est un plaisir charmant }
\livretVerse#5 { D’estre jeune & belle ! }
\livretVerse#7 { Triomphez à tout moment, }
\livretVerse#7 { D’une Conqueste nouvelle : }
\livretVerse#7 { Que c’est un plaisir charmant }
\livretVerse#5 { D’estre jeune & belle. }
\livretFinAct Fin du second Acte
\sep
\livretAct ACTE III
\livretDescAtt\justify {
  Le Theatre change, & represente la Solitude où Argus fait sa demeure prés
  d’un Lac, & au milieu d’une Forests.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center { Argus, Io. }
\livretPers Argus
\livretRef#'DABrecit
\livretVerse#8 { Dans ce Solitaire Séjour }
\livretVerse#12 { Vous estes sous ma garde, & Junon vous y laisse : }
\livretVerse#8 { Mes yeux veilleront tour à tour, }
\livretVerse#8 { Et vous observeront sans cesse. }
\livretPers Io
\livretVerse#12 { Est-ce là le bon-heur que Junon m’a promis ? }
\livretVerse#12 { Argus apprenez-moy quel crime j’ay commis. }
\livretPers Argus
\livretVerse#5 { Vous estes aimable, }
\livretVerse#7 { Vos yeux devoient moins charmer ; }
\livretVerse#5 { Vous estes coupable }
\livretVerse#7 { De vous faire trop aimer. }
\livretPers Io
\livretVerse#12 { Ne me déguisez rien, de quoy m’accuse-t’elle ? }
\livretVerse#12 { Quelle offense à ses yeux me rend si criminelle ? }
\livretVerse#12 { Ne pouray-je appaiser son funeste couroux ? }
\livretPers Argus
\livretVerse#7 { C’est une offense cruelle }
\livretVerse#5 { De paroistre belle }
\livretVerse#5 { A des yeux jaloux. }
\livretVerse#12 { L’Amour de Jupiter a trop paru pour vous. }
\livretPers Io
\livretVerse#12 { Je suis perduë, ô Ciel ! si Junon est jalouse. }
\livretPers Argus
\livretVerse#7 { On ne plaist guere à l’Epouse, }
\livretVerse#7 { Lors qu’on plaist tant à l’Epoux. }
\livretVerse#12 { Vous n’en serez pas mieux d’estre ingrate & volage. }
\livretVerse#8 { Vous quittez un fidelle Amant }
\livretVerse#10 { Pour recevoir un plus brillant hommage ; }
\livretVerse#6 { Mais c’est un avantage }
\livretVerse#8 { Que vous payerez cherement. }
\livretVerse#12 { Vous n’en serez pas mieux d’estre ingrate & volage. }
\livretVerse#12 { J’ay l’ordre d’enfermer vos dangereux appas, }
\livretVerse#12 { La Déesse défend que vous voyez personne. }
\livretPers Io
\livretVerse#12 { Aux rigueurs de Junon Jupiter m’abandonne ; }
\livretVerse#8 { Non, Jupiter ne m’aime pas. }
\livretDidasPPage\wordwrap {
  Argus enferme Io.
}

\livretScene SCENE SECONDE
\livretDescAtt\wordwrap-center { Hierax, Argus. }
\livretPersDidas Hierax voyant Io qui entre dans la Demeure d’Argus
\livretRef#'DBArecit
\livretVerse#8 { La Perfide craint ma presence, }
\livretVerse#12 { Elle me fuit en vain, & j’iray la chercher… }
\livretPersDidas Argus arrestant Hierax
\livretVerse#8 { Non. }
\livretPers Hierax
\livretVerse#8 { \transparent { Non. } Laissez-moy luy reprocher }
\livretVerse#6 { Sa cruelle inconstance. }
\livretPers Argus
\livretVerse#7 { Non, on ne la doit point voir. }
\livretPers Hierax
\livretVerse#8 { Quoy, Junon me devient contraire ? }
\livretPers Argus
\livretVerse#12 { L’ordre est exprés pour tous, perdez un vain espoir. }
\livretPers Hierax
\livretVerse#12 { L’amitié fraternelle a si peu de pouvoir. }
\livretPers Argus
\livretVerse#12 { Non, je ne connois plus ny d’Amy, ny de Frere, }
\livretVerse#8 { Je ne connois que mon devoir. }
\livretVerse#12 { Laissez la Nymphe en paix, ce n’est plus vous qu’elle aime. }
\livretPers Hierax
\livretVerse#12 { Quel est l’heureux Amant qui s’en est fait aimer. }
\livretVerse#12 { Nommez-le-moy. }
\livretPers Argus
\livretVerse#12 { \transparent { Nommez-le-moy. } Tremblez à l’entendre nommer, }
\livretVerse#12 { C’est un Dieu tout-puissant, c’est Jupiter luy-mesme. }
\livretPers Hierax
\livretVerse#12 { O Dieux ! }
\livretPers Argus
\livretVerse#12 { \transparent { O Dieux ! } Dégagez-vous d’un amour si fatal, }
\livretVerse#10 { Sans balancer, il faut vous y resoudre, }
\livretVerse#8 { C’est un redoutable Rival }
\livretVerse#8 { Qu’un Amant qui lance la foudre. }
\livretPers Hierax
\livretVerse#10 { Dieux tout-puissants ! ah, vous estiez jaloux }
\livretVerse#12 { De la felicité que vous m’avez ravie, }
\livretVerse#10 { Dieux tout-puissants ! ah, vous estiez jaloux }
\livretVerse#8 { De me voir plus heureux que vous. }
\livretVerse#12 { Vous n’avez pû souffrir le bon-heur de ma vie, }
\livretVerse#10 { Et je voyois vos grandeurs sans envie, }
\livretVerse#12 { J’aimois, j’estois aimé, mon sort estoit trop doux ; }
\livretVerse#10 { Dieux tout-puissants ! ah, vous estiez jaloux }
\livretVerse#12 { De la felicité que vous m’avez ravie, }
\livretVerse#10 { Dieux tout-puissants ! ah, vous estiez jaloux }
\livretVerse#8 { De me voir plus heureux que vous. }
\livretPers Argus
\livretVerse#8 { Heureux qui peut briser sa chaîne ! }
\livretVerse#8 { Finissez une plainte vaine, }
\livretVerse#8 { Meprisez l’infidelité, }
\livretVerse#8 { Un Cœur ingrat vaut-il la peine }
\livretVerse#6 { D’estre tant regretté. }
\livretVerse#8 { Heureux qui peut briser sa chaîne. }
\livretPers Hierax & Argus
\livretVerse#8 { Heureux qui peut briser sa chaîne. }
\livretPers Argus
\livretVerse#6 { Liberté, liberté. }

\livretScene SCENE TROISIESME
\livretDescAtt\wordwrap-center {
  Argus, Hierax, une Nymphe qui represente Syrinx.
  Troupe de Nymphes en habit de chasse.
}
\livretDidas\justify {
  La Nypmhe Syrinx. Huit Nymphes Compagnes de Syrinx chantantes.
  Quatre autres Nymphes chantantes. Six Nymphes Compagnes de Syrinx
  dançantes.
}
\livretPers Syrinx, Chœur de Nymphes
\livretRef#'DCAchoeur
\livretVerse#6 { Liberté, liberté. }
\livretDidas\justify {
  Une partie des Nymphes dancent dans le temps que les autres chantent.
}
\livretPers Argus, & Hierax
\livretVerse#12 { Quelles Dances, quels chants, & quelle nouveauté. }
\livretPers Syrinx & les Nymphes
\livretVerse#7 { S’il est quelque bien au monde, }
\livretVerse#5 { C’est la liberté. }
\livretPers Argus, & Hierax
\livretVerse#4 { Que voulez-vous ? }
\livretPers Chœur de Nymphes
\livretVerse#6 { Liberté, liberté. }
\livretPers Argus, & Hierax
\livretVerse#10 { Que voulez-vous ? il faut qu’on nous réponde. }
\livretPers Syrinx & les Nymphes
\livretVerse#7 { S’il est quelque bien au monde, }
\livretVerse#5 { C’est la liberté. }

\livretScene SCENE IV
\livretDescAtt\wordwrap-center {
  Argus, Hierax, Syrinx, Troupe de Nymphes, Mercure deguisé en Berger,
  Troupe de Bergers, Toupe de Satyres, Troupe de Sylvains.
}
\livretPers Mercure, Chœur des Nymphes, de Bergers, & de Sylvains
\livretRef#'DDAchoeur
\livretVerse#6 { Liberté, liberté. }
\livretPersDidas Mercure déguisé en Berger parlant à Argus
\livretRef#'DDBrecit
\livretVerse#12 { De la Nymphe Syrinx Pan cherit la memoire, }
\livretVerse#12 { Il en regrette encor la perte chaque jour, }
\livretVerse#10 { Pour celebrer une feste à sa gloire, }
\livretVerse#10 { Ce Dieu luy-mesme assemble icy sa Cour : }
\livretVerse#12 { Il veut que du mal-heur de son fidelle Amour }
\livretVerse#12 { Un spectacle touchant represente l’histoire. }
\livretPers Argus
\livretVerse#12 { C’est un plaisir pour nous ; poursuivez, j’y consens, }
\livretVerse#12 { Je ne m’oppose point à des Jeux innocens. }
\livretDidasP\justify {
  Argus va prendre place sur un siege de gazon proche de l’endroit
  où Io est enfermée, & fait placer Hierax de l’autre costé.
}
\livretPersDidas Mercure parlant à part à toute la Troupe qu’il conduit
\livretVerse#12 { Il donne dans le piege ; achevez sans remise, }
\livretVerse#12 { Achevez de surprendre Argus, & tous ses yeux : }
\livretVerse#10 { Si vous tentez une grande entreprise, }
\livretVerse#12 { Mercure vous conduit, l’Amour vous favorise, }
\livretVerse#10 { Et vous servez le plus puissant des Dieux. }
\livretDidasP\justify {
  Mercure, les Bergers, les Satyres, & les Sylvains r’entrent derriere le Theatre.
}

\livretScene SCENE CINQUIESME
\livretDescAtt\wordwrap-center {
  Argus, Hierax, Syrinx, Troupe de Nymphes.
}
\livretPers Syrinx, & le Chœur des Nymphes
\livretRef#'DEAchoeur
\livretVerse#6 { Liberté, liberté. }
\livretVerse#7 { S’il est quelque bien au monde, }
\livretVerse#5 { C’est la liberté. }
\livretVerse#6 { Liberté, liberté. }
\livretPers Syrinx
\livretVerse#12 { L’Empire de l’Amour n’est pas moins agité }
\livretVerse#6 { Que l’Empire de l’Onde ; }
\livretVerse#10 { Ne cherchons point d’autre felicité }
\livretVerse#10 { Qu’un doux loisir dans une paix profonde. }
\livretPers Syrinx, & le Chœur
\livretVerse#7 { S’il est quelque bien au monde, }
\livretVerse#5 { C’est la liberté. }
\livretVerse#6 { Liberté, liberté. }
\livretDidasP\justify {
  Dans le temps qu’une partie des Nymphes chante, le reste de la Troupe dance.
}

\livretScene SCENE SIXIESME
\livretDescAtt\wordwrap-center {
  Un des Sylvains representant le Dieu Pan.
  Troupe de Bergers, Troupe de Satyrs, Troupe de Sylvains,
  Syrinx, Troupe de Nymphes. Argus & Hierax.
}
\livretDidasP\justify {
  Des Bergers & des Sylvains dançants & chantants viennent offrir des
  Presens de fruits & de Fleurs à la Nymphe Syrinx, & tâchent de luy
  persuader de n’aller point à la Chasse, & de s’engager sous les loix
  de l’Amour.
}
\livretDidasP\justify {
  Douze Satyres chantants, & portans des Presents à Syrinx.
  Quatre Satyrs joüants de la Flûte.
  Douze Bergers portants des Presents à Syrinx.
  Quatre Bergers joüants de la Flûte.
  Quatre Sylvains dançants.
  Quatre Bergers heroïques dançants.
  Deux Bergers chantants.
}
\livretPers [Deux Bergers]
\livretRef#'DFBbergers
\livretVerse#7 { Quel bien devez-vous attendre, }
\livretVerse#8 { Beauté qui chassez dans ces Bois ? }
\livretVerse#5 { Que pouvez-vous prendre }
\livretVerse#5 { Qui vaille un Cœur tendre }
\livretVerse#5 { Soûmis à vos Loix ? }
\livretVerse#5 { Ce n’est qu’en aimant }
\livretVerse#6 { Qu’on trouve un sort charmant, }
\livretVerse#8 { Aimez, enfin, à vostre tour, }
\livretVerse#8 { Il faut que tout cede à l’Amour : }
\livretVerse#8 { Il sçait fraper d’un coup certain }
\livretVerse#8 { Le Cerf leger qui fuit en vain ; }
\livretVerse#8 { Jusques dans les Antres secrets, }
\livretVerse#5 { Au fond des Forests, }
\livretVerse#6 { Tout doit sentir ses traits. }
\null
\livretVerse#7 { Lors que l’Amour vous appelle, }
\livretVerse#8 { Pourquoy fuyez-vous ses plaisirs ? }
\livretVerse#5 { La Roze nouvelle }
\livretVerse#5 { N’en est que plus belle }
\livretVerse#5 { D’aimer les Zephirs. }
\livretVerse#5 { Ce n’est qu’en aimant }
\livretVerse#6 { Qu’on trouve un sort charmant, }
\livretVerse#8 { Aimez, enfin, à vostre tour, }
\livretVerse#8 { Il faut que tout cede à l’Amour : }
\livretVerse#8 { Il sçait fraper d’un coup certain }
\livretVerse#8 { Le Cerf leger qui fuit en vain ; }
\livretVerse#8 { Jusques dans les Antres secrets, }
\livretVerse#5 { Au fond des Forests, }
\livretVerse#6 { Tout doit sentir ses traits. }
\livretPers Pan
\livretRef#'DFDrecit
\livretVerse#8 { Je vous aime, Nymphe charmante, }
\livretVerse#12 { Un Amant immortel cherche à plaire à vos yeux. }
\livretPers Syrinx
\livretVerse#12 { Pan est un Dieu puissant, je revere les Dieux, }
\livretVerse#8 { Mais le nom d’Amant m’épouvante. }
\livretPers Pan
\livretVerse#12 { Pour vous faire trouver le nom d’Amant plus doux, }
\livretVerse#8 { J’y joindray le titre d’Epoux. }
\livretVerse#6 { Je n’auray pas de peine }
\livretVerse#4 { A m’engager }
\livretVerse#6 { Dans une aimable chaîne, }
\livretVerse#6 { Je n’auray pas de peine }
\livretVerse#4 { A m’engager }
\livretVerse#6 { Pour ne jamais changer. }
\livretVerse#8 { Aimez un Dieu qui vous adore, }
\livretVerse#8 { Unissons-nous d’un nœud charmant. }
\livretPers Syrinx
\livretVerse#7 { Un Epoux doit estre encore }
\livretVerse#7 { Plus à craindre qu’un Amant. }
\livretPers Pan
\livretVerse#8 { Dissipez de vaines allarmes, }
\livretVerse#8 { Eprouvez l’Amour & ses charmes, }
\livretVerse#8 { Connoissez ses plus doux appas : }
\livretVerse#5 { Non, ce ne peut estre }
\livretVerse#7 { Que faute de le connoistre }
\livretVerse#5 { Qu’il ne vous plaist pas. }
\livretPers Syrinx
\livretVerse#8 { Les maux d’autruy me rendront sage. }
\livretVerse#4 { Ah ! quel mal-heur }
\livretVerse#8 { De laisser engager son cœur ! }
\livretVerse#12 { Pourquoy faut-il passer le plus beau de son âge }
\livretVerse#8 { Dans une mortelle langueur ? }
\livretVerse#4 { Ah, quel mal-heur ! }
\livretVerse#8 { Pourquoy n’avoir pas le courage }
\livretVerse#8 { De s’affranchir de la rigueur }
\livretVerse#6 { D’un funeste esclavage ? }
\livretVerse#4 { Ah ! quel mal-heur }
\livretVerse#8 { De laisser engager son cœur ! }
\livretPers Pan
\livretVerse#4 { Ah, quel dommage }
\livretVerse#8 { Que vous ne sçachiez pas aimer ! }
\livretVerse#12 { Que vous sert-il d’avoir tant d’attraits en partage, }
\livretVerse#12 { Si vous en negligez le plus grand avantage ? }
\livretVerse#10 { Que vous sert-il de sçavoir tout charmer ? }
\livretVerse#4 { Ah, quel dommage }
\livretVerse#8 { Que vous ne sçachiez pas aimer ! }
\livretPers Chœur de Sylvains, de Satyres, & de Bergers
\livretRef#'DFEchoeur
\livretVerse#4 { Aymons sans cesse. }
\livretPers Chœur de Nymphes
\livretVerse#4 { N’aimons jamais. }
\livretPers Chœur de Sylvains, de Satyres, & de Bergers
\livretVerse#8 { Cedons à l’Amour qui nous presse, }
\livretVerse#8 { Pour vivre heureux aimons sans cesse. }
\livretPers Chœur de Nymphes
\livretVerse#4 { Pour vivre en paix, }
\livretVerse#4 { N’aimons jamais. }
\livretPers Syrinx
\livretVerse#12 { Le chagrin suit toûjours les Cœurs que l’Amour blesse. }
\livretPers Pan
\livretVerse#6 { La tranquile Sagesse }
\livretVerse#8 { N’a que des plaisirs imparfaits. }
\livretPers Chœur de Sylvains, de Satyres, & de Bergers
\livretVerse#4 { Aymons sans cesse. }
\livretPers Chœur de Nymphes
\livretVerse#4 { N’aimons jamais. }
\livretPers Syrinx
\livretVerse#8 { On ne peut aimer sans foiblesse. }
\livretPers Pan
\livretVerse#8 { Que cette foiblesse a d’attraits ! }
\livretPers Chœur de Sylvains, de Satyres, & de Bergers
\livretVerse#4 { Aymons sans cesse. }
\livretPers Chœur de Nymphes
\livretVerse#4 { N’aimons jamais. }
\livretPers Chœur de Sylvains, de Satyres, & de Bergers
\livretVerse#8 { Cedons à l’Amour qui nous presse, }
\livretVerse#8 { Pour vivre heureux aimons sans cesse. }
\livretPers Chœur de Nymphes
\livretVerse#4 { Pour vivre en paix, }
\livretVerse#4 { N’aimons jamais. }
\livretPers Syrinx
\livretRef#'DFFchoeur
\livretVerse#12 { Faut-il qu’en vains discours un si beau jour se passe, }
\livretVerse#12 { Mes Compagnes courons dans le fond des Forests. }
\livretVerse#12 { Voyons qui d’entre-nous se sert mieux de ses traits. }
\livretVerse#5 { Courons à la Chasse. }
\livretPers Chœurs
\livretVerse#5 { Courons à la Chasse. }
\livretPersDidas Syrinx revenant sur le Theatre suivie de Pan
\livretVerse#8 { Pourquoy me suivre de si prés ? }
\livretPers Pan
\livretVerse#13 { Pourquoy fuïr que vous aime ? }
\livretPers Syrinx
\livretVerse#13 { \transparent { Pourquoy fuïr que vous aime ? } Un Amant m’embarasse. }
\livretPers Syrinx & les Chœurs derriere le Theatre
\livretVerse#5 { Courons à la Chasse. }
\livretPersDidas Pan \line { revenant une seconde fois sur la Scene suivant toûjours Syrinx }
\livretRef#'DFGchoeur
\livretVerse#12 { Je ne puis vous quitter, mon cœur s’attache à vous }
\livretVerse#8 { Par des nœuds trop forts & trop doux… }
\livretPers Syrinx
\livretVerse#12 { Mes Compagnes ? Venez ?… C’est en vain que j’appelle. }
\livretPers Pan
\livretVerse#8 { Ecoutez, Ingrate, écoutez, }
\livretVerse#8 { Un Dieu charmé de vos beautez, }
\livretVerse#8 { Qui vous jure un amour fidelle. }
\livretPersDidas Syrinx fuyant
\livretVerse#12 { Je declare à l’Amour une guerre immortelle. }
\livretPersDidas Troupe de Bergers qui arrestent Syrinx
\livretVerse#5 { Cruelle, arrestez. }
\livretPersDidas Troupe de Sylvains & de Satyres qui arrestent Syrinx
\livretVerse#5 { Arrestez, cruelle. }
\livretPers Syrinx
\livretVerse#8 { On me retient de tous costez. }
\livretPers Chœurs de Satyres, de Sylvains, & de Bergers
\livretVerse#5 { Cruelle, arrestez. }
\livretPers Syrinx
\livretVerse#8 { Dieux, Protecteurs de l’innocence, }
\livretVerse#8 { Nayades, Nymphes de ces Eaux, }
\livretVerse#8 { J’implore icy vostre assistance. }
\livretDidasP\justify {
  Syrinx se jette dans les Eaux.
}
\livretPersDidas Pan suivant Syrinx dans le Lac où elle s’est jettée
\livretVerse#12 { Où vous exposez-vous ? Quels prodiges nouveaux ? }
\livretVerse#7 { La Nymphe est changée en Rozeaux ! }
\livretDidasP\justify {
  Le vent penetre dans les Roseaux, & leur fait former un bruit plaintif.
}
\livretRef#'DFHplainte
\livretVerse#12 { Helas ! quel bruit ! qu’entens-je ! Ah quelle voix nouvelle ! }
\livretVerse#12 { La Nymphe tâche encor d’exprimer ses regrets. }
\livretVerse#12 { Que son murmure est doux ! que sa plainte a d’attraits, }
\livretVerse#10 { Ne cessons point de nous plaindre avec-elle. }
\livretVerse#8 { R’animons les Restes charmants }
\livretVerse#8 { D’une Nymphe qui fut si belle, }
\livretVerse#12 { Elle répond encore à nos gemissements, }
\livretVerse#10 { Ne cessons point de nous plaindre avec elle. }
\livretDidasP\justify {
  Pan donne des Roseaux aux Bergers, aux Satyres, & aux Sylvains,
  qui en forment un Concert de Flûtes.
}
\livretPers Pan
\livretVerse#12 { Les yeux qui m’ont charmés ne verront plus le jour. }
\livretVerse#8 { Estoit-ce ainsi, cruel Amour, }
\livretVerse#12 { Qu’il falloit te vanger d’une Beauté rebelle, }
\livretVerse#12 { N’auroit-il pas suffi de t’en rendre vainqueur, }
\livretVerse#12 { Et de voir dans tes fers son insensible Cœur }
\livretVerse#12 { Brûler avec le mien d’un ardeur éternelle, }
\livretVerse#8 { Que tout ressente mes tourments. }
\livretPersDidas Pan, & deux Bergers accompagnez du Concert de Flûtes
\livretVerse#8 { R’animons les Restes charmants }
\livretVerse#8 { D’une Nymphe qui fut si belle, }
\livretVerse#12 { Elle répond encor à nos gemissemens, }
\livretVerse#10 { Ne cessons point de nous plaindre avec elle. }
\livretDidasP\justify {
  Argus commence à s’assoupir, Mercure déguisé en Berger s’approche de luy,
  & acheve de l’endormir en le touchant de son Caducée.
}
\livretPers Pan
\livretVerse#12 { Que ces roseaux plaintifs soient à jamais aimez… }
\livretPers Mercure
\livretVerse#12 { Il suffit, Argus dort, tous ses yeux sont fermez. }
\livretVerse#8 { Allons, que rien ne nous retarde, }
\livretVerse#8 { Délivrons la Nymphe qu’il garde. }

\livretScene SCENE VII
\livretDescAtt\wordwrap-center {
  Io, Mercure, Troupe de Sylvains, de Satyres, et de Bergers,
  Argus, Hierax.
}
\livretPersDidas Mercure \line { faisant sortir Io de la Demeure d’Argus, qu’il ouvre d‘un coup de son Caducée }
\livretRef#'DGArecit
\livretVerse#12 { Reconnoissez Mercure, & fuyez avec nous ; }
\livretVerse#12 { Eloignez-vous d’Argus avant qu’il se reveille. }
\livretPersDidas Hierax arrestant Io, & parlant à Mercure
\livretVerse#8 { Argus avec cent yeux sommeille ; }
\livretVerse#4 { Mais croyez-vous }
\livretVerse#8 { Endormir un Amant jaloux }
\livretVerse#12 { Demeurez. }
\livretPers Mercure
\livretVerse#12 { \transparent { Demeurez. } Mal-heureux, d’où te vient cette audace ? }
\livretPers Hierax
\livretVerse#12 { J’ay tout perdu, j’attens le trépas sans effroy, }
\livretVerse#8 { Un coup de foudre est une grace }
\livretVerse#8 { Pour un mal-heureux comme moy. }
\livretVerse#12 { Eveillez-vous, Argus, vous vous laissez surprendre. }
\livretPers Argus, & Hierax
\livretVerse#7 { Puissante Reine des Cieux, }
\livretVerse#7 { Junon, venez nous défendre. }
\livretPersDidas Mercure \line { frapant Argus & Hierax de son Caducée }
\livretVerse#12 { Commencez d’éprouver la colere des Dieux. }
\livretDidasP\justify {
  Argus tombe mort, & Hierax changé en Oyseau de Proye s’envole.
}
\livretPers Chœur de Sylvain, de Satyres, & de Bergers
\livretVerse#12 { Fuyons. }
\livretPers Io
\livretVerse#12 { \transparent { Fuyons. } Vous me quittez quel secours puis-je attendre ? }
\livretPers Chœur de Sylvain, de Satyres, & de Bergers
\livretVerse#8 { Fuyons, Junon vient dans ces Lieux. }

\livretScene SCENE HUITIESME
\livretDescAtt\wordwrap-center {
  Junon sur son Char, Argus, Io, Erinnis, Furie.
}
\livretPers Junon
\livretRef#'DHArecit
\livretVerse#12 { Revoy le jour, Argus, que ta figure change. }
\livretDidasP\justify {
  Argus transformé en Paon, vient se placer devant le Char de Junon.
}
\livretPers Junon
\livretVerse#12 { Et vous, Nymphe, apprenez comment Junon se vange. }
\livretVerse#12 { Sors, barbare Erinnis, sors du fond des Enfers, }
\livretVerse#12 { Vien, pren soin de servir ma vengeance fatale, }
\livretVerse#12 { Et d’en montrer l’horreur en cent Climats divers : }
\livretVerse#8 { Epouvante tout l’Univers }
\livretVerse#8 { Par les tourments de ma Rivale. }
\livretVerse#10 { Vien la punir au gré de mon couroux : }
\livretVerse#8 { Redouble ta Rage infernale, }
\livretVerse#8 { Et fay, s’il se peut, qu’elle égale }
\livretVerse#8 { La fureur de mon cœur jaloux. }
\livretDidasP\justify {
  La Furie sort des Enfers, elle poursuit Io, elle l’enleve,
  & Junon remonte dans le Ciel.
}
\livretPersDidas Io poursuivie par la Furie
\livretVerse#8 { O Dieux ! où me reduisez-vous ? }
\livretFinAct Fin du troisiéme Acte
\sep
\livretAct ACTE IV
\livretDescAtt\justify {
  Le Theatre change, & represente l’endroit le plus glacé de la Scythie.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center {
  Des Peuples paroissent transis de froid.
}
\livretPersDidas Chœur des Peuples des Climats glacez chantants
\livretRef#'EABchoeur
\livretVerse#6 { L’hyver qui nous tourmente }
\livretVerse#6 { S’obstine à nous geler, }
\livretVerse#6 { Nous ne sçaurions parler }
\livretVerse#7 { Qu’avec une voix tremblante. }
\livretVerse#6 { La neige & les glaçons }
\livretVerse#8 { Nous donnent de mortels frissons. }
\livretVerse#6 { Les Frimats se répandent }
\livretVerse#6 { Sur nos Corps languissants, }
\livretVerse#6 { Le froid transit nos sens }
\livretVerse#7 { Les plus durs Rochers se fendent. }
\livretVerse#6 { La neige & les glaçons }
\livretVerse#8 { Nous donnent de mortels frissons. }

\livretScene SCENE SECONDE
\livretDescAtt\wordwrap-center {
  Io, la Furie, les Peuples des climats glacez.
}
\livretPers Io
\livretRef#'EBArecit
\livretVerse#8 { Laissez-moy, Cruelle Furie, }
\livretVerse#12 { Cruelle, laissez-moy respirer un moment. }
\livretVerse#8 { Ah ! Barbare, plus je te prie, }
\livretVerse#12 { Et plus tu prens plaisir d’augmenter mon tourment. }
\livretPers La Furie
\livretVerse#8 { Soûpire, gémis, pleure, crie, }
\livretVerse#12 { Je me fais de ta peine un spectacle charmant. }
\livretPers Io
\livretVerse#8 { Laissez-moy, Cruelle Furie, }
\livretVerse#12 { Cruelle, laissez-moy respirer un moment. }
\livretVerse#12 { Quel horrible séjour ! Quel froid insuportable ! }
\livretVerse#12 { Tes Serpens animez par ta rage implacable }
\livretVerse#10 { Ne sont-ils par d’assez cruels Bourreaux ? }
\livretVerse#8 { Pour punir un Cœur miserable, }
\livretVerse#12 { Viens-tu chercher si loin des Supplices nouveaux ? }
\livretPers La Furie
\livretVerse#12 { Mal-heureux Habitans d’une Demeure affreuse, }
\livretVerse#12 { Connoissez de Junon le funeste couroux ; }
\livretVerse#8 { Par sa vengeance rigoureuse, }
\livretVerse#8 { Vous voyez une mal-heureuse }
\livretVerse#8 { Qui souffre cent fois plus que vous. }
\livretDidasP\wordwrap { Io, & la Furie repettent ces deux derniers Vers. }
\livretPers Chœur des Peuples des Climats glacez
\livretVerse#4 { Ah quelle peine }
\livretVerse#12 { De trembler, de languir dans l’horreur des Frimats ! }
\livretPers Io
\livretVerse#4 { Ah quelle peine }
\livretVerse#12 { D’éprouver tant de maux sans trouver le trépas ! }
\livretVerse#8 { Ah quelle vengeance inhumaine ! }
\livretPers La Furie
\livretVerse#12 { Vien changer de tourments, passe en d’autres Climats. }
\livretDidasP\wordwrap { La Furie entraisne & enleve Io. }
\livretPers Io
\livretVerse#4 { Ah quelle peine ! }
\livretPers Chœur des Peuples des Climats glacez
\livretVerse#4 { Ah quelle peine }
\livretVerse#12 { De trembler, de languir dans l’horreur des Frimats ! }

\livretScene SCENE TROISIESME
\livretDescAtt\justify {
  Le Theatre change, & represente des deux costez les Forges des Chalybes
  qui travaillent à forger l’Acier ; la Mer paroist dans l’enfoncement.
}
\livretDidas\justify {
  Huit Calybes dançants. Deux Conducteurs de Chalybes chantants.
  Chœur de Chalybes.
}
\livretDidas\justify {
  Dans le temps que plusieurs Chalybes travaillent dans les Forges,
  quelques autres vont & viennent avec empressement pour apporter
  l’Acier des Mines, & pour disposer ce qui est necessaire au travail
  qui se fait.
}
\livretPers Les deux Conducteurs, & le Chœur des Chalybes
\livretRef#'ECAchoeur
\livretVerse#8 { Que chacun avec soin s’empresse. }
\livretVerse#8 { Forgez, qu’on travaille sans cesse, }
\livretVerse#8 { Qu’on prépare tout ce qu’il faut. }
\livretVerse#8 { Que le feu des Forges s’allume, }
\livretVerse#8 { Travaillons d’un effort nouveau ; }
\livretVerse#8 { Qu’on fasse retentir l’Enclume }
\livretVerse#8 { Sous les coups pesants du marteau. }

\livretScene SCENE QUATRIESME
\livretDescAtt\wordwrap-center {
  Io, la Furie, les conducteurs de Chalybes.
  Troupe & chœur des Chalybes
}
\livretPersDidas Io \line { au milieu des Feux qui sortent des Forges }
\livretRef#'EDAchoeur
\livretVerse#12 { Quel déluge de feux vient sur moy se répandre ? }
\livretVerse#8 { O Ciel ! }
\livretDidasP\justify {
  Les Chalybes passent auprés d’Io avec des morceaux d’espées, de lances,
  & de haches à demy forgées.
}
\livretPers La Furie
\livretVerse#8 { \transparent { O Ciel ! } Le Ciel ne peut t’entendre, }
\livretVerse#8 { Tu ne te plains pas assez haut. }
\livretPers Les deux Conducteurs, & le Chœur des Chalybes
\livretVerse#8 { Qu’on prépare tout ce qu’il faut. }
\livretPers Io
\livretVerse#8 { Junon seroit moins inhumaine, }
\livretVerse#12 { Tu me fais trop souffrir, tu sers trop bien sa haine. }
\livretPers Le Furie
\livretVerse#8 { Au gré de son dépit jaloux, }
\livretVerse#12 { Tes maux les plus cruels seront encor trop doux. }
\livretPers Io
\livretVerse#8 { Helas, quelle rigueur extrême ! }
\livretVerse#8 { C’est en vain que Jupiter m’aime, }
\livretVerse#12 { La haine de Junon joüit de mon tourment ; }
\livretVerse#8 { Que vous haïssez fortement, }
\livretVerse#12 { Grands Dieux ! qu’il s’en faut bien que vous aimiez de mesme ! }
\livretDidasP\justify {
  Les Feux des Forges redoublent, & les Chalybes environnent Io avec des
  morceaux d’acier & brûlants.
}
\livretPers Io
\livretVerse#8 { Ne pourray-je cesser de vivre ? }
\livretVerse#8 { Cherchons le trépas dans les Flots. }
\livretPers La Furie
\livretVerse#8 { Par tout, ma rage te doit suivre, }
\livretVerse#8 { N’attens ny secours ny repos. }
\livretDidasP\justify {
  Io fuït, & monte au haut d’un Rocher, d’où elle se precipite dans la Mer,
  la Furie s’y jette aprés la Nymphe.
}

\livretScene SCENE CINQUIESME
\livretDescAtt\column {
  \wordwrap-center { Le Theatre change, & represente l’Antre des Parques. }
  \wordwrap-center {
    Suite des Parques. La Guerre. Les Fureurs de la Guerre.
    Les maladies violentes & languissantes.
    La Famine. L’Incendie. L’Inondation, &c. Chantants, Dançants.
  }
}
\livretPers Chœur de la Suite des Parques
\livretRef#'EEAchoeur
\livretVerse#8 { Executons l’Arrest du Sort, }
\livretVerse#8 { Suivons ses loix les plus cruelles ; }
\livretVerse#8 { Presentons sans cesse à la Mort }
\livretVerse#6 { Des Victimes nouvelles. }
\livretPers La Guerre
\livretVerse#12 { Que le Fer, }
\livretPers La Famine
\livretVerse#12 { \transparent { Que le Fer, } Que la Faim, }
\livretPers L'Incendie
\livretVerse#12 { \transparent { Que le Fer, Que la Faim, } Que les Feux, }
\livretPers L'Inondation
\livretVerse#12 { \transparent { Que le Fer, Que la Faim, Que les Feux, } Que les Eaux, }
\livretPers Toutes ensembles
\livretVerse#12 { Que tout serve à creuser mille & mille Tombeaux. }
\livretPers Les Maladies violents
\livretVerse#12 { Qu’on s’empresse d’entrer dans les Royaumes sombres }
\livretVerse#8 { Par mille chemins differents. }
\livretPers Les Maladies languissantes
\livretVerse#12 { Achevez d’expirer, infortunez Mourants, }
\livretVerse#12 { Cherchez un long repos dans le séjour des Ombres. }
\livretPers Le Chœur
\livretVerse#8 { Executons l’Arrest du Sort, }
\livretVerse#8 { Suivons ses loix les plus cruelles ; }
\livretVerse#8 { Presentons sans cesse à la Mort }
\livretVerse#6 { Des Victimes nouvelles. }
\livretPers La Guerre
\livretVerse#12 { Que le Fer, }
\livretPers La Famine
\livretVerse#12 { \transparent { Que le Fer, } Que la Faim, }
\livretPers L'Incendie
\livretVerse#12 { \transparent { Que le Fer, Que la Faim, } Que les Feux, }
\livretPers L'Inondation
\livretVerse#12 { \transparent { Que le Fer, Que la Faim, Que les Feux, } Que les Eaux, }
\livretPers Toutes ensembles
\livretVerse#12 { Que tout serve à creuser mille & mille Tombeaux. }
\livretDidasP\justify {
  La suite des Parques témoigne le plaisir qu’elle prend à terminer le
  sort des Humains.
}

\livretScene SCENE SIXIESME
\livretDescAtt\wordwrap-center {
  Io, la Furie, la Suite des Parques.
}
\livretPersDidas Io \line { parlant à la suite des Parques }
\livretRef#'EFCrecit
\livretVerse#8 { C’est contre moy qu’il faut tourner }
\livretVerse#8 { Vostre rigueur la plus funeste ; }
\livretVerse#12 { D’une vie odieuse arrachez-moy le reste, }
\livretVerse#8 { Hastez-vous de la terminer. }
\livretPers Le Chœur de la Suite des Parques
\livretVerse#8 { C’est aux Parques de l’ordonner. }
\livretPers Io
\livretVerse#12 { Favorisez mes vœux, Déesses Souveraines, }
\livretVerse#12 { Qui reglez du Destin les immuables loix ; }
\livretVerse#8 { Finissez mes jours & mes peines, }
\livretVerse#12 { Ne me condamnez pas à mourir mille fois. }
\livretDidasP\justify {
  Le fonds de l’Antre des Parques s’ouvre, & les trois Parques en sortent.
}

\livretScene SCENE SEPTIESME
\livretDescAtt\wordwrap-center {
   Les Trois Parques, Io, la Furie, suite des Parques.
}
\livretPers Les Trois Parques
\livretRef#'EGBchoeur
\livretVerse#5 { Le fil de la vie }
\livretVerse#5 { De tous les Humains, }
\livretVerse#5 { Suivant nostre envie, }
\livretVerse#5 { Tourne dans nos mains. }
\livretPers Io
\livretVerse#12 { Tranchez mon triste sort d’un coup qui me délivre }
\livretVerse#12 { Des tourments que Junon me contraint à souffrir ; }
\livretVerse#8 { Chacun vous fait de vœux pour vivre, }
\livretVerse#8 { Et je vous en fais pour mourir. }
\livretPers La Furie
\livretVerse#12 { Jupiter l’a soûmise aux loix de son Epouse ; }
\livretVerse#8 { Elle a rendu Junon jalouse ; }
\livretVerse#12 { L’amour d’un Dieu puissant a trop sçeu la charmer. }
\livretVerse#8 { Elle est trop peu punie encore. }
\livretPers Io
\livretVerse#8 { Est-ce un si grand crime d’aimer }
\livretVerse#8 { Ce que tout l’Univers adore ? }
\livretPers Les Parques
\livretVerse#12 { Nymphe appaise Junon, si tu veux voir la fin }
\livretVerse#6 { De ton sort déplorable ; }
\livretVerse#6 { C’est l’Arrest du Destin, }
\livretVerse#6 { Il est irrevocable. }
\livretPers Io
\livretVerse#12 { Helas, comment fléchir une haine implacable ? }
\livretPers Les Parques, la Furie, le Chœur de la suite des Parques
\livretVerse#6 { C’est l’Arrest du Destin, }
\livretVerse#6 { Il est irrevocable. }
\livretFinAct Fin du quiatriéme Acte
\sep
\livretAct ACTE V
\livretDescAtt\wordwrap-center {
  Le Theatre change, & represente les Rivages du Nil, & l’une des Embouchûres
  par où ce Fleuve entre dans la Mer.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center { Io, la Furie. }
\livretPersDidas Io \line { sortant de la Mer, d’où elle est tirée par la Furie }
\livretRef#'FABrecit
\livretVerse#12 { Terminez mes tourmens puissant Maistre du Monde ; }
\livretVerse#8 { Sans vous, sans vostre amour, helas ! }
\livretVerse#6 { Je ne souffrirois pas. }
\livretVerse#12 { Reduite au desespoir, mourante, vagabonde, }
\livretVerse#12 { J’ay porté mon supplice en mille affreux Climats ; }
\livretVerse#12 { Une horrible Furie attachée à mes pas, }
\livretVerse#12 { M’a suivie au travers du vaste sein de l’Onde. }
\livretVerse#12 { Terminez mes tourmens puissant Maistre du Monde, }
\livretVerse#8 { Voyez de quels maux icy bas, }
\livretVerse#12 { Vostre Epouse punit mes mal-heureux appas ; }
\livretVerse#10 { Délivrez-moy de ma douleur profonde, }
\livretVerse#12 { Ouvrez-moy par pitié les portes du Trépas. }
\livretVerse#12 { Terminez mes tourmens puissant Maistre du Monde, }
\livretVerse#8 { Sans vous, sans vostre amour, helas ! }
\livretVerse#6 { Je ne souffrirois pas. }
\livretVerse#12 { C’est Jupiter qui m’aime ! eh ! qui le pourroit croire ? }
\livretVerse#8 { Je ne suis plus dans sa memoire. }
\livretVerse#12 { Il n’entend point mes cris, il ne voit point mes pleurs, }
\livretVerse#12 { Aprés m’avoir livrée aux plus cruels mal-heurs, }
\livretVerse#10 { Il est tranquile au comble de sa Gloire, }
\livretVerse#10 { Il m’abandonne au milieu des douleurs. }
\livretVerse#13 { A la fin, je succombe, heureuse, si je meurs ! }
\livretDidasP\justify {
  Io tombe accablée de ses tourments, & Jupiter touché de pitié
  descend du Ciel.
}

\livretScene SCENE SECONDE
\livretDescAtt\wordwrap-center { Jupiter, Io, & la Furie. }
\livretPers Jupiter
\livretRef#'FBBrecit
\livretVerse#12 { Il ne m’est pas permis de finir vostre peine, }
\livretVerse#8 { Et ma puissance souveraine, }
\livretVerse#12 { Doit suivre du Destin l’irrevocable loy : }
\livretVerse#12 { C’est tout ce que je puis par un amour extréme, }
\livretVerse#12 { Que de quitter le Ciel & ma gloire suprême }
\livretVerse#12 { Pour prendre part aux maux que vous souffrez pour moy. }
\livretPers Io
\livretVerse#8 { Ah ! mon supplice augmente encore ! }
\livretVerse#12 { Tous le feu des Enfers me brusle, & me dévore ; }
\livretVerse#12 { Mourray-je tant de fois sans voir finir mon sort ? }
\livretPers Jupiter
\livretVerse#12 { Ma tendresse pour vous rend Junon inflexible. }
\livretVerse#12 { Elle voit mon amour, il luy paroit trop fort, }
\livretVerse#12 { Son couroux se redouble, & devient invincible. }
\livretPers Io
\livretVerse#12 { N’importe, en ma faveur, soyez toûjours sensible. }
\livretPers Jupiter
\livretVerse#12 { C’est trop vous exposer à son jaloux transport. }
\livretVerse#12 { J’irrite en vous aimant sa vengeance terrible. }
\livretPers Io
\livretVerse#8 { Aimez-moy, s’il vous est possible, }
\livretVerse#12 { Assez pour la forcer à me donner la mort. }
\livretDidasP\justify {
  Junon descend sur la Terre.
}

\livretScene SCENE TROISIÈME
\livretDescAtt\wordwrap-center { Jupiter, Junon, Io, la Furie. }
\livretPers Jupiter
\livretRef#'FCArecit
\livretVerse#8 { Venez Déesse impitoyable, }
\livretVerse#8 { Venez, voyez, reconnoissez }
\livretVerse#12 { Cette Nymphe mourante autrefois trop aimable. }
\livretVerse#12 { C’est assez la punir, c’est vous vanger assez, }
\livretVerse#12 { L’éclat de sa beauté ne la rend plus coupable ; }
\livretVerse#12 { Par la cruelle horreur du tourment qui l’accable, }
\livretVerse#12 { Son crime & ses appas sont ensemble effacez. }
\livretVerse#8 { Sans jalousie, & sans allarmes, }
\livretVerse#8 { Voyez ses yeux noyez de larmes }
\livretVerse#12 { Que l’ombre de la mort commence de couvrir. }
\livretPers Junon
\livretVerse#8 { Ils n’ont encor que trop de charmes }
\livretVerse#8 { Puis qu’ils sçavent vous attendrir. }
\livretPers Jupiter
\livretVerse#12 { Une juste pitié peut-elle vous aigrir ? }
\livretVerse#12 { Vostre couroux fatal ne doit-il point s’éteindre ? }
\livretPers Junon
\livretVerse#12 { Ah ! vous la plaignez trop, elle n’est pas à plaindre. }
\livretVerse#8 { Non, elle ne peut trop souffrir. }
\livretPers Jupiter
\livretVerse#12 { Je sçay que c’est de vous que son sort doit dépendre. }
\livretVerse#12 { Ce n’est qu’à vos bontez qu’elle doit recourir. }
\livretVerse#12 { Il n’est rien que de moy vous ne deviez attendre, }
\livretVerse#12 { Si je puis obliger vostre haine à se rendre. }
\livretPers Io
\livretVerse#6 { Ah ! laissez-moy mourir. }
\livretPers Jupiter
\livretVerse#8 { Prenez soin de la secourir. }
\livretPers Junon
\livretVerse#8 { Vous l’aimez d’un amour trop tendre, }
\livretVerse#8 { Non, elle ne peut trop souffrir. }
\livretPers Jupiter
\livretVerse#12 { Quoy le Cœur de Junon, quelque grand qu’il puisse estre, }
\livretVerse#12 { Ne sçauroit triompher d’une injuste fureur ? }
\livretPers Junon
\livretVerse#12 { De la Terre & du Ciel Jupiter est le Maistre, }
\livretVerse#12 { Et Jupiter n’est pas le Maistre de son Cœur ? }
\livretPers Jupiter
\livretVerse#8 { Hé bien, il faut que je commence }
\livretVerse#6 { A me vaincre en ce jour. }
\livretPers Junon
\livretVerse#10 { Vous m’apprendrez à me vaincre à mon tour. }
\livretPersDidas Jupiter & Junon ensemble
\livretPersVerse Junon \line {
  \hspace#7 \column {
    \livretVerse#8 { J’abandonneray ma vengeance, }
    \livretVerse#6 { Rendez-moy vostre amour. }
  }
}
\livretPersVerse Jupiter \line {
  \hspace#7 \column {
    \livretVerse#8 { Abandonnez vostre vengeance, }
    \livretVerse#6 { Je vous rends mon amour. }
  }
}
\livretPers Jupiter
\livretVerse#12 { Noires Ondes du Stix, c’est par vous que je jure, }
\livretVerse#12 { Fleuve affreux, écoutez le serment que je fais. }
\livretVerse#12 { Si cette Nymphe, enfin, reprend tous ses attraits, }
\livretVerse#12 { Si Junon fait cesser les tourments qu’elle endure, }
\livretVerse#12 { Je jure que ses yeux ne troubleront jamais }
\livretVerse#12 { De nos Cœurs réunis la bien-heureuse paix. }
\livretVerse#12 { Noires Ondres du Stix, c’est par vous que je jure, }
\livretVerse#12 { Fleuve affreux, écoutez le serment que je fais. }
\livretPers Junon
\livretVerse#12 { Nymphe, je veux finir vostre peine éternelle, }
\livretVerse#12 { Que la Furie emporte aux Enfers avec elle }
\livretVerse#12 { Le trouble & les horreurs dont vos sens sont saisis. }
\livretDidasP\justify {
  La Furie s’enfonce dans les Enfers, & Io se trouve délivrée de ses peine.
}
\livretVerse#8 { Aprés un rigoureux supplice, }
\livretVerse#12 { Goûtez les biens parfaits que les Dieux ont choisis : }
\livretVerse#8 { Et sous le nouveau nom d’Isis, }
\livretVerse#12 { Joüissez d’un bon-heur qui jamais ne finisse. }
\livretPers Jupiter & Junon
\livretVerse#12 { Dieux, recevez Isis au rang des Immortels. }
\livretVerse#12 { Peuples voisins du Nil, dressez-luy des Autels. }
\livretDidasP\justify {
  Les Divinitez du Ciel descendent pour recevoir Isis, les Peuples d’Egypte
  luy dessent un Autel, & la reconnoissent pour la Divinité qui les doit proteger.
}
\column-break
\livretDidas\wordwrap-center {
  Divinitez qui descendent du Ciel dans la Gloire,
  Peuples d’Egypte chantants. Quatre Egyptiennes chantantes.
  Peuples d’Egypte dançants. Quatre Egyptiennes dançantes.
}
\livretPers Chœur des Divinitez
\livretRef#'FCBchoeur
\livretVerse#8 { Venez, Divinité nouvelle. }
\livretPers Chœur des Peuples d'Egypte
\livretVerse#8 { Isis, tournez sur nous vos yeux, }
\livretVerse#8 { Voyez l’ardeur de nostre zele. }
\livretPers Chœur des Divinitez
\livretVerse#8 { La Celeste Cour vous appelle. }
\livretPers Chœur des Peuples d'Egypte
\livretVerse#8 { Tout vous revere dans ces lieux. }
\livretDidasP\justify {
  Jupiter & Junon prennent place au milieu des Divinitez, & y font placer Isis.
}
\livretPers Jupiter & Junon
\livretVerse#6 { Isis est immortelle, }
\livretVerse#8 { Isis va briller dans ces lieux. }
\livretVerse#8 { Isis joüit avec les Dieux, }
\livretVerse#6 { D’une Gloire éternelle. }
\livretDidasP\justify {
  Jupiter & Junon, & les Divinitez remontent au Ciel, & y conduisent
  Isis dans le temps que les Chœurs des Divinitez, & des Peuples d’Egypte
  repetent ces quatre derniers Vers.
}
\livretFinAct Fin du cinquiéme & dernier Acte
}
