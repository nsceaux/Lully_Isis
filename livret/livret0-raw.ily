\livretAct PROLOGUE
\livretDescAtt\justify {
  Le Theatre represente le Palais de la Renommée.  Il est ouvert de
  tous costez pour recevoir les nouvelles de ce qui se fait de
  conséderable sur la Terre, & de ce qui se passe de memorable sur la
  Mer, que l’on découvre dans l’enfoncement.  La Divinité qui préside
  dans ce Palais y paroist accompagnée de sa Suite ordinaire : Les
  Rumeurs & les Bruits qui portent comme elle chacun une Trompette à
  la main, y viennent en foule de divers endroits du monde.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center {
  La Renommée, Suite de la Renommée, les Rumeurs, et les Bruits.
}
\livretPers\wordwrap\smallCaps {
  Chœur de la Suite de la Renommée, des Rumeurs, & des Bruits.
}
\livretRef#'AABchoeur
%# Publi=ons en tous lieux
%# Du plus grand des Heros la valeur tri=omphante,
%# Que la Terre, & les Cieux
%# Retentissent du bruit de sa gloire éclatante.
\livretPers La Renommée
\livretRef#'AACchoeur
%# C'est luy dont les Dieux ont fait choix
%# Pour combler le bon-heur de l'Empire François;
%# En vain, pour le troubler, tout s'unit, tout conspire,
%# C'est en vain que l'Envie =a ligué tant de Rois,
%# Heureux l'Empire
%# Qui suit ses Loix!
\livretPers Le Chœur
%# Heureux l'Empire
%# Qui suit ses Loix!
\livretPers La Renommée
%# Il faut que par tout on l'admire,
%# Parlons de ses Vertus, racontons ses Exploits,
%# A peine y pourrons-nous suffire
%# Avec toutes nos voix.
\livretPers La Renommée et le chœur
%# Heureux l'Empire
%# Qui suit ses Loix!
%# Il faut le dire
%# Cent & cent fois.
%# Heureux l'Empire
%# Qui suit ses Loix!

\livretScene SCENE SECONDE
\livretDescAtt\wordwrap-center {  
  Deux Tritons chantants, Trouppe de Dieux Marins joüant des
  instruments et daçants.  Neptune, la Renommée, Chœur de la Suite de
  la Renommée.
}
\livretPersDidas Deux Tritons chantants
\livretRef#'ABBtritons
%# C'est le Dieu des Eaux qui va paraistre,
%# Rangeons-nous prés de nostre Maistre:
%# Enchaînons les Vents
%# Les plus terribles,
%# Que le bruit des Flots céde à nos Chants;
%# Regnez, Zephirs paisibles,
%# Ramenez le doux Printemps.
%# Fuy=ez loin d'icy, cru=els orages,
%# Rien ne doit troubler ces Rivages.
%# Enchaînons les Vents
%#4 Les plus terribles, &c.
\livretPersDidas Neptune parlant à la Renommée
\livretRef#'ABDrecit
%# Mon Empire a servy de The=atre à la Guerre;
%# Publi=ez des Exploits nouveaux:
%# C'est le mesme Vainqueur si fameux sur la Terre
%# Qui tri=omphe encor sur les Eaux.
\livretPers Neptune et la Renommée
\livretVerse#12 { \raise#0.7 { \left-brace#20 \raise#1 \column { Celebrez Celebrons } \right-brace#20 } son grand Nom sur la Terre & sur l’Onde. }
%# Qu'il ne soit pas borné par les plus vastes Mers:
%# Qu'il vole jusqu'au bout du Monde,
%# Qu'il dure autant que l'Univers.
\livretDidasP\wordwrap {
  Le Chœur repete ces quatre derniers Vers.
}
\livretRef#'ABEchoeur
%#12 Celebrons son grand Nom sur la Terre & sur l'Onde, &c.

\livretScene SCENE TROISIESME
\livretDescAtt\wordwrap-center {
  Les neuf Muses, les Arts Libéraux, Apollon, Neptune,
  Suite de Neptune, la Renommée, Suite de la Renommée
}
\livretPers Calliope
\livretRef#'ACBrecit
%# Cessez pour quelque temps, bruit terrible des Armes,
%# Qui troublez le repos de cent Climats divers;
\livretPers Calliope, Clio, Melpomene, Thalie, & Vranie
%# Ne troublez pas les charmes
%# De nos divins Concerts.
\livretDidasP\justify {
  Erato, Euterpe, Terpsichore, & Polymnie, forment un Concert d’Instruments.
}
\livretPers Melpomene
%# Recommençons nos Chants, allons les faire entendre
%# Dans une Auguste Cour.
\livretPers Thalie et Calliope
%# La Paix, la douce Paix n'ose encore descendre
%# Du celeste Séjour;
\livretPers Calliope, Clio, Melpomene, Thalie, & Vranie
%# Prés du Vainqueur, allons attendre
%# Son bien-heureux retour.
\livretRef#'ACCair
\livretDidasPPage\justify {
  Les Arts accompagnent Apollon, & se réjoüissent du bon-heur que ce Dieu
  qui les conduit, leur fait esperer.
}
\livretPersDidas Apollon parlant à la Renommée
\livretRef#'ACErecit
%# Ne parlez pas toujours de la Guerre cru=elle,
%# Parlons des Plaisirs, & des Jeux.
%# Les Muses, & les Arts vont signaler leur zele,
%# Je vais favoriser leurs vœux;
%# Nous preparons une Feste nouvelle,
%# Pour le Heros qui les appelle,
%# Dans cet Azile *heureux.
%# Ne parlez pas toujours de la Guerre cru=elle,
%# Parlons des Plaisirs, & des Jeux.
\livretPers La Renommée, Neptune, Apollon, les Muses, & Chœur
\livretRef#'ACFchoeur
%# Ne parlons pas toujours de la Guerre cru=elle,
%# Parlons des Plaisirs, & des Jeux.
\livretPers\wordwrap\smallCaps {
  La Renommée, Neptune, Apollon, les Muses, les Tritons,
  & le Chœur de la Suite de la Renommée
}
\livretRef#'ACHchoeur
%# Hastez-vous, Plaisirs, hastez-vous,
%# Hastez-vous de montrer vos charmes les plus doux.
\livretPers La Renommée
%# Il n'est pas encor temps de croire
%# Que les paisibles Jeux ne seront plus troublez;
%# Rien ne plaist au Heros qui les a rassemblez
%# A l'égal des Exploits d'éternelle mémoire.
%# Ennemis de la Paix, tremblez;
%# Vous le verrez bien-tost courir à la Victoire,
%# Vos efforts redoublez
%# Ne serviront qu'à redoubler sa gloire.
\livretPers\wordwrap\smallCaps {
  La Renommée, Neptune, Apollon, les Muses, les Tritons,
  & le Chœur de la Suite de la Renommée
}
%# Hastez-vous, Plaisirs, hastez-vous,
%# Hastez-vous de montrer vos charmes les plus doux.
\livretDidasP\justify {
  Dans le temps que le Chœur chante, & que les Instruments joüent,
  la Suite de Neptune dance avec celle d’Apollon, & toutes ces Divinitez
  vont ensemble prendre part à la nouvelle Feste que le Dieux du Parnasse
  a préparée avec les Muses, & les arts.
}
\livretFinAct Fin du Prologue
\sep
