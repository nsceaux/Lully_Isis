\livretAct ACTE IV
\livretDescAtt\justify {
  Le Theatre change, & represente l’endroit le plus glacé de la Scythie.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center {
  Des Peuples paroissent transis de froid.
}
\livretPersDidas Chœur des Peuples des Climats glacez chantants
\livretRef#'EABchoeur
%# L'*hyver qui nous tourmente
%# S'obstine à nous geler,
%# Nous ne sçaurions parler
%# Qu'avec une voix tremblante.
%# La neige & les glaçons
%# Nous donnent de mortels frissons.
%#6 Les Frimats se répandent
%# Sur nos Corps languissants,
%# Le froid transit nos sens
%#7 Les plus durs Rochers se fendent.
%# La neige & les glaçons
%# Nous donnent de mortels frissons.

\livretScene SCENE SECONDE
\livretDescAtt\wordwrap-center {
  Io, la Furie, les Peuples des climats glacez.
}
\livretPers Io
\livretRef#'EBArecit
%# Laissez-moy, Cru=elle Furie,
%# Cru=elle, laissez-moy respirer un moment.
%# Ah! Barbare, plus je te prie,
%# Et plus tu prens plaisir d'augmenter mon tourment.
\livretPers La Furie
%# Soûpire, gémis, pleure, crie,
%# Je me fais de ta peine un spectacle charmant.
\livretPers Io
%# Laissez-moy, Cru=elle Furie,
%# Cru=elle, laissez-moy respirer un moment.
%# Quel *horrible séjour! Quel froid insuportable!
%# Tes Serpens animez par ta rage implacable
%# Ne sont-ils par d'assez cru=els Bourreaux?
%# Pour punir un Cœur miserable,
%# Viens-tu chercher si loin des Supplices nouveaux?
\livretPers La Furie
%# Mal-heureux Habitans d'une Demeure affreuse,
%# Connoissez de Junon le funeste couroux;
%# Par sa vengeance rigoureuse,
%# Vous voy=ez une mal-heureuse
%# Qui souffre cent fois plus que vous.
\livretDidasP\wordwrap { Io, & la Furie repettent ces deux derniers Vers. }
\livretPers Chœur des Peuples des Climats glacez
%# Ah quelle peine
%# De trembler, de languir dans l'*horreur des Frimats!
\livretPers Io
%# Ah quelle peine
%# D'éprouver tant de maux sans trouver le trépas!
%# Ah quelle vengeance inhumaine!
\livretPers La Furie
%# Vien changer de tourments, passe en d'autres Climats.
\livretDidasP\wordwrap { La Furie entraisne & enleve Io. }
\livretPers Io
%# Ah quelle peine!
\livretPers Chœur des Peuples des Climats glacez
%# Ah quelle peine
%# De trembler, de languir dans l'*horreur des Frimats!

\livretScene SCENE TROISIESME
\livretDescAtt\justify {
  Le Theatre change, & represente des deux costez les Forges des Chalybes
  qui travaillent à forger l’Acier ; la Mer paroist dans l’enfoncement.
}
\livretDidas\justify {
  Huit Calybes dançants. Deux Conducteurs de Chalybes chantants.
  Chœur de Chalybes.
}
\livretDidas\justify {
  Dans le temps que plusieurs Chalybes travaillent dans les Forges,
  quelques autres vont & viennent avec empressement pour apporter
  l’Acier des Mines, & pour disposer ce qui est necessaire au travail
  qui se fait.
}
\livretPers Les deux Conducteurs, & le Chœur des Chalybes
\livretRef#'ECAchoeur
%# Que chacun avec soin s'empresse.
%# Forgez, qu’on travaille sans cesse,
%# Qu'on prépare tout ce qu’il faut.
%# Que le feu des Forges s'allume,
%# Travaillons d'un effort nouveau;
%# Qu'on fasse retentir l'Enclume
%# Sous les coups pesants du marteau.

\livretScene SCENE QUATRIESME
\livretDescAtt\wordwrap-center {
  Io, la Furie, les conducteurs de Chalybes.
  Troupe & chœur des Chalybes
}
\livretPersDidas Io \line { au milieu des Feux qui sortent des Forges }
\livretRef#'EDAchoeur
%# Quel déluge de feux vient sur moy se répandre?
%#- O Ciel!
\livretDidasP\justify {
  Les Chalybes passent auprés d’Io avec des morceaux d’espées, de lances,
  & de haches à demy forgées.
}
\livretPers La Furie
%#= Le Ciel ne peut t'entendre,
%# Tu ne te plains pas assez haut.
\livretPers Les deux Conducteurs, & le Chœur des Chalybes
%# Qu'on prépare tout ce qu’il faut.
\livretPers Io
%# Junon seroit moins inhumaine,
%# Tu me fais trop souffrir, tu sers trop bien sa haine.
\livretPers Le Furie
%# Au gré de son dépit jaloux,
%# Tes maux les plus cru=els seront encor trop doux.
\livretPers Io
%# Helas, quelle rigueur extrême!
%# C'est en vain que Jupiter m'aime,
%# La haine de Junon joü=it de mon tourment;
%# Que vous ha=ïssez fortement,
%# Grands Dieux! qu'il s'en faut bien que vous aimiez de mesme!
\livretDidasP\justify {
  Les Feux des Forges redoublent, & les Chalybes environnent Io avec des
  morceaux d’acier & brûlants.
}
\livretPers Io
%# Ne pourray-je cesser de vivre?
%# Cherchons le trépas dans les Flots.
\livretPers La Furie
%# Par tout, ma rage te doit suivre,
%# N'attens ny secours ny repos.
\livretDidasP\justify {
  Io fuït, & monte au haut d’un Rocher, d’où elle se precipite dans la Mer,
  la Furie s’y jette aprés la Nymphe.
}

\livretScene SCENE CINQUIESME
\livretDescAtt\column {
  \wordwrap-center { Le Theatre change, & represente l’Antre des Parques. }
  \wordwrap-center {
    Suite des Parques. La Guerre. Les Fureurs de la Guerre.
    Les maladies violentes & languissantes.
    La Famine. L’Incendie. L’Inondation, &c. Chantants, Dançants.
  }
}
\livretPers Chœur de la Suite des Parques
\livretRef#'EEAchoeur
%# Executons l'Arrest du Sort,
%# Suivons ses loix les plus cru=elles;
%# Presentons sans cesse à la Mort
%# Des Victimes nouvelles.
\livretPers La Guerre
%#- Que le Fer,
\livretPers La Famine
%#- Que la Faim,
\livretPers L'Incendie
%#- Que les Feux,
\livretPers L'Inondation
%#= Que les Eaux,
\livretPers Toutes ensembles
%# Que tout serve à creuser mille & mille Tombeaux.
\livretPers Les Maladies violents
%# Qu'on s'empresse d'entrer dans les Roy=aumes sombres
%# Par mille chemins differents.
\livretPers Les Maladies languissantes
%# Achevez d'expirer, infortunez Mourants,
%# Cherchez un long repos dans le séjour des Ombres.
\livretPers Le Chœur
%# Executons l'Arrest du Sort,
%# Suivons ses loix les plus cru=elles;
%# Presentons sans cesse à la Mort
%# Des Victimes nouvelles.
\livretPers La Guerre
%#- Que le Fer,
\livretPers La Famine
%#- Que la Faim,
\livretPers L'Incendie
%#- Que les Feux,
\livretPers L'Inondation
%#= Que les Eaux,
\livretPers Toutes ensembles
%# Que tout serve à creuser mille & mille Tombeaux.
\livretDidasP\justify {
  La suite des Parques témoigne le plaisir qu’elle prend à terminer le
  sort des Humains.
}

\livretScene SCENE SIXIESME
\livretDescAtt\wordwrap-center {
  Io, la Furie, la Suite des Parques.
}
\livretPersDidas Io \line { parlant à la suite des Parques }
\livretRef#'EFCrecit
%# C'est contre moy qu'il faut tourner
%# Vostre rigueur la plus funeste;
%# D'une vie =odi=euse arrachez-moy le reste,
%# Hastez-vous de la terminer.
\livretPers Le Chœur de la Suite des Parques
%# C'est aux Parques de l'ordonner.
\livretPers Io
%# Favorisez mes vœux, Dé=esses Souveraines,
%# Qui reglez du Destin les immu=ables loix;
%# Finissez mes jours & mes peines,
%# Ne me condamnez pas à mourir mille fois.
\livretDidasP\justify {
  Le fonds de l’Antre des Parques s’ouvre, & les trois Parques en sortent.
}

\livretScene SCENE SEPTIESME
\livretDescAtt\wordwrap-center {
   Les Trois Parques, Io, la Furie, suite des Parques.
}
\livretPers Les Trois Parques
\livretRef#'EGBchoeur
%# Le fil de la vie
%# De tous les Humains,
%# Suivant nostre envie,
%# Tourne dans nos mains.
\livretPers Io
%# Tranchez mon triste sort d'un coup qui me délivre
%# Des tourments que Junon me contraint à souffrir;
%# Chacun vous fait de vœux pour vivre,
%# Et je vous en fais pour mourir.
\livretPers La Furie
%# Jupiter l'a soûmise aux loix de son Epouse;
%# Elle a rendu Junon jalouse;
%# L'amour d'un Dieu puissant a trop sçeu la charmer.
%# Elle est trop peu punie =encore.
\livretPers Io
%# Est-ce un si grand crime d'aimer
%# Ce que tout l'Univers adore?
\livretPers Les Parques
%# Nymphe appaise Junon, si tu veux voir la fin
%# De ton sort déplorable;
%# C'est l'Arrest du Destin,
%# Il est irrevocable.
\livretPers Io
%# Helas, comment fléchir une haine implacable?
\livretPers Les Parques, la Furie, le Chœur de la suite des Parques
%# C'est l'Arrest du Destin,
%# Il est irrevocable.
\livretFinAct Fin du quiatriéme Acte
\sep
