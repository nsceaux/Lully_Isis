\livretAct ACTE III
\livretDescAtt\justify {
  Le Theatre change, & represente la Solitude où Argus fait sa demeure prés
  d’un Lac, & au milieu d’une Forests.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center { Argus, Io. }
\livretPers Argus
\livretRef#'DABrecit
%# Dans ce Solitaire Séjour
%# Vous estes sous ma garde, & Junon vous y laisse:
%# Mes yeux veilleront tour à tour,
%# Et vous observeront sans cesse.
\livretPers Io
%# Est-ce là le bon-heur que Junon m'a promis?
%# Argus apprenez-moy quel crime j'ay commis.
\livretPers Argus
%# Vous estes aimable,
%# Vos yeux devoient moins charmer;
%# Vous estes coupable
%# De vous faire trop aimer.
\livretPers Io
%# Ne me déguisez rien, de quoy m'accuse-t’elle?
%# Quelle offense à ses yeux me rend si criminelle?
%# Ne pouray-je appaiser son funeste couroux?
\livretPers Argus
%# C'est une offense cru=elle
%# De paroistre belle
%# A des yeux jaloux.
%# L'Amour de Jupiter a trop paru pour vous.
\livretPers Io
%# Je suis perdu=ë, ô Ciel! si Junon est jalouse.
\livretPers Argus
%# On ne plaist guere à l'Epouse,
%# Lors qu'on plaist tant à l'Epoux.
%# Vous n'en serez pas mieux d'estre ingrate & volage.
%# Vous quittez un fidelle Amant
%# Pour recevoir un plus brillant hommage;
%# Mais c'est un avantage
%# Que vous pay=erez cherement.
%# Vous n'en serez pas mieux d'estre ingrate & volage.
%# J'ay l'ordre d'enfermer vos dangereux appas,
%# La Dé=esse défend que vous voy=ez personne.
\livretPers Io
%# Aux rigueurs de Junon Jupiter m'abandonne;
%# Non, Jupiter ne m'aime pas.
\livretDidasPPage\wordwrap {
  Argus enferme Io.
}

\livretScene SCENE SECONDE
\livretDescAtt\wordwrap-center { Hierax, Argus. }
\livretPersDidas Hierax voyant Io qui entre dans la Demeure d’Argus
\livretRef#'DBArecit
%# La Perfide craint ma presence,
%# Elle me fuit en vain, & j'iray la chercher…
\livretPersDidas Argus arrestant Hierax
%#- Non.
\livretPers Hierax
%#= Laissez-moy luy reprocher
%# Sa cru=elle inconstance.
\livretPers Argus
%# Non, on ne la doit point voir.
\livretPers Hierax
%# Quoy, Junon me devient contraire?
\livretPers Argus
%# L'ordre est exprés pour tous, perdez un vain espoir.
\livretPers Hierax
%# L'amitié fraternelle a si peu de pouvoir.
\livretPers Argus
%# Non, je ne connois plus ny d'Amy, ny de Frere,
%# Je ne connois que mon devoir.
%# Laissez la Nymphe en paix, ce n'est plus vous qu'elle aime.
\livretPers Hierax
%# Quel est l'*heureux Amant qui s'en est fait aimer.
%#- Nommez-le-moy.
\livretPers Argus
%#= Tremblez à l'entendre nommer,
%# C'est un Dieu tout-puissant, c'est Jupiter luy-mesme.
\livretPers Hierax
%#- O Dieux!
\livretPers Argus
%#= Dégagez-vous d'un amour si fatal,
%# Sans balancer, il faut vous y resoudre,
%# C'est un redoutable Rival
%# Qu'un Amant qui lance la foudre.
\livretPers Hierax
%# Dieux tout-puissants! ah, vous estiez jaloux
%# De la felicité que vous m'avez ravie,
%# Dieux tout-puissants! ah, vous estiez jaloux
%# De me voir plus heureux que vous.
%# Vous n'avez pû souffrir le bon-heur de ma vie,
%# Et je voy=ois vos grandeurs sans envie,
%# J'aimois, j'estois aimé, mon sort estoit trop doux;
%# Dieux tout-puissants! ah, vous estiez jaloux
%# De la felicité que vous m'avez ravie,
%# Dieux tout-puissants! ah, vous estiez jaloux
%# De me voir plus heureux que vous.
\livretPers Argus
%# Heureux qui peut briser sa chaîne!
%# Finissez une plainte vaine,
%# Meprisez l'infidelité,
%# Un Cœur ingrat vaut-il la peine
%# D'estre tant regretté.
%# Heureux qui peut briser sa chaîne.
\livretPers Hierax & Argus
%# Heureux qui peut briser sa chaîne.
\livretPers Argus
%# Liberté, liberté.

\livretScene SCENE TROISIESME
\livretDescAtt\wordwrap-center {
  Argus, Hierax, une Nymphe qui represente Syrinx.
  Troupe de Nymphes en habit de chasse.
}
\livretDidas\justify {
  La Nypmhe Syrinx. Huit Nymphes Compagnes de Syrinx chantantes.
  Quatre autres Nymphes chantantes. Six Nymphes Compagnes de Syrinx
  dançantes.
}
\livretPers Syrinx, Chœur de Nymphes
\livretRef#'DCAchoeur
%# Liberté, liberté.
\livretDidas\justify {
  Une partie des Nymphes dancent dans le temps que les autres chantent.
}
\livretPers Argus, & Hierax
%# Quelles Dances, quels chants, & quelle nouveauté.
\livretPers Syrinx & les Nymphes
%# S'il est quelque bien au monde,
%# C'est la liberté.
\livretPers Argus, & Hierax
%# Que voulez-vous?
\livretPers Chœur de Nymphes
%# Liberté, liberté.
\livretPers Argus, & Hierax
%# Que voulez-vous? il faut qu'on nous réponde.
\livretPers Syrinx & les Nymphes
%# S'il est quelque bien au monde,
%# C'est la liberté.

\livretScene SCENE IV
\livretDescAtt\wordwrap-center {
  Argus, Hierax, Syrinx, Troupe de Nymphes, Mercure deguisé en Berger,
  Troupe de Bergers, Toupe de Satyres, Troupe de Sylvains.
}
\livretPers Mercure, Chœur des Nymphes, de Bergers, & de Sylvains
\livretRef#'DDAchoeur
%# Liberté, liberté.
\livretPersDidas Mercure déguisé en Berger parlant à Argus
\livretRef#'DDBrecit
%# De la Nymphe Syrinx Pan cherit la memoire,
%# Il en regrette encor la perte chaque jour,
%# Pour celebrer une feste à sa gloire,
%# Ce Dieu luy-mesme assemble icy sa Cour:
%# Il veut que du mal-heur de son fidelle Amour
%# Un spectacle touchant represente l'*histoire.
\livretPers Argus
%# C'est un plaisir pour nous; poursuivez, j'y consens,
%# Je ne m'oppose point à des Jeux innocens.
\livretDidasP\justify {
  Argus va prendre place sur un siege de gazon proche de l’endroit
  où Io est enfermée, & fait placer Hierax de l’autre costé.
}
\livretPersDidas Mercure parlant à part à toute la Troupe qu’il conduit
%# Il donne dans le piege; achevez sans remise,
%# Achevez de surprendre Argus, & tous ses yeux:
%# Si vous tentez une grande entreprise,
%# Mercure vous conduit, l'Amour vous favorise,
%# Et vous servez le plus puissant des Dieux.
\livretDidasP\justify {
  Mercure, les Bergers, les Satyres, & les Sylvains r’entrent derriere le Theatre.
}

\livretScene SCENE CINQUIESME
\livretDescAtt\wordwrap-center {
  Argus, Hierax, Syrinx, Troupe de Nymphes.
}
\livretPers Syrinx, & le Chœur des Nymphes
\livretRef#'DEAchoeur
%# Liberté, liberté.
%# S'il est quelque bien au monde,
%# C'est la liberté.
%# Liberté, liberté.
\livretPers Syrinx
%# L'Empire de l'Amour n'est pas moins agité
%# Que l'Empire de l'Onde;
%# Ne cherchons point d'autre felicité
%# Qu'un doux loisir dans une paix profonde.
\livretPers Syrinx, & le Chœur
%# S'il est quelque bien au monde,
%# C'est la liberté.
%# Liberté, liberté.
\livretDidasP\justify {
  Dans le temps qu’une partie des Nymphes chante, le reste de la Troupe dance.
}

\livretScene SCENE SIXIESME
\livretDescAtt\wordwrap-center {
  Un des Sylvains representant le Dieu Pan.
  Troupe de Bergers, Troupe de Satyrs, Troupe de Sylvains,
  Syrinx, Troupe de Nymphes. Argus & Hierax.
}
\livretDidasP\justify {
  Des Bergers & des Sylvains dançants & chantants viennent offrir des
  Presens de fruits & de Fleurs à la Nymphe Syrinx, & tâchent de luy
  persuader de n’aller point à la Chasse, & de s’engager sous les loix
  de l’Amour.
}
\livretDidasP\justify {
  Douze Satyres chantants, & portans des Presents à Syrinx.
  Quatre Satyrs joüants de la Flûte.
  Douze Bergers portants des Presents à Syrinx.
  Quatre Bergers joüants de la Flûte.
  Quatre Sylvains dançants.
  Quatre Bergers heroïques dançants.
  Deux Bergers chantants.
}
\livretPers [Deux Bergers]
\livretRef#'DFBbergers
%# Quel bien devez-vous attendre,
%# Beauté qui chassez dans ces Bois?
%# Que pouvez-vous prendre
%# Qui vaille un Cœur tendre
%# Soûmis à vos Loix?
%# Ce n'est qu'en aimant
%# Qu'on trouve un sort charmant,
%# Aimez, enfin, à vostre tour,
%# Il faut que tout cede à l'Amour:
%# Il sçait fraper d'un coup certain
%# Le Cerf leger qui fuit en vain;
%# Jusques dans les Antres secrets,
%# Au fond des Forests,
%# Tout doit sentir ses traits.
\null
%# Lors que l'Amour vous appelle,
%# Pourquoy fuy=ez-vous ses plaisirs?
%# La Roze nouvelle
%# N'en est que plus belle
%# D'aimer les Zephirs.
%# Ce n'est qu'en aimant
%# Qu'on trouve un sort charmant,
%# Aimez, enfin, à vostre tour,
%# Il faut que tout cede à l'Amour:
%# Il sçait fraper d'un coup certain
%# Le Cerf leger qui fuit en vain;
%# Jusques dans les Antres secrets,
%# Au fond des Forests,
%# Tout doit sentir ses traits.
\livretPers Pan
\livretRef#'DFDrecit
%# Je vous aime, Nymphe charmante,
%# Un Amant immortel cherche à plaire à vos yeux.
\livretPers Syrinx
%# Pan est un Dieu puissant, je revere les Dieux,
%# Mais le nom d'Amant m'épouvante.
\livretPers Pan
%# Pour vous faire trouver le nom d'Amant plus doux,
%# J'y joindray le titre d'Epoux.
%# Je n'auray pas de peine
%# A m'engager
%# Dans une aimable chaîne,
%# Je n'auray pas de peine
%# A m'engager
%# Pour ne jamais changer.
%# Aimez un Dieu qui vous adore,
%# Unissons-nous d'un nœud charmant.
\livretPers Syrinx
%# Un Epoux doit estre encore
%# Plus à craindre qu'un Amant.
\livretPers Pan
%# Dissipez de vaines allarmes,
%# Eprouvez l'Amour & ses charmes,
%# Connoissez ses plus doux appas:
%# Non, ce ne peut estre
%# Que faute de le connoistre
%# Qu'il ne vous plaist pas.
\livretPers Syrinx
%# Les maux d'autruy me rendront sage.
%# Ah! quel mal-heur
%# De laisser engager son cœur!
%# Pourquoy faut-il passer le plus beau de son âge
%# Dans une mortelle langueur?
%# Ah, quel mal-heur!
%# Pourquoy n'avoir pas le courage
%# De s'affranchir de la rigueur
%# D'un funeste esclavage?
%# Ah! quel mal-heur
%# De laisser engager son cœur!
\livretPers Pan
%# Ah, quel dommage
%# Que vous ne sçachiez pas aimer!
%# Que vous sert-il d'avoir tant d'attraits en partage,
%# Si vous en negligez le plus grand avantage?
%# Que vous sert-il de sçavoir tout charmer?
%# Ah, quel dommage
%# Que vous ne sçachiez pas aimer!
\livretPers Chœur de Sylvains, de Satyres, & de Bergers
\livretRef#'DFEchoeur
%# Aymons sans cesse.
\livretPers Chœur de Nymphes
%# N'aimons jamais.
\livretPers Chœur de Sylvains, de Satyres, & de Bergers
%# Cedons à l'Amour qui nous presse,
%# Pour vivre *heureux aimons sans cesse.
\livretPers Chœur de Nymphes
%# Pour vivre en paix,
%# N'aimons jamais.
\livretPers Syrinx
%# Le chagrin suit toûjours les Cœurs que l'Amour blesse.
\livretPers Pan
%# La tranquile Sagesse
%# N'a que des plaisirs imparfaits.
\livretPers Chœur de Sylvains, de Satyres, & de Bergers
%# Aymons sans cesse.
\livretPers Chœur de Nymphes
%# N'aimons jamais.
\livretPers Syrinx
%# On ne peut aimer sans foiblesse.
\livretPers Pan
%# Que cette foiblesse a d'attraits!
\livretPers Chœur de Sylvains, de Satyres, & de Bergers
%# Aymons sans cesse.
\livretPers Chœur de Nymphes
%# N'aimons jamais.
\livretPers Chœur de Sylvains, de Satyres, & de Bergers
%# Cedons à l'Amour qui nous presse,
%# Pour vivre *heureux aimons sans cesse.
\livretPers Chœur de Nymphes
%# Pour vivre en paix,
%# N'aimons jamais.
\livretPers Syrinx
\livretRef#'DFFchoeur
%# Faut-il qu'en vains discours un si beau jour se passe,
%# Mes Compagnes courons dans le fond des Forests.
%# Voy=ons qui d'entre-nous se sert mieux de ses traits.
%# Courons à la Chasse.
\livretPers Chœurs
%# Courons à la Chasse.
\livretPersDidas Syrinx revenant sur le Theatre suivie de Pan
%# Pourquoy me suivre de si prés?
\livretPers Pan
%#- Pourquoy fuïr que vous aime?
\livretPers Syrinx
%#= Un Amant m'embarasse.
\livretPers Syrinx & les Chœurs derriere le Theatre
%# Courons à la Chasse.
\livretPersDidas Pan \line { revenant une seconde fois sur la Scene suivant toûjours Syrinx }
\livretRef#'DFGchoeur
%# Je ne puis vous quitter, mon cœur s'attache à vous
%# Par des nœuds trop forts & trop doux…
\livretPers Syrinx
%# Mes Compagnes? Venez?… C'est en vain que j'appelle.
\livretPers Pan
%# Ecoutez, Ingrate, écoutez,
%# Un Dieu charmé de vos beautez,
%# Qui vous jure un amour fidelle.
\livretPersDidas Syrinx fuyant
%# Je declare à l'Amour une guerre immortelle.
\livretPersDidas Troupe de Bergers qui arrestent Syrinx
%# Cru=elle, arrestez.
\livretPersDidas Troupe de Sylvains & de Satyres qui arrestent Syrinx
%# Arrestez, cru=elle.
\livretPers Syrinx
%# On me retient de tous costez.
\livretPers Chœurs de Satyres, de Sylvains, & de Bergers
%# Cru=elle, arrestez.
\livretPers Syrinx
%# Dieux, Protecteurs de l'innocence,
%# Na=yades, Nymphes de ces Eaux,
%# J'implore icy vostre assistance.
\livretDidasP\justify {
  Syrinx se jette dans les Eaux.
}
\livretPersDidas Pan suivant Syrinx dans le Lac où elle s’est jettée
%# Où vous exposez-vous? Quels prodiges nouveaux?
%# La Nymphe est changée en Rozeaux!
\livretDidasP\justify {
  Le vent penetre dans les Roseaux, & leur fait former un bruit plaintif.
}
\livretRef#'DFHplainte
%# Helas! quel bruit! qu'entens-je! Ah quelle voix nouvelle!
%# La Nymphe tâche encor d'exprimer ses regrets.
%# Que son murmure est doux! que sa plainte a d'attraits,
%# Ne cessons point de nous plaindre avec-elle.
%# R'animons les Restes charmants
%# D'une Nymphe qui fut si belle,
%# Elle répond encore à nos gemissements,
%# Ne cessons point de nous plaindre avec elle.
\livretDidasP\justify {
  Pan donne des Roseaux aux Bergers, aux Satyres, & aux Sylvains,
  qui en forment un Concert de Flûtes.
}
\livretPers Pan
%# Les yeux qui m'ont charmés ne verront plus le jour.
%# Estoit-ce ainsi, cru=el Amour,
%# Qu'il falloit te vanger d'une Beauté rebelle,
%# N'auroit-il pas suffi de t'en rendre vainqueur,
%# Et de voir dans tes fers son insensible Cœur
%# Brûler avec le mien d'un ardeur éternelle,
%# Que tout ressente mes tourments.
\livretPersDidas Pan, & deux Bergers accompagnez du Concert de Flûtes
%# R'animons les Restes charmants
%# D'une Nymphe qui fut si belle,
%# Elle répond encor à nos gemissemens,
%# Ne cessons point de nous plaindre avec elle.
\livretDidasP\justify {
  Argus commence à s’assoupir, Mercure déguisé en Berger s’approche de luy,
  & acheve de l’endormir en le touchant de son Caducée.
}
\livretPers Pan
%# Que ces roseaux plaintifs soient à jamais aimez…
\livretPers Mercure
%# Il suffit, Argus dort, tous ses yeux sont fermez.
%# Allons, que rien ne nous retarde,
%# Délivrons la Nymphe qu'il garde.

\livretScene SCENE VII
\livretDescAtt\wordwrap-center {
  Io, Mercure, Troupe de Sylvains, de Satyres, et de Bergers,
  Argus, Hierax.
}
\livretPersDidas Mercure \line { faisant sortir Io de la Demeure d’Argus, qu’il ouvre d‘un coup de son Caducée }
\livretRef#'DGArecit
%# Reconnoissez Mercure, & fuy=ez avec nous;
%# Eloignez-vous d'Argus avant qu'il se reveille.
\livretPersDidas Hierax arrestant Io, & parlant à Mercure
%# Argus avec cent yeux sommeille;
%# Mais croy=ez-vous
%# Endormir un Amant jaloux
%#- Demeurez.
\livretPers Mercure
%#= Mal-heureux, d'où te vient cette audace?
\livretPers Hierax
%# J'ay tout perdu, j'attens le trépas sans effroy,
%# Un coup de foudre est une grace
%# Pour un mal-heureux comme moy.
%# Eveillez-vous, Argus, vous vous laissez surprendre.
\livretPers Argus, & Hierax
%# Puissante Reine des Cieux,
%# Junon, venez nous défendre.
\livretPersDidas Mercure \line { frapant Argus & Hierax de son Caducée }
%# Commencez d'éprouver la colere des Dieux.
\livretDidasP\justify {
  Argus tombe mort, & Hierax changé en Oyseau de Proye s’envole.
}
\livretPers Chœur de Sylvain, de Satyres, & de Bergers
%#- Fuy=ons.
\livretPers Io
%#= Vous me quittez quel secours puis-je attendre?
\livretPers Chœur de Sylvain, de Satyres, & de Bergers
%# Fuy=ons, Junon vient dans ces Lieux.

\livretScene SCENE HUITIESME
\livretDescAtt\wordwrap-center {
  Junon sur son Char, Argus, Io, Erinnis, Furie.
}
\livretPers Junon
\livretRef#'DHArecit
%# Revoy le jour, Argus, que ta figure change.
\livretDidasP\justify {
  Argus transformé en Paon, vient se placer devant le Char de Junon.
}
\livretPers Junon
%# Et vous, Nymphe, apprenez comment Junon se vange.
%# Sors, barbare Erinnis, sors du fond des Enfers,
%# Vien, pren soin de servir ma vengeance fatale,
%# Et d'en montrer l'*horreur en cent Climats divers:
%# Epouvante tout l'Univers
%# Par les tourments de ma Rivale.
%# Vien la punir au gré de mon couroux:
%# Redouble ta Rage infernale,
%# Et fay, s'il se peut, qu'elle égale
%# La fureur de mon cœur jaloux.
\livretDidasP\justify {
  La Furie sort des Enfers, elle poursuit Io, elle l’enleve,
  & Junon remonte dans le Ciel.
}
\livretPersDidas Io poursuivie par la Furie
%# O Dieux! où me reduisez-vous?
\livretFinAct Fin du troisiéme Acte
\sep
