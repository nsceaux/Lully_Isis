\livretAct ACTE V
\livretDescAtt\wordwrap-center {
  Le Theatre change, & represente les Rivages du Nil, & l’une des Embouchûres
  par où ce Fleuve entre dans la Mer.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center { Io, la Furie. }
\livretPersDidas Io \line { sortant de la Mer, d’où elle est tirée par la Furie }
\livretRef#'FABrecit
%# Terminez mes tourmens puissant Maistre du Monde;
%# Sans vous, sans vostre amour, helas!
%# Je ne souffrirois pas.
%# Reduite au desespoir, mourante, vagabonde,
%# J'ay porté mon supplice en mille affreux Climats;
%# Une *horrible Furie =attachée =à mes pas,
%# M'a suivie =au travers du vaste sein de l'Onde.
%# Terminez mes tourmens puissant Maistre du Monde,
%# Voy=ez de quels maux icy bas,
%# Vostre Epouse punit mes mal-heureux appas;
%# Délivrez-moy de ma douleur profonde,
%# Ouvrez-moy par pitié les portes du Trépas.
%# Terminez mes tourmens puissant Maistre du Monde,
%# Sans vous, sans vostre amour, helas!
%# Je ne souffrirois pas.
%# C'est Jupiter qui m'aime! eh! qui le pourroit croire?
%# Je ne suis plus dans sa memoire.
%# Il n'entend point mes cris, il ne voit point mes pleurs,
%# Aprés m'avoir livrée =aux plus cru=els mal-heurs,
%# Il est tranquile au comble de sa Gloire,
%# Il m'abandonne au milieu des douleurs.
%# A la fin, je succombe, heureuse, si je meurs!
\livretDidasP\justify {
  Io tombe accablée de ses tourments, & Jupiter touché de pitié
  descend du Ciel.
}

\livretScene SCENE SECONDE
\livretDescAtt\wordwrap-center { Jupiter, Io, & la Furie. }
\livretPers Jupiter
\livretRef#'FBBrecit
%# Il ne m'est pas permis de finir vostre peine,
%# Et ma puissance souveraine,
%# Doit suivre du Destin l'irrevocable loy:
%# C'est tout ce que je puis par un amour extréme,
%# Que de quitter le Ciel & ma gloire suprême
%# Pour prendre part aux maux que vous souffrez pour moy.
\livretPers Io
%# Ah! mon supplice augmente encore!
%# Tous le feu des Enfers me brusle, & me dévore;
%# Mourray-je tant de fois sans voir finir mon sort?
\livretPers Jupiter
%# Ma tendresse pour vous rend Junon inflexible.
%# Elle voit mon amour, il luy paroit trop fort,
%# Son couroux se redouble, & devient invincible.
\livretPers Io
%# N'importe, en ma faveur, soy=ez toûjours sensible.
\livretPers Jupiter
%# C'est trop vous exposer à son jaloux transport.
%# J'irrite en vous aimant sa vengeance terrible.
\livretPers Io
%# Aimez-moy, s'il vous est possible,
%# Assez pour la forcer à me donner la mort.
\livretDidasP\justify {
  Junon descend sur la Terre.
}

\livretScene SCENE TROISIÈME
\livretDescAtt\wordwrap-center { Jupiter, Junon, Io, la Furie. }
\livretPers Jupiter
\livretRef#'FCArecit
%# Venez Dé=esse impitoy=able,
%# Venez, voy=ez, reconnoissez
%# Cette Nymphe mourante autrefois trop aimable.
%# C'est assez la punir, c'est vous vanger assez,
%# L'éclat de sa beauté ne la rend plus coupable;
%# Par la cru=elle *horreur du tourment qui l'accable,
%# Son crime & ses appas sont ensemble effacez.
%# Sans jalousie, =& sans allarmes,
%# Voy=ez ses yeux noy=ez de larmes
%# Que l'ombre de la mort commence de couvrir.
\livretPers Junon
%# Ils n'ont encor que trop de charmes
%# Puis qu'ils sçavent vous attendrir.
\livretPers Jupiter
%# Une juste pitié peut-elle vous aigrir?
%# Vostre couroux fatal ne doit-il point s'éteindre?
\livretPers Junon
%# Ah! vous la plaignez trop, elle n'est pas à plaindre.
%# Non, elle ne peut trop souffrir.
\livretPers Jupiter
%# Je sçay que c'est de vous que son sort doit dépendre.
%# Ce n'est qu'à vos bontez qu'elle doit recourir.
%# Il n'est rien que de moy vous ne deviez attendre,
%# Si je puis obliger vostre haine à se rendre.
\livretPers Io
%# Ah! laissez-moy mourir.
\livretPers Jupiter
%# Prenez soin de la secourir.
\livretPers Junon
%# Vous l'aimez d'un amour trop tendre,
%# Non, elle ne peut trop souffrir.
\livretPers Jupiter
%# Quoy le Cœur de Junon, quelque grand qu'il puisse estre,
%# Ne sçauroit tri=ompher d'une injuste fureur?
\livretPers Junon
%# De la Terre & du Ciel Jupiter est le Maistre,
%# Et Jupiter n'est pas le Maistre de son Cœur?
\livretPers Jupiter
%# Hé bien, il faut que je commence
%# A me vaincre en ce jour.
\livretPers Junon
%# Vous m'apprendrez à me vaincre à mon tour.
\livretPersDidas Jupiter & Junon ensemble
\livretPersVerse Junon \line {
  \hspace#7 \column {
    \livretVerse#8 { J’abandonneray ma vengeance, }
    \livretVerse#6 { Rendez-moy vostre amour. }
  }
}
\livretPersVerse Jupiter \line {
  \hspace#7 \column {
    \livretVerse#8 { Abandonnez vostre vengeance, }
    \livretVerse#6 { Je vous rends mon amour. }
  }
}
\livretPers Jupiter
%# Noires Ondes du Stix, c'est par vous que je jure,
%# Fleuve affreux, écoutez le serment que je fais.
%# Si cette Nymphe, enfin, reprend tous ses attraits,
%# Si Junon fait cesser les tourments qu'elle endure,
%# Je jure que ses yeux ne troubleront jamais
%# De nos Cœurs ré=unis la bien-heureuse paix.
%# Noires Ondres du Stix, c'est par vous que je jure,
%# Fleuve affreux, écoutez le serment que je fais.
\livretPers Junon
%# Nymphe, je veux finir vostre peine éternelle,
%# Que la Furie =emporte aux Enfers avec elle
%# Le trouble & les horreurs dont vos sens sont saisis.
\livretDidasP\justify {
  La Furie s’enfonce dans les Enfers, & Io se trouve délivrée de ses peine.
}
%# Aprés un rigoureux supplice,
%# Goûtez les biens parfaits que les Dieux ont choisis:
%# Et sous le nouveau nom d’Isis,
%# Joü=issez d'un bon-heur qui jamais ne finisse.
\livretPers Jupiter & Junon
%# Dieux, recevez Isis au rang des Immortels.
%# Peuples voisins du Nil, dressez-luy des Autels.
\livretDidasP\justify {
  Les Divinitez du Ciel descendent pour recevoir Isis, les Peuples d’Egypte
  luy dessent un Autel, & la reconnoissent pour la Divinité qui les doit proteger.
}
\column-break
\livretDidas\wordwrap-center {
  Divinitez qui descendent du Ciel dans la Gloire,
  Peuples d’Egypte chantants. Quatre Egyptiennes chantantes.
  Peuples d’Egypte dançants. Quatre Egyptiennes dançantes.
}
\livretPers Chœur des Divinitez
\livretRef#'FCBchoeur
%# Venez, Divinité nouvelle.
\livretPers Chœur des Peuples d'Egypte
%# Isis, tournez sur nous vos yeux,
%# Voy=ez l'ardeur de nostre zele.
\livretPers Chœur des Divinitez
%# La Celeste Cour vous appelle.
\livretPers Chœur des Peuples d'Egypte
%# Tout vous revere dans ces lieux.
\livretDidasP\justify {
  Jupiter & Junon prennent place au milieu des Divinitez, & y font placer Isis.
}
\livretPers Jupiter & Junon
%# Isis est immortelle,
%# Isis va briller dans ces lieux.
%# Isis joü=it avec les Dieux,
%# D'une Gloire éternelle.
\livretDidasP\justify {
  Jupiter & Junon, & les Divinitez remontent au Ciel, & y conduisent
  Isis dans le temps que les Chœurs des Divinitez, & des Peuples d’Egypte
  repetent ces quatre derniers Vers.
}
\livretFinAct Fin du cinquiéme & dernier Acte
