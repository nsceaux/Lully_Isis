\livretAct ACTE PREMIER
\livretDescAtt\justify {
  Le Theatre represente des Prairies agreables,
  où le Fleuve Inachus serpente.
}
\livretScene SCENE PREMIERE
\livretPers Hierax
\livretRef#'BABrecit
%# Cessons d'aimer une Infidelle,
%# Evitons la honte cru=elle
%# De servir, d'adorer qui ne nous aime plus,
%# Achevons de briser les nœuds qu'elle a rompus:
%# Dégageons-nous, sortons d'un si funeste empire.
%# Helas! malgré moy je soupire,
%# Ah, mon Cœur, quelle lacheté!
%# Quel charme te retient dans un honteux martire?
%# Tu n'as pas craint des fers qui nous ont tant cousté,
%# As-tu peur de la Liberté?
%# Revenez, Liberté charmante,
%# Vous n'estes que trop diligente,
%# Lors qu'il faut dans un Cœur faire place à l'Amour,
%# Mais que vous estes lente,
%# Lors qu'un juste Dépit presse vostre retour.

\livretScene SCENE SECONDE
\livretDescAtt\wordwrap-center {
  Pirante, Hierax
}
\livretPers Pirante
\livretRef#'BBArecit
%# C'est trop entretenir ces tristes resveries;
%# Venez, tournez vos pas vers ces Rives fleuries;
%# Regardez ces flots argentez,
%# Qui dans ces Vallons écartez,
%# Font briller l'émail des Prairies.
%# Interrompez vos soûpirs,
%# Tout doit estre icy tranquile;
%# Ce beau séjour est l'Azile
%# Du Repos, & des Plaisirs.
\livretPers Hierax
%# Depuis qu'une Nimphe inconstante
%# A trahy mon amour, & m'a manqué de foy:
%# Ces lieux, jadis si beaux, n'ont plus rien qui m'enchante,
%# Ce que j'aime a changé, tout est changé pour moy.
\livretPers Pirante
%# La Fille d'Inachus hautement vous prefere
%# A mille autres Amants de vostre sort jaloux;
%# Vous avez l'aveu de son Pere,
%# En faveur d'Argus, vostre Frere,
%# La puissante Junon se declare pour vous.
\livretPers Hierax
%# Si l'Ingrate m'aimoit, je serois son Espoux.
%# Cette Nymphe legere
%# De jour en jour differe
%# Un Hymen qu'autrefois elle avoit crû si doux.
%# L'inconstante n'a plus l'empressement extréme
%# De cét Amour naissant qui répondoit au mien,
%# Son changement paroist en dépit d'elle-mesme,
%# Je ne le connois que trop bien;
%# Sa bouche quelquefois dit encor qu'elle m'aime,
%# Mais son cœur, ny ses yeux ne m'en disent plus rien.
\livretPers Pirante
%# Se peut-il qu'elle dissimule?
%# Aprés tant de serments, ne la croy=ez-vous pas?
\livretPers Hierax
%# Je ne le crûs que trop, helas?
%# Ces serments qui trompoient mon cœur tendre & credule.
%# Ce fut dans ces Vallons, où par mille détours
%# Inachus prend plaisir à prolonger son cours;
%# Ce fut sur son charmant rivage,
%# Que la Fille volage
%# Me promit de m'aimer toûjours.
%# Le Zephir fut témoin, l'Onde fut attentive,
%# Quand la Nymphe jura de ne changer jamais;
%# Mais le Zephir leger, & l'Onde fugitive,
%# Ont enfin emporté les serments qu'elle a faits.
%# Je la voy l'Infidelle.
\livretPers Pirante
%# Eclaircissez-vous avec elle.

\livretScene SCENE TROISIESME
\livretDescAtt\wordwrap-center {
  La Nymphe Io, Mycene, Hierax, Pirante.
}
\livretPers Io
\livretRef#'BCArecit
%# M'aimez-vous? puis-je m'en flater?
\livretPers Hierax
%# Cru=elle, en voulez-vous douter?
%# En vain vostre inconstance éclate,
%# En vain elle m'anime à briser tous les nœuds,
%# Je vous aime toûjours, Ingrate,
%# Plus que vous ne voulez; & plus que je ne veux.
\livretPers Io
%# Je crains un funeste présage.
%# Un aigle dévorant vient de fondre à mes yeux,
%# Sur un Oyseau qui dans ces lieux,
%# M'entretenoit d'un doux ramage.
%# Differez nostre *Hymen, suivons l'avis des Cieux.
\livretPers Hierax
%# Nostre *Hymen ne déplaist qu'à vostre cœur volage,
%# Répondez-moy de vous, je vous répons des Dieux.
%# Vous juriez autrefois que cette Onde rebelle,
%# Se feroit vers sa source une route nouvelle,
%# Plûtost qu'on ne verroit vostre Cœur dégagé:
%# Voy=ez couler ces flots dans cette vaste Plaine,
%# C'est le mesme penchant qui toûjours les entraîne,
%# Leur cours ne change point, & vous avez changé.
\livretPers Io
%# Laissez-moy revenir de mes fray=eurs secretes;
%# J'attens de vostre amour cét effort genereux.
\livretPers Hierax
%# Je veux ce qui vous plaist, Cru=elle que vous estes,
%# Vous n'abusez que trop d'un amour mal-heureux.
\livretPers Io
%#- Non, je vous aime encor.
\livretPers Hierax
%#= Qu'elle froideur extréme!
%# Inconstante, est-ce ainsi qu'on doit dire qu'on aime?
\livretPers Io
%# C'est à tord que vous m'accusez,
%# Vous avez veu toûjours vos Rivaux méprisez.
\livretPers Hierax
%# Le mal de mes Rivaux n'égale point ma peine,
%# La douce illusi=on d'une esperance vaine
%# Ne les fait point tomber du faiste du bon-heur,
%# Aucun d'eux, comme moy, n'a perdu vostre Cœur,
%# Comme eux, à vostre *humeur severe,
%# Je ne suis point accoûtumé:
%# Quel tourment de cesser de plaire,
%# Lors qu'on a fait l'effay du plaisir d'estre aimé!
%# Je ne le sens que trop, vostre Cœur se détache,
%# Et je ne sçay qui me l'arrache.
%# Je cherche en vain l'*heureux Amant
%# Qui me dérobe un bien charmant,
%# Où j'ay crû devoir seul pretendre;
%# Je sentirois moins mon tourment
%# Si je trouvois à qui m'en prendre.
%# Vous fuy=ez mes regards, vous ne me dîtes rien.
%# Il faut vous délivrer d'un fâcheux entretien,
%# Ma presence vous blesse, & c'est trop vous contraindre.
\livretPers Io
%# Jaloux, sombre; & chagrin, partout où je vous voy,
%# Vous ne cessez point de vous plaindre;
%# Je voudrois vous aimer autant que je le doy,
%# Et vous me forcez à vous craindre.
\livretPers Io & Hierax
%# Non, il ne tient qu'à vous
%# De rendre nostre sort plus doux.
\livretPers Io
%# Non, il ne tient qu'à vous
%# De rendre
%# Mon cœur plus tendre.
\livretPers Hierax
%# Non, il ne tient qu'à vous
%# De rendre mon cœur moins jaloux.
\livretPers Io & Hierax
%# Non, il ne tient qu'à vous
%# De rendre nostre sort plus doux.

\livretScene SCENE QUATRIESME
\livretDescAtt\wordwrap-center {
  Io, Mycene.
}
\livretPers Mycene
\livretRef#'BDArecit
%# Ce Prince trop long-temps dans ses chagrins s'obstine.
%# On pardonne au premier transport
%# D'un Amour qui se plaint à tort,
%# Et qui sans raison se mutine;
%# Mais à la fin
%# On se chagrine,
%# Contre un Amour chagrin.
\livretPers Io
%# Je veux bien te parler enfin sans artifice,
%# Ce Prince infortuné s'allarme avec Justice,
%# Le Maistre souverain de la Terre & des Cieux
%# Entreprend de plaire à mes yeux,
%# Du cœur de Jupiter l'Amour m'offre l'Empire;
%# Mercure est venu me le dire:
%# Je le voy chaque jour descendre dans ces lieux.
%# Mon cœur, autant qu'il peut, fait toûjours resistance,
%# Et pour attaquer ma constance,
%# Il ne falloit pas moins que le plus grand des Dieux.
\livretPers Mycene
%# On écoute aisément Jupiter qui soûpire,
%# C'est un Amant qu'on n'ose mépriser;
%# Et du plus grand des Cœurs le glori=eux Empire
%# Est difficile à refuser.
\livretPers Io
%# Lors qu'on me presse de me rendre
%# Aux attraits d'un Amour nouveau;
%# Plus le charme est puissant, & plus il seroit beau
%# De pouvoir m'en deffendre.
%# Quoy, tu veux me quitter. d'où vient ce soin pressant?
\livretPers Mycene
%# C'est pour vous seule, icy, que Mercure descend.

\livretScene SCENE CINQUIESME
\livretDescAtt\wordwrap-center {
  Mercure, Io, chœur des Divinitez de la Terre, et chœur des Echos.
}
\livretPersDidas Mercure sur un Nüage
\livretRef#'BEAchoeur
%# Le Dieu puissant qui lance le Tonnerre,
%# Et qui des Cieux tient le Sceptre en ses mains,
%# A resolu de venir sur la Terre
%# Chasser les maux qui troûblent les Humains.
%# Que la Terre avec soin à cét honneur réponde,
%# Echos, retentissez dans ces lieux pleins d'appas;
%# Annoncez qu'aujourd'huy pour le bon-heur du Monde,
%# Jupiter descend icy bas.
\livretDidasP\wordwrap {
  Les Chœurs repetent ces quatre derniers Vers dans le temps
  que Mercure descend sur la Terre.
}
\livretPersDidas Mercure parlant à Io
\livretRef#'BEBrecit
%# C'est ainsi que Mercure
%# Pour abuser les Dieux jaloux
%# Doit parler hautement à toute la Nature,
%# Mais il doit s'expliquer autrement avec vous.
%# C'est pour vous voir, c'est pour vous plaire,
%# Que Jupiter descend du celeste Séjour;
%# Et les biens qu'icy-bas sa presence va faire,
%# Ne seront dus qu'à son amour.
\livretPers Io
%# Pourquoy du haut des Cieux, ce Dieu veut-il descendre?
%# Mes vœux sont engagés, mon cœur a fait un choix,
%# L'Amour tost ou tard doit pretendre,
%# Que tous les Cœurs se rangent sous ses Loix:
%# C'est un hommage qu'il faut rendre,
%# Mais c'est assés de le rendre une fois.
\livretPers Mercure
%# Ce seroit en aimant une contrainte étrange,
%# Qu'un Cœur pour mieux choisir n'osast se dégager:
%# Quand c'est pour Jupiter qu'on change,
%# Il n'est pas honteux de changer.
%# Que tout l'Univers se pare
%# De ce qu'il a de plus rare,
%# Que tout brille dans ces lieux.
%# Que la Terre partage
%# L'éclat & la gloire des Cieux;
%# Que tout rende *hommage
%# Au plus grand des Dieux.

\livretScene SCENE SIXIESME
\livretDescAtt\justify {
  Les divinitez de la Terre, des Eaux, & des Richesses soûterraines,
  viennent magnifiquement parées pour recevoir Jupiter, & pour luy rendre
  hommage.
}
\livretPers Chœur des Divinitez
\livretRef#'BFAchoeur
%# Que la Terre partage
%# L'éclat & la gloire des Cieux;
%# Que tout rende *hommage
%# Au plus grand des Dieux.
\livretDescAtt\wordwrap-center { Vingt-quatre Divinitez chantantes }
\livretDidasP\justify {
  Huit Divinitez de la Terre. Huit Divinitez des Eaux. Huit Divinitez
  des Richesses soûterraines.
}
\livretDescAtt\wordwrap-center { Douze Divinitez dançantes }
\livretDidasP\justify {
  Quatre Divinitez de la Terre. Quatre Divinitez des Eaux. Quatre Divinitez
  des Richesses soûterraines.
}
\livretPersDidas Jupiter descendant de Ciel
\livretRef#'BFDchoeur
%# Les armes que je tiens protegent l'Innocence,
%# L'effort n'en est fatal qu'à l'orgueil des Titans.
%# Vous qui suivez mes Loix, vivez sous ma puissance,
%# Toûjours heureux, toûjours contents.
%# Jupiter vient sur la Terre,
%# Pour la combler de bien-faits,
%# Il est armé du Tonnere,
%# Mais c'est pour donner la paix.
\livretDidasP\justify {
  Le Chœur des Divinitez repete ces quatre derniers Vers dans le temps
  que Jupiter descend.
}
\livretFinAct Fin du premier Acte
\sep
