\livretAct ACTE SECOND
\livretDescAtt\justify {
  Le Theatre devient obscurcy par des Nuages épais qui l’environnent
  de tous costez.
}
\livretScene SCENE PREMIERE
\livretPers Io
\livretRef#'CABrecit
%# Où suis-je, d'où vient ce Nu=age!
%# Les Ondes de mon Pere, & son charmant Rivage,
%# Ont disparu tout à coup à mes yeux!
%# Où puis-je trouvez un passage?
%# La jalouse Reine des Cieux
%# Me fait-elle si tost acheter l'avantage
%# De plaire au plus puissant des Dieux?
%# Que vois-je! quel éclat se répand dans ces lieux?
\livretDidasP\justify {
  Jupiter paroist, & les Nuages qui obscurcissent le Theatre sont illuminez
  & peints de couleurs les plus brillantes & les plus agreables.
}

\livretScene SCENE SECONDE
\livretDescAtt\wordwrap-center { Jupiter, Io. }
\livretPers Jupiter
\livretRef#'CBArecit
%# Vous voy=ez Jupiter, que rien ne vous étonne.
%# C'est pour tromper Junon & ses regards jaloux
%# Qu'un nu=age vous environne,
%# Belle Nymphe r’assurez-vous.
%# Je vous aime, & pour vous le dire
%# Je sors avec plaisir de mon suprême Empire.
%# La foudre est dans mes mains, les Dieux me font la cour,
%# Je tiens tout l'Univers sous mon obe=ïssance;
%# Mais si je pretens en ce jour
%# Engager vostre cœur à m'aimer à son tour,
%# Je fonde moins mon esperance
%# Sur la grandeur de ma puissance,
%# Que sur l'excés de mon amour.
\livretPers Io
%# Que sert-il qu'icy-bas vostre amour me choisisse?
%# L'*honneur m'en vient trop tard, j'ay formé d'autres nœuds:
%# Il falloit que ce bien pour combler tous mes vœux,
%# Ne me coûtast point d'injustice,
%# Et ne fit point de mal-heureux.
\livretPers Jupiter
%# C'est une assez grande gloire
%# Pour vostre premier Vainqueur,
%# D'estre encor dans vostre memoire,
%# Et de me disputer si long-temps vostre Cœur.
\livretPers Io
%# La Gloire doit forcer mon cœur à se défendre.
%# Si vous sortez du Ciel pour chercher les douceurs
%# D'une amour tendre,
%# Vous pourrez aisément attaquer d'autres Cœurs,
%# Qui feront gloire de se rendre.
\livretPers Jupiter
%# Il n'est rien dans les Cieux, il n'est rien icy-bas,
%# De si charmant que vos appas;
%# Rien ne peut me toucher d'une flame si forte;
%# Belle Nymphe vous l'emportez
%# Sur les autres Beautez,
%# Autant que Jupiter l'emporte
%# Sur les autres Divinitez.
%# Verrez-vous tant d'amour avec indifference?
%# Quel trouble vous saisit? où tournez-vous vos pas?
\livretPers Io
%# Mon Cœur en vostre presence
%# Fait trop peu de resistance;
%# Contentez-vous, helas!
%# D'étonner ma constance,
%# Et n'en tri=omphez pas.
\livretPers Jupiter
%# Et pourquoy craignez-vous Jupiter qui vous aime?
\livretPers Io
%# Je crains tout, je me crains moy-mesme.
\livretPers Jupiter
%#- Quoy, voulez-vous me fuïr?
\livretPers Io
%#= C'est mon dernier espoir.
\livretPers Jupiter
%#- Ecoutez mon amour.
\livretPers Io
%#= Ecoutez mon Devoir.
\livretPers Jupiter
%# Vous avez un cœur libre, & qui peut se défendre.
\livretPers Io
%# Non, vous ne laissez pas mon cœur en mon pouvoir.
\livretPers Jupiter
%# Quoy, vous ne voulez pas m'entendre?
\livretPers Io
%# Je n'ay que trop de peine à ne le pas vouloir.
%#- Laissez-moy.
\livretPers Jupiter
%#= Quoy, si tost?
\livretPers Io
%# Je devois moins attendre;
%# Que ne fuy=ois-je, *helas! avant que de vous voir!
\livretPers Jupiter
%# L'Amour pour moy vous sollicite,
%# Et je voy que vous me quittez.
\livretPers Io
%# Le Devoir veut que je vous quitte,
%# Et je sens que vous m'arrestez.

\livretScene SCENE TROISIESME
\livretDescAtt\wordwrap-center {
  Mercure, Jupiter, Io.
}
\livretPers Mercure
\livretRef#'CCArecit
%# Iris est icy-bas, & Junon elle-mesme,
%# Pourroit vous suivre dans ces lieux.
\livretPers Jupiter
%# Pour la Nymphe que j'aime,
%# Je crains ses transports furi=eux.
\livretPers Mercure
%# Sa vengeance seroit funeste
%# Si vostre amour estoit surpris.
\livretPers Jupiter
%# Va, prens soin d'arrester Iris,
%# Mon amour prendra soin du reste.
\livretDidasP\justify {
  Io tâche à fuïr Jupiter qui la suit.
}

\livretScene SCENE QUATRIESME
\livretDescAtt\wordwrap-center {
  Mercure, Iris.
}
\livretPers Mercure
\livretRef#'CDArecit
%# Arrestez, belle Iris, différez un moment
%# D'accomplir en ces lieux ce que Junon desire.
\livretPers Iris
%# Vous m'arresterez vainement,
%# Et vous n'aurez rien à me dire.
\livretPers Mercure
%# Mais, si je vous disois que je veux vous choisir
%# Pour attacher mon cœur d'une éternelle chaîne?
\livretPers Iris
%# Je vous écouterois peut-estre avec plaisir,
%# Mais je vous croirois avec peine.
\livretPers Mercure
%# Refusez-vous d'unir vostre cœur & le mien?
\livretPers Iris
%# Jupiter & Junon nous occupent sans cesse,
%# Nos soins sont assez grands sans que l'Amour nous blesse,
%# Nous n'avons pas tous deux le loisir d'aimer bien.
\livretPers Mercure
%# Si je fais ma premiere affaire,
%# De vous voir, & de vous plaire?
\livretPers Iris
%# Je feray mon premier devoir
%# De vous plaire, & de vous voir.
\livretPers Mercure
%# Un cœur fidelle
%# A pour moy de charmants appas:
%# Vous avez mille attraits, vous n'estes que trop belle,
%# Mais je crains que vous n'ay=ez pas
%# Un Cœur fidelle.
\livretPers Iris
%# Pourquoy craignez-vous tant
%# Que mon cœur se dégage?
%# Je vous permets d'estre inconstant,
%# Si-tost que je seray volage.
\livretPers Mercure, & Iris
%# Promettez-moy de constantes amours;
%# Je vous promets de vous aimer toûjours.
\livretPers Mercure
%# Que la feinte entre nous finisse;
\livretPers Iris
%# Parlons sans mystere en ce jour.
\livretPers Mercure, & Iris
%# Le moindre artifice
%# Offense l'Amour.
\livretPers Iris
%# Quel soin presse icy-bas Jupiter de descendre?
\livretPers Mercure
%# Le seul bien des Mortels luy fait quitter les Cieux.
%# Mais quel soupçon nouveau Junon peut-elle prendre?
%# Ne suivroit-elle point Jupiter en ces lieux?
\livretPers Iris
%# Dans les Jardins d'Hébé Junon vient de se rendre.
\livretDidasP\justify {
  Junon paroist au milieu d’un Nuage qui s’avance.
}
\livretPers Mercure
%# Un Nu=age entr'ouvert la découvre à mes yeux.
%# Iris parle ainsi sans mystere?
%# C'est ainsi que je puis me fi=er à sa foy?
\livretPers Iris
%# Ne me reprochez-pas que je suis peu sincere,
%# Vous ne l'estes pas plus que moy.
\livretPers Mercure, & Iris
%# Gardez pour quelqu'autre
%# Vostre amour trompeur;
%# Je reprens mon cœur,
%# Reprenez le vostre.
\livretDidasP\justify {
  Le Nuage s’approche de Terre, & Junon descend.
}

\livretScene SCENE CINQUIESME
\livretDescAtt\wordwrap-center {
  Junon, Iris.
}
\livretPers Iris
\livretRef#'CEBrecit
%# J'ay cherché vainement la Fille d'Inachus.
\livretPers Junon
%# Ah, je n'ay pas besoin d'en sçavoir davantage,
%# Non, Iris, ne la cherchons plus.
%# Jupiter, dans ces lieux, m'a donné de l'ombrage,
%# J'ay traversé les Airs, j'ay percé le Nu=age
%# Qu'il opposoit à mes regards:
%# Mais en vain j'ay tourné les yeux de toutes parts,
%# Ce Dieu par son pouvoir suprême
%# M'a caché la Nymphe qu'il aime,
%# Et ne m'a laissé voir que des Troupeaux épars,
%# Non, non, je ne suis point une incredule Epouse
%# Qu'on puisse tromper aisément,
%# Voy=ons qui feindra mieux de Jupiter Amant,
%# Ou de Junon jalouse.
%# Il est maistre des Cieux, la Terre suit sa loy,
%# Sous sa toute-puissance il faut que tout fléchisse,
%# Mais puisqu'il ne pretend s'armer que d'artifice,
%# Tout Jupiter qu'il est, il est moins fort que moy.
%# Dans ces lieux écartez, voy que la Terre est belle.
\livretPers Iris
%# Elle *honore son Maistre, & brille sous ses pas.
\livretPers Junon
%# L'Amour, cét Amour infidelle,
%# Qui du plus haut des Cieux l'appelle,
%# Fait que tout luy rit icy-bas.
%# Prés d'une Maistresse nouvelle
%# Dans le fond des Deserts on trouve des appas,
%# Et le Ciel mesme ne plaist pas
%# Avec une Epouse immortelle.

\livretScene SCENE SIXIESME
\livretDescAtt\wordwrap-center {
  Jupiter, Junon, Mercure, Iris.
}
\livretPers Jupiter
\livretRef#'CFArecit
%# Dans les Jardins d'Hébé vous deviez en ce jour
%# D'une nouvelle Nymphe augmenter vostre Cour;
%# Quel dessein si pressant dans ces lieux vous ameine?
\livretPers Junon
%# Je ne vous suivray pas plus loin.
%# Je viens de vostre amour attendre un nouveau soin:
%# Ne vous étonnez pas qu'on vous quitte avec peine,
%# Et que de Jupiter on ait toûjours besoin.
%# Vous m'aimez, & j'en suis certaine.
\livretPers Jupiter
%# Souhaitez, je promets
%# Que vos vœux seront satisfaits.
\livretPers Junon
%# J'ay fait choix d'une Nymphe, & déja la Dé=esse,
%# De l'aimable Jeunesse
%# Se prepare à la recevoir;
%# Mais je n'ose sans vous disposer de personne
%# Si j'ay quelque pouvoir,
%# Je n'en pretens avoir
%# Qu'autant que vostre amour d'en donne.
%# Ce don de vostre main me sera preci=eux.
\livretPers Jupiter
%# J'approuve vos desirs, que rien n'y soit contraire.
%# Mercure, ay=ez soin de luy plaire,
%# Et portez à son gré mes ordres en tous lieux.
%# Que tout suive les loix de la Reine des Cieux.
\livretPers Mercure, & Iris
%# Que tout suive les loix de la Reine des Cieux.
\livretPers Jupiter
%# Parlez, que vostre choix hautement se declare.
\livretPers Junon
%# La Nymphe qui me plaist ne vous déplaira pas.
%# Vous ne verrez point icy bas
%# De merite plus grand, ny de Beauté plus rare:
%# Les honneurs que je luy prepare
%# Ne luy sont que trop dûs;
%# Enfin, Junon choisit la fille d'Inachus.
\livretPers Jupiter
%#- La fille d'Inachus!
\livretPers Junon
%#= Declarez-vous pour elle.
%# Peut-on voir à ma suite une Nymphe plus belle,
%# Plus capable d'orner ma Cour,
%# Et de marquer pour moy le soin de vostre amour?
%# Vous me l'avez promise, & je vous la demande.
\livretPers Jupiter
%# Vous ne sçauriez combler d'une gloire trop grande
%# La Nymphe que vous choisissez,
%# Junon commande,
%# Allez, Mercure, obe=ïssez.
\livretPers Iris
%# Junon commande,
%# Allez, Mercure, obe=ïssez.

\livretScene SCENE VII
\livretDescAtt\wordwrap-center {
  Le Theatre change, & represente les Jardins d’Hebé, Déesse de la Jeunesse.
}
\livretDidasP\wordwrap-center {
  Hebé, Troupe des Jeux et de Plaisirs. Troupe de Nymphes de la Suite de Junon.
}
\livretDidasP\justify {
  Hebé, Déesse de la Jeunesse. Six Nymphes de Junon suivantes.
  Vingt-quatre Jeux & Plaisirs chantants.
  Neuf Jeux & Plaisirs dançants.
}
\livretDidasP\justify {
  Des Jeux & des Plaisirs s’avancent en dançant devant la Déesse Hebé.
}
\livretPers Hebé
\livretRef#'CGBchoeur
%# Les Plaisirs les plus doux
%# Sont faits pour la Jeunesse.
%# Venez Jeux charmants, venez-tous;
%# Gardez-vous bien d'amener avec vous
%# La severe Sagesse:
%# Les plaisirs les plus doux
%# Sont faits pour la Jeunesse.
%# Fuy=ez, fuy=ez, sombre Tristesse,
%# Noirs chagrins, fuy=ez loin de nous,
%# Vous estes destinez pour l'affreuse veillesse!
%# Les plaisirs les plus doux
%# Sont faits pour la Jeunesse.
\livretDidasP\justify {
  Le Chœur repete ces deux derniers Vers.
}
\livretDidasP\justify {
  Les Jeux, les Plaisirs, & les Nymphes de Junon se divertissent par des Dances
  & par des Chansons, en attendant la nouvelle Nymphe dont Junon veut faire
  choix.
}
\livretPersDidas Deux Nymphes chantent ensemble
\livretRef#'CGDnymphes
%# Aymez, profitez du temps,
%# Jeunesse charmante,
%# Rendez vos desirs contents.
%# Tout rit, tout enchante
%# Dans les plus beaux ans.
%# L'Amour vous éclaire,
%# Marchez sur ses pas;
%# Cherchez à vous faire
%# Des nœuds pleins d'appas,
%# Que vous sert de plaire,
%# Si vous n'aimez pas?
\null
%# Pourquoy craignez-vous d'aimer,
%# Beautez inhumaines,
%# Cessez de vous allarmer;
%# L'Amour a des peines,
%# Qui doivent charmer.
%# Ce Dieu vous éclaire,
%# Marchez sur ses pas.
%# Cherchez à vous faire
%# Des nœuds pleins d'appas,
%# Que vous sert de plaire,
%# Si vous n'aimez pas?
\livretPers Chœur
\livretRef#'CGFchoeur
%# Que ces Lieux ont d'attraits,
%# Goûtons-en bien les charmes;
%# L'Amour n'y fait jamais
%# Verser de tristes larmes,
%# Les soins, & les allarmes,
%# N'en troublent point la paix,
%# Joü=issons dans ces Retraites,
%# Des douceurs les plus parfaites,
%# Suivez-nous charmants Plaisirs,
%# Comblez tous nos desirs.
\null
%# Voy=ons couler ces Eaux
%# Dans ces ri=ants Boccages;
%# Chantez petits Oyseaux,
%# Chantez sur ces feuillages;
%# Joignez vos doux ramages
%# A nos Concerts nouveaux.
%# Joü=issons dans ces Retraites,
%# Des douceurs les plus parfaites,
%# Suivez-nous charmants Plaisirs,
%# Comblez tous nos desirs.

\livretScene SCENE VIII
\livretDescAtt\wordwrap-center {
  Io, Mercure, Iris, Hebé, les Jeux, les Plaisirs,
  Trouppe de Nymphes de la Suite de Junon.
}
\livretPersDidas Mercure, & Iris conduisant Io
\livretRef#'CHAchoeur
%# Servez, Nymphe, servez, avec un soin fidelle,
%# La puissante Reine des Cieux:
%# Suivez dans ces aimables lieux,
%# La Jeunesse immortelle;
%# Tout plaist, & tout rit avec elle.
\livretDidasP\justify {
  Hebé, & les Nymphes reçoivent Io.
}
\livretPers Hebé, & le Chœur des Nymphes
%# Que c'est un plaisir charmant
%# D'estre jeune & belle!
%# Tri=omphez à tout moment,
%# D'une Conqueste nouvelle:
%# Que c'est un plaisir charmant
%# D'estre jeune & belle.
\livretFinAct Fin du second Acte
\sep
