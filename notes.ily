 \notesSection "Notes"
\markuplist\with-line-width-ratio #0.7 \column-lines {
  \livretAct NOTES
  \null
  \livretParagraph {
    Cette édition de l'opéra \italic { Isis } de Jean-Baptiste Lully,
    réalisée pour Les Talens Lyriques – Christophe Rousset, est basée
    sur les sources suivantes :
  }
  \null
  \indented-lines#5 {
    \line\bold { [Ballard 1719] }
    \line {
      \italic { Isis, tragédie, mise en musique par Mr de Lully, }
      Jean-Baptiste Lully (1632-1687).
    }
    \line { Partition imprimée complète. Ballard, Paris, 1719 }
    \wordwrap-lines {
      Bibliothèque nationale de France, département Musique, VM2-32.
      \with-url #"https://gallica.bnf.fr/ark:/12148/btv1b8446951f"
      \smaller\typewriter "https://gallica.bnf.fr/ark:/12148/btv1b8446951f"
    }
  }
  \null
  \indented-lines#5 {
    \line\bold { [Matériel 1677] }
    \wordwrap-lines {
      \italic {
        Isis, tragédie, mise en musique par Monsieur de Lully,
      }
      Jean-Baptiste Lully (1632-1687).
    }
    \line { Partition imprimée réduite. Ballard, Paris, 1677 }
    \wordwrap-lines {
      Bibliothèque nationale de France, département Musique, RES VMC-177
      \with-url #"https://gallica.bnf.fr/ark:/12148/btv1b55006681s"
      \smaller\typewriter "https://gallica.bnf.fr/ark:/12148/btv1b55006681s"
    }
  }
  \null
  %% livret
  \indented-lines#5 {
    \line\bold { [Livret imprimé] }
    \wordwrap-lines {
      \italic {
        Isis, tragedie en musique, ornée d'entrées de ballet, de
        machines, & de changements de theatre.
      }
      Quinault, Philippe (1635-1688)
    }
    \line { Monographie imprimée. Ballard, Paris, 1677. }
    \wordwrap-lines {
      Bibliothèque nationale de France, département Réserve des livres
      rares, RES-YF-2211
      \with-url #"https://gallica.bnf.fr/ark:/12148/bpt6k104070p"
      \smaller\typewriter "https://gallica.bnf.fr/ark:/12148/bpt6k104070p"
    }
  }
  \null
  \paragraph {
    De façon à pouvoir diriger sur le conducteur Ballard 1719, c’est
    cette source qui est retranscrite dans cette édition. Le
    matériel 1677 étant néanmoins plus fiable, les différences entre
    le matériel 1677 et Ballard 1719 sont indiquées sous forme de
    suggestions. En outre, les agréments des deux sources sont
    retranscrites dans cette édition.
  }
  \null
  \paragraph {
    Le matériel d'orchestre est constitué des parties
    séparées suivantes :
    dessus, hautes-contre, tailles, quintes, basses & basse continue,
    trompettes, timbales.
  }
  \null
  \paragraph {
    Cette édition est distribuée selon les termes de la licence
    Creative Commons Attribution-ShareAlike 4.0.  Il est donc permis,
    et encouragé, de jouer cette partition, la distribuer,
    l’imprimer.
  }
}
