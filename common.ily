\header {
  copyrightYear = "2018"
  composer = "Jean-Baptiste Lully"
  poet = "Philippe Quinault"
  date = "1677"
}

#(ly:set-option 'original-layout (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'use-ancient-clef (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'show-ancient-clef (not (symbol? (ly:get-option 'part))))
#(ly:set-option 'print-footnotes #t)
#(ly:set-option 'use-rehearsal-numbers #t)
#(ly:set-option 'apply-vertical-tweaks
                (and (not (eqv? #t (ly:get-option 'urtext)))
                     (not (symbol? (ly:get-option 'part)))))
#(ly:set-option 'print-footnotes
                (not (symbol? (ly:get-option 'part))))
%% Staff size
#(set-global-staff-size (if (symbol? (ly:get-option 'part)) 20 16))

%% Line/page breaking algorithm
%%  optimal   for lead sheets
%%  page-turn for instruments and vocal parts
\paper {
  #(define page-breaking (if (eqv? (ly:get-option 'part) #f)
                             ly:optimal-breaking
                             ly:page-turn-breaking))
}

\language "italiano"
\include "nenuvar-lib.ily"
\setPath "ly"

#(if (eqv? (ly:get-option 'part) 'haute-contre-sol)
     (begin
       (ly:set-option 'part 'haute-contre)
       (ly:set-option 'haute-contre-sol #t)))

#(if (eqv? (ly:get-option 'part) 'taille-sol)
     (begin
       (ly:set-option 'part 'taille)
       (ly:set-option 'taille-sol #t)))

\opusPartSpecs
#`((dessus       "Dessus"        () (#:notes "dessus" #:clef "dessus"))
   (haute-contre "Hautes-contre" ()
                 (;; Haute-contre => clé d'ut3 sauf si option clé de sol
                  #:notes "haute-contre"
                  #:clef ,(if (eqv? (ly:get-option 'haute-contre-sol) #t)
                             "treble"
                              "haute-contre")))
   (taille "Tailles" ()
           (;; Tailles => d'ut3 sauf si option clé de sol
            #:notes "taille"
            #:clef ,(if (eqv? (ly:get-option 'taille-sol) #t)
                        "treble"
                        "taille")))
   (quinte       "Quintes"       () (#:notes "quinte" #:clef "alto"))
   (basse        "Basses"        () (#:notes "basse" #:tag-notes basse #:clef "basse"))
   (timbales     "Timbales"      () (#:notes "timbales" #:clef "basse"))
   (trompette    "Trompette"     () (#:notes "trompette" #:clef "treble"))
   (basse-continue
    "Basse continue & basses" ()
    (#:notes "basse" #:tag-notes basse-continue #:clef "basse")))
\opusTitle "Isis"

\header {
  maintainer = \markup {
    Nicolas Sceaux,
    \with-url #"http://www.lestalenslyriques.com" \line {
      Les Talens Lyriques – Christophe Rousset
    }
  }
  license = "Creative Commons Attribution-ShareAlike 4.0 License"
}

#(define-markup-command (tacet layout props) ()
   (interpret-markup layout props #{ \markup {
  \hspace#2 \pad-to-box #'(0 . 0) #'(-2 . 1) \small TACET } #}))

#(define-markup-command (croche-pointee-double layout props) ()
   (interpret-markup
    layout props
    #{ \markup\score {
       \notemode { \forceStemLength #2 { si'8.^[ si'16] } }
       \layout {
         \context {
           \Staff
           \remove "Staff_symbol_engraver"
           \remove "Time_signature_engraver"
           \remove "Clef_engraver"
           \override StaffSymbol.line-count = 1
           \magnifyStaff #1/2
           \override Beam.direction = #UP
         }
         indent = 0
         ragged-right = ##t
       }
     }#}))

sugRythme =
#(define-music-function (parser location music) (ly:music?)
   #{ <>^\markup\score {
       \notemode { \forceStemLength #2 { $music } }
       \layout {
         \context {
           \Staff
           \remove "Staff_symbol_engraver"
           \remove "Time_signature_engraver"
           \remove "Clef_engraver"
           \override StaffSymbol.line-count = 1
           \magnifyStaff #1/2
           \override Beam.direction = #UP
           \override Stem.direction = #UP
         }
         indent = 0
         ragged-right = ##t
       }
     } #})
