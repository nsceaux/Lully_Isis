<<
  \tag #'(hebe basse) {
    \clef "vbas-dessus" <>^\markup\character Hebé
    r4 re'' mi'' |
    fa'' do''( sib'8)[\trill la'16] sib' |
    la'2\trill r8 re'' |
    si'4.\trill si'8 dod''4 |
    re''8 re''4( mi''16[ fa''] mi''4)\trill |
    re''4. re''8 la' sib' |
    do''4 do'' la' |
    sib'2 sib'8 la' |
    sol'2\trill r8 sol' |
    do'' sib' la'4 sib'8 do'' |
    re''2 mi''8 fa'' |
    mi''2 do''8 re'' |
    sib'2 sol'8 la' |
    sol'2\trill fa'4 |
    r re'' mi'' |
    fa'' do''4( sib'8)\trill[ la'16] sib' |
    la'2\trill r8 re'' |
    si'4.\trill si'8 dod''4~ |
    dod''8 re'' re''8[ mi''16 fa'']( mi''4)\trill |
    re''4. re''8 dod''\trill mi'' |
    la'4 mi' mi'8 la' |
    fa'4\trill re' sib'8 sib' |
    sib'2 r8 re'' |
    sol'4( fa'\trill) mi'8 fa' |
    mi'4.\trill do''8 do'' do'' |
    re'' mi'' re''4\trill si'8 sold' |
    la'2 si'8 do'' |
    si'2\trill la'4 |
    r re'' mi'' |
    fa'' do''4( sib'8\trill)[ la'16] sib' |
    la'2\trill r8 re'' |
    si'4.\trill si'8 dod''4 |
    re''8 re''4( mi''16[ fa''] mi''4)\trill |
    re''2
  }
  \tag #'vdessus \clef "vdessus"
  \tag #'vhaute-contre \clef "vhaute-contre"
  \tag #'vtaille \clef "vtaille"
  \tag #'vbasse \clef "vbasse"
  \tag #'(vdessus vhaute-contre vtaille vbasse) { R2.*33 | r4 r }
>>
<<
  \tag #'vdessus {
    <>^\markup\character Chœur fa''8 fa'' |
    mi''4\trill mi'' fa'' |
    re''2\trill re''4 |
    mi''4.\trill mi''8 fa''[ mi''] |
    fa''4 fa''( mi'') |
    fa''2 fa''8 fa'' |
    re''4 re''( do''8\trill)[ si'16] do'' |
    si'2\trill si'4 |
    dod''4.\trill dod''8 re''4~ |
    re''8 mi'' fa''4( mi'')\trill |
    re''2. |
  }
  \tag #'vhaute-contre {
    re'8 re' |
    dod'4\trill dod' re' |
    re'2 sol'4 |
    sol'4. sol'8 la'4~ |
    la'8 sol' sol'2\trill |
    fa'2 fa'8 fa' |
    fa'4 la'4. la'8 |
    re'2 sol'4 |
    mi'4.\trill mi'8 fa'4 |
    \footnoteHere #'(0 . 0) \markup {
      Matériel 1677 : \raise#1 \score {
        \new Staff \withLyrics {
          \tinyQuote \clef "petrucci-c3" \time 3/4 \partial 4
          fa'4~ | fa'8 mi'16[ re'] re'4\( dod'\) |
        } \lyrics { la jeu -- nes - }
        \layout { \quoteLayout }
      }
    }
    mi'8[ re'] re'4( dod') |
    re'2. |
  }
  \tag #'vtaille {
    la8 la |
    la4 la la |
    sib2 sib4 |
    do'4. do'8 do'4 |
    do' do'8[^\markup\croche-pointee-double sib la sib] |
    la2\trill la8 la |
    sib4 la4.\trill la8 |
    sol2 sol4 |
    la4. la8 la4 |
    la la2 |
    fad2.\trill |
  }
  \tag #'vbasse {
    re8 re |
    la4 la fa |
    sib2 sol4 |
    do'4. do'8 la4 |
    fa do2 |
    fa fa8 fa |
    sib4 fad4.\trill fad8 |
    sol2 mi4 |
    la4. la8 fa4 |
    re la,2 |
    re2. |
  }
  \tag #'(hebe basse) { \tag #'basse <>^\markup\character Chœur r4 R2.*10 }
>>
