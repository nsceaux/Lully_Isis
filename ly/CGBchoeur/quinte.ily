\clef "quinte" R2.*33 |
r4 r la8 la |
la4 la re' |
re'2 re'4 |
do' do' fa' |
fa sol4.\trill sol8 |
la2 la8 la |
sib4 la re' |
re'2 sol4 |
la4. la8 la4 |
la la2 |
la2. |
