\clef "taille" R2.*33 |
r4 r re'8 re' |
dod'4\trill dod' re' |
re'2 sol'4 |
mi'4.\trill do'8 do'4 |
do' do'2 |
do' do'8 do' |
re'4 re'2 |
re'2 mi'4 |
mi'4. mi'8 re'4 |
re' re'( dod') |
re'2. |
