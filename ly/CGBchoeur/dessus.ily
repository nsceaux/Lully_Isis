\clef "dessus" R2.*33 |
r4 r fa''8 fa'' |
mi''4\trill mi'' fa'' |
re''2 re''4 |
mi''4. mi''8 fa''4 |
fa'' mi''4.\trill fa''8 |
fa''2 fa''8 fa'' |
re''4 \once\slurDashed do''4.(\trill si'16 do'') |
si'2\trill si'4 |
dod''4. dod''8 re''4~ |
re''8 mi'' \footnoteHere #'(0 . 0) \markup {
  Matériel 1677 : \raise#1 \score {
    { \tinyQuote \clef "french" \time 3/4 \partial 4
      re''4~ | re''8 mi'' mi''4.\trill re''8 | re''2. | \bar "|." }
    \layout { \quoteLayout }
  }
} fa''4( mi'')\trill |
re''2. |
