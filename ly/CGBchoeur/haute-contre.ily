\clef "haute-contre" R2.*33 |
r4 r la'8 la' |
la'4 la' la' |
sib'2 sib'4 |
sol'4. sol'8 la'4~ |
la'8 sol' sol'2 |
fa'2 fa'8 fa' |
fa'4 la'4. la'8 |
sol'2 sol'4 |
mi'4. mi'8 fa'4~ |
fa'8 sol' \footnoteHere #'(0 . 0) \markup {
  Matériel 1677 : \raise#1 \score {
    { \tinyQuote \clef "petrucci-c1" \time 3/4 \partial 4
      fa'4~ |
      fa'8 sol' la'4. la'8 |
      fad'2. \bar "|." }
    \layout { \quoteLayout }
  }
} la'4( sol') |
fad'2.\trill |
