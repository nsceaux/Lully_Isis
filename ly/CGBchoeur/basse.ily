\clef "basse" <<
  \tag #'basse { R2.*33 | r4 r }
  \tag #'(basse-continue bc-part) {
    \tag #'bc-part <>^"[B.C.]"
    re,4 re dod |
    re mi2 |
    fa4 fad2 |
    sol4. sol8 la4 |
    sib8 sol la4 la, |
    re4. mi8 fa re |
    la4. sol8 fad4 |
    sol8 la sib4 sol |
    do'4. re'8 do' sib |
    la sol fa mib re do |
    sib, do sib, la, sol, fa, |
    do4 do'8 sib la sib |
    sol4. sol8 do' fa |
    do2 fa8 mi |
    re2 dod4 |
    re mi2 |
    fa4 fad2 |
    sol4. sol8 la4 |
    sib8 sol la4 la, |
    re4. re8 la sol |
    fa4 dod4. dod8 |
    re2 sol,4 |
    sol4. la8 sib4 |
    si!2. |
    do'4 do4. la,8 |
    si,8 do re4 mi |
    fa mi re |
    mi mi, la, |
    re2 dod4 |
    re mi2 |
    fa4 fad2 |
    sol4. sol8 la4 |
    sib8 sol la4 la, |
    re2
  }
>> \tag #'bc-part <>^"[Tous]" re4 |
la2 fa4 |
sib2 sol4 |
do'4. do'8 la4 |
fa do2 |
fa fa4 |
sib fad2 |
sol2 mi4 |
la4. la8 fa4 |
re la,2 |
<<
  \tag #'basse { re2. }
  \tag #'(basse-continue bc-part) {
    re2.~ |
    \once\set Staff.whichBar = "|"
    \tag #'bc-part <>^"[B.C.]"
    re4. do8 sib, la, |
    sol,2
  }
>>
