\score {
  \new ChoirStaff <<
    \new Staff \withLyricsB <<
      \global \keepWithTag #'vhaute-contre \includeNotes "voix"
    >>
    \keepWithTag #'couplet1 \includeLyrics "paroles"
    \keepWithTag #'couplet2 \includeLyrics "paroles"
    \new Staff \withLyricsB <<
      \global \keepWithTag #'vtaille \includeNotes "voix"
    >>
    \keepWithTag #'couplet1 \includeLyrics "paroles"
    \keepWithTag #'couplet2 \includeLyrics "paroles"
    \new Staff \withLyricsB <<
      \global \keepWithTag #'vbasse \includeNotes "voix"
    >>
    \keepWithTag #'couplet1 \includeLyrics "paroles"
    \keepWithTag #'couplet2 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s1*3 s2. \bar "" \break s4 s1*3 s2. \break
      }      
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
