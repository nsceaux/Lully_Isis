\clef "basse" sol8([ sol]) |
sol([ sol]) sol([ sol]) do([ do]) re([ re]) |
mi([ mi mi mi] mi4) si,8([ si,]) |
do([ do]) do([ do]) re([ re]) re([ re]) |
sol,8([ sol, sol, sol,] sol,4) sol8([ sol]) |
sol([ sol]) mi([ mi]) la([ la]) re([ re]) |
la,([ la,] la,4) la8([ la]) fa([ fa]) |
sib([ sib]) sol([ sol]) la([ la]) la,([ la,]) |
re([ re re re] re4) sol8([ sol]) |
sol([ sol]) mi([ mi]) fa([ fa]) re([ re]) |
mi([ mi]) do([ do]) re([ re]) si,([ si,]) |
la,([ la,]) sol,([ sol,]) re([ re re re]) |
sol,([ sol, sol, sol,] sol,4)
