\tag #'(couplet1 basse) {
  L’Hy -- ver qui nous tour -- men -- te
  s’obs -- tine à nous ge -- ler :
  Nous ne sçau -- rions par -- ler
  qu’a -- vec u -- ne voix trem -- blan -- te :
  La neige & les gla -- çons
  nous don -- nent de mor -- tels fris -- sons.
}
\tag #'couplet2 {
  Les Fri -- mats se ré -- pan -- dent
  sur nos corps lan -- guis -- sants :
  Le froid tran -- sit nos sens,
  les plus durs Ro -- chers se fen -- dent :
}
