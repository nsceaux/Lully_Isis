<<
  \tag #'(vhaute-contre basse) {
    \clef "vhaute-contre" <>^\markup\character Chœur de Peuples des Climats glacez
    re'8[( re')] |
    re'[( re')] re'[( re')] mi'[( mi')] fad'[( fad')] |
    sol'[( sol' sol' sol')] sol'4 re'8[( re')] |
    mi'[( mi')] do'[( do')] la[( la)] re'[( re')] |
    si[( si si si] si4) re'8[( re')] |
    re'[( re')] sol'[( sol')] mi'[( mi')] fa'[( fa')] |
    mi'[( mi'] mi'4) mi'8([ mi']) fa'([ fa']) |
    fa'([ fa']) sol'([ sol']) mi'([ mi']) la'([ la']) |
    fa'([ fa' fa' fa']) fa'4 re'8([ re']) |
    re'([ re']) mi'([ mi']) do'([ do']) re'([ re']) |
    si([ si]) do'([ do']) la([ la]) re'([ re']) |
    do'([ do']) si([ si]) la4. sol8 |
    sol([ sol sol sol] sol4)
  }
  \tag #'vtaille {
    \clef "vtaille" si8([ si]) |
    si([ si]) si([ si]) do'([ do']) do'([ do']) |
    si([ si si si]) si4 si8([ si]) |
    sol([ sol]) sol([ sol]) sol([ sol]) fad([ fad]) |
    sol([ sol sol sol] sol4) si8([ si]) |
    si([ si]) mi'([ mi']) dod'([ dod']) re'([ re']) |
    dod'([ dod'] dod'4) dod'8([ dod']) re'([ re']) |
    re'([ re']) mi'([ mi']) dod'4.\trill re'8 |
    re'8([ re' re' re']) re'4 si8([ si]) |
    si([ si]) do'([ do']) la([ la]) si([ si]) |
    sol([ sol]) la([ la]) fad([ fad]) si([ si]) |
    fad([ fad]) sol([ sol]) fad4. sol8 |
    sol([ sol sol sol] sol4)
  }
  \tag #'vbasse {
    \clef "vbasse" sol8([ sol]) |
    sol([ sol]) sol([ sol]) do([ do]) re([ re]) |
    mi([ mi mi mi]) mi4 si,8([ si,]) |
    do([ do]) do([ do]) re([ re]) re([ re]) |
    sol,8([ sol, sol, sol,] sol,4) sol8([ sol]) |
    sol([ sol]) mi([ mi]) la([ la]) re([ re]) |
    la,([ la,] la,4) la8([ la]) fa([ fa]) |
    sib([ sib]) sol([ sol]) la([ la]) la,([ la,]) |
    re([ re re re]) re4 sol8([ sol]) |
    sol([ sol]) mi([ mi]) fa([ fa]) re([ re]) |
    mi([ mi]) do([ do]) re([ re]) si,([ si,]) |
    la,([ la,]) sol,([ sol,]) re([ re re]) re |
    sol,([ sol, sol, sol,] sol,4)
  }
>>