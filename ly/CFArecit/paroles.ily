\tag #'(recit2 basse) {
  Dans les jar -- dins d’Hé -- bé vous de -- viez en ce jour
  d’u -- ne nou -- vel -- le nymphe aug -- men -- ter vô -- tre Cour ;
  quel des -- sein si pres -- sant dans ces lieux vous a -- me -- ne ?
  
  Je ne vous sui -- vray pas plus loin.
  Je viens de vôtre a -- mour at -- tendre un nou -- veau soin :
  ne vous é -- ton -- nez pas qu’on vous quitte a -- vec pei -- ne,
  et que de Ju -- pi -- ter on ait toû -- jours be -- soin.
  Vous m’ai -- mez, & j’en suis cer -- tai -- ne.
  
  Sou -- hai -- tez, je pro -- mets
  que vos vœux se -- ront sa -- tis -- faits.
  
  J’ay fait choix d’u -- ne nymphe, & dé -- ja la Dé -- es -- se
  de l’ai -- ma -- ble Jeu -- nes -- se,
  se pré -- pare à la re -- ce -- voir ;
  mais, je n’o -- se sans vous, dis -- po -- ser de per -- son -- ne,
  si j’ay quel -- que pou -- voir,
  je n’en pre -- tens a -- voir
  qu’au -- tant que vôtre a -- mour d’en don -- ne.
  Ce don de vô -- tre main me se -- ra pre -- ci -- eux.
  
  J’a -- prou -- ve vos de -- sirs ; que rien n’y soit con -- trai -- re :
  Mer -- cure, ay -- ez soin de luy plai -- re,
  et por -- tez à son gré, mes or -- dres en tous lieux :
  Que tout sui -- ve les loix de la Rei -- ne des Cieux.
}

Que tout sui -- ve les loix de la Rei -- ne des cieux.
Que tout sui -- ve les loix de la Rei -- ne des cieux.

\tag #'(recit2 basse) {
  Par -- lez, & qu'hau -- te -- ment vô -- tre choix se dé -- cla -- re.
  
  La Nym -- phe qui me plaît ne vous dé -- plai -- ra pas.
  Vous ne ver -- rez point i -- cy bas
  de me -- ri -- te plus grand, ny de beau -- té plus ra -- re :
  les hon -- neurs que je luy pré -- pa -- re
  ne luy sont que trop dûs ;
  en -- fin, Ju -- non choi -- sit la fil -- le d’I -- na -- chus.
  
  La fil -- le d’I -- na -- chus !
  
  De -- cla -- rez- vous pour el -- le.
  Peut- on voir à ma suite u -- ne nym -- phe plus bel -- le,
  plus ca -- pa -- ble d’or -- ner ma Cour,
  et de mar -- quer pour moy le soin de vôtre a -- mour ?
  Vous me l’a -- vez pro -- mise, & je vous la de -- man -- de.
  
  Vous ne sçau -- riez com -- bler d’u -- ne gloi -- re trop gran -- de
  la nym -- phe que vous choi -- sis -- sez,
  Ju -- non com -- man -- de,
  al -- lez, al -- lez, Mer -- cure, o -- be -- ïs -- sez.
  
  Ju -- non com -- man -- de,
  al -- lez, al -- lez, Mer -- cure, o -- be -- ïs -- sez.
}
