\clef "basse" la,2 la8 si do' la |
 mi' re'16 do' si8 la sold8 mi la sol? |
fa mi fa re mi fa mi re |
do si, do la, mi re mi mi, |
\once\tieDashed la,1~ |
la,2 sol,8 fa, |
mi,2 fa, |
sol, mi,4 mi |
fa2 re |
\once\tieDashed mi1~ |
mi2 la |
dod2. |
re4. do8 si,4 la, |
sol,2 sol8 fa |
mi4 re8 do sol,4 |
do2 fa4 re |
mi2 \once\tieDashed dod4~ dod |
re2 mi8 do re re, |
sol,1 |
sol4. fa8 mi4. re8 |
do2 mi,4 mi |
fa re la8 sol fa4 |
mi1 |
sold, | \allowPageTurn
la,4. si,8 dod2 |
re mi4 |
do2 re4 mi8 mi, |
\once\tieDashed la,2.~ |
la,2 si, |
mi2. re4 |
do2 sold, |
la,2. fa,4 |
\once\tieDashed sol,2~ sol,4 |
fad,1 |
sol,2 do |
fa4 mi8 fa sol sol, |
do2. |
re |
mi |
fa |
mi4. fa8 mi re |
do2. |
re |
mi |
la,4 mi,2 |
la,4. sol,8 \once\tieDashed fa,2~ |
fa, mi,4 la, |
re,2 \once\tieDashed re~ |
re2. | \allowPageTurn
la,~ |
la,1 |
sold,2 la,4 |
mi2 la |
fad2. |
sol2 mi |
fa4 sol sol, |
do1~ |
do2. |
sol |
fa2 mi |
re la |
si la4 |
sol8 mi si4 si, |
mi2. |
fa |
\once\tieDashed mi1~ |
mi2 sold, |
la,2 fa,4 sol, |
do2. |
fa2 re4~ |
re mi2 |
la,2. |
fa |
re4 mi2 |
la,4 la4. sol8 fa mi |
re1 |
