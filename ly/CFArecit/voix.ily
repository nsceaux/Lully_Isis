<<
  \tag #'(recit2 basse) {
    \clef "vbasse" R1*4 |
    <>^\markup\character Jupiter
    la8 la16 la la8 mi fa fa16 la fa8\trill fa16 mi |
    mi4\trill r16 mi mi mi mi8 fa |
    sol4 sol8 sol fa4\trill fa8 mi |
    re4\trill r8 sol16 sol do'4 do'8 do' |
    la4\trill la8 la si4 si8 si |
    sold4\trill sold8
    \ffclef "vbas-dessus" <>^\markup\character Junon
    r16 mi'' mi'' mi'' si' si' sold'8 si' |
    mi'4. mi''8 do'' do'' do'' do'' |
    la'\trill la' la' sol' sol' fad' |
    fad'\trill r16 re'' re'' re'' re'' fad'! sol'8 sol'16 sol' la'8 si'16 do'' |
    si'4\trill si'8 sol' sol'16 sol' sol' sol' |
    do''8. do''16 re''8 mi'' re''8.\trill do''16 |
    do''4 r8 do''16 mi'' la'\trill r la' la' re''8^\markup\croche-pointee-double re'' |
    si'4\trill si'8
    \ffclef "vbasse" <>^\markup\character Jupiter
    sold16 sold la4 r8 la16 la |
    fad4\trill r8 re16 re sol8. sol16 \sugRythme { si'8 } sol4*1/2 \sugRythme { si'16 } sol8*1/2 \sugRythme { si'16 } fad8*1/2 |
    sol4
    \ffclef "vbas-dessus" <>^\markup\character Junon
    si'8 re'' si'4\trill si'8 si' |
    sol'4\trill sol'8 sol' sol'4 la'8 si' |
    do'' do'' r do''16 mi'' do''4\trill do''8 do'' |
    la'\trill la' r la'16 si' do''4 do''16 do'' do'' re'' |
    mi''4 sold'\trill r8 mi' si' si'16 do'' |
    re''4 si'8 si' mi''4 mi''8 si' |
    do'' do'' r mi'' la'4 la'8 la'16 la' |
    fad'8. re''16 re''8 si' sold'8.\trill si'16 |
    mi'4. mi''8 fa''16 mi'' re'' do'' si'8.\trill do''16 |
    la'4\trill la' r8 do'' |
    la'\trill la' la' la' fad'\trill fad'16 fad' si'8 si'16 si' |
    sold'4\trill
    \ffclef "vbasse" <>^\markup\character Jupiter
    r8 mi mi8. mi16 fad8 sold |
    la4. la8 si si si si |
    mi8. mi16 r8 do' mi\trill mi16 mi fa8 fa16 fa |
    re8\trill re r sol16 la si8 si16 do' |
    re'4. la8 do' do' do' si |
    si4\trill sol8 sol mi4\trill fa8 sol |
    la8 la16 si do'4. do'16 si |
    do'4
  }
  \tag #'recit1 {
    \ffclef "vbas-dessus" R1*4 R1 R2. R1*2 R1*3 R2. R1 R2.*2 R1*8 R1
    R1 R2. R1 R2. R1*4 R2. R1*2 R2. | r4
  }
>>
<<
  \tag #'(recit1 basse) {
    \tag #'basse \ffclef "vbas-dessus" <>^\markup\character Iris
    mi'4 mi' |
    fad'4. fad'8 fad'4 |
    sold'2 sold'8 sold' |
    la'2 la'8 si' |
    sold'2\trill r4 |
    r do'' do'' |
    la'4.\trill la'8 re''4 |
    si'2\trill sold'8 sold' |
    la'2 la'8 sold' |
    la'2
  }
  \tag #'recit2 {
    \ffclef "vhaute-contre" <>^\markup\character Mercure
    do'4 do' |
    la4. la8 re'4 |
    si2 mi'8 mi' |
    mi'2 mi'8 re' |
    mi'2 r4 |
    r mi' mi' |
    fa' re' si |
    sold2\trill si8 si |
    do'4.( si8) si do' |
    la2
  }
>>
<<
  \tag #'recit1 {
    r2 |
    R1*2 R2.*2 R1 R2. R1 R2. R1 R2. R1 R2.*2 R1 R1 R2.*4 R1 R1 R1
    R2.*6 R1*2
  }
  \tag #'(recit2 basse) {
    \ffclef "vbasse" <>^\markup\character Jupiter
    r4 r8 do' |
    fa4 r16 la la si do'8 do'16 do' dod'8 dod'16 re' |
    re'8. re'16
    \ffclef "vbas-dessus" <>^\markup\character Junon
    r8 la' fa' fa' fa' re' |
    la' re'' re'' re'' la' si' |
    do''4 do''16 do'' do'' << \new Voice { \voiceOne \sug do'' } { \voiceTwo mi' \oneVoice } >> fa'8 fa'16 mi' |
    mi'4\trill la'8 si' do''4 do''8 re'' |
    mi''4 si'8 si'16 si' do''8. do''16 |
    sold'8\trill sold' r mi''16 mi'' dod''8\trill dod''16 dod'' dod''8 re'' |
    re''8 re'' r la'16 la' la'8 si'16 do'' |
    si'4\trill r8 sol' do'' do'' re'' mi'' |
    la' r16 fa'' re''8.\trill re''16 re''8 mi'' |
    do''4
    \ffclef "vbasse" <>^\markup\character Jupiter
    r8 do sol8. sol16 sol8 sol |
    mi4\trill
    \ffclef "vbas-dessus" <>^\markup\character Junon
    r16 do'' do'' mi'' do''8.\trill si'16 |
    si'8\trill si' r sol'16 la' si'8\trill si'16 do'' |
    re''4 re''8 re'' mi''4 mi''8 mi'' |
    la'8\trill la' r la'16 si' do''8 do''16 si' la'8. sol'16 |
    fad'4\trill si'8 si'16 si' dod''8. red''16 |
    mi''8 r16 si'\trill fad'8. fad'16 fad'8 sol' |
    mi'4\trill sold'8 sold'16 sold' sold'8 sold' |
    la'4 la'8 la'16 la' la'8 sold' |
    si'4 si'
    \ffclef "vbasse" <>^\markup\character Jupiter
    r16 sold sold sold sold8 si |
    mi4 si8 do' re'4 re'8 si |
    do'4 do'8 mi fa16 fa fa fa re8. sol16 |
    do4. sol8 do'^\markup\croche-pointee-double do' |
    la4 la8 la fa fa |
    re\trill re mi mi mi8. mi16 la,4
    \ffclef "vbas-dessus" <>^\markup\character Iris
    r8 do'' do'' mi'' |
    la'4\trill la'8 r16 fa'' re''8.\trill re''16 |
    si'8\trill si' sold'8.\trill sold'16 sold'8 la' |
    la'4 r r2 |
    R1 |
  }
>>
