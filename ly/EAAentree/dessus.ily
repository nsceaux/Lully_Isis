\clef "dessus" \phrasingSlurDashed \phrasingSlurDown re''8\(( re'') |
re''( re'')\) sol''\( sol''\) fad''\(( fad'') fad''\)( sol'') |
mi''\(( mi'') mi''( mi'')\) re''\(( re'') re''\)( mi'') |
do''\(( do'') do''\)( re'') si'\(( si') si'\)( do'') |
\phrasingSlurUp la'\(( la') la'( la' la'4)\) la'8 si' |
\phrasingSlurDown do''\(( do'') do''( do'')\) \phrasingSlurUp sol'\(( sol') sol'\)( la') |
\phrasingSlurDown si'\(( si') si'\)( do'') re''\(( re'') re''\)( mi'') |
fad''\(( fad'') fad''\)( sol'') la''( la'') fad''( fad'') |
sol''\(( sol'') sol''( sol'') sol''\) la'' fad''8.\trill sol''16 |
sol''8\(( sol'') sol''( sol'' sol'')\) re''\( re''\)( mi'') |
do''\(( do'') do''\)( re'') si'\(( si') si'\)( do'') |
la'\( la' la'\) re'' sol'( sol') do''\(( do'') |
do''( do'')\) si'( si') la'4.\trill sol'8 |
\phrasingSlurUp sol'\(( sol') sol' sol' sol'\) \phrasingSlurDown re''\( re''\)( mi'') |
\phrasingSlurUp sol'\(( sol') sol'( sol' sol'4)\)
