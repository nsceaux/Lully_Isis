I -- ris est i -- cy- bas, & Ju -- non el -- le- mê -- me,
pour -- roit vous sui -- vre dans ces lieux.

Pour la Nym -- phe que j’ai -- me,
je crains ses trans -- ports fu -- ri -- eux.

Sa ven -- gean -- ce se -- roit fu -- nes -- te,
si vôtre a -- mour é -- toit sur -- pris.

Va, prens soin d’ar -- rê -- ter I -- ris,
mon a -- mour pren -- dra soin du res -- te.
