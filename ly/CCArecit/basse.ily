\clef "basse" \once\tieDashed sol,1~ |
sol,2 \once\tieDashed sol~ |
sol mi |
fad sol8 fad mi4 |
re2 do4. si,8 |
la,2 mi8 fa16 re mi8 mi, |
la,2 la4. sol8 |
fad2 mi4 la |
re2 si, |
do4 dod re8. sol,16 re,4 |
sol,2. |
