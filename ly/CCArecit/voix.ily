\clef "vhaute-contre" <>^\markup\character Mercure
R1 |
r4 r8 re' si si si si |
sol4 sol8 sol do'4 do'8 do' |
la4 la16 la si do' si8. si16 si8 dod' |
re'4
\ffclef "vbasse" <>^\markup\character Jupiter
re8 re la4 la8 si |
do'4 do'8. mi'16 sold8\trill la16 re mi8 mi16 mi |
la,4
\ffclef "vhaute-contre" <>^\markup\character Mercure
r8 mi'16 mi' dod'8 dod'16 dod' dod'8 mi' |
la4 la16 re' re' re' mi'8 mi' fad' sol' |
fad'4
\ffclef "vbasse" <>^\markup\character Jupiter
re'8 r16 fad sol8 sol16 sol la8 si |
mi4\trill r8 mi16 la fad8\trill fad16 sol re8. re16 |
sol,4 sol, r |
