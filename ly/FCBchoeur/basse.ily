\clef "basse"
<<
  \tag #'basse { R1*7 | r2 r4 }
  \tag #'(basse-continue bc-part) {
    \tag #'bc-part <>^"[B.C.]"
    do4\repeatTie do' do'4. do'8 |
    sol4. sol8 sol4 sol |
    la2. fa4 |
    sol2 sol4 sol |
    do'2. do'4 |
    si4. si8 si4. do'8 |
    la2. re'4 |
    sol2.
  }
>> \tag #'bc-part <>^"[Tous]" sol4 |
mi2. mi4 |
fa4. fa8 fa4 mi |
re2. do4 |
do <<
  \tag #'basse { r4 r2 | R1*2 | r2 r4 }
  \tag #'(basse-continue bc-part) {
    \tag #'bc-part <>^"[B.C.]"
    do' do'4. la8 |
    sib4 sib sib4. sol8 |
    re'2. re'4 |
    sol2 sol4
  }
>> \tag #'bc-part <>^"[Tous]" sol4 |
mi2. mi4 |
la la la fa |
sol2. sol4 |
do4. do8 do4 do |
fa4. fa8 fa4. re8 |
la2. sol4 |
fa fa fa fa |
do'4. do'8 do'4. do8 |
<<
  \tag #'basse { sol2 r | R1*3 | r2 r4 }
  \tag #'(basse-continue bc-part) {
    sol2.*2/3 \tag #'bc-part <>^"[B.C.]" s4 sol4 |
    do'2. do'4 |
    si4. si8 si4. do'8 |
    la2. re'4 |
    sol2.
  }
>> \tag #'bc-part <>^"[Tous]" sol4 |
mi2. mi4 |
la la la fa |
sol2. sol4 |
do4 <<
  \tag #'basse { r4 r2 | R1*2 | r4 }
  \tag #'(basse-continue bc-part) {
    \tag #'bc-part <>^"[B.C.]"
    do'8 do' do'4. do'8 |
    si2 si4 si |
    do'1 |
    sol4
  }
>> \tag #'bc-part <>^"[Tous]" sol4 sol fa |
mi mi mi mi |
fa fa fa mi |
re re re re |
mi <<
  \tag #'basse { r4 r2 | R1*2 | r4 }
  \tag #'(basse-continue bc-part) {
    \tag #'bc-part <>^"[B.C.]"
    mi'8 mi' mi'4 mi' |
    la2 la4 la |
    re'1 |
    sol4
  }
>> \tag #'bc-part <>^"[Tous]" sol4 sol fa |
mi mi mi re |
do do do si, |
la, la, re re |
sol, <<
  \tag #'basse { r4 r2 | R1*2 | r4 }
  \tag #'(basse-continue bc-part) {
    \tag #'bc-part <>^"[B.C.]"
    sol8 sol sol4 sol |
    do'2 do'4 do' |
    do'2 si |
    do'4
  }
>> \tag #'bc-part <>^"[Tous]" do'4 do' sib |
la la mi mi |
fa fa fa mi |
re4. do8 si,4 do |
sol, <<
  \tag #'basse { r4 r2 | R1*2 | r4 }
  \tag #'(basse-continue bc-part) {
    \tag #'bc-part <>^"[B.C.]"
    sol8 sol sol4. sol8 |
    do'2 do'4 do' |
    do'2 si |
    do'4
  }
>> \tag #'bc-part <>^"[Tous]" do'4 do' sib |
la la mi mi |
fa fa fa mi |
re do sol,2 |
<<
  \tag #'basse { do2 r | R4.*35 | r8 r }
  \tag #'(basse-continue bc-part) {
    do1*1/2 \tag #'bc-part <>^"[B.C.]" s2 | \allowPageTurn
    do'4. |
    mi8 fa re |
    sol4 sol16 fa |
    mi4 mi8 |
    fa4 fa8 |
    sol4. |
    la8 si do' |
    sol8 sol,4 |
    do4 do8 |
    sol sol mi |
    fa16 mi fa sol la si |
    do'8 do' la |
    re' re' re |
    sol4 sol8 |
    la4 la8 |
    si4 si8 |
    do'4 do8 |
    sol sol fa |
    mi re do |
    sol sol,4 |
    do8 do re |
    mi mi do |
    sol8 fa16 mi re do |
    si,8. do16 re si, |
    do8 si, do |
    sol,8. la,16 si, sol, |
    do si, do re mi fa |
    sol4 sol8 |
    la4 la8 |
    si4 si8 |
    do'4 do8 |
    fa4 re8 |
    sol sol fa |
    mi re do |
    sol sol,4 |
    do4
  }
>> \tag #'bc-part <>^"[Tous]" do'8 |
do'4. |
do'8 si do' |
sol4 sol8 |
re4 re8 |
la4 la8 |
mi4. |
fa8 fa re |
sol sol,4 |
do4 do8 |
sol sol mi |
fa16 mi fa sol la si |
do'8 do' la |
re'4 re8 |
sol4 mi8 |
fa4 fa8 |
re4 re8 |
mi mi re |
do si, la, |
mi4. |
la,8 la sol |
fa fa re |
\mergeDifferentlyDottedOn
<<
  \tag #'(basse bc-part) \new Voice {
    \tag #'bc-part { <>^"[Basses]" \voiceOne }
    la4.~ |
    la~ |
    la~ |
    la~ |
    la |
    la4
  }
  \tag #'(basse-continue bc-part) \new Voice {
    \tag #'bc-part { <>_"[B.C.]" \voiceTwo }
    la4 re8 |
    la,16 si, dod8 la, |
    re16 mi fa8 re |
    la8 fa re |
    la16 sol fa mi re8 |
    la,4
  }
>> \tag #'bc-part <>^"[Tous]" la8 |
re'4 re'8 |
si4. |
do'8 si do' |
sol4 sol8 |
re4 re8 |
la4 la8 |
mi4. |
fa8 fa re |
sol sol,4 |
do4 do8 |
sol sol mi |
fa16 mi fa sol la si |
do'8 do' la |
re'4 re8 |
mi4 mi8 |
fad4 fad8 |
sol4 sol8 |
re re' do' |
si la sol |
re4. |
sol4 sol8 |
do'4 la8 |
sib4 sib8 |
sol4 sol8 |
do' do' sib |
la sol fa |
do4. |
fa8 fa sol |
la la fa |
do'4 fa8 |
do16 re mi8 do |
fa16 sol la8 fa |
do'16 sib la sol fa8 |
do16 re mi do fa8 |
do'4 do8 |
fa4 fa8 |
re4 re8 |
sol4 sol8 |
mi4 mi8 |
la la la |
si si do' |
sol sol,4 |
do8 do re |
mi mi do |
<<
  \tag #'(basse bc-part) \new Voice {
    \tag #'bc-part { <>^"[Basses]" \voiceOne }
    sol4.~ |
    sol~ |
    sol~ |
    sol~ |
    sol |
    sol4
  }
  \tag #'(basse-continue bc-part) \new Voice {
    \tag #'bc-part { <>_"[B.C.]" \voiceTwo }
    sol4 do8 |
    sol,16 fa, sol, la, si, sol, |
    do re mi8 do |
    sol mi do |
    sol16 fa mi re do8 |
    sol,4
  }
>> \tag #'bc-part <>^"[Tous]" sol8 |
la4 la8 |
si4 si8 |
do'4 do8 |
fa4 re8 |
sol8 sol fa |
mi re do |
sol sol,4 |
do8 do re |
mi mi do |
<<
  \tag #'basse {
    sol4.~ |
    sol~ |
    sol~ |
    sol~ |
    sol |
    sol4
  }
  \tag #'(basse-continue bc-part) {
    \tag #'bc-part <>^"[B.C.]"
    sol4 do8 |
    sol,16 fa, sol, la, si, sol, |
    do re mi8 do |
    sol mi do |
    sol16 fa mi re do8 |
    sol,4
  }
>> \tag #'bc-part <>^"[Tous]" sol8 |
la4 la8 |
si4 si8 |
do'4 do8 |
fa4 re8 |
sol sol fa |
mi re do |
sol sol,4 |
do8\fermata
