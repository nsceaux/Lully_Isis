\tag #'(choeur1 basse) {
  Ve -- nez, ve -- nez, Di -- vi -- ni -- té nou -- vel -- le,
  ve -- nez, ve -- nez, Di -- vi -- ni -- té nou -- vel -- le.
}
\tag #'(choeur2-d choeur2-hc choeur2-t choeur2-b basse) {
  I -- sis, I -- sis, tour -- nez sur nous vos yeux.
}
\tag #'(choeur1 basse) {
  Ve -- nez, ve -- nez, Di -- vi -- ni -- té nou -- vel -- le.
}
\tag #'(choeur2-d choeur2-hc choeur2-t choeur2-b basse) {
  I -- sis, I -- sis, tour -- nez sur nous vos yeux,
  voy -- ez l’ar -- deur de nô -- tre ze -- le.
  Voy -- ez l’ar -- deur de nô -- tre ze -- le.
}
\tag #'(choeur1 basse) {
  Ve -- nez, ve -- nez, Di -- vi -- ni -- té nou -- vel -- le.
}
\tag #'(choeur2-d choeur2-hc choeur2-t choeur2-b basse) {
  I -- sis, I -- sis, tour -- nez sur nous vos yeux.
}
\tag #'(choeur1 basse) {
  La cé -- les -- te Cour vous ap -- pel -- le.
}
\tag #'(choeur2-d choeur2-hc choeur2-t choeur2-b basse) {
  Tout vous ré -- vé -- re dans ces lieux,
  tout vous ré -- vé -- re dans ces lieux.
}
\tag #'(choeur1 basse) {
  La cé -- les -- te Cour vous ap -- pel -- le.
}
\tag #'(choeur2-d choeur2-hc choeur2-t choeur2-b basse) {
  Tout vous ré -- vé -- re dans ces lieux,
  tout vous ré -- vé -- re dans ces lieux.
}
\tag #'(choeur1 basse) {
  La cé -- les -- te Cour vous ap -- pel -- le.
}
\tag #'(choeur2-d choeur2-hc choeur2-t choeur2-b basse) {
  Tout vous ré -- vé -- re dans ces lieux,
  tout vous ré -- vé -- re dans ces lieux.
}
\tag #'(choeur1 basse) {
  La cé -- les -- te Cour vous ap -- pel -- le.
}
\tag #'(choeur2-d choeur2-hc choeur2-t choeur2-b basse) {
  Tout vous ré -- vé -- re dans ces lieux,
  tout vous ré -- vé -- re dans ces lieux.
}
\tag #'(junon jupiter basse) {
  I -- sis est im -- mor -- tel -- le,
  I -- sis, I -- sis est im -- mor -- tel -- le,
  I -- sis va bril -- ler, __ va bril -- ler dans ces lieux,
  I -- sis joü -- it a -- vec les Dieux,
  d’u -- ne gloire é -- ter -- nel -- le.
  \tag #'jupiter { D’u -- ne gloire é -- ter -- nel -- le. I -- sis, }
  \tag #'(junon basse) {
    I -- sis joü -- it a -- vec les Dieux,
    d’u -- ne gloire é -- ter -- nel -- le.
  }
  I -- sis joü -- it a -- vec les Dieux,
  d’u -- ne gloire é -- ter -- nel -- le.
}
%%
\tag #'(choeur2-d choeur2-hc choeur2-t choeur2-b basse) {
  I -- sis est im -- mor -- tel -- le,
  I -- sis, I -- sis est im -- mor -- tel -- le,
  \tag #'(choeur2-d choeur2-hc basse) {
    I -- sis, I -- sis va bril -- ler dans les Cieux,
  }
  \tag #'(choeur2-t choeur2-b) {
    I -- sis va bril -- ler __ dans les Cieux,
  }
  I -- sis joü -- it a -- vec les Dieux,
  d’u -- ne gloire é -- ter -- nel -- le,
  \tag #'(choeur2-d choeur2-hc choeur2-t basse) {
    I -- sis, I -- sis joü -- it a -- vec les Dieux,
  }
  d’u -- ne gloire é -- ter -- nel -- le.
  \tag #'(choeur2-b) { I -- sis, }
  I -- sis est im -- mor -- tel -- le,
  I -- sis, I -- sis est im -- mor -- tel -- le,
  \tag #'(choeur2-d choeur2-hc basse) {
    I -- sis, I -- sis va bril -- ler dans les cieux,
  }
  \tag #'(choeur2-t choeur2-b) {
    I -- sis va bril -- ler __ dans les cieux,
  }
  I -- sis joü -- it a -- vec les Dieux,
  d’u -- ne gloire é -- ter -- nel -- le.
  I -- sis joü -- it a -- vec les Dieux,
  d’u -- ne gloire é -- ter -- nel -- le.
  \tag #'(choeur2-d choeur2-hc choeur2-t basse) {
    I -- sis joü -- it a -- vec les Dieux,
  }
  d’u -- ne gloire é -- ter -- nel -- le.
  \tag #'(choeur2-b) { I -- sis, }
  I -- sis joü -- it a -- vec les Dieux,
  d’u -- ne gloire é -- ter -- nel -- le.
  \tag #'(choeur2-d choeur2-hc choeur2-t basse) {
    I -- sis joü -- it a -- vec les Dieux,
  }
  d’u -- ne gloire é -- ter -- nel -- le.
  \tag #'(choeur2-b) { I -- sis, }
  I -- sis joü -- it a -- vec les Dieux,
  d’u -- ne gloire é -- ter -- nel -- le.
  \tag #'(choeur2-d choeur2-hc choeur2-t basse) {
    I -- sis joü -- it a -- vec les Dieux,
  }
  d’u -- ne gloire é -- ter -- nel -- le.
  \tag #'(choeur2-b) { I -- sis, }
  I -- sis joü -- it a -- vec les Dieux,
  d’u -- ne gloire é -- ter -- nel -- le.
}
