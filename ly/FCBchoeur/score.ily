\score {
  <<
    \new StaffGroup \with { \haraKiriFirst } <<
      \new Staff << { \noHaraKiri s1*59 \revertNoHaraKiri }
        \global \includeNotes "dessus"
      >>
      \new Staff << { \noHaraKiri s1*59 \revertNoHaraKiri }
        \global \includeNotes "haute-contre"
      >>
      \new Staff << { \noHaraKiri s1*59 \revertNoHaraKiri }
        \global \includeNotes "taille"
      >>
      \new Staff << { \noHaraKiri s1*59 \revertNoHaraKiri }
        \global \includeNotes "quinte"
      >>
      \new Staff << { \startHaraKiri s1*60 \stopHaraKiri }
        \global \keepWithTag #'basse \includeNotes "basse"
      >>
    >>
    \new ChoirStaff \with { \haraKiri } <<
      \new Staff \withLyrics <<
        { \noHaraKiri s1*59 \revertNoHaraKiri }
        \global \keepWithTag #'choeur1-d1 \includeNotes "voix"
      >> \keepWithTag #'choeur1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { \noHaraKiri s1*59 \revertNoHaraKiri }
        \global \keepWithTag #'choeur1-d2 \includeNotes "voix"
      >> \keepWithTag #'choeur1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { \noHaraKiri s1*59 \revertNoHaraKiri }
        \global \keepWithTag #'choeur1-hc \includeNotes "voix"
      >> \keepWithTag #'choeur1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { \noHaraKiri s1*59 \revertNoHaraKiri }
        \global \keepWithTag #'choeur1-b \includeNotes "voix"
      >> \keepWithTag #'choeur1 \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \haraKiri } <<
      \new Staff \withLyrics <<
        { \noHaraKiri s1*59 \revertNoHaraKiri }
        \global \keepWithTag #'choeur2-d \includeNotes "voix"
      >> \keepWithTag #'choeur2-d \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { \noHaraKiri s1*59 \revertNoHaraKiri }
        \global \keepWithTag #'choeur2-hc \includeNotes "voix"
      >> \keepWithTag #'choeur2-hc \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { \noHaraKiri s1*59 \revertNoHaraKiri }
        \global \keepWithTag #'choeur2-t \includeNotes "voix"
      >> \keepWithTag #'choeur2-t \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { \noHaraKiri s1*59 \revertNoHaraKiri }
        \global \keepWithTag #'choeur2-b \includeNotes "voix"
      >> \keepWithTag #'choeur2-b \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'junon \includeNotes "voix"
      >> \keepWithTag #'junon \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'jupiter \includeNotes "voix"
      >> \keepWithTag #'jupiter \includeLyrics "paroles"
      \new Staff <<
        \global \keepWithTag #'basse-continue \includeNotes "basse"
        \includeFigures "chiffres"
        \origLayout {
          s1*7\pageBreak
          s1*7\pageBreak
          s1*7\pageBreak
          s1*7\pageBreak
          s1*7\pageBreak
          s1*6\pageBreak
          s1*5\pageBreak
          s1*6 s4 \bar "" \pageBreak
          s2. s1*4 s2 \bar "" \pageBreak
          s2 s1*2 s4.*3 s4 \bar "" \pageBreak
          s8 s4.*7\break s4.*7\break s4.*8\break s4.*9 s4 \bar "" \pageBreak
          s8 s4.*8 s4 \bar "" \pageBreak
          s8 s4.*7 s8 \bar "" \pageBreak
          s4 s4.*8\pageBreak
          s4.*10\pageBreak
          s4.*8\pageBreak
          s4.*9\pageBreak
          s4.*9\pageBreak
          s4.*11\pageBreak
          s4.*9\pageBreak
          s4.*9\pageBreak
        }
        \modVersion {
          s1*60\pageBreak
          s4.*9\break s4.*9\break s4.*9\break s4.*8\pageBreak
        }
      >>
    >>
  >>
  \layout { indent = 0 }
  \midi { }
}
