<<
  %% Chœur des divinités
  \tag #'(choeur1-d1 basse) {
    \clef "vdessus" <>^\markup\character Chœur des Divinitez
    r4 mi'' mi''4. mi''8 |
    re''4.\trill re''8 re''4. sol''8 |
    mi''2.\trill mi''4 |
    re''2\trill re''4. re''8 |
    mi''2. mi''4 |
    re''4.\trill re''8 re''4. mi''8 |
    do''2.\trill re''4 |
    si'2\trill si'4 <<
      \tag #'basse { s4 s1*3 s4 }
      \tag #'choeur1-d1 { r4 | R1*3 | r4 }
    >> mi''4 mi''4. fa''8 |
    re''4.\trill re''8 re''4. sol''8 |
    fad''2.\trill fad''4 |
    sol''2 sol''4 <<
      \tag #'basse { s4 s1*8 s2. }
      \tag #'choeur1-d1 { r4 | R1*8 | r2 r4 }
    >> re''4 |
    mi''2. mi''4 |
    re''4.\trill re''8 re''4. mi''8 |
    do''2. re''4 |
    si'2\trill si'4 <<
      \tag #'basse { s4 s1*3 s4 }
      \tag #'choeur1-d1 { r4 | R1*3 | r4 }
    >> do''8 do'' do''4. do''8 |
    re''2 re''4 re''8[ mi''] |
    mi''1\trill |
    re''4 <<
      \tag #'basse { s2. s1*3 s4 }
      \tag #'choeur1-d1 { r4 r2 | R1*3 | r4 }
    >> si'8 si' si'4. mi''8 |
    dod''2\trill dod''4 dod'' |
    re''1 |
    re''4 <<
      \tag #'basse { s2. s1*3 s4 }
      \tag #'choeur1-d1 { r4 r2 | R1*3 | r4 }
    >> re''8 re'' re''4 re'' |
    mi''2 mi''4 mi'' |
    fa''1 |
    mi''4\trill <<
      \tag #'basse { s2. s1*3 s4 }
      \tag #'choeur1-d1 { r4 r2 | R1*3 | r4 }
    >> re''8 re'' re''4. re''8 |
    mi''2 mi''4 mi'' |
    fa''1 |
    mi''4 <<
      \tag #'basse { s2. s1*4 }
      \tag #'choeur1-d1 { r4 r2 | R1*4 | R4.*135 r8 }
    >>
  }
  \tag #'choeur1-d2 {
    \clef "vbas-dessus" r4 do'' do''4. do''8 |
    si'4.\trill si'8 si'4. si'8 |
    do''2. re''4 |
    si'2\trill si'4. si'8 |
    do''2. do''4 |
    sol' sol' sol' sol' |
    sol'2. fad'4 |
    sol'2 sol'4 r |
    R1*3 |
    r4 do'' do''4. do''8 |
    sib'4.\trill sib'8 sib'4. sib'8 |
    la'2.\trill re''4 |
    si'2\trill si'4 r |
    R1*8 |
    r2 r4 si' |
    do''2. do''4 |
    sol'4. sol'8 sol'4. sol'8 |
    sol'2. fad'4 |
    sol'2 sol'4 r |
    R1*3 |
    r4 mi''8 mi'' mi''4. mi''8 |
    re''2 re''4 re'' |
    do''1 |
    si'4\trill r r2 |
    R1*3 |
    r4 sold'8 sold' sold'4.\trill sold'8 |
    la'2 la'4 la' |
    \footnoteHere #'(0 . 0) \markup { Matériel 1677 : \italic si }
    la'1 |
    si'4 r r2 |
    R1*3 |
    r4 si'8 si' si'4 si' |
    do''2 do''4 do'' |
    re''1 |
    sol'4 r r2 |
    R1*3 |
    r4 si'8 si' si'4. si'8 |
    do''2 do''4 do'' |
    re''1 |
    sol'4 r r2 |
    R1*4 |
    R4.*135 r8
  }
  \tag #'choeur1-hc {
    \clef "vhaute-contre" r4 sol' sol'4. sol'8 |
    sol'4. re'8 re'4. re'8 |
    la'2. la'4 |
    sol'2 sol'4. sol'8 |
    sol'2. do'4 |
    re'4. re'8 re'4. do'8 |
    mi'2. re'4 |
    re'2 re'4 r |
    R1*3 |
    r4 sol' sol'4. la'8 |
    fa'4. re'8 re'4. re'8 |
    re'2. re'4 |
    re'2 re'4 r |
    R1*8 |
    r2 r4 sol'4 |
    sol'2. do'4 |
    re'4. re'8 re'4. re'8 |
    mi'2. re'4 |
    re'2 re'4 r |
    R1*3 |
    r4 sol'8 sol' sol'4. sol'8 |
    sol'2 sol'4 sol' |
    sol'1 |
    sol'4 r r2 |
    R1*3 |
    r4 mi'8 mi' mi'4. mi'8 |
    mi'2 mi'4 mi' |
    fad'1 |
    sol'4 r r2 |
    R1*3 |
    r4 sol'8 sol' sol'4. sol'8 |
    sol'2 sol'4 sol' |
    sol'1 |
    sol'4 r r2 |
    R1*3 |
    r4 sol'8 sol' sol'4. sol'8 |
    sol'2 sol'4 sol' |
    sol'1 |
    sol'4 r r2 |
    R1*4 |
    R4.*135 r8
  }
  \tag #'choeur1-b {
    \clef "vbasse" r4 do' do'4. do'8 |
    sol4. sol8 sol4. sol8 |
    la2. fa4 |
    sol2 sol4. sol8 |
    do'2. do'4 |
    si4.\trill si8 si4. do'8 |
    la2. re'4 |
    sol2 sol4 r |
    R1*3 |
    r4 do' do'4. la8 |
    sib4 sib sib4. sol8 |
    re'2. re'4 |
    sol2 sol4 r |
    R1*8 |
    r2 r4 sol |
    do'2. do'4 |
    si4.\trill si8 si4. do'8 |
    la2. re'4 |
    sol2 sol4 r |
    R1*3 |
    r4 do'8 do' do'4. do'8 |
    si2 si4 si |
    do'1 |
    sol4 r r2 |
    R1*3 |
    r4 mi'8 mi' mi'4. mi'8 |
    la2 la4 la |
    re'1 |
    sol4 r r2 |
    R1*3 |
    r4 sol8 sol sol4. sol8 |
    do'2 do'4 do' |
    do'2( si) |
    do'4 r r2 |
    R1*3 |
    r4 sol8 sol sol4. sol8 |
    do'2 do'4 do' |
    do'2( si)\trill |
    do'4 r r2 |
    R1*4 |
    R4.*135 r8
  }
  %% Chœur des Egyptiens
  \tag #'(choeur2-d basse) {
    <<
      \tag #'basse { s1*7 s2. }
      \tag #'choeur2-d {
        \clef "vdessus" <>^\markup\character Chœur des Egyptiens
        R1*7 |
        r2 r4
      }
    >> si' |
    do''2. do''4 |
    la'4.\trill la'8 si'4 do'' |
    si'2.\trill do''4 |
    do'' <<
      \tag #'basse { s2. s1*2 s2. }
      \tag #'choeur2-d { r4 r2 | R1*2 | r2 r4 }
    >> re''4 |
    mi''2. mi''4 |
    do''\trill do'' do'' fa'' |
    re''2.\trill do''4 |
    do''4. mi''8 mi''4 mi'' |
    fa''4. fa''8 fa''4 fa'' |
    mi''2 mi'' |
    r4 fa'' fa'' fa'' |
    mi''4.\trill mi''8 mi''4. fa''8 |
    re''2\trill re''4 <<
      \tag #'basse { s4 s1*3 s2. }
      \tag #'choeur2-d { r4 | R1*3 | r2 r4 }
    >> re''4 |
    mi''2. mi''4 |
    do''\trill do'' do'' fa'' |
    re''2.\trill do''4 |
    do'' <<
      \tag #'basse { s2. s1*2 s4 }
      \tag #'choeur2-d { r4 r2 | R1*2 | r4 }
    >> re''4 re'' re'' |
    mi''4 mi'' mi'' mi'' |
    do'' do'' do'' do'' |
    re'' re'' re'' re'' |
    si'\trill <<
      \tag #'basse { s2. s1*2 s4 }
      \tag #'choeur2-d { r4 r2 | R1*2 | r4 }
    >> si'4 si' si' |
    do'' do'' do'' re'' |
    mi'' mi'' mi'' re'' |
    do'' do'' la'4. re''8 |
    si'4\trill <<
      \tag #'basse { s2. s1*2 s4 }
      \tag #'choeur2-d { r4 r2 | R1*2 | r4 }
    >> mi''4 mi'' mi'' |
    fa'' fa'' sol'' sol'' |
    do'' do'' re'' mi'' |
    fa'' fa'' fa'' mi'' |
    re''\trill <<
      \tag #'basse { s2. s1*2 s4 }
      \tag #'choeur2-d { r4 r2 | R1*2 | r4 }
    >> mi''4 mi'' mi'' |
    fa'' fa'' sol'' sol'' |
    do'' do'' do'' do'' |
    fa'' mi'' re''4.\trill do''8 |
    do''2 <<
      \tag #'basse { s2 s4.*35 s4 }
      \tag #'choeur2-d { r2 R4.*35 | r8 r }
    >> <>^\markup\character Chœurs mi''8 |
    mi''4. |
    mi''8 fa'' mi'' |
    re''4\trill re''8 |
    r r fa'' |
    mi''4\trill fa''8 |
    sol''4. |
    do''8 do'' fa'' |
    re''4.\trill |
    do''4 mi''8 |
    re''4\trill sol''8 |
    do'' do'' fa'' |
    mi'' mi'' la'' |
    fad''4\trill re''8 |
    si'4\trill mi''8 |
    do''4 do''8 |
    fa''16[ sol'' fa'' mi''] re''[ do''] |
    si'8\trill si' si' |
    mi'' re'' do'' |
    si'4.\trill |
    la'4 mi''8 |
    fa''4 fa''8 |
    dod''4\trill re''8 |
    mi''4. |
    re''8 re''8. mi''16 |
    dod''8 re'' re'' |
    mi'' mi'' fa'' |
    mi''4.\trill |
    re''4 la'8 |
    re''4. |
    mi''8 fa'' mi'' |
    re''4\trill re''8 |
    r r fa'' |
    mi''4\trill fa''8 |
    sol''4. |
    do''8 do'' fa'' |
    re''4.\trill |
    do''4 mi''8 |
    re''4\trill sol''8 |
    do'' do'' fa'' |
    mi''\trill mi'' la'' |
    fad''4\trill re''8 |
    re''4 do''8 |
    do''4 do''8 |
    si'4 si'8 |
    la'\trill la' la' |
    re'' do'' si' |
    la'4.\trill |
    sol'4 re''8 |
    mi''4 fa''8 |
    re''4 re''8 |
    sol''4 sol''8 |
    mi'' mi'' mi'' |
    fa'' mi'' fa'' |
    fa''( mi''4)\trill |
    fa''4. |
    r8 r la' |
    sol'4\trill la'8 |
    sib'4. |
    la'8 la'8. sib'16 |
    sol'8\trill do'' do'' |
    sib'\trill sib' la' |
    la'8( sol'4\trill) |
    fa'4 do''8 |
    fa''4 fa''8 |
    re''4\trill re''8 |
    sol''4 sol''8 |
    mi''\trill mi'' do'' |
    re'' re'' do'' |
    do''( si'4\trill) |
    do''4. |
    r8 r mi'' |
    re''4\trill mi''8 |
    fa''4. |
    mi''8 mi''8. fa''16 |
    re''8\trill sol'' sol'' |
    fa''\trill fa'' mi'' |
    mi''( re''4)\trill |
    do''4 do''8 |
    re''4 re''8 |
    mi''4 mi''8 |
    fa''4 fa''8 |
    re''8\trill re'' re'' |
    sol'' fa'' mi'' |
    mi''( re''4)\trill |
    do''4. |
    r8 r mi'' |
    re''4\trill mi''8 |
    fa''4. |
    mi''8 mi''8. fa''16 |
    re''8\trill sol'' sol'' |
    fa''\trill fa'' mi'' |
    mi''( re''4)\trill |
    do''4 do''8 |
    re''4 re''8 |
    mi''4 mi''8 |
    fa''4 fa''8 |
    re''\trill re'' re'' |
    sol'' fa'' mi'' |
    mi''( re''4)\trill |
    do''8\fermata
  }
  \tag #'choeur2-hc {
    \clef "vhaute-contre" R1*7 |
    r2 r4 re' |
    mi'2. mi'4 |
    do'4. do'8 re'4 mi' |
    fa'2. mi'4 |
    mi'\trill r r2 |
    R1*2 |
    r2 r4 sol' |
    sol'2. sol'4 |
    mi' mi' mi' la' |
    sol'2. << { \voiceOne sol'4 \oneVoice } \new Voice { \voiceTwo \sug fa' } >> |
    mi'4.\trill sol'8 sol'4 sol' |
    la'4. la'8 la'4 la' |
    la'2 la' |
    r4 la' la' la' |
    sol'4. sol'8 sol'4. sol'8 |
    sol'2 sol'4 r |
    R1*3 |
    r2 r4 sol' |
    sol'2. sol'4 |
    mi' mi' mi' la' |
    sol'2. fa'4 |
    mi'\trill r r2 |
    R1*2 |
    r4 sol' sol' sol' |
    sol'4 sol' sol' sol' |
    fa' fa' fa' fa' |
    fa' fa' fa' fa' |
    mi' r r2 |
    R1*2 |
    r4 re' re' re' |
    mi' mi' mi' fa' |
    sol' sol' sol' sol' |
    sol' sol' fad'4.\trill sol'8 |
    sol'4 r r2 |
    R1*2 |
    r4 sol' sol' sol' |
    la' la' sol' sol' |
    fa' fa' fa' sol' |
    la' la' sol' sol' |
    sol' r r2 |
    R1*2 |
    r4 sol' sol' sol' |
    la' la' sol' sol' |
    fa' fa' fa' do' |
    re' sol sol'4. fa'8 |
    mi'2\trill r |
    R4.*35 |
    r8 r sol' |
    sol'4. |
    sol'8 sol' sol' |
    sol'4 sol'8 |
    r r la' |
    la'4 la'8 |
    sol'4. |
    fa'8 la' la' |
    sol'4. |
    mi'4 sol'8 |
    sol'4 sol'8 |
    la' la' la' |
    sol' sol' la' |
    la'4 fad'8 |
    sol'4 sol'8 |
    fa'4 fa'8 |
    << { \voiceOne la'4 la'8 \oneVoice } \new Voice \sugNotes { \voiceTwo fa'4 fa'8 } >> |
    mi' mi' mi' |
    mi' mi' mi' |
    mi'4. |
    dod'4 dod'8 |
    re'4 re'8 |
    mi'4 fa'8 |
    sol'4. |
    fa'8 fa'8. sol'16 |
    mi'8\trill la' la' |
    la' la' la' |
    la'4. |
    fad'4 fad'8 |
    sol'4. |
    sol'8 sol' sol' |
    sol'4 sol'8 |
    r r la' |
    la'4 la'8 |
    sol'4. |
    fa'8 la' la' |
    sol'4. |
    mi'4 sol'8 |
    sol'4 sol'8 |
    la' la' la' |
    sol' sol' la' |
    la'4 fad'8 |
    sol'4 sol'8 |
    la'4 la'8 |
    sol'4 sol'8 |
    fad' fad' fad' |
    sol' fad' sol' |
    sol'( fad'4) |
    sol'4 sol'8 |
    sol'4 la'8 |
    fa'4 fa'8 |
    sib'4 sib'8 |
    sol' sol' sol' |
    la'8 sib' la' |
    la'( sol'4)\trill |
    fa'4. |
    r8 r fa' |
    mi'4 fa'8 |
    sol'4. |
    fa'8 fa'8. sol'16 |
    mi'8 la' la' |
    sol' sol' fa' |
    fa'( mi'4)\trill |
    fa' la'8 |
    la'4 la'8 |
    sol'4 sol'8 |
    sol'4 sol'8 |
    la' la' la' |
    sol' sol' sol' |
    sol'4. |
    mi' |
    r8 r sol' |
    sol'4 do'8 |
    si4.\trill |
    do'8 sol' sol' |
    sol' do' do' |
    re' re' mi' |
    sol'8.[ fa'16 mi' fa'] |
    mi'4\trill la'8 |
    la'4 sol'8 |
    sol'4 sol'8 |
    la'4 la'8 |
    sol'8 sol' sol' |
    sol' sol' sol' |
    sol'4. |
    mi' |
    r8 r sol' |
    sol'4 do'8 |
    si4.\trill |
    do'8 sol' sol' |
    sol' do' do' |
    re' re' mi' |
    sol'8.[ fa'16 mi' fa'] |
    mi'4\trill la'8 |
    la'4 sol'8 |
    sol'4 sol'8 |
    la'4 la'8 |
    sol' sol' sol' |
    sol' sol' sol' |
    sol'4. |
    mi'8\fermata
  }
  \tag #'choeur2-t {
    \clef "vtaille" R1*7 |
    r2 r4 sol |
    sol2. sol4 |
    la4. fa'8 re'4 do' |
    re'2. sol4 |
    sol r r2 |
    R1*2 |
    r2 r4 si |
    do'2. do'4 |
    do' do' do' re' |
    si2.\trill do'4 |
    do'4. do'8 do'4 do' |
    do'4. do'8 do'4 re' |
    do'2 do' |
    r4 do' do' do' |
    do'4. do'8 do'4 do' |
    si2\trill si4 r |
    R1*3 |
    r2 r4 si |
    do'2. do'4 |
    do' do' do' re' |
    si2.\trill do'4 |
    do' r r2 |
    R1*2 |
    r4 si si si |
    do'4 do' do' do' |
    la la la la |
    si si si si |
    sold\trill r r2 |
    R1*2 |
    r4 sol sol sol |
    do' do' do' si |
    do' do' do' re' |
    mi' mi' re'4.\trill re'8 |
    re'4 r r2 |
    R1*2 |
    r4 do' do' do' |
    do' do' do' sib |
    la la si dod' |
    re' re' re' do' |
    si\trill r r2 |
    R1*2 |
    r4 do' do' re' |
    do' do' do' sib |
    la la la la |
    si do' si4.\trill do'8 |
    do'2 r |
    R4.*35 |
    r8 r do' |
    do'4. |
    mi'8 re' do' |
    si4\trill si8 |
    r r re' |
    do'4 re'8 |
    mi'4. |
    la8 la re' |
    si4.\trill |
    do'4 do'8 |
    si si do' |
    la16[ sol la si do' re']( |
    mi'8) do' do' |
    re'4 re'8 |
    re'4 do'8 |
    la4 la8 |
    re'16[ mi' re' do'] si[ la] |
    sold8\trill sold sold |
    la sold la |
    la( sold4) |
    la4 la8 |
    re'4 fa'8 |
    mi'4 re'8 |
    dod'4. |
    re'8 re' re' |
    la fa' fa' |
    dod' dod' re' |
    re'4( dod'8) |
    re'4 re'8 |
    re'4. |
    do'8 re' do' |
    si4\trill si8 |
    r r re' |
    do'4 re'8 |
    mi'4. |
    la8 la re' |
    si4.\trill |
    do'4 do'8 |
    si si do' |
    la16[ sol la si do' re']( |
    mi'8) do' do' |
    re'4 re'8 |
    sol4 do'8 |
    la4 re'8 |
    re'4 re'8 |
    re' re' re' |
    re' re' re' |
    re'4. |
    si4 si8 |
    do'4 do'8 |
    re'4 re'8 |
    re'4 re'8 |
    do' do' do' |
    do' do' do' |
    do'4. |
    la |
    r8 r do' |
    do'4 fa'8 |
    mi'4.\trill |
    fa'8 fa' fa' |
    do' do' fa' |
    mi' mi' fa' |
    do'4. |
    la4 la8 |
    re'4 re'8 |
    si4 si8 |
    mi'4 mi'8 |
    do' do' << { \voiceOne mi' \oneVoice } \new Voice { \voiceTwo \sug la } >> |
    re' re' mi' |
    mi'( re'4)\trill |
    do'4. |
    r8 r do' |
    si4 do'8 |
    re'4. |
    do'8 do'8. re'16 |
    si8\trill mi' mi' |
    si si do' |
    do'( si4)\trill |
    do'4 mi'8 |
    re'4 re'8 |
    do'4 do'8 |
    do'4 re'8 |
    si8 si si |
    do' si do' |
    do'( si4) |
    do'4. |
    r8 r do' |
    si4\trill do'8 |
    re'4. |
    do'8 do'8. re'16 |
    si8\trill mi' mi' |
    si si do' |
    do'( si4)\trill |
    do'4 mi'8 |
    re'4 re'8 |
    do'4 do'8 |
    do'4 re'8 |
    si si si |
    do' si do' |
    do'( si4)\trill |
    do'8\fermata
  }
  \tag #'choeur2-b {
    \clef "vbasse" R1*7 |
    r2 r4 sol4 |
    mi2.\trill mi4 |
    fa4. fa8 fa4 mi |
    re2.\trill do4 |
    do r r2 |
    R1*2 |
    r2 r4 sol |
    mi2.\trill mi4 |
    la la la fa |
    sol2. sol4 |
    do4. do8 do4 do |
    fa4. fa8 fa4 re |
    la2 la |
    r4 fa fa fa |
    do'4. do'8 do'4. do8 |
    sol2 sol4 r |
    R1*3 |
    r2 r4 sol |
    mi2.\trill mi4 |
    la la la fa |
    sol2. sol4 |
    do4 r r2 |
    R1*2 |
    r4 sol sol fa |
    mi4 mi mi mi |
    fa fa fa mi |
    re re re re |
    mi r r2 |
    R1*2 |
    r4 sol sol fa |
    mi mi mi re |
    do do do si, |
    la, la, re4. re8 |
    sol,4 r r2 |
    R1*2 |
    r4 do' do' sib |
    la la mi mi |
    fa fa fa mi |
    re4. do8 si,4 do |
    sol, r r2 |
    R1*2 |
    r4 do' do' sib |
    la la mi mi |
    fa fa fa mi |
    re do sol,4. do8 |
    do2 r |
    R4.*35 |
    r8 r do' |
    do'4. |
    do'8 si do' |
    sol4 sol8 |
    r r re |
    la4 la8 |
    mi4. |
    fa8 fa re |
    sol( sol,4) |
    do4 do8 |
    sol sol mi |
    fa16[ mi fa sol la si]( |
    do'8) do' la |
    re'4 re8 |
    sol4 mi8 |
    fa4 fa8 |
    re4 re8 |
    mi mi re |
    do si, la, |
    mi4. |
    la,8 la sol |
    fa fa re |
    la4.~ |
    la~ |
    la~ |
    la~ |
    la |
    la4 la8 |
    re'4 re'8 |
    si4. |
    do'8 si do' |
    sol4 sol8 |
    r r re |
    la4 la8 |
    mi4. |
    fa8 fa re |
    sol8( sol,4) |
    do4 do8 |
    sol sol mi |
    fa16[ mi fa sol la si]( |
    do'8) do' la |
    re'4 re8 |
    mi4 mi8 |
    fad4 fad8 |
    sol8[ fad] sol |
    re re' do' |
    si la sol |
    re4. |
    sol4 sol8 |
    do'4 la8 |
    sib4 sib8 |
    sol4\trill sol8 |
    do' do' sib |
    la sol fa |
    do4. |
    fa8 fa sol |
    la la fa |
    do'4.~ |
    do'~ |
    do'~ |
    do'~ |
    do' |
    do'4 do8 |
    fa4 fa8 |
    re4 re8 |
    sol4 sol8 |
    mi4 mi8 |
    la la la |
    si si do' |
    sol8( sol,4) |
    do8 do re |
    mi mi do |
    sol4.~ |
    sol~ |
    sol~ |
    sol~ |
    sol |
    sol4 sol8 |
    la4 la8 |
    si4 si8 |
    do'4 do8 |
    fa4 re8 |
    sol8 sol fa |
    mi re do |
    sol( sol,4) |
    do8 do re |
    mi mi do |
    sol4.~ |
    sol~ |
    sol~ |
    sol~ |
    sol |
    sol4 sol8 |
    la4 la8 |
    si4 si8 |
    do'4 do8 |
    fa4 re8 |
    sol sol fa |
    mi re do |
    sol8( sol,4) |
    do8\fermata
  }
  \tag #'(junon basse) {
    <<
      \tag #'basse { s1*59 s2 \ffclef "vdessus" }
      \tag #'junon { \clef "vdessus" R1*59 r2 }
    >> r4 <>^\markup\character Junon r8 mi'' |
    mi''4. |
    do''8 la' re'' |
    si'4\trill si'8 |
    r r do'' |
    la'4\trill re''8 |
    si'4.\trill |
    do''8 re'' mi'' |
    mi''( re''4)\trill |
    do''4 do''8 |
    si'\trill si' do'' |
    la'16[ sol' la' si' do'' re'']( |
    mi''8) mi'' do'' |
    la' la' re'' |
    si'4\trill si'8 |
    do''4 do''8 |
    re''4 re''8 |
    mi''4 fa''8 |
    re''\trill si' si' |
    do'' re'' mi'' |
    mi''( re''4)\trill |
    do''4. |
    r8 r mi'' |
    re''4\trill mi''8 |
    fa''4. |
    mi''8 fa''16[ mi''] re''[ do''] |
    si'8\trill sol'' << { \voiceOne sol'' \oneVoice } \new Voice { \voiceTwo \sug fa'' } >> |
    mi'' fa''16[ mi''] re''[ do''] |
    do''8( si'4)\trill |
    do''4 do''8 |
    re''4 re''8 |
    mi''4 do''8 |
    la'4 fa''8 |
    re''\trill si' si' |
    do'' re'' mi'' |
    mi''( re''4)\trill |
    do''4 \tag #'junon { r8 R4.*90 }
  }
  \tag #'jupiter {
    \clef "vbasse" R1*59 |
    r2 r4 <>^\markup\character Jupiter r8 do' |
    do'4. |
    mi8 fa re |
    sol4 sol8 |
    r r mi |
    fa4 fa8 |
    sol4. |
    la8 si do' |
    sol8( sol,4) |
    do4 do8 |
    sol sol mi |
    fa16[ mi fa sol la si]( |
    do'8) do' la |
    re' re' re |
    sol4 sol8 |
    la4 la8 |
    si4 si8 |
    do'4 do8 |
    sol sol fa |
    mi re do |
    sol8( sol,4) |
    do8 do re |
    mi mi do |
    sol4.~ |
    sol~ |
    sol~ |
    sol~ |
    sol |
    sol4 sol8 |
    la4 la8 |
    si4 si8 |
    do'4 do8 |
    fa4 re8 |
    sol sol fa |
    mi re do |
    sol8( sol,4) |
    do4 r8 |
    R4.*90
  }
>>