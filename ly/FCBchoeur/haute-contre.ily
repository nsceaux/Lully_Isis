\clef "haute-contre" R1*7 |
r2 r4 sol' |
sol'2. sol'4 |
fa'4. fa'8 re'4 << \sug sol' \\ mi' >> |
fa'2. sol'4 |
sol' r r2 |
R1*2 |
r2 r4 sol' |
sol'2. sol'4 |
la' la' la' la' |
sol'2.\trill fa'4 |
mi'4.\trill do''8 do''4 do'' |
do''4. do''8 do''4 re'' |
do''2. sib'4 |
la'4 do'' do'' do'' |
do''4. do''8 do''4. do''8 |
si'2 r |
R1*3 |
r2 r4 sol' |
sol'2. sol'4 |
la' la' la' la' |
sol'2. fa'4 |
mi'\trill r r2 |
R1*2 |
r4 si' si' si' |
do'' do'' do'' do'' |
la' la' la' la' |
si' si' si' si' |
sold'\trill r r2 |
R1*2 |
r4 re' re' re' |
mi' mi' mi' fa' |
sol' sol' sol' sol' |
sol' sol' fad'4.\trill sol'8 |
sol'4 r r2 |
R1*2 |
r4 do'' do'' do'' |
do'' do'' do'' sib' |
la' la' si' dod'' |
re'' re'' re'' do'' |
si'\trill r r2 |
R1*2 |
r4 do'' do'' do'' |
do'' do'' do'' sib' |
la' la' la' la' |
si' do'' si'4.\trill do''8 |
do''2 r |
R4.*35 |
r8 r do'' |
do''4. |
do''8 re'' do'' |
si'4 si'16 do'' |
re''8 re'' re'' |
do''4 re''8 |
mi''4. |
la'8 la' re'' |
si'4\trill~ si'16 do'' |
do''4 do''8 |
si'4 do''8 |
la'16 sol' la' si' do'' re'' |
do''8 do'' do'' |
la'4 la'8 |
sol'4 do''8 |
la'4 la'8 |
re''16 mi'' re'' do'' si' la' |
sold'8 sold' sold' |
la' sold'8. la'16 |
la'8 sold'8.\trill la'16 |
la'4 la'8 |
la'4 la'8 |
la'4 la'8 |
sol'4. |
fa'8 fa'8. sol'16 |
mi'8 la' la' |
la' la' la' |
la'4. |
fad'4\trill fad'8 |
sol'4. |
sol'8 re'' do'' |
si'4 si'16 do'' |
re''8 re'' re'' |
do''4 re''8 |
mi''4. |
la'8 la' re'' |
si'4\trill~ si'16 do'' |
do''4 do''8 |
si'4 do''8 |
la'16 sol' la' si' do'' re'' |
do''8 do'' do'' |
la'4 fad'8 |
sol'4 sol'8 |
la'4 la'8 |
sol'4 sol'8 |
fad' fad' fad' |
sol' fad' sol' |
sol' fad'8.\trill sol'16 |
sol'4 si'8 |
do''4 do''8 |
fa'4 fa'8 |
sib'4 sib'8 |
sol' sol' sol' |
la' sib' la' |
la'( sol'8.\trill) fa'16 |
fa'4. |
r8 r fa' |
mi'4 fa'8 |
sol'4. |
fa'8 fa'8. sol'16 |
mi'8 la' la' |
sol' sol' fa' |
fa'8( mi'8.)\trill fa'16 |
fa'4 la'8 |
re''4 re''8 |
si'4 si'8 |
mi''4 mi''8 |
do'' do'' do'' |
re'' sol' sol' |
sol'4~ sol'16 fa' |
mi'4.\trill |
r8 r do'' |
si'4 do''8 |
re''4. |
do''8 do''8. re''16 |
si'8 mi'' mi'' |
si' si' do'' |
do'' si'8.\trill do''16 |
do''4 la'8 |
la'4 sol'8 |
sol'4 do''8 |
do''4 re''8 |
<< { re'' re'' re'' } \\ \sugNotes { si' si' si' } >> |
do'' si' do'' |
do'' si'8.\trill do''16 |
do''4. |
r8 r do'' |
si'4 do''8 |
re''4. |
do''8 do''8. re''16 |
si'8 mi'' mi'' |
si'8 si' do'' |
do'' si'8.\trill do''16 |
do''4 la'8 |
la'4 sol'8 |
sol'4 do''8 |
do''4 re''8 |
si' si' si' |
do'' si' do'' |
do'' si'8.\trill do''16 |
do''8\fermata
