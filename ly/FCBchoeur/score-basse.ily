\score {
  \new StaffGroup <<
    \new Staff \with { \haraKiriFirst } <<
      \global \keepWithTag #'basse \includeNotes "basse"
    >>
    \new Staff \with { \tinyStaff } <<
      <>^"B.C."
      \global \keepWithTag #'basse-continue \includeNotes "basse"
    >>
  >>
  \layout { indent = 0 }
}
