\bookpart {
  \act "Acte Troisiéme"
  \sceneDescription\markup\wordwrap-center {
    Le Théatre change, & représente la Solitude dont Argus fait sa demeure
    prés d’un Lac, au milieu d’une Forêt.
  }
  \scene "Scene Premiere" "Scene I"
  \sceneDescription\markup\wordwrap-center { Argus, Io. }
  %% 3-1
  \pieceToc "Ritournelle"
  \includeScore "DAAritournelle"
}
\bookpart {
  %% 3-2
  \pieceToc\markup\wordwrap {
    Argus, Io : \italic { Dans ce solitaire séjour }
  }
  \includeScore "DABrecit"
}
\bookpart {
  \scene "Scene II" "Scene II"
  \sceneDescription\markup\wordwrap-center { Hierax, Argus. }
  %% 3-3
  \pieceToc\markup\wordwrap {
    Hierax, Argus : \italic { La perfide craint ma presence }
  }
  \includeScore "DBArecit"
}
\bookpart {
  \scene "Scene III" "Scene III"
  \sceneDescription\markup\column {
    \wordwrap-center {
      Argus, Hierax, une Nymphe qui représente Syrinx.
    }
    \wordwrap-center {
      Troupe de Nymphes en habit de chasse. Syrinx. Chœur de Nymphes
    }
  }
  %% 3-4
  \pieceToc\markup\wordwrap {
    Hierax, Argus, chœur : \italic { Liberté, liberté. }
  }
  \includeScore "DCAchoeur"
}
\bookpart {
  \paper { page-count = 5 }
  \scene "Scene IV" "Scene IV"
  \sceneDescription\markup\wordwrap-center {
    Argus, Hierax, Syrinx, Troupe de Nymphes, Mercure déguisé en Berger,
    Troupe de Bergers, Troupe de Satyres & de Sylvains.
  }
  %% 3-5
  \pieceToc\markup\wordwrap {
    Mercure, chœur : \italic { Liberté, liberté. }
  }
  \includeScore "DDAchoeur"
}
\bookpart {
  %% 3-6
  \pieceToc\markup\wordwrap {
    Mercure, Argus : \italic { De la nymphe Syrinx, Pan chérit la mémoire }
  }
  \includeScore "DDBrecit"
  
  \scene "Scene V" "Scene V"
  \sceneDescription\markup\wordwrap-center {
    Argus, Hierax, Syrinx, Troupe de Nymphes.
  }
  %% 3-7
  \pieceToc\markup\wordwrap {
    Syrinx, chœur : \italic { Liberté, liberté. }
  }
  \includeScore "DEAchoeur"
  \markup\fill-line {
    \line\italic {
      Dans le temps qu’uen partie des Nymphes chante, le reste de la Troupe danse.
    }
  }
}
\bookpart {
  %% 3-8
  \pieceToc\markup { Air de Sylvains et des Satyres }
  \includeScore "DEBair"
}
\bookpart {
  %% 3-9
  \pieceToc\markup\wordwrap {
    Chœur : \italic { Liberté, liberté. }
  }
  \markup {
    [Matériel 1677 : \italic { on reprend ce dernier Trio }
    \normal-text Liberté, &c]
  }
  \includeScore "DECchoeur"
}
\bookpart {
  %% 3-10
  \pieceToc\markup\wordwrap {
    Marche [des Bergers & Satyres qui apportent des presents à Syrinx]
  }
  \includeScore "DEDmarche"
}

\bookpart {
  \scene "Scene VI" "Scene VI"
  \sceneDescription\markup\column {
    \wordwrap-center {
      Une des Sylvains représentant le Dieu Pan.
      Troupe de Bergers, de Satyres & de Sylvains.
    }
    \smaller\justify {
      Des Bergers & des Sylvains dansants & chantants viennent offrir des présens
      de fruits & de fleurs à la Nymphe Syrinx, & tâchent de luy persuader de
      n’aller point à la chasse, & de s’engager sous les loix de l’Amour.
    }
  }
  %% 3-11
  \pieceToc "Air"
  \markup {
    [Matériel 1677 :
    \italic {
      Les Violons, les Fluttes & les Hautbois joüent cét Air
      alternativement avec les \concat { voix. \normal-text ] }
    }
  }
  \includeScore "DFAair"
}
\bookpart {
  %% 3-12
  \pieceToc\markup\wordwrap {
    Deux Bergers : \italic { Quel bien devez-vous attendre }
  }
  \includeScore "DFBbergers"

  %% 3-13
  \pieceToc\markup\wordwrap { Troisième Air }
  \includeScore "DFCair"
}
\bookpart {
  %% 3-14
  \pieceToc\markup\wordwrap {
    Pan, Syrinx : \italic { Je vous aime, nymphe charmante }
  }
  \includeScore "DFDrecit"
}
\bookpart {
  %% 3-15
  \pieceToc\markup\wordwrap {
    Chœurs, Syrinx, Pan : \italic { Aymons sans cesse }
  }
  \includeScore "DFEchoeur"
}
\bookpart {
  \paper { mix-systems-per-page = 2 }
  %% 3-16
  \pieceToc\markup\wordwrap {
    Syrinx, Pan, chœur : \italic { Faut-il qu’en vains discours un si beau jour se passe }
  }
  \includeScore "DFFchoeur"
}
\bookpart {
  %% 3-17
  \pieceToc\markup\wordwrap {
    Pan, Syrinx, chœur : \italic { Je ne puis vous quitter, mon cœur s’attache à vous }
  }
  \includeScore "DFGchoeur"
  \markup\fill-line {
    \null
    \line\italic {
      Le vent pénétre dans les Roseaux, & leur fait former un bruit plaintif.
    }
  }
}
\bookpart {
  %% 3-18
  \pieceToc\markup\wordwrap { Plainte du Dieu Pan }
  \includeScore "DFHplainte"
}

\bookpart {
  \scene "Scene VII" "Scene VII"
  \sceneDescription\markup\wordwrap-center {
    Io, Mercure, Troupe de Sylvains, de Satyres, & de Bergers, Argus, Hierax.
  }
  %% 3-19
  \pieceToc\markup\wordwrap {
    Mercure, Hierax, Io, Argus, chœur :
    \italic { Reconnoissez Mercure, & fuyez avec nous }
  }
  \includeScore "DGArecit"

  \scene "Scene VIII" "Scene VIII"
  \sceneDescription\markup\wordwrap-center {
    Junon sur son char, Argus, Io, Erinins, la Furie.
  }
  %% 3-20
  \pieceToc\markup\wordwrap {
    Junon, Io :
    \italic { Revoy le jour, Argus, que ta figure change }
  }
  \includeScore "DHArecit"
  \actEnd FIN DU TROISIÈME ACTE
}
\bookpart {
  %% 3-21
  \pieceToc\markup Entr’acte
  \reIncludeScore "DFAair" "DFBentracte"
}
