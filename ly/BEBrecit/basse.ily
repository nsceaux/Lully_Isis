\clef "basse" sib,1\repeatTie |
\once\tieDashed sib2~ sib4 la |
sib2 re |
mib4 mi2 |
fa mib8 re do sib, |
la,4. sib,8 fa,2 |
sib,1 |
mib2 do |
fa4 fa8 mib re4 |
mib re8 do sib, la, sol,4 |
fa, fa \once\tieDashed do2~ |
do fa |
sol2. |
lab8 fa sol4 sol, |
do do'8 sib la2 |
sib4 sib,8 do re4 re8 mib |
fa2. |
sol8 mib fa4 fa, |
sib,2 sib |
la8^\markup\croche-pointee-double sol fa mib re2 |
mib re |
do do, |
fa,4 fa8 mib re do sib,4 sib la |
sol la sib mib fa fa, |
sib,2. sib4 fad2 |
sol4. fa8 mi4 fa do do, |
fa, fa8 mib re do sib,4 sib la |
fa,2 fa4 mi fad2 |
sol2 fa4 mib4. re8 do4 |
re4. mi8 fad re << \sug sol4 \\ fad4 >> la sib |
do'8 sib la4 sol do re re, |
sol,2 sol8 la sib2 si4 |
do' la sib mib mi2 |
fa fa8 sol lab2 sol8 fa |
mib4 fa sol mib fa fa, |
sib, sib sol lab2 sol8 fa |
mib4 fa sol mib fa fa, |
sib,4. fa8 sol la sib4 sib, do |
re do sib, fa4. sol8 la4 |
sib sol4. fa8 mi2 fa8 mib |
re4 do sib, la,4. sol,8 fa,4 |
sib, sib8 la sol4 fa do' do |
fa4. fa8 sol la sib4 sib, do |
fa2 fa4 mib2 re4 |
do si,2 do2. | \allowPageTurn
do'4 re' mib' si do' fa |
sol sol,2 do4 do'8 sib la4 |
sib \footnoteHere #'(0 . 0) \markup {
  Matériel 1677 : \italic la♮
} lab sol fa4 mib8 re do4 |
fa2 fa8 mib re4 mib8 re do sib, |
la,4 sib,8 do re mib fa4 fa,2 |
sib,4. la,8 sib, do re4 mib8 re do sib, |
la,4 sib,8 do re mib fa4 fa,2 |
sib,8_\markup\italic fort guay la, sib, do re mib |
fa mib fa sol la fa |
sib2. |
la |
sib4 sib,8 do re sib, |
mib4. fa8 mib re |
do sib, la,4 sib, |
fa, fa8 sol fa mib |
re4 sol la |
sib mib2 |
fa8 mib fa sol la fa |
fa4 mi do |
fa4. mib8 re do |
sib, do re4 sib, |
mib8 fa mib re do4 |
fa8 mi fa sol la fa |
sib4. la8 sib do' |
re' sib do'4 do |
fa2 fa4 |
mib2. |
re4 do sib, |
fa4 fa,2 |
sib, sib4 |
lab2. |
sol4 re mib |
fa fa,2 |
sib,2 sib4 |
lab2. |
sol4 re mib |
fa fa,2 |
sib,4

