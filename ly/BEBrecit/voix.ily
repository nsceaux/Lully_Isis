\ffclef "vhaute-contre" <>^\markup\character Mercure
r4 fa'8 fa' re'4\trill re'8 fa' |
sib sib r16 sib sib sib mib'8 mib' mib' re' |
re'4\trill re'8 mib' fa'4 fa'8 fa' |
sol' mib' do'\trill do' do' do' |
la8\trill la r fa16 fa do'4 do'8 re' |
mib'4 mib'8 re' do'4\trill do'8 re' |
sib2 re'4 re'8 re' |
sol2 mib'4 mib'8 mib' |
do'\trill do' r16 do' do' do' fa'8 re' |
sib4\trill sib8 do' re'4\trill re'8 mi' |
fa'4 r8 do'16 re' mib'4 do'8 do' |
sol4 mib'8 mib' mib'8.[ re'16] re'8 re' |
si4\trill si8 sol' re' mib' |
do'4.\trill do'8 do' si |
do'4 mib'8 mib' mib'4 mib'8 fa' |
re'4\trill re'8 mib' fa'4 fa'8 sol' |
do'4\trill do'8 fa' do' re' |
sib4.\trill sib8 sib la |
sib4
\ffclef "vbas-dessus" <>^\markup\character [Io]
r8 sib' re'' re'' re'' mib'' |
fa''4. la'8 sib' sib' sib' re'' |
sol'4\trill sol'8 mib'' si'8\trill si' si'8. do''16 |
do''4. sol'8 sib' sib' sib' la' |
la'2.\trill r4 r^"Air" fa' |
sib' do'' re'' mib'' do''4.\trill fa''8 |
re''2\trill sib'4 re'' re'' do'' |
sib'4. sib'8 sib'4 la' sol'4.\trill fa'8 |
fa'2. r4 r fa' |
fa'2. do''4 do'' la' |
sib'2 la'4 sol'4.( fad'8) sol'4 |
fad'2\trill re'4 sib' do'' re'' |
sol' la' sib' la'4.\trill sol'8 fad'4 |
sol'2. re''4 mib'' fa'' |
mib''2\trill re''4 do''2\trill sib'4 |
la'2\trill fa'4 do'' re'' mib'' |
sol' la' sib' sib'2 sib'8 la' |
sib'2. do''4 re'' mib'' |
sol' la' sib' sib'2 sib'8 la' |
sib'2 r4
\ffclef "vhaute-contre" <>^\markup\character-text Mercure \normal-text Air
r4 re' mib' |
fa' mib'4.\trill re'8 do'4\trill do' do' |
re' sib sol do'2 la4\trill |
sib do' re' mib'4.( re'8) mib'4 |
re'4.\trill re'8 mi'4 fa' mi'4.\trill fa'8 |
fa'2. r4 re' mib' |
fa'2 fa4 do' do' re' |
mib' fa' re' mib'2 do'4 |
mib' fa' sol' re' mib'2 |
re'\trill do'4 do'2 fa'4 |
re'\trill re' mib' fa' sol' mib' |
do'2\trill la4 fa' sol'8[ fa'] mib'[ re'] |
mib'[ do'] re'2 do'\trill sib4 |
sib2. fa'4 sol'8[ fa'] mib'[ re'] |
mib'[ do'] re'2 do'\trill sib4 |
sib2. |
r4 r fa'^"Air" |
re' re' sib |
fa'2 do'4 |
re'2 re'4 |
sib do' re' |
mib'4 do'4.\trill sib8 |
la2\trill la4 |
r sib do' |
re'8 mib' mib'4.\trill re'8 |
do'2\trill fa'4 |
do'2\trill do'8 do' |
la4\trill fa fa' |
re'\trill sib re'8 re' |
sib4 sol mib' |
do' la fa' |
re'2\trill re'8 mi' |
fa'2 fa'8 mi' |
fa'2 la4 |
la la4.\trill la8 |
sib4 do' re' |
mib'2( re'8) mib' |
re'2\trill re'4 |
re' re'4.\trill re'8 |
mib'4 fa' sol' |
re'4( do'4.)\trill sib8 |
sib2 re'4 |
re' re'4.\trill re'8 |
mib'4 fa' sol' |
re'4( do'4.)\trill sib8 |
sib4

