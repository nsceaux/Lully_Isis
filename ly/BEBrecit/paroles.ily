C’est ain -- si que Mer -- cu -- re,
pour a -- bu -- ser les Dieux ja -- loux,
doit par -- ler hau -- te -- ment à tou -- te la Na -- tu -- re ;
mais il doit s’ex -- pli -- quer au -- tre -- ment a -- vec vous :
C’est pour vous voir, c’est pour vous plai -- re,
que Ju -- pi -- ter des -- cend du cé -- les -- te sé -- jour ;
et les biens qu’i -- cy- bas sa pré -- sen -- ce va fai -- re,
ne se -- ront dûs qu’à son a -- mour.
Et les biens qu’i -- cy- bas sa pré -- sen -- ce va fai -- re,
ne se -- ront dûs qu’à son a -- mour.

Pour -- quoy du haut des cieux, ce Dieu veut- il des -- cen -- dre ?
Mes vœux sont en -- ga -- gés, mon cœur a fait un choix.
L’A -- mour tôt ou tard doit pré -- ten -- dre,
que tous les cœurs se ran -- gent sous ses loix :
L’A -   loix :
C’est un hom -- ma -- ge qu’il faut ren -- dre ;
mais c’est as -- sés de le rendre u -- ne fois.
C’est un hom -- ma -- ge qu’il faut ren -- dre ;
mais c’est as -- sez de le rendre u -- ne fois,
mais c’est as -- sez de le rendre u -- ne fois.

Ce se -- roit en ai -- mant u -- ne con -- trainte é -- tran -- ge,
qu’un cœur pour mieux choi -- sir n’o -- sat se dé -- ga -- ger :
Ce se -  - ger :
Quand c’est pour Ju -- pi -- ter qu’on chan -- ge,
il n’est pas hon -- teux de chan -- ger.
Quand c’est pour Ju -- pi -- ter qu’on chan -- ge,
il n’est pas hon -- teux de chan -- ger.
Il n’est pas hon -- teux de chan -- ger.

Que tout l’u -- ni -- vers se pa -- re
de ce qu’il a de plus ra -- re,
que tout bril -- le dans ces lieux :
Que  lieux :
Que la ter -- re par -- ta -- ge,
que la ter -- re par -- ta -- ge
l’é -- clat & la gloi -- re des cieux ;
Que tout rende hom -- mage
au plus grand des Dieux.
Que tout rende hom -- mage
au plus grand des Dieux.
Que tout rende hom -- mage
au plus grand des Dieux.

