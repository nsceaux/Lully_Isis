\piecePartSpecs
#`((basse-continue #:score-template "score-voix" #:indent 0)
   (silence #:music , #{
  s1*3\allowPageTurn s2.\allowPageTurn s1*2\allowPageTurn
  s1*2\allowPageTurn s2.\allowPageTurn s1*3\allowPageTurn
  s2.*2\allowPageTurn s1*2\allowPageTurn s2.*2\allowPageTurn
  s1*4\allowPageTurn #}))
