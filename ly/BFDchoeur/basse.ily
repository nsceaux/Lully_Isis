\clef "basse"
<<
  \tag #'basse { R1*8 R2. R1*28 | r2 r4 }
  \tag #'(basse-continue bc-part) {
    \tag #'bc-part <>^"[B.C.]"
    \once\tieDashed sol,1~ |
    sol,2 fad, |
    sol, la, |
    sib,1 |
    mib2. do4 |
    \once\tieDashed re2~ re8 do sib, la, |
    sol,2 sol4 fa |
    mib4. re8 do2 |
    re4 sol, re, |
    sol,2. \tag #'bc-part <>^"[à 3]" sol8 sol |
    re4 re mib mib |
    sib, sib, r8 sib sib sib |
    la2 sol |
    fa2. << \sugNotes { fa8 fa } \\ fa4 >> |
    mib mib re sol |
    do2 r8 do do do |
    sol2 fad4 sol |
    re4. re'8 sib4 sol |
    do'2. << { do'8 do } \\ \sug do4 >> |
    << \sugNotes { fa4~ fa8. fa16 } \\ fa2 >> fa4 re |
    sib2. sib8 sol |
    la4 la fa4. fa8 |
    dod4 re la,2 |
    re4. re'8 re'4 re' |
    sol2. sol8 sol |
    do' re' do' sib la sol fa mib |
    re do re sib, mib re mib do |
    fa4 fa re4. re8 |
    do4 sib, fa fa, |
    sib,4. sib,8 re4 sib, |
    fa2. fa8 fa |
    do8 sib, do re mib re mib do |
    sol fad sol la sib la sib sol |
    re' mib' re' do' sib la sol fa |
    mib fa sol fa mib re mib do |
    re4 re sib,4. sib,8 |
    la,4 sol, re re, |
    sol,2.
  }
>> \tag #'bc-part <>^"[Tous]" sol8 sol |
re4 re \sugRythme { si'4. si'8 } mib4 mib |
sib,2~ sib,8 sib sib sib |
la2 sol |
fa2. fa8 fa |
mib4 mib re sol |
do2~ do8 do do do |
sol2 fad4 sol |
re4. re'8 sib4 sol |
do'2. do'8 do |
fa4~ fa8. fa16 fa4 re |
sib2. sib8 sol |
la4 la fa4. fa8 |
dod4 re la,2 |
re4. re'8 re'4 re' |
sol2. sol8 sol |
do' re' do' sib la sol fa mib |
re do re sib, mib re mib do |
fa4 fa re4. re8 |
do4 sib, fa fa, |
sib,4. sib,8 re4 sib, |
fa2. fa8 fa |
do sib, do re mib re mib do |
sol fad sol la sib la sib sol |
re' mib' re' do' sib la sol fa |
mib fa sol fa mib re mib do |
re4 re sib,4. sib,8 |
la,4 sol, re re, |
sol,1\fermata |

