\clef "dessus" R1*8 R2. |
r2 r4 \twoVoices #'(dessus1 dessus2 dessus) <<
  { re''8^"Violons" mib'' |
    fa''4 fa'' sib' mib'' |
    re'' re'' r8 re'' re'' mi'' |
    fa''2 fa''4 mi'' |
    fa''2. do''8 re'' |
    mib'' fa'' sol''4 fa'' mib''8 re'' |
    mib''4 mib'' r8 sol'' sol'' la'' |
    sib''2 la''4 sol'' |
    fad''4.\trill fad''8 sol''4 sol'' |
    mi''8 re'' mi'' fa'' sol'' fa'' sol'' mi'' |
    fa''4~ fa''8. la''16 la''4 la'' |
    fa''2. fa''8 sol'' |
    mi''4\trill mi'' fa''8 sol'' la''4 |
    sol'' fa'' mi''4.\trill re''8 |
    re''4. fad''8 fad''4 fad'' |
    sol''8 la'' sib'' la'' sol'' la'' sol'' fa'' |
    mi''4. mi''8 fa'' sol'' la'' fa'' |
    sib''4. lab''8 sol'' fa'' mib'' re'' |
    do''4\trill do'' re''4. re''8 |
    la'4 sib' la'4.\trill sib'8 |
    sib'4. re''8 re''4 re'' |
    do''8 sib' la' sol' fa' sol' la' si' |
    do''4. do''8 sol' \footnoteHere #'(0 . 0) \markup {
      Matériel 1677 : \italic fa♯
    } fad'? sol' la' |
    sib' la' sib' do'' re''4. sol''8 |
    fad''4. fad''8 sol'' la'' sib'' la'' |
    sol''2. sol''8 la'' |
    fad''4 fad'' re''4. re''8 |
    do''4 sib' la'4.\trill sol'8 |
    sol'2.
  }
  { sib'8 do'' |
    re''4 re'' sol'4. la'8 |
    sib'4 sib' r8 fa' fa' sol' |
    la'2 sib'4. do''8 |
    la'2.\trill la'8 \footnoteHere #'(0 . 0) \markup {
      Matériel 1677 : \italic si♮
    } si'?8 |
    do''4 do'' do'' si'\trill |
    do''4 do'' r8 mib'' mib'' fa'' |
    re''2 do''4 sib' |
    la'4.\trill la'8 sib'4 sib' |
    sol'8 fa' sol' la' sib' la' sib' sol' |
    la'4~ la'8. fa''16 fa''4 fa'' |
    re''2. re''8 mi'' |
    dod''4\trill dod'' re''8 mi'' fa''4 |
    mi'' re'' dod''4.\trill re''8 |
    re''4. la'8 la'4 la' |
    sib'8 do'' re'' do'' sib' do'' sib' la' |
    sol'4. sol'8 la' sib' do'' la' |
    re''4. re''8 sib'4 do''8 sib' |
    la'4\trill la' fa''4. fa''8 |
    mib''4 re'' do''4.\trill sib'8 |
    sib'4. sib'8 sib'4 sib' |
    la'8 sol' fa' sol' la' \footnoteHere #'(0 . 0) \markup {
      Matériel 1677 : \italic si♮
    } si'?8 do'' re'' |
    mib''2. mib''8 mib'' |
    re''4 re''8 do'' sib' do'' re'' sib' |
    la'4. re''8 re''4. re''8 |
    re''2 do''4. do''8 |
    la'4 la' sib'4. sib'8 |
    fad'4 sol' fad'4.\trill sol'8 |
    \startHaraKiri sol'2.
  }
>>
re''8 mib'' |
fa''4 fa'' mib''( re''8) mib'' |
re''4\trill re'' r8 re'' re''\trill re'' |
do''2 sib'4( la'8) sib' |
la'2.\trill do''8 re'' |
mib''4 mib'' fa'' re'' |
mib'' mib'' r8 mib'' mib'' mib'' |
re''2 do''4 sib' |
la'4.\trill re''8 re''4 re'' |
mi''2. mi''8 mi'' |
fa''4~ fa''8. fa''16 fa''4 fa'' |
re''2. re''8 mi'' |
dod''4\trill dod'' re''4. re''8 |
mi''4 fa'' mi''4.\trill re''8 |
re''4. la'8 la'4 la' |
sib'2. sib'8 sib' |
sol'4 sol'8 r16 sol' do''4 do'' |
sib'2. sib'8 do'' |
la'4 la' fa''4. fa''8 |
mib''4 re'' do''4.\trill sib'8 |
sib'4. re''8 re''4 re'' |
do''2. do''8 re'' |
mib''4~ mib''8. mib''16 mib''4 mib'' |
re''2. re''8 sol'' |
fad''4~ fad''8. re''16 re''4 re'' |
\sugRythme { si'4. si'8 } re''4 re'' do''4 do''8 do'' |
la'4 la' re''4. re''8 |
do''4 sib' la'4.\trill sol'8 |
sol'1\fermata |
