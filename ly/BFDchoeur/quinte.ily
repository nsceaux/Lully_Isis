\clef "quinte" R1*8 R2. R1*28 |
r2 r4 sib8 sib |
sib4 sib sol4. sol8 |
sib4 sib r8 sib sib sib |
do'4 la sib sol |
la2. la8 la |
sol4 do' re' re' |
<< \sugNotes { mib'? mib' } \\ { do' do' } >> r8 sol sol la |
sib2 re'4 re' |
re'4. re'8 re'4 re' |
do'2. do'8 do' |
do'4~ do'8. do'16 do'4 re' |
re'2. fa'8 mi' |
mi'4 mi' re'4. re'8 |
la4 la la4. la8 |
la4. re'8 re'4 re' |
sib2. sib8 sib |
do'4 do'8 do' do'4 la |
sib4. sib8 sol4 sol8 sol |
la4 la sib4. sib8 |
do'4 re' \once\slurDashed mib'( re'8) mib' |
re'4. sib8 sib4 sib |
do'4. la8 la4. si8 |
do'4 do'8 do' sol \footnoteHere #'(0 . 0) \markup {
  Matériel 1677 : fa♯
} fad? sol la |
sib la sib do' re'4 re'8 re' |
re'4~ re'8. re'16 re'4 re' |
mib' sib do' do'8 la |
la4 la sol4. sol8 |
la4 sib8 do' re'4. re'8 |
re'1\fermata |
