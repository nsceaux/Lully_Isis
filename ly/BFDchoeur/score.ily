\score {
  <<
    \new StaffGroup \with { \haraKiriFirst } <<
      \new GrandStaff <<
        \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus" >>
        \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus" >>
      >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \with { \haraKiri } \withLyrics <<
        \global \keepWithTag #'jupiter \includeNotes "voix"
      >> \keepWithTag #'jupiter \includeLyrics "paroles"
      \new Staff <<
        \global \keepWithTag #'basse-continue \includeNotes "basse"
        \includeFigures "chiffres"
        \origLayout {
          s1*4\break s1*3 s2 \bar "" \break s2 s2.*2 \bar "" \break
          s4 s1*5\pageBreak
          s1*5\break s1*6\break s1*5\pageBreak
          s1*4 s2 \bar "" \break s2 s1 s2. \bar "" \pageBreak
          s4 s1*6\pageBreak
          s1*6 s2 \bar "" \pageBreak
          s2 s1*4 s2 \bar "" \pageBreak
          s2 s1*5\pageBreak
        }
        \modVersion {
          s1*8 s2. s1*47\pageBreak
        }
      >>
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
