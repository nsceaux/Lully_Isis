\clef "haute-contre" R1*8 R2. R1*28 |
r2 r4 sib'8 do'' |
re''4 re'' sib'4. sib'8 |
sib'4 sib' r8 fa' fa' sol' |
la'4 fa' fa' mi' |
fa'2. la'8 si'? |
do''4 do'' do'' si' |
do'' do'' r8 do'' do'' do'' |
sib'2 la'4 sol' |
fad'4.\trill la'8 sib'4 sib' |
sol'2. sol'8 sol' |
la'4~ la'8. la'16 la'4 la' |
sib'2. sib'8 sib' |
la'4 la' la'4. la'8 |
la'4 la' la'4. la'8 |
fad'4.\trill fad'8 fad'4 fad' |
sol'2. sol'8 sol' |
mi'8 fa' sol' mi' \once\tieDashed fa'4~ fa'8. fa'16 |
fa'4 fa' sol' sol'8 sol' |
fa'4 fa' sib'4. sib'8 |
la'4 sib' la'4.\trill sib'8 |
sib'4. sib'8 sib'4 sib' |
la'2. la'8 la' |
sol'4 sol'8 do'' do''4 do'' |
sib'2. sib'8 sib' |
la'4~ la'8. la'16 sib'4 sib' |
sib' sol' sol' sol'8 la' |
fad'4\trill fad' sol'4. sol'8 |
fad'4 sol' fad'4.\trill sol'8 |
sol'1\fermata |
