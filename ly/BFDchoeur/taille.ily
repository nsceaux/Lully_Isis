\clef "taille" R1*8 R2. R1*28 |
r2 r4 sol'8 sol' |
fa'4 fa' sol'4. sol'8 |
fa'4 fa' r8 re' re' mi' |
fa'2 sol'4 sol' |
do'2. fa'8 fa' |
sol'4 sol' fa' sol' |
sol' sol' r8 sol' sol' sol' |
sol'2 la'4 re' |
re'4. fad'8 sol'4 sol' |
sol'2. sol'8 sol' |
fa'4~ fa'8. fa'16 fa'4 fa' |
fa'2. fa'8 sol' |
mi'4 mi' fa'4. fa'8 |
mi'4 re' dod'4.\trill re'8 |
re'4. re'8 re'4 re' |
re'2. re'8 re' |
do'4 do'8 do' do'4 do' |
re'4. re'8 mib'4 mib'8 mib' |
do'4 do' re'4. re'8 |
mib'4 fa' fa'4. fa'8 |
fa'4. fa'8 fa'4 fa' |
fa'2. fa'8 fa' |
mib'4 do'8 do' do'4 do' |
re'2. sol'8 sol' |
la'4~ la'8. la'16 sol'4 sol' |
sol' mib' mib' mib'8 mib' |
re'4 re' re'4. sib8 |
do'4 re' re'4. do'8 |
si1\fermata |
