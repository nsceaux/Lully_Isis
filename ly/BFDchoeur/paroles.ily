\tag #'(jupiter basse) {
  Les ar -- mes que je tiens pro -- te -- gent l’in -- no -- cen -- ce,
  l’ef -- fort n’en est fa -- tal qu’à l’or -- gueil des Ti -- tans :
  Vous qui sui -- vez mes Loix, vi -- vez sous ma puis -- san -- ce,
  toû -- jours heu -- reux, toû -- jours con -- tents.
  Ju -- pi -- ter vient sur la ter -- re,
  pour la com -- bler de bien- faits,
  Ju -- pi -- ter vient sur la ter -- re,
  pour la com -- bler de bien- faits,
  il est ar -- mé du ton -- ne -- re,
  il est ar -- mé du ton -- ne -- re,
  mais c’est pour don -- ner la paix.
  Il est ar -- mé du ton -- ne -- re,
  mais c’est pour don -- ner la paix.
  Il est ar -- mé du ton -- ne -- re,
  mais, c’est pour don -- ner la paix.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Ju -- pi -- ter vient sur la ter -- re,
  pour la com -- bler de bien- faits,
  Ju -- pi -- ter vient sur la ter -- re,
  pour la com -- bler de bien- faits,
  il est ar -- mé du ton -- ne -- re,
  il est ar -- mé du ton -- ne -- re,
  mais c’est pour don -- ner la paix.
  Il est ar -- mé du ton -- ne -- re,
  \tag #'(vdessus vhaute-contre) {
    il est ar -- mé du ton -- ne -- re,
  }
  \tag #'vtaille {
    il est ar -- mé, ar -- mé du ton -- ne -- re,
  }
  mais c’est pour don -- ner la paix.
  \tag #'(vdessus vhaute-contre) {
    Il est ar -- mé du ton -- ne -- re,
    il est ar -- mé du ton -- ne -- re,
    il est ar -- mé, ar -- mé du ton -- ne -- re,
  }
  \tag #'vtaille {
    Il est ar -- mé, il est ar -- mé du ton -- ne -- re,
    il est ar -- mé, ar -- mé, ar -- mé du ton -- ne -- re,
  }
  \tag #'vbasse {
    Il est ar -- mé du ton -- ne -- re,
  }
  mais c’est pour don -- ner la paix.
}
