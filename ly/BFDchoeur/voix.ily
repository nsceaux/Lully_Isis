<<
  \tag #'(jupiter basse) {
    \clef "vbasse" <>^\markup\character-text Jupiter descendant du Ciel
    R1 |
    r4 r8 sol re' re' re' la |
    sib4. sib8 fa fa fa fa |
    re4\trill re8 fa sib sib sib re' |
    sol4\trill sol8 sol do'4 do'8 do' |
    la2\trill fad8 fad16 fad sol8 la |
    sib4. sib8 si\trill si si do' |
    do'4 do' r8 do' la\trill la |
    fad4\trill r8 sib la8.\trill sol16 |
    sol2. sol8 sol |
    re4 re mib mib |
    sib, sib, r8 sib sib sib |
    la2\trill sol4.\trill fa8 |
    fa2. fa8 fa |
    mib4 mib re\trill sol |
    do4 do r8 do do do |
    sol2 fad4 sol |
    re4. re'8 sib4 sol |
    do'2. do'8 do |
    fa4 fa8. fa16 fa4 re |
    sib2. sib8 sol |
    la4 la fa fa |
    dod re la,4. re8 |
    re4. re'8 re'4 re' |
    sol2. sol8 sol |
    do'[\melisma re' do' sib] la[ sol fa mib] |
    re[ do re sib,] mib[ re mib do]( |
    fa4)\melismaEnd fa re4. re8 |
    do4\trill sib, fa4( mib8) fa |
    sib,4. sib,8 re4 sib, |
    fa2. fa8 fa |
    do8[\melisma sib, do re] mib[ re mib do] |
    sol[ fad sol la] sib[ la sib sol] |
    re'[ mib' re' do'] sib[ la sol fa] |
    mib[ fa sol fa] mib[ re mib do]( |
    re4)\melismaEnd re sib,4. sib,8 |
    la,4 sol, re4. re8 |
    sol,2.
  }
  \tag #'vdessus \clef "vdessus"
  \tag #'vhaute-contre \clef "vhaute-contre"
  \tag #'vtaille \clef "vtaille"
  \tag #'vbasse \clef "vbasse"
  \tag #'(vdessus vhaute-contre vtaille vbasse) {
    R1*8 R2. R1*28 | r2 r4
  }
>>
<<
  \tag #'(jupiter basse) { \tag #'basse <>^\markup\character Chœur r4 | R1*28 }
  \tag #'vdessus {
    <>^\markup\character Chœur re''8 mib'' |
    fa''4 fa'' mib''( re''8) mib'' |
    re''4\trill re'' r8 re'' re'' re'' |
    do''2 sib'4( la'8) sib' |
    la'2.\trill do''8 re'' |
    mib''4 mib'' fa'' re'' |
    mib'' mib'' r8 mib'' mib'' mib'' |
    re''2 do''4 sib' |
    la'4.\trill re''8 re''4 re'' |
    mi''2. mi''8 mi'' |
    fa''4 fa''8 r16 fa'' fa''4 fa'' |
    re''2. re''8 mi'' |
    dod''4 dod'' re''4. re''8 |
    mi''4 fa'' mi''4.\trill re''8 |
    re''4. la'8 la'4 la' |
    sib'2. sib'8 sib' |
    sol'4 sol'8 r16 sol' do''4 do'' |
    sib'2. sib'8 do'' |
    la'4\trill la' fa''4. fa''8 |
    mib''4 re'' do''4.\trill sib'8 |
    sib'4. re''8 re''4 re'' |
    do''2.\trill do''8 re'' |
    mib''4 mib''8 r16 mib'' mib''4 mib'' |
    re''2.\trill re''8 sol'' |
    fad''4\trill fad''8 r16 re'' re''4 re'' |
    \sugRythme { si'4. si'8 } re''4 re'' do'' do''8 do'' |
    la'4\trill la' re''4. re''8 |
    do''4\trill sib' la'4.\trill sol'8 |
    sol'1\fermata |
  }
  \tag #'vhaute-contre {
    sol'8 sol' |
    fa'4 fa' sol' sol' |
    fa'4 fa' r8 fa' fa' fa' |
    fa'2 fa'4 mi' |
    fa'2. fa'8 fa' |
    sol'4 sol' fa' sol' |
    sol' sol' r8 sol' sol' sol' |
    sol'2 la'4 sol' |
    fad'4.\trill fad'8 sol'4 sol' |
    sol'2. sol'8 sol' |
    la'4 la'8 r16 la' fa'4 fa' |
    fa'2. fa'8 sol' |
    mi'4\trill mi' fa'4. fa'8 |
    mi'4 re' dod'4.\trill re'8 |
    re'4. fad'8 fad'4 fad' |
    sol'2. sol'8 sol' |
    mi'[ fa' sol' mi']( fa'4) fa'8 r16 fa' |
    fa'4 fa' mib' mib'8 mib' |
    do'4 do' re'4. sib8 |
    do'4\trill re' mib'( re'8) mib' |
    re'4.\trill fa'8 fa'4 fa' |
    fa'2. fa'8 fa' |
    sol'4 sol'8 r16 sol' sol'4 sol' |
    sol'2. sol'8 sol' |
    la'4 la'8 r16 la' sol'4 sol' |
    sol' sol' sol' sol'8 la' |
    fad'4\trill fad' sol'4. sol'8 |
    fad'4 sol' fad'4.\trill sol'8 |
    sol'1\fermata |
  }
  \tag #'vtaille {
    sib8 do' |
    re'4 re' \sugRythme { si'4. si'8 } sib4 sib |
    sib sib r8 sib sib sib |
    do'2 re'4 sib |
    do'2. la8 si |
    do'4 do' do' si |
    do'4 do' r8 do' do' do' |
    sib2 re'4 re' |
    re'4. re'8 re'4 sib |
    do'2. do'8 do' |
    do'4 do'8 r16 do' do'4 re' |
    re'2. sib8 sib |
    la4 la la4. la8 |
    la4 la la4. la8 |
    fad4.\trill re'8 re'4 re' |
    re'2. re'8 re' |
    do'4 do'8 do' do'4 la |
    re'4 re' sol sol8 sol |
    fa4 fa sib4. sib8 |
    la4 sib la4.\trill sib8 |
    sib4. sib8 sib4 sib |
    la4. la8 la4. si8 |
    do'4 do'8 do' sol[\melisma fad? sol la] |
    sib[ la sib do']( re'4)\melismaEnd re'8 re' |
    re'4 re' re' re' |
    mib' mib' mib' mib'8 mib' |
    re'4 la sib4. re'8 |
    do'4 re' re'4. re'8 |
    re'1\fermata |
  }
  \tag #'vbasse {
    sol8 sol |
    re4 re \sugRythme { si'4. si'8 } mib4 mib |
    sib, sib, r8 sib sib sib |
    la2\trill sol4.\trill fa8 |
    fa2. fa8 fa |
    mib4 mib re\trill sol |
    do4 do r8 do do do |
    sol2 fad4 sol |
    re4. re'8 sib4 sol |
    do'2. do'8 do |
    fa4 fa8. fa16 fa4 re |
    sib2. sib8 sol |
    la4 la fa4. fa8 |
    dod4 re la,4. re8 |
    re4. re'8 re'4 re' |
    sol2. sol8 sol |
    do'[\melisma re' do' sib] la[ sol fa mib] |
    re[ do re sib,] mib[ re mib do]( |
    fa4)\melismaEnd fa re4. re8 |
    do4 sib, fa4( mib8) fa |
    sib,4. sib,8 re4 sib, |
    fa2. fa8 fa |
    do8[\melisma sib, do re] mib[ re mib do] |
    sol[ fad sol la] sib[ la sib sol] |
    re'[ mib' re' do'] sib[ la sol fa] |
    mib[ fa sol fa] mib[ re mib do]( |
    re4)\melismaEnd re sib,4. sib,8 |
    la,4 sol, re4. re8 |
    sol,1\fermata |
  }
>>
