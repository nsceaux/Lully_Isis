\clef "basse" fa,4 fa8 sol la4 fa |
do'4. do'8 dod'2 |
re'4 re8 mi fa4 re sol sol8 la si4 sol |
do' fa sol sol, |
do do8 re mi4 mi8 fa |
sol4 sol,8 la, sib,4 sib,8 do |
re4 re8 mi fa4 fa8 sol |
la4 la8 si dod'4 la |
re' sol la la, |
re mi fa fa8 sol |
la4 la8 sib do'4 do8 re |
mi4 do fa fa, |
do8 do' sib la sol la sol fa |
mi fa mi re do re do sib, |
la, sol, fa,4 do do, |
fa,1*15/16~ \hideNotes fa,16 |
