\clef "quinte" do'2. do'4 |
do'4. re'8 mi'2 |
re'2. re'4 |
re'2. re'4 |
do'4 la re' sol |
sol2. sol4 |
sol8 la sib do' re'4. re'8 |
re' mi' fa' mi' re'4 la |
la mi' mi'4. mi'8 |
re'4. sol'8 mi'4 la |
la8 sib do'4 do'2 |
do'8 re' mi' re' do'2 |
do'4. do'8 la4 do' |
do'1~ |
do'2. r8 sol |
fa4 do' do'4. do'8 |
do'1 |
