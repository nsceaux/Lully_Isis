\clef "dessus" la''2. la''4 |
sol''4.\trill sol''8 sol''4. fa''16 mi'' |
fa''4. fa''8 fa''4.\trill mi''8 |
re''4.\trill re''8 sol''4 sol''8 fa'' |
mi''4. fa''8 re''4.\trill do''8 |
do''4 mi''8 fa'' sol''4 sol''8 la'' |
sib''2 re''4 re''8 mi'' |
fa''4 fa''8 sol'' la''2 |
dod''4 dod''8 re'' mi''4 la''8 mi'' |
fa''4. mi''8 mi''4.\trill re''8 |
re''4 do''8 sib' la'4 la'8 sib' |
do''4 do''8 re'' mi''4 mi''8 fa'' |
sol''4. la''8 la''4.\trill sol''8 |
sol''1~ |
sol''2. r8 sol'' |
la''4. sib''8 sol''4.\trill fa''8 |
fa''1 |
