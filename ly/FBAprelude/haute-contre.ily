\clef "haute-contre" do''2~ do''4. fa'8 |
sol'2 la' |
la'4. la'8 la'4 re''8 do'' |
si'4 si'8 do'' re''4 si' |
do''4. do''8 si'4.\trill do''8 |
do''2. do''4 |
sib'8 do'' re'' do'' sib'4. sib'8 |
la'2. la'4 |
la'2 la'8 si' dod''4 |
re''4. re''8 dod''4.\trill re''8 |
re''4 sol' fa'8 sol' la'4 |
la'4. la'8 sol'4. sol'8 |
sol'4 do'' do''4. do''8 |
do''1~ |
do''2. r8 do'' |
do''4 do'' do''4. sib'8 |
la'1\trill |
