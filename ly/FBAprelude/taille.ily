\clef "taille" fa'2. fa'4 |
fa'2 mi'4 la'8 sol' |
fa'4 fa'8 sol' la'4 la' |
sol'2 sol'4. sol'8 |
sol'4 la' sol'4.\trill fa'8 |
mi' fa' sol' fa' mi'4.\trill mi'8 |
re'4 sol'2 sol'4 |
fa'8 sol' la' sol' fa'4 fa' |
mi'2 la' |
la'4 sib' la' mi' |
fa' mi'8 re' do'4 fa' |
mi'2 mi'8 fa' sol' fa' |
mi'4. fa'8 fa'4.\trill mi'8 |
mi'1~ |
mi'2. r8 mi' |
fa'4. sol'8 << \sug sol'4. \\ mi'4.\trill >> fa'8 |
fa'1 |
