\clef "basse" \tieDashed do1~ |
do2~ do4. si,8 | \tieSolid
la,2. fad,4 |
sol,2. |
re1 |
la,2 re |
sol,2 fa,4 |
mi,2 fa, |
sol,4. fa,8 mi,4 mi |
re do sol,2 |
do,2. do4 |
fa2 fa4 re |
la2. la4 |
fad2. fad4 |
sol2. << \sugNotes { sol8 sol } \\ sol4 >> |
re2. re4 |
la2 la4 sol |
fa1 |
mi2. mi4 |
la4. sol8 fa4 mi |
re2. re4 |
sol fa mi4. re8 |
do2. do4 |
fa fa re re |
sol2. mi4 |
re do sol4. sol8 |
do2. do4 |
sol8 fa sol la si la si sol |
do'4 do' la la |
re'2. re'4 |
<< \sugNotes { sol2. sol4 } \\ { sol2 sol4 sol } >> |
do8 si, do re mi re mi do |
<< \sugNotes { fa2. fa4 } \\ { fa2 fa4 fa } >> |
re8 do re mi fa mi fa re |
<< \sugNotes { sib2. sib4 } \\ { sib4 sib sib sol } >> |
la2. la4 |
re2. re4 |
sol2. sol4 |
sol1~ |
<<
  \new Voice \sugNotes { \voiceOne sol2. sol4 }
  { \voiceTwo sol4. sol8 sol4. sol8 | \oneVoice }
>>
do'2

