\tag #'(voix2 basse) {
  Mon Em -- pire a ser -- vi de thé -- âtre à la guer -- re ;
  pu -- bli -- ez des ex -- ploits nou -- veaux :
  c’est le mes -- me Vain -- queur si fa -- meux sur la ter -- re
  qui tri -- omphe en -- cor sur les Eaux.
  C’est le mes -- me Vain -- queur si fa -- meux sur la ter -- re,
  qui tri -- omphe en -- cor sur les Eaux.
}
Ce -- lé -- brez son grand Nom sur la terre & sur l’on -- de,
ce -- lé -- brez son grand Nom sur la terre & sur l’on -- de ;
qu’il ne soit pas bor -- né par les plus vas -- tes mers :
qu’il ne soit pas bor -- né par les plus vas -- tes mers :
\tag #'(voix1 basse) {
  qu’il vo -- le,
  qu’il vo -- le,
}
\tag #'voix2 {
  qu’il vo -- le jus -- qu’au bout du mon -- de,
}
qu’il vo -- le,
qu’il vo -- le jus -- qu’au bout du mon -- de ;
\tag #'(voix1 basse) { qu’il du -- re, }
qu’il dure au -- tant que l’u -- ni -- vers.
