<<
  \tag #'(voix2 basse) {
    \clef "vbasse" <>^\markup\character-text Neptune à la Renommée
    r4 sol8 sol do'4 do'8 do |
    sol4 sol8. sol16 mi4\trill mi8 sol |
    do4 do8 mi16 mi la8 la16 la re'8. re'16 |
    si4\trill r8 sol16 sol re8 re16 mi |
    fa4 fa8 la fa4\trill fa8 mi |
    mi8\trill mi la16 la la do' fad4\trill fad8 sol |
    sol4 r8 sol16 la si8\trill si16 sol |
    do'4 do'8 sol la4 fa8 la |
    re4\trill re r sol8 do' |
    fa4. mi8 re4\trill re8 mi |
    do2.
  }
  \tag #'voix1 {
    \clef "vdessus" R1*3 R2. R1*2 R2. R1*3 | r2 r4
  }
>>
<<
  \tag #'(voix1 basse) {
    \tag #'basse \ffclef "vdessus"
    <>^\markup\character La Renommée mi''8 mi'' |
    la'2\trill la'4 si' |
    do''2 do''4 do'' |
    do''2\trill do''4. si'8 |
    si'2\trill si'4 re''8 mi'' |
    fa''2 <<
      \new CueVoice { s4^\markup\note-by-number #2 #1 #UP s^\markup\note-by-number #3 #0 #UP }
      { fa''4 sol'' | }
    >>
    mi''2\trill mi''4 mi'' |
    la'2 la'4 si' |
    sold'2\trill sold'4 mi'' |
    dod''4 dod'' re'' mi'' |
    fa''2. re''4 |
    si'4. si'8 do''4. re''8 |
    mi''2. do''4 |
    la'4 la' re'' re'' |
    si'2.\trill do''4 |
    re'' mi'' re''4.\trill do''8 |
    do''2. do''4 |
    si'8[\melisma la' si' do''] re''[ do'' re'' si'] |
    mi''[ fa'' mi'' re''] do''[ si' la' sol']( |
    fad'2)\trill\melismaEnd fad'4 re'' |
    si'8[\melisma la' si' do''] re''[ do'' re'' si']( |
    mi''2)\melismaEnd mi''4 mi'' |
    la'8[\melisma sol' la' si'] do''[ si' do'' la']( |
    fa''2)\melismaEnd fa''4 fa'' re''\trill re'' re'' mi'' |
    dod''2.\trill dod''4 |
    re''2 re''4 re'' |
    si'2\trill si'4 mi'' |
    fa''2. mi''8[ re''] |
    mi''4. fa''8 re''4.\trill do''8 |
    do''2
  }
  \tag #'voix2 {
    <>^\markup\character Neptune do8 do |
    fa2 fa4 re |
    la2 la4 la |
    fad2\trill fad4 fad |
    sol2 sol4 sol8 sol |
    re2 re4 re |
    la2 la4 sol |
    fa2 fa8[ mi] fa4 |
    mi2\trill mi4 mi |
    la4. sol8 fa4 mi |
    re2. re4 |
    sol fa mi4.\trill re8 |
    do2. do4 |
    fa fa re re |
    sol2. mi4 |
    re do sol4. sol8 |
    do2. do4 |
    sol8[\melisma fa sol la] si[ la si sol]( |
    do'4)\melismaEnd do' la la |
    re'2. re'4 |
    sol2 sol4 sol |
    do8[\melisma si, do re] mi[ re mi do]( |
    fa2)\melismaEnd fa4 fa |
    re8[\melisma do re mi] fa[ mi fa re]( |
    sib4)\melismaEnd sib sib sol |
    la2. la4 |
    re2 re4 re |
    sol2. sol4 |
    sol1~ |
    sol4. sol8 sol4. sol8 |
    do'2
  }
>>