\piecePartSpecs
#`((dessus #:score-template "score-dessus2")
   (basse #:music ,#{ <>^"[à 3]" #})
   (basse-continue #:music ,#{ <>^"[à 3]" #})
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
