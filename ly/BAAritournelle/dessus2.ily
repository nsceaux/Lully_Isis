\clef "dessus" r4 r8 sib' re''4. re''8 |
<< \sug re''2. \\ sib'2. >> do''8 sib' |
do''4. do''8 fa''4. fa''8 |
fa''4. re''8 sol''4. sol''8 |
mi''4. mi''8 mi''4. mi''8 |
fa''4. sol''8 mi''4.\trill fa''8 |
fa''4. la'8 do''4. sol'8 |
sib'2. sib'4 |
sib'4. do''8 do''4.\trill sib'8 |
la'4.\trill la'8 la'4. sib'8 |
do''4. re''16 la' sib'4. sib'8 |
sib'4. do''8 la'4.\trill sol'8 |
sol'4. re''8 sol''4. sol''8 |
<< sol''2 \\ \sug mi'' >> fa''4. fa''8 |
re''2. re''4 |
do''2 do''4 re'' |
sib'4. sib'8 sib'4. do''8 |
la'2\trill sib'4. sib'8 |
sol'2~ sol'8 sol' la' sib' |
la'4. sib'8 la'4.\trill sib'8 |
sib'4. re''8 re''4. mib''8 |
fa''4. fa''8 fa''2~ |
fa''4 sib' mib''4. mib''8 |
mib''4 re'' do''4.\trill sib'8 |
sib'4. re''8 re''4. mib''8 |
sib'1 |
