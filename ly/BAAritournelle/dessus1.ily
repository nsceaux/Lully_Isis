\clef "dessus" r4 r8 re'' fa''4. fa''8 |
fa''2. mib''8 re'' |
mib''4. mib''8 mib''4 re''8 do'' |
re''4. fa''8 sib''4. sib''8 |
sol''4. sol''8 do'''4. do'''8 |
la''4. sib''8 sol''4.\trill fa''8 |
fa''4. do''8 mib''4. mib''8 |
re''4.\trill re''8 sol''4. sol''8 |
sol''4. la''8 la''4.\trill sol''8 |
fad''4.\trill fad''8 fad''4. sol''8 |
la''2 re''4. re''8 |
sol''4. la''8 fad''4.\trill sol''8 |
sol''2 sib''4. sib''8 |
sib''2 la''4. la''8 |
la''2 sol''4. la''16 fa'' |
mi''2\trill fa''4. fa''8 |
fa''4. sol''16 re'' mib''4. mib''8 |
mib''4. re''16 do'' re''4. re''8 |
re''2 do''4. do''8 |
do''4. re''8 do''4.\trill sib'8 |
sib'4. fa''8 fa''4. sol''8 |
lab''4. lab''8 lab''4. sol''16 fa'' |
sol''4 sol'' sol''4. sol''8 |
la''4 sib'' la''4.\trill sib''8 |
sib''4. fa''8 fa''4. sol''8 |
sib''1 |
