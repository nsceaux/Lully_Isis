\clef "bass" sib,1~ |
sib,4. fa8 sib4. sib8 |
sib2 la4. la8 |
sib4. sib8 sol4. sol8 |
do'4. do'8 la4. la8 |
re'4. sib8 do'4 do |
fa4. fa8 do4. do8 |
sol2. sol,4 |
mib4. mib8 do4. do8 |
re4. la8 re'4. re'8 |
fad2 sol4. sol8 |
mib'4 do' re' re |
sol2~ sol8 sol la sib |
do'2 fa8 fa sol la |
sib,2 sib4. sib8 |
sib2 la4 sib |
sol2~ sol8 sol fa mib |
fa2 sib,8 sib, do re |
mib2 mi |
fa4. sib,8 fa,2 |
sib, sib4. sib8 |
re2 re4. re8 |
mib4. mib8 do4. do8 |
fa4 sib, fa,2 |
sib,2 sib4. sib8 |
sib,1 |
