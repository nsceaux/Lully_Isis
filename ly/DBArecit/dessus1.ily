\clef "dessus" R1 R2. R1 R1 R1 R1*2 R2.*2 R1 R2. R1 R1*2 R2. R1 R2. R1
R1 R1 R2.*2 R4. |
r16^"Violons" la'' la''8. la''16 |
fa''8. fa''16 mi'' re'' |
dod'' si' dod'' re'' mi'' dod'' |
re''8 la'16 sol' la' si' |
do''8 re''8. re''16 |
si'8 sol' re'' |
si' do'' do'' |
la' si' sold' |
la' la' sold' |
la'4. |
la'4. |
fa''4 fa''8 |
re''16 do'' re'' mi'' fa'' sol'' |
mi'' fa'' sol'' la'' sib'' sol'' |
la'' sib'' la'' sol'' fa''8 |
la''8 la''8. la''16 |
sol''8. sol''16 la''8 |
fad''16 mi'' fad'' sol'' la'' fad'' |
sol'' la'' sib'' la'' sol'' fa'' |
mi''4 mi''8 |
fa'' la'' la'' |
la''8. sib''16 sol''8\trill |
la''8. sib''16 la'' sol'' |
fa''8 fa'' sol'' |
la''4 la''8 |
re''4 re''8 |
sol''16 la'' sol'' fa'' mi'' re'' |
do''8. do''16 fa''8 |
re''8. do''16 re'' mi'' |
fa'' mi'' re'' dod'' re'' mi'' |
dod'' re'' mi'' fa'' sol'' mi'' |
fa''4 fa''16 sol'' |
mi''8 la''16 sib'' la'' sol'' |
fa''4 fa''8 |
fa''8. sol''16 mi''8\trill |
re''4. |
R1 R2. R1 R1 R2.*2 R1 R1 R2. R1*2 R2.*2 R1 R1 R2.*2 R1*34
