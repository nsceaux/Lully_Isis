\clef "dessus" R1 R2. R1 R1 R1 R1*2 R2.*2 R1 R2. R1 R1*2 R2. R1 R2. R1
R1 R1 R2.*2 R4. | \allowPageTurn
r16 fa'' fa''8. fa''16 |
re''8. re''16 sol''8 |
mi''16 re'' mi'' fa'' sol'' mi'' |
fa''8 fa''16 mi'' fa'' sol'' |
mi''8 fa''8. fa''16 |
re''8 si' sol' |
re'' mi'' mi'' |
do'' re'' si' |
do'' si' do''16 re'' |
dod''4.\trill |
dod''4.\trill |
re''4 re''8 |
si'16 la' si' do'' re'' si' |
do'' re'' mi'' fa'' sol'' mi'' |
fa'' mi'' fa'' sol'' la''8 |
fa''8 fa''8. fa''16 |
mi''8. re''16 do'' sib' |
la' sol' la' sib' do'' la' |
sib' do'' re'' do'' sib' la' |
sol'8 do''16 re'' do'' sib' |
la'8 fa'' fa'' |
fa''8. sol''16 mi''8\trill |
fa''8. sol''16 fa'' mi'' |
re''8 re'' re'' |
do''16 la' re'' do'' sib' la' |
sib'4 sib'8 |
sib'16 la' sib' sol' do'' sib' |
la'8\trill la'' la'' |
fa''8. fa''16 sib''8 |
la''16 sol'' fa'' mi'' fa'' sol'' |
mi'' re'' dod'' re'' mi'' dod'' |
re''4 re''16 mi'' |
dod''4\trill dod''8 |
re''4 re''8 |
re''8. mi''16 dod''8\trill |
re''4. |
R1 \allowPageTurn R2. \allowPageTurn R1 \allowPageTurn R1
\allowPageTurn R2.*2 \allowPageTurn R1 \allowPageTurn R1
\allowPageTurn R2. \allowPageTurn R1*2 \allowPageTurn R2.*2
\allowPageTurn R1 \allowPageTurn R1 \allowPageTurn R2.*2
\allowPageTurn R1*34
