s1 <2>4 <5/>2 s <7>4 <6>

<_+>1 <6> s2 <6>
s1 s2 <5->4 s <6>4. <5->8
s4 <6+> s <6 5> <_+>2. <6>2 <5->
s <6 5>4 <4>8 <_+> <_+>1 s2.
<_+>1 <5/>2. <_->1
s2. <6>4 s2 <6 5> <4> \new FiguredBass <3>4 s2. <"">4\figExtOn <"">8\figExtOff

s4 <6>8 s4 <6 5>8 <4> <_+>4 s4. s8 <6>\figExtOn <6>\figExtOff s4. s8 <6>4 s8 <6 5> <_+>
<6> <6 5> <_+> <_+>4. <_+> <"">4\figExtOn <"">8\figExtOff s4.*3
<"">16*5\figExtOn <"">16\figExtOff s4. <_+>4\figExtOn <_+>8\figExtOff <_->4. <"">16*5\figExtOn <"">16\figExtOff <"">16*5\figExtOn <"">16\figExtOff <6 4>4 <5 3>8

s4.*2 <5/>4. <_-> <5/> <"">16*5\figExtOn <"">16\figExtOff s8 <_-> <6>
<"">4\figExtOn <"">8\figExtOff <_+>4. s8 <6 5>4 <_+>4. s <6 4>4 <_+>8 s4.
s2 <4+> <6>4 <6> <6> <_+>4. <6>16 <6+> s4 <7>8 <6>
<_+>2 <6>4 <5/> s <6> <5/> <_+> <6>8 <6+> <_+>4

s2. <_+>8\figExtOn <_+>\figExtOff <6>1 s4 <6> <7>8 <6>
<"">4.\figExtOn <"">8\figExtOff <6>2 <6 5>2 <4>4 \new FiguredBass <3> s2 <4+>4 <6>
<5/> <_-> <_+>2 s8. <6>16 <7>8 <6> <_+>2 <6>4 <5+>
s <5/>2 <_+>8.\figExtOn <_+>16\figExtOff <6>8 <6+> <_+>4 s1 <5/>1
<"">4.\figExtOn <"">8\figExtOff <"">4.\figExtOn <"">8\figExtOff <6 5>2 <4>4 \new FiguredBass <3> s1 s2 <_->
<6 5>2 <_+> s1 <6>2 <6 5> s <_-> s4. <6>8 <"">\figExtOn <"">\figExtOff <_->4

<_+>2 <_+>4.\figExtOn <_+>8\figExtOff <6>2 <5/> <_->2 s4. <6>8 <7>2 <6>4\figExtOn <6>\figExtOff <_+>1
<5/> <"">4\figExtOn <"">2.\figExtOff <6 5>2 <4>4 \new FiguredBass <3> s1 <"">8\figExtOn <"">\figExtOff
s4 <_->2 <6 5> <4>4 <_+> s4 <"">8*5\figExtOn <"">8\figExtOff <_+>2 <6> s <5/> <_!>
<6> s <6 5>4.\figExtOn <6>8\figExtOff <_+>2 <4>4 <7 _+> s1 s2 <6> s1
s2 <_+>4.\figExtOn <_+>8\figExtOff <_+>2 <4>4 <3+>
