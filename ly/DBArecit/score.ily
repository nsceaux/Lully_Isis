\score {
  \new ChoirStaff <<
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'voix0 \includeNotes "voix"
    >>  \keepWithTag #'voix0 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix1 \includeNotes "voix"
    >> \keepWithTag #'voix1 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1 s2. s1\pageBreak
        s1*3\break s1 s2.*2\break s1 s2. s1\break
        s1*2 s2.\break s1 s2. s1\break s1*2 s2.*2 s4.\pageBreak
        s4.*8\break s4.*7\break s4.*7\pageBreak
        s4.*7\break s4.*7\break s1 s2. s1\break s1 s2.*2\pageBreak
        s1*2 s2.\break s1*2 s2. s4 \bar "" \break s2 s1*2\break
        s2.*2 s1 s2 \bar "" \break s2 s1*4\break s1*5\pageBreak
        s1*4 s2 \bar "" \break s2 s1*4 s4 \bar "" \break
        s2. s1*4 s2 \bar "" \break s2 s1*4 s2 \bar "" \break
      }
    >>
  >>
  \layout { }
  \midi { }
}
