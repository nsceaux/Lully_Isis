\tag #'(voix1 basse) {
  La per -- fi -- de craint ma pré -- sen -- ce,
  el -- le me fuit en vain, & j’i -- ray la cher -- cher…
  
  Non.
  
  Lais -- sez- moy luy re -- pro -- cher
  sa cru -- elle in -- cons -- tan -- ce.
  
  Non, non, on ne la doit point voir.
  
  Quoy ! Ju -- non me de -- vient con -- trai -- re ?
  
  L’ordre est ex -- prés pour tous, per -- dez un vain es -- poir.
  
  L’A -- mi -- tié fra -- ter -- nelle a si peu de pou -- voir ?
  
  Non, je ne con -- nois plus ny d’a -- my, ny de fre -- re,
  je ne con -- nois que mon de -- voir.
  Lais -- sez la Nymphe en paix, ce n’est plus vous qu’elle ai -- me.
  
  Quel est l’heu -- reux A -- mant qui s’en est fait ai -- mer ?
  Nom -- mez- le- moy.
  
  Trem -- blez à l’en -- ten -- dre nom -- mer,
  c’est un Dieu tout- puis -- sant, c’est Ju -- pi -- ter luy- mê -- me.
  
  O Dieux !
  
  Dé -- ga -- gez- vous d’un a -- mour si fa -- tal,
  sans ba -- lan -- cer, sans ba -- lan -- cer, il faut vous y re -- sou -- dre :
  - sou -- dre :
  C’est un re -- dou -- ta -- ble ri -- val
  qu’un a -- mant qui lan -- ce, qui lan -- ce la fou -- dre,
  qui lan -- ce la fou -- dre.
  C’est un re -- dou -- ta -- ble ri -- val
  qu’un a -- mant qui lan -- ce la fou -- dre,
  qui lan -- ce la fou -- dre.
  
  Dieux tous puis -- sants ! ah ! vous é -- tiez ja -- loux
  de la fe -- li -- ci -- té que vous m’a -- vez ra -- vi -- e,
  Dieux tous puis -- sants ! ah ! vous é -- tiez ja -- loux
  de me voir plus heu -- reux que vous.
  Vous n’a -- vez pû souf -- frir le bon- heur de ma vi -- e,
  et je voy -- ois vos gran -- deurs sans en -- vi -- e :
  J’ai -- mois, j’é -- tois ai -- mé, mon sort é -- toit trop doux ;
  Dieux tous puis -- sants ! ah ! vous é -- tiez ja -- loux
  de la fe -- li -- ci -- té que vous m’a -- vez ra -- vi -- e,
  Dieux tous puis -- sants ! ah ! vous é -- tiez ja -- loux
  de me voir plus heu -- reux que vous.
  
  Heu -- reux, heu -- reux qui peut bri -- ser, qui peut bri -- ser sa chaî -- ne !
  Heu -- reux, heu -- reux qui peut bri -- ser sa chaî -- ne !
  Fi -- nis -- sez u -- ne plain -- te vai -- ne,
  mé -- pri -- sez l’in -- fi -- de -- li -- té ;
  un cœur in -- grat vaut- il la pei -- ne
  d’ê -- tre tant re -- gret -- té.
  Heu -- reux, heu -- reux qui peut bri -- ser, qui peut bri -- ser sa chaî -- ne !
  Heu -- reux, heu -- reux qui peut bri -- ser sa chaî -- ne !
}
\tag #'(voix0 basse) {
  Heu -- reux, heu -- reux, heu -- reux, heu -- reux qui peut bri -- ser,
  qui peut bri -- ser sa chaî -- ne.
  Heu -- reux qui peut bri -- ser,
  heu -- reux, heu -- reux qui peut bri -- ser sa chaî -- ne !
}
\tag #'voix1 {
  Heu -- reux, heu -- reux qui peut bri -- ser sa chaî -- ne.
  Heu -- reux, heu -- reux qui peut bri -- ser sa chaî -- ne !
}
