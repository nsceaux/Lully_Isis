\clef "basse" re,2 \once\tieDashed re~ |
re4 dod2 |
re4. do8 sib,2 |
la,1 |
fad, |
sol,2 mi, |
fa,1 | \allowPageTurn
sib,2 si,4 |
do mi2 |
fa4 mi re2 |
mi2. |
do2 sold, |
la, re4 mi8 mi, |
la,1~ |
la,2. |
re1 |
fad,2. |
sol,1 |
do2. la,4 |
sib,1 |
do2 do,4 |
fa,2. |
fa8. sol16 fa mi |
re8-\tag #'bc-part ^"[à 3]" re' la |
sib sib sol |
la la la, |
re4. |
la8 fa re |
sol4. sol8 mi do |
fa re mi |
do re mi |
la,4. |
la,4 la,8 |
re8. mi16 fa re |
sol4 sol,8 |
do4 do8 |
fa4 \sugRythme { do''16\rest si' } fa8 |
re16 mi fa sol la sib |
do'8. sib16 la8 |
re'8. re'16 re8 |
sol4~ sol16 sol |
do' re' do' sib la sol |
fa la sol fa mi re |
do8. sib,16 do8 |
fa4 fa8 |
sib4 sib8 |
fad4 fad8 |
sol4 sol8 |
\mergeDifferentlyDottedOn
<< \sugNotes { mi4~ mi16 mi } \\ mi4. >> |
fa16 mi fa sol la fa |
sib do' sib la sib sol |
re'8. re'16 re8 |
la4~ la16 la |
re' do' sib la sib sol |
la sib la sol fa mi |
re fa mi re do sib,? |
la,8. sol,16 la,8 |
re,4. |
\tag #'bc-part <>^"[B.C.]"
re2 do |
sib,4 fad, sol, |
la,~ la,16 sol, fa, mi, re,8 re16 do sib,4 |
la, la8 sol fad2 |
sol4 sold2 |
la8. sol16 fa8 mi16 re la,4 |
re2 la,4 la8 sol |
fad1 | \allowPageTurn
sol4 mi8 fa re4 |
do4. sib,8 la,2 |
sib, do4 do, |
fa,8 fa16 mi re4 do |
sib, fad, sol, |
la,~ la,16 sol, fa, mi, re,8 re16 do sib,4 |
la,4 la8 sol fad2 |
sol4 sold2 |
la8. sol16 fa8 mi16 re la,4 |
<< { re4 re, } \\ \sug re,2 >> re2 |
mi r4 do |
fa8 mi re do sib, la, sol, fa, |
sib,2 do4 do, |
fa,2 fa,4 fa8 mi |
re do sib, la, sol, fa, mi, re, |
sol,2 la, |
re, re |
la, sib, |
do sol, |
re4. do8 sib, la, sol,4 |
la,2 re4. do8 |
sib,2 fad, |
sol, do4. la,8 |
sib,2. sol,4 |
la,2 re |
mi2. do4 |
fa8 mi re do sib, la, sol, fa, |
sib,2 do4 do, |
fa,2. fa8 mi |
re do sib, la, sol, fa, mi, re, |
sol,2 la, |
re,4 re8 dod re mi fa sol |
la4. la8 fa4 re |
sib4. sib8 fad4. fad8 |
sol4. sol8 mi4. mi8 |
fa sol fa mi re mi fa re |
mi4 la, mi,2 |
la,4 la fa4. re8 |
sol4. sol8 mi4. do8 |
fa4. fa8 re4. re8 |
sol4. mi8 la4 sol8 fa |
mi re do sib, la,2 |
re,2. mi,4 |
\once\set Staff.whichBar = "|"
\time 3/8
fa,4. |
