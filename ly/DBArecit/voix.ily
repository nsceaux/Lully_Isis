<<
  \tag #'(voix1 basse) {
    \ffclef "vbasse-taille" <>^\markup\character Hierax
    r4 r8 la16 la re'8. re'16 fa8 fa16 sol |
    mi8\trill mi la la16 sol fa8 mi |
    fa4 fa8 la re4 re8 mi |
    dod4\trill
    \ffclef "vbasse-taille" <>^\markup\character Argus
    la4
    \ffclef "vbasse-taille" <>^\markup\character Hierax
    r8 mi16 mi la la si dod' |
    re'4 la8 la do'4 do'8 si |
    si8.\trill si16
    \ffclef "vbasse-taille" <>^\markup\character Argus
    sol4 do'8 r16 mi mi mi fa sol |
    do4
    \ffclef "vbasse-taille" <>^\markup\character Hierax
    la8 r16 do' fa8 fa16 fa fa8 la |
    re8 re
    \ffclef "vbasse-taille" <>^\markup\character Argus
    sib8 sib16 sib sol8 sol |
    mi\trill r16 sol do'8 sol la sib |
    la\trill
    \ffclef "vbasse-taille" <>^\markup\character Hierax
    do'16 do' dod'8\trill dod'16 dod' re'8 re'16 re' si8\trill si16 si |
    sold4\trill
    \ffclef "vbasse-taille" <>^\markup\character Argus
    si4 mi16 mi mi mi |
    la4 la8 la si4\trill si8 si |
    do' do' r16 la la mi fa8 re mi16[ re] mi mi( |
    la,4) r8 la mi8.\trill mi16 mi8 mi |
    dod4\trill r16 mi mi mi la8. la16 |
    fad8.\trill fad16
    \ffclef "vbasse-taille" <>^\markup\character Hierax
    r8 re' la\trill la la sib |
    do'4 la8 la16 la re'8 la |
    sib2 r8 sib sib re' |
    sol4
    \ffclef "vbasse-taille" <>^\markup\character Argus
    r8 do' mi\trill mi16 mi fa8 fa16 fa |
    re4\trill fa8 fa sib4 sib8 re' |
    sol4\trill sol8 sol16 sol do'8 do |
    fa4 fa8 r
    \ffclef "vbasse-taille" <>^\markup\character Hierax
    r do' |
    la4.\trill |
    \ffclef "vbasse-taille" <>^\markup\character Argus
    r16 re' re'8 la |
    sib sib sol |
    la la la, |
    re4. |
    la8 fa re |
    sol4. |
    sol8 mi do |
    fa re mi |
    do re mi |
    la,4 la,8 |
    la,4 la,8 |
    re8. mi16 fa re |
    sol4 sol16 sol |
    do4 do16 do |
    fa4 r16 fa |
    re[ mi fa sol la sib]( |
    do'8.) sib16 la8 |
    re'8. re'16 re8 |
    sol4 sol16 sol |
    do'[\melisma re' do' sib la sol] |
    fa[ la sol fa mi re]( |
    do8.)\melismaEnd sib,16 do8 |
    fa4 fa8 |
    sib8. sib16 sib sib |
    fad4\trill fad16 fad |
    sol4 sol16 sol |
    mi4\trill r16 mi |
    fa[\melisma mi fa sol la fa] |
    sib[ do' sib la sib sol]( |
    re'8.)\melismaEnd re'16 re8 |
    la4 la16 la |
    re'16[\melisma do' sib la sib sol] |
    la[ sib la sol fa mi] |
    re[ fa mi re do sib,]( |
    la,8.\trill)\melismaEnd sol,16 la,8 |
    re4 re8 |
    \ffclef "vbasse-taille" <>^\markup\character Hierax
    r4 fa fad4. fad8 |
    sol4 re'8 la16 sib sol8.\trill fa16 |
    mi8.\trill dod16 dod dod re mi fa fa sol la la8.\trill sol16 |
    la4 la la si8 do' |
    si4\trill mi' si16 si do' re' |
    dod'8\trill la16 la re'8 mi'16 fa' dod'8.\trill re'16 |
    re'8 r16 fa fa sol la si do'8 do'16 do' dod'8\trill dod'16 mi' |
    la4\trill la re' la8 re' |
    si\trill sol16 sol do'4 do'8 si |
    do'8. do'16 r8 mi fa4 r16 fa sol la |
    re4 r8 sib sol\trill sol sol la |
    fa4 r8 fa fad8. fad16 |
    sol4 re'8 la16 sib sol8.\trill fa16 |
    mi8.\trill dod16 dod dod re mi fa fa sol la la8.\trill sol16 |
    la4 la r8 la si do' |
    si4 mi' si16 si do' re' |
    dod'8\trill la16 la re'8 mi'16 fa' dod'8.\trill re'16 |
    re'4.
    \ffclef "vbasse-taille" <>^\markup\character Argus
    la8 fa4 re |
    sib4. sol8 do'4. sol8 |
    la4. do'8 re'[ do'] sib[ la] |
    re'[ do'] sib[ la] sol2\trill |
    fa4 r8 do' la4.\trill la8 |
    re'4 re'8[ do'] sib[ la] sol[ fa] |
    sib[ la] sol[ fa] mi2\trill |
    re r4 la8 sib |
    do'4 do'8 do' sib4.\trill la8 |
    sol4\trill sol r re8 mi |
    fa4 sol8 la sol4.\trill fa8 |
    mi4.\trill la8 fad4\trill fad |
    sol sol la8[ sol] la la( |
    sib4) sol mi4. fa8 |
    re2 re4. mi8 |
    dod4.\trill la8 fa4 re |
    sib4. sol8 do'4. sol8 |
    la4. do'8 re'[ do'] sib[ la] |
    re'[ do'] sib[ la] sol2\trill |
    fa4. do'8 la4.\trill la8 |
    re'4 re'8[ do'] sib[ la] sol[ fa] |
    sib[ la] sol[ fa] mi2\trill |
    re
  }
  \tag #'voix0 {
    \clef "vbasse-taille" R1 R2. R1 R1 R1 R1*2 R2.*2 R1 R2. R1 R1*2 R2. R1 R2. R1 R1 R1
    R2.*2 R4. R4.*9 R4. R4. R4.*25 R1 R2. R1 R1 R2.*2 R1 R1 R2. R1*2
    R2.*2 R1 R1 R2.*2 R1*22 r2
  }
>>
<<
  \tag #'(voix0 basse) {
    <>^\markup\character Hierax r4 re' |
    dod' la re'2 |
    r4 re' do'8\trill[ si] do'4 |
    si\trill si do' do' |
    la4.\trill la8 re'4. si8 |
    sold4.\trill la8 si[\melisma do' re' si] |
    do'[ re' do']\melismaEnd si( la4.) la8 |
    si4.\trill si8 do'4. mi'8 |
    la4. la8 re'4. re'8 |
    si4. mi'8 dod'4 dod' |
    re'8[ dod'] re'4 re'( dod') |
    re'1 |
  }
  \tag #'voix1 {
    r2 |
    r4 la fa re |
    sib4. sib8 fad4.\trill fad8 |
    sol4. sol8 mi[\melisma fa sol mi] |
    fa[ sol fa mi] re[ mi fa re] |
    mi[ re mi fa] mi4.\trill\melismaEnd mi8( |
    la,4.) la8 fa4. re8 |
    sol4. sol8 mi4.\trill do8 |
    fa4. fa8 re[\melisma mi fa re] |
    sol[ fa sol mi] la4 sol8[ fa] |
    mi[ re do sib,]( la,2)\melismaEnd |
    re1 |
  }
>>
\omit Staff.TimeSignature
