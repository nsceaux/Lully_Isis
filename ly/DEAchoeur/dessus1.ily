\clef "dessus" <>^"Violons" R4.*2 |
r8 fa'' fa'' |
re''8. re''16 sol''8 |
mi'' mi'' fa'' |
re''8.\trill mi''16 fa''8 |
mi''\trill mi'' fad'' |
sol'' re'' mi'' |
fa''4. |
r8 fa'' mib'' |
re''8.\trill re''16 sol''8 |
mi''8.\trill mi''16 la''8 |
fa''4 fa''8 |
fa''8. sol''16 mi''8 |
fa'' r r |
R4.*64 |
r8 r
