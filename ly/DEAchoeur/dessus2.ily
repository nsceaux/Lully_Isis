\clef "dessus" R4. |
r8 do'' do'' |
la'8. la'16 re''8 |
si'8. si'16 mi''8 |
do''4 do''8 |
do''8. do''16 si'8 |
do''8 sol' la' |
sib' sib' sib' |
la' la' sib' |
do''4 do''8 |
fa'8. fa'16 sib'8 |
sol'8. sol'16 do''8 |
la'8.\trill la'16 re''8 |
sol'8. la'16 sib'8 |
la'\trill r r |
R4.*64 |
r8 r
