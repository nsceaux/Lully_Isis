\clef "basse" \tag #'basse <>^"[à 3]"
fa,8 fa fa |
mi8. re16 do8 |
fa re re |
sol8. sol16 mi8 |
la la fa |
sol8. fa16 sol8 |
do do' do' |
sol sol sol |
re' re' re' |
la la la |
sib8. la16 sol8 |
do'8. sib16 la8 |
re'8. do'16 sib8 |
do'8. sib16 do'8 |
fa8
\setMusic #'choeur {
  \clef "alto" fa'8[ fa'] |
  mi'8. re'16 do'8 |
  fa'8 fa' re' |
  sol'8. sol'16 mi'8 |
  la'8. la'16 fa'8 |
  sol'8. fa'16 sol'8 |
  do'4 do'8 |
  re' re' re' |
  mib'4 do'8 |
  re' re' sib |
  do' re'16[ do'] re'8 |
  sol8 sol' sol' |
  re' re' re' |
  la la la |
  sib sib sib |
  do' do' do' |
  re'8. do'16 sib8 |
  do'8. sib16 do'8 |
}
\tag #'basse <>^"[B.C.]"
\keepWithTag #'(conducteur basse basse-continue) \choeur
fa4 \clef "bass" fa8 |
mi4 fa8 |
sib,4. |
do4 do,8 |
fa,4 fa16 sol |
la8 sib4 |
do' sib16 la |
sol fa mi4 |
fa8. mib16 re8 |
do16 sib, fa8 fa, |
sib,8. do16 re mib |
fa4. |
mi |
fa8 re sol |
do4. fa16 mib re8. do16 |
sib,8. la,16 sol,8 |
do4 la,8 |
sib, do do, |
fa,4 fa8 |
mi4. |
re8. do16 sib,8 |
do4 do,8 |
fa,4 fa8 |
mi8. re16 do8 |
fa16 mi fa sol la sib |
do' re' do' sib do' la |
re'8. do'16 sib8 |
do'4 do8 |
fa8
\choeur
fa4

