<<
  \tag #'(vdessus1 syrinx) \clef "vdessus"
  \tag #'vdessus2 \clef "vbas-dessus"
  \tag #'(vhaute-contre basse) \clef "vhaute-contre"
  \tag #'(syrinx vdessus1 vdessus2 vhaute-contre basse) { R4.*14 }
>>
\setMusic #'choeur <<
  \tag #'(vdessus1 basse) {
    <<
      \tag #'basse { mi'8 \ffclef "vbas-dessus" do''8 do'' | la' \ffclef "vdessus" }
      \tag #'vdessus1 { R4. | r8 }
    >> fa''8 fa'' |
    re''8.\trill re''16 sol''8 |
    mi''8.\trill mi''16 fa''8 |
    re''8.\trill mi''16 fa''8 |
    mi''4\trill do''8 |
    la'\trill la' re'' |
    sib'4 do''8 |
    la'\trill la' re'' |
    do''16[ sib'] la'8.\trill sol'16 |
    sol'8 re'' mi'' |
    fa''4. |
    r8 fa'' mib'' |
    re'' sib' la' |
    sol'8. sol'16 do''8 |
    la'8. la'16 re''8 |
    sol'8. la'16 sib'8 |
    la'4
  }
  \tag #'vdessus2 {
    r8 do'' do'' |
    la'8.\trill la'16 re''8 |
    si'8.\trill si'16 mi''8 |
    do''8. do''16 do''8 |
    do''8. do''16 si'8 |
    do''4 mi'8 |
    fad' fad' fad' |
    sol'4 la'8 |
    fad'8\trill fad' sib' |
    la'16[ sol'] fad'8.\trill sol'16 |
    sol'8 sib' sib' |
    la' la' sib'  |
    do'' do'' do'' |
    fa' sol' fa' |
    mi'8. mi'16 la'8 |
    fa'8. fa'16 fa'8 |
    fa'8. fa'16 mi'8\trill |
    fa'4
  }
  \tag #'vhaute-contre {
    mi'8. re'16 do'8 |
    fa'8 fa' re' |
    sol'8. sol'16 mi'8 |
    la'8. la'16 fa'8 |
    sol'8. fa'16 sol'8 |
    do'4 do'8 |
    re' re' re' |
    mib'4 do'8 |
    re' re' sib |
    do' re'16[ do'] re'8 |
    sol8 sol' sol' |
    re' re' re' |
    la la la |
    sib sib sib |
    do' do' do' |
    re'8. do'16 sib8 |
    do'8. sib16 do'8 |
    fa4
  }
  \tag #'syrinx { R4.*17 r8 r }
>>
<<
  \tag #'(vhaute-contre basse) { r8 fa' fa' | }
  \tag #'vdessus1 { s8 <>^\markup\character Chœur }
  \tag #'(vdessus1 vdessus2) << { s8 \noHaraKiri } R4. >>
  \tag #'syrinx { R4. }
>> \keepWithTag #'(vdessus1 vdessus2 vhaute-contre basse syrinx) \choeur
\revertNoHaraKiri
<<
  \tag #'(syrinx basse) {
    <>^\markup\character-text Syrinx [Lentement] fa'8 | \noBreak
    do''8. do''16 sib' la' |
    sib'4 sib'16 la' |
    sol'8\trill la'8. sib'16 |
    la'4\trill la'16 sib' |
    do''8 re''16[ do''] sib'[ la'] |
    sol'4\trill sol'8 |
    r16 sib' sib'8. do''16 |
    la'4\trill sib'8 |
    do''16 re'' do''8.\trill sib'16 |
    sib'4. |
    la'8 la' sib' |
    sol'8.\trill sol'16 sol'8 |
    la' fa'\trill[ mi'16] fa' |
    mi'4\trill mi'16 do'' |
    la'8 sib'8. do''16 |
    re''4 sib'8 |
    sol'\trill sol' do'' |
    sib'16[ la'] sol'8.\trill fa'16 |
    fa'8. la'16^\markup\italic [guay] fa'8 |
    do''8. re''16 mi''8 |
    fa''8. la'16 sib'8 |
    sol'8.\trill sol'16 do''8 |
    la'8.\trill do''16 do''8 |
    do''4.~ |
    do''~ |
    do''8. do''16 do''8 |
    fa''8. la'16 sib'8 |
    sol'8.\trill la'16 sib'8 |
    la'\trill <<
      \tag #'basse { \ffclef "vhaute-contre" fa' fa' }
      \tag #'syrinx { r r }
    >>
  }
  \tag #'vdessus1 { s8 s4.*28 s8 <>^\markup\character Chœur }
  \tag #'(vdessus1 vdessus2) { r8 R4.*28 \noHaraKiri R4. }
  \tag #'vhaute-contre { r8 | R4.*28 | r8 fa' fa' | }
>>
\choeur
