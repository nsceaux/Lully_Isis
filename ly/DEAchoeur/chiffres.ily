\simultaneous {
  \tag #'bc {
    s4. <6>8.\figExtOn <6>\figExtOff s4. <_+> s4 <5>8 <4>4 <_+>8 s4 <4+>8 s4 <6>8 s4 <6>8 <6>4 <5/>8
    <"">8.\figExtOn <"">\figExtOff <"">8.\figExtOn <"">\figExtOff <"">8.\figExtOn <"">\figExtOff
    <4>4 <3>8
    s4.*19
    <6>4. s4 <6 5>8
    <4>4 <3>8 s4. <6>8 <6 5>4 <"">16*5\figExtOn <"">16\figExtOff s8 <5/>4 s <6>8
    <6 _->8 <4> <3> <"">16*5\figExtOn <"">16\figExtOff s4. <6> s8 <7> <_+> s4. s8 <6>8. <6>16
    s4. s4 <6>8 s <4> <3> s4. <6> s4 <6 5>8 <4>4 <3>8 s4.
    <6> s4.*2 s4 <6 5>8 <4>4 <3>8
  }
  \tag #'bc {
    %\bassFigureStaffAlignmentUp
    s4.*15
    <6>8.\figExtOn <6>\figExtOff s4. <_+>8.\figExtOn <_+>\figExtOff s4 <6 5>8 <4>4 <_+>8 s4. <_+>
    s4 <6 5 _->8 <_+>4 <6>8 <6 5> <_+>4 s <6>8 s4 <6>8 <6>4. s8 <6> <7>
    s4 <6>8 s4 <6 5>8 <4>4.
    s4.*30
    <6>8.\figExtOn <6>\figExtOff s4. <_+>8.\figExtOn <_+>\figExtOff s4 <6 5>8 <4>4 <_+>8 s4. <_+>
    s4 <6 5 _->8 <_+>4 <6>8 <6 5> <_+>4 s <6>8 s4 <6>8 <6>4. s8 <6> <7>
    s4 <6>8 s4 <6 5>8 <4>4.
  }
}
