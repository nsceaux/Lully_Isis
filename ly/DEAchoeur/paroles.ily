\tag #'(vdessus1 vdessus2 vhaute-contre basse) {
  Li -- ber -- té, li -- ber -- té, li -- ber -- té,
  \tag #'(vhaute-contre basse) { li -- ber -- té, li -- ber -- té, }
  \tag #'vdessus2 { li -- ber -- té, }
  li -- ber -- té.
  S’il est quel -- que bien au mon -- de,
  c’est la li -- ber -- té.
  Li -- ber -- té, li -- ber -- té, li -- ber -- té, li -- ber -- té, li -- ber -- té,
  \tag #'(vdessus2 vhaute-contre) { li -- ber -- té, }
  li -- ber -- té.
}

\tag #'(syrinx basse) {
  L’Em -- pi -- re de l’A -- mour n’est pas moins a -- gi -- té
  que l’Em -- pi -- re de l’on -- de,
  ne cher -- chons point d’au -- tre fe -- li -- ci -- té
  qu’un doux loi -- sir dans u -- ne paix pro -- fon -- de.
  S’il est quel -- que bien au mon -- de,
  c’est la li -- ber -- té.
  Li -- ber -- té, li -- ber -- té, li -- ber -- té, li -- ber -- té, li -- ber -- té. __
  Li -- ber -- té, li -- ber -- té, li -- ber -- té.
}

\tag #'(vdessus1 vdessus2 vhaute-contre basse) {
  Li -- ber -- té, li -- ber -- té, li -- ber -- té,
  \tag #'(vhaute-contre basse) { li -- ber -- té, li -- ber -- té, }
  \tag #'vdessus2 { li -- ber -- té, }
  li -- ber -- té.
  S’il est quel -- que bien au mon -- de,
  c’est la li -- ber -- té.
  Li -- ber -- té, li -- ber -- té, li -- ber -- té, li -- ber -- té, li -- ber -- té,
  \tag #'(vdessus2 vhaute-contre) { li -- ber -- té, }
  li -- ber -- té.
}
