\clef "dessus2" fa'4. fa'8 |
fa'4. mib'16 re' mib'4. fa'8 |
re'2\trill re'4. mi'8 |
fa'4. fa'8 fa'4 sol' |
la' sib'8 la' sol'4.\trill fa'8 |
mi'1\trill |
la'4. sib'8 la'4. sol'8 |
fa'1 |
sib'4. do''8 sib'4. la'8 |
sol'1 |
la'4. sib'8 la'4. sol'8 |
fad'4 sol' sol' fad' |
sol'2 re'4. re'8 |
sol'4. fa'8 fa'4.\trill mi'8 |
mi'1\trill |
la'4. sol'8 fa'4. mi'8 |
re'2 re'4 sol' |
mi'2\trill fa'4. fa'8 |
fa'2 \once\slurDashed mi'4.(\trill re'16 mi') |
fa'2.
