\clef "dessus" la'4. la'8 |
sol'4.\trill sol'8 sol'4 la' |
sib'2. sib'4 |
la'4.\trill la'8 la'4 sib' |
do'' re''8 do'' sib'4.\trill la'8 |
sol'1\trill |
do''4. re''8 do''4. sib'8 |
la'1\trill |
re''4. mib''8 re''4. do''8 |
sib'1 |
do''4. re''8 do''4. sib'8 |
la'4 sib' la'4.\trill la'8 |
sib'2 sib'4. sib'8 |
sib'4. la'8 la'4.\trill sol'8 |
sol'1 |
do''4. do''8 do''2~ |
do'' sib'4. sib'8 |
sib'2 la'4. sib'8 |
sol'2\trill~ sol'8 sol' la' sib' |
la'2.\trill
