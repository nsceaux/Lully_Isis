\clef "basse" fa4. fa8 |
do'2. do'4 |
sol2. sol4 |
%\clef "alto"
re'4. re'8 do'4 sib |
la4. la8 sib2 |
do'4. re'8 do'4. sib8 |
la1 |
re'4. mib'8 re'4. do'8 |
sib1 |
mib'4. fa'8 mib'4. re'8 |
do'1 |
re'4 sol re2 |
%\clef "bass"
sol4. la8 sol4 fa |
mi do fa fa, |
do4. re8 do4 sib, |
la,1 |
sib,2 sol, |
do1 |
do, |
fa,2.
