Ter -- mi -- nez mes tour -- ments, puis -- sant Maî -- tre du mon -- de :
sans vous, sans vôtre a -- mour, he -- las !
je ne souf -- fri -- rois pas.
Re -- duite au de -- ses -- poir, mou -- ran -- te, va -- ga -- bon -- de,
j’ay por -- té mon sup -- plice en mille af -- freux cli -- mats ;
une hor -- ri -- ble Fu -- rie at -- ta -- chée à mes pas,
m’a sui -- vie au tra -- vers du vas -- te sein de l’On -- de.
Ter -- mi -- nez mes tour -- ments, puis -- sant Maî -- tre du mon -- de :
voy -- ez de quels maux i -- cy bas,
vôtre é -- pou -- se pu -- nit mes mal- heu -- reux ap -- pas ;
dé -- li -- vrez- moy de ma dou -- leur pro -- fon -- de,
ou -- vrez- moy par pi -- tié, les por -- tes du tré -- pas.
Ter -- mi -- nez mes tour -- ments, puis -- sant Maî -- tre du mon -- de :
sans vous, sans vôtre a -- mour, he -- las !
je ne souf -- fri -- rois pas.
C’est Ju -- pi -- ter qui m’ai -- me ! eh ! qui le pour -- roit croi -- re ?
Je ne suis plus dans sa mé -- moi -- re,
il n’en -- tend point mes cris, il ne voit point mes pleurs.
A -- prés m’a -- voir li -- vrée aux plus cru -- els mal- heurs,
il est tran -- quile au com -- ble de sa gloi -- re ;
il m’a -- ban -- don -- ne, il m’a -- ban -- donne au mi -- lieu des dou -- leurs.
A la fin je suc -- com -- be,
à la fin je suc -- com -- be ; heu -- reu -- se, heu -- reu -- se, si je meurs.
