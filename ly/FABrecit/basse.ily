\clef "basse" fa4 |
mib reb8 do sib,4 fa8 mib |
reb2 do |
fa la, |
sib,4. sib,8 do reb do do, |
fa,4 fa2 mib8 reb |
do2 reb |
mib2. |
\once\tieDashed do2~ do8 lab, |
reb2 re |
mib4. reb8 do4. sib,8 |
lab,4 sol,2 |
lab,4 lab8 sol fa2 |
mib4 reb8 do sib,4 fa8 mib |
reb2 do |
fa4 re mib sol, |
lab,2 sib, |
mib,4 mib8 re do2 |
fa sol4 |
si,2 do4 |
lab8 fa sol4 sol, |
do4. do8 fa2 |
mib4 reb8 do sib,4 fa8 mib |
reb2 do |
fa la, |
sib,4. sib,8 do reb do do, |
fa,1~ | \allowPageTurn
fa,2 fa |
mi1 |
fa2 fa4 mib |
reb8. do16 sib,4 lab, |
\once\tieDashed sol,2~ sol,8 lab, fa,4 |
mib,2 mib4 reb8 do |
sib,4. lab,8 sol,4 |
lab,2 lab |
sol4 do'4. fa8 |
mi2. |
fa |
mib!2 reb |
do la, |
sib, si, |
do4. lab,8 sib,2 |
do4 do, fa,2 |
