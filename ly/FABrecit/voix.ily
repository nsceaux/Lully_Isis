\clef "vbas-dessus" <>^\markup\character-text Io
\line { sortant de la Mer, d’où elle est tirée par la Furie. }
lab'8 lab' |
la'4\trill sib'8 do'' reb''4 do''8 do'' |
fa''4 fa''8 sol'' mi''4\trill mi''8 r16 do'' |
la'4\trill r16 do'' do'' re'' mib''4 mib''8.[ re''16] |
re''4\trill r8 re'' mi'' fa'' fa'' mi'' |
fa''4 r8 fa' do'' do'' do'' reb'' |
mib''4 r8 lab' fa'\trill fa' r reb''16 fa'' |
sib'8\trill sib' r sib'16 sib' mib''8 mib''16 mib'' |
lab'4 lab'8 lab'16 lab' mib'8 lab' |
fa'8\trill fa''16 fa'' reb''8 reb''16 reb'' sib'8\trill sib'16 sib' fa'8 fa'16 sib' |
sol'4\trill mib'8 mib' lab'4 lab'8 sib' |
do''8 lab' mib''8. reb''16 reb''8 do'' |
do''4\trill do'' r lab'8 lab' |
la'4\trill sib'8 do'' reb''4 do''8 do'' |
fa''4 fa''8 sol'' mi''4\trill mi''8 r16 sol' |
lab'8 lab'16 do'' lab'8\trill lab'16 sol' sol'8\trill sib'16 sib' mib''8 mib''16 sib' |
do''4 lab'8 sib'16 do'' sol'4( fa'8.)\trill mib'16 |
mib'2 r8 mib'' mib'' mib'' |
mib''4 re''8 re''16 re'' si'8.\trill re''16 |
sol'8 sol' r re''16 re'' mib''8 mib''16 sol'' |
do''8. fa''16 si'8\trill si' si' do'' |
do''2 r4 lab'8 lab' |
la'4\trill sib'8 do'' reb''4 do''8 do'' |
fa''4 fa''8 sol'' mi''4\trill mi''8 r16 do'' |
la'4\trill r16 do'' do'' re'' mib''4 mib''8.[ re''16] |
re''4\trill r8 re'' mi'' fa'' fa'' mi'' |
fa''2 r |
r lab'8 lab'16 lab' lab'8. sol'16 |
sol'8\trill sol' r4 sol'' do''16 do'' do'' do'' |
la'8\trill la' r16 do'' do'' do'' fa'8 fa' sol' la' |
sib'8 sib'16 reb'' reb''8 reb'' re''8.\trill re''16 |
mib''4 r8 sib' sib' do'' lab'8.\trill sol'16 |
sol'4.\trill mib'8 sib' sib' sib' do'' |
reb'' reb'' reb'' fa'' reb''8.\trill do''16 |
do''4\trill do'' do'' re'' |
mib''8 mib'' mi''\trill mi'' mi'' fa'' |
sol''4 sol''8 r16 do'' do''8 sol' |
lab'4 lab'8 r16 reb'' reb''8 reb'' |
sol'4\trill sol'8 lab' fa'4 fa'8 sol' |
mi'4\trill r8 do''16 do'' fa''4 do''8 do'' |
fa'8 fa' r re''16 re'' sol''4 re''8 re'' |
sol'8.\trill sol'16 r8 do'' reb''8. reb''16 r8 sib' |
sol'\trill sol' sol' lab' fa'4 r |
