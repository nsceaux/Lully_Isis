\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s1*3\break s1*3 s2.\break s2. s1 s2 \bar "" \break
        s2 s2. s1*2\break s1*2 s2 \bar "" \break s2 s1 s2.*2 s2 \bar "" \pageBreak
        s4 s1*3 s2 \bar "" \break s2 s1*4\break s1 s2. s1\break
        s1 s2. s1 s2.\break s2.*2 s1*2\break
      }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
