\key do \minor
\time 4/4 \partial 4 \midiTempo#80 s4 s1*6
\digitTime\time 3/4 s2.*2
\time 4/4 s1
\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2.
\time 4/4 s1*5
\digitTime\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2.*3
\time 4/4 s1*9
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\digitTime\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2.*3
\time 4/4 s1*5 \bar "|."
