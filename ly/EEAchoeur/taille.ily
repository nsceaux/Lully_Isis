\clef "taille" r8 fa' fa' sol'16 la' sib'8 sib'16 la' sol'8 sol' |
sol' sol'16 fa' mi'8 mi' fa' sol' mi'8.\trill fa'16 |
fa'8
\setMusic #'choeur {
  do'8 do' do' sib sib sib sib |
  do' do' mi' mi' mi' mi' mi' mi' |
  re' re' re' re' re' re' re' re' |
  do'2 r8 do' do' do' |
  do' do' do' sib sib sib mib' mib' |
  re' re' re' re' re' re' re' re' |
  mi'4. sol'8 fa'4 la'8 la' |
  sol'4 sol'8 la' fa'4 fa'8 sol' |
  mi'8 mi' mi' mi' re'4. re'8 |
  re'4 re'8 re' do'4 mi'8 mi' |
  fa'4. fa'8 fa'4 fa'8 fa' |
  fa'4 fa'8 sol' sol' sol' sol' la' |
  fa'4 fa'8 sib' sol'4. fa'8 |
  fa'4 r r2 |
  R1 |
  r4 << \sugNotes { mi'8 mi' fa'4 fa'8 sol' } \\ { mi'8 mi' mi'4 mi'8 mi' } >> |
  do'4 do'8 do' re'4 re'8 mib' |
  fa'4 sib8 do' re'4 sib8 sib |
  do'4 do'8 re' mi'4 mi'8 mi' |
  fa'4 fa'8 do'
  \footnoteHere #'(0 . 0) \markup { Ballard 1719 : \italic ré }
  \sug do'4 la8 la |
  sib4 sib8 sib sib4 re'8 re' |
  do'4 do'8 do' re'4 re'8 re' |
}
\keepWithTag #'() \choeur
do'4 r4 r2 |
R2. R1*2 R2. R1*2 R1. R2.*2 R1*2 R2. R1*2 R1. R1 \allowPageTurn
r8 \choeur
do'2 r |
