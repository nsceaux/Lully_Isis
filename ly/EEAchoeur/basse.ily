\clef "basse" \tag #'bc-part <>_"[Tous]" fa,8-\tag #'(basse-continue bc-part) \repeatTie fa fa fa sib sib sib sol |
do' do' do' la re' sib do' do |
fa
\setMusic #'choeur {
  fa8 fa fa sib sib sib sol |
  do' do' do' do' la la la la |
  re' re re re sol sol sol sol |
  do2~ do8 do do do |
  fa fa fa re mib mib mib do |
  re re re re sol, sol, sol sol |
  mi4. mi8 fa4 fa8 fa |
  do4 do'8 la sib4 sib8 sol |
  la8 la la la re4. re8 |
  sol4 sol8 sol do4 do8 do |
  fa4. fa8 re4 re8 re |
  sib,4 sib8 sol do' do' do' la |
  re'4 re'8 sib do'4 do |
  <<
    \tag #'basse <<
      { \voiceOne fa,4 r r2 | R1 | r4 \oneVoice }
      \new CueVoice { \voiceTwo fa,1 | sib,2 si, | do4 }
    >>
    \tag #'(basse-continue bc-part) { \tag #'bc-part <>^"[B.C.]" fa,1 | sib,2 si, | do4 }
  >> \tag #'bc-part <>^"[Tous]" do'8 sib la4 la8 sol |
  fa4 fa8 mib re4 re8 do |
  sib,4 sib8 la sol4 sol8 fa |
  mi4 mi8 re do4 do8 sib, |
  la,4 la8 la fa4 fa8 fa |
  re4 re8 re sib,4 sib,8 sib, |
  la,4 la,8 la, sib,4 sib,8 sib, |
}
\keepWithTag #'(basse basse-continue bc-part) \choeur
<<
  \tag #'basse {
    fa,4 r r2 |
    R2. R1*2 R2. R1*2 R1. R2.*2 R1*2 R2. R1*2 R1. R1 \allowPageTurn r8
  }
  \tag #'(basse-continue bc-part) {
    fa,2*1/2 \tag #'bc-part <>^"[B.C.]" s4 mi,2 |
    re,4 sol,2 |
    do4. sib,8 la, sol, fa,4 |
    do2 fad, |
    sol, do4 |
    re4. mib8 re4. do8 |
    sib,4 la,8 sol, fad,4 sol, |
    mib2. do4 re re, |
    sol,2. |
    do,4 fa,2 |
    sib,2. mib4 |
    re2 do |
    si,2. |
    do4. re8 do4. sib,8 |
    la,8 la sol fa mi4 fa |
    re2. sib,4 do2 |
    fa,1~ | \allowPageTurn
    fa,8 \tag #'bc-part <>^"[Tous]"
  }
>>
\choeur
fa,1 |
