\score {
  <<
    \new StaffGroup \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        \global \keepWithTag #'recit \includeNotes "voix"
      >> \keepWithTag #'recit \includeLyrics "paroles"
      \new Staff <<
        \global \keepWithTag #'basse-continue \includeNotes "basse"
        \includeFigures "chiffres"
        \origLayout {
          s1*3 s2 \bar "" \pageBreak
          s2 s1*2 s4 \bar "" \pageBreak
          s2. s1*3\pageBreak
          s1*3 s2. \bar "" \pageBreak
          s4 s1*4\pageBreak
          s1*3 s2 \bar "" \pageBreak
          s2 s1*2 \pageBreak
          s1 s2. s1\break s1 s2. s1*2 s1 \bar "" \break s2 s2.*2\break
          s1*2 s2. s1\break s1 s1. s1\pageBreak
          s1*4 s4 \bar "" \pageBreak
          s2. s1*3\pageBreak
          s1*3 s2. \bar "" \pageBreak
          s4 s1*4\pageBreak
          s1*3 s2 \bar "" \pageBreak
        }
        \modVersion {
          s1*24 s1 s2. s1*2 s2. s1*2 s1. s2.*2 s1*2 s2. s1*2 s1. s1*11\pageBreak
        }
      >>
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
