s2. <6>4 s <6> s8 <5> <4> <3>
\setMusic #'choeur {
  s1*2
  s2 <_+> s1
  s4. <6>8 s4. <6 5>8 <4>4. <_+>8 <_+>2 <6>1 s4. <6>8 s4. <6 5>8
  <_+>2 <_+> <_+>1 s1*2
  s4. <6 5>8 <4>4 <3> s1 s2 <5/> s4 <"">8\figExtOn <"">\figExtOff <6>4. <6>8
  <"">4.\figExtOn <"">8\figExtOff <6>4. <6 _->8 s1 <6>4. <6+>8 <"">4.\figExtOn <"">8\figExtOff
  <6>1 <6>2.. <6 5>8 <6>1
}
\keepWithTag #'() \choeur
s2 <6> s4 <_+>2 <"">4.\figExtOn <"">8\figExtOff <6>2 s
<5/> s <6 5 _->4 <_+>2..\figExtOn <_+>8\figExtOff <6>4.\figExtOn <6>8\figExtOff <5/>2 <7>2 <6>4\figExtOn <6>\figExtOff
<_+>2 s2. <_->
s2. <6>4 <_+>2\figExtOn <_+>\figExtOff <6>2 <5/>4 s1
\new FiguredBass <6>2 <6> <7>2. <6 5>4 <4> <3> s1
\choeur
