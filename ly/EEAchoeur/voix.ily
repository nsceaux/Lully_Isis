<<
  \tag #'(vdessus basse) {
    \clef "vdessus" <>^\markup\character Chœur de la Suite des Parques
  }
  \tag #'vhaute-contre \clef "vhaute-contre"
  \tag #'vtaille \clef "vtaille"
  \tag #'vbasse \clef "vbasse"
  \tag #'recit \clef "vbasse"
  \tag #'(vdessus vhaute-contre vtaille vbasse recit basse) R1*2
>>
\setMusic #'choeur <<
  \tag #'(vdessus basse) {
    r8 la' la' la' fa' fa' fa' sib' |
    sol'\trill sol' sol' sol' do'' do'' do'' do'' |
    la'\trill la' la' re'' si'\trill si' si' si' |
    do''4 do'' r8 do'' do'' do'' |
    la'\trill la' la' re'' sib' sib' sib' do'' |
    la'\trill la' la' re'' si'\trill si' si' si' |
    do''4. do''8 do''4 do''8 fa'' |
    mi''4\trill mi''8 fa'' re''4\trill re''8 mi'' |
    dod''8\trill dod'' dod'' dod'' re''4. re''8 |
    si'4\trill si'8 si' mi''4 mi''8 mi'' |
    do''4. do''8 fa''4 fa''8 fa'' |
    re''4\trill re''8 re'' mi'' mi'' mi'' mi'' |
    fa''4 fa''8 fa'' fa''4( mi'')\trill |
    fa''4 <<
      \tag #'basse {
        \ffclef "vbasse" do'8 do' la4 \ffclef "vhaute-contre" fa'8 fa' |
        re'4 \ffclef "vbasse" sib8 sib sol4 \ffclef "vhaute-contre" sol'8 sol' |
        mi'4\trill \ffclef "vdessus"
      }
      \tag #'vdessus { r4 r2 | R1 | r4 }
    >> <>^\markup\character Tous sol'8 sol' do''4 do''8 sib' |
    la'4 do''8 do'' fa''4 fa''8 mib'' |
    re''4 re''8 do'' sib'4 sib'8 la' |
    sol'4\trill sol''8 fa'' mi''4\trill mi''8 re'' |
    do''4 do''8 do'' la'4\trill la'8 la' |
    fa'4 fa''8 fa'' re''4\trill re''8 mi'' |
    fa''4 do''8 do'' sib'4 sib'8 sib' |
  }
  \tag #'vhaute-contre {
    r8 fa' fa' fa' re' re' re' sol' |
    mi'\trill mi' mi' mi' mi' mi' mi' mi' |
    fa' fa' fa' fa' sol' sol' sol' sol' |
    mi'4\trill mi' r8 mi' mi' mi' |
    fa' fa' fa' fa' sol' sol' sol' sol' |
    sol' sol' sol' fad' sol' sol' sol' sol' |
    sol'4. sol'8 la'4 la'8 la' |
    sol'4 sol'8 la' fa'4 fa'8 sol' |
    mi'\trill mi' mi' la' fad'4.\trill fad'8 |
    sol'4 sol'8 sol' sol'4 sol'8 sol' |
    fa'4. la'8 la'4 fa'8 fa' |
    fa'4 fa'8 sol' sol' sol' sol' la' |
    la'4 la'8 sib' la'4( sol')\trill |
    fa' r r <>^\markup\character La Famine fa'8 fa' |
    re'4 r r <>^\markup\character L'Inondation sol'8 sol' |
    mi'4\trill mi'8 mi' la'4 la'8 mi' |
    fa'4 fa'8 do' re'4 fa'8 fa' |
    fa'4 fa'8 fa' sol'4 sol'8 re' |
    mi'4 mi'8 fa' sol'4 sol'8 sol' |
    la'4 fa'8 fa' fa'4 do'8 do' |
    re'4 re'8 re' fa'4 fa'8 sol' |
    la'4 fa'8 fa' re'4 re'8 re' |
  }
  \tag #'vtaille {
    r8 do' do' do' sib sib sib sib |
    do' do' do' do' do' do' do' do' |
    re' re' re' re' re' re' re' re' |
    do'4 do' r8 sol sol sol |
    fa do' do' sib sib sib mib' mib' |
    re' re' re' re' re' re' re' re' |
    \footnoteHere #'(0 . 0) \markup { Ballard 1719 : \italic ré }
    \sugNotes { mi'4. mi'8 } do'4 do'8 do' |
    do'4 do'8 do' sib4 sib8 sib |
    la la la la la4. re'8 |
    re'4 re'8 re' do'4 do'8 do' |
    la4. la8 re'4 la8 la |
    sib4 sib8 sib do' do' do' do' |
    re'4 re'8 re' do'2 |
    la4 r r2 |
    R1 |
    r4 do'8 do' do'4 do'8 do' |
    do'4 la8 la sib4 sib8 la |
    sib4 sib8 do' re'4 sib8 sib |
    do'4 do'8 re' mi'4 mi'8 mi' |
    fa'4 do'8 do' do'4 la8 la |
    sib4 sib8 sib sib4 sib8 sib |
    do'4 do'8 fa fa4 fa8 fa |
  }
  \tag #'vbasse {
    r8 fa fa fa sib sib sib sol |
    do' do' do' do' la la la la |
    re' re8 re re sol sol sol sol |
    do4 do r8 do do do |
    fa fa fa re mib mib mib do |
    re re re re sol, sol, sol sol |
    mi4.\trill mi8 fa4 fa8 fa |
    do4 do'8 la sib4 sib8 sol |
    la la la la re4. re8 |
    sol4 sol8 sol do4 do8 do |
    fa4. fa8 re4 re8 re |
    sib,4 sib8 sol do' do' do' la |
    re'4 re'8 sib do'4( do) |
    fa4 <>^\markup\character La Guerre do'8 do' la4\trill r |
    r <>^\markup\character L'Incendie sib8 sib sol4\trill r |
    r do'8 sib la4\trill la8 sol |
    fa4 fa8 mib re4\trill re8 do |
    sib,4 sib8 la sol4 sol8 fa |
    mi4\trill mi8 re do4 do8 sib, |
    la,4 la8 la fa4 fa8 fa |
    re4 re8 re sib,4 sib,8 sib, |
    la,4 la,8 la, sib,4 sib,8 sib, |
  }
  \tag #'recit { R1*22 }
>>
\keepWithTag #'(recit vdessus vhaute-contre vtaille vbasse basse) \choeur
<<
  \tag #'(vdessus basse) la'4\trill
  \tag #'vhaute-contre do'4
  \tag #'vtaille fa4
  \tag #'vbasse fa,4
  \tag #'recit r4
>>
<<
  \tag #'(vdessus vhaute-contre vtaille vbasse) {
    r4 r2 | R2. R1*2 R2. R1*2 R1. R2.*2 R1*2 R2. R1*2 R1. R1
  }
  \tag #'(recit basse) {
    \tag #'basse \ffclef "vbasse"
    <>^\markup\character Les Maladies Violentes
    r8 la16 sib do'4 do'8 do' |
    fa4 re8 re16 re sol8. sol16 |
    do4 do8 do fa8 fa16 sol la8 la16 sib |
    sol4\trill
    \ffclef "vtaille" <>^\markup\character Les Maladies Languissantes
    r8 do'16 do' do'4 re'8 la |
    sib4 sib8 sib16 sib la8.\trill sol16 |
    fad2\trill r4 re' |
    re' re' re' re' |
    re'2 do'4 do'8 do' fad4.\trill sol8 |
    sol sol
    \ffclef "vbasse" <>^\markup\character Les Maladies Violentes
    r re16 re sol8 sol16 re |
    mib4 do8\trill do16 do fa8. fa16 |
    sib,4 sib,8 fa sib8 sib16 sib sol8\trill sol16 do' |
    la4\trill
    \ffclef "vtaille" <>^\markup\character Les Maladies Languissantes
    r8 re'16 re' fad4 fad8 fad |
    sol4 sol8 sol16 sol fa16[ mi] fa8 |
    mi2\trill r4 do' |
    do'4 do' do' do' |
    do'2 sib4 sib8 sib mi4.\trill fa8 |
    fa2 fa4 r |
  }
>>
\tag #'basse \ffclef "vdessus"
\tag #'(vdessus basse) <>^\markup\character Chœur
\choeur
<<
  \tag #'(vdessus basse) { la'2\trill r }
  \tag #'vhaute-contre { do'2 r }
  \tag #'vtaille { fa2 r }
  \tag #'vbasse { fa,2 r }
>>
