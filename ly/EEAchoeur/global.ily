\key fa \major \tempo\markup\normal-text\italic\smaller "[fort viste]"
\time 2/2 \midiTempo#132 s1*24 \midiTempo#80 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\digitTime\time 2/2 \midiTempo#160 s1*2
\time 3/2 s1.
\digitTime\time 3/4 \midiTempo#80 s2.*2
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\digitTime\time 2/2 \midiTempo#160 s1*2
\time 3/2 s1.
\digitTime\time 2/2 s1
\time 2/2 \midiTempo#132 \tempo\markup\normal-text\italic\smaller "[viste]" s1*23 \bar "|."
