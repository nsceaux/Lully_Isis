\clef "haute-contre" r8 la' la' la'16 sol' fa'8 sol'16 la' sib'8 sib'16 la' |
sol'8 la'16 sib' do'' re'' do'' sib' la'8 sol' sol'8.\trill fa'16 |
fa'8
\setMusic #'choeur {
  fa'8 fa' fa' re' re' re' sol' |
  mi' mi' mi' mi' la' la' la' la' |
  fa' fa' la' la' sol' sol' sol'8. fa'16 |
  mi'2\trill r8 mi' mi' mi' |
  fa' fa' fa' fa' sol' sol' sol' sol' |
  sol' sol' sol' fad' sol' sol' sol' sol' |
  sol'4. sol'8 la'4 do''8 do'' |
  do''4 do''8 do'' sib'4 sib'8 sib' |
  la'8 la' la' la' fad'4.\trill fad'8 |
  sol'4 sol'8 sol' sol'4 do''8 do'' |
  la'4. la'8 la'4 la'8 la' |
  sib'4 re''8 re'' do'' do'' do'' do'' |
  la'4 fa'8 fa' do''4. sib'8 |
  la'4 r r2 |
  R1 |
  r4 mi'8 mi' la'4 la'8 mi' |
  fa'4 la'8 la' sib'4 sib'8 la' |
  sib'4 fa'8 fa' sol'4 sol'8 re' |
  mi'4 mi'8 fa' sol'4 sol'8 sol' |
  la'4 fa'8 fa' fa'4 do'8 do' |
  re'4 re'8 re' fa'4 fa'8 sol' |
  la'4 << \sugNotes { la'8 fa' } \\ { fa'8 fa' } >> fa'4 fa'8 fa' |
}
\keepWithTag #'() \choeur
fa'4 r4 r2 |
R2. R1*2 R2. R1*2 R1. R2.*2 R1*2 R2. R1*2 R1. R1 \allowPageTurn
r8 \choeur
fa'2 r |
