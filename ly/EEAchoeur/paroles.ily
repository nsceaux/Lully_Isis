\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  E -- xé -- cu -- tons l’Ar -- rest du sort,
  e -- xé -- cu -- tons l’Ar -- rest du sort,
  sui -- vons ses loix les plus cru -- el -- les,
  e -- xé -- cu -- tons l’Ar -- rêt du sort,
  sui -- vons ses loix les plus cru -- el -- les ;
  pré -- sen -- tons sans cesse à la Mort
  des Vic -- ti -- mes nou -- vel -- les.
  Pré -- sen -- tons sans cesse à la Mort,
  pré -- sen -- tons sans cesse à la Mort
  des Vic -- ti -- mes,
  des Vic -- ti -- mes nou -- vel -- les.
}
\tag #'(vbasse basse) { Que le Fer, }
\tag #'(vhaute-contre basse) { Que la Faim, }
\tag #'(vbasse basse) { Que les Feux, }
\tag #'(vhaute-contre basse) { Que les Eaux, }
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  que tout serve à creu -- ser mille & mil -- le tom -- beaux.
  Que tout serve à creu -- ser mille & mil -- le tom -- beaux,
  que tout serve à creu -- ser,
  que tout serve à creu -- ser mille & mil -- le tom -- beaux.
}
\tag #'(recit basse) {
  Qu’on s’em -- pres -- se d’en -- trer dans les Roy -- au -- mes som -- bres,
  par mil -- le che -- mins dif -- fe -- rents.

  A -- che -- vez d’ex -- pi -- rer, in -- for -- tu -- nez Mou -- rans,
  cher -- chez un long re -- pos dans le sé -- jour des Om -- bres.

  Qu’on s’em -- pres -- se d’en -- trer dans les Roy -- au -- mes som -- bres,
  par mil -- le che -- mins dif -- fe -- rents.

  A -- che -- vez d’ex -- pi -- rer, in -- for -- tu -- nez Mou -- rans,
  cher -- chez un long re -- pos dans le sé -- jour des Om -- bres.
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  E -- xé -- cu -- tons l’Ar -- rest du sort,
  e -- xé -- cu -- tons l’Ar -- rest du sort,
  sui -- vons ses loix les plus cru -- el -- les,
  e -- xé -- cu -- tons l’Ar -- rêt du sort,
  sui -- vons ses loix les plus cru -- el -- les ;
  pré -- sen -- tons sans cesse à la Mort
  des Vic -- ti -- mes nou -- vel -- les.
  Pré -- sen -- tons sans cesse à la Mort,
  pré -- sen -- tons sans cesse à la Mort
  des Vic -- ti -- mes,
  des Vic -- ti -- mes nou -- vel -- les.
}
\tag #'(vbasse basse) { Que le Fer, }
\tag #'(vhaute-contre basse) { Que la Faim, }
\tag #'(vbasse basse) { Que les Feux, }
\tag #'(vhaute-contre basse) { Que les Eaux, }
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  que tout serve à creu -- ser mille & mil -- le tom -- beaux.
  Que tout serve à creu -- ser mille & mil -- le tom -- beaux,
  que tout serve à creu -- ser,
  que tout serve à creu -- ser mille & mil -- le tom -- beaux.
}
