\clef "dessus" r8 do'' do'' fa''16 mi'' re''8 mi''16 fa'' sol''8 sol''16 fa'' |
mi''8 fa''16 sol'' la'' sib'' la'' sol'' fa''8 sib''16 la'' sol''8 la''16 sib'' |
la''8
\setMusic #'choeur {
  la'8 la' la' fa' fa' fa' sib' |
  sol' sol' sol' sol' do'' do'' do'' do'' |
  la' la' la' re'' si' si' si' si' |
  do''2 r8 do'' do'' do'' |
  la' la' la' re'' sib' sib' sib' do'' |
  la' la' la' re'' si' si' si' si' |
  do''4. do''8 do''4 do''8 fa'' |
  mi''4 mi''8 fa'' re''4 re''8 mi'' |
  dod''8\trill dod'' dod'' dod'' re''4. re''8 |
  si'4\trill si'8 si' mi''4 mi''8 mi'' |
  do''4. do''8 fa''4 fa''8 fa'' |
  re''4 re''8 re'' mi'' mi'' mi'' mi'' |
  fa''4 fa''8 fa'' mi''4. fa''8 |
  fa''4 r r2 |
  R1 |
  r4 sol'8 sol' do''4 do''8 sib' |
  la'4\trill do''8 do'' fa''4 fa''8 mib'' |
  re''4 re''8 do'' sib'4 sib'8 la' |
  sol'4 sol''8 fa'' mi''4\trill mi''8 re'' |
  do''4 do''8 do'' la'4 la'8 la' |
  fa'4 fa''8 fa'' re''4 re''8 mi'' |
  fa''4 do''8 do'' sib'4 sib'8 sib' |
}
\keepWithTag #'() \choeur
la'4\trill r4 r2 |
R2. R1*2 R2. R1*2 R1. R2.*2 R1*2 R2. R1*2 R1. R1 \allowPageTurn
r8 \choeur
la'2 r |
