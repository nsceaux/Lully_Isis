\clef "quinte" r8 do' do' do' re' re' re' sib |
do' do' do' do' re' re' do'8. do'16 |
do'8
\setMusic #'choeur {
  fa8 la la sib re' re' re' |
  do' do' do' do' do' do' do' do' |
  re' re' fa' fa' sol' sol sol sol |
  sol2 r8 sol sol sol |
  fa fa fa sib sol sol sol la |
  la la la la sol sol sol sol |
  do'4. do'8 do'4 do'8 do' |
  do'4 do'8 do' re'4 fa'8 mi' |
  mi'8 la la la la4. la8 |
  sol4 sol8 sol sol4 sol8 sol |
  fa4. do'8 re'4 re'8 re' |
  re'4 sib8 sib do' do' do' do' |
  re'4 re'8 re' do'4. do'8 |
  do'4 r r2 |
  R1 |
  r4 do'8 do' do'4 do'8 do' |
  do'4 << { la8 la } \\ \sugNotes { la8 sol } >> fa4 sib8 do' |
  re'4 fa'8 fa' sib4 re'8 re' |
  sol4 do'8 si do'4 do'8 do' |
  fa'4 fa'8 fa' fa'4 fa8 fa |
  fa4 fa8 fa fa4 sib8 sib |
  fa4 fa8 fa fa4 fa8 fa |
}
\keepWithTag #'() \choeur
fa4 r4 r2 |
R2. R1*2 R2. R1*2 R1. R2.*2 R1*2 R2. R1*2 R1. R1 \allowPageTurn
r8 \choeur
fa2 r |
