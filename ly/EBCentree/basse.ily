\clef "basse" \footnoteHere #'(0 . 0) \markup { Matériel 1677 : B.C. tacet. }
\phrasingSlurDashed \phrasingSlurDown
r4 |
r sol8( sol) re'\(( re') re'\)( si) |
do'\(( do') do'\)( la) si\(( si) si\)( sol) |
la( la) re' re sol\(( sol) sol\)( do) |
re\(( re) re( re) re4\) re8( re) |
\phrasingSlurUp la,\(( la,) la,\)( si,) do\(( do) do( do)\) |
sol,\(( sol,) sol,\)( la,) si,\(( si,) si,\)( do) |
\phrasingSlurDown re\(( re) re\)( mi) fad( fad) re( re) |
mi( mi) do( do) re( re) re re, |
\phrasingSlurUp sol,\(( sol,) sol,( sol, sol,)\) sol\( sol( mi)\) |
\phrasingSlurDown fa\(( fa) fa\)( re) mi\(( mi) mi\)( do) |
re\(( re) re\)( si,) do\(( do) do\) la, |
re( re) sol,( sol,) \phrasingSlurUp re,\(( re,) re,( re,)\) |
sol,\(( sol,) sol, sol, sol,\) sol\( sol( mi)\) |
\tag #'basse-continue \measure 3/4
sol,\(( sol,) sol,( sol, sol,4)\)
\tag #'basse-continue {
  \break
  \omit Score.BarNumber
  \measure 4/4 sol,4^"[B.C.]" sol4. fa8 mi re |
  do1~ |
  \once\set Staff.whichBar = "|"
  \custosNote do8
}
