\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille)
   (quinte)
   (basse)
   (basse-continue #:ragged #t)
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
