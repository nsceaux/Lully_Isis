\clef "taille" r4 | \phrasingSlurDashed \phrasingSlurDown
r re'8\(( re') re'( re') re'( re')\) |
mi'\(( mi') mi'\)( fad') sol'\(( sol') sol'( sol')\) |
mi'( mi') re'( re') re'\(( re') re'\)( mi') |
re'\(( re') re'( re' re'4)\) re'8( re') |
mi'\(( mi') mi'( mi')\) do'\(( do') do'( do')\) |
re'\(( re') re'\)( do') si( si) re'( re') |
re'\(( re') re'( re') re'( re')\) re'( re') |
si( si) mi'( mi') re'\(( re') re'( re')\) |
re'\(( re') re'( re' re')\) re'\( re'\)( sol') |
fa'\(( fa') fa'( fa')\) mi'\(( mi') mi'( mi')\) |
re'\(( re') re'( re')\) do'( do') mi'( mi') |
re'\(( re') re'( re') re'( re')\) re'( re') |
\phrasingSlurUp si\(( si) si si si\) re'\( re'\)( sol') |
si\(( si) si( si si4)\)
