\clef "haute-contre" r4 | \phrasingSlurDown
r si'8( si') \phrasingSlurDashed la'\(( la') la'\)( si') |
sol'\(( sol') sol'\)( do'') si'( si') sol'( sol') |
sol'\(( sol') sol'\)( fad') sol'\(( sol') sol'\)( la') |
fad'\(( fad') fad'( fad' fad'4)\) fad'8( sol') |
la'\(( la') la'( la')\) mi'\(( mi') mi'\)( fad') |
sol'\(( sol') sol'( sol') sol'( sol')\) si'( si') |
la'\(( la') la'( la')\) la'\(( la') la'( la')\) |
sol'( sol') do''( do'') la'( la') re''( re'') |
si'\(( si') si'( si' si')\) si'\( si'( do'')\) |
la'\(( la') la'\)( si') sol'\(( sol') sol'\)( la') |
fad'\(( fad') fad'\)( sol') mi'\(( mi') mi'\)( la') |
fad'( fad') sol'( sol')
\footnoteHere #'(0 . 0) \markup { Ballard 1719 : \italic mi }
\sug sol'4 fad'8(\trill mi'16 fad') |
sol'8\(( sol') sol'( sol') sol'\) si' si'( do'') |
sol'\(( sol') sol'( sol' sol'4)\)
