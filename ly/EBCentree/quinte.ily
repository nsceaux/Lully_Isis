\clef "quinte" r4 | \phrasingSlurDashed \phrasingSlurDown
r re'8\(( re') re'( re') re'( re')\) |
do'\(( do') do'( do')\) re'( re') si( si) |
do'( do') re'\(( re') re'( re')\) si( la) |
\phrasingSlurUp la\(( la) la( la la4)\) re'8( re') |
\phrasingSlurDown do'( do') do'\(( do') do'( do') do'( do')\) |
\phrasingSlurUp si\(( si) si\)( la) sol\(( sol) sol( sol)\) |
fad( fad) la\(( la) la( la)\) re'( re') |
sol\(( sol) sol\)( la) la\(( la) la8.\trill\) sol16 |
sol8\(( sol) sol( sol sol)\) sol\( sol( do')\) |
do'\(( do') do'\)( si) si\(( si) si\)( la) |
la\(( la) la\)( sol) sol\(( sol) sol\)( la) |
la( la) sol( sol) la( la) re'( re') |
\phrasingSlurDown re'\(( re') re' re' re'\) \phrasingSlurUp sol8\( sol( do')\) |
re'( re') re'( re' re'4)
