\clef "quinte" sib4 |
sib2 sib4 |
sib sol sol |
sol fa fa' |
fa' re'2 |
do' do'4 |
sib sib4.\trill la8 |
la4. sib8 do'4 |
la4. sib8 do'4 |
la4. si8 do'4 |
re'2 sib4 |
do' sib sib |
la8 sib do'4 do' |
do'4. sib8 la4 |
sib re' fa' |
fa'4. mib'8 re'4 |
mib' re' do' |
do' sib4. sib8 |
sib2 sol4 |
<< sib2 \\ \sug sol >> sol4 |
la sib2 |
sol4 fa fa |
fa sib2 |
sib4 \footnoteHere #'(0 . 0) \markup {
  Matériel 1677 : \italic la♮
} lab4 sol |
sol2 sol4 |
la sib2 |
sol4 fa fa |
fa2. |
