\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille)
   (quinte)
   (basse)
   (basse-continue #:system-count 3)
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
