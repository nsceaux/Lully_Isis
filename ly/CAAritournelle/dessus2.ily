\clef "dessus" R1*3 |
r4 r8 do'' sib' la' sol' fa' |
mi'2\trill~ mi'8 mi' mi' fa' |
sol'2~ sol'8 sol' do'' do'' |
do''4. fa''8 sol'' la'' sol'' fa'' |
mi''2\trill~ mi''8 mi'' mi'' fa'' |
sol''4. mi''8 la'' sol'' fa'' mi'' |
fa''2~ fa''8 fa'' fad''\trill sol'' |
la''2~ la''4. la''8 |
re''4. re''8 sol'' fa'' mib'' re'' |
mib''4. re''8 do'' do'' do''8^\markup\croche-pointee-double sib' |
la'2\trill~ la'8 la' re'' do'' |
sib' la' sol'4. la'8 fad'8. sol'16 |
sol'2~ sol'8 si' si' do'' |
re''2~ re''4. re''8 |
sol'2~ sol'8 sol' sol' la' |
sib'4. la'8 sib' sol' do'' sib' |
la' la' re'' re'' si' si' mi'' mi'' |
do'' do'' re'' mi'' fa'' mi'' re'' do'' |
si'2\trill~ si'4. do''8 |
re''4. si'8 do'' re'' si'8.\trill do''16 |
do''1 |
