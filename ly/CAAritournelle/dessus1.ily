\clef "dessus" r4 r8 sol'' fa'' mi'' re'' do'' |
si'2\trill~ si'8 si' si' do'' |
re''4. mi''8 fa'' re'' sol'' fa'' |
mi''2\trill~ mi''8 mi'' mi'' fa'' |
sol''2~ sol''8 sol'' sol'' la'' |
sib''2~ sib''8 sib'' la'' sol'' |
la''4. la''8 sol'' fa'' mi'' re'' |
dod''2\trill~ dod''8 dod'' dod'' re'' |
mi''2~ mi''4. mi''8 |
la'2~ la'8 la' la' sib' |
do''4. la'8 re'' do'' sib' la' |
sib'2. sib'8 sib' |
sib'4. sib'8 la' la' la'\trill^\markup\croche-pointee-double sol' |
fad'2\trill~ fad'8 fad' fad' fad' |
sol' la' sib' sib' sib' do'' la' re'' |
si'2~ si'8 re'' re'' mi'' |
fa''4. mi''8 fa'' re'' sol'' fa'' |
mi''2\trill~ mi''8 mi'' mi'' fa'' |
sol''4. la''8 sol'' fa'' mi'' re'' |
do'' do'' fa'' fa'' re'' re'' sol'' sol'' |
mi'' mi'' fa'' sol'' la''4. la''8 |
re''2~ re''4. mi''8 |
fa''4. sol''8 mi'' fa'' re''8.\trill do''16 |
do''1 |
