\clef "basse" do2~ do8 re mi fa |
sol4. sol8 fa mi re do |
si,1 |
do |
do'4. do'8 sib la sol fa |
mi1 |
fa2. sol4 |
la4. la8 sol fa mi re |
dod1 |
re4. re8 do sib, la, sol, |
fad,1 |
sol,2. sol,4 |
do1 |
re4. la8 re' do' sib la |
sol fa mib re do4 re8 re, |
sol,4. sol8 fa mi re do |
si,1 |
do4. do8 sib, la, sol, fa, |
mi,2 mi |
fa8 fa re re sol sol mi mi |
la la la sol fa4 fad |
sol4. sol8 fa mi re do |
si, la, sol,4 la,8 fa, sol,4 |
do1 |
