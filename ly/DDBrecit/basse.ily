\clef "basse" re1 |
re,2. |
re2. do4 |
sib, sol,2 |
la, la4 dod |
re2. |
mi4. re8 mi mi, |
la,2. sol,4 |
fa, fad,2 |
sol,4 re8 do sib,4 |
la,2 re |
sib,4. sol,8 la,2 |
re, re |
do sib, |
la, fad, |
sol, la, |
re,2. mi,4 |
fa,1 |
fa2 re |
mi2 fa4 |
sol la8 fa sol sol, |
do2. sib,4 |
la,1~ |
la,2 sib, |
do la, |
sib,8 la, sol, fa, do do, |
fa,4. |
