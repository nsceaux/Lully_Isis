De la nym -- phe Sy -- rinx, Pan ché -- rit la mé -- moi -- re,
il en re -- grette en -- cor la per -- te cha -- que jour ;
Pour cé -- le -- brer u -- ne fête à sa gloi -- re,
ce Dieu luy- même as -- semble i -- cy sa cour :
Il veut que du mal- heur de son fi -- dele a -- mour
un spec -- ta -- cle tou -- chant re -- pré -- sen -- te l’his -- toi -- re.

C’est un plai -- sir pour nous ; pour -- sui -- vez, j’y con -- sens,
je ne m’op -- po -- se point à des jeux in -- no -- cents.

Il don -- ne dans le piege, a -- che -- vez sans re -- mi -- se,
a -- che -- vez de sur -- prendre Ar -- gus, & tous ses yeux :
Si vous ten -- tez u -- ne grande en -- tre -- pri -- se,
Mer -- cu -- re vous con -- duit, l’A -- mour vous fa -- vo -- ri -- se,
et vous ser -- vez le plus puis -- sant des Dieux.
