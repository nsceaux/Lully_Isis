\clef "vhaute-contre" <>^\markup\character-text Mercure \line {
  déguisé en Berger, parlant à Argus
}
r4 la8 la re'4 re'8 re' |
re'4 la8. la16 sib8 sib16 la |
la4\trill la8 r16 la la8 si do' la |
re' re' mi' mi' mi' mi' |
dod'4\trill la8 la16 la mi'8 mi'16 fa' sol'8 fa'16 mi' |
fa'4 fa'8 r16 fa' re'8 re' |
si\trill si si do' si8.\trill la16 |
la1 |
r8 do' do' do' sib! la |
sib sib sib la sol\trill[ fa16] sol |
la4 mi'8 mi' fa'4 fa'8 fa' |
re'4\trill re'8 sol' mi'4\trill mi'8 fa' |
re'4\trill re'8 r
\ffclef "vbasse-taille" <>^\markup\character Argus
fa8 fa16 fa sol8 la |
mi4\trill r8 mi16 la re4 r8 re16 sol |
mi4\trill r8 la la la sib do' |
sib4\trill sol8 fa mi4.\trill mi16 fa |
re2 r |
\ffclef "vhaute-contre" <>^\markup\character-text Mercure \line {
  parlant à part à toute la Troupe qu’il conduit
}
-\tag #'conducteur ^\markup\italic\column {
  \line {
    Argus va prendre place sur un siege de gazon proche de l’endroit où Io est enfermée,
  }
  \line { & fait placer Hierax de l’autre coté. }
  " "
}
r4 r8 do' la la la la |
fa4 fa8 fa sib4 sib8 sib |
sol4\trill sol16 r do' do' la8\trill la16 re' |
si8\trill si do' do' do' si |
do'4 r16 sol sol sol do'8 do'16 re' mi'8 mi'16 do' |
fa'4 fa'8 r16 do' la8\trill la la la |
fa4 r8 fa' re' do' sib la |
sol4\trill sol8 r do'4 do'8 do' |
re' re' mi' fa' mi'8.\trill fa'16 |
fa'4. |
