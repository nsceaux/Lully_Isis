\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1 s2. s1\break s2. s1 s2.\break s2. s1 s2.*2\break
        s1*3 s2 \bar "" \break s2 s1*3\break s1*2 s2. s4 \bar "" \pageBreak
        s2 s1*2 s2 \bar "" \break
      }
      \modVersion { s1 s2. s1 s2. s1 s2.*2 s1 s2.*2 s1*6 s1\break }
    >>
  >>
  \layout { }
  \midi { }
}
