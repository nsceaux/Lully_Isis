\clef "basse" fa2. fa4 mi fa |
sib,2. la,4 sib, sol, |
do2. la,4 re2 |
sib,2 do1 |
fa,2 fa4. fa8 |
fa2 mi re |
do4. re8 mi do |
fa2 mi4 |
fa8 mib re4 do |
sib, la, sol, |
fa, fa2 |
mi4 fad2 |
sol sol,4 |
re4. mi8 fad4 |
sol la si |
do' sol sol, |
do4. re8 mi do |
do2 do'8 sib |
la2 sib4 |
fa do re |
la,2. |
sib,2 sib8 la |
sol4 la sib |
mib fa fa, |
sib, do re |
sol, la, sib, |
do re mi |
fa sol la |
sib sol do' |
mi4. mi8 fa4 |
sib, do do, |
fa,2 fa2~ | \allowPageTurn
fa4 mi2 |
fa fad~ |
fad2. |
sol2 sol, |
re1 |
sol2 la8 fa sol sol, |
do1 |
do'2. sib4 |
la2 la, |
sib,1 |
si,2. |
do4 sib,8 la, sol,_\markup\croche-pointee-double fa, mi,4 |
fa, fa sib, |
do do'4. sib8 |
la4. sol16 fa do4 |
fa,2 fa |
mi fa |
sib, sib |
la sol |
fa4. mi8 re2 |
sol do |
fa, sol, |
do fa |
do~ do8 sol la sib |
do'4 sib8 la sol4. la8 |
sib4 si do'8 do'16 sib? |
la8 la16 sol fa8 fa mib4 |
re sib, do do, |
fa, fa8 sol la4 la8 sib |
fa,2. |
fa2 fa4 |
fa mi2 |
fa4. mib8 re do |
sib,4. la,8 sol, fa, |
do4. re8 mi do |
do2 do'8 sib |
la4. sol8 fa4 |
sib sib,2 |
do4. re8 mib do |
re4 re'4. do'8 |
sib4 la sol |
do re re, |
sol, sol la |
sib2. |
la |
sib2 si4 |
do'2 la4 |
re'4. do'8 sib4 |
do' do2 |
\once\tieDashed fa2~ fa4 dod |
re2 sol8 fa mi re |
do4. sib,8 la,4 |
sib,2 do |
\custosNote fa,4
