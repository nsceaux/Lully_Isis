\key fa \major
\time 3/2 \midiTempo#160 s1.*4
\time 4/4 \midiTempo#80 s1
\time 3/2 \midiTempo#160 s1.
\digitTime\time 3/4 s2. \bar ".!:" s2.*9 \alternatives s2. s2.
s2.*13
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1
\time 2/2 \midiTempo#160 s1
\time 4/4 \midiTempo#80 s1*2
\time 2/2 \midiTempo#160 s1
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.*3
\digitTime\time 2/2 \midiTempo#160 s1 \bar ".!:" s1*6 \alternatives s1 s1
\bar ".!:" s1
\digitTime\time 3/4 \midiTempo#80 s2.*2
\digitTime\time 2/2 \midiTempo#160 s1 \alternatives s1 { \digitTime\time 3/4 s2. }
\bar ".!:" s2.*4 \alternatives s2. s2. s2.*13
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1
