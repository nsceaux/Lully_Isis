\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1.*4 s1 s1.\break s2.*8\break s2.*9\break
        s2.*8 s2 \bar "" \break s2 s2. s1 s2.\break s1*3\pageBreak
        s1*3\break s1 s2. s2. \bar "" \break s4 s2.*2\break
        s2. s1*4\break s1*6\break s2.*2 s1*2 s2.\pageBreak
        s2.*6\break s2.*7\break s2.*6 s1\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
