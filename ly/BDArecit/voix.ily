\ffclef "vbas-dessus" <>^\markup\character Micene
R1.*4 |
r4 r8 do'' la'\trill la' si' do'' |
si'2\trill do''4 do''8 mi' fa'4 sol' |
mi'2\trill mi'4 |
r^"Air" fa' sol' |
la' sib' do'' |
re''2 mi''4 |
fa''2 la'8 sib' |
do''4 do'' re'' |
sib'4.\trill( la'8) sib'4 |
la'2\trill re''4 |
si'\trill do'' re'' |
mi'' re''4.\trill sol''8 |
mi''2\trill do''4 |
mi''2\trill do''4 |
fa'' do'' re'' |
la'2.\trill |
do''4 re'' mib'' |
re''2\trill re''4 |
mib'' mib'' re'' |
re''( do''4.)\trill sib'8 |
sib'2. |
sib'4 do'' re'' |
sol'2. |
la'4 sib' do'' |
sib'2\trill sol'4 |
sib' sib' la' |
la'( sol'4.)\trill fa'8 |
fa'2
\ffclef "vbas-dessus" <>^\markup\character Io
r8 do''16 do'' fa'8 sol'16 la' |
sib'8 sib' sol' sol' la' sib' |
la'4\trill la'8 r16 fa' do''8 do'' do'' do'' |
la'\trill la' re'' do'' sib' la' |
sib'4 sib'8 r16 sol' re''8 re'' re'' mi'' |
fa''4 re''8 re'' la'4 la'8 re'' |
si'4\trill re''8 mi'' do''\trill do'' do'' do''16 si'! |
do''4 r8 sol' do'' do'' do'' re'' |
mi''4 do''8 do'' do''4 re''8 mi'' |
fa''4 fa''8 r16 fa'' do''8 do''16 do'' la'8 la'16 la' |
fa'8 fa' r fa''16 mib'' re''8 re''16 do'' sib'8 re'' |
sol'8 fa' fa' mi' mi'\trill r16 sol' |
do''8 do'' sol' la' sib' sib'16 re'' sib'8 sib'16 la' |
la'8\trill la' la'16 la' sib' do'' re''8 mi''16 fa'' |
mi''4\trill do''16 sol' sol' sol' la'8 sib' |
do''4 re''8 mi''16 fa'' mi''8.\trill fa''16 |
fa''2
\ffclef "vbas-dessus" <>^\markup\character-text Mycene \normal-text Air
la'4. sib'8 |
do''2 do''4. do''8 |
re''2 re''4 mi'' |
fa''2 sib'4( la'8) sib' |
la'2\trill la'4 fa'' |
re''4. re''8 mib''4. mib''8 |
mib''4 re'' re''4.\trill do''8 |
do''2 la'4. sib'8 |
do''1 |
sol'4 sol'8 la' sib'4. do''8 |
re''4 re''8 mi''16 fa'' mi''8\trill do'' |
fa''4 fa''8 r la' sib'16 do'' |
sib'4.\trill la'8 sol'4.\trill fa'8 |
fa'1 |
fa'2. |
\ffclef "vbas-dessus" <>^\markup\character-text Io \line { \normal-text Air \italic gay }
do''4 la' fa' |
sib'4. sib'8 sib' do'' |
la'4\trill la' sib'8 do'' |
re''4 re''8 re'' mi'' fa'' |
mi''2.\trill |
mi''4\trill do''4. re''8 |
mib''4 mib''( re''8) mib'' |
re''2\trill re''4 |
do''4.\trill sib'8 la' sol' |
fad'2\trill re''8 fad' |
sol'4 la'4. sib'8 |
sib'4( la'2)\trill |
sol'4 sib' do'' |
re'' re''4. mi''8 |
fa''2 do''4 |
sib'4.\trill la'8 sol' fa' |
mi'2\trill do''8 fa'' |
la'4\trill sib'4. do''8 |
la'4( sol'2)\trill |
fa'4 la'4. la'8 mi'\trill mi'16 la' |
fad'4\trill r8 re'' si'\trill si' do''8. re''16 |
mi''4
\ffclef "vbas-dessus" <>^\markup\character Micene
do''8 do''16 do'' fa''8. do''16 |
re''4 sib'8 re'' sib'4\trill sib'8 la' |
la'4\trill
\once\set Staff.whichBar = "|."