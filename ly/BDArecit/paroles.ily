Ce prin -- ce trop long -- temps dans ses cha -- grins s’ob -- sti -- ne.
On par -- donne au pre -- mier trans -- port
d’un a -- mour qui se plaint à tort,
et qui sans rai -- son se mu -- ti -- ne :
- ti -- ne :
Mais à la fin,
on se cha -- gri -- ne
contre un a -- mour cha -- grin.
Mais à la fin,
on se cha -- gri -- ne
contre un a -- mour cha -- grin.

Je veux bien te par -- ler en -- fin sans ar -- ti -- fi -- ce :
Ce Prince in -- for -- tu -- né s’al -- larme a -- vec jus -- ti -- ce,
le Maî -- tre sou -- ve -- rain de la terre & des cieux
en -- tre -- prend de plaire à mes yeux,
du cœur de Ju -- pi -- ter l’A -- mour m’of -- fre l’em -- pi -- re ;
Mer -- cure est ve -- nu me le di -- re,
je le vois cha -- que jour des -- cen -- dre dans ces lieux.
Mon cœur, au -- tant qu’il peut, fait toû -- jours ré -- sis -- tan -- ce ;
et pour at -- ta -- quer ma cons -- tan -- ce,
il ne fal -- loit pas moins que le plus grand des Dieux.

On é -- coute ai -- sé -- ment Ju -- pi -- ter qui soû -- pi -- re,
c’est un a -- mant qu’on n’o -- se mé -- pri -- ser :
On é -  - ser :
Et du plus grand des cœurs le glo -- ri -- eux Em -- pi -- re
est dif -- fi -- cile à re -- fu -- ser.
- ser.

Lors qu’on me pres -- se de me ren -- dre
aux at -- traits d’un A -- mour nou -- veau :
- veau :
plus le charme est puis -- sant, & plus il se -- roit beau
de pou -- voir m’en def -- fen -- dre.
Plus le charme est puis -- sant, & plus il se -- roit beau
de pou -- voir m’en def -- fen -- dre.
Quoy ! tu veux me quit -- ter ? d’où vient ce soin pres -- sant ?

C’est pour vous seule, i -- cy, que Mer -- cu -- re des -- cend.
