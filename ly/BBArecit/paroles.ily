C’est trop en -- tre -- te -- nir ces tris -- tes res -- ve -- ri -- es ;
ve -- nez, tour -- nez vos pas vers ces ri -- ves fleu -- ri -- es ;
re -- gar -- dez ces flots ar -- gen -- tez,
qui dans ces val -- lons é -- car -- tez,
font bril -- ler l’é -- mail des prai -- ri -- es.
In -- ter -- rom -- pez vos soû -- pirs,
tout doit être i -- cy tran -- qui -- le ;
ce beau sé -- jour est l’a -- zi -- le
du re -- pos, __ du re -- pos, & des plai -- sirs.
Ce beau sé -- jour est l’a -- zi -- le
du re -- pos, __ du re -- pos, & des plai -- sirs.

De -- puis qu’u -- ne Nimphe in -- cons -- tan -- te
a tra -- hi mon a -- mour, & m’a man -- qué de foy,
ces lieux, ja -- dis si beaux, n’ont plus rien qui m’en -- chan -- te ;
ce que j’aime a chan -- gé, tout est chan -- gé pour moy.

La fil -- le d’I -- na -- chus hau -- te -- ment vous pré -- fe -- re
à mille au -- tres a -- mants de vô -- tre sort ja -- loux ;
vous a -- vez l’a -- veu de son pe -- re ;
en fa -- veur d’Ar -- gus, vô -- tre Fre -- re,
la puis -- san -- te Ju -- non se dé -- cla -- re pour vous.

Si l’In -- gra -- te m’ai -- moit, je se -- rois son é -- poux.
Cet -- te Nym -- phe lé -- ge -- re
de jour en jour dif -- fe -- re
un hy -- men qu’au -- tre -- fois elle a -- voit crû si doux.
L’in -- cons -- tan -- te n’a plus l’em -- pres -- se -- ment ex -- trê -- me
de cét a -- mour nais -- sant qui ré -- pon -- doit au mien : mien :
son chan -- ge -- ment pa -- roît en dé -- pit d’el -- le- mê -- me,
je ne le con -- nois que trop bien ;
sa bou -- che quel -- que -- fois dit en -- cor qu’el -- le m’ai -- me ;
mais son cœur, ny ses yeux ne m’en di -- sent plus rien.
Sa bou -- che quel -- que -- fois dit en -- cor qu’el -- le m’ai -- me ;
mais son cœur, ny ses yeux ne m’en di -- sent plus rien.

Se peut- il qu’el -- le dis -- si -- mu -- le ?
A -- près tant de ser -- ments, ne la croy -- ez- vous pas ?

Je ne le crûs que trop, he -- las !
Ces ser -- ments qui trom -- poient mon cœur tendre & cre -- du -- le :
Ce fut dans ces val -- lons, où par mil -- le dé -- tours
I -- na -- chus prend plai -- sir à pro -- lon -- ger son cours ;
ce fut sur son char -- mant ri -- va -- ge,
que la Fil -- le vo -- la -- ge
me pro -- mit de m’ai -- mer toû -- jours.
Le Zé -- phir fut té -- moin, l’On -- de fut at -- ten -- ti -- ve,
quand la Nym -- phe ju -- ra de ne chan -- ger ja -- mais ;
mais le Ze -- phir le -- ger, & l’On -- de fu -- gi -- ti -- ve,
ont en -- fin em -- por -- té les ser -- ments qu’elle a faits.
Je la vois l’In -- fi -- del -- le !

E -- clair -- cis -- sez- vous a -- vec el -- le.
