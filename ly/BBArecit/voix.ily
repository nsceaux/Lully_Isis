\clef "vhaute-contre" R1 |
<>^\markup\character Pirante
r4 r8 sol' re'\trill re' re' re' |
mib'4. mib'8 re'\trill re' re' la |
sib sib r sol re'8. re'16 re'8 sib |
fa'4 fa'8 sol' mib'4\trill mib'8 re' |
do'8\trill do' r do'16 re' mib'8 do' sol' sol'16 re' |
mib'2 do'8 do' do' re' |
sib4\trill sib8 sol re'4 re'8 re' |
sol'4. fa'8 mi'4\trill fad'8 sol' |
fad'2\trill re'4 |
r r8 sol' re' \footnoteHere#'(0 . 0) \markup { Matériel 1677 : \italic mi♮ }
mib' |
fa'4 mib'( re'8) mib' |
re'2\trill fa'8 fa' |
sol'4 mib' do' |
re' re'4( do')\trill |
sib4. re'8 re' mi' |
fa'2 fa'8 mi' |
fa'4 fa' la'8 la' |
la'2.~ |
la'~ |
la'4 sol'4.\trill sol'8 |
sol'2. |
fa'8[ mi'] fa'4( mi'8\trill) re' |
re'2. |
r8 fa' fa'4. re'8 |
mib'4 do'4.\trill sib8 |
la4\trill la re'8 re' |
re'2.~ |
re'~ |
re'4 do'4.\trill do'8 |
do'2. |
sib8[ la] sib4( la8.\trill) sol16 |
sol4
\ffclef "vbasse-taille" <>^\markup\character hierax r8 sib sol\trill sol16 sol re8\trill re16 mib |
fa8 fa r fa16 fa sib8 sib16 sib |
sol8\trill sol sol la sib do' |
la4\trill r8 fa do'8. do'16 sib8.\trill la16 |
sib4 sol8 sol sol4 fa8[ mi16] fa |
mi8\trill mi r la16 la la8 si16 do' |
si4.\trill si8 dod' re' re' dod' |
re'4
\ffclef "vhaute-contre" <>^\markup\character Pirante
r8 la re' re' re' re' |
si4.\trill re'16 re' sol'4 sol'8 re' |
mib'8 mib' r mib'16 mib' mi'4\trill mi'8 mi' |
fa'8 do' do' re' sib8.\trill la16 |
la4\trill r8 fa'16 fa' la8 la sib sib16 sib |
sol8\trill sol r mib'16 mib' mib'8 re' do' do'16 do' |
la8\trill la r do'16 do' fa'4 fa'8 fa' |
sol'4. mib'16 re' do'4\trill do'8 fa' |
re'4\trill
\ffclef "vbasse-taille" <>^\markup\character hierax
r8 fa16 fa sib8 sib16 sib |
fad4.\trill fad16 fad fad8\trill fad16 sol |
sol4 r8 re'16 re' sol8 sol16 sol |
mi8\trill mi r16 mi mi mi fa8 sol |
la8 la r la16 la la8\trill la16 la |
sib8. sib16 sib8 la sol[ fad16] sol |
fad1\trill |
r4 sol8 sol re'4 re'8 sib |
mib'8 re' do'8.\trill sib16 la8 sib |
fa4 fa8 re sol la sib sol |
la4. fa8 mi re la,8. re16 |
re1 |
re |
re'4 re'8 do' sib4.\trill la8 |
sol4 sol8 fa mi4\trill mi8 mi |
fa fa re16 re re mib do8\trill do16 fa |
sib,4. sib8 la\trill la la sol |
fad4\trill fad8 sol mib4 mib8[ re16] mib |
re8\trill re16 r re8 mi fad4\trill sol8 la |
sib4 sib8 do' re'4 re'8 re |
sol4. sol8 do' do' la8.\trill la16 |
sib4 la8 sol fad4 sol8[ fad16] sol |
re8 re16 r re'8 la sib4 fad8 sol |
mib4 mib8 do re4 re8 re |
sol,4
\ffclef "vhaute-contre" <>^\markup\character Pirante
r8 re'16 re' sib4\trill sib16 sib sib la |
la8\trill la r re'16 re' sol'8 sol'16 sol' |
mi'8.\trill mi'16 mi'8 mi' fad' sol' |
fad'4\trill
\ffclef "vbasse-taille" <>^\markup\character hierax
r16 la la la sib8 sib |
sol4\trill do' la\trill r8 fa16 fa |
sib8 do'16 re' mib'8 mib'16 mib' la8\trill la16 sib |
sib8 sib r sib fa4 fa8 fa16 fa |
re8\trill sib,16 do re8 re16 mib fa8 fa16 sol la8 la16 sib |
do'8 do' do' si si8.\trill do'16 |
do'2. do'4 |
la4\trill la8 la16 la re'8 la |
sib8 sib r sib16 re' sib8\trill sib16 sib |
sol8\trill sol r sol16 fa mib8\trill mib16 re do8.\trill sib,16 |
sib,4 r8 fa16 fa fad8 fad16 sol |
la2 re'4. re'8 |
re'4 re'8 re' re'2 |
re'4 sib8 la sol4\trill sol8 fa |
mi8.\trill mi16 mi8 fa mi8.\trill re16 |
re4 fad8\trill fad16 fad sol8 la |
sib8 sib si8.\trill si16 do'8^\markup\croche-pointee-double re' |
mib' mib' mib' do' la sib16 do' |
fad4\trill sol8 do re4 re8 re |
sol,4 r8 sol16 re mib8 mib16 re |
re8\trill re
\ffclef "vhaute-contre" <>^\markup\character Pirante
sib16 sib do' re' la8\trill la16 sib |
sol4\trill sol r2 |
