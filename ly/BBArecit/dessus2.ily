\clef "dessus" R1*9 R2.*23 R1 R2.*2 R1*2 R2. R1*4 R2. R1*4 R2.*6 R1 |
r4 sol'8 sol' sol'4 fa'8 fa' |
sol' sol' la'8. sib'16 do''8 sib' |
la'4.\trill la''8 la''8. la''16 sol''8.\trill fa''16 |
mi''4.\trill fa''8 dod'' re'' dod''8.\trill re''16 |
re''1 |
re'' |
r2 re''4 re''8 do'' |
sib'4 sib'8 la' sol'4. do''8 |
do'' la' re''16 mib'' fa'' sol'' mib''8 mib''16 do'' |
re''4. re''8 do'' do'' do'' sib' |
la'4\trill re''2 do''8\trill sib'16 do'' |
re''4 fad''8 sol'' la''4. la''8 |
la''4 sol''8. sol''16 sol''4 fad''8\trill mi''16 fad'' |
sol''8 la'' sib''8. sib''16 sol''8. sol''16 fa''8. fa''16 |
fa''4 fad''8.\trill sol''16 la''4 << \sugNotes { re''8. sol''16 } \\ { re''8. mi''16 } >> |
fad''4\trill fad''8 fad'' sol'' re''4 re''8 |
re''4 do''8 mib'' la'4\trill la'8^\markup\croche-pointee-double sib' |
sol'2 r |
R2.*3 R1 R2. R1*2 R2. R1 R2.*2 R1 R2. R1*3 R2.*4 R1 R2.*2 R1 |

