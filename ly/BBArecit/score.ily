\score {
  \new ChoirStaff <<
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*4 s2 \bar "" \break s2 s1*3\break
        s1 s2.*6\pageBreak
        s2.*7\break s2.*7 s4 \bar "" \break s2 s2.*2 s1\break
        s2.*2 s1 s2 \bar "" \break s2 s2. s1*2\break
        s1*2 s2. s2 \bar "" \pageBreak
        s2 s1*2 s2 \bar "" \break s2 s2.*2 s2 \bar "" \break
        s4 s2.*3 s1\break
        s1 s2. s1 s2 \bar "" \pageBreak
        s2 s1*4\break s2. s1 s2 \bar "" \break s2 s1*2\pageBreak
        s1*3\break s1*2 s2.\break s2.*2 s1\break s2. s1 s2 \bar "" \pageBreak
        s2 s2. s1 s2.\break s2. s1 s2 \bar "" \break
        s4 s1*3\break s2.*3\break s2. s1 s2.\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
