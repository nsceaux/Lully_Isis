\piecePartSpecs
#`((basse-continue #:score-template "score-voix" #:indent 0)
   (dessus #:score "score-dessus")
   (basse #:score-template "score-voix" #:music ,#{ \beginMark "TACET" #})
   (silence))
