\clef "basse" sol,1~ |
sol,~ |
sol,2 fad, |
sol, sol |
re do4. sib,8 |
fa4 mib8. re16 do4 si, |
do2 fad, |
sol,4 sol fa2 |
dod4. re8 la,2 |
re4. mi8 fad re |
sol2 sol4 |
la2 la4 |
sib4 sib,8 do re4 |
mib do fa |
sib, fa,2 |
sib,2 sib4 |
la sol2 |
fa4. mi8 re4 |
dod4. si,8 dod la, |
re mi fa sol la fa |
sib do' sib la sib sol |
la sol la si dod' la |
re' sol la4 la, |
re4. mib8 re do |
si,2. |
do4. re8 mib do |
re4 re8 do sib,4 |
fad,4. mi,8 fad, re, |
sol, la, sib, do re sib, |
mib fa mib re mib do |
re do re mi fad re |
sol do re4 re, |
sol,1 |
re2. |
mib2 mi4 |
fa2 fad |
sol dod4 re |
la,2 fad,4 |
sol,4. sol8 la sib la la, |
re1 | \allowPageTurn
sol2 si, |
do sib, |
la,4. sib,8 sol,4 |
fa, fa mib re |
mib2. mi4 |
fa2 re |
mib fa4 fa, |
sib,2. |
la, |
sol,2 si,4 |
do4. sib,8 la, sol, |
fa,2 fad,4 |
sol, sol8 fa mib4 |
re4. mib8 re do sib, la, |
sol,4 sol8 sol re'4 re'8 sib |
mib' re' do'8. sib16 la8 sib |
\once\tieDashed fa4~ fa8 re sol la sib sol |
la4. fa8 mi re la,8. re16 |
re4. mib8 re do sib, la, |
re1 |
re'4 re'8 do' sib4. la8 |
sol4 sol8 fa mi4 mi8 mi |
fa fa re8. mib16 do8 do16 fa |
sib,4. sib8 la4 la8 sol |
fad4 fad8 sol mib2 |
re4 re8 mi fad4 sol8 la |
sib4 sib8 do' re'4 re'8 re |
sol4. sol8 do' do' la8. la16 |
sib4 la8 sol fad4 sol |
re re'8 la sib4 fad8 sol |
mib4 mib8 do re4 re8 re, |
sol,1 | \allowPageTurn
re2 si,4 |
do dod2 |
re2 sol,4 |
mib4 mi fa2 |
re4 do2 |
sib,1~ |
sib,4. la,16 sol, fa,4 fa |
mib re2 |
do1 |
fad,2. |
sol,2 re4 |
mib4. re8 do8. sib,16 fa,4 |
sib,2 la,8. sol,16 |
re4. mi8 fad4 re |
sib4 la8 sol fad mi re do |
sib, la, sol, fa, mi,4. re,8 |
\once\tieDashed la,4~ la,8 sol, la,4 |
re, re8 do sib, la, |
sol,4 sol8 fa mib re |
do2 do4 |
re4 mib8 do re4 re, |
sol,2 sol,4 |
sol,2 re4 |
sol,2. fa,8 mi, |
\once\set Staff.whichBar = "|"
\custosNote re,
