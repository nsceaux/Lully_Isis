\clef "dessus" R1*9\allowPageTurn R2.*23\allowPageTurn
R1\allowPageTurn R2.*2\allowPageTurn R1*2\allowPageTurn
R2.\allowPageTurn R1*4\allowPageTurn R2.\allowPageTurn
R1*4\allowPageTurn R2.*6\allowPageTurn R1\allowPageTurn |
r4_"Violons" sib'8 sib' la'4\trill la'8 re'' |
sib'8 sib' do''8. re''16 mib''8 re'' |
do''4.\trill fa''8 fa''8. fa''16 mi''8.\trill re''16 |
dod''4.\trill la''8 sol''8 fa'' mi''8.\trill re''16 |
re''1 |
re''2 la''4 la''8 sol'' |
fad''4.\trill fad''8 sol''4. la''8 |
re''4 sol''8 la'' sib''4. sol''8 |
la'' fa'' sib''8. sib''16 sib''8 sib''16 la'' |
sib''4. fa''8 fad''\trill fad'' fad'' sol'' |
la''4 la''8 sib'' sol''4 sol''8 la'' |
fad''4\trill re'' do''4. do''8 |
do''4 sib' la'4.\trill re''8 |
sib'8 do'' re''8. re''16 mib''4. mib''8 |
re''4 do''8. do''16 do''8. re''16 sib'4 |
la'\trill \sugRythme { si'16 si' si'8 } la'8 sib'16 do'' sib'8. sib'16 la'8 sib' |
sol'4 sol'8 sol' sol'4 fad'8.\trill sol'16 |
sol'2 r |
R2.*3 R1 R2. R1*2 R2. R1 R2.*2 R1 R2. R1*3 R2.*4 R1 R2.*2 R1 |

