\key re \minor
\time 4/4 \midiTempo#80 s1*9
\digitTime\time 3/4 s2.*23
\time 4/4 s1
\digitTime\time 3/4 s2.*2
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1*4
\digitTime\time 3/4 s2.
\time 4/4 s1*4
\digitTime\time 3/4 s2.*6
\digitTime\time 2/2 \midiTempo#160 s1
\time 2/2 \beginMarkSmall "Air [trio]" \bar ".!:" s1
\digitTime\time 3/4 \midiTempo#80 s2.
\time 2/2 \midiTempo#160 s1*2
\alternatives { \digitTime\time 2/2 s1 } s1 s1*2
\digitTime\time 3/4 \midiTempo#80 s2.
\time 2/2 s1*8
\time 4/4 s1
\digitTime\time 3/4 s2.*3
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\digitTime\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2.*2
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 2/2 \midiTempo#160 s1*3
\digitTime\time 3/4 \midiTempo#80 s2.*4
\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2.*2
\time 2/2 \midiTempo#160 s1 \bar "|."
