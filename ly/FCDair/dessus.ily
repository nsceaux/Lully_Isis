\clef "dessus" mi''8. fa''16 sol''8 |
do''8. re''16 mi''8 |
fa''8. sol''16 mi''8 |
re''8.\trill do''16 si'8 |
do''8. re''16 mi''8 |
la'8. si'16 do''8 |
re''8. mi''16 do''8 |
si'8.\trill la'16 sol'8 |
si'8.\trill la'16 sol'8 |
sol''8. fa''16 mi''8 |
fa''8. sol''16 la''8 |
mi''8. fa''16 re''8 |
dod''8.\trill si'16 la'8 |
la''8. sol''16 fa''8 |
sib''8. sib''16 la''8 |
sol''8.\trill la''16 fa''8 |
mi''4\trill re''8 |
sol''8. la''16 sol''8 |
do''8. re''16 mi''8 |
la'8. si'16 do''8 |
re''8. do''16 si'8 |
do''8. re''16 mi''8 |
fa''8. sol''16 la''8 |
re''8 mi''8. fa''16 |
re''4\trill do''8\fermata |
