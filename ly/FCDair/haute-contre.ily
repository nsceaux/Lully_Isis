\clef "haute-contre" do''4 do''8 |
do''4 do''8 |
si'4 do''8 |
si'8.\trill la'16 sol'8 |
sol'4 sol'8 |
fad'8.\trill sol'16 la'8 |
sol'8. sol'16 fad'8 |
sol'4 sol'8 |
sol'4 sol'8 |
si'8 si' dod'' |
re''4 do''8 |
sib'8. la'16 sol'8 |
la'4 la'8 |
re''4 re''8 |
re''8 do'' do'' |
mi''4 re''8 |
dod''4\trill re''8 |
re''4 sol'8 |
la' sol'4 |
fa' << la'8 \\ \sug sol' >> |
sol'4 sol'8 |
sol'4 sol'8 |
la'4 la'8 |
si' do''8. re''16 |
si'4\trill do''8\fermata |
