\clef "quinte" do'4 do'8 |
la8. si16 do'8 |
re'4 sol8 |
sol4 sol8 |
sol4 mi8 |
la4 la8 |
re'4 re'8 |
re'4 re'8 |
re'4 re'8 |
re'8 mi' mi' |
re'4 la8 |
sib sol sol' |
mi'4 mi'8 |
re'4 re'8 |
re' mi' do' |
la4 la'8 |
la'4 la'8 |
re'4 do'8 |
do' do'4 |
re'8. re'16 do'8 |
re'4 sol8 |
do'4 do'8 |
do' la re' |
re' do' la |
sol4 sol8\fermata |
