\clef "basse"
\footnoteHere #'(0 . 0) \markup { Matériel 1677 : B.C. tacet. }
 do8. re16 mi8 |
fa4 mi8 |
re4 do8 |
sol4 sol,8 |
do4. |
re4 do8 |
si,8. do16 re8 |
sol,8. la,16 si,8 |
sol,4 sol8 |
sol mi la |
re8. mi16 fa8 |
sol8. la16 sib8 |
la4. |
fa |
sol8 mi fa |
dod4 re8 |
la4 re'8 |
si4 do'8 |
la8 mi4 |
fa8. mi16 re8 |
sol4 sol8 |
mi8. re16 do si, |
la,8 la fa |
sol do fa, |
sol,4 do8\fermata |
