\clef "taille" sol'8. fa'16 mi'8 |
la'4 sol'8 |
fa'4 sol'8 |
sol' re' re' |
mi'4 mi'8 |
re'4 la8 |
si8. la16 la8 |
si4 si8 |
si4 si8 |
re'8 sol' la' |
la'4 la'8 |
sol' mi'4 |
mi'8. re'16 dod'8 |
fa'8. sol'16 la'8 |
sol'4 fa'8 |
mi' la re' |
mi' la' fad' |
sol'8. fa'16 mi'8 |
mi'8. re'16 do'8 |
<< \sugNotes { do'8. re'16 mi'8  } \\ { do'8. re'16 re'8 } >> |
si8. do'16 re'8 |
mi'8. fa'16 sol'8 |
fa'4 fa'8 |
fa' mi' do' |
sol'8. fa'16 mi'8\fermata |
