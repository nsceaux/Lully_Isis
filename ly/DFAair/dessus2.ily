\clef "dessus" <>^"Hautbois" do''4 re'' mi'' fa'' mi'' re'' |
mi''2 mi''4 r r mi'' |
la' si' do'' do''4. do''8 si'4 |
do''2 re''4 mi'' si' do'' |
si' do'' re'' do'' si' la' |
si' do'' re'' sol' sol' la' |
si'2.~ si' |
si'2 si'4 mi'' re'' dod'' |
re'' sib' la' la'4. sib'8 sol'4\trill |
la'2 fa''4 mi'' re'' dod'' |
re'' mi'' fa'' dod''2.\trill~ |
dod''2 dod''4 re'' re'' re'' |
mi'' dod''4. re''8 re''2 re''4 |
do'' sib' la' sib' do'' re'' |
la'2.\trill~ la'2 la'4 |
sib'4. sib'8 sib'4 do'' la' re'' |
si'2\trill mi''4 re'' do'' si' |
do'' re'' mi'' si'2.\trill~ |
si'2 si'4 do'' sol' la' |
si' sol' do'' do''4 si'4.\trill do''8 |
do''2.~ do'' |
r4 r si' mi'' re'' dod'' |
do''2.~ do'' |
