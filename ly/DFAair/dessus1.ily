\clef "dessus" <>^"Hautbois" mi''4 fa'' sol'' la'' sol'' fa'' |
sol''2 sol''4 r r sol'' |
do'' re'' mi'' fa'' re'' sol'' |
mi''2\trill fa''4 sol'' fa'' mi'' |
re'' mi'' fa'' mi'' re'' do'' |
re'' mi'' fa'' mi'' re'' do'' |
re''2.\trill~ re'' |
re''2 re''4 sol'' fa'' mi'' |
fa'' sol'' la'' re'' re'' mi'' |
dod''2\trill la''4 sol'' fa'' mi'' |
fa'' sol'' la'' mi''2.\trill~ |
mi''2 mi''4 fa'' fa'' fa'' |
sol'' mi''4.\trill re''8 re''2 sib''4 |
la'' sol'' fad'' sol'' la'' sib'' |
fad''2.\trill~ fad''2 fad''4 |
sol''4. sol''8 sol''4 la''4 fad''4.\trill sol''8 |
sol''2 sol''4 fa'' mi'' re'' |
mi'' fa'' sol'' re''2.\trill~ |
re''2 re''4 mi'' mi'' fa'' |
fa'' sol'' mi'' re'' re''4.\trill do''8 |
do''2.~ do'' |
r4 r re'' sol'' fa'' mi'' |
do''2.~ do'' |
