\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*4\break s2. s1*2\break s2.*2 s1 s2 \bar "" \break
        s2 s1 s2. s2 \bar "" \break s2 s1 s1. s2 \bar "" \break
        s4 s2.*6\pageBreak
        s2.*7\break s2.*2 s1 s2.*4\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
