\clef "basse" sib,1~ |
sib, |
mib2. re4 |
do2 sib, |
la,8. la,16 sol,8 fa, do do, |
\once\tieDashed fa,1~ |
fa,2 sib, |
sol, fad,4 |
sol, sol8 fa mib re |
do1 |
re2 si, |
do1 | \allowPageTurn
mi,2. |
fa,2 fa4 mib |
re2 do4 fa |
sib,1 la,2 |
sol,4 sol8 la sib mib |
fa sol fa mib re4 |
mib8 fa mib re do4 |
fa fa,2 |
sib, sib4 |
fad2. |
sol2 sol,4 |
do8 re do sib, la,4 |
re4. mi8 fa re mi fa sol mi fa sib, |
do2 do,4 |
fa,4 fa8 sol fa mib |
re4 do sib, |
mib4 re mib |
sib,8 la, sib, do re sib, |
mib fa sol4 fa8 mib |
re4 re8 mi fa4 fa,8 sol, |
la,4 la,4. sib,8 |
do2 do,4 |
fa,4 fa8 sol fa mib |
re4 do sib, |
la,2 sib,4 |
fa,4. sol,8 la, fa, |
sib, la, sib, do re mib |
fa2 mib4. re8 |
do re mib2 |
re8 mib fa4 fa, |
sib,4. do8 sib, la, |
\once\set Staff.whichBar = "|"
\custosNote sol,8
