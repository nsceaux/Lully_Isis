\clef "vbasse-taille" r2 r4 <>^\markup\character Hierax r8 fa |
sib re' sib sib fa fa sol lab |
sol8\trill sol r sol16 la sib8 sib si\trill si16 do' |
do'8 do' r do'16 do' mi4\trill r8 mi16 mi |
fa8. fa16 sol8 la sol8.\trill fa16 |
fa4 r8 do'16 sib la8\trill la16 sol fa8 fa |
do8. do16 do8 fa re4\trill r16 fa fa fa |
sib4 r8 sol re' re' |
si4\trill si8 si16 si do'8 re' |
mib'8 mib' r do' la\trill la16 la la8\trill la16 la |
fad8\trill fad re'8. fad16 sol8 sol16 sol sol8. sol16 |
mi4\trill r8 do sol sol sol sol |
do'4 do'8 do'16 sib sib8 la |
la16\trill la r do' do' do' la la fa fa fa fa sol8. la16 |
sib4 r8 sib16 sib do'4 do'16 do' re' mib' |
re'2\trill r4 re8 \footnoteHere #'(0 . 0) \markup { Matériel 1677 : \italic mi♭ }
mib? fa4 sol8 la |
sib4 sib8 do' re' mib' |
do'4\trill do' sib8 la |
sol4\trill r mib'8 re' |
do'4\trill do'8 re' mib' fa' |
re'4\trill sib r8 re' |
do'4.\trill sib8 la4 |
sib2 sib8 la |
sol4\trill sol do'8 do' |
do'4 sib4.\trill sib8 |
sib2 la8 sib |
sol2\trill sol8 la |
fa2 r4 |
sib2 sib8 sib |
sib2~ sib8 sib |
sib2.~ |
sib2 sib4 |
r4 fa8 sol la4 la8 sib |
do'4 do'4. re'8 |
sib2\trill la8 sib |
la2\trill r4 |
fa2 fa8 fa |
fa2~ fa8 fa |
fa2.~ |
fa2 fa4 |
r la8 sib do'4 do'8 re' |
mib'4 do'4.\trill sib16[ la] |
sib2 sib8 la |
sib2. |
