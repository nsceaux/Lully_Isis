\key re \minor
\time 4/4 \midiTempo#80 s1*4
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\digitTime\time 3/4 s2.*2
\time 4/4 s1*3
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\time 3/2 \midiTempo#160 s1.
\digitTime\time 3/4 s2.*16
\time 4/4 s1
\digitTime\time 3/4 s2.*7
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.*3 \bar "|."
\once\override Staff.TimeSignature.stencil = ##f