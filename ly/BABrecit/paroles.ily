Ces -- sons, ces -- sons d’ai -- mer une In -- fi -- del -- le,
é -- vi -- tons la hon -- te cru -- el -- le
de ser -- vir, d’a -- do -- rer qui ne nous ai -- me plus ;
a -- che -- vons de bri -- ser les nœuds qu’elle a rom -- pus :
dé -- ga -- geons- nous, sor -- tons, sor -- tons d’un si fu -- neste em -- pi -- re.
He -- las ! mal -- gré moy je sou -- pi -- re,
ah ! mon cœur, quel -- le la -- che -- té !
Quel char -- me te re -- tient dans un hon -- teux mar -- ti -- re ?
Tu n’as pas craint des fers qui nous ont tant coû -- té,
as- tu peur de la li -- ber -- té ?
Re -- ve -- nez, re -- ve -- nez, Li -- ber -- té char -- man -- te,
re -- ve -- nez, re -- ve -- nez, Li -- ber -- té char -- man -- te,
vous n’ê -- tes que trop di -- li -- gen -- te,
lors qu’il faut dans un cœur fai -- re place à l’a -- mour ;
mais que vous ê -- tes len -- te,
lors qu’un jus -- te dé -- pit pres -- se vô -- tre re -- tour.
Mais que vous ê -- tes len -- te,
lors qu’un jus -- te dé -- pit pres -- se vô -- tre re -- tour.