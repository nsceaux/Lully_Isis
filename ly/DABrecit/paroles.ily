Dans ce so -- li -- tai -- re sé -- jour
vous ê -- tes sous ma garde, & Ju -- non vous y lais -- se :
mes yeux veil -- le -- ront tour à tour,
et vous ob -- ser -- ve -- ront sans ces -- se.

Est- ce là le bon- heur que Ju -- non m’a pro -- mis ?
Ar -- gus, ap -- pre -- nez- moy quel cri -- me j’ay com -- mis.

Vous es -- tes ai -- ma -- ble,
vos yeux de -- voient moins char -- mer ;
Vous es -- tes cou -- pa -- ble,
vous es -- tes cou -- pa -- ble
de vous fai -- re trop ai -- mer.

Ne me dé -- gui -- sez rien, de quoy m’ac -- cu -- se- t’el -- le ?
Quelle of -- fense à ses yeux me rend si cri -- mi -- nel -- le ?
Ne pou -- ray-je ap -- pai -- ser son fu -- nes -- te cou -- roux ?

C’est une of -- fen -- se cru -- el -- le,
de pa -- roî -- tre bel -- le
à des yeux ja -- loux.
L’a -- mour de Ju -- pi -- ter a trop pa -- ru pour vous.

Je suis perdu -- ë, ô Ciel ! si Ju -- non est ja -- lou -- se.

On ne plaît guere à l’e -- pou -- se,
lors qu’on plaît tant à l’e -- poux.
Vous n’en se -- rez pas mieux d’être in -- grate & vo -- la -- ge.
Vous n’en se -- rez pas mieux d’être in -- grate & vo -- la -- ge.
Vous quit -- tez un fi -- delle A -- mant,
pour re -- ce -- voir un plus bril -- lant hom -- ma -- ge ;
Mais, c’est un a -- van -- ta -- ge
que vous pay -- e -- rez che -- re -- ment.
Vous n’en se -- rez pas mieux d’être in -- grate & vo -- la -- ge.
Vous n’en se -- rez pas mieux d’être in -- grate & vo -- la -- ge.
J’ay l’or -- dre d’en -- fer -- mer vos dan -- ge -- reux ap -- pas,
la Dé -- es -- se dé -- fend que vous voy -- ez per -- son -- ne.

Aux ri -- gueurs de Ju -- non, Ju -- pi -- ter m’a -- ban -- don -- ne !
Non, non, Ju -- pi -- ter ne m’ai -- me pas.
