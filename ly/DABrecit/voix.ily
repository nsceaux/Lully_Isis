\ffclef "vbasse-taille" <>^\markup\character Argus
r8 fa8. fa16 fa fa do8 do16 fa |
re8\trill r16 sib, re re re mib fa8 fa16 fa sib8 sib16 do' |
la8\trill la r do' la\trill la16 sol fa8 fa16 mi |
re8\trill sol sol16 sol sol la re8 sol |
do do
\ffclef "vbas-dessus" <>^\markup\character Io
r8 do''16 do'' sol'8\trill sol'16 la' |
sib'4 sib'8 re'' sib'4 sib'8 la' |
la'4\trill r8 re'' si'\trill si' si' re'' |
sol' fa' fa' fa' fa' mi' |
mi'2\trill
\ffclef "vbasse-taille" <>^\markup\character Argus
r8 do' |
la4.\trill sol8 fa4 |
do'2 do'4 r8 do' re'4. la8 |
sib4 sol4.\trill sol8 |
sol2 r8 sol |
sib2 sib8 do' |
la4\trill la r8 la |
do'2 do'8 do' |
fa4 fa sib8 sib |
sib2 la4 |
la( sol4.)\trill fa8 |
fa2
\ffclef "vbas-dessus" <>^\markup\character Io
la'8 la'16 la' la'8 do'' |
fa'4. fa'8 sib' sib' sib' sib' |
sol'\trill sol' r sib'16 sib' mib''8 mib''16 mib'' |
do''4.\trill do''8 re'' re'' re'' re'' |
si'\trill si' r sol'16 sol' sol'8 la'16 si'! |
do''4 do''8 do'' re''4 re''8 re'' |
sol'4
\ffclef "vbasse-taille" <>^\markup\character Argus
mi8 mi16 mi fa8 sol16 la |
re8 re r sib16 sib la8.\trill sol16 |
fad8\trill fad la sib la8.\trill sol16 |
sol4 r8 sol sib16 sib sib sol |
re'4 sib8 sib16 sib sol8 sol |
mi4\trill
\ffclef "vbas-dessus" <>^\markup\character Io
r16 do'' do'' do'' fa''8 fa'' |
re''4\trill re''8 fa'' sib'4 sib'8 la' |
la'\trill la'
\ffclef "vbasse-taille" <>^\markup\character Argus
r16 fa fa fa re8\trill mib16 fa |
sol8 sol sol sol16 la sib4 sib8 la |
sib2 r4 fa^\markup\italic { [fort guay] } |
fa4. sol8 la4 sib |
do'2 sol4 la |
fa2 re4. sol8 |
mi2\trill do4. do'8 |
do'4. la8 sib4 do' |
re'2 la4 sib |
do'2 fa4. sol8 |
la2( sol)\trill |
fa la4. la8 |
sib2 sol4.\trill fa8 |
mi2. re4 |
dod1\trill |
r4 mi mi la |
fad sol la sib |
do'2( sib4.\trill) la8 |
sib2( la2)\trill |
sol sib |
la4.\trill la8 la4 re' |
si2\trill sol |
mi4 mi fa4. sol8 |
re2\trill re4 mi |
do2 r4 fa |
fa4. sol8 la4 sib |
do'2 sol4 la |
fa2 re4. sol8 |
mi2\trill do4. do'8 |
do'4. la8 sib4 do' |
re'2 la4 sib |
do'2 fa4. sol8 |
la2( sol)\trill |
fa4 r8 do' fa fa fa fa |
do do do^\markup\croche-pointee-double re mib8. fa16 |
re4\trill r8 fa16 fa sib8 sib16 re' |
sol8.\trill re16 re8 re mi fa |
mi\trill mi
\ffclef "vbas-dessus" <>^\markup\character Io
r8 do''16 do'' do''8 re''16 la' |
sib'8 sib'16 do'' re''8 re''16 mi'' fa''8 fa''16 r la'4 |
re'' sib'8 sib' sol'\trill sol' sol' la' |
fa'2 r |
R1 |
