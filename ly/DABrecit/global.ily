\key fa \major
\digitTime\time 3/4 \midiTempo#80 s2.
\time 4/4 s1*2
\digitTime\time 3/4 s2.*2
\time 2/2 \midiTempo#160 s1*2
\digitTime\time 3/4 \midiTempo#80 s2. \midiTempo#120 s2.*12
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.
\digitTime\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2.*6
\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2.
\time 4/4 s1
\digitTime\time 2/2 \midiTempo#160 s1*31
\time 2/2 s1
\digitTime\time 3/4 \midiTempo#80 s2.*4
\time 4/4 s1
\time 2/2 \midiTempo#160 s1*3 \bar "|."
