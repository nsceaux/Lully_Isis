\clef "basse" fa,2. |
sib,2 la,4 sol, |
fa,1 |
sol,4 mi,8. fa,16 sol,4 |
do, do4. sib,16 la, |
sol,1 |
re2 sol |
si,4~ si,2 |
do4. sib,8 la,8. sol,16 |
fa,2 fa4 |
mi4. re8 do4 |
fa sib,8 do re4 |
sol,8 la, sib,4 si, |
do2~ do8 do |
sol,2~ sol,8 sol, |
re2~ re8 re |
la,2~ la,8 la, |
sib, do sib, la, sol,4 |
do2 re4 |
sib, do do, |
fa,2 \once\tieDashed fa~ |
fa re |
mib do4 |
lab2 fa |
sol~ sol8 fa |
mib2 re |
do2 la,4 |
sib,2 do4 |
re4. do8 re re, |
\once\tieDashed sol,2.~ |
sol, | \allowPageTurn
do2 la,4 |
sib,1 |
fa,2 sib,4 |
mib2 re8 sib, fa,4 |
sib,4. do8 re4 sib, |
fa2 fa, |
mi,1 |
re,2 sol, |
do4. re8 mi4 do |
fa4 mib re do |
sib, do re2 |
la,2. sib,4 |
do sib, do do, |
fa,1 |
sol,2. la,4 |
sib,2 sol, |
la,2. si,4 |
dod2 la, |
re4 sib, la, sol, |
fad,2 sol,4 do, |
re,1 |
sol,2. sol,4 |
re mi fa re |
sol fa mi re |
do2 fa, |
sol,1 |
do4. sib,8 la,4 sib, |
fa,1 |
mi, |
re,2 sol, |
do4. re8 mi4 do |
fa4 mib re do |
sib, do re2 |
la,2. sib,4 |
do sib, do do, |
\once\tieDashed fa,1~ |
fa,2. |
sib,2. |
si, |
do2 fad,4 |
sol,2 re |
sib, do4 do, |
fa,2. mi,4 |
\once\tieDashed re,1*15/16~ \hideNotes re,16 |
