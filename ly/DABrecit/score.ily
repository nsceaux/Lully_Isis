\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2. s1*2\break s2.*2\break s1*2 s2. s2 \bar "" \break
        s4 s2.*8\break s2.*3 s1*2\break s2. s1 s2.\pageBreak
        s1 s2.*2\break s2.*4\break s1 s2. s1\break
        s1*7\break s1*8\break s1*7 s2 \bar "" \pageBreak
        s2 s1*5 s2 \bar "" \break s2 s1*3 s2.*2\break s2.*2 s2 \bar "" \break
      }
    >>
  >>
  \layout { }
  \midi { }
}
