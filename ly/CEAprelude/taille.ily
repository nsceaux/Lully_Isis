\clef "taille" fad'4. fad'8 fad'4. sol'8 |
la'4. la'8 la'4. sol'8 |
fad' mi' re'4 re'4. re'8 |
re'4. re'8 fad' sol' la' fad' |
si' la' sol' fad' mi'4.\trill mi'8 |
re'4 re'8 dod' si4 mi'8 re' |
dod'1 |
do'2. do'4 |
si sol' la'4. la'8 |
la'4. la'8 la'4 sol' |
sol'4. sol'8 fad'4. fad'8 |
fad' mi' re' dod' si4 mi' |
mi' dod'\trill re'4. re'8 |
re'4 mi' mi'4.\trill re'8 |
re'4. re'8 re'4 re' |
do'4. do'8 do'4 do' |
si8 dod' re'4 si mi' |
mi' dod' re'4. re'8 |
re'4 mi' mi' la'8 sol' |
fad'1\trill |
