\clef "haute-contre" la'4. la'8 la'4. la'8 |
re''2 re''4 la' |
la'2 la'4. la'8 |
sol'4 si' la'4. la'8 |
sol' la' si'4 mi''4. mi''8 la'4 si' si'4.\trill la'8 |
la'1 |
sol'4. sol'8 sol'4 la' |
si'4. si'8 dod'' re'' mi''4 |
la'4. la'8 la'4 si' |
sol'4. la'8 la'4.\trill sol'8 |
fad'2 mi'4 mi' |
mi'4. mi'8 fad' sol' la' la' |
sol'4. sol'8 mi'4. la'8 |
fad'4.\trill la'8 la'4 si' |
sol'4. la'8 la'4.\trill sol'8 |
fad'2\trill mi'4. mi'8 |
mi'4.\trill mi'8 re'4 re''8 re'' |
re'' \footnoteHere #'(0 . 0) \markup {
  Matériel 1677 : \italic do♮
} do''? si' do'' dod''?4.\trill re''8 |
re''1 |
