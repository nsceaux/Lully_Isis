\clef "basse" re2 re, |
re4. la,8 re4. mi8 |
fad4. fad8 fad4. fad8 |
sol4. sol8 fad2 |
mi4. re8 dod4.\trill dod8 |
re4. re8 mi4 mi, |
la,4. la,8 la,4 si, |
do4. do8 si,4 la, |
mi8 fad mi re dod\trill si, dod la, |
re4. re'8 re'4 si |
do'4. do'8 la4. la8 |
si4. la8 sold2 |
la4. sol8 fad4. fad8 |
sol4. sol8 la4 la, |
re4. re8 re4 si, |
do4. do8 la,4. la,8 |
si,4. la,8 sold,2 |
la,4. sol,8 fad,4. fad,8 |
sol,4. sol,8 la,2 |
re,1 |
