\clef "dessus" re''4. la'8 re''4. mi''8 |
fad''4. fad''8 fad''4. sol''8 |
la'' sol'' fad'' mi'' re''8 do''16 si' do''8. re''16 |
si'4.\trill re''8 red''4.\trill red''8 |
mi''4. fad''8 sol'' mi'' la'' sol'' |
fad'' sol'' \once\slurDashed fad''( sol'') sold''4.\trill la''8 |
la''1 |
mi''4. mi''8 mi''4. fad''8 |
\once\slurDashed sol''4.( la''16 si'') la''4.\trill sol''8 |
fad''4.\trill fad''8 fad''4 sol'' |
mi''4. fad''8 fad''4.\trill mi''8 |
re'' dod'' si'4 mi''4. mi''8 |
dod''4\trill la'2 la'8 la' |
si' do'' \once\slurDashed si'( do'') dod''?4.\trill re''8 |
re''4. fad''8 fad''4 sol'' |
mi''4. fad''8 fad''4.\trill mi''8 |
re'' dod'' si'4 mi''4. mi''8 |
dod''4\trill la' la''4. la''8 |
si''4. mi''8 mi''4.\trill re''8 |
re''1 |
