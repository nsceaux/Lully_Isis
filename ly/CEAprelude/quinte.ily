\clef "quinte" la4. re'8 re'2 |
re' re'4.\trill dod'8 |
re'2 la4. la8 |
si2. si4 |
si2 la4. la8 |
la4 fad mi4.\trill mi8 |
mi1 |
mi' |
mi'2 mi'4. mi'8 |
re'4. re'8 re'4 re' |
do'4. do'8 do'4 do' |
si2 si4 si |
la2 la4.\trill la8 |
sol4 si la4. la8 |
la4. la8 la4 sol |
sol4. sol8 fad4. fad8 |
fad4 si si4. si8 |
la2 la4. la8 |
sol4 si la4. la8 |
la1 |
