\bookpart {
  \actn "Prologue"
  %% 0-1
  \pieceToc "Ouverture"
  \includeScore "AAAouverture"
  \markup\sep
  \markup\justify {
    Le Théatre représente le Palais de la \smallCaps Renommée : il est
    ouvert de tous côtez pour recevoir les nouvelles de ce qui se fait
    de considerable sur la Terre, & de ce qui se passe de mémorable
    sur la Mer, que l’on découvre dans l’enfoncement. La Divinité qui
    préside dans ce Palais y paroît accompagnée de sa suite
    ordinaire : Les Rumeurs & les Bruits qui portent comme elle,
    chacun une Trompette à la main, y viennent en foule de divers
    endroits du monde.
  }
}
\bookpart {
  \scene "Scene Premiere" "Scene I"
  \sceneDescription\markup\wordwrap-center {
    La Renommée & sa Suite, les Rumeurs, et les Bruits.
  }
  %% 0-2
  \pieceToc\markup\wordwrap {
    Chœur : \italic { Publions en tous lieux }
  }
  \includeScore "AABchoeur"
}
\bookpart {
  %% 0-3
  \pieceToc\markup\wordwrap {
    La Renommée, chœur : \italic { C’est luy dont les Dieux ont fait choix }
  }
  \includeScore "AACchoeur"
}
\bookpart {
  \scene "Scene II" "Scene II"
  \sceneDescription\markup\column {
    \wordwrap-center {
      Deux Tritons chantants, Dieux Marins joüants des Instruments,
      & dansants. Neptune, la Renommée & sa Suite.
    }
    \wordwrap-center {
      Les Tritons, & les autres Dieux Marins accompagnent Neptune
      sortant de le Mer, qui entre dans le Palais de la Renommée.
    }
  }
  %% 0-4
  \pieceToc\markup\wordwrap { Premier air des Tritons }
  \includeScore "ABAair"
}
\bookpart {
  %% 0-5
  \pieceToc\markup\wordwrap {
    Deux Tritons : \italic { C’est le Dieu des Eaux qui va paraistre }
  }
  \includeScore "ABBtritons"
}
\bookpart {
  %% 0-6
  \pieceToc\markup\wordwrap { Deuxième air des Tritons }
  \includeScore "ABCair"
}
\bookpart {
  %% 0-7
  \pieceToc\markup\wordwrap {
    Netpune, la Renommée : \italic { Mon Empire a servy de Theatre à la Guerre }
  }
  \includeScore "ABDrecit"
  %% 0-8
  \pieceToc\markup\wordwrap {
    Chœur : \italic { Celebrons son grand Nom sur la Terre & sur l’Onde }
  }
  \includeScore "ABEchoeur"
}
\bookpart {
  \scene "Scene III" "Scene III"
  \sceneDescription\markup\wordwrap {
    Les neuf Muses, les Arts Liberaux, Apollon, Neptune & sa Suite,
    la Renommée & sa Suite.
  }
  %% 0-9
  \pieceToc "Prélude des Muses"
  \includeScore "ACAprelude"
}
\bookpart {
  %% 0-10
  \pieceToc\markup\wordwrap {
    Calliope, Thalie, Apollon, Melpomene :
    \italic { Cessez pour quelque temps, bruit terrible des Armes }
  }
  \includeScore "ACBrecit"
  %% 0-11
  \pieceToc "Premier air pour les Muses"
  \includeScore "ACCair"
}
\bookpart {
  %% 0-12
  \pieceToc "Deuxième air pour les Muses"
  \includeScore "ACDair"
  %% 0-13
  \pieceToc\markup\wordwrap {
    Apollon : \italic { Ne parlons pas toujours de la Guerre cruelle }
  }
  \includeScore "ACErecit"
}
\bookpart {
  %% 0-14
  \pieceToc\markup\wordwrap {
    Chœur : \italic { Ne parlons pas toujours de la Guerre cruelle }
  }
  \includeScore "ACFchoeur"
}
\bookpart {
  %% 0-15
  \pieceToc "Air [pour les Trompettes]"
  \includeScore "ACGair"
}
\bookpart {
  %% 0-16
  \pieceToc\markup\wordwrap {
    La Renommée, Apollon, Neptune, chœur : \italic { Hastez-vous, Plaisirs, hastez-vous }
  }
  \includeScore "ACHchoeur"
  \markup\wordwrap { 
    [Matériel 1677] \italic {
      On joüe l’Air des Trompettes [page \page-refII #'ACGair "]," & sur la
      derniere note on recommence le Chœur \normal-text Hastez-vous.
      \raise#1 \musicglyph #"scripts.segno"
    }
  }
}
\bookpart {
  %% 0-17
  \pieceToc "Ouverture"
  \includeScore "ACIouverture"
  \actEnd "FIN DU PROLOGUE"
}
