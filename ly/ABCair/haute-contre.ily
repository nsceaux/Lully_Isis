\clef "haute-contre" si'4 |
sol'2 sol'4 |
sol' la' fad'\trill |
sol'2 sol'4 |
fad' sol'2 |
sol'4 sol'4. do''8 |
do''4 la' sol'8 la' |
si' do'' re''4. re''8 |
dod''4 re'' r8 re'' |
re''2 re''4 |
sol'4. sol'8 do''4 |
si'2 mi''8 re'' |
dod''4.\trill si'8 la' sol' |
fad'\trill sol' la'4 re' |
sol' fad' sol' |
sol'8 la' la'4.\trill sol'8 |
\footnoteHere #'(0 . 0) \markup {
  Ballard 1719 : \raise#1 \score {
    \new Staff {
      \tinyQuote \key sol \major \time 3/4 \clef "petrucci-c1" fad'4\trill fad'4. fad'8 |
    }
    \layout { \quoteLayout }
  }
}
fad'4\trill sol'4. la'8 |
si'2. |
do'' |
la'2. |
la'4. la'8 si' do'' |
re''2 re''4 |
si'2\trill
