\clef "taille" sol'4 |
sol'2 re'4 |
do'2 re'4 |
re'4. re'8 dod'4 |
re'4 sol2 |
re'4 do' mi' |
mi' re'8 do' si do' |
re'2 re'4 |
la' fad' r8 la' |
la'4. la'8 sol'4 |
sol'8 fad' mi'4 mi' |
mi' si2\trill |
la4 mi' re' |
re'2 re'4 |
mi' re' re' |
mi' mi'2\trill |
re'4 re'4. re'8 |
sol'2. |
sol'4 la'2 |
la'2. |
<< \sugNotes { re'4. re'8 sol'4 } \\ { re'8 mi' fad'4 re' | } >>
sol' fad'4.\trill sol'8 |
sol'2
