\clef "quinte" re'4 |
re'4. do'8 si4 |
mi' mi' re' |
si8 la sol2 |
la4 mi'4. si8 |
si4 sol mi |
la la re' |
re'8 do' si4 si |
la la r8 re' |
re'2 re'4 |
do'2 do'8 re' |
mi'2 mi'4 |
mi' la2 |
la sol4 |
sol re' sol |
sol do' la |
la si4. re'8 |
re'2. |
do'2 mi'4 |
re'4. re'8 do' si |
la2 sol8 la |
si do' re'4. re'8 |
re'2
