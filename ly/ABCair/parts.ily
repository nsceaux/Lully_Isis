\piecePartSpecs
#`((dessus #:system-count 3)
   (haute-contre #:system-count 3)
   (taille #:system-count 3)
   (quinte #:system-count 3)
   (basse #:system-count 3)
   (basse-continue #:system-count 3)
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
