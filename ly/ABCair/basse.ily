\clef "basse"
\footnoteHere #'(0 . 0) \markup { Matériel 1677 : B.C. tacet. }
sol,4 |
sol4. la8 si4 |
do' la re' |
sol4. fad8 mi4 |
re mi2 |
si,4 do2 |
la,4 re sol, |
sol8 la si4 sol |
la re r8 re' |
re'4. do'8 si4 |
do'4. si8 la4 |
mi4. re8 mi4 |
la,2 re4~ |
re8 mi fad4 sol |
do re si, |
mi do2 |
re4 sol,2 |
sol8 fad sol la si sol |
do' si la si do' la |
re' re' do' si la sol |
fad mi re do si, la, |
sol,4 re re, |
<<
  \tag #'basse { sol,2 }
  \tag #'basse-continue {
    \measure 2/4
    sol,2~ |
    \measure 3/4 sol,4 sol8 fad mi re |
    \time 2/2 do1 |
  }
>>
