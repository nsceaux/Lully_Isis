\clef "dessus" sol''4 |
si'4. do''8 re''4 |
mi'' do'' la'8 si' |
si'4.\trill la'8 sol'4 |
re'' si'4.\trill sol''8 |
sol''4 mi''4. la''8 |
la''4 fad'' si''~ |
si''8 la'' sol''4.\trill fad''8 |
mi''4\trill re'' r8 fad'' |
fad''4. fad''8 sol''4 |
mi''4.\trill mi''8 la''4 |
la''4. si''8 sold''4\trill |
la''4. sol''8 fad'' mi'' |
re''4 do''4.\trill si'8 |
do''4 la' si'8 do'' |
si' do'' do''4.\trill si'8 |
la'4\trill sol'4. re''8 |
re''2. |
mi''\trill |
fad''4.\trill sol''8 fad'' sol'' |
la''8. sol''16 fad''4 sol''~ |
sol''8 la'' la''4.\trill sol''8 |
sol''2
