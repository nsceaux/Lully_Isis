\tag #'(recit1 basse) {
  Ve -- nez, ve -- nez Dé -- esse im -- pi -- toy -- a -- ble,
  ve -- nez, voy -- ez, re -- con -- nois -- sez
  cet -- te Nym -- phe mou -- rante au -- tre -- fois trop ai -- ma -- ble.
  C’est as -- sez la pu -- nir, c’est vous van -- ger as -- sez,
  l’é -- clat de sa beau -- té ne la rend plus cou -- pa -- ble ;
  par la cru -- elle hor -- reur du tour -- ment qui l’ac -- ca -- ble,
  son crime & ses ap -- pas sont en -- semble ef -- fa -- cez.
  Sans ja -- lou -- sie, & sans al -- lar -- mes,
  voy -- ez ses yeux noy -- ez de lar -- mes
  que l’om -- bre de la mort com -- men -- ce de cou -- vrir.
  
  Ils n’ont en -- cor que trop de char -- mes
  puis qu’ils sça -- vent vous at -- ten -- drir.
  
  U -- ne jus -- te pi -- tié peut- el -- le vous ai -- grir ?
  Vô -- tre cour -- roux fa -- tal ne doit- il point s’é -- tein -- dre ?
  
  Ah ! vous la plai -- gnez trop, el -- le n’est pas à plain -- dre ;
  non, el -- le ne peut trop souf -- frir.
  
  Je sçay que c’est de vous que son sort doit dé -- pen -- dre.
  Ce n’est qu’à vos bon -- tez qu’el -- le doit re -- cou -- rir.
  Il n’est rien que de moy vous ne de -- viez at -- ten -- dre,
  si je puis o -- bli -- ger vô -- tre haine à se ren -- dre.
  
  Ah ! lais -- sez- moy mou -- rir.
  
  Pre -- nez soin de la se -- cou -- rir.
  
  Vous l’ai -- mez d’un a -- mour trop ten -- dre ;
  non, el -- le ne peut trop souf -- frir.
  
  Quoy ! le cœur de Ju -- non, quel -- que grand qu’il puisse ê -- tre,
  ne sçau -- roit tri -- om -- pher d’une in -- jus -- te fu -- reur ?
  
  De la terre & du ciel Ju -- pi -- ter est le maî -- tre,
  et Ju -- pi -- ter n’est pas le maî -- tre de son cœur ?
  
  Hé bien, il faut que je com -- men -- ce
  à me vaincre en ce jour.
  
  Vous m’ap -- pren -- drez à me vaincre à mon tour.
}
\tag #'(recit1 basse) {
  J’a -- ban -- don -- ne -- ray ma ven -- gean -- ce,
  ren -- dez- moy vôtre a -- mour.
  J’a -- ban -- don -- ne -- ray ma ven -- gean -- ce,
  ren -- dez- moy, ren -- dez- moy vôtre a -- mour ?
  J’a -- ban -- don -- ne -- ray ma ven -- gean -- ce,
  ren -- dez- moy vôtre a -- mour ?
  Ren -- dez- moy, ren -- dez- moy vôtre a -- mour.
  J’a -- ban -- don -- ne -- ray ma ven -- gean -- ce,
  ren -- dez- moy, ren -- dez- moy vôtre a -- mour,
  ren -- dez- moy, ren -- dez- moy vôtre a -- mour.
}
\tag #'recit2 {
  A -- ban -- don -- nez, a -- ban -- don -- nez vô -- tre ven -- gean -- ce,
  je vous rends mon a -- mour.
  A -- ban -- don -- nez, a -- ban -- don -- nez vô -- tre ven -- gean -- ce,
  je vous rends, je vous rends mon a -- mour.
  Je vous rends mon a -- mour.
  A -- ban -- don -- nez, a -- ban -- don -- nez vô -- tre ven -- gean -- ce,
  je vous rends mon a -- mour.
  A -- ban -- don -- nez, a -- ban -- don -- nez vô -- tre ven -- gean -- ce,
  je vous rends mon a -- mour,
  je vous rends, je vous rends mon a -- mour.
}
\tag #'(recit1 basse) {
  Noi -- res On -- des du Stix, c’est par vous que je ju -- re ;
  Fleuve af -- freux, é -- cou -- tez le ser -- ment que je fais.
  Si cet -- te Nymphe, en -- fin, re -- prend tous ses at -- traits,
  si Ju -- non fait ces -- ser les tour -- ments qu’elle en -- du -- re,
  je ju -- re que ses yeux ne trou -- ble -- ront ja -- mais
  de nos cœurs ré -- ü -- nis la bien- heu -- reu -- se paix.
  Noi -- res On -- dres du Stix, c’est par vous que je ju -- re ;
  Fleuve af -- freux, é -- cou -- tez le ser -- ment que je fais.
  
  Nym -- phe, je veux fi -- nir vô -- tre peine é -- ter -- nel -- le,
  que la Fu -- rie em -- porte aux en -- fers a -- vec el -- le
  le trouble & les hor -- reurs dont vos sens sont sai -- sis :
  que la Fu -- rie em -- porte aux en -- fers a -- vec el -- le,
  le trouble & les hor -- reurs dont vos sens sont sai -- sis.
  
  A -- près un ri -- gou -- reux sup -- pli -- ce,
  goû -- tez les biens par -- faits que les Dieux ont choi -- sis :
  et sous le nou -- veau nom d’I -- sis,
  joü -- is -- sez d’un bon- heur qui ja -- mais ne fi -- nis -- se.
  Et sous le nou -- veau nom d’I -- sis,
  joü -- is -- sez d’un bon- heur,
  joü -- is -- sez d’un bon- heur qui ja -- mais ne fi -- nis -- se.
  Joü -- is -- sez d’un bon- heur,
  joü -- is -- sez d’un bon- heur qui ja -- mais ne fi -- nis -- se.
}
\tag #'(recit1 recit2 basse) {
  Dieux, re -- ce -- vez I -- sis au rang des Im -- mor -- tels.
  Dieux, re -- ce -- vez I -- sis au rang des Im -- mor -- tels :
  Peu -- ples voi -- sins du Nil, dres -- sez- luy des au -- tels,
  dres -- sez- luy des au -- tels.
}
