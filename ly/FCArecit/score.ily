\score {
  \new ChoirStaff <<
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'recit1 \includeNotes "voix"
    >> \keepWithTag #'recit1 \includeLyrics "paroles"
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'recit2 \includeNotes "voix"
    >> \keepWithTag #'recit2 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2 s2.\break s1*2 s2.\break s2.*4\break
        s1*4\break s1*3 s2 \bar "" \break s2 s1*2\pageBreak
        s1 s2.*2\break s2.*2 s1\break s2.*3\break
        s1*3\break s2. s1 s2.\break s2. s1 s2.*2\pageBreak
        s1*2 s2. s2 \bar "" \break s4 s2. s1 s2.\break s1 s2. s1 s2 \bar "" \break
        s4 s2. s1 s2 \bar "" \break s4 s1*2\break s1*3\pageBreak
        s1*3\break s1*2 s2 \bar "" \break s2 s1*2\break s1*3\pageBreak
        s1*3 s2.\break s1*2 s2. s2 \bar "" \break s2 s1*2 s2.\break
        s1*4\break s1 s2. s1*3\break s1*2 s2. s1\pageBreak
        s1*2\break s1*3\break s1 s2. s1*3 s2 \bar "" \break
        s2 s1 s2. s1*3\break s1*2 s2. s1*3\break s1*6\pageBreak
        s1*7\break s1*4\break 
      }
      \modVersion {
        s1*2 s2. s1*2 s2.*5 s1*3 s1 s1*2 s1 s1*4 s2.*4 s1 s2.*3 s1*3 s2. s1
        s2.*2 s1 s2.*2 s1*2 s2.*3 s1 s2. s1 s2. s1 s2.*2 s1 s2. s1 s1 s1*14\break
      }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
