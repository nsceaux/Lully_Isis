<<
  \tag #'(recit1 basse) {
    \ffclef "vbasse" <>^\markup\character Jupiter
    r4 r8 re la8. la16 re'8. la16 |
    fa4\trill fa8 fa16 sol mi4\trill mi8 r16 la |
    fa8. sib16 sol8.\trill do'16 do'8 do' |
    do'4 la8 la sib4 sib8 sib |
    sol4\trill sol8 la fa4\trill fa8 mi |
    mi4\trill mi8 la16 la mi8\trill mi16 la |
    fad4\trill la8 la16 la re'8 la |
    sib8 r16 sol re'8 sib sib sib |
    sol\trill sol sol sol re8. sol16 |
    mi8.\trill mi16 do'8 do'16 do' mi8 mi |
    fa4 fa8 la re4 re8 mi |
    dod4\trill dod8 la re' do' sib la |
    sib4 sol8 sol mi4\trill mi8 fa |
    re2 r |
    r4 la8 la16 si do'8 do' do' do' |
    do'4 do' r8 la la sib |
    sol4\trill sol sol( fad8) sol |
    fad4\trill fad8 la sib sib sib sib |
    mi4\trill r8 mi fa fa fa mi |
    fa
    \ffclef "vbas-dessus" <>^\markup\character Junon
    do''8 do'' do'' dod''\trill dod'' dod'' re'' |
    mi'' mi'' r mi''16 fa'' re''8\trill re''16 do'' si'8 do'' |
    la'4\trill
    \ffclef "vbasse" <>^\markup\character Jupiter
    r8 mi16 mi la8 la16 la |
    fad8.\trill sib16 sol8\trill sol sol fad |
    sol4 sib8 sib16 sib re8\trill mi |
    fa fa fa la fa8.\trill mi16 |
    mi8\trill mi
    \ffclef "vbas-dessus" <>^\markup\character Junon
    mi''4. si'16 si' si'8 si' |
    sold'4\trill si'8 si'16 si' mi''8. mi''16 |
    dod''4\trill dod'' fa''~ |
    fa'' mi''16 mi'' fa'' sol'' dod''8.\trill re''16 |
    re''4
    \ffclef "vbasse" <>^\markup\character Jupiter
    r8 fa fa fa sol la |
    sib4 sib8 sib la4\trill la8 mi |
    fa4 fa8 sib sib la la\trill la |
    re4 r8 re16 re sol8 sol16 sol |
    mi4\trill r8 la16 la la4 si8 dod' |
    re'8. re'16 re'8 do' do' si |
    si4\trill si8 sol16 sol do'8 do'16 sol |
    la4 << { \voiceOne la8 \oneVoice } \new Voice { \voiceTwo \sug fa } >> la
    re4\trill re8 << \new Voice { \voiceOne \sug sol } { \voiceTwo mi \oneVoice } >> |
    do4 do8 r
    \ffclef "vbas-dessus" <>^\markup\character Io
    do''4~ |
    do'' sib'8 sib' sib' la' |
    la'4\trill
    \ffclef "vbasse" <>^\markup\character Jupiter
    r8 do'16 do' do'8. do'16 do'8 do'16 si |
    si4\trill
    \ffclef "vbas-dessus" <>^\markup\character Junon
    r8 re''16 re'' si'8 si'16 si' mi''8. mi''16 |
    dod''4 dod'' fa''~ |
    fa''8 mi''8. mi''16 fa'' sol'' dod''8.\trill re''16 |
    re''4
    \ffclef "vbasse" <>^\markup\character Jupiter
    la8 r16 la re'8 re'16 la |
    sib4 sib8 re' sib4\trill sib8 la |
    la4\trill la8 fa16 sol la8 la16 si |
    do'4 la8 la re'4 re'8 mi' |
    dod'4\trill
    \ffclef "vbas-dessus" <>^\markup\character Junon
    r8 mi''16 mi'' la'8\trill la'16 la' |
    re''4 la'8 la' sib'4 sib'8 sib' |
    sol'\trill sol' r16 do'' do'' do'' fa''8 do'' |
    re''8. re''16 sol'8.\trill sol'16 sol'8 do'' |
    la'4\trill
    \ffclef "vbasse" <>^\markup\character Jupiter
    r8 do' fa r16 la fa\trill fa fa fa |
    re8\trill re r re16 re sol8 sol16 sol mi4\trill
    \ffclef "vbas-dessus" <>^\markup\character Junon
    r16 la' la' la' sib'8 sib'16 sib' mi'8\trill mi'16 fa' |
    re'2 r |
    <>^\markup\character Junon
    r2 r8 re''16 re'' re''8. la'16 |
    sib'4 sib'8 sib' sol'\trill sol' do''8. do''16 |
    re''4 sol'8. do''16 la'4\trill r |
    r8 la'16 la' la'8. mi'16 fa'4 fa'8 sib' |
    sol'\trill sol' sol' do'' la'4\trill la'8 re'' |
    si'4\trill si'8 mi'' dod''\trill dod''16 dod'' dod''8.\trill dod''16 |
    re''4 re''8[ do''16] re'' mi''8 mi'' mi'' mi'' |
    fa''4 re''8 si' sold'4 mi''8 sold'\trill |
    la'4 la'8 si' do''4 si'8.\trill la'16 |
    la'8 dod''16 dod'' dod''8. re''16 mi''4 mi''8 mi'' |
    la'8\trill la' sib' sib' sib'4 la'8 la' |
    re''4 re''8 mi'' dod''4\trill mi''8 fa'' |
    re''4 re''8 mi'' fa''4 dod''8.\trill re''16 |
    re''2 r |
    \set Staff.explicitClefVisibility = #end-of-line-invisible
    \clef "vbasse" <>^\markup\character-text Jupiter [Lentement]
    r2 fa4. fa8 |
    re4 re8 re sib,4 r8 sib16 sib |
    sol4\trill sol8 sol mi\trill mi r la16 la |
    dod4\trill r8 la16 la re'4~ |
    re' fa8 sol mi4\trill mi8 fa |
    re4 r8 la la la la sib |
    do'8. do'16 fa8 fa sol la |
    re4\trill sib8 re' sol4 sol8 sol |
    mi4\trill mi8 mi la4 la8 mi |
    fa4 fa8. re'16 si8 si si si |
    sold8.\trill sold16 sold8 si mi8.\trill mi16 |
    la4 la8 la si4 si8 si |
    do'8. do'16 re'8. mi'16 si4.\trill la8 |
    la2 fa4. fa8 |
    re4 re8 re sib,4 r8 sib16 sib |
    sol4\trill sol8 sol mi\trill mi r la16 la |
    dod4 r8 la16 la re'4~ |
    re' fa8 sol mi4\trill mi8 fa |
    re2 r |
    R1 |
    \ffclef "vbas-dessus" <>^\markup\character Junon
    do''4 do'' r8 do'' re'' mi'' |
    si'4\trill si'8 si' si'4\trill si'8 si' |
    mi'4\trill mi'16 mi'' mi'' mi'' do''8 do'' |
    la'4\trill la'8 mi' fa'4 fa'8 mi' |
    mi'8\trill mi'16 mi' la' la' la' si' do''8 do''16 do'' do''8\trill do''16 re'' |
    mi''8 r16 mi'' mi'' mi'' si' si' sold'8\trill sold'16 si' mi'8\trill mi'16 mi' |
    la'4 la'8 r16 mi'' fa''8 si' si' re'' |
    sold'4\trill si'8 do'' la'4\trill la'8 sold' |
    la'1*15/16 \once\override TextScript.self-alignment-X = #RIGHT
    s16-\tag #'recit1 ^\markup\italic {
      La Furie s’enfonce dans les Enfers, & Io se trouve delivrée de ses peine.
    } |
    r2 r4 r8 do'' |
    si'8.\trill si'16 si'8. mi''16 la'8. si'16 |
    sold'2 sold'4 r8 mi' |
    la'4 la' la'4. si'8 |
    do''2 do''4 re'' |
    mi''2 re''4.\trill do''8 |
    do''2. do''4 |
    si'8\trill si' si' si' do''8. do''16 |
    la'2\trill re''4. re''8 |
    re''2 dod''4.\trill re''8 |
    re''2 la'4 si' |
    do''2 do''4 re'' |
    si'2\trill si'4 r8 mi'' |
    sol'8. sol'16 sol'8 fa' fa' mi' |
    mi'2\trill mi''4. mi''8 |
    mi''2 re''4.\trill re''8 |
    re''2 do''4.\trill do''8 |
    do''2 si'4. re''8 |
    sold'2\trill mi''4. mi''8 |
    fa''2 si'4 do'' |
    do''2( si')\trill |
    la' mi''4. mi''8 |
    mi''2 re''4.\trill re''8 |
    re''2 do''4.\trill do''8 |
    do''2 si'4. re''8 |
    sold'2\trill mi''4. mi''8 |
    fa''2 si'4 do'' |
    do''2( si')\trill |
    la'1 |
    R1
    <>^\markup\character Junon
    r4 mi'' r8 mi''16 mi'' do''8 do'' |
    la'4.\trill do''8 dod''8.\trill dod''16 dod''8 re'' |
    re''4 fa'' r8 la'16 la' la'8 si' |
    do''4. do''8 do''\trill do'' do'' si' |
    si'4\trill re''8 re''16 re'' mi''8 mi'' |
    do''4\trill do''8 do'' fa''4 fa''8 fa'' |
    re''4\trill re''8 re'' mi''4 re''8.\trill do''16 |
    do''1 |
  }
  \tag #'recit2 {
    \clef "vbasse"
    R1*2 R2. R1*2 R2.*5 R1*3 R1 R1*2 R1 R1*4 R2.*4 R1 R2.*3 R1*3 R2.
    R1 R2.*2 R1 R2.*2 R1*2 R2.*3 R1 R2. R1 R2. R1 R2.*2 R1 R2. R1*2
    <>^\markup\character Jupiter
    r8 re' re'8. re'16 sib8 sib fad8.\trill fad16 |
    sol8 sol sol sol do' do' la8.\trill la16 |
    sib4 do'16[ sib] do'8 fa fa fa fa |
    dod8 dod dod dod re re re re |
    mi mi mi mi fa4 fad8 fad |
    sol4 sold8\trill[ fad16] sold la4 la8 la |
    sib4 si8. si16 do'8 do do do |
    re re re re mi mi mi mi |
    fa fa fa re mi4 mi16[ re] mi8 |
    la,8. la16 la8. si16 do'8 do' dod' dod' |
    re' re' re' re' do' do' do' re' |
    sib4\trill sib16[ la] sib8 la4\trill la8 fa |
    sib4 sib8 sol la4 la8 la, |
    re2 r |
    R1*3 R2. R1*2 R2. R1 R1*2 R2. R1*5 R2. R1*5 R2. R1 R1*4 R1*2 R2.
    R1*5 R2. R1*5 R2. R1*15 R1
    <>^\markup\character Jupiter
    r4 do' r8 do'16 do' mi8\trill mi |
    fa4. fa8 mi8.\trill mi16 mi8 fa |
    re4 re' r8 re'16 re' re'8 re |
    la4. la8 fad\trill fad fad sol |
    sol4 si8\trill si16 si do'8 do' |
    la4\trill la8 la re'4 re'8 re' |
    si4\trill si8 si do'4 si8.\trill do'16 |
    do'1 |
  }
>>
