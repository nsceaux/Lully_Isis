\clef "basse" re,1\repeatTie |
\once\tieDashed re2~ re4 dod |
re mi2 |
fad sol |
dod re4 re, |
la,2. |
re2 fad,4 |
\once\tieDashed sol,2.~ |
sol, |
do |
la,2 sib, |
la, fad, |
sol, la, |
re,1 |
re2 la,4 mi, |
fa,2 fa |
dod1 | \allowPageTurn
re2 sol, |
do2 re8 sib, do do, |
fa,4 fa mi4. re8 |
dod2 re4 mi8 mi, |
la,2. |
re8. sib,16 mib8 do re re, |
sol,2. |
re2 si,4 |
do2 re |
mi2. |
la,2 re4 |
sol2 la8 la, |
\once\tieDashed re1~ |
re2 dod |
re do |
sib,2. |
la,1 |
fad,2. |
sol,2 mi,4 |
fa,2 sol, |
do2. |
re4 mi2 |
fa2 fad |
sol sold |
la re4 |
sol2 la8 la, |
\tieDashed re2.~ |
re1~ |
re2. | \tieSolid
do2 sib, |
la,2 la4 |
fad2 sol |
do'2 la4 |
sib do' do |
fa2 fa, |
sib, sol,4 |
la, fa, sol, la, |
\once\tieSolid re,1~ | \allowPageTurn
re,8 re' re'8. re'16 sib8 sib fad8. fad16 |
sol4. sol8 do'4 la |
sib do'8 do fa fa fa fa |
dod dod dod dod re4. re8 |
mi8 mi mi mi fa4 fad |
sol sold la4. la8 |
sib4 si do'8 do do do |
re re re re mi mi mi mi |
fa fa fa re mi4 mi, |
la,8. la16 la8. si16 do'8 do' dod' dod' |
re' re' re' re' do' do' do' re' |
sib2 la4. fa8 |
sib4 sib8 sol la4 la, |
\once\tieDashed re1~ |
re1 |
fad,2 sol,~ |
sol, la, |
sol,2 fa,4 |
sib,4. sol,8 la,2 |
re, re |
la,2. |
sib,2 si, |
do dod |
re1 |
mi2. |
do2 sold, |
la, mi4 mi, |
la,2 re |
fad, \once\tieDashed sol,~ |
sol, la, |
sol, fa,4 |
sib,4. sol,8 la,2 |
re, re8 re do si, |
la,1~ |
la,2 \once\tieDashed la~ |
la sold |
la4 la,2 |
\once\tieDashed la,1~ | \allowPageTurn
la,2. fa,4 |
mi,2 mi |
do re |
mi fa8 re mi mi, |
la,1 |
la2. la4 |
la4 sol fa |
mi2. re4 |
do4. re8 do4 si, |
la,2 la4 si |
do' fa sol sol, |
do do8 re mi4 do |
sol sol8 fa mi4 |
\once\tieDashed fa2~ fa4 fa |
mi1 |
re |
fad, |
sol,2 sol |
si,2. |
do do4 |
fa2 fad |
sold la4 la, |
re2. re4 |
re2 do |
re1 |
mi2 mi, |
la,2. la,4 |
fa2 fad |
sold2 la4 la, |
re2. re4 |
re2 do |
re1 |
mi2 mi, |
la,2. si,4 |
do1~ | \allowPageTurn
do |
fa2 mi |
re1 |
la2 fad |
sol2 mi4 |
fa2 re |
sol fa4 sol8 sol, |
do1*15/16~ \hideNotes do16 |
