s1 s2 <2>4 <5-> s8. <6->16 <6 5->2
<5->2 <_-> <5->1 <_+>2.
<_+>2 <6>4 <_->2. <_+> s
<6>2 <7>4 <6> <_+>2 <5-> <_->2 <4>4 <_+> s1
s2. <6>4 s1 <5-> <_+>2
<_-> s2 s8 <6 5> <4> \new FiguredBass <3> s2 <6+>

<5->2. <4>8 <_+> <_+>2. <_+>8. <6>16 s8 <6 5> <4> \new FiguredBass <_+>
<_->2 s8 <6 5> s2. s4 <6> <7> <6>
<_+>2. <_+> <7 _->4 <6 5> <_+> s1
<6->2 <5-> s4 <6-> <7> <6>
<7>2 <6>4 <_+>1 <6>2 <5->4
s2 <6>4 <6 5>2 <4>4 <3> s2. <7>4 <5->2

s1 <_!>2 <5/> <_+>2. <7 _->4 <6 5>
<_+> s2. <6- 4>1 s2.
<6>2 <7>4 <6> <_+>2. <6>2 <_-> s2
<6>4 <6 5> <4> <3> s1 s2
<6 5 _->4 <_+> <6> <6 5 _-> <_+> s1
s2. <6>4 <_->2. <6>4 <6 5> <4>8 <3> s2

<6>2 s4. <6->8 <5/>2. <5/>4 s <5/> <_+>2
<7>4 <5/>2 <6>4 <7>4 <6> <_+>2 <7>4 <6>
<4> <_+> <_+>2 <6>4 <5/> s <6-> <7-> <6>
<7>4 <6> <_+>4. <6>8 s4. <6>8 <4>4 <_+> s1

s1 <6>2 <_-> <6 5> <_+> <4+> <6>4
s4. <6 5>8 <_+>2 s1 <6>2. s2
<6> s <5/> s <6 5> <_+>2.
<6>2 <5/> s <4>4 <_+> <_+>1 <6>2 <_->
<6 5> <_+> <4+> <6>4 s4. <6 5>8 <4>4 <_+> s1*2
s1 <2>2 <5/> s2. s2 <6 4>

s2. <7>8 <6> <_+>1
<6>2 <7>4 <6> <_+>2 <"">8\figExtOn <"">\figExtOff <4> <_+> s1*2
<2>4 <6> <7>8 <6> <_+>2.\figExtOn <_+>4 <6>2 <6>4\figExtOff <6+> s2. <6>4
<"">4\figExtOn <"">\figExtOff <4> <3> s1 s2 <6>4 s2 <6> <7> <6+> s1
<5/>1 s <6>2 <5/>4 s1 <7>2 <6> <5/>1
<7>2. <6 5>4 <4+>2 <6> <7> <6 4+>4 <7> <4>2 <_+> s1 <7>2 <6>

<5/>1 <7>2 <6 5> <4+> <6> <7> <6 4+>4 <7> <4>2 <_+> <"">2.\figExtOn <"">4\figExtOff s1
s1 s2 <6+> s2.. <6>8 s2 <5/>
s2 <6>4 s1 s2 <7>4 <4>8 <3>
