\clef "dessus" R1*2 R2. R1 R2.*61 |
r4 r fa'' |
sib' sib' re'' |
sol'2 la'4 |
sib'2 sib'8 sib' |
do''2 re''4 |
do'' do'' re''8 re'' |
mib''2 re''4 |
do''2\trill~ do''8 sib' |
sib'2 re''8 re'' |
do''4.\trill do''8 do'' re'' |
mib''2 mib''8 mib'' |
re''2 sib'8 sib' |
la'4.\trill la'8 la' la' |
sib'4. sib'8 sib' do'' |
re''4 re'' do''8 sib' |
la'2~\trill la'8 sol' |
sol'2 re''8 re'' |
mib''4. mib''8 mib'' mib'' |
do''4.\trill fa''8 fa'' fa'' |
sib'4 sib' mib'' |
do''2\trill~ do''8 sib' |
sib'2 fa''4 |
sib' sib' re'' |
sol'2 la'4 |
sib'2 sib'8 sib' |
do''2 re''4 |
do''\trill do'' re''8 re'' |
mib''2 re''4 |
<< { re''( do''2)\trill } \\ \sugNotes { do''2\trill~ do''8 sib' } >> |
sib'2. |
