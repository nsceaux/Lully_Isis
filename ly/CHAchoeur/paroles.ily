\tag #'(voix1 basse) {
  Ser -- vez, Nym -- phe, ser -- vez a -- vec un soin fi -- de -- le,
  la puis -- san -- te Rei -- ne des Cieux.
  Sui -- vez dans ces ai -- ma -- bles lieux,
  la Jeu -- nesse im -- mor -- tel -- le ;
  Sui -- vez, sui -- vez dans ces ai -- ma -- bles lieux,
  la Jeu -- nesse im -- mor -- tel -- le :
  Tout plaît, & tout rit, & tout rit a -- vec el -- le.
  Sui -- vez, sui -- vez dans ces ai -- ma -- bles lieux,
  la Jeu -- nes -- se, la Jeu -- nesse im -- mor -- tel -- le ;
  Tout plaît & tout rit, & tout rit a -- vec el -- le,
  & tout rit __ a -- vec el -- le.
  Tout plaît & tout rit a -- vec el -- le,
  & tout rit __ a -- vec el -- le.

  Que c’est un plai -- sir char -- mant
  d’ê -- tre jeune & bel -- le !
  Tri -- om -- phez à tout mo -- ment,
  d’u -- ne con -- quê -- te nou -- vel -- le :
  Tri -- om -- phez, tri -- om -- phez à tout mo -- ment
  d’u -- ne con -- quê -- te nou -- vel -- le :
  Que c’est un plai -- sir char -- mant
  d’ê -- tre jeune & bel -- le !
}
\tag #'voix2 {
  Tout plaît, & tout rit, & tout rit a -- vec el -- le.
  Sui -- vez, sui -- vez dans ces ai -- ma -- bles lieux,
  la Jeu -- nesse im -- mor -- tel -- le ;
  Tout plaît & tout rit, & tout rit a -- vec el -- le,
  & tout rit __ a -- vec el -- le.
  Tout plaît & tout rit a -- vec el -- le,
  & tout rit __ a -- vec el -- le.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Que c’est un plai -- sir char -- mant
  d’ê -- tre jeune & bel -- le !
  d’ê -- tre jeune & bel -- le !
  Tri -- om -- phons à tout mo -- ment :
  Tri -- om -- phons, tri -- om -- phons à tout mo -- ment :
  d’u -- ne con -- quê -- te nou -- vel -- le :
  Tri -- om -- phons à tout mo -- ment
  d’u -- ne con -- quê -- te nou -- vel -- le :
  Que c’est un plai -- sir char -- mant
  d’ê -- tre jeune & bel -- le !
  d’ê -- tre jeune & bel -- le !
}
