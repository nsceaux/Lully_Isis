\clef "haute-contre" R1*2 R2. R1 R2.*61 |
r4 r fa' |
fa' fa' fa' |
mib'2 mib'4 |
re'2 sol'8 sol' |
fa'2 sib'4 |
la' la' sib'8 sib' |
la'2 sib'4 |
sib'( la'4.)\trill sib'8 |
sib'2 sib'8 sib' |
la'4. la'8 la' si' |
do''2 do''8 do'' |
sib'2 sol'8 sol' |
fad'4. fad'8 fad' fad' |
sol'4. sol'8 sol' fad' |
sol'4 sol' sol' |
sol' fad'4.\trill mi'16 fad' |
sol'2 si'8 si' |
do''4. do''8 do'' do'' |
la'4. la'8 la' la' |
sol'4 sol' sol' |
fa'2~ fa'8 mib' |
re'2 fa'4 |
fa' fa' fa' |
mib'2 mib'4 |
re'2 sol'8 sol' |
fa'2 sib'4 |
la' la' sib'8 sib' |
la'2 sib'4 |
sib' la'4.\trill sib'8 |
sib'2. |
