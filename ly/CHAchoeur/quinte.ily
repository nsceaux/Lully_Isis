\clef "quinte" R1*2 R2. R1 R2.*61 |
r4 r sib |
sib fa fa |
sol2 do'4 |
sib2 sib8 sib |
la2 re'4 |
fa' fa' fa'8 fa' |
mib'2 fa'4 |
fa'2. |
fa'2 sib8 sib |
do'4. fa'8 fa' fa' |
mib'2 do'8 do' |
re'2 re'8 re' |
re'4. re'8 re' re' |
re'4. re'8 re' do' |
sib4 sib sol |
sol re'2 |
re' re'8 re' |
do'4. do'8 do' do' |
do'4. re'8 re' re' |
re'4 re' do' |
do' fa'4. fa'8 |
fa'2 sib4 |
sib fa fa |
sol2 do'4 |
sib2 sib8 sib |
la2 re'4 |
fa' fa' fa'8 fa' |
mib'2 fa'4 |
fa'2. |
fa' |
