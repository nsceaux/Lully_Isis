\clef "taille" R1*2 R2. R1 R2.*61 |
r4 r re' |
re' re' sib |
sib2 do'4 |
\once\tieDashed re'2~ re'8 mib' |
do'2 sib4 |
do' do' sib8 sib |
do'2 re'8 mib' |
fa'4. mib'8 re' mib' |
re'2\trill fa'8 fa' |
fa'4. fa'8 fa' fa' |
sol'2 sol'8 sol' |
sol'2 re'8 re' |
re'4. re'8 la' la' |
sol'4. re'8 re' re' |
re'4 re' mib' |
re'2 re'8 do' |
si2\trill sol'8 sol' |
sol'4. sol'8 sol' sol' |
fa'4. fa'8 fa' fa' |
re'4 sib sib |
sib la4. sib8 |
sib2 re'4 |
re' re' sib |
sib2 do'4 |
\once\tieDashed re'2~ re'8 mib' |
do'2\trill sib4 |
do' do' sib8 sib |
do'2 re'8 mib' |
fa'4. mib'8 re' mib' |
re'2.\trill |
