\clef "basse"
<<
  \tag #'basse { R1*2 R2. R1 R2.*61 r4 r }
  \tag #'(basse-continue bc-part) {
    \tag #'bc-part <>^"[B.C.]"
    sib,1~ |
    sib, |
    mib4 mi2 |
    fa4. mib8 re do sib,16 la, sol,8 |
    fa,2. |
    fa |
    re4 mib4. do8 |
    re4 re'4. do'8 |
    sib4 la sol |
    do re re, |
    sol,4. sol8 fa mib |
    re2 sib,4 |
    do re2 |
    mib2. |
    fa2 fa,4 |
    sib,2. |
    mib2 do4 |
    fa2 re4 |
    sol2 do4 |
    fa, sol,2 |
    do4. do8 mib do |
    sol2 sol4 |
    re2. |
    mib4 do2 |
    sol4. la8 sib sol |
    do'2 fa4 |
    sib, do2 |
    fa,4 fa8 sol fa mib |
    re4 sib,8 do re sib, |
    mib fa sol fa mib re |
    do2 do8 do |
    fa mib fa sol fa mib |
    re2 re4 |
    mib8 re do4 sib, |
    fa4 fa,2 |
    sib,2 sib,4 |
    mib8 fa sol fa mib re |
    do2 do8 do |
    fa mib fa sol fa mib |
    re2 re4 |
    mib8 re do4 sib, |
    fa4 fa,2 |
    sib,2 sib8 la |
    sol4 sol la |
    sib2 mib4 |
    fa2 fa8 mib |
    re4 sol mib |
    fa4 fa,2 |
    sib,2 sib4 |
    la2. |
    sol |
    fa2 re4 |
    sol2 do4 |
    fa, sol,2 |
    do4. re8 mib do |
    sol2 sol8 la |
    sib2 sol4 |
    do'4. do'8 la sib |
    sol4 do' do |
    fa2 sib8 la |
    \sugRythme { si'4. si'8 } sol4 sol la |
    sib2 mib4 |
    fa2 fa8 mib |
    re4 sol mib |
    fa fa,2 |
    sib,
  }
>> \tag #'bc-part <>^"[Tous]" sib4 |
re re re |
mib2 do4 |
sol2 sol8 sol |
la2 sib4 |
fa2 re8 re |
do2 sib,4 |
fa fa,2 |
sib,2 sib,8 sib, |
fa4. fa8 mib re |
do2 do8 do |
sol2 sol8 sol |
re'4. re'8 re' do' |
sib4. sib8 sib la |
sol4 sol do |
re2. |
sol,2 sol8 sol |
do'4. do'8 do' do |
fa4. re8 re re |
sol4 sol mib |
fa fa,2 |
sib, sib4 |
re re re |
mib2 do4 |
sol2 sol8 sol |
la2 sib4 |
fa fa re8 re |
do2 sib,4 |
fa fa,2 |
sib,2. |

