<<
  \tag #'(voix1 basse) {
    \clef "vhaute-contre"
    <>^\markup\character-text Mercure \line { \hspace#2 Mercure & Iris conduisant Io }
    R1 |
    r4 r8 fa' re'4\trill sib8 sib16 re' |
    sol8.\trill sol16 do'8 do' do' do' |
    la\trill la r fa16 fa sib8 do' re'\trill re'16 mi' |
    fa'2
    \ffclef "vbas-dessus" <>^\markup\character Iris
    r8 do'' |
    la'4.\trill la'8 sib'4 |
    sib' la'( sol'8\trill)[ fad'16] sol' |
    fad'2\trill re''8 la' |
    sib'4 do'' re'' |
    sib'4( la'2)\trill |
    sol'4. re''8 re'' mib'' |
    fa''4. sib'8 sib'4 |
    lab' lab'( sol'8) lab' |
    sol'2\trill sib'8 mib'' |
    do''2\trill re''8 mib'' |
    re''4\trill sib' sib' |
    sol'4.\trill sol'8 do''4 |
    la'4.\trill la'8 re''4 |
    si'4\trill si' do'' |
    do''2( si'4) |
    do''4. do''8 sol' la' |
    sib'4. sib'8 sib'4 |
    lab'4 lab'( sol'8) lab' |
    sol'2\trill sol'8 la' |
    sib'4 sib' sib'8 sib' |
    sol'4 sol' la' |
    la'( sol'2)\trill |
    fa'2 la'4 |
    sib'2 sib'8 sib' |
    sol'2\trill sol'8 sol' |
    do''[ re''] mib''[ re''] do''[ sib'] |
    la'4\trill la' la'8 la' |
    re''8[ mib'' re'' do'' sib' la']( |
    sol'4) la' sib' |
    \tag #'voix1 \sugRythme { si'2 si'4 } sib'4( la'2) |
    sib'2 sib'4 |
    sol'2\trill sol'8 sol' |
    do''[ re''] mib''[ re''] do''[ sib'] |
    la'4\trill la' la'8 la' |
    re''8[ mib'' re'' do'' sib' la']( |
    sol'4) la' sib' |
    sib'2( la'4) |
    sib'2
    \ffclef "vbas-dessus"
    <>^\markup\character-text Hebé \line { \hspace#2 Hebé & les Nymphes reçoivent Io }
    fa'4 | \noBreak
    sib' sib' do'' |
    re''2 mib''4 |
    do''2\trill la'4~ |
    la'8 la' sib'4. do''8 |
    re''4( do''2)\trill |
    sib'2 re''8 mib'' |
    fa''2 do''4 |
    do''( sib'\trill la'8) sib' |
    la'4.\trill la'8 la' re'' |
    si'4\trill si' do'' |
    do''8[ re''16 mib'']( re''2)\trill |
    do''2 sol'8 la' |
    sib'2 sib'8 do'' |
    re''4. sib'8 sib' la' |
    sol'4.\trill do''8 do'' re'' |
    sib'4\trill sol'4. do''8 |
    la'4\trill fa' r8 fa' |
    \tag #'voix1 \sugRythme { si'4. si'8 } sib'4 sib' do'' |
    re''2 mib''4 |
    do''2\trill la'4~ |
    la'8 la' sib'4. do''8 |
    re''4( do''2\trill) |
    sib'2
  }
  \tag #'voix2 {
    \clef "vhaute-contre" R1*2 R2. R1 R2.*11 |
    r4 r <>^\markup\character Mercure re'4 |
    sib4. sib8 mib'4 |
    do'4. do'8 fa'4 |
    re' re' mib' |
    mib'( re'2)\trill |
    do'2 r4 |
    r r8 re' re' mib' |
    fa'4. fa'8 fa'4 |
    sib mib'( re'8) mib' |
    re'2\trill re'8 re' |
    mi'4 mi' fa' |
    fa'2( mi'4) |
    fa'2 do'4 |
    re'2 re'8 re' |
    sib2 sib8 sib |
    mib'[ fa'] sol'[ fa'] mib'[ re'] |
    do'4\trill do' do'8 do' |
    fa'[ sol' fa' mib' re' do']( |
    sib4) do' re' |
    re'( do'2\trill) |
    sib2 re'4 |
    sib2 sib8 sib |
    mib'[ fa'] sol'[ fa'] mib'[ re'] |
    do'4\trill do' do'8 do' |
    fa'[ sol' fa' mib' re' do']( |
    sib4) do' re' |
    re'( do'2\trill) |
    sib2 r4 |
    R2.*52
  }
  \tag #'vdessus \clef "vdessus"
  \tag #'vhaute-contre \clef "vhaute-contre"
  \tag #'vtaille \clef "vtaille"
  \tag #'vbasse \clef "vbasse"
  \tag #'(vdessus vhaute-contre vtaille vbasse) { R1*2 R2. R1 R2.*61 r4 r }
>>
<<
  \tag #'vdessus {
    <>^\markup\character Chœur des Nymphes fa''4 | \noBreak
    sib' sib' re'' |
    sol'2 la'4 |
    sib'2 sib'8 sib' |
    do''2 re''4 |
    do''\trill do'' re''8 re'' |
    mib''2\trill re''4 |
    re''( do''2)\trill |
    sib'2 re''8 re'' |
    do''4.\trill do''8 do'' re'' |
    mib''2 mib''8 mib'' |
    re''2 sib'8 sib' |
    la'4.\trill la'8 la' la' |
    sib'4. sib'8 sib' do'' |
    re''4 re'' do''8[ sib'] |
    sib'4( la'2)\trill |
    sol'2 re''8 re'' mib''4. mib''8 mib'' mib'' |
    do''4.\trill fa''8 fa'' fa'' |
    sib'4 sib' mib'' |
    re''( do''2)\trill |
    sib'2 fa''4 |
    sib' sib' re'' |
    sol'2 la'4 |
    sib'2 sib'8 sib' |
    do''2 re''4 |
    do''\trill do'' re''8 re'' |
    mib''2 re''4 |
    re''( do''2)\trill |
    sib'2. |
  }
  \tag #'vhaute-contre {
    re'4 |
    fa' fa' fa' |
    mib'2 mib'4 |
    re'2 sol'8 sol' |
    fa'2 fa'4 |
    fa' fa' fa'8 << { \voiceOne fa' \oneVoice } \new Voice { \voiceTwo \sug sib } >> |
    do'2 re'8[ mib'] |
    fa'4.\melisma mib'8[ re' mib']\melismaEnd |
    re'2\trill fa'8 fa' |
    fa'4. fa'8 fa' fa' |
    sol'2 sol'8 sol' |
    sol'2 sol'8 sol' |
    fad'4.\trill fad'8 fad' fad' |
    sol'4. sol'8 sol' fad' |
    sol'4 sol' sol'4 |
    sol'( fad'2)\trill |
    sol'2 sol'8 sol' |
    sol'4. sol'8 sol' sol' |
    fa'4. fa'8 re' re' |
    re'4 re' sol' |
    fa'4.( mib'8[ re' mib']) |
    re'2\trill re'4 |
    fa' fa' fa' |
    mib'2 mib'4 |
    re'2 sol'8 sol' |
    fa'2 fa'4 |
    fa' fa' fa'8 sib |
    do'2 re'8[ mib'] |
    fa'4.( mib'8)[ re' mib'] |
    re'2.\trill |
  }
  \tag #'vtaille {
    sib4 |
    sib sib sib |
    sib2 do'4 |
    sib2 re'8 mib' |
    do'2 sib4 |
    la la sib8 sib |
    la2 sib4 |
    sib( la2) |
    sib2 sib8 sib |
    la4. la8 la si |
    do'2 do'8 do' |
    sib2 re'8 re' |
    re'4. re'8 re' re' |
    re'4. re'8 re' do' |
    sib4 sib mib'4 |
    re'2. |
    si2\trill si8 si |
    do'4. do'8 do' do' |
    la4. la8 la la |
    sol[ la] sib4 sib |
    sib( la2\trill) |
    sib2 sib4 |
    sib sib sib |
    sib2 do'4 |
    sib2 re'8 mib' |
    do'2 sib4 |
    la la sib8 sib |
    la2 sib4 |
    sib( la2\trill) |
    sib2. |
  }
  \tag #'vbasse {
    sib4 |
    re re re |
    mib2 do4 |
    sol2 sol8 sol |
    la2 sib4 |
    fa fa re8 re |
    do2\trill sib,4 |
    fa( fa,2) |
    sib,2 sib,8 sib, |
    fa4. fa8 mib\trill re |
    do2 do8 do |
    sol2 sol8 sol |
    re'4. re'8 re' do' |
    sib4. sib8 sib la |
    sol4 sol do4 |
    re2. |
    sol,2 sol8 sol |
    do'4. do'8 do' do |
    fa4. re8 re re |
    sol4 sol mib |
    fa4( fa,2) |
    sib,2 sib4 |
    re re re |
    mib2 do4 |
    sol2 sol8 sol |
    la2 sib4 |
    fa fa re8 re |
    do2\trill sib,4 |
    fa( fa,2) |
    sib,2. |
  }
  \tag #'(voix1 basse) {
    \tag #'basse <>^\markup\character Chœur r4 R2.*29
  }
>>
