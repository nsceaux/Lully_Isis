\clef "taille" sol'2 sol'4. sol'8 |
sol'2 fa'4. fa'8 |
mi'2 la'2 |
la' sol'4. sol'8 |
sol'2 fa'4. fa'8 |
fa'4 re' re'4. re'8 |
re'2 do'4. do'8 |
do'4 fa' fa'4. fa'8 |
<< { \sug mib'4 \sug sol' } \\ mib'2 >> sol'4. sol'8 |
fa'4. mib'16 re' do'4. re'8 |
sib2. r8 re' |
sol'4. sol'8 fad'4. fad'8 |
sol' la' sib'4 la'4. la'8 |
la'4. sib'8 << { \sug sol'4. \sug sol'8 } \\ sol'2 >> |
fad'1 |
fad' |
R2.*5 |
r4 sol' sol' |
la' la' la' |
sib' fa' fa' |
mib' mib' mib' |
re' la' la' |
sol'4. fa'8 mi'4 |
re'2 re'4 |
re' re' sib' |
la'4. sol'8 fa' mib' |
re'4 sol'4. sol'8 |
sol'4 sol' sol' |
fa'2 fa'4 |
mib' sol'4. sol'8 |
fa'8 sol' la'4 sib'~ |
sib' sol' sol' |
fa'2 fa'8 mib' |
re'4 re'8 do' sib4 |
do' do' do' |
re' re' re' |
mi' sol'4. sol'8 |
fa'4 la'4. la'8 |
sol'2 sol'4 |
fa'2 fa'4 |
fa' mi'4. fa'8 |
fa'4 re'4. re'8 |
re'4 do'4. do'8 |
do'4 do' fa' |
fa' mi'2 |
mi'4. re'8 dod'4 |
re'2 re'4 |
re' dod'4. re'8 |
re'4 re'4. re'8 |
do'4 mi'4. mi'8 |
re'4 la' la' |
sol'2 sol'4 |
fa'8 mib' re'4. re'8 |
sol4 sol' sol' |
sol' fa'4. fa'8 |
fa'4 sol'4. sol'8 |
sol'4 mi' la' |
la' la'4. la'8 |
sol'4 sol'4. sol'8 |
fa'4 la'4. la'8 |
sol'4 sol' fad' |
sol'8 fa' mib'4 mib' |
re'2 re'4 |
sib do'4. do'8 |
do'4 la re'8 do' |
sib2 sol'4 |
sol' fa'4. fa'8 |
fa'4 fa'2 |
mib'2 mib'4 |
re'4 re' re' |
mib' re'4. do'8 |
si2. |
