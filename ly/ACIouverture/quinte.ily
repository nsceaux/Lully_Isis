\clef "quinte" re'4. re'8 sib4 sol |
la re' re'4. re'8 |
mi'2 mi'4. mi'8 |
re'4. re'8 re'2 |
do'4. do'8 do'2 |
sib4. sib8 la4. la8 |
sol2 sol4. sol8 |
<<
  \new CueVoice {
    s4^\markup\note-by-number #2 #1 #UP s^\markup\note-by-number #3 #0 #UP
  }
  { la4 la re' sib | }
>>
sib mib'2 do'4 |
do' fa'2 fa'4 |
fa'2. r8 re' |
re'4 sol la re' |
sol4. mib'8 mib'4. mib'8 |
re'2 re'4. re'8 |
re'1 |
re' |
R2.*7 |
r4 sib sib |
do' do' do' |
re'2 re'4 |
sol sol do' |
la la la |
sol sib8 do' re'4 |
do'2 do'4 |
sib re' re' |
do' do' do' |
re' re' re' |
mib' mib' mib' |
fa' fa' re' |
sol' sol' mib' |
fa'2 fa'4 |
sib2 sib4 |
la8 sib do'2 |
sib4 sib sib |
do' do' do' |
re' re' re' |
mi' mi' mi' |
fa' fa' sib |
do'2 do'4 |
fa fa sib |
sol2 sol4 |
fa la4. la8 |
sol la sib2 |
la la4 |
re' sib re'~ |
re' la2 |
re'2 re'4 |
mi'2 mi'4 |
la2 re'4 |
sol do'2 |
la4 re'2 |
re'4 re' sib |
do' do' do' |
re' re' re' |
do'8 re' mi'4. mi'8 |
re'4 re' re' |
mib' mi' mi' |
fa' fad' fad' |
sol' sol' re' |
mib' mib' do' |
re'2 re'4 |
sol2 sol4 |
fa la4. la8 |
sol4 sib sib |
la4. sib8 do'4 |
sib2 sib4 |
sol la4. la8 |
la4. la8 sib4 |
sol re'4. re'8 |
re'2. |
