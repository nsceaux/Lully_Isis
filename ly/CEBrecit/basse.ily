\clef "basse" re1 |
dod2 \once\tieDashed re~ |
re mi4 mi, |
la,1 |
mi |
si,4~ si,8 la, sol,4 sold, |
la,2 mi4 mi, |
la,2 la |
dod \once\tieDashed re~ |
re2 dod4 |
\once\tieDashed si,2~ si,8 la, sol,4 |
la,4 \once\tieDashed la~ la8 sol |
fad4 mi8 re la,4 |
re si, fad4. mi8 |
re2 dod4 |
si,4 mi mi, |
la, la8. fad16 sol4 mi |
fad4 re8 dod16 si, fad8 fad, |
si,4 si la |
sol fad2 |
mi4. re8 dod4. si,8 |
la,2. |
<<
  { \voiceTwo re2. | si,4 \oneVoice }
  \new Voice {
    \voiceOne \hideNotes \once\tieDashed re2.~ | \unHideNotes \sug re4
  }
>> fad,2 |
sol,4 sol sold |
la8. fad16 mi8 re la,4 |
re4. dod8 \once\tieDashed si,2~ |
si, mi4 |
\once\tieDashed la,2.~ |
la, | \allowPageTurn
re,2 re4^\markup\italic gay |
dod2. |
si, |
la,2 la4~ |
la8 sol fad4 mi |
re2. |
mi4. fad8 mi re |
dod2 re4 |
mi mi,2 |
la,4 la8 si la sol |
fad4 sol8 fad mi re |
la,4. mi8 fad \footnoteHere #'(0 . 0) \markup {
  Matériel 1677 : \italic sol♯
} sol |
la4. la8 sol fad |
sol4. sol8 fad mi |
fad2 si,4 |
si4. si8 la sol |
fad4. mi8 re4 |
sol4. fad8 mi4 |
la4 la, si, |
dod2 re4 |
si, fad,2 |
sol, sol4 |
sold2. |
la2 si8 sol |
la4 la,2 |
re4. mi8 fad sol |
re2~ re8 re do si, |
\once\set Staff.whichBar = "|"
\custosNote la,
