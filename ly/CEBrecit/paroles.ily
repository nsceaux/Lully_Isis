J’ay cher -- ché vai -- ne -- ment la fil -- le d’I -- na -- chus.

Ah ! je n’ay pas be -- soin d’en sça -- voir da -- van -- ta -- ge,
non, I -- ris, ne la cher -- chons plus.
Ju -- pi -- ter, dans ces lieux, m’a don -- né de l’om -- bra -- ge,
j’ay tra -- ver -- sé les airs, j’ay per -- cé le nu -- a -- ge
qu’il op -- po -- soit à mes re -- gards :
mais en vain j’ay tour -- né les yeux de tou -- tes parts.
Ce Dieu par son pou -- voir su -- prê -- me,
m’a ca -- ché la nym -- phe qu’il ai -- me,
et ne m’a lais -- sé voir que des trou -- peaux é -- pars :
Non, non, je ne suis point une in -- cré -- dule e -- pou -- se
qu’on puis -- se trom -- per ai -- sé -- ment,
voy -- ons qui fein -- dra mieux de Ju -- pi -- ter a -- mant,
ou de Ju -- non ja -- lou -- se.
Il est Maî -- tre des Cieux, la Ter -- re suit sa Loy ;
sous sa tou -- te- puis -- sance, il faut que tout flé -- chis -- se ;
mais, puis -- qu’il ne pre -- tend s’ar -- mer que d’ar -- ti -- fi -- ce,
tout Ju -- pi -- ter qu’il est, il est moins fort que moy.
Dans ces lieux é -- car -- tez, voy que la Terre est bel -- le !

Elle ho -- no -- re son Maître, & bril -- le sous ses pas.

L’A -- mour, cet a -- mour in -- fi -- del -- le,
qui du plus haut des cieux l’ap -- pel -- le,
fait que tout luy rit i -- cy- bas :
L’A -  bas :
Près d’u -- ne maî -- tres -- se nou -- vel -- le
dans le fond des de -- serts on trou -- ve des ap -- pas,
et le Ciel mê -- me ne plaît pas
a -- vec une E -- pouse im -- mor -- tel -- le.
- le.
