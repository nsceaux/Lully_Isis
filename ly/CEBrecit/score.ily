\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2 s2 \bar "" \break s2 s1*2\break s1 s2 \bar "" \break s2 s1*2\pageBreak
        s2. s1 s2 \bar "" \break s4 s2. s1 s2 \bar "" \break s4 s2.*2 \bar "" \break
        s4 s2.*3\break s1 s2.*2\break s2.*3 s2 \bar "" \pageBreak
        s2 s2.*2\break s2.*5 s4 \bar "" \break s2 s2.*7\break
        s2.*7\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
