\key re \major
\time 4/4 \midiTempo#80 s1*9
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.*2
\time 4/4 s1
\digitTime\time 3/4 s2.*2
\time 4/4 s1
\digitTime\time 3/4 s2.*3
\digitTime\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2.*5
\time 4/4 s1
\digitTime\time 3/4 s2.*4
\bar ".!:" \midiTempo#160 s2.*8 \alternatives s2.*2 s2.
\bar ".!:" s2.*13 \alternatives s2. { \time 4/4 s1 }
\bar "|."
