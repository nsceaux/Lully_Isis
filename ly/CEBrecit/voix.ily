\ffclef "vbas-dessus" <>^\markup\character Iris
r2 r8 re''16 re'' fad'8\trill fad'16 sol' |
la'8 mi' sol'16 sol' sol' fad' fad'4\trill
\ffclef "vbas-dessus" <>^\markup\character Junon
re''4 |
r8 re''16 re'' re''8 re'' si'\trill si'16 si' sold'8\trill sold'16 la' |
la'8 la' mi'' r16 dod'' la'8 la'16 la' mi'8 fad' |
sol'8 sol'16 si' sol'8 sol'16 mi' si'8 si'16 si' sol'8 sol'16 fad' |
fad' fad' r fad' fad' fad' sol' la' si'8 si'16 si' mi''8 mi''16 mi'' |
dod''8\trill dod'' r16 dod'' re'' mi'' si'4\trill si'8 si'16 dod'' |
la'4\trill r8 dod''16 mi'' la'8 la'16 la' mi'8 fad' |
sol'8 sol' sol' fad' fad'4.\trill re'8 |
la'8 la' la' la' si' dod'' |
re''4 re''8 si'16 si' fad'8\trill fad' sol' sol'16 sol' |
mi'8\trill mi' r16 mi' mi' mi' fad'8 sol' |
la'8 si' dod'' re'' dod''8.\trill re''16 |
re''4 r8 fad'' lad'4\trill r16 lad' lad' lad' |
si'8 si' si' si' si' dod'' |
re''8 re''16 fad' sol'8 sol'16 si' sol'8\trill sol'16 sol' |
mi'8.\trill mi''16 dod''\trill dod'' dod'' re'' si'\trill si' si' si' dod''8 dod'' |
lad'4\trill re''8 mi''16 fad'' dod''8.\trill re''16 |
si'8\trill si' r re''16 re'' red''8\trill red''16 mi'' |
mi''8. mi''16 la'8 la' la'8. sold'16 |
sold'4 mi'8 mi' la'4 la'8 si' |
dod''8\trill mi'' la' sol' sol' fad' |
fad'\trill fad' la'4 la'16 la' la' la' |
re''8 la' do'' do'' do'' si' |
si'\trill si' r16 si' si' si' mi''8. mi''16 |
dod''8\trill r16 re'' mi''8 fad'' mi''8.\trill re''16 |
re''2 r8 fad'16 fad' si'8 si'16 re'' |
si'4\trill re''8 re''16 re'' sold'8. la'16 |
la'8. la'16
\ffclef "vbas-dessus" <>^\markup\character Iris
r8 mi'16 mi' la'8 la'16 si' |
dod''8. mi''16 la'8. sol'16 sol'8. fad'16 |
fad'2\trill
\ffclef "vbas-dessus" <>^\markup\character Junon
r8 re' |
la'2 la'8 la' |
si'4 dod'' re'' |
dod''2\trill dod''8 la' |
la' la' re''4 mi'' |
fad''2 re''4 |
si'2\trill si'4 |
mi''4. mi''8 re'' dod'' |
si'2\trill si'8 dod'' |
la'2. |
r4 r r8 re' |
la'2. |
dod''4.\trill dod''8 dod'' re'' |
si'2\trill si'8 dod'' |
lad'4\trill lad' si'8 si' |
fad'4\trill fad'4. sol'8 |
la'2 la'4 |
si'4. la'8 sol' fad' |
mi'2.\trill |
mi''4. mi''8 fad''4 |
re''4. do''8 do''16[ si'] do''8 |
si'2\trill r8 si' |
si'2 dod''8 re'' |
dod''4 dod'' re'' |
re''8.[ mi''16]( mi''2\trill) |
re''2. |
re''2 r |
