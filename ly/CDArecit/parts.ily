\piecePartSpecs
#`((basse-continue #:score-template "score-voix")
   (silence #:music , #{

 s2.*3\allowPageTurn
 s1*3\allowPageTurn
 s2.*3\allowPageTurn
 s1*2\allowPageTurn
 s2.*2\allowPageTurn
 s1\allowPageTurn
 s2.\allowPageTurn
 s1*6\allowPageTurn
 s2.\allowPageTurn
 s1*6\allowPageTurn
 \ru#49 { s2.\allowPageTurn }
 s1*3\allowPageTurn
 \ru#17 { s2.\allowPageTurn }
 s1*3\allowPageTurn
 s2.*2\allowPageTurn
 s1\allowPageTurn
 s2.*2\allowPageTurn
 s1\allowPageTurn
 s2.\allowPageTurn
 s1*6\allowPageTurn
 s2.\allowPageTurn
 s1*2\allowPageTurn
 \ru#36 { s4.\allowPageTurn }
 s1\allowPageTurn

#}))
