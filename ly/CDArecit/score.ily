\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'recit1 \includeNotes "voix"
    >> \keepWithTag #'recit1 \includeLyrics "paroles"
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'recit2 \includeNotes "voix"
    >> \keepWithTag #'recit2 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*3 s1*2\pageBreak
        s1 s2.*2\break s2. s1*2\break s2.*2 s1\break
        s2. s1*2\break s1*3 s2 \bar "" \break s2 s2. s1 s2 \bar "" \pageBreak
        s2 s1*2\break s1*2 s2. \break s2.*8\break
        s2.*6\break s2.*7\break s2.*6\pageBreak
        s2.*5\break s2.*6\break s2.*6\break s2.*4 s1\pageBreak
        s1*2 s2.*2 s2 \bar "" \break s4 s2.*7 s2 \bar "" \break
        s4 s2.*6 s1\break s1*2 s2.\break s2. s1 s2.\pageBreak
        s2. s1 s2.\break s1*3 s2 \bar "" \break s2 s1*2 s4 \bar "" \break
        s2 s1*2\break s4.*7\pageBreak
        s4.*8\break s4.*6\break s4.*8\break
      }
      \modVersion {
        s2.*3 s1*3 s2.*3 s1*2 s2.*2 s1 s2. s1*6 s2. s1*6 s2.*49 s1\break
        s1*2 s2.*2\break
        s2.*15 s1\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
