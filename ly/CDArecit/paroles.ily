\tag #'(recit1 basse) {
  Ar -- rê -- tez, belle I -- ris, dif -- fé -- rez un mo -- ment
  d’ac -- com -- plir en ces lieux ce que Ju -- non de -- si -- re.

  Vous m’ar -- re -- te -- rez vai -- ne -- ment,
  et vous n’au -- rez rien à me di -- re.

  Mais, si je vous di -- sois que je veux vous choi -- sir
  pour at -- ta -- cher mon cœur d’une é -- ter -- nel -- le chaî -- ne ?

  Je vous é -- cou -- te -- rois peut- être a -- vec plai -- sir,
  mais je vous croi -- rois a -- vec pei -- ne.

  Re -- fu -- sez- vous d’u -- nir vô -- tre cœur & le mien ?

  Ju -- pi -- ter & Ju -- non nous oc -- cu -- pent sans ces -- se,
  nos soins sont as -- sez grands sans que l’A -- mour nous bles -- se,
  nous n’a -- vons pas tous deux le loi -- sir d’ai -- mer bien.

  Si je fais ma pre -- miere af -- fai -- re
  de vous voir & de vous plai -- re ?

  Je fe -- ray mon pre -- mier de -- voir
  de vous plaire, & de vous voir.

  Un cœur fi -- del -- le
  a pour moy de char -- mants ap -- pas :
  Vous a -- vez mille at -- traits, vous n’ê -- tes que trop bel -- le ;
  mais je crains que vous n’ay -- ez pas
  un cœur fi -- del -- le.

  Pour -- quoy crai -- gnez- vous tant
  que mon cœur se dé -- ga -- ge ?
  Pour -- quoy crai -- gnez- vous tant
  que mon cœur se dé -- ga -- ge ?
  Je vous per -- mets d’être in -- cons -- tant,
  si- tôt que je se -- ray vo -- la -- ge.
  Je vous per -- mets d’être in -- cons -- tant,
  si- tôt que je se -- ray vo -- la -- ge.

  Pro -- met -- tez- moy de cons -- tan -- tes a -- mours ;
  je vous pro -- mets de vous ai -- mer toû -- jours ;
  Pro -- met -- tez- moy de cons -- tan -- tes a -- mours ;
  je vous pro -- mets, je vous pro -- mets de vous ai -- mer toû -- jours.

  Que la feinte en -- tre nous fi -- nis -- se ;

  Par -- lons sans mis -- tere en ce jour.

  Le moindre ar -- ti -- fi -- ce,
  le moindre ar -- ti -- fi -- ce
  of -- fen -- se l’A -- mour.
  Le moindre ar -- ti -- fi -- ce,
  le moindre ar -- ti -- fi -- ce
  of -- fen -- se, of -- fen -- se l’A -- mour.
  Le moindre ar -- ti -- fi -- ce
  of -- fen -- se l’A -- mour.

  Quel soin presse i -- cy- bas Ju -- pi -- ter de des -- cen -- dre ?

  Le seul bien des mor -- tels luy fait quit -- ter les cieux ;
  Mais, quel soup -- çon nou -- veau Ju -- non peut- el -- le pren -- dre ?
  Ne sui -- vroit- el -- le point Ju -- pi -- ter en ces lieux ?

  Dans les Jar -- dins d’Hé -- bé, Ju -- non vient de se ren -- dre.

  Un Nu -- age en -- tr’ou -- vert la dé -- couvre à mes yeux.
  I -- ris parle ain -- si sans mis -- te -- re ?
  C’est ain -- si que je puis me fi -- er à sa foy ?

  Ne me re -- pro -- chez- pas que je suis peu sin -- ce -- re,
  vous ne l’ê -- tes pas plus que moy.

  Gar -- dez pour quel -- qu’au -- tre
  vôtre a -- mour trom -- peur,
  vôtre a -- mour trom -- peur ;
  je re -- prens mon cœur,
  re -- pre -- nez, re -- pre -- nez le vô -- tre.
  Gar -- dez pour quel -- qu’au -- tre
  vôtre a -- mour trom -- peur ;
  je re -- prens mon cœur,
  je re -- prens mon cœur,
  je re -- prens mon cœur,
  re -- pre -- nez, re -- pre -- nez le vô -- tre.
  Je re -- prens mon cœur,
  re -- pre -- nez, re -- pre -- nez le vô -- tre.
}
\tag #'recit2 {
  Pro -- met -- tez- moy de cons -- tan -- tes a -- mours ;
  je vous pro -- mets, je vous pro -- mets de vous ai -- mer toû -- jours ;
  Pro -- met -- tez- moy de cons -- tan -- tes a -- mours ;
  je vous pro -- mets, je vous pro -- mets de vous ai -- mer toû -- jours.

  Le moindre ar -- ti -- fi -- ce
  of -- fen -- se l’A -- mour.
  Le moindre ar -- ti -- fi -- ce,
  le moindre ar -- ti -- fi -- ce
  of -- fen -- se, of -- fen -- se l’A -- mour.
  Le moindre ar -- ti -- fi -- ce
  of -- fen -- se l’A -- mour.

  Gar -- dez pour quel -- qu’au -- tre
  vôtre a -- mour trom -- peur ;
  je re -- prens mon cœur,
  re -- pre -- nez, re -- pre -- nez le vô -- tre.
  Gar -- dez pour quel -- qu’au -- tre
  vôtre a -- mour trom -- peur ;
  Gar -- dez pour quel -- qu’au -- tre
  vôtre a -- mour trom -- peur ;
  je re -- prens, je re -- prens mon cœur,
  je re -- prens, je re -- prens mon cœur,
  re -- pre -- nez, re -- pre -- nez le vô -- tre.
  Je re -- prens mon cœur,
  re -- pre -- nez, re -- pre -- nez le vô -- tre.
}
