\key re \minor
\digitTime\time 3/4 \midiTempo#160 s2.*3
\time 4/4 \midiTempo#80 s1*3
\digitTime\time 3/4 s2.*3
\time 4/4 s1*2
\digitTime\time 3/4 s2.*2
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*6
\digitTime\time 3/4 s2.
\time 4/4 s1*6
\digitTime\time 3/4 \midiTempo#160 s2.*49
\time 4/4 \midiTempo#80
\key sol \major s1*3
\digitTime\time 3/4 \midiTempo#160 s2.*17
\time 4/4 \midiTempo#80 s1*3
\digitTime\time 3/4 s2.*2
\time 4/4 s1
\digitTime\time 3/4 s2.*2
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*6
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\time 3/8 s4.*36
\digitTime\time 2/2 s1 \bar "|."
