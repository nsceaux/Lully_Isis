<<
  \tag #'(recit1 basse) {
    \clef "vhaute-contre" R2.*3 |
    <>^\markup\character Mercure
    r4 re'8 re' sol4 la8 sib |
    la4\trill la8 sib do'4 re'8 la |
    sib4 sib8 do' re'4 re'8 sib |
    mib'4 do'8 do'16 do' fa'8. fa'16 |
    re'8.\trill re'16
    \ffclef "vbas-dessus" <>^\markup\character Iris
    sib'16 sib' sib' sib' sol'8\trill sol'16 sol' |
    mi'4\trill do''16 do'' do'' re'' sol'8\trill sol'16 la' |
    fa'4\trill fa'8 r
    \ffclef "vhaute-contre" <>^\markup\character Mercure
    la4\trill la16 la la la |
    sib4 sib8 re' sib4\trill sib8 sib |
    sol\trill sib sib sib mib' mib' |
    si8\trill si si si si do' |
    do'4 do'8
    \ffclef "vbas-dessus" <>^\markup\character Iris
    sol'8 sol' sol' sol' la' |
    sib' do'' re'' re'' mib'' fa'' |
    sib' mib''8. mib''16 mib'' re'' do''4\trill do''8 re'' |
    sib'4 sib'8
    \ffclef "vhaute-contre" <>^\markup\character Mercure
    re'8 re' re' re' re' |
    mib'4 do'8 do' la4\trill la8 la |
    fad4\trill
    \ffclef "vbas-dessus" <>^\markup\character Iris
    re''8 re'' la'4 la'8 re'' |
    si'4\trill sol'8 sol' do''4 do''8 do'' |
    la'4\trill la'8 fa' do'' do'' do'' re'' |
    mib''4 la'8\trill la'16 la' sib'8 do'' |
    fad'4\trill fad'8 re'' re'' re'' re''8. re''16 |
    mib''4 do''8 sib' la'4\trill la'8 sib' |
    sol'4\trill
    \ffclef "vhaute-contre" <>^\markup\character Mercure
    r8 sib16 sib sib8\trill sib16 sib sib8 la |
    la4\trill la8 fa'16 fa' re'8\trill re' re' mi' |
    dod'4\trill dod'8
    \ffclef "vbas-dessus" <>^\markup\character Iris
    la'16 la' re''8 re''16 do'' do''8. si'16 |
    si'4\trill si'8 mi'' dod''\trill re'' re'' dod'' |
    re''2.
    \ffclef "vhaute-contre" <>^\markup\character-text Mercure \normal-text Air
    sib4 do'4. re'8 |
    sib4( la2)\trill |
    sol4 re' mib' |
    fa'2 fa'8 sol' |
    mib'4.\trill( re'8) mib'4 |
    re'2\trill re'8 do' |
    si4\trill \sugRythme { si'4. si'8 } do'4 re' |
    mib'2 r8 sol' |
    sib4.\trill sib8 do'4 |
    re' mib'( re')\trill |
    do' mib'4. fa'8 |
    re'4\trill re'8 do' sib[ la16] sib |
    la2.\trill |
    sib4 do' re' |
    sib( la2)\trill |
    sol
    \ffclef "vbas-dessus" <>^\markup\character-text Iris \normal-text Air
    sol'4 |
    re''4. la'8 sib' do'' |
    sib'4.\trill sib'8 fad'4 |
    sol'2 sol'8 la' |
    fad'4\trill re' sol' |
    re''4. la'8 sib' do'' |
    sib'4.\trill sib'8 fad'4 |
    sol'2 sol'8 la' |
    fad'2\trill re'4 |
    r8 re'' re''4 sib' |
    sol'\trill do'' la'8 sib' |
    la'4\trill r8 fa' sib' sib' |
    do'' re'' mib'' re''[ do'' re''] |
    re''4.\trill do''8( sib'4) |
    r8 la' la'4 si' |
    do'' re'' re''16[ do''] re''8 |
    mib''2 r8 la' |
    fad'4.\trill re''8 la' sib' |
    fad' sol' sol'8.[ la'16]( la'4)\trill |
    sol'2. |
    r8 la' la'4 si' |
    do'' sol' la' |
    sib'2 sib'8 sib' |
    sol'4.\trill sol'8 sol' do'' |
    la'4.\trill la'8 sib'[ la'] |
    sib'[ do''] sib'4( la'8.)\trill sib'16 |
    sib'8 fa' fa'4 sol' |
    la' do'' re'' |
    sib' sib' do'' |
    la'8\trill la' la'4 sib' |
    sol'8\trill sol' sol'4 la' |
    fad'4.\trill fad'8 sol'[ fad'] |
    sol'[ la'] sol'4( fad'8.)\trill sol'16 |
    sol'2 r |
    \ffclef "vhaute-contre" <>^\markup\character Mercure
    r4 r8 re'16 re' si8\trill si16 si si8 re' |
    sol4 sol8
    \ffclef "vbas-dessus" <>^\markup\character Iris
    si'8 la'\trill la'16 re'' sol'8 sol'16 fad' |
    fad'2\trill re''4 |
    la'\trill la'4. si'8 |
    do''4 do'' r8 do'' |
    sol'4 sol'4. la'8 |
    si'4 si' r8 si' |
    mi'2\trill mi'8 la' |
    fad'2.\trill |
    r8 la' mi'4\trill mi'8 fad' |
    sol'4 sol' do'' |
    do'' si'4.\trill si'8 |
    si'4\trill la' r8 la' |
    la'4\trill sol' r8 sol' |
    sol'2 sol'8 fad' |
    sol'2 r8 si' |
    mi'4\trill mi'4. la'8 |
    fad'4\trill re' r8 sol' |
    sol'2 sol'8 fad' |
    sol'2 r |
    r4 <>^\markup\character Iris r8 si' mi''4 si'8 si'16 si' |
    sol'4 sol'8 si' fad'4 sol'8 la' |
    sol'4\trill mi'8
    \ffclef "vhaute-contre" <>^\markup\character Mercure
    sol16 la si8\trill si16 do' |
    re'8 re' mi' mi' fad' sol' |
    fad'4\trill r re'8 re'16 re' la8 si |
    do' mi' do'\trill do' do' si |
    si\trill si r16 si si si do'8 re' |
    mi'4 do'8 mi' la4 la8 la |
    fad4\trill
    \ffclef "vbas-dessus" <>^\markup\character Iris
    re''8 re''16 re'' si'8\trill si' |
    sold'4\trill r8 mi'' si'\trill si' si' do'' |
    la'4\trill la'
    \ffclef "vhaute-contre" <>^\markup\character Mercure
    r8 do'16 do' dod'8\trill dod'16 mi' |
    la4. re'16 re' re'4\trill re'8 dod' |
    re'4. re'8 la la16 la do'8 do'16 si |
    si8\trill si re' re' si4\trill do'8 re' |
    mi'4 do'8 mi' do'4\trill do'8 do' |
    la4\trill
    \ffclef "vbas-dessus" <>^\markup\character-text Iris Junon paroît au milieu d’un Nuage qui s’avance
    re''8 re''16 re'' la'8 si' |
    do''4 mi'8 mi' la'4 la'8 la' |
    fad'8\trill fad'16 r re'' fad' sol' la' si'4 la'8.\trill sol'16 |
    sol'4 sol'8 |
    la' si' do'' |
    si' sol' re'' |
    re'' re'' dod'' |
    re''4 si'8 |
    do''8 la' re'' |
    si'\trill si' do'' |
    re''4 re''8 |
    sol' do'' si' |
    la'\trill re'' do'' |
    si'4\trill mi''8 |
    si'( la'4)\trill |
    sol' r8 |
    R4. |
    r8 r mi' |
    fad' sold' la' |
    sold'\trill mi' la' |
    la' la' sold' |
    la'4 r8 |
    r la' sol' |
    fad'4\trill fad'8 |
    sol'4 r8 |
    r la' si' |
    do''4 do''8 |
    si'\trill si' do'' |
    re''4 re''8 |
    sol' do'' si' |
    la' re'' do'' |
    si'4\trill mi''8 |
    si'( la'4)\trill |
    sol'8 si' do'' |
    re''4 re''8 |
    \footnoteHere #'(0 . 0) \markup {
      Ballard 1719 : \italic la
    }
    \sug sol'8 do'' si' |
    la' re'' do'' |
    si'4 mi''8 |
    si'( la'4)\trill |
    sol'4 r r2 |
  }
  \tag #'recit2 {
    \clef "vhaute-contre" R2.*3 R1*3 R2.*3 R1*2 R2.*2 R1 R2. R1*6 R2. R1*6 R2.*35 |
    <>^\markup\character Mercure
    r8 re' re'4 \footnoteHere #'(0 . 0) \markup {
      Matériel 1677 : \italic mi♮
    } mib' |
    fa'4 fa' re' |
    mib' mib'8[ re'] mib'4 |
    re'4.\trill re'8 re' re' |
    sib4. mib'8 mib' mib' |
    mib'4. do'8 re'[ do'] |
    re'[ mib'] re'4( do'8.)\trill sib16 |
    sib8 re' re'4 mi' |
    fa' fa' fa' |
    fa'8[ mib'] mib'4 mib' |
    mib'?8 re' re'4 re' |
    re'8 do' do'4 do' |
    do'4. do'8 sib[ la] |
    sib[ do'] sib4( la8.)\trill sol16 |
    sol2 r |
    R1*2 R2.*2 |
    r4 r r8 mi' |
    si4\trill si4. do'8 |
    re'4 re' r8 re' |
    sol2 sol8 do' |
    la2\trill r8 la |
    do'4 do'4. re'8 |
    si4\trill si mi' |
    fad' fad'4. sol'8 |
    mi'4\trill mi' r8 mi' |
    re'4\trill re' r8 mi' |
    do'2\trill si8 do' |
    si2\trill re'4 |
    re' do'4. do'8 |
    do'4 do' r8 si |
    la2\trill si8 do' |
    si2\trill r |
    R1*2 R2.*2 R1 R2.*2 R1 R2. R1*6 R2. R1*2 |
    R4.*2 |
    r8 r re' |
    mi' fad' sol' |
    fad' re' sol' |
    sol' sol' fad' |
    sol' re' mi' |
    fa'4 fa'8 |
    mi' mi' mi' |
    fad' fad' fad' sol'[ fad'] sol' |
    sol'4( fad'8) |
    sol'4 si8 |
    dod' red' mi' |
    red'\trill si mi' |
    mi' mi' red'\trill |
    mi'4 la8 |
    si dod' re' |
    dod'\trill la re' |
    re' re' dod'\trill |
    re' re' do' |
    si\trill re' mi' |
    fa'4 fa'8 |
    mi'\trill mi' fad' |
    sol' sol' sol' |
    fa'8.\trill[ mi'16] fa'8 |
    mi'\trill mi' mi' |
    fad' fad' fad' |
    sol'[ fad'] sol' |
    sol'4( fad'8) |
    sol' re' mi' |
    fa'8.[ mi'16] fa'8 |
    mi'\trill mi' mi' |
    fad' fad' fad' |
    sol'[ fad'] sol' |
    sol'4( fad'8) |
    sol'4 r r2 |
  }
>>
\stopStaff
