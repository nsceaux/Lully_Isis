\clef "basse" sol4. \footnoteHere #'(0 . 0) \markup {
  Matériel 1677 : \italic fa♮
} fad8 sol re |
mib4. re8 mib do |
re4. do8 re re, |
sol,2 \once\tieDashed sol~ |
sol fad |
sol1 |
la2. |
sib2 si4 |
do'4 la8. sib16 do'8 do |
fa2 mib |
re1 |
mib2. |
re2 sol4 |
\once\tieDashed do2~ do8 do' sib la |
sol4 re2 |
mib fa4 fa, |
sib,2. si,4 |
do1 |
re2. re,4 |
sol, sol mi2 |
fa mib4. re8 |
\once\tieDashed do4~ do2 | \allowPageTurn
re si, |
do re4 re, |
sol, sol mi2 |
fa2 sib4. sol8 |
la2 fad |
sol4. mi8 la re la,4 |
re4. do8 sib, la, |
sol,4 la, sib,8_\markup\croche-pointee-double do |
re4 re,2 |
sol, sol4 |
re4. do8 re mib |
fa4. sol8 la fa |
sib4 fad2 |
sol8 fa mib4 re |
do4. re8 mib do |
sol4. sol8 lab4 |
fa sol sol, |
do4. sib,8 la,4 |
sib, fad, sol, |
re, re8 mib re do |
sib,4 la, sol, |
re re,2 |
sol, sol4 |
\once\tieDashed fad2~ fad4 | \allowPageTurn
sol2 re4 |
mib2 do4 |
re4. do8 sib,4 |
fad,2. |
sol,2 re4 |
mib2 do4 |
re2 re8 do |
sib,4. do8 re4 |
mib mi2 |
fa4. mib8 re4 |
do8 sib, mib do fa fa, |
sib, la, sib, do re mib |
fa2. |
mib4 si,2 |
do4. re8 mib do |
re re' la sib fad sol |
re sol, re,2 |
sol,4 sol2 |
re2 sol4 |
do2 do4 |
sol2 re4 |
mib2 do4 |
fa2 \once\tieDashed sib,4~ |
sib, fa,2 |
sib,4 sib2 |
la2. |
sol2. |
fa2. |
mib |
re2 sol,4~ |
sol, re,2 |
sol,1~ | \allowPageTurn
sol,2 \once\tieDashed sol~ |
sol fad4 mi |
re2 re8 mi |
fa2 mi8 re |
do2 do8. re16 |
mi2 re8 do |
si,2~ si,8 si, |
do2~ do8 la, |
re2~ re8 re |
la,2~ la,8 la, |
mi2~ mi8 mi |
re2~ re8 re |
do2~ do8 do |
si,2~ si,8 si, |
la,4 re re, |
sol,2 sol,4 |
do la,2 |
re mi4 |
do re re, |
sol,2. fad,4 |
mi,1 |
mi2. red4 |
mi2 re8. do16 |
si,4 do dod |
re2 re, |
la,2. |
\once\tieDashed mi2~ mi8 re |
do2 dod |
re2. | \allowPageTurn
mi2. mi,4 |
la,2. sol,4 |
fad,2 mi, |
re, re4 fad, |
sol,2 sol8 fa mi re |
do2 la, |
re4 re'8 re' do' si |
la2 dod |
re4~ re16 do si, la, sol,4 re, |
sol, sol8 |
sol4 fad8 |
sol4 fad8 |
mi la la, |
re4 mi8 |
do re4 |
sol do8 |
si,4. |
do8 la,4 |
re4. |
mi4 do8 |
re re,4 |
sol, sol8 |
la si do' |
si8. la16 sol8 |
fad si si, |
mi8. re16 dod8 |
si, mi mi, |
la, la fad |
re la,4 |
re4. |
sol,8 sol4 |
re4. |
la4 la,8 |
mi4. si,8 si4 |
do'4. |
re' |
mi'4 do'8 |
re' re4 |
sol do8 |
si,4. |
do |
re |
mi4 do8 |
re re,4 |
sol,2~ sol,8 sol, fad, mi, |
\once\set Staff.whichBar = "|"
\footnoteHere #'(0 . 0) \markup {
  Ce prélude à la basse continue n'est pas présent dans Ballard 1719.
}
\key re \major
re,4. re8 fad4 re |
la4. la8 dod'4 la |
re' si mi' mi |
la la, dod la, |
re sol, la,2 |
re,1 |

