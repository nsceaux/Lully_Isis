\clef "haute-contre" re''4 |
re''4. do''8 sib'4 |
sib' sib' sib' |
sib' la'4. la'8 |
sib'4. sib'8 sol'4 |
do'' sib' la' |
re'' do''4. do''8 |
do''4. sib'8 la'4 |
do''4. sib'8 la'4 |
la'4. la'8 sol'4 |
sol'4. fa'8 fa' sol' |
la'4 fa' sol' |
la' fa' la' |
la'4. sib'8 do''4 |
sib' sib' re'' |
re'' sib'2 |
sib'4 sib' sol' |
lab' fa'4. sib'8 |
sol'2 sol'4 |
sol'4. fa'8 mi'4 |
fa' sib'2 |
sib'4 la'4.\trill sib'8 |
sib'2 sib'4 |
sib'8 do'' re''4 sol' |
sol'4. fa'8 mi'4 |
fa' sib'2 |
sib'4 la'4.\trill sib'8 |
sib'2. |
