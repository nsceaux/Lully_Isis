\clef "taille" fa'4 |
fa'2 fa'4 |
re' sib sib |
do' do'4. do'8 |
sib4. fa'8 sol'4 |
la' mi' fa' |
fa' mi'4.\trill fa'8 |
fa'2 fa'4 |
fa'2 fa'4 |
fa'4. fa'8 mib'4 |
re'2 re'8 mi' |
fa'4 fa' mi' |
fa'2 fa'4 |
fa'2 fa'4 |
fa'2 fa'4 |
re'4. mib'8 fa'4 |
sol' fa' mib' |
mib' re'4.\trill mib'8 |
mib'2 sib4 |
do'2 do'4 |
do' re'2 |
do'4 do'4. re'8 |
re'4. mib'8 fa'4 |
sol' re' mib'8 fa' |
sol'4 do'2 |
do'4 re'2 |
do'4 do'4. re'8 |
re'2. |
