\clef "basse"
\footnoteHere #'(0 . 0) \markup { Matériel 1677 : B.C. tacet. }
sib,4 |
sib,2 sib,8 do |
re4 mib8 fa sol4 |
do fa fa, |
sib, sib2 |
la4 sol fa |
sib, do2 |
fa,2 fa4 |
fa,2. |
fa4. fa8 do4 |
sol4. la8 sib4 |
la sib sol |
fa2 fa8 sol |
la4. sol8 fa4 |
sib2 sib,8 do |
re4. do8 sib,4 |
mib sib, do |
lab, sib,2 |
mib, mib4 |
mi!4. re8 do4 |
fa4 re2 |
mib4 fa fa, |
sib, sib lab |
sol fa mib |
mi!4. re8 do4 |
fa re2 |
mib4 fa fa, |
<<
  \tag #'basse sib,2.
  \tag #'basse-continue {
    sib,2.~ |
    sib,4. do8 sib, la, |
    \custosNote sol,8
  }
>>
