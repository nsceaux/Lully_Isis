\clef "dessus" fa''4 |
sib''4. la''8 sib''4 |
fa'' sol'' re'' |
mib'' do'' fa'' |
re''4.\trill re''8 mib''4 |
fa'' sol'' la'' |
sib'' sol''4. la''8 |
la''4.\trill sol''8 fa''4 |
la''4.\trill sol''8 fa''4 |
do''4. re''8 mib''4 |
sib'4. do''8 re''4 |
do'' re'' sib' |
do'' la' fa'' |
do''4. re''8 mib''4 |
re'' sib' sib'' |
fa''4. sol''8 lab''4 |
sol'' re'' mib'' |
fa'' fa''4.\trill mib''8 |
mib''?4. fa''8 sol''4 |
do''4. re''8 sib'4 |
la'4\trill fa''2 |
mib''8 re'' do''4.\trill sib'8 |
sib'4. do''8 re''4 |
mib'' fa'' sol'' |
do''4. re''8 sib'4 |
la' fa''2 |
mib''8 re'' do''4.\trill sib'8 |
sib'2. |
