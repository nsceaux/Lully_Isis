\piecePartSpecs
#`((dessus #:score-template "score-dessus2")
   (basse #:score-template "score-voix"
          #:music ,#{ \beginMark TACET #})
   (basse-continue #:score-template "score-voix"))
