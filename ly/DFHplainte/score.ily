\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new GrandStaff <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'berger1 \includeNotes "voix"
    >> \keepWithTag #'berger1 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'berger2 \includeNotes "voix"
    >> \keepWithTag #'berger2 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'pan \includeNotes "voix"
    >> \keepWithTag #'pan \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1.*6 s1 s1. s1\break s1. s1 s1.*2\break s1 s1. s1*2\pageBreak
        s1. s1*2 s1.\break s1*2 s1. s1\break s1*3 s1. s1\pageBreak
        s1*2 s1.*3\break s1.*2 s1*3 s1.\break s1.*4 s1\pageBreak
        s1*3 s1.*4\break s1.*4 s1*2\break s1 s2.*2\pageBreak
        s1*3\break s2.*2 s1\break s1*4\pageBreak
        s1*3 s1.\break s1. s1*6\break s1*3 s1.\pageBreak
        s1 s1*7\break s1. s1*4\pageBreak
        s1*8\break s1*7\pageBreak
        s1*8\break s1*6\pageBreak
        s1*4 s2 \bar "" \break
      }
    >>
  >>
  \layout { }
  \midi { }
}
