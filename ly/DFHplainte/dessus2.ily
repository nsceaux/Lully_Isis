\clef "dessus" R1.*4 |
r2 r4 mib'' fa''2 |
mib''2. mib''4 fa''2 |
mib''2 r |
r r4 la' sib'2 |
la' r |
r r4 re'' mib''2 |
re''4 r r2 |
r2 r4 sol' lab'2 |
sol' r r |
R1 |
r2 r4 si' do''2 |
si'4 r r2 |
R1 |
r2 r4 re'' mib''2 |
re'' r |
R1 |
r2 r4 sib' do''2 |
sib'4 r r2 |
R1 |
r2 r4 si' do''2 |
si' r |
R1*3 |
r2 r4 mib'' fa''2 |
mib''2 r |
R1*2 |
R1. |
r2 r4 si' do''2 |
si'2\trill r r |
R1. |
r2 r4 la' sib'2 |
la'2 r |
R1*2 |
r2 r4 re'' mib''2 |
re'' r r |
R1.*2 |
r2 r4 si' do''2 |
si'\trill r |
R1*3 |
r2 r4 mib'' fa''2 |
mib''2 r r |
R1.*6 |
r4 r8 mib'' re''8.\trill re''16 re''8 mib'' |
do''4.\trill do''8 re'' mib'' mib'' re'' |
mib''4. mib''8 mib''8. re''16 \once\tieDashed re''4~ |
re''8 sol' sol' sol' do''8. sib'16 |
la'8 sib' do''8. sib'16 la'8 re'' |
si'4. si'8 si'4. si'8 |
do''4. do''8 do''4 do''8 re'' |
mib''4 mib''8 fa'' sol''4. sol''8 |
do''8^\markup\croche-pointee-double do'' \once\tieDashed do''4~ do''8 re''16 mib'' |
re''8. fa''16 fa''4.\trill fa''8 |
sol''8 lab'' sib'' do''' fa''8. sol''16 \once\slurDashed lab''8.( sib''16) |
sol''4\trill mib''8. mib''16 do''8. do''16 sib'8 do''16 sol' |
lab'4. sib'8 sol'8.\trill sol'16 sol'8 do'' |
si'4\trill re''8 re'' mib''4 si'8\trill do'' |
re''4 sol''4. fa''8 mib''4~ |
mib'' \once\tieDashed re''~ re''4. re''8 |
re''4 si'8 si' do''4. do''8 |
do''2 si'4.(\trill la'16 si') |
do''2 r r |
R1. |
r4 si' si' do'' |
re''1~ |
re''4 sol' sol' la' |
si'2. si'4 |
si'2 \once\tieDashed do''~ |
do'' si' |
do''4 r r2 |
R1*2 |
R1. |
r2 r4 sol''8 sol'' |
sol''4. sol''8 fa''4 mi'' |
fa''4 mib'' re'' do'' |
si'2 do''~ |
do''4. do''8 si'4.\trill do''8 |
do''1 |
R1*2 |
R1. |
r4 re'' fa''4. fa''8 |
sib'2 mib''4. mib''8 |
mib''2 re'' |
mib'' r |
R1*3 |
r4 si'8 do'' re''2 |
sol'4. do''8 re''4 mi'' |
fa''4 re'' si'4.\trill si'8 |
do''2 si'4. do''8 |
do''2 r |
R1*2 |
r4 re'' re'' do'' |
do'' do'' sib'\trill la' |
sib'2 r |
R1*3 |
r4 si'8 do'' re''2 |
sol' do''4. sol'8 |
lab'2 \once\slurDashed la'4( si'8) do'' |
si'1\trill |
r4 sol'' sol'' sol'' |
sol'' do'' do'' do'' |
do''2 la'4 la' |
sol'2 sol'4 sol' |
sol'1 |
sol'2. mib''4 |
mib'' mib'' fa''4. fa''8 |
sol''2. sol''4 |
do'' si' do''4. re''8 |
si'!2\trill r |
R1*5 R2. |
