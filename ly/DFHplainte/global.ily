\key sol \minor
\time 3/2 \midiTempo#160 s1.*6
\time 2/2 s1
\time 3/2 s1.
\time 2/2 s1
\time 3/2 s1.
\time 4/4 \midiTempo#80 s1
\time 3/2 \midiTempo#160 s1.*2
\time 4/4 \midiTempo#80 s1
\time 3/2 \midiTempo#160 s1.
\time 4/4 \midiTempo#80 s1*2
\time 3/2 \midiTempo#160 s1.
\time 2/2 s1
\time 4/4 \midiTempo#80 s1
\time 3/2 \midiTempo#160 s1.
\time 4/4 \midiTempo#80 s1*2
\time 3/2 \midiTempo#160 s1.
\time 2/2 s1*4
\time 3/2 s1.
\time 2/2 s1*3
\time 3/2 s1.*5
\digitTime\time 2/2 s1*3
\time 3/2 s1.*5
\digitTime\time 2/2 s1*4
\time 3/2 s1.*8
\time 4/4 \midiTempo#80 s1*3
\digitTime\time 3/4 s2.*2
\time 4/4 s1*3
\digitTime\time 3/4 s2.*2
\time 4/4 s1*7
\digitTime\time 2/2 \midiTempo#160 s1
\time 3/2 s1.*2
\digitTime\time 2/2 s1*9
\time 3/2 s1.
\digitTime\time 2/2 s1*8
\time 3/2 s1.
\digitTime\time 2/2 s1*33
\key sol \major s1*2
\time 4/4 \midiTempo#80 s1
\time 2/2 \midiTempo#160 s1*3
\digitTime\time 3/4 \midiTempo#80 s2. \bar "|."
