<<
  \tag #'(pan basse) {
    \clef "vbasse" <>^\markup\italic Tristement R1.*2 |
    r2 r do' |
    si1 si2 |
    do'1 r2 |
    R1. |
    r2 r4 do' |
    la2\trill r r |
    r r4 fa |
    re2\trill re4 r r2 |
    r4 sib4. lab16 lab lab8[ sol16] lab |
    sol2\trill sol4 r r2 |
    r4 sol sol fa mib\trill re |
    mib mib8 fa sol4 sol8 fa |
    sol2 r r |
    r4 r8 sol sol la sib[ la16] sib |
    la4\trill fa8 fa sib4 sib8 la |
    sib2 r r |
    r4 sib re4. re8 |
    mib4 sib8 do' fa4\trill fa8 sol |
    mib2 mib4 r r2 |
    r4 r8 sib sib sib lab\trill sol |
    lab4 fa8 fa fa4 mib8[ re16] mib |
    re1\trill r2 |
    r4 sol sol sol |
    sol fa fa fa |
    fa2 mib4( re8) mib |
    re2\trill re4. mib8 |
    do2\trill do4 r r2 |
    r4 do'8 do' do'4 re'8 re'16[ do']( |
    sib2) sib4 sib |
    fad2\trill r4 re8 re |
    sol2 sol8 r sol4 sol fad |
    sol2 sol4 r r2 |
    r sol4 sol8 sol mi4\trill mi |
    fa4. fa8 sol4. lab8 sol4.\trill fa8 |
    fa2 r r |
    r4 fa sol la |
    sib2 re4 mib |
    do2\trill do4 re |
    sib,2 sib,4 r r2 |
    r sib4 sib8 sib sol4 re |
    mib2 do'4 do'8 sib lab4\trill sol |
    lab sol fa mib re4.\trill do8 |
    si,2\trill r r |
    r4 sol sol sol |
    sol fa fa fa |
    fa2 mib4( re8) mib |
    re2\trill re4 mib |
    do2\trill do4 r r2 |
    R1.*7 |
    r4 r8 do' do'8. sib16 sib8 do' |
    lab4 r8 lab lab sol fa8.\trill mib16 |
    mib8 r16 mib sol8 mib sib8. sib16 si8\trill si |
    do'4 r8 do'16 do' la8\trill la16 la |
    re'4 fad8 fad16 sol re8. re16 |
    sol4 sol8 r16 sol fa8. fa16 mib8\trill re |
    mib4 mib8 mib lab4 lab8 fa |
    do'4 do8 re mib4 mi8.\trill mi16 |
    fa8. fa16 fa8 sol lab[ sol16] lab |
    sib8 r16 sib lab8 lab lab[ sol16] lab |
    sol4\trill sol8 lab sib4 sib8 sib, |
    mib4 mib8 r16 mib lab8. lab16 mi8.\trill mi16 |
    fa4 fa8 fa do'4 do'8 do |
    sol4 sol8 fa mib4 re8 do |
    si,4. si,8 do re mib[ re16] mib |
    fa4. fa8 fad fad fad fad |
    sol4 sol8 sol lab4 lab8 fa |
    sol2( sol,) |
    do2 r4 sol sol sol |
    do'2. do'4 do' fa |
    sol1 |
    R1*4 |
    r2 r4 sol8 sol |
    do'4 <<
      \tag #'basse { s2. s1*2 s1. s1*4 \ffclef "vbasse" }
      \tag #'pan {
        do'8 do' fa4 fa |
        sib2 sib4 lab |
        sol2 sol4 sol |
        lab2 lab4 fa sib sib, |
        mib2 mib4 r4 |
        R1*3 |
      }
    >>
    r4 sol sol fa |
    <<
      \tag #'basse {
        mi4 s2. s1*2 s1. s1*2 \ffclef "vbasse"
      }
      \tag #'pan {
        mi2. mi4 |
        fa4 fa fa mib! |
        re2. re4 |
        mib mib mib re do4.\trill sib,8 |
        sib,2 r |
        R1 |
      }
    >>
    r4 sib sib lab |
    sol <<
      \tag #'basse { s2. s1*25 s2 r4 \ffclef "vbasse" }
      \tag #'pan {
        sol4 re re |
        mib2 mib4 re |
        do2 do4 sib, |
        lab,1 |
        sol,2 r |
        R1*3 |
        r4 do' do' do' |
        do' do' sib sib |
        fad fad sol( fad8) sol |
        re2 r |
        R1 |
        r4 sol sol re |
        mib2 mib4 mib |
        do2 do4 do |
        re1 |
        sol,2 r |
        R1*3 |
        r4 mib mib mib |
        mi mi mi mi |
        fa2 fad4 fad |
        sol2 sol4 do |
        sol,1 |
        do2.
      }
    >> do'4 |
    do' do' lab lab |
    mi2. mi4 |
    fa4 sol lab( sol8) \once\override TextScript.self-alignment-X = #RIGHT lab
    -\tag #'pan ^\markup\override #'(baseline-skip . 0) \column\italic\smaller {
      \line { Argus commence à s’assoupir ; Mercure déguisé
        en Berger s’approche
      }
      \line { de luy, & acheve de l’endormir en le touchant de son Caducée. }
    }
    sol2
    \ffclef "vhaute-contre" <>^\markup\character Mercure
    r4 re'8 re' |
    << { \voiceOne si2\trill \oneVoice } \new Voice { \voiceTwo \sug sol2 } >> r4 sol8 sol |
    do'4 r8 sol16 sol sib4 sib8 la |
    la4\trill r8 la re'4 r8 la |
    do' do' do' si si4\trill si |
    sol'8 sol' fa' mi' re'4\trill re'8 mi' |
    do'4\trill do' r |
  }
  \tag #'(berger1 basse) {
    <<
      \tag #'basse {
        s1.*6 s1 s1. s1 s1. s1 s1.*2 s1 s1. s1*2 s1. s1 s1 s1. s1*2
        s1. s1*4 s1. s1*3 s1.*5 s1*3 s1.*5 s1*4 s1.*8 s1*3 s2.*2 s1*3
        s2.*2 s1*7 s1 s1.*2 s1*6 s4 \ffclef "vhaute-contre"
      }
      \tag #'berger1 {
        \clef "vhaute-contre"
        R1.*6 R1 R1. R1 R1. R1 R1.*2 R1 R1. R1*2 R1. R1 R1 R1. R1*2
        R1. R1*4 R1. R1*3 R1.*5 R1*3 R1.*5 R1*4 R1.*8 R1*3 R2.*2 R1*3
        R2.*2 R1*7 R1 R1.*2 R1*6
        r4^\markup\italic { Deux Bergers se joignent à Pan }
      }
    >> sol'8 sol' lab'4 lab' |
    fa'2\trill fa'4 fa' |
    sol'2 sol'4 sol' |
    mib'2 mib'4 lab' fa'4\trill( mib'8) fa' |
    sol'2 sol'4 r |
    <<
      \tag #'basse { R1*3 s1 s4 \ffclef "vhaute-contre" }
      \tag #'berger1 { R1*4 | r4 }
    >> sol'4 sol' sol' |
    do' do' do' do' |
    fa'2. fa'4 |
    sib sib do' re' mib'( re'8) mib' |
    re'2\trill r |
    R1 |
    <<
      \tag #'basse { s1 s4 \ffclef "vhaute-contre" }
      \tag #'berger1 { R1 r4 }
    >> mib'4 fa' fa' |
    sib2 sib4 sib |
    mib'2 mib'4 re' |
    do'1 |
    si2\trill r |
    R1*3 |
    r4 sol' sol' sol' |
    fad'\trill fad' sol' sol' |
    la' la' re'4. sol'8 |
    fad'2\trill r |
    R1 |
    r4 re' re' re' |
    sib2 sib4 sib |
    mib'2 do'4. sib8 |
    sib2( la)\trill |
    sol r |
    R1*3 |
    r4 sol' sol' sol' |
    sol' sol' sol' sol' |
    do'2 re'4 re' |
    re'2 re'4 mib' |
    mib'2( re')\trill |
    do'
    \tag #'berger1 { r2 | R1*9 R2. }
  }
  \tag #'berger2 {
    \clef "vtaille" R1.*6 R1 R1. R1 R1. R1 R1.*2 R1 R1. R1*2 R1. R1 R1
    R1. R1*2 R1. R1*4 R1. R1*3 R1.*5 R1*3 R1.*5 R1*4 R1.*8 R1*3 R2.*2
    R1*3 R2.*2 R1*7 R1 R1.*2 R1*6 |
    r4 mib'8 mib' fa'4 fa' |
    re'2\trill re'4 re' |
    mib'2 mib'4 mib' |
    do'2\trill do'4 fa' re'\trill( do'8) re' |
    mib'2 mib'4 r |
    R1*4 |
    r4 do' do' sib |
    lab lab lab lab |
    lab2.( sol8) lab |
    sol4\trill sol la sib sib la |
    sib2 r |
    R1*2 |
    r4 sib sib lab |
    sol2 sol4 << \new Voice { \voiceOne \sug sol } { \voiceTwo fa \oneVoice } >> |
    sol2 sol4 sol sol2( fa) |
    sol r |
    R1*3 |
    r4 mib' mib' mib' |
    re' re' re' re' |
    do' do' sib( la8) sib |
    la2\trill r |
    R1 |
    r4 sib sib sib |
    sol2 sol4 sol |
    do'2 la4.\trill sol8 |
    sol2( fad\trill) |
    sol2 r |
    R1*3 |
    r4 sib sib sib |
    sib sib sib( la8) sib |
    la2 do'4 do' |
    si2 si4 do' |
    do'2.( si4) |
    do'2 r |
    R1*9 R2.
  }
>>
