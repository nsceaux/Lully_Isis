\tag #'(pan basse) {
  He -- las ! Hé -- las ! quel bruit ! qu’en -- tens- je ! Ah ! quel -- le voix nou -- vel -- le !
  La Nym -- phe tâche en -- cor d’ex -- pri -- mer ses re -- grets.
  Que son mur -- mure est doux ! que sa plainte a d’at -- traits !
  Ne ces -- sons point de nous plaindre a -- vec- el -- le.
  Que son mur -- mure est doux ! que sa plainte a d’at -- traits !
  Ne ces -- sons point, ne ces -- sons point de nous plaindre a -- vec- el -- le.
  Ra -- ni -- mons les res -- tes char -- mants
  d’u -- ne Nym -- phe qui fut si bel -- le,
  El -- le ré -- pond en -- cor à nos gé -- mis -- se -- ments,
  ne ces -- sons point de nous plaindre a -- vec el -- le.
  El -- le ré -- pond en -- cor,
  el -- le ré -- pond en -- cor à nos gé -- mis -- se -- ments,
  ne ces -- sons point,
  ne ces -- sons point de nous plaindre a -- vec el -- le.
  
  Les yeux qui m’ont char -- mé ne ver -- ront plus le jour :
  E -- toit-ce ain -- si, cru -- el A -- mour,
  qu’il fal -- loit te ven -- ger d’u -- ne Beau -- té re -- bel -- le ?
  N’au -- roit- il pas suf -- fi de t’en ren -- dre vain -- queur,
  et de voir dans tes fers son in -- sen -- si -- ble cœur
  brû -- ler a -- vec le mien d’un ar -- deur é -- ter -- nel -- le ?
  N’au -- roit- il pas suf -- fi de t’en ren -- dre vain -- queur,
  et de voir dans tes fers son in -- sen -- si -- ble cœur
  brû -- ler a -- vec le mien d’un ar -- deur é -- ter -- nel -- le ?
  Que tout res -- sen -- te mes tour -- ments.

  Ra -- ni -- mons, ra -- ni -- mons
}
\tag #'(berger1 berger2) { Ra -- ni -- mons }
les res -- tes char -- mants
d’u -- ne Nym -- phe qui fut si bel -- le :
\tag #'pan { El -- le ré -- pond en -- cor, }
\tag #'basse { El -- le ré -- pond, El -- le ré -- pond, }
\tag #'(berger1 berger2) { El -- le ré -- pond, }
el -- le ré -- pond en -- core à nos ge -- mis -- se -- ments.
\tag #'(pan basse) { Ne ces -- sons point, ne ces -- sons point }
\tag #'(berger1 berger2) { Ne ces -- sons point, }
de nous plaindre a -- vec el -- le.
El -- le ré -- pond en -- core à nos ge -- mis -- se -- ments.
Ne ces -- sons point de nous plaindre a -- vec el -- le.
Ne ces -- sons point,
ne ces -- sons point de nous plaindre a -- vec el -- le.

Que ces ro -- seaux plain -- tifs soient à ja -- mais ai -- mez…

Il suf -- fit, Ar -- gus dort, tous ses yeux sont fer -- mez.
Al -- lons, que rien ne nous re -- tar -- de,
dé -- li -- vrons la Nym -- phe qu’il gar -- de.
