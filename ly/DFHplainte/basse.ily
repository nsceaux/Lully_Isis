\clef "basse" do'1~ do'4 do' |
si2 sib2. sib4 |
lab1. |
sol1 sol,2 |
\tieDashed do1.~ |
do~ |
do1 |
fa1. |
fa,1 |
sib,1.~ |
sib,2 do4 re |
mib1.~ |
mib2 si,1 |
do2 sib,4 lab, |
sol,1. |
sol2~ sol8 fa mi4 |
fa2 re8 mib! do4 |
sib,1. |
sib,2 lab, |
sol,4. lab,8 sib,2 |
mib1. |
mib2 mi4 mi, |
fa,2 sol,~ |
sol,1.~ |
sol,1 |
lab,2 la, |
si, do |
sol,1 |
do1. | \allowPageTurn
do'2 fad |
sol2 sol, |
re1 |
mib2 do re4 re, |
sol,1.~ |
sol,1 do4 sib, |
lab,2 sol,4 fa, do2 |
fa,1. |
fa2. mib4 |
re2 sib,4 mib, |
fa,1 |
sib,1.~ |
sib,1 si,2 |
do mi,1 |
fa,1. |
sol,~ |
sol,1 |
lab,2 la, |
si, do |
sol,1 |
do1.~ |
do2 do'2. do'4 |
si2 sib2. sib4 |
la2 lab2. lab4 |
mi2 mi2. mi4 |
fa2 fad2. fad4 |
sol2 sol lab |
fa sol sol, |
do4. do'8 do'4 sib8 do' |
lab4. lab8 lab sol fa4 |
mib8 r16 mib sol8 mib sib8. sib16 si8 si |
do'4. do'8 la8. la16 |
re'4 fad8. sol16 re4 |
sol4. sol8 fa4 mib8 re |
mib4. mib8 lab4 lab8 fa |
do'4 do8 re mib4 mi |
fa4. sol8 lab4 |
sib lab2 |
sol4 sol8 lab sib4. sib,8 |
mib4. mib8 lab8. lab16 mi8. mi16 |
fa4. fa8 do'4 << { do'8 do } \\ \sug do4 >> |
sol4. fa8 mib4 re8 do |
si,4. si,8 do re mib4 |
fa4. fa8 fad2 |
sol4. sol8 lab4. fa8 |
sol2 sol, |
do1~ do4 sib, |
lab,1. |
sol,2 sol4 do |
si,1 |
do |
sol,2. sol4 |
fa2 mib4 fa |
sol2. sol4 |
do'2 fa |
sib2. lab4 |
sol2. sol4 |
lab2. fa4 sib sib, |
mib2~ mib4. re8 |
do4 sib, lab, sol, |
fa,2 fa |
sol do4 fa, |
sol, sol2 fa4 |
mi?2. mi4 |
fa2 fa4 mib |
re2. re4 |
mib mib mib re do2 |
sib,4 sib re2 |
mib mib4 lab, |
sib,4 sib sib lab |
sol sol re re |
mib2 mib4 re |
do2 do4 sib, |
lab,1 | \allowPageTurn
sol,4 sol si,2 |
do4 sib, lab, sol, |
fa, fa fa2 |
mib4 do sol sol, |
do2. do'4 |
do' do' sib sib |
fad2 sol |
re4 re re mi |
fad1 |
sol4 sol sol re |
mib2 mib4 mib |
do2. do4 |
re1 |
sol,4 sol si,2 |
do2. do4 |
fa,2 fad, |
sol, sol4 fa |
mib1 |
mi |
fa2 fad |
sol2. do4 |
sol,1 |
do2. do'4 |
do' do' lab lab |
mi2. mi4 |
fa sol lab2 | \tieSolid
sol1~ |
sol |
mi | \tieDashed
fa2 fad~ |
fad sol |
mi4 re8 do sol4 sol, |
do4. do8 si, la, |
\once\set Staff.whichBar = "|"
\custosNote sol,8
