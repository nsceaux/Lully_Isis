\clef "dessus" <>^"Flutes" R1.*4 |
r2 r4 sol'' lab''2 |
sol''2. sol''4 lab''2 |
sol'' r |
r r4 do'' re''2 |
do'' r |
r r4 fa'' sol''2 |
fa''4 r r2 |
r r4 sib' do''2 |
sib' r r |
R1 |
r2 r4 re'' mib''2 |
re''4 r r2 |
R1 |
r2 r4 fa'' sol''2 |
fa'' r |
R1 |
r2 r4 sol'' lab''2 |
sol''4 r r2 |
R1 |
r2 r4 re'' mib''2 |
re'' r |
R1*3 |
r2 r4 sol'' lab''2 |
sol'' r |
R1*2 |
R1. |
r2 r4 re'' mib''2 |
re'' r r |
R1. r2 r4 do'' re''2 |
do'' r |
R1*2 |
r2 r4 fa'' sol''2 |
fa'' r r |
R1.*2 |
r2 r4 re'' mib''2 |
re'' r |
R1*3 |
r2 r4 sol'' lab''2 |
sol'' r r |
R1.*6 | \allowPageTurn
r4 r8 sol'' fa''4\trill sol''8. sol''16 |
\slurDashed sol''8.( lab''16) fa''8\trill mib'' fa'' sol'' lab''( sol''16) lab'' |
sol''4. sol''8 fa''8.\trill fa''16 fa''8( mib''16 re'') |
mib''4. mib''8 mi''( fa''16) sol'' |
fad''8 sol'' la''8. sol''16 sol''8 fad'' |
sol''4. re''8 re''4. re''8 |
sol'4 sol''8 sol'' mib''4 lab''8( sol''16) lab'' |
sol''4\trill sol''8 fa'' mib'' re'' do''8. sol'16 |
lab'4 lab''8 sol'' fa''8.\trill fa''16 |
fa''8. re''16 re''4.\trill re''8 |
mib''4. mib''8 mib''4 re''8(\trill do''16 re'') |
mib''4 sol''8. sol''16 mib''8. fa''16 sol''4 |
do''8 do'' do'' re'' mib''4.( re''16) mib'' |
re''4\trill si'8 si' do'' do'' re'' mib'' |
fa''4. mib''16 re'' mib''8 fa'' sol'' sol' |
lab'4. lab'8 la'8. si'?16 do''4 |
si'4\trill sol''8 fa'' mib''4. fa''8 |
re''4. mib''8 re''4.\trill do''8 |
do''2 r r |
R1. |
r4 re'' re'' mib'' |
fa''2. sol''8 re'' |
mib''4 mib'' mib''( re''8) mib'' |
re''2. re''4 |
re''2 mib'' |
re''1\trill |
do''4 r r2 |
R1*2 R1. | \allowPageTurn
r2 r4 mib''8 mib'' |
mi''4. mi''8 fa''4 sol'' |
lab'' sol'' fa''4.\trill mib''8 |
fa''4 mib''8 re'' mib''4. fa''8 |
re''4.\trill re''8 re''4.\trill do''8 |
do''1 |
R1*2 |
R1. | \allowPageTurn
r4 sib'' lab''4. sib''8 |
sol''2. lab''4 |
fa''4.\trill fa''8 fa''4. sol''8 |
mib''2 r |
R1*3 |
r4 sol'' fa''4. sol''8 |
mi''4. mi''8 fa''4 sol'' |
lab'' fa'' re''4. re''8 |
mib''4. re''8 re''4.\trill do''8 |
do''2 r |
R1*2 |
r4 fad'' fad'' sol'' |
la''2. la''4 |
re''2 r |
R1*3 |
r4 sol'' fa''4. sol''8 |
mib''2 mib''4. mib''8 |
mib''2 re''4.\trill re''8 |
re''1 |
r4 mib'' mib'' mib'' |
do'' sol'' sol'' sol'' |
\sugNotes {
  fa''2 la''4 la'' |
  re''2
} sol''4 sol'' |
sol''2. re''4 |
mib''2. sol''4 |
sol'' sol'' \sugRythme { si'4. si'8 } lab''4 lab'' |
sib''2. do'''8 sol'' |
lab''4 sol'' sol'' fa'' |
sol''2 r |
R1*5 R2. |
