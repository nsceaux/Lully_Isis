\bookpart {
  \act "Acte Cinquième"
  \sceneDescription\markup\wordwrap-center {
    Le Théatre change, & représente les Rivages du Nil, & l’une des Embouchûres
    par où ce Fleuve entre dans la Mer.
  }
  \scene "Scene Premiere" "Scene I"
  \sceneDescription\markup\wordwrap-center { Io, la Furie. }
  %% 5-1
  \pieceToc "Ritournelle"
  \includeScore "FAAritournelle"
  %% 5-2
  \pieceToc\markup\wordwrap {
    Io : \italic { Terminez mes tourments, puissant Maître du monde }
  }
  \includeScore "FABrecit"
  \markup\fill-line\italic {
    \line { Io tombe accablée de ses tourments : Jupiter touché de pitié, descend du Ciel. }
  }
}
\bookpart {
  \scene "Scene II" "Scene II"
  \sceneDescription\markup\wordwrap-center { Jupiter, Io, la Furie. }
  %% 5-3
  \pieceToc "Prélude"
  \includeScore "FBAprelude"
}
\bookpart {
  %% 5-4
  \pieceToc\markup\wordwrap {
    Jupiter, Io : \italic { Il ne m’est pas permis de finir vôtre peine }
  }
  \includeScore "FBBrecit"
  \markup\fill-line\italic {
    \line { Junon descend sur la terre. }
  }
  
  \scene "Scene III" "Scene III"
  \sceneDescription\markup\wordwrap-center { Jupiter, Junon, Io, la Furie. }
  %% 5-5
  \pieceToc\markup\wordwrap {
    Jupiter, Junon, Io : \italic { Venez Déesse impitoyable }
  }
  \includeScore "FCArecit" 
  \markup\wordwrap-center\italic {
    Les Divinitez du ciel descendent pour recevoir Isis,
    les Peuples d’Egypte dressent un Autel,
    & la reconnoissent pour la Divinité qui les doit proteger.
  }
}
\bookpart {
  \sceneDescription\markup\wordwrap-center {
    Divinitez qui descendent du Ciel dans la Gloire,
    Peuples d’Egypte chantants. Quatre Egyptiennes chantantes.
    Peuples d’Egypte dançants. Quatre Egyptiennes dançantes.
  }
  %% 5-6
  \pieceToc\markup\wordwrap {
    Chœurs, Jupiter, Junon : \italic { Venez, Divinité nouvelle }
  }
  \includeScore "FCBchoeur"
}
\bookpart {
  %% 5-7
  \pieceToc\markup\wordwrap {
    Premier air pour les Egyptiens. Canaries.
  }
  \includeScore "FCCcanaries"
  %% 5-8
  \pieceToc\markup\wordwrap {
    Deuxième air pour les Egyptiens.
  }
  \includeScore "FCDair"
}
\bookpart {
  %% 5-9
  \pieceToc\markup\wordwrap {
    Chœurs : \italic { Isis est immortelle }
  }
  \includeScore "FCEchoeur"
  \actEnd "FIN DU CINQUIÉME ET DERNIER ACTE"
}
