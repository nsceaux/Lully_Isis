\clef "taille" do'2 do'4. do'8 |
re'4. re'8 mib'4. mib'8 |
fa'2. fa'4 |
sol'2 fa'4. fa'8 |
fa'2 sol'4 re' |
re'2~ re'8 fa' sol' la' |
re'2 do'4. do'8 |
do'2 re'4. re'8 |
si2. mi'4 |
fa' sol' sol'4. sol'8 |
sol'4. sol'8 sol'4. sol'8 |
la'2. fa'4 |
fa'4. fa'8 sol'4. sol'8 |
<< sol'4. \\ \sug mi'4. >> mi'8 fa'4. mi'8 |
mi'2. do'4 |
sol'4 do' do'4. sib8 |
la1\trill |
