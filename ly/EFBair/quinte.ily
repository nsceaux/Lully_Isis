\clef "quinte" la2 fa4. fa8 |
sol la si4 do' do' |
re'2. re'4 |
sol4. do'8 do'4. do'8 |
sib2. re'4 |
<< \sug fa'2 \\ re'2 >> la4. fa8 |
sol2 sol4. sol8 |
fa2 la4 re' |
re'2. do'4 |
re' sol sol4. sol8 |
sol2 do'4. do'8 |
fa'4. fa'8 do'4. do'8 |
re'4. re'8 sib4. sib8 |
do'2 do'4 fa |
sol2. fa4 |
sol4 la8 sib do'4. do'8 |
do'1 |
