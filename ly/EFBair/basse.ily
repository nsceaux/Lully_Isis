\clef "basse"
\tag #'basse-continue \footnoteHere #'(0 . 0) \markup { Matériel 1677 : B.C. tacet. }
fa,2 fa4. fa8 |
fa2 mib |
re2. re4 |
do2 fa,8 fa, sol, la, |
sib,2~ sib,8 la, sib, do |
re2~ re8 re mi fa |
si,2 do8 do re mi |
fa2 fad4. fad8 |
sol2. mi4 |
re do sol sol, |
do2 do'4 sib |
la4. la8 la4. la8 |
sib4. sib8 sol4. sol8 |
do'2 fa |
do2. la,4 |
sol, fa, do do, |
fa,1 |
