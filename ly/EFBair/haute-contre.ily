\clef "haute-contre" la'2 la'4. la'8 |
sol'2 do''4. do''8 |
do''2 sib'4. sib'8 |
sib'2 la'8 la' sib' do'' |
re''2~ re''8 re'' do'' sib' |
la'2\trill la'4. la'8 |
sol'2 sol'4. sol'8 |
la'2 la'4. la'8 |
sol'2. sol'4 |
fa'4 mi' re'4. sol'8 |
mi'4. do''8 do''2 |
do''2 do''4. do''8 |
sib'4. re''8 re''4. re''8 |
<< { mi''4. do''8 } \\ \sugNotes { do''4 do'' } >> do''4. do''8 |
do''2. do''4 |
sib' la' sol'4. fa'8 |
fa'1 |
