\clef "dessus" fa''2~ fa''8 la' si' do'' |
si'4\trill sol' sol''4. sol''8 |
fa''4. fa''8 fa''4. sol''8 |
mi''4.\trill mi''8 la''4. la''8 |
la''4. sib''8 sol''4.\trill sol''8 |
sol''4. fa''8 fa''4. fa''8 |
fa''4. mi''8 mi''4. mi''8 |
mi''2 re''4.\trill re''8 |
re''2. sol''4 |
si'4 do'' do''4.\trill si'8 |
do''4. mi''8 mi''4.\trill mi''8 |
fa''4. do''8 mib''4. fa''8 |
re''4. fa''8 sib''4. sib''8 |
sib''4. la''8 la''4.\trill sol''8 |
sol''2. do'''4 |
mi''4\trill fa'' fa'' mi'' |
fa''1 |
