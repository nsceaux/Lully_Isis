\score {
  <<
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'recit \includeNotes "voix"
      >>  \keepWithTag #'recit \includeLyrics "paroles"
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        \global \keepWithTag #'recit2 \includeNotes "voix"
      >> \keepWithTag #'recit2 \includeLyrics "paroles"
      \new Staff <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
        \origLayout {
          s2.*6 s1\break s2.*2 s1\break s1 s2. s1\break s1 s2.*2\pageBreak
          s2.*3\break s1*2\break s2. s1*3\break s2. s1*2\break s1*3 s2. \bar "" \pageBreak
          s4 s1*3 s2 \bar "" \break
        }
      >>
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
