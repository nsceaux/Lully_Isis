\clef "basse" sol,4. sol8 fad sol |
mi4. mi8 re mi |
do4. do8 si, la, |
re2 sol,4~ |
sol, re,2 |
sol, \once\tieDashed sol4~ |
sol2 fad |
sol4 sol,2 |
do2. |
re1 |
si, |
do4 si,2 |
la, re |
\once\tieDashed sol~ sol4 fa |
mi2. |
fa4 re mi |
dod2. |
re4 si,8. do16 re8 re, |
sol,2~ sol,8 sol |
fad2 mi |
re fad, |
sol,2. | \allowPageTurn
sol2 do4 re |
mi mi, la,4. la,8 |
si,4 si,8 sol, la,2 |
re,4 re si,4 |
do2 si,8 do re4 |
sol,8 sol16 fad sol la si do' re'8 re16 dod re mi fad sol |
la8 sol16 fad mi re do si, la,8 sol, la,4 |
re,8 re16 dod? re mi fad re sol8 sol,16 fad, sol, la, si, do? |
re8 do16 si, la, sol, fad, mi, re,8 do, re,4 |
sol,2 r4 r8 sol |
re4. re8 mi4. mi8 |
do1 |
fa2 re |
mi r8 mi do do |
fa8 re mi mi mi mi, |
la,4. la8 fad fad |
si sol la la la la, |
re1 |
