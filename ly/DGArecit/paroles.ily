\tag #'(recit basse) {
  Re -- con -- nois -- sez Mer -- cure, & fuy -- ez a -- vec nous ;
  E -- loi -- gnez- vous d’Ar -- gus, a -- vant qu’il se ré -- veil -- le.
  
  Ar -- gus a -- vec cent yeux som -- meil -- le ;
  mais, croy -- ez- vous
  en -- dor -- mir un a -- mant ja -- loux ?
  De -- meu -- rez.
  
  Mal- heu -- reux, d’où te vient cét au -- da -- ce ?
  
  J’ay tout per -- du, j’at -- tens le tré -- pas sans ef -- froy,
  un coup de foudre est u -- ne gra -- ce
  pour un mal -- heu -- reux com -- me moy.
  E -- veil -- lez- vous, Ar -- gus,
  é -- veil -- lez- vous, é -- veil -- lez- vous ;
  vous vous lais -- sez sur -- pren -- dre.
}
\tag #'(recit recit2 basse) {
  Puis -- san -- te Rei -- ne des cieux,
  Ju -- non, ve -- nez nous dé -- fen -- dre,
  ve -- nez, ve -- nez nous dé -- fen -- dre.
  
  Com -- men -- cez d’é -- prou -- ver la co -- le -- re des Dieux.
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Fuy -- ons, fuy -- ons, fuy -- ons.
}
\tag #'(recit basse) {
  Vous me quit -- tez, quel se -- cours puis-je at -- ten -- dre ?
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Fuy -- ons, fuy -- ons, Ju -- non vient dans ces Lieux.
  Fuy -- ons, fuy -- ons, Ju -- non vient dans ces Lieux.
}
