<<
  \tag #'(recit basse) {
    \clef "vhaute-contre" <>^\markup\character Mercure
    R2.*5 |
    r4 r16 re' re' re' si8\trill si |
    do'4 do'8 mi' do'4\trill do'8 si |
    si4\trill r16 sol' sol' sol' si8\trill si |
    do'8. mi'16 do'8 do' do' do' |
    la4\trill la8
    \ffclef "vbasse-taille"
    \tag #'recit <>^\markup\character-text Hierax \line { arrêtant Io, & parlant à Mercure }
    \tag #'basse <>^\markup\character Hierax
    re8 la16 la la la fad8 la |
    re4 re re' sol8 sol |
    mi\trill la16 la si8 si16 dod' re'8. mi'16 |
    dod'4\trill r8 la16 la fad4\trill
    \ffclef "vhaute-contre" <>^\markup\character Mercure
    r8 re'16 re' |
    si4\trill si8 si si4 do'8 re' |
    mi'4 mi'8
    \ffclef "vbasse-taille" <>^\markup\character Hierax
    do'8 do' do' |
    la\trill r16 la re'8 si16 si sold8\trill sold16 si |
    mi4 mi'8 dod'16 mi' la la la mi |
    fad8 fad re'16 re' re' mi' la8\trill la16 si |
    sol4 r16 sol sol sol re'8 re' |
    re'4 r16 re' re' re' mi'4 r16 mi' mi' mi' |
    la8 r16 la la8 la re'4 la |
    si4 si8 re' si do' |
    re'4 re'8 re' mi' mi' fa' re' |
    si4\trill si8 mi' dod'4\trill dod'8 r16 dod' |
    re'4 r8 re' re'4 re'8 dod' |
    re'4 re'8
    \ffclef "vhaute-contre" \tag #'basse <>^\markup\character Mercure
    \tag #'recit <>^\markup\character-text Mercure
    \line { frappant Argus & Hierax de son Caducée }
    la16 la si8 do'16 re' |
    mi'4 mi'8 fad' sol'4 sol'8 fad' |
    sol'2 r |
    <<
      \tag #'basse {
        R1*3 s1*2 s2
      }
      \tag #'recit { R1*5 r2 }
    >>
    \ffclef "vdessus" <>^\markup\character Io
    r8 do'' do'' mi'' |
    la'4 r8 la'16 la' re''4 si'8 si' |
    sold'4 sold'
    \tag #'recit { r2 R2.*3 R1 }
  }
  \tag #'recit2 {
    \clef "vbasse-taille" R2.*6 R1 R2.*2 R1*2 R2. R1 R1 R2.*5 R1 R1
    r4 r8 <>^\markup\character Argus re sol la |
    si4 si8 si do' do' la si |
    sold4\trill sold8 sold la4 la8 r16 la |
    fad4\trill r8 si mi4\trill mi8 la |
    fad4\trill fad8 r8 r4 |
    R1 R1*6 R1*3 R2.*3 R1
  }
  \tag #'(vdessus basse) {
    <<
      \tag #'basse {
        s2.*6 s1 s2.*2 s1*2 s2. s1 s1 s2.*5 s1 s1
        s2. s1*3 s2. s1 s1*4 \ffclef "vdessus"
      }
      \tag #'vdessus {
        \clef "vdessus" R2.*6 R1 R2.*2 R1*2 R2. R1 R1 R2.*5 R1 R1
        R2. R1*3 R2. R1 R1*4 |
      }
    >>
    r2 r4 <>^\markup\character Chœurs r8 si' |
    la'4. re''8 si'4.\trill si'8 |
    mi''2 <<
      \tag #'basse { s2 s1 s2 \ffclef "vdessus" }
      \tag #'vdessus { r2 R1 r2 }
    >> <>^\markup\character Chœurs r8 mi'' mi'' mi'' |
    do'' re'' si'\trill si' si' mi'' |
    dod''4. dod''8 dod'' dod'' |
    re'' re'' re'' re'' re'' dod'' |
    re''1 |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" R2.*6 R1 R2.*2 R1*2 R2. R1 R1 R2.*5 R1 R1
    R2. R1*3 R2. R1 R1*4 |
    r2 r4 r8 sol' |
    fad'4.\trill fad'8 sol'4. sol'8 |
    sol'2 r |
    R1 |
    r2 r8 sold' la' la' |
    la' fa' mi' mi' mi' mi' |
    mi'4. mi'8 fad' fad' |
    fad' \footnoteHere#'(0 . 0) \markup {
      Ballard 1719 : \italic fa♯
    } \sug sol' mi' mi' mi' la' |
    fad'1\trill |
  }
  \tag #'vtaille {
    \clef "vtaille" R2.*6 R1 R2.*2 R1*2 R2. R1 R1 R2.*5 R1 R1
    R2. R1*3 R2. R1 R1*4 |
    r2 r4 r8 re' |
    re'4. re'8 sol4. sol8 |
    do'2 r |
    R1 |
    r2 r8 si8 do' do' |
    la la la la la sold |
    la4. la8 la la |
    si si la la la la |
    la1 |
  }
  \tag #'vbasse {
    \clef "vbasse" R2.*6 R1 R2.*2 R1*2 R2. R1 R1 R2.*5 R1 R1
    R2. R1*3 R2. R1 R1*4 |
    r2 r4 r8 sol |
    re4. re8 mi4. mi8 |
    do2 r |
    R1 |
    r2 r8 mi do do |
    fa re mi mi mi mi |
    la,4. la8 fad fad |
    si sol la la la la, |
    re1 |
  }
>>
