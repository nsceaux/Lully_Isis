\clef "dessus" mi''8 |
mi''4. |
mi''8 fa'' mi'' |
re''4\trill re''16 mi'' |
fa''8 fa'' fa'' |
mi''4\trill fa''8 |
sol''4. |
do''8 do'' fa'' |
re''4\trill~ re''16 do'' |
do''4 mi''8 |
re''4 sol''8 |
do'' do'' fa'' |
mi'' mi'' la'' |
fad''4\trill re''8 |
si'4 mi''8 |
do''4 do''8 |
fa''16 sol'' fa'' mi'' re'' do'' |
si'8 si' si' |
mi'' re'' do'' |
si'4~\trill si'16 la' |
la'4 mi''8 |
fa''4 fa''8 |
dod''4\trill re''8 |
mi''4. |
re''8 re''8. mi''16 |
dod''8 re'' re'' |
mi'' mi'' fa'' |
\sugRythme { si'4~ si'16[ si'] }
mi''4\trill re''8 |
re''4 la'8 |
re''4. |
mi''8 fa'' mi'' |
re''4\trill re''16 mi'' |
fa''8\trill fa'' fa'' |
mi''4 fa''8 |
sol''4. |
do''8 do'' fa'' |
re''4\trill~ re''16 do'' |
do''4 mi''8 |
re''4 sol''8 |
do'' do'' fa'' |
mi'' mi'' la'' |
fad''4\trill re''8 |
re''4 do''8 |
do''4 do''8 |
si'4\trill si'8 |
la' la' la' |
re'' do'' si' |
la'4~\trill la'16 sol' |
sol'4 re''8 |
mi''4 fa''8 |
re''4 re''8 |
sol''4 sol''8 |
mi'' mi'' mi'' |
fa'' mi'' fa'' |
fa'' mi''8.\trill fa''16 |
fa''4. |
r8 r la' |
sol'4 la'8 |
sib'4. |
la'8 la'8. sib'16 |
sol'8\trill do'' do'' |
sib' sib' la' |
la'8( sol'8.)\trill fa'16 |
fa'4 do''8 |
fa''4 fa''8 |
re''4 re''8 |
sol''4 sol''8 |
mi'' mi'' mi'' |
re'' re'' do'' |
do''( si'8.)\trill do''16 |
do''4. |
r8 r mi'' |
re''4 mi''8 |
fa''4. |
mi''8 mi''8. fa''16 |
re''8 sol'' sol'' |
fa'' fa'' mi'' |
re''4~\trill re''16 do'' |
do''4 do''8 |
re''4 re''8 |
mi''4 mi''8 |
fa''4 fa''8 |
re''8 re'' re'' |
sol'' fa'' mi'' |
mi'' re''8.\trill do''16 |
do''4. |
