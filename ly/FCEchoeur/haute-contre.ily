\clef "haute-contre" do''8 |
do''4. |
do''8 re'' do'' |
si'4 si'16 do'' |
re''8 re'' re'' |
do''4 re''8 |
mi''4. |
la'8 la' re'' |
si'4\trill~ si'16 do'' |
do''4 do''8 |
si'4 do''8 |
la'16 sol' la' si' do'' re'' |
do''8 do'' do'' |
la'4 la'8 |
sol'4 do''8 |
la'4 la'8 |
re''16 mi'' re'' do'' si' la' |
sold'8 sold' sold' |
la' sold'8. la'16 |
la'8 sold'8.\trill la'16 |
la'4 la'8 |
la'4 la'8 |
la'4 la'8 |
sol'4. |
fa'8 fa'8. sol'16 |
mi'8 la' la' |
la' la' la' |
la'4. |
fad'4\trill fad'8 |
sol'4. |
sol'8 re'' do'' |
si'4 si'16 do'' |
re''8 re'' re'' |
do''4 re''8 |
mi''4. |
la'8 la' re'' |
si'4\trill~ si'16 do'' |
do''4 do''8 |
si'4 do''8 |
la'16 sol' la' si' do'' re'' |
do''8 do'' do'' |
la'4 fad'8 |
sol'4 sol'8 |
la'4 la'8 |
sol'4 sol'8 |
fad' fad' fad' |
sol' fad' sol' |
sol' fad'8.\trill sol'16 |
sol'4 si'8 |
do''4 do''8 |
fa'4 fa'8 |
sib'4 sib'8 |
sol' sol' sol' |
la' sib' la' |
la'( sol'8.\trill) fa'16 |
fa'4. |
r8 r fa' |
mi'4 fa'8 |
sol'4. |
fa'8 fa'8. sol'16 |
mi'8 la' la' |
sol' sol' fa' |
fa'8( mi'8.)\trill fa'16 |
fa'4 la'8 |
re''4 re''8 |
si'4 si'8 |
mi''4 mi''8 |
do'' do'' do'' |
re'' sol' sol' |
sol'4~ sol'16 fa' |
mi'4.\trill |
r8 r do'' |
si'4 do''8 |
re''4. |
do''8 do''8. re''16 |
si'8 mi'' mi'' |
si' si' do'' |
do'' si'8.\trill do''16 |
do''4 la'8 |
la'4 sol'8 |
sol'4 do''8 |
do''4 re''8 |
<< { re'' re'' re'' } \\ \sugNotes { si' si' si' } >> |
do'' si' do'' |
do'' si'8.\trill do''16 |
do''4. |
