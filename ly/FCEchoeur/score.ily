\score {
  <<
    \new StaffGroup <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
      \new Staff << \global \keepWithTag #'basse \includeNotes "basse" >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'choeur2-d \includeNotes "voix"
      >> \keepWithTag #'choeur2-d \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'choeur2-hc \includeNotes "voix"
      >> \keepWithTag #'choeur2-hc \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'choeur2-t \includeNotes "voix"
      >> \keepWithTag #'choeur2-t \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'choeur2-b \includeNotes "voix"
      >> \keepWithTag #'choeur2-b \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
  \midi { }
}
