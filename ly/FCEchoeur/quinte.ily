\clef "quinte" do'8 |
do'4. |
mi'8 re' mi'16 fa' |
sol'4 re'8 |
re'4 re'8 |
mi' do'4 |
do'4. |
do'8 fa fa |
sol4 sol8 |
sol4 do'8 |
re' re' do' |
do'4 do'8 |
do' do' do' |
re'4 re'8 |
re'4 do'8 |
do'4 la8 |
la4 si8 |
si si si |
do' re' mi' |
mi'4. |
mi'4 mi'8 |
re'4 re'8 |
mi' la4 |
la4. |
la4 la8 |
la4 fa'8 |
mi'8 la4 |
la8 la'4 |
la'4 la'8 |
sol'4. |
mi'8 re' sol' |
sol'4 re'8 |
re'4 re'8 |
mi'8 do'4 |
do'4. |
do'8 fa fa |
sol4 sol8 |
sol4 do'8 |
re' re' do' |
do'4 do'8 |
do' do' do' |
re'4 re'8 |
si4 do'8 |
la4 la8 |
si4 sol8 |
la re'4 |
re' re'8 |
re'4. |
re'4 re'8 |
do'4 do'8 |
re'4 re'8 |
sib4 sib8 |
do'4 do'8 |
do'4 do'8 |
do'4. |
do' |
r8 r do' |
do'4.~ |
do'~ |
do'~ |
do'~ |
do'~ |
do' |
do'4 do'8 |
re'4 re'8 |
re'4 re'8 |
do'4. |
do'8 do' do' |
sol'4 sol'8 |
sol'4 sol8 |
sol4. |
r8 r sol' |
sol'4 mi'8 |
re'4. |
<< \sugNotes {  mi'8 do' do' } \\ { sol8  do' do' } >> |
re' mi' mi' |
<< \sugNotes { re' sol re' } \\ { re' sol sol } >> |
sol4 sol8 |
do'4 do'8 |
<< { fa'4 fa'8 } \\ \sugNotes { si4 si8 } >> |
mi'4 do'8 |
la4 re'8 |
re'8 re' re' |
mi' fa' sol' |
sol'4 sol8 |
sol4. |
