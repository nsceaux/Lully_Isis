\clef "basse" do'8 |
do'4. |
do'8 si do' |
sol4 sol8 |
re4 re8 |
la4 la8 |
mi4. |
fa8 fa re |
sol sol,4 |
do4 do8 |
sol sol mi |
fa16 mi fa sol la si |
do'8 do' la |
re'4 re8 |
sol4 mi8 |
fa4 fa8 |
re4 re8 |
mi mi re |
do si, la, |
mi4. |
la,8 la sol |
fa fa re |
<<
  \tag #'(basse bc-part) \new Voice {
    \tag #'bc-part { <>^"[Tous]" \voiceOne }
    la4.~ |
    la~ |
    la~ |
    la~ |
    la |
    la4
  }
  \tag #'(basse-continue bc-part) \new Voice {
    \tag #'bc-part { <>_"[B.C.]" \voiceTwo }
    la4 re8 |
    la,16 si, dod8 la, |
    re16 mi fa8 re |
    la8 fa re |
    la16 sol fa mi re8 |
    la,4
  }
>> \tag #'bc-part <>^"[Tous]" la8 |
re'4 re'8 |
si4. |
do'8 si do' |
sol4 sol8 |
re4 re8 |
la4 la8 |
mi4. |
fa8 fa re |
sol sol,4 |
do4 do8 |
sol sol mi |
fa16 mi fa sol la si |
do'8 do' la |
re'4 re8 |
mi4 mi8 |
fad4 fad8 |
sol4 sol8 |
re re' do' |
si la sol |
re4. |
sol4 sol8 |
do'4 la8 |
sib4 sib8 |
sol4 sol8 |
do' do' sib |
la sol fa |
do4. |
fa8 fa sol |
la la fa |
do'4 fa8 |
do16 re mi8 do |
fa16 sol la8 fa |
do'16 sib la sol fa8 |
do16 re mi do fa8 |
do'4 do8 |
fa4 fa8 |
re4 re8 |
sol4 sol8 |
mi4 mi8 |
la la la |
si si do' |
sol sol,4 |
do8 do re |
mi mi do |
<<
  \tag #'(basse bc-part) \new Voice {
    \tag #'bc-part { <>^"[Tous]" \voiceOne }
    sol4.~ |
    sol~ |
    sol~ |
    sol~ |
    sol |
    sol4
  }
  \tag #'(basse-continue bc-part) \new Voice {
    \tag #'bc-part { <>_"[B.C.]" \voiceTwo }
    sol4 do8 |
    sol,16 fa, sol, la, si, sol, |
    do re mi8 do |
    sol mi do |
    sol16 fa mi re do8 |
    sol,4
  }
>> \tag #'bc-part <>^"[Tous]" sol8 |
la4 la8 |
si4 si8 |
do'4 do8 |
fa4 re8 |
sol8 sol fa |
mi re do |
sol sol,4 |
do4. |
