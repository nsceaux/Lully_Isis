<<
  %% Chœur des Egyptiens
  \tag #'(choeur2-d basse) {
    \clef "vdessus" <>^\markup\character Chœurs mi''8 |
    mi''4. |
    mi''8 fa'' mi'' |
    re''4\trill re''8 |
    r r fa'' |
    mi''4\trill fa''8 |
    sol''4. |
    do''8 do'' fa'' |
    re''4.\trill |
    do''4 mi''8 |
    re''4\trill sol''8 |
    do'' do'' fa'' |
    mi'' mi'' la'' |
    fad''4\trill re''8 |
    si'4\trill mi''8 |
    do''4 do''8 |
    fa''16[ sol'' fa'' mi''] re''[ do''] |
    si'8\trill si' si' |
    mi'' re'' do'' |
    si'4.\trill |
    la'4 mi''8 |
    fa''4 fa''8 |
    dod''4\trill re''8 |
    mi''4. |
    re''8 re''8. mi''16 |
    dod''8 re'' re'' |
    mi'' mi'' fa'' |
    mi''4.\trill |
    re''4 la'8 |
    re''4. |
    mi''8 fa'' mi'' |
    re''4\trill re''8 |
    r r fa'' |
    mi''4\trill fa''8 |
    sol''4. |
    do''8 do'' fa'' |
    re''4.\trill |
    do''4 mi''8 |
    re''4\trill sol''8 |
    do'' do'' fa'' |
    mi''\trill mi'' la'' |
    fad''4\trill re''8 |
    re''4 do''8 |
    do''4 do''8 |
    si'4 si'8 |
    la'\trill la' la' |
    re'' do'' si' |
    la'4.\trill |
    sol'4 re''8 |
    mi''4 fa''8 |
    re''4 re''8 |
    sol''4 sol''8 |
    mi'' mi'' mi'' |
    fa'' mi'' fa'' |
    fa''( mi''4)\trill |
    fa''4. |
    r8 r la' |
    sol'4\trill la'8 |
    sib'4. |
    la'8 la'8. sib'16 |
    sol'8\trill do'' do'' |
    sib'\trill sib' la' |
    la'8( sol'4\trill) |
    fa'4 do''8 |
    fa''4 fa''8 |
    re''4\trill re''8 |
    sol''4 sol''8 |
    mi''\trill mi'' do'' |
    re'' re'' do'' |
    do''( si'4\trill) |
    do''4. |
    r8 r mi'' |
    re''4\trill mi''8 |
    fa''4. |
    mi''8 mi''8. fa''16 |
    re''8\trill sol'' sol'' |
    fa''\trill fa'' mi'' |
    mi''( re''4)\trill |
    do''4 do''8 |
    re''4 re''8 |
    mi''4 mi''8 |
    fa''4 fa''8 |
    re''8\trill re'' re'' |
    sol'' fa'' mi'' |
    mi''( re''4)\trill |
    do''4. |
  }
  \tag #'choeur2-hc {
    \clef "vhaute-contre" sol'8 |
    sol'4. |
    sol'8 sol' sol' |
    sol'4 sol'8 |
    r r la' |
    la'4 la'8 |
    sol'4. |
    fa'8 la' la' |
    sol'4. |
    mi'4 sol'8 |
    sol'4 sol'8 |
    la' la' la' |
    sol' sol' la' |
    la'4 fad'8 |
    sol'4 sol'8 |
    fa'4 fa'8 |
    << { \voiceOne la'4 la'8 \oneVoice } \new Voice \sugNotes { \voiceTwo fa'4 fa'8 } >> |
    mi' mi' mi' |
    mi' mi' mi' |
    mi'4. |
    dod'4 dod'8 |
    re'4 re'8 |
    mi'4 fa'8 |
    sol'4. |
    fa'8 fa'8. sol'16 |
    mi'8\trill la' la' |
    la' la' la' |
    la'4. |
    fad'4 fad'8 |
    sol'4. |
    sol'8 sol' sol' |
    sol'4 sol'8 |
    r r la' |
    la'4 la'8 |
    sol'4. |
    fa'8 la' la' |
    sol'4. |
    mi'4 sol'8 |
    sol'4 sol'8 |
    la' la' la' |
    sol' sol' la' |
    la'4 fad'8 |
    sol'4 sol'8 |
    la'4 la'8 |
    sol'4 sol'8 |
    fad' fad' fad' |
    sol' fad' sol' |
    sol'( fad'4) |
    sol'4 sol'8 |
    sol'4 la'8 |
    fa'4 fa'8 |
    sib'4 sib'8 |
    sol' sol' sol' |
    la'8 sib' la' |
    la'( sol'4)\trill |
    fa'4. |
    r8 r fa' |
    mi'4 fa'8 |
    sol'4. |
    fa'8 fa'8. sol'16 |
    mi'8 la' la' |
    sol' sol' fa' |
    fa'( mi'4)\trill |
    fa' la'8 |
    la'4 la'8 |
    sol'4 sol'8 |
    sol'4 sol'8 |
    la' la' la' |
    sol' sol' sol' |
    sol'4. |
    mi' |
    r8 r sol' |
    sol'4 do'8 |
    si4.\trill |
    do'8 sol' sol' |
    sol' do' do' |
    re' re' mi' |
    sol'8.[ fa'16 mi' fa'] |
    mi'4\trill la'8 |
    la'4 sol'8 |
    sol'4 sol'8 |
    la'4 la'8 |
    sol'8 sol' sol' |
    sol' sol' sol' |
    sol'4. |
    mi' |
  }
  \tag #'choeur2-t {
    \clef "vtaille" do'8 |
    do'4. |
    mi'8 re' do' |
    si4\trill si8 |
    r r re' |
    do'4 re'8 |
    mi'4. |
    la8 la re' |
    si4.\trill |
    do'4 do'8 |
    si si do' |
    la16[ sol la si do' re']( |
    mi'8) do' do' |
    re'4 re'8 |
    re'4 do'8 |
    la4 la8 |
    re'16[ mi' re' do'] si[ la] |
    sold8\trill sold sold |
    la sold la |
    la( sold4) |
    la4 la8 |
    re'4 fa'8 |
    mi'4 re'8 |
    dod'4. |
    re'8 re' re' |
    la fa' fa' |
    dod' dod' re' |
    re'4( dod'8) |
    re'4 re'8 |
    re'4. |
    do'8 re' do' |
    si4\trill si8 |
    r r re' |
    do'4 re'8 |
    mi'4. |
    la8 la re' |
    si4.\trill |
    do'4 do'8 |
    si si do' |
    la16[ sol la si do' re']( |
    mi'8) do' do' |
    re'4 re'8 |
    sol4 do'8 |
    la4 re'8 |
    re'4 re'8 |
    re' re' re' |
    re' re' re' |
    re'4. |
    si4 si8 |
    do'4 do'8 |
    re'4 re'8 |
    re'4 re'8 |
    do' do' do' |
    do' do' do' |
    do'4. |
    la |
    r8 r do' |
    do'4 fa'8 |
    mi'4.\trill |
    fa'8 fa' fa' |
    do' do' fa' |
    mi' mi' fa' |
    do'4. |
    la4 la8 |
    re'4 re'8 |
    si4 si8 |
    mi'4 mi'8 |
    do' do' << { \voiceOne mi' \oneVoice } \new Voice { \voiceTwo \sug la } >> |
    re' re' mi' |
    mi'( re'4)\trill |
    do'4. |
    r8 r do' |
    si4 do'8 |
    re'4. |
    do'8 do'8. re'16 |
    si8\trill mi' mi' |
    si si do' |
    do'( si4)\trill |
    do'4 mi'8 |
    re'4 re'8 |
    do'4 do'8 |
    do'4 re'8 |
    si8 si si |
    do' si do' |
    do'( si4) |
    do'4. |
  }
  \tag #'choeur2-b {
    \clef "vbasse" do'8 |
    do'4. |
    do'8 si do' |
    sol4 sol8 |
    r r re |
    la4 la8 |
    mi4. |
    fa8 fa re |
    sol( sol,4) |
    do4 do8 |
    sol sol mi |
    fa16[ mi fa sol la si]( |
    do'8) do' la |
    re'4 re8 |
    sol4 mi8 |
    fa4 fa8 |
    re4 re8 |
    mi mi re |
    do si, la, |
    mi4. |
    la,8 la sol |
    fa fa re |
    la4.~ |
    la~ |
    la~ |
    la~ |
    la |
    la4 la8 |
    re'4 re'8 |
    si4. |
    do'8 si do' |
    sol4 sol8 |
    r r re |
    la4 la8 |
    mi4. |
    fa8 fa re |
    sol8( sol,4) |
    do4 do8 |
    sol sol mi |
    fa16[ mi fa sol la si]( |
    do'8) do' la |
    re'4 re8 |
    mi4 mi8 |
    fad4 fad8 |
    sol8[ fad] sol |
    re re' do' |
    si la sol |
    re4. |
    sol4 sol8 |
    do'4 la8 |
    sib4 sib8 |
    sol4\trill sol8 |
    do' do' sib |
    la sol fa |
    do4. |
    fa8 fa sol |
    la la fa |
    do'4.~ |
    do'~ |
    do'~ |
    do'~ |
    do' |
    do'4 do8 |
    fa4 fa8 |
    re4 re8 |
    sol4 sol8 |
    mi4 mi8 |
    la la la |
    si si do' |
    sol8( sol,4) |
    do8 do re |
    mi mi do |
    sol4.~ |
    sol~ |
    sol~ |
    sol~ |
    sol |
    sol4 sol8 |
    la4 la8 |
    si4 si8 |
    do'4 do8 |
    fa4 re8 |
    sol8 sol fa |
    mi re do |
    sol( sol,4) |
    do4. |
  }
>>
