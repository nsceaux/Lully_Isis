\score {
  \new StaffGroup <<
    \new Staff \with { \haraKiriFirst } <<
      \global \keepWithTag #'basse \includeNotes "basse"
    >>
    \new Staff \with { \tinyStaff } <<
      <>^\markup\whiteout B.C.
      \global \keepWithTag #'basse-continue \includeNotes "basse"
    >>
  >>
  \layout { }
}
