\clef "taille" sol'8 |
sol'4. |
sol'8 sol' sol' |
sol'4 sol'8 |
la' la' la' |
la'4 la'8 |
sol'4. |
fa'8 la' la' |
sol'4~ sol'16 fa' |
mi'4 sol'8 |
sol'4 sol'8 |
fa' la' la' |
sol' sol' la' |
la'4 fad'8 |
sol'4 sol'8 |
fa'4 fa'8 |
fa'4 fa'8 |
mi' mi' mi' |
mi'4 mi'8 |
mi'4 mi'16 re' |
dod'4 dod'8 |
re'4 fa'8 |
mi'4 re'8 |
dod'4. |
re'8 re' re' |
mi' fa' fa' |
dod' dod' re' |
re' dod'8.\trill re'16 |
re'4 re'8 |
re'4. |
do'8 re' mi'16 fa' |
sol'4 sol'8 |
la' la' la' |
la'4 la'8 |
sol'4. |
fa'8 la' la' |
sol'4~ sol'16 fa' |
mi'4 sol'8 |
sol'4 sol'8 |
fa' la' la' |
sol' sol' la' |
la'4 la'8 |
sol'4 mi'8 |
re'4 re'8 |
re'4 re'8 |
re'4 la'8 |
sol' la' re' |
re'4 re'16 do' |
si4 sol'8 |
sol'4 << sol'8 \\ \sug fa' >> |
fa'4 fa'8 |
sol'4 sol'8 |
sol'4 << \sug sol'8 \\ do'8 >> |
fa' sol' do' |
do'4 do'16 sib |
la4. |
r8 r do'8 |
do'4 fa'8 |
mi'4. |
fa'8 fa' fa' |
do' do' fa' |
mi' mi' fa' |
do'4~ do'16 sib |
la4 la'8 |
la'4 la'8 |
sol'4 sol'8 |
sol'4 sol'8 |
la' la' la' |
re' re' mi' |
re'4\trill~ re'16 do' |
do'4. |
r8 r do' |
re'4 do'8 |
si4. |
do'8 sol' sol' |
sol' do' do' |
re' re' mi' |
sol'4 sol'8 |
mi'4 mi'8 |
re'4 re'8 |
do'4 sol'8 |
la'4 la'8 |
sol'8 sol' sol' |
sol'4 sol'8 |
sol'4 sol'16 fa' |
mi'4. |
