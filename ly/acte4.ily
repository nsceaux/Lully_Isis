\bookpart {
  \act "Acte Quatriéme"
  \sceneDescription\markup\wordwrap-center {
    Le Théâtre change, & représente l’endroit le plus glacé de la Scythie.
  }
  \scene "Scene Premiere" "Scene I"
  \sceneDescription\markup\wordwrap-center {
    Des Peuples paroissent transis de froid.
  }
  %% 4-1
  \pieceToc\markup\wordwrap { [Entrée des Peuples des Climats glacez] }
  \includeScore "EAAentree"
  %% 4-2
  \pieceToc\markup\wordwrap {
    Chœur : \italic { L’hyver qui nous tourmente }
  }
  \includeScore "EABchoeur"
}
\bookpart {
  \scene "Scene II" "Scene II"
  \sceneDescription\markup\wordwrap-center {
    Io, la Furie, les Peuples des climats glacez.
  }
  %% 4-3
  \pieceToc\markup\wordwrap {
    Io, la Furie, chœur : \italic { Laissez-moy, Cruelle Furie }
  }
  \includeScore "EBArecit"
  %% 4-4
  \pieceToc\markup\wordwrap { [Entrée des Peuples des Climats glacez] }
  \markup\wordwrap {
    [Matériel 1677]
    \italic { On jouë encore une fois l’Entrée des Peuples des Climats glacez. }
  }
  \includeScore "EBCentree"
}
\bookpart {
  \scene "Scene III" "Scene III"
  \sceneDescription\markup\wordwrap-center {
    Le Théâtre change, & représente des deux côtez les Forges des Chalybes
    qui travaillent à forger l’Acier ; la Mer paroît dans l’enfoncement.
  }
  %% 4-5
  \pieceToc\markup\wordwrap {
    Chœur des Chalybes : \italic { Que le feu des forges s’allume }
  }
  \includeScore "ECAchoeur"
}
\bookpart {
  %% 4-6
  \pieceToc\markup\wordwrap { Entrée des Forgerons }
  \includeScore "ECBentree"
}
\bookpart {
  \scene "Scene IV" "Scene IV"
  \sceneDescription\markup\wordwrap-center {
    Io, la Furie, les conducteurs de Chalybes & leur Suite.
  }
  %% 4-7
  \pieceToc\markup\wordwrap {
    Io, la Furie, chœur : \italic { Quel déluge de feux vient sur moy se répandre }
  }
  \includeScore "EDAchoeur"
}
\bookpart {
  \scene "Scene V" "Scene V"
  \sceneDescription\markup\column {
    \wordwrap-center {
      Le Théâtre change, & représente l’Antre des Parques.
    }
    \wordwrap-center {
      Suite des Parques. La Guerre. Les Fureurs de la Guerre.
      Les maladies violentes & languissantes.
      La Famine. L’Incendie. L’Inondation, &c. Chantants, Dançants.
    }
  }
  %% 4-8
  \pieceToc\markup\wordwrap {
    Chœur : \italic { Executons l’arrest du sort }
  }
  \includeScore "EEAchoeur"
}
\bookpart {
  \scene "Scene VI" "Scene VI"
  \sceneDescription\markup\wordwrap-center {
    Io, la Furie, la Suite des Parques.
  }
  %% 4-9
  \pieceToc\markup\wordwrap { Premier air des Parques }
  \includeScore "EFAair"
  %% 4-10
  \pieceToc\markup\wordwrap { Deuxiéme air des Parques }
  \includeScore "EFBair"
  %% 4-11
  \pieceToc\markup\wordwrap {
    Io, chœur : \italic { C’est contre moy qu’il faut tourner }
  }
  \includeScore "EFCrecit"
  \markup\italic\fill-line {
    \line { Le fond de l’Antre s’ouvre, & les trois Parques en sortent. }
  }
}
\bookpart {
  \scene "Scene VII" "Scene VII"
  \sceneDescription\markup\wordwrap-center {
    Les Trois Parques, Io, la Furie, Suite des Parques.
  }
  %% 4-12
  \pieceToc "Ritournelle"
  \includeScore "EGAritournelle"
  %% 4-13
  \pieceToc\markup\wordwrap {
    Les Trois Parques, Io, la Furie : \italic { Le fil de la vie }
  }
  \includeScore "EGBchoeur"
  \actEnd FIN DU QUATRIÈME ACTE
}
\bookpart {
  %% 4-14
  \pieceToc "Entr’acte"
  \reIncludeScore "DEBair" "EGCentracte"
}