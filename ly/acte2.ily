\bookpart {
  \act "Acte Deuxiéme"
  \sceneDescription\markup\wordwrap-center {
    Le Théatre devient obscurcy par des Nuages épais
    qui l’environnent de tous côtez.
  }
  \scene "Scene Premiere" "Scene I"
  \sceneDescription\markup\wordwrap-center { Io }
  %% 2-1
  \pieceToc "Ritournelle"
  \includeScore "CAAritournelle"
  %% 2-2
  \pieceToc\markup\wordwrap {
    Io : \italic { Où suis-je, d’où vient ce Nuage }
  }
  \includeScore "CABrecit"
}
\bookpart {
  \scene "Scene II" "Scene II"
  \sceneDescription\markup\wordwrap-center {
    Jupiter paroît, & les Nuages qui obscurcissent le Théatre, sont illuminez
    & peints de couleurs les plus brillantes & les plus agréables.
  }
  \sceneDescription\markup\wordwrap-center { Jupiter, Io }
  %% 2-3
  \pieceToc\markup\wordwrap {
    Jupiter, Io : \italic { Vous voyez Jupiter, que rien ne vous étonne }
  }
  \includeScore "CBArecit"
}
\bookpart {
  \scene "Scene III" "Scene III"
  \sceneDescription\markup\wordwrap-center {
    Mercure, Jupiter.
  }
  %% 2-4
  \pieceToc\markup\wordwrap {
    Mercure, Jupiter : \italic { Iris est icy-bas, & Junon elle-mesme }
  }
  \includeScore "CCArecit"

  \scene "Scene IV" "Scene IV"
  \sceneDescription\markup\wordwrap-center {
    Mercure, Iris.
  }
  %% 2-5
  \pieceToc\markup\wordwrap {
    Mercure, Iris : \italic { Arrestez, belle Iris, différez un moment }
  }
  \includeScore "CDArecit"
  \markup\wordwrap-center\italic {
    Le Nuage s’approche de Terre, & Junon descend.
  }
}
\bookpart {
  \scene "Scene V" "Scene V"
  \sceneDescription\markup\wordwrap-center { Junon, Iris. }
  %% 2-6
  \pieceToc\markup Prélude
  \includeScore "CEAprelude"
}
\bookpart {
  %% 2-7
  \pieceToc\markup\wordwrap {
    Iris, Junon : \italic { J’ay cherché vainement le Fille d’Inachus }
  }
  \includeScore "CEBrecit"
}
\bookpart {
  \scene "Scene VI" "Scene VI"
  \sceneDescription\markup\wordwrap-center {
    Jupiter, Junon, Mercure, Iris.
  }
  %% 2-8
  \pieceToc\markup\wordwrap {
    Récit : \italic { Dans les Jardins d’Hébé vous deviez en ce jour }
  }
  \includeScore "CFArecit"
}
\bookpart {
  \scene "Scene VII" "Scene VII"
  \sceneDescription\markup\column {
    \wordwrap-center {
      Le Théatre change, & représente les Jardins d’Hebé, Déesse de la Jeunesse.
    }
    \wordwrap-center {
      Hebé, Troupe de Jeux & de Plaisirs, Troupe de Nymphes de la suite de Junon.
      Les Jeux & les Plaisirs s’avancent en dansant devant la Déesse Hebé.
    }
  }
  %% 2-9
  \pieceToc\markup\wordwrap { Entrée pour le Jeunesse }
  \includeScore "CGAentree"
}
\bookpart {
  %% 2-10
  \pieceToc\markup\wordwrap {
    Hebé, chœur : \italic { Les Plaisirs les plus doux }
  }
  \includeScore "CGBchoeur"
}
\bookpart {
  \sceneDescription\markup\wordwrap-center {
    Les Jeux, les Plaisirs & les Nymphes de Junon se divertissent par des Danses
    & par des Chansons, en attendant la nouvelle Nymphe dont Junon veut faire choix.
  }
  %% 2-11
  \pieceToc\markup\wordwrap { Premier Air }
  \includeScore "CGCair"
}
\bookpart {
  %% 2-12
  \pieceToc\markup\wordwrap {
    Deux Nymphes : \italic { Aymez, profitez du temps }
  }
  \includeScore "CGDnymphes"
}
\bookpart {
  %% 2-13
  \pieceToc\markup\wordwrap { Deuxième Air }
  \includeScore "CGEair"
}
\bookpart {
  \paper { systems-per-page = 1 }
  %% 2-14
  \pieceToc\markup\wordwrap {
    Chœur : \italic { Que ces Lieux ont d’attraits }
  }
  \includeScore "CGFchoeur"
}
\bookpart {
  \scene "Scene VIII" "Scene VIII"
  \sceneDescription\markup\wordwrap-center {
    Io, Mercure, Iris, Hebé, les Jeux, les Plaisirs,
    Troupe de Nymphes de la Suite de Junon.
  }
  %% 2-15
  \pieceToc\markup\wordwrap {
    Mercure, Iris, Hebé, chœur : \italic {
      Servez, Nymphe, servez, avec un soin fidelle
    }
  }
  \includeScore "CHAchoeur"
  \actEnd FIN DU DEUXIÈME ACTE
}
