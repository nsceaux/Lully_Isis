\clef "haute-contre" sol'4 sol' sol' |
fad'2\trill la'4 |
sol' sol' sol' |
mi' mi' la' |
fad' fad' fad' |
sol'4. sol'8 sol'4 |
mi'4. mi'8 la'4 |
fad'4. sol'8 mi'4 |
fad'2. |
la'4 la' si' |
sol'2 fad'4 |
fad'4. fad'8 sol' la' |
si'2 si'4 |
si' si' si' |
la'2 la'4 |
fad' fad' sol' |
mi'4. mi'8 la'4 |
fad'2.\trill |
si'4 si' si' |
la'2 la'4 |
sol' sol' sol' |
fad'\trill fad' sol' |
mi' mi' la' |
la'2 sol'4 |
sol' sol' sol' |
sol'4. sol'8 fad'4\trill |
sol'2. |
