\piecePartSpecs
#`((dessus #:system-count 3)
   (haute-contre)
   (taille)
   (quinte)
   (basse)
   (basse-continue)
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
