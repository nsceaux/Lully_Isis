<<
  \tag #'vdessus {
    \clef "vdessus" si'4 si' si' |
    la'2\trill re''4 |
    si'\trill si' re'' |
    sol' sol' do'' |
    la'\trill la' re'' |
    si'4.\trill si'8 mi''4 |
    dod''4. dod''8 re''4 |
    re''4. re''8 dod''4\trill |
    re''2. |
    fad''4 fad'' sol'' |
    mi''2 fad''4 |
    red''4.\trill red''8 mi''4 |
    mi''4. mi''8 red''4 |
    mi'' mi'' mi'' |
    dod''2 fad''4 |
    re'' re'' re'' |
    re''4. re''8 dod''4 |
    re''2. |
    re''4 re'' mi'' |
    mi''2 re''4 |
    re'' do'' do'' |
    do'' si' si' |
    si' la' la' |
    re''2 re''4 |
    si' si' mi'' |
    la'4. la'8 re''4 |
    si'2.\trill |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" sol'4 sol' sol' |
    fad'2\trill fad'4 |
    sol' sol' sol' |
    mi' mi' la' |
    fad'\trill fad' fad' |
    sol'4. sol'8 sol'4 |
    mi'4. mi'8 la'4 |
    fad'4. sol'8 mi'4 |
    fad'2. |
    la'4 la' sol' |
    sol'2 la'4 |
    fad'4.\trill fad'8 sol'4 |
    fad'4.\trill \voiceOne << fad'8 \new Voice { \voiceTwo \sug mi' } >> \oneVoice fad'4 |
    sol' sol' sol' |
    mi'2\trill la'4 |
    fad' fad' sol' |
    mi'4. mi'8 la'4 |
    fad'2.\trill |
    sol'4 sol' sol' |
    la'2 la'4 |
    sol' sol' sol' |
    fad' fad' sol' |
    mi'\trill mi' mi' |
    fad'2 sol'4 |
    sol'4 sol' sol' |
    sol'4. sol'8 fad'4\trill |
    sol'2. |
  }
  \tag #'vtaille {
    \clef "vtaille" re'4 re' re' |
    re'2 la4 |
    sol mi' \voiceOne << mi' \new Voice { \voiceTwo \sug re' } >> \oneVoice |
    do' do' mi' |
    re' re' la |
    sol4. sol8 sol4 |
    la4. la8 la4 |
    si4. si8 la4 |
    la2. |
    re'4 re' re' |
    do'2 do'4 |
    si4. si8 si4 |
    si4. si8 si4 |
    si si si |
    la2 la4 |
    si4 si si |
    la4. la8 la4 |
    la2. |
    si4 si si |
    la2 la4 |
    si do' do' |
    re' re' re' |
    sol la la |
    la2 sol4 |
    sol mi' mi' |
    re'4. re'8 re'4 |
    re'2. |
  }
  \tag #'vbasse {
    \clef "vbasse" sol4 sol sol |
    re2 re4 |
    mi mi si, |
    do do la, |
    re re re |
    sol4. fad8 mi4 |
    la4. sol8 fad4 |
    si4. sol8 la4 |
    re2. |
    re'4 re' si |
    do'2 la4 |
    si4. si8 mi4 |
    si,4. si,8 si,4 |
    mi4 mi mi |
    la2 fad4 |
    si si sol |
    la4. la8 la,4 |
    re2. |
    sol4 sol sol |
    fad2 fad4 |
    mi mi mi |
    re re re |
    do do do |
    si,2 si,4 |
    mi mi do |
    re4. re8 re4 |
    sol,2. |
  }
>>
\stopStaff
