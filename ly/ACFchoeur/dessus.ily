\clef "dessus" si'4 si' si' |
la'2 re''4 |
si'\trill si' re'' |
sol' sol' do'' |
la' la' re'' |
si'4.\trill si'8 mi''4 |
dod''4. dod''8 re''4 |
re''4. re''8 dod''4\trill |
re''2. |
fad''4 fad'' sol'' |
mi''2 fad''4 |
red''4.\trill red''8 mi''4 |
mi''4. mi''8 red''4 |
mi'' mi'' mi'' |
dod''2 fad''4 |
re'' re'' re'' |
re''4. re''8 dod''4\trill |
re''2. |
re''4 re'' mi'' |
mi''2 re''4 |
re'' do'' do'' |
do'' si' si' |
si' la' la' |
re''2 re''4 |
si' si' mi'' |
la'4. la'8 re''4 |
si'2.\trill |
