\clef "basse" sol4 sol sol |
re2 re4 |
mi mi si, |
do do la, |
re re re |
sol4. fad8 mi4 |
la4. sol8 fad4 |
si4. sol8 la4 |
re2. |
re'4 re' si |
do'2 la4 |
si4. si8 mi4 |
si,4. si,8 si,4 |
mi mi mi |
la2 fad4 |
si si sol |
la2 la,4 |
re2. |
sol4 sol sol |
fad2 fad4 |
mi mi mi |
re re re |
do do do |
si,2 si,4 |
mi mi do |
re2 re,4 |
<<
  \tag #'basse {
    sol,2. |
  }
  \tag #'basse-continue {
    \alternatives {
      sol,2. |
    } {
      \once\override Staff.TimeSignature.stencil = #ly:time-signature::print
      \digitTime\time 2/2
      sol,2 sol8 fad mi re |
    }
    do2 do4 do |
    do2
  }
>>
