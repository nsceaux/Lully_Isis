\clef "taille" re'4 re' re' |
re'2 fad'4 |
mi' mi' re' |
do' mi' mi' |
re' re' re' |
re'4. re'8 mi'4 mi'4. mi'8 fad'4 |
fad'4. mi'8 mi'4 |
re'2. |
re'4 la' sol' |
sol'2 la'4 |
la'4. la'8 sol'4 |
fad'4. << fad'8 \\ \sug mi'8 >> fad'4 |
sol' sol' sol' |
mi'2 re'4 |
re' fad' mi' |
mi'4. mi'8 mi'4 |
re'2. |
sol4 sol sol |
la2 la4 |
si do' do' |
re' re' re' |
sol la la |
si2 si4 |
si mi' mi' |
re'4. re'8 re'4 |
re'2. |
\stopStaff