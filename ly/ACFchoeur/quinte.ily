\clef "quinte" sol sol sol |
la2 la4 |
sol sol sol |
sol sol la |
la la la |
sol4. si8 si4 |
la4. la8 la4 |
si4. si8 la4 |
la2. |
re'4 re' re' |
do'2 do'4 |
si4. si8 si4 |
si4. si8 si4 |
sol sol sol |
la2 la4 |
si si si |
la4. la8 la4 |
la2. |
sol4 sol' mi' |
fad'2 fad'4 |
sol' sol sol |
la si si |
do' do' do' |
re'2 re'4 |
sol sol la |
la4. la8 la4\trill |
sol2. |
