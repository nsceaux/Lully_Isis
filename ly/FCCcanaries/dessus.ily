\clef "dessus"
\setMusic #'rondeau {
  sol''8[ mi''] |
  fa'' fa'' re'' |
  mi''8 fa''16 mi'' re'' do'' |
  re''8 sol'' mi'' |
  fa'' fa'' re'' |
  mi''8. fa''16 fa''8\trill~ |
  fa''16 mi'' re''8.\trill do''16 |
  do''8
}
\keepWithTag #'() \rondeau sol''8 mi'' |
do'' mi''8 do'' |
fa'' fa''16 mi'' re'' do'' |
si'8.\trill si'16 mi''8 |
re''16 do'' si'8.\trill la'16 |
la'8 la''8. la''16 sol''8. la''16 fa''8 |
mi''8.\trill mi''16 fad''8 |
sol'' do''8. re''16 |
si'8\trill
\rondeau
mi''8[ fa''] |
re'' re'' mi'' |
dod''\trill la' la'' |
sol''16 fa'' mi''8.\trill re''16 |
re''8 fa''8. fa''16 |
fad''4 fad''16 fad'' |
sol''8. sol''16 la''8 |
si''8 la''8.\trill sol''16 |
sol''8
\rondeau
