\clef "basse"
\setMusic #'rondeau {
  do'4 si8 |
  do'4 do8 |
  sol mi la |
  re4 sol8 |
  do4 fa,8~ |
  fa, sol,4 |
}
\footnoteHere #'(0 . 0) \markup { Matériel 1677 : B.C. tacet. }
do8 do' |
\keepWithTag #'() \rondeau |
do8 do do' |
do4. |
re |
mi4 do8 |
re mi mi, |
la,4 la8 |
si4. |
do'8. do'16 do'8 |
si la4\trill |
sol4 do'8 |
\rondeau
do8 do'[ la] |
sib sib sol |
la la fa |
sol la la, |
re4 re'8 |
do'4. |
si4 la8 |
sol re' re |
sol do[ do'] |
\rondeau
do8
