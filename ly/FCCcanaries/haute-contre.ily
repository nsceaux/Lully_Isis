\clef "haute-contre"
\setMusic #'rondeau {
  re''4 re''8 |
  sol'4 la'8 |
  si' si' dod'' |
  re'' re'' si' |
  do'' re'' re''\trill~ |
  re''16 do'' si'8.\trill do''16 |
  do''8
}
mi''8 do'' |
\keepWithTag #'() \rondeau mi''8 do'' |
do''8 do''8 do'' |
do'' re''16 do'' si' la' |
sold'4 la'8 |
la'8 sold'8.\trill la'16 |
la'8 do''^\markup\croche-pointee-double do'' |
re''4 re''8 |
do''8. do''16 la'8 |
sol'8 sol' fad' |
sol' si'[ do''] |
\rondeau
do''8[ do''] |
sib' sib' sib' |
la'4 re''8 |
re''8 dod''8.\trill re''16 |
re''8 re'' re'' |
la'4 la'16 la' |
si'8. si'16 do''8 |
re''8 re''8.\trill do''16 |
si'8\trill << { mi''[ do''] } \\ \sugNotes { si'[ do''] } >> |
\rondeau
