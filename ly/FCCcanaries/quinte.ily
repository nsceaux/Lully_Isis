\clef "quinte"
\setMusic #'rondeau {
  re'4 re'8 |
  do'4 do'8 |
  si si la |
  la4 si8 |
  sol do' la |
  re'4 sol8 |
  sol
}
do'8 mi' |
\keepWithTag #'() \rondeau do'8 mi' |
sol8 sol8 do' |
la la si |
si8. si16 do'8 |
si si4 |
do'8 do' la |
re'4 re'8 |
mi'8. mi'16 do'8 |
re' do'8. do'16 |
re'8 re'[ mi'] |
\rondeau
do'8[ do'] |
re' fa' mi' |
mi' mi' la |
sib la8. la16 |
la8 re' re' |
re'4 re'8 |
<<
  { re'4 re'8 | re'8. re'16 do'8 | \voiceTwo si8 do'[ mi'] | } \\
  \sugNotes { re'8. re'16 do'8 | si re'8. re'16 | \voiceOne re'8 re'[ mi'] | }
>>
\rondeau
