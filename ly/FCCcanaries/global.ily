\key do \major \beginMark "Rondeau"
\midiTempo#100
\time 3/8 \partial 4 s4 \bar ".!:" s4.*6 \alternatives s4. s4.
s4.*7 s8
\beginMarkSmall "Rondeau" s4 s4.*6 s8
s4 s4.*7 s8
\beginMarkSmall "Rondeau" s4 s4.*6 s8 \bar "|."
