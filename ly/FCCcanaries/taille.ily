\clef "taille"
\setMusic #'rondeau {
  sol'8[ sol'] |
  sol'4 sol'8 |
  sol' la'16 sol' fa' mi' |
  re'8 mi' mi' |
  re' la' sol' |
  sol'4 fa'8 |
  la' sol'8. fa'16 |
  mi'8\trill
}
\keepWithTag #'() \rondeau sol'8 sol' |
mi'\trill << { sol'8[ sol'] } \\ \sugNotes { sol'8[ mi'] } >> |
fa'4 fa'8 |
mi'4 mi'8 |
fa' mi' mi' |
mi' mi' mi'16 fa' |
sol'4 sol'8 |
sol'8. sol'16 la'8 |
si' mi' la' |
re'
\rondeau
sol'8[ la'] |
fa' fa' sol' |
mi' dod' fa' |
mi' mi' mi' |
fa' la' la' |
la'4 la'16 la' |
sol'8. sol'16 fad'8 |
sol'8 fad'8.\trill sol'16 |
sol'8
\rondeau
