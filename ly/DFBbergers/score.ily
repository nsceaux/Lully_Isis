\score {
  \new ChoirStaff <<
    \new Staff \withLyricsB <<
      \global \keepWithTag #'voix1 \includeNotes "voix"
    >>
    \keepWithTag #'couplet1 \includeLyrics "paroles"
    \keepWithTag #'couplet2 \includeLyrics "paroles"
    \new Staff \withLyricsB <<
      \global \keepWithTag #'voix2 \includeNotes "voix"
    >>
    \keepWithTag #'couplet1 \includeLyrics "paroles"
    \keepWithTag #'couplet2 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1.*4 s2 \bar "" \break s1 s1.*4\break s1.*4 s2. \bar "" \pageBreak
        s2. s1.*4 s2 \bar "" \break
      }
    >>
  >>
  \layout { }
  \midi { }
}
