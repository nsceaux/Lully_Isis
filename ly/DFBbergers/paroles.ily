\tag #'(couplet1 basse) {
  Quel bien de -- vez- vous at -- ten -- dre,
  beau -- té qui chas -- sez dans ces bois ?
  Que pou -- vez- vous pren -- dre
  qui vaille un cœur ten -- dre
  soû -- mis à vos loix ?
  loix ?

  Ce n’est qu’en ai -- mant
  qu’on trouve un sort char -- mant ;
  Ai -- mez en -- fin, à vô -- tre tour,
  il faut que tout cede à l’A -- mour :
  Il sçait fra -- per d’un coup cer -- tain,
  le cerf lé -- ger qui fuit en vain ;
  Jus -- que dans les an -- tres se -- crets,
  au fond des fo -- rets,
  tout doit sen -- tir ses traits.
  Ce n’est qu’en ai -   traits.
}
\tag #'couplet2 {
  Lors que l’A -- mour vous ap -- pel -- le,
  pour -- quoy fuy -- ez- vous ses plai -- sirs ?
  La Ro -- ze nou -- vel -- le
  n’en est que plus bel -- le,
  d’ai -- mer les zé -- phirs :
  - phirs :
}
