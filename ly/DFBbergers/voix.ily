<<
  \tag #'(voix1 basse) {
    \clef "vpetite-haute-contre" mi'4 fa' sol' la' sol' fa' |
    sol'2 sol'4 r r sol' |
    do' re' mi' fa' re' sol' |
    mi'2\trill fa'4 sol' fa' mi' |
    re' mi' fa' mi' re' do' |
    re' mi' fa' mi' re' do' |
    re'2.\trill~ re' |
    re'2 re'4 sol' fa' mi'\trill |
    fa' sol' la' re' re' mi' |
    dod'2\trill la'4 sol' fa' mi' |
    fa' sol' la' mi'2.\trill~ |
    mi'2 mi'4 fa' fa' fa' |
    sol' mi'4.\trill re'8 re'2 sib'4 |
    la' sol' fad' sol' la' sib' |
    fad'2.~ fad'2 fad'4 |
    sol'4. sol'8 sol'4 la'4 fad'4.\trill sol'8 |
    sol'2 sol'4 fa' mi' re' |
    mi' fa' sol' re'2.\trill~ |
    re'2 re'4 mi' mi' fa' |
    fa' sol' mi' re' re'4.\trill do'8 |
    do'2.~ do' |
    r4 r re' sol' fa' mi' |
    do'2.~ do' |
  }
  \tag #'voix2 {
    \clef "vtaille" do'4 re' mi' fa' mi' re' |
    mi'2 mi'4 r r mi' |
    la si do' \sugRythme { si'4. si'8 } do'4 do' si4 |
    do'2 re'4 mi' si do' |
    si\trill do' re' do' si la |
    si do' re' sol sol la |
    si2.~ si |
    si2 si4 mi' re' dod' |
    re' sib la la4. sib8 sol4\trill |
    la2 fa'4 mi' re' dod' |
    re' mi' fa' dod'2.\trill~ |
    dod'2 dod'4 re' re' re' |
    mi' dod'4.\trill re'8 re'2 re'4 |
    do' sib la sib do' re' |
    la2.\trill~ la2 la4 |
    sib4. sib8 sib4 do' la re' |
    si2\trill mi'4 re' do' si |
    do' re' mi' si2.\trill~ |
    si2 si4 do' sol la |
    si sol do' do'4 si4.\trill do'8 |
    do'2.~ do' |
    r4 r si mi' re' dod' |
    do'2.~ do' |
  }
>>
