\clef "basse" do2 do'4 fa2. |
do2 sol,4 do re mi |
fa2 mi4 re sol2 |
la4 sol fa mi re do |
sol do si, do2 do4 |
sol do si, do si, la, |
sol,2 re4 sol fa sol |
sol,2 sol4 mi la la, |
re2 do4 sib,2. |
la,2.~ la,~ |
la, la2 la4 |
sol fa mi re do sib, |
sol, la,2 re re4 |
fad, sol, re, sol,2 sol,4 |
re2 re4 do4 sib, la,4 |
sol, sol8 fa mib4 do re re, |
sol,2.~ sol,~ |
sol, sol2 sol4 |
fa mi re do sib, la, |
sol, mi, la, fa, sol,2 |
do,4 do re mi re do |
sol2 sol4 mi la la, |
do,2.~ do, |
