\clef "taille" fa'4 fa' |
sol' sol' sol' |
fa' fa' fa' |
re'4. re'8 do'4 |
sib4. fa'8 fa'4 |
fa'2 fa'8 fa' |
sol'4 sol' sol' |
sol' sol' sol' |
fad'4. fad'8 sol'4 |
sol' sib' la' |
la'2 la'4 |
sib' re' re' |
mib' mib' mib' |
re'2~ re'8 re' |
re'2 sol'8 sol' |
sol'4 sol' sol' |
la' la' fa'8 fa' |
fa'4 fa' fa' |
fa'4. fa'8 fa'4 |
fa'4. fa'8 sol'4 |
sol'4. sol'8 la'4 |
la'4. sib'8 sol'4 |
fa'2 do'4 |
do'4 do'4. do'8 |
re'4 mib' fa' |
fa'2~ fa'8 fa' |
fa'2 fa'4 |
fa'4 fa'4. fa'8 |
sol'4 fa' mib' |
mib'2 re'8 mib' |
re'2 fa'4^\markup\whiteout\italic [doux] |
fa' fa'4. fa'8 |
sol'4 fa' mib' |
mib'2( re'8) mib' |
re'2. |
