\clef "haute-contre" sib'4 sib' |
sib' sib' do'' |
la' la' la' |
sol'4. sol'8 fa'4 |
fa'4. fa'8 sib'4 |
la'2 la'8 si' |
do''4 do'' sol'8 la' |
sib'4 sib' sib' |
la'4. la'8 re''4 |
sib'4. sib'8 do''4 |
la'2 fad'4 |
sol' la' sol' |
sol' sol' sol' |
sol'( fad'4.)\trill sol'8 |
sol'2 si'8 si' |
do''4 do'' do'' |
do'' do'' la'8 la' |
sib'4 sib' fa'8 sol' |
la'2 la'4 |
sib'4. sib'8 sib'4 |
sol'4 do''2 |
fa'4. sol'8 la'4 |
la'2 fa'4 |
fa'2. |
fa'8 sol' la'4 sib' |
sib'4( la'4.)\trill sib'8 |
sib'2 sib'4 |
sib'2. |
sib'2 sib'4 |
sib'4 la'4.\trill sib'8 |
sib'2 sib'4^\markup\whiteout\italic [doux] |
sib'2. |
sib'2 sib'4 |
sib'4 la'4.\trill sib'8 |
sib'2. |
