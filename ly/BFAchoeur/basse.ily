\clef "basse" sib4 sib, |
mib mib do |
fa fa re |
sol4. sol8 la4 |
sib4. sib8 sib,4 |
fa2 fa8 fa |
do4 do do' |
sol sol sol |
re'4. do'8 sib4 |
mib'4. re'8 do'4 |
re'2 re'4 |
sib fad sol |
mib mib do |
re2 re4 sol2 sol8 sol |
mi4 do do' |
la fa fa8 fa |
re4 sib, sib |
la4. sol8 fa4 |
sib4. la8 sol4 |
do'4. sib8 la4 |
re'4. sib8 do'4 |
fa2 fa4 |
mib mib4. mib8 |
re4 do sib, |
fa2 fa,4 |
sib,2 sib4 |
lab lab4. lab8 |
sol4 re mib |
fa fa,2 |
sib, sib4 |
lab lab4. lab8 |
sol4 re mib |
fa fa,4. sib,8 |
sib,2. |
