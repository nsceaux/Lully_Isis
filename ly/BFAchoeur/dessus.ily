\clef "dessus" re''4 re'' |
mib'' mib'' mib'' |
do'' do'' fa'' |
sib'4. sib'8 do''4 |
re''4. mib''8 re''4 |
do''2\trill do''8 re'' |
mib''4 mib'' mib'' |
re'' sol'' sol'' |
la''4. la''8 sib''4 |
sol''4. sol''8 la''4 |
fad''2 la'4 |
re'' re'' sib' |
mib'' sol' do'' |
la'2~ la'8 re'' |
si'2 re''8 re'' |
mi''4 mi'' mi'' |
fa'' fa'' do''8 do'' |
re''4 re'' re'' |
do''4. do''8 fa''4 |
re''4. re''8 re''4 |
mi''4. mi''8 fa''4 |
fa''4. fa''8 mi''4 |
fa''2 la'4 |
la'4 la'4.\trill la'8 |
sib'4 do'' re'' |
do''2\trill~ do''8 fa'' |
re''2\trill re''4 |
re'' re''4.\trill re''8 |
mib''4 fa'' sol'' |
re''4( do''4.\trill) sib'8 |
sib'2 re''4^\markup\italic [doux] |
re'' re''4. re''8 |
mib''4 fa'' sol'' |
re''4( do''4.)\trill sib'8 |
sib'2. |
