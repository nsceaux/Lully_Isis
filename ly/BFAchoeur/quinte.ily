\clef "quinte" re'4 re' |
sol sol do' |
do' la << fa' \\ \sug re' >> |
re'4. sib8 la4 |
re'4. sib8 sib4 |
do'2 do'8 do' |
do'4 do' do' |
sib re' re' |
re'2 re'4 |
mib'4. fa'8 mib'4 |
re'2 re'4 |
re' re' sol |
sol sib la |
la2\trill~ la8 sol |
sol2 sol8 sol |
do'4 do' do' |
do' do' do'8 do' |
sib4 sib sib |
do'4. do'8 do'4 |
sib4. sib8 sib4 |
do'4. do'8 do'4 |
re'4. re'8 do'4 |
do'2 do'4 |
do'2 do'4 |
sib4 la re'8 mib' |
\once\tieDashed fa'2~ fa'8 fa' |
re'2 re'4 |
re'2 re'4 |
sib2 sol4 |
fa2 fa4 |
fa2 re'4^\markup\whiteout\italic [doux] |
re'2 re'4 |
sib2 sol4 |
fa2 fa4 |
fa2. |
