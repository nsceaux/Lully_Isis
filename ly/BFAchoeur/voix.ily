<<
  \tag #'vdessus {
    \clef "vdessus" <>^\markup\character Chœur re''4 re'' |
    mib'' mib'' mib'' |
    do''\trill do'' fa'' |
    sib'4. sib'8 do''4 |
    re''4. mib''8 re''4 |
    do''2\trill do''8 re'' |
    mib''4 mib'' mib'' |
    re''\trill re'' sib' |
    la'4.\trill la'8 re''4 |
    sib'4. sib'8 do''4 |
    la'2\trill la'4 |
    re'' re'' sib' |
    mib'' sol' do'' |
    la'2\trill~ la'8 re'' |
    si'2\trill re''8 re'' |
    mi''4 mi'' mi'' |
    fa'' fa'' do''8 do'' |
    re''4 re'' re'' |
    do''4.\trill do''8 fa''4 |
    re''4.\trill re''8 re''4 |
    mi''4. mi''8 fa''4 |
    fa''4. fa''8 mi''4 |
    fa''2 la'4 |
    la'4 la'4.\trill la'8 |
    sib'4 do'' re'' |
    do''2\trill~ do''8 fa'' |
    re''2\trill re''4 |
    re'' re''4.\trill re''8 |
    mib''4 fa'' sol'' |
    re''( do''4.)\trill sib'8 |
    sib'2 re''4^\markup\italic [doux] |
    re'' re''4.\trill re''8 |
    mib''4 fa'' sol'' |
    re''4( do''4.)\trill sib'8 |
    sib'2. |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" fa'4 fa' |
    sol' sol' sol' |
    fa' fa' fa' |
    sol'4. sol'8 fa'4 |
    fa'4. fa'8 fa'4 |
    fa'2 fa'8 fa' |
    sol'4 sol' sol' |
    sol' sol' sol' |
    fad'4.\trill fad'8 sol'4 |
    sol'4. sol'8 la'4 |
    fad'2\trill fad'4 |
    sol' la' sol' |
    sol' mib' mib' |
    re'2~ re'8 re' |
    re'2 sol'8 sol' |
    sol'4 sol' sol' |
    la' la' fa'8 fa' |
    fa'4 fa' fa' |
    fa'4. fa'8 fa'4 |
    fa'4. fa'8 sol'4 |
    sol'4. sol'8 la'4 |
    la'4. sib'8 sol'4\trill |
    fa'2 fa'4 |
    do'4 do'4. do'8 |
    re'4 mib' fa' |
    fa'2~ fa'8 fa' |
    fa'2 fa'4 |
    fa' fa'4. fa'8 |
    sol'4 fa' mib' |
    mib'2( re'8) mib' |
    re'2\trill fa'4^\markup\italic [doux] |
    fa' fa'4.\trill fa'8 |
    sol'4 fa' mib' |
    mib'2( re'8) mib' |
    re'2.\trill |
  }
  \tag #'vtaille {
    \clef "vtaille" sib4 sib |
    sib sib do' |
    la\trill la re' |
    re'4. re'8 do'4 |
    sib4. la8 sib4 |
    la2\trill la8 si |
    do'4 do' do' |
    sib sib re' |
    re'4. re'8 re'4 |
    mib'4. fa'8 mib'4 |
    re'2 re'4 |
    re' re' re' |
    sol sol sol |
    sol( fad4.) sol8 |
    sol2 si8 si |
    do'4 do' do' |
    do' do' la8 la |
    sib4 sib sib |
    do'4. do'8 do'4 |
    re'4. do'8 sib4 |
    do'4. do'8 do'4 |
    re'4. re'8 do'4 |
    la2 do'4 |
    do' do'4. do'8 |
    sib4 la sib |
    sib4( la4.)\trill sib8 |
    sib2 sib4 |
    sib sib4. sib8 |
    sib4 sib sib |
    sib( la4.)\trill sib8 |
    sib2 sib4^\markup\italic [doux] |
    sib sib4. sib8 |
    sib4 sib sib |
    sib4( la4.)\trill sib8 |
    sib2. |
  }
  \tag #'vbasse {
    \clef "vbasse" sib4 sib, |
    mib mib do |
    fa fa re |
    sol4. sol8 la4 |
    sib4. sib8 sib,4 |
    fa2 fa8 fa |
    do4 do do' |
    sol sol sol |
    re'4. do'8 sib4 |
    mib'4. re'8 do'4 |
    re'2 re'4 |
    sib fad sol |
    mib mib do |
    re4.( do8) re4 |
    sol2 sol8 sol |
    mi4\trill do do' |
    la\trill fa fa8 fa |
    re4\trill sib, sib |
    la4.\trill sol8 fa4 |
    sib4. la8 sol4 |
    do'4. sib8 la4 |
    re'4. sib8 do'4 |
    fa2 fa4 |
    mib mib4. mib8 |
    re4 do\trill sib, |
    fa4.( mib8) fa4 |
    sib,2 sib4 |
    lab4 lab4. lab8 |
    sol4 re mib |
    fa( fa,4.) sib,8 |
    sib,2 sib4^\markup\italic [doux] |
    lab lab4. lab8 |
    sol4 re mib |
    fa4( fa,4.) sib,8 |
    sib,2. |
  }
>>
