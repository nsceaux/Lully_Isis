\clef "haute-contre" la'8 |
sol' sol' la' |
\mergeDifferentlyDottedOn
<< { fa'8 sol'8. fa'16 } \\ \sugNotes { fa'8 sol' mi' } >> |
mi'8. re'16 mi'8 |
fa'4 la'8 |
si' do''8. re''16 |
do''8 do'' do'' |
<< \sugNotes { si'8 do''8. re''16  } \\ { si'8 do'' la' } >> |
si' do'' la' |
sol' sol'8. la'16 |
<< { fa'8 sol'8. fa'16 } \\ \sugNotes { fa'8 sol' mi' } >> |
mi'8.\trill re'16 mi'8 |
fa'4 la'8 |
si' do''8. re''16 |
do''8 do'' do'' |
si' do''8. re''16 |
si'8 do'' do''\noBeam |
do''8 la'4 |
la'4 la'8 |
re''8 re''8. mi''16 |
dod''8\trill re'' re'' |
re''4 re''8 |
do''4 do''8 |
\sugRythme { si'8[ si'8. si'16] } do''8 sib' la' |
do'' la'16 sib' do'' la' |
re''4 re''8 |
do''4 do''8 |
do''8 do''8. sib'16 |
la'4\trill do''8 |
la'4.\trill |
