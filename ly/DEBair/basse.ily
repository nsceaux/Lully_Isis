\clef "basse" \footnoteHere #'(0 . 0) \markup { Matériel 1677 : B.C. tacet. }
fa8 |
do'8. sib16 la8 |
sib sol do' |
do4 do8 |
fa fa re |
sol sol fa |
mi la fa |
sol do fa, |
sol, la, fa, |
do8. sib,16 la,8 |
sib, sol, do |
do,4. |
fa,8 fa re |
sol sol fa |
mi la fa |
sol do fa, |
sol, do do'\noBeam |
fa8. mi16 re8 |
la fa dod |
re sib, sol, |
la, re re |
sol4 sol8 |
do'4 do'8 |
la sol fa |
do fa fa |
re sib, sol, |
do do la, |
fa, do do, |
fa,4 do'8 |
fa,4. |
