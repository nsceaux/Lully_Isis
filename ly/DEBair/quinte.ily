\clef "quinte" do'8 |
do'4 do'8 |
sib4 do'8 |
do'4 do'8 |
do' do' re' |
<< { re'8 sol sol } \\ \sugNotes { sib4 la8 } >> |
sol do' re' |
re' do' la |
sol do' do' |
do'4 do'8 |
sib4 do'8 |
do'4 do'8 |
do'8 do' re' |
<< { re'8 sol sol } \\ \sugNotes { sib4 la8 } >> |
sol do' re' |
re' do' la |
sol sol do'\noBeam |
do'4 re'8 |
mi' fa' mi' |
re' fa'8. mi'16 |
mi'8 fa' fa' |
re'8. do'16 sib8 |
do'4 do'8 |
do'4. |
do'8 do' la |
la sib sib |
do'4 do'8 |
do' do'8. do'16 |
do'4 do'8 |
do'4. |
