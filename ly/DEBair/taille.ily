\clef "taille" fa'8 |
sol' mi' fa' |
re' re' do' |
do'8. fa'16 sol'8 |
fa'4 fa'8 |
sol' mi'8. re'16 |
mi'8 mi' la' |
sol' mi' do' |
sol' mi' fa' |
sol' mi' fa' |
re' re'\trill do' |
do'8. fa'16 sol'8 |
fa'4 fa'8 |
sol' mi'8. re'16 |
mi'8 mi' la' |
\mergeDifferentlyDottedOn
<< \sugNotes { sol'8 mi'8. re'16 } \\ { sol'8 mi' do' } >> |
<< { sol'8[ mi'] } \\ \sugNotes { re'8[ mi'] } >> sol'\noBeam |
la'8. sol'16 fa'8 |
mi' re' mi' |
la sib4 |
la8 la la' |
sol'4 sol'8 |
mi'16 fa' sol'8 mi' |
la' mi' fa' |
mi' fa' fa' |
fa' fa' sol' |
mi' do' fa'~ |
fa'16 sol' mi'8. fa'16 |
fa'4 sol'8 |
fa'4. |
