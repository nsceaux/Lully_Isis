\clef "dessus" <>^"[Violons]" fa''8 |
mi''8.\trill re''16 do''8 |
re'' sib' sol' |
do''8. re''16 sib'8 |
la'8\trill fa' fa'' |
re'' mi''8.\trill fa''16 |
sol''8 mi'' fa'' |
re'' mi''8. fa''16 |
re''8\trill do'' fa'' |
mi''8.\trill re''16 do''8 |
re'' sib' sol' |
do''8. re''16 sib'8 |
la'\trill fa' fa'' |
re'' mi''8.\trill fa''16 |
sol''8 mi'' fa'' |
re'' mi''8. fa''16 |
re''8\trill do'' mi''\noBeam |
fa''8. sol''16 la''8 |
dod'' re'' la' |
fa'' fa''8. sol''16 |
mi''8\trill re'' fa'' |
sib''8. la''16 sib''8 |
\once\slurDashed sol''( mi'') do'' |
fa'' sol''8. la''16 |
sol''8\trill fa'' la'' |
\once\slurDashed fa''( re'') sib'' |
\once\slurDashed sol''( mi'') la''~ |
la''16 sol'' sol''8.\trill fa''16 |
fa''4 mi''8 |
fa''4. |
