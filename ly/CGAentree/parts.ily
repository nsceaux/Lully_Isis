\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille)
   (quinte)
   (basse)
   (basse-continue #:system-count 2)
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
