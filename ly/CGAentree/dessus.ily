\clef "dessus" re''4 fa'' re'' |
la''2 fa''4 |
sol'' la''2 |
\once\slurDashed fa''4.(\trill mi''16 fa'') re''4 |
re'' fa'' re'' |
la''2 mi''4 |
fa'' fa''4.\trill mi''8 |
mi''2. |
mi''4 fa'' re'' |
sol''2 mi''4 |
dod'' \once\slurDashed re''4.( mi''8) |
mi''2\trill la'4 |
mi'' fa'' re'' |
sol'' mi'' dod'' |
re''8 mi'' mi''4.\trill re''8 |
re''2. |
