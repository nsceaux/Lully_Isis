\clef "taille" la'4 la' la' |
la' mi' fa' |
re' la'2 |
la' fa'4 |
fa' la' fa' |
mi'2 la'4 |
la'2 la'4 |
la'2. |
mi'4 re' re' |
mi'2 dod'8 re' |
mi'4 fa'2 |
mi'2 re'4 |
dod' re' re' |
re' mi'2 |
re'4 la'4. la'8 |
la'2. |
