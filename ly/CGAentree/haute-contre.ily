\clef "haute-contre" la'4 re'' re'' |
mi''2 la'4 |
re'' \once\slurDashed dod''4.( si'16 dod'') |
re''2 re''4 |
la' re'' re'' |
mi'' \once\slurDashed dod''4.( si'16 dod'') |
re''4 re''4. mi''8 |
dod''2. |
dod''4 re'' si' |
sol'2 \once\tieDashed la'4~ |
la' la'2 |
sol'2 fa'4 |
la' la' sib' |
sib'2 la'4 |
re'' dod''4. re''8 |
re''2. |
