\clef "quinte" fa'4 fa' fa' |
mi'2 re'4 |
sol' mi'2 |
re'2 la4 |
la la2 |
la4 mi'2 |
re' re'4 |
mi'2. |
la4 la si |
si2 \once\tieDashed la4~ |
la la2 |
la la4 |
la fa fa |
sib sib mi |
sol la4. la8 |
la2. |
