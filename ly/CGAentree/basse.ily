\clef "basse"
\footnoteHere #'(0 . 0) \markup { Matériel 1677 : B.C. tacet. }
re2 re'4 |
dod'2 re'4 |
sib la la, |
re2 re4 |
fa re2 |
dod4 la,2 |
re4 re,2 |
la,2. |
la4 re sol |
mi2 la4~ |
la re2 |
dod re4 |
la, re sib, |
sol,2 la,4 |
sib, la,2 |
re,2. |
