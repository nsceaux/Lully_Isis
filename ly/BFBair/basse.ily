\clef "basse"
\footnoteHere #'(0 . 0) \markup { Matériel 1677 : B.C. tacet. }
sib,2 sib4. sib8 |
la2 sib4 sib, |
mib2 mi |
fa4. sol8 la2 |
sib si |
do'4. sib8 la4 sib |
sol2 do'4 do fa4. sol8 fa mib re do |
fa,2 fa8 fa mib re |
do4 do,2 do8 do |
sol4 sol,2 sol8 fa |
mib4. re8 do2 |
re4 re'8 la sib4 fad |
sol do re re, |
sol, sol8 la sib4 si |
do'8 do' sib do' la2 |
sib8 lab sol fa mib re do sib, |
fa4. mib8 re2 |
sol4 mib fa fa, |
sib,2 fa8 fa mib re |
sib,1\fermata |
