\clef "taille" fa'2 fa'4. fa'8 |
la'2 re'8 mib' fa'4 |
mib'4. sol'8 sol'2 |
do'4 fa'2 fa'4 |
fa'2 sol'4. sol'8 |
sol'4 mi'8 mi' fa'4 fa' |
fa'2 mi'4.\trill re'16 mi' |
fa'1 |
fa'2 r8 fa' fa' fa' |
mib'4 mib'2 sol'4 |
sol' sol'2 re'8 re' |
mib'4 mib'2 mib'8 mib' |
re'4 re' re' re' |
re' mib' re'4. re'8 |
re'2~ re'8 re' re' re' |
mi' fa' sol' mi' fa'4. fa'8 |
fa' fa' mib' re' mib'4. fa'8 |
fa'2 fa'4 fa' |
sol' sol' fa'4. mib'8 |
re'2 r8 fa' fa' fa' |
re'1\fermata |
