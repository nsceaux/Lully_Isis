\clef "dessus" sib''4. sib''8 fa''4. fa''8 |
fa''2. re''8 re'' |
mib''4. re''8 do'' re'' sib' do'' |
la'4.\trill sib'8 do'' re'' mib'' fa'' |
re''2\trill~ re''8 re'' mi'' fa'' |
mi''4 do''8 do'' fa''4. fa''8 |
sol''4. sol''8 \once\slurDashed sol''4.(\trill fa''16 sol'') |
la''1 |
la''2 r8 do'' do'' re'' |
mib''4 mib''2 mib''8 fa'' |
re''4 re''2 re''8 re'' |
sol''4 sol''2 la''8 sib'' |
\once\slurDashed fad''4.(\trill mi''16 fad'') sol''4 re''8 la' |
sib'4. do''8 la'4.\trill sol'8 |
sol'2~ sol'8 re'' sol'' fa'' |
mi''4. do''8 fa'' fa'' mib'' fa'' |
re'' re'' mib'' fa'' sol'' sol'' la'' sib'' |
la''4.\trill sol''8 fa'' mib'' re'' do'' |
sib' la' sib' do'' do''4.\trill sib'8 |
sib'2 r8 do'' do'' re'' |
sib'1\fermata |
