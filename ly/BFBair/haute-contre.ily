\clef "haute-contre" re''4. re''8 re''4. re''8 |
do''2 fa'4. fa'8 |
sol'2 sol'4. sol'8 |
fa'2 fa'4 do''8 re'' |
sib'4. la'8 sol'4. sol'8 |
sol'4 sol'8 sol' la'4 re'' |
re''2 do''4. do''8 |
do''1 |
do''2 r8 la' la' si' |
do''4 do''2 sol'8 la' |
sib'4 sib'2 sib'8 sib' |
sib'4 sib' do'' do'' |
la'4 la'8 la' sol'4 la' |
\sugRythme { si'4. si'8 } sol'4 sol' fad'4.\trill sol'8 |
sol'2~ sol'8 sol' sol' sol' |
sol'4. sol'8 la' sib' do'' la' |
sib'4. sib'8 sib' sib' do'' re'' |
do'' sib' la'4 la'4. la'8 |
sib'4 sib' la'4. sib'8 |
sib'2 r8 la' la' si' |
sib'1\fermata |
