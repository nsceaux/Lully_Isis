\clef "quinte" sib2 sib4. sib8 |
do'2 sib4 sib |
sib2 do' |
do' do'4 do' |
re'2 re'4. re'8 |
do'2 do'4 sib |
sib2 do'4. do'8 |
do'1 |
do'2 r8 fa fa fa |
sol4 sol2 do'8 do' |
sib4 sol2 sol4 |
sol4. sol8 do'4 la |
la re' re' re'8 do' |
sib4. la8 la4.\trill sib8 |
sib4 sib8 do' re'4 re' |
do'4. do'8 do' sib la do' |
sib4. sib8 sib4. sib8 |
do'4. do'8 re'4. re'8 |
re'4 mib' do' fa' |
fa'2 r8 fa fa fa |
fa'1\fermata |
