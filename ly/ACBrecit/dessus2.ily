\clef "dessus" R1*8 R2.*18 |
<>_"Flutes" re''4. re''8 mib''4 |
\once\slurDashed do''4.(\trill sib'16 do'') re''4 |
\once\slurDashed sib'4.(\trill la'16 sib') do''4 |
la'4.\trill sib'8 do''8. re''16 |
sib'4 la'4.\trill sol'8 |
sol'2. |
R2.*35 |
<>^"Flutes" re''4. re''8 mib''4 |
do''4.(\trill sib'16 do'') re''4 |
sib'4.\trill( la'16 sib') do''4 |
la'4.\trill sib'8 do'' re'' |
sib'4 la'4.\trill sol'8 |
sol'2. |
