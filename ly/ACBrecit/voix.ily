<<
  \tag #'(voix1 basse) {
    \clef "vbas-dessus" <>^\markup\character Callyope r4 sol' sib' sol' |
    re'' re'' re''4. la'8 |
    sib'2 mib''4 mib'' |
    do''2\trill do''4 fa'' |
    re''\trill re'' sib' fa' |
    sol'2 sol'4 do'' |
    la'2. la'4 |
    sib' sib' sib' do'' |
    re''2. |
    sib'4 do'' re'' |
    mib''2 do''4 |
    la'2 fad'4 |
    sol' la' sib' |
    sib'( la'4.)\trill sol'8 |
    sol'2. |
    <>^\markup\character Callyope
    re''4 re'' mib'' |
    fa''2 re''4 |
    mib''2 mib''4 |
    do'' do'' re'' |
    re''4( do''4.\trill) sib'8 |
    sib'2. |
    sib'4 do'' re'' |
    mib''2 do''4 |
    la'2\trill la'4 |
    re'' do'' sib' |
    sib'4( la'4.)\trill sol'8 |
    sol'2 r4 |
    R2.*5 |
    \ffclef "vbas-dessus" <>^\markup\character Melpomene
    r8 re'' re''4 sib' |
    sol'2 do''4 |
    la'4.\trill fa''8 re''4~ |
    re''8 mi'' mi''4.\trill re''8 |
    dod''2\trill dod''4 |
    mi''4 mi'' fa'' |
    fa''( mi''4.)\trill re''8 |
    re''2
    \ffclef "vbas-dessus" <>^\markup\character Thalie
    re''4 |
    si'2 re''4 |
    mib''2 mib''4 |
    do'' do'' la' |
    sib' sib' sib' |
    sol' sol' mib''8 mib'' |
    mib''4.( re''8) re'' mib'' |
    re''2\trill
    \ffclef "vbas-dessus" <>^\markup\character Callyope
    sol''4 |
    mi''2 fa''4 |
    fa''4.( mib''8) mib''4 |
    mib'' re'' re'' |
    sol' sol' la'4 |
    fad'\trill fad' re''8 re'' |
    re''2 re''8 do'' |
    re''2. |
    <>^\markup\character Callyope
    la'4. la'8 sib'4 |
    sol'2 do''4 |
    la'2 re''4 |
    sib'2 sib'4 |
    mib'' mib''4. mib''8 |
    do''4.(\trill sib'8) do''4 |
    re''2. |
    do''4. do''8 re''4 |
    mib''2 mib''4 |
    re''2 sib'4 |
    la'2\trill la'4 |
    re'' do''4.\trill sib'8 |
    sib'4( la'4.\trill) sol'8 |
    sol'2 r4 |
    R2.*5 |
  }
  \tag #'voix2 {
    \clef "vbas-dessus" R1*8 R2.*7 |
    <>^\markup\character Thalie
    sib'4 sib' sib' |
    la'2\trill si'4 |
    do''2 do''4 |
    la' la' sib' |
    sib'( la'4.)\trill sib'8 |
    sib'2. |
    sol'4 la' si' |
    do''2 la'4 |
    fad'2\trill fad'4 |
    sib' la' sol' |
    sol'( fad'4.)\trill sol'8 |
    sol'2 r4 |
    R2.*27 |
    <>^\markup\character Thalie
    fad'4. fad'8 sol'4 |
    mi'2 la'4 |
    fa'2 sib'4 |
    sol'2 sol'4 |
    do''4 do''4. do''8 |
    la'4.(\trill sol'8) la'4 |
    sib'2. |
    la'4. la'8 si'4 |
    do''2 la'4 |
    sib'2 sol'4 |
    fad'2\trill fad'4 |
    sib' la'4. sol'8 |
    sol'4( fad'4.)\trill sol'8 |
    sol'2 r4 |
    R2.*5 |
  }
  \tag #'voix3 {
    \clef "vhaute-contre" R1*8 R2.*7 |
    <>^\markup\character Apollon
    sol4 sol sol |
    re'2 sol4 |
    do'2 do'4 |
    fa' fa' sib |
    fa2~ fa8 sib |
    sib2. |
    mib'4 mib' re' |
    do'2 do'4 |
    re'2 re'4 |
    sib do' do' |
    re'2~ re'8 sol |
    sol2 r4 |
    R2.*27 |
    <>^\markup\character Apollon
    re'4. re'8 sol4 |
    do'2 la4 |
    re'2 sib4 |
    mib'2 mib'4 |
    do' do'4. do'8 |
    fa'2 fa4 |
    sib2. |
    fa'4. mib'8 re'4 |
    do'2 do'4 |
    sol2 sol4 |
    re'2 re'4 |
    sib4 do'4. do'8 |
    re'2~ re'8 sol |
    sol2 r4 |
    R2.*5 |
  }
>>
