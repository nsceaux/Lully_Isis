\clef "dessus" R1*8 R2.*18 |
\footnoteHere #'(0 . 0) \markup {
  Le matériel 1677 ne précise pas l’instrumentation.
} <>_"Flutes" sib''4. sib''8 do'''4 |
\once\slurDashed la''4.(\trill sol''16 la'') sib''4 |
\once\slurDashed sol''4.(\trill fad''16 sol'') la''4 |
fad''4.\trill sol''8 la''8. sib''16 |
sol''4 fad''4.\trill sol''8 |
sol''2. | \allowPageTurn
R2.*35 |
<>^"Flutes" sib''4. sib''8 do'''4 |
la''4.\trill( sol''16 la'') sib''4 |
sol''4.\trill( fad''16 sol'') la''4 |
fad''4.\trill sol''8 la'' sib'' |
sol''4 fad''4.\trill sol''8 |
sol''2. |
