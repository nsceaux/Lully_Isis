\clef "basse" sol,2\repeatTie sol |
fad1 |
sol |
la |
sib2 re |
mib mi |
fa2 fad |
sol4 fa mib2 |
re4. mi8 fad re |
sol4 la sib |
\clef "alto" do'4. re'8 mib' do' |
re'4. mib'8 re' do' |
sib4 la sol |
re' re2 |
sol2. |
sol4 sol sol |
re'2 sol4 |
do'2 do'4 |
fa' fa' sib |
fa2. |
sib4. do'8 re' sib |
mib'2 re'4 |
do'2 do'4 |
re'4. mib'8 re' do' |
sib4 do'2 |
re' re4 |
\clef "bass" sol4. sol8 do4 |
fa4. fa8 sib,4 |
mib4. mib8 do4 |
re2 fad,4 |
sol, re,2 |
sol,2. |
sol2 re4 |
mib2 do4 |
fa2 sib4~ |
sib8 la sol2 |
la4 sol8 fa mi re |
dod2 re4 |
sol, la,2 |
re4. mi8 fad4 |
sol2 sol,4 |
do2 do4 |
fa2 fa8 mib |
re2. |
mib2 do4 |
fa2 fa,4 |
sib,4 sib8[ la] sol4 |
do'2 re'8 sib |
do'2 do4 |
fa sib,2 |
mib2 do4 |
re re'8 do' sib la |
sol fa mib2 |
re2. |
\clef "alto" re'4. re'8 sol4 |
do'2 la4 |
re'2 sib4 |
mib'2 mib'4 |
do'4 do'4. do'8 |
fa'2 fa4 |
sib8 la sib do' re' mib' |
fa'4. mib'8 re'4 |
do'2 do'4 |
sol2 sol4 |
re'4. mib'8 re' do' |
sib4 do'4. do'8 |
re'2 re4 |
\clef "bass" sol4. sol8 do4 |
fa4. fa8 sib,4 |
mib4. mib8 do4 |
re2 fad,4 |
sol, re,2 |
sol,2. |
