\tag #'(voix1 basse) {
  Ces -- sez, ces -- sez pour quel -- que temps, Bruit ter -- ri -- ble des Ar -- mes,
  qui trou -- blez le re -- pos de cent cli -- mats di -- vers ;
  ne trou -- blez point les char -- mes
  de nos di -- vins con -- certs.
}

Ne trou -- blez point les char -- mes
de nos di -- vins con -- certs.
Ne trou -- blez point les char -- mes
de nos di -- vins con -- certs.

\tag #'(voix1 basse) {
  Re -- com -- men -- çons nos chants, al -- lons les faire en -- ten -- dre
  dans une Au -- gus -- te Cour.
  La Paix, la dou -- ce Paix n’ose en -- co -- re des -- cen -- dre
  du cé -- les -- te sé -- jour.
  La Paix, la dou -- ce Paix, n’ose en -- co -- re des -- cen -- dre
  du cé -- les -- te sé -- jour :
}
Près du Vain -- queur al -- lons at -- ten -- dre
son bien heu -- reux re -- tour.
Près du Vain -- queur al -- lons at -- ten -- dre
son bien heu -- reux re -- tour.
