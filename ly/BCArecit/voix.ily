<<
  \tag #'(recit1 basse) {
    \ffclef "vbas-dessus" <>^\markup\character Io
    r4 re''8 re'' la'2\trill |
    <<
      { \voiceOne sib'8 sib' sib' la' la'2\trill \oneVoice }
      \new Voice \with { autoBeaming = ##f } \sugNotes {
        \voiceTwo sib'8 sib' sib' sib' fad'2\trill
      }
    >>
    \ffclef "vbasse-taille" <>^\markup\character Hierax
    r2 r4 r8 re' |
    sib4 sib8 sib16 la sol8.\trill fa16 |
    mi2\trill r8 mi |
    fa sol la si do' la |
    re'4 re'16 r sib8 sol4\trill sol8 sol16 sol |
    mi8\trill mi16 do fa4 fa8 mi |
    fa2 la8 sib |
    sol4\trill sol8[ fad16] sol fad8\trill re' |
    si4\trill si8 r do'8 do'16 sib sib8[ la16] sib |
    la4\trill r8 fa' dod' re' re' dod' |
    re'4.
    \ffclef "vbas-dessus" <>^\markup\character Io
    re''8 sib'8 sib'16 sib' fa'8 fa'16 sol' |
    la'4 la'8 fa' do'' do'' re'' mi'' |
    fa''4 re''8 re'' si'4\trill si'8 re'' |
    sol' sol' sol' sol' do'' do'' sib'\trill la' |
    sib'4 sib'8 sib'16 do'' la'8\trill la' la' sol' |
    la'4 la' r8 la'16 la' re''8 re''16 re'' |
    si'4.\trill si'8 mi'' si' dod'' re'' |
    dod''4\trill
    \ffclef "vbasse-taille" <>^\markup\character Hierax
    la8 la fad4\trill sol8 la |
    sib sib sib re' sol sol |
    mi4\trill mi8 sol sol sol la\trill sib |
    do'4 r16 la sib do' mi8.\trill fa16 |
    fa2. |
    r4 <>^"Air" la4. re'8 |
    dod'2\trill re'8 la |
    sib2 sol8 sol |
    sol[ la sol] fa[ mi] la |
    fa4\trill re la8 sib |
    do'4 sol4.\trill la8 |
    sib2 sib8 la |
    sol4.\trill la8 fa4 |
    mi4\trill mi r8 do' |
    do'4. si8 si si |
    sold2\trill mi'8 si |
    do'4 si4.\trill la8 |
    la2 r8 la |
    re'8. do'16 sib8[ la] sol[ fa] |
    mi2\trill fa4 |
    sol8 la sib4. do'8 |
    la4\trill fa do'8 la |
    sib4 sib8 la sol4\trill |
    sol8 fa mi4\trill mi8 re |
    dod4\trill dod r8 la |
    si4. dod'8 re' mi' |
    dod'4\trill r8 mi8 mi4 |
    fa4 mi4.\trill re8 |
    re2 r8 la |
    re4
    \ffclef "vbas-dessus" <>^\markup\character Io
    sib'8 sib' sib'4 sib'8 sib' |
    sol'8.\trill mi'16 mi'8 mi' la' mi' |
    fa'4 fa'8 la' la'\trill la' si' dod'' |
    re''4 re''8 mi'' dod''4\trill dod''8 re'' |
    mi''4
    \ffclef "vbasse-taille" <>^\markup\character Hierax
    r8 la la sol sol fad |
    fad4.\trill la8 do' do' do' si |
    si4\trill si8 r r16 sol sol sol do'8 sol |
    la4 fa8 la re4\trill re8 mi |
    do4
    \ffclef "vbas-dessus" <>^\markup\character Io
    do''4. si'16 si' si'8 si' |
    sold'4\trill
    \ffclef "vbasse-taille" <>^\markup\character Hierax
    mi8 mi16 mi fad8 sold |
    la8 la do' mi' la4 la8 re' |
    sold4\trill si8 do' la4\trill la8 sold |
    la4 la8
    \ffclef "vbas-dessus" <>^\markup\character Io
    dod''16 dod'' re''8 re'' re'' re''16 la' |
    sib'8. sib'16 sib' do'' re'' mi'' fa''8 fa''16 dod'' re''8 re''16 mi'' |
    dod''4\trill
    \ffclef "vbasse-taille" <>^\markup\character Hierax
    r8 la re' re' la8.\trill sib16 |
    do'8. do'16 sib8.\trill la16 sol8.\trill fa16 |
    mi4\trill do8 do' sol\trill sol sol la |
    sib4 sol8 sol16 la fa8.\trill mi16 |
    fa4 re8 re' re' si sold\trill si |
    mi si mi' re' re'[ dod'16] re' |
    dod'4\trill la8 la la4 sol8.\trill sol16 |
    sol4 sol8 do' fa4 fa8 mi |
    fa8. do'16 la8 la la si do'8. re'16 |
    si8\trill si r16 sol la si do'8 do' do' si |
    do'4 sib8. sib16 sib8 la16 sib sol8.\trill fa16 |
    mi8\trill mi la8 la16 si dod'8\trill la |
    re'4 sol8 la fa4 mi8.\trill re16 |
    re4 r r16 la la la fa8 fa |
    re4\trill r8 sib16 sib fad4\trill fad8 sol |
    sol8 sol r16 re re mi fa8 fa fa sol |
    la2 la4 r |
    r4^"Air" re la fa |
    sib4. sol8 do'4 do |
    fa4. fa8 fa4 fa |
    mi\trill mi re\trill re |
    do4. do8 re4 mi |
    fa re mi mi |
    la,2 la, |
    r4 re re mi |
    fa fa fad4. fad8 |
    sol4. la8 sib4 sol |
    re'4. re'8 dod'4 re' |
    la2 la |
    r4 fa fa sol |
    la la si si |
    do'4. do'8 dod'4 re' |
    sib sol la la, |
    re2 re4 r |
    R1 |
    r4 la8 la mi4\trill mi8 mi |
    fa4 r8 fa fa fa fa mi |
    mi4\trill r8 do' do' do' sib la |
    sib4 sib8 re' sib4\trill sib8 sib |
    sol4\trill sol8 sol sol4 la8 sib |
    do'4 do'8 re' sol4\trill sol8 la |
    fa4\trill fa8 r
    \ffclef "vbas-dessus" <>^\markup\character Io
    r8 re'' |
    dod''2\trill la'4 la'8 mi' |
    fa'4. fa'8 sib' sib' sib'[ la'16] sib' |
    la'2\trill la'8 la' si' dod'' |
    re''4 re''8. mi''16 dod''4\trill dod'' |
    r la'8 sib' do''4 do''8 la' |
    sib'4. re''8 sol'8. la'16 sol'8.\trill fa'16 |
    fa'4 fa''8. la'16 la'8 si' |
    dod''4\trill dod''8 re'' re''4.( dod''8) |
    re''4 la'8 sib' do''4 do''8 la' |
    sib'4. re''8 sol' la' sol'8.\trill fa'16 |
    fa'4 fa''8. la'16 la'8 si' |
    dod''4\trill dod''8 re'' re''4.( dod''8) |
    re''1 |
    <>^\markup\character Io
    r4 fa'' dod''\trill dod''8 dod'' |
    re''4 re'' si'\trill si'8 si' |
    do''4 do'' la'2\trill |
    r4 la' si' si' |
    do'' re'' re''4.\trill do''8 |
    do''2 r4 do''\trill |
    la' la'8 la' fa'4 fa' |
    sib'4 sib' la'\trill la' |
    mi'' fa'' re''( dod''8) re'' |
    dod''4\trill mi'' fa''2 |
    r4 do''8 do'' do''[ sib'8.]( la'16) sib'8 |
    la'2 r4 re'' |
    si'\trill si'8 si' mi''4 mi'' |
    dod''2\trill dod''4 dod'' |
    re''4 re''8 re'' re''4( mi''8) fa'' |
    mi''2\trill mi''4 mi'' |
    fa''4 fa''8 mi'' re''4.\trill do''8 |
    si'4\trill sold' la' la' |
    \footnoteHere #'(0 . 0) \markup {
      Ballard 1719 : \italic { De rendre Mon cœur moins jaloux. }
    }
    si' do'' si'4.\trill la'8 |
    la'4 la' fad'\trill fad'8 fad' |
    sib'4 sib' sol'\trill sol'8 sol' |
    do''4( sib'8) do'' re''4 re'' |
    do''\trill do''8 re'' sib'4\trill( la'8) sib' |
    la'2\trill la'4 fa'' |
    re''2\trill r4 re'' |
    si'4\trill si'8 si' si'4( do''8) re'' |
    dod''4\trill dod'' re'' re'' |
    mi'' fa'' mi''4.\trill re''8 |
    re''1 |
  }
  \tag #'recit2 {
    \clef "vbasse-taille"
    R1*3 R2.*3 R1 R2.*3 R1*10 R2. R1 R2.*2 R2.*20 R2.*3 R2. R1
    R2. R1*7 R2. R1*5 R2. R1 R2. R1 R2. R1*5 R2. R1*4 R1 R1*18 R1*6
    R2. R1*6 R2. R1*3 R2. R1*2
    <>^\markup\character Hierax
    r2 r4 la |
    fad4\trill fad8 fad sol4 sol |
    mi mi8 mi fa4 fa |
    re\trill re sol mi |
    la fa sol sol, |
    do do' la\trill la |
    fa fa8 fa sib4 sib |
    sol\trill sol re' re' |
    dod' re' sib4\trill( la8) sib |
    la4\trill la re re |
    mi4\trill mi8 mi mi4. mi8 |
    fa4 fa8 fa fad4.\trill fad8 |
    sol2 sold |
    la4 la8 la la4 la |
    sib2 si |
    do'4 do'8 do' dod'4\trill dod' |
    re' re'8 do' si4\trill si |
    mi' mi' do' do' |
    sold la mi4.\trill mi8 |
    la4 la re' re' |
    sol sol8 sol do'4. do'8 |
    la4\trill la sib sib |
    la4. sib8 sol4.\trill fa8 |
    fa4 fa re re |
    sib,4 sib8 sib fad4\trill fad |
    sol sol8 sol mi4\trill mi |
    la la fa fa |
    dod re la,4. re8 |
    re1 |
  }
>>