\key do \major
\time 4/4 \midiTempo#80 s1*3
\digitTime\time 3/4 s2.*3
\time 4/4 s1
\digitTime\time 3/4 s2.*3
\time 4/4 s1*10
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.*2 \midiTempo#160 s2.*20
\bar ".!:" s2.*3 \alternatives s2. { \time 4/4 \midiTempo#80 s1 }
\digitTime\time 3/4 s2.
\time 4/4 s1*7
\digitTime\time 3/4 s2.
\time 4/4 s1*5
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*5
\digitTime\time 3/4 s2.
\time 4/4 s1*4
\digitTime\time 2/2 \midiTempo#160 s1
\digitTime\time 2/2 s1*18
\time 2/2 s1*6
\digitTime\time 3/4 \midiTempo#80 s2.
\time 4/4 s1*6
\digitTime\time 3/4 s2.
\time 4/4 s1*3
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 2/2 \midiTempo#160 s1*30 \bar "|."
