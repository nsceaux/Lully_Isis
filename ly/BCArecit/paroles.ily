\tag #'(recit1 basse) {
  M’ai -- mez- vous ? puis- je m’en flat -- ter ?

  Cru -- elle, en vou -- lez- vous dou -- ter ?
  En vain vostre in -- cons -- tance é -- clat -- te,
  en vain el -- le m’a -- nime à bri -- ser tous les nœuds,
  je vous ai -- me toû -- jours, in -- gra -- te,
  plus que vous ne vou -- lez, & plus que je ne veux.

  Je crains un fu -- nes -- te pré -- sa -- ge,
  un ai -- gle dé -- vo -- rant vient de fondre à mes yeux,
  sur un oy -- seau qui dans ces lieux,
  m’en -- tre -- te -- noit d’un doux ra -- ma -- ge.
  Dif -- fe -- rez nôtre hy -- men, sui -- vons l’a -- vis des cieux.

  Notre hy -- men ne dé -- plaît qu’à vô -- tre cœur vo -- la -- ge,
  ré -- pon -- dez- moy de vous, je vous ré -- pons des Dieux.
  Vous ju -- riez au -- tre -- fois que cette On -- de re -- bel -- le,
  se fe -- roit vers sa source u -- ne rou -- te nou -- vel -- le,
  plû -- tôt qu’on ne ver -- roit vô -- tre cœur dé -- ga -- gé :
  Voy -- ez cou -- ler ces flots dans cet -- te vas -- te plai -- ne,
  c’est le mê -- me pen -- chant qui toû -- jours les en -- traî -- ne,
  leur cours ne chan -- ge point, & vous a -- vez chan -- gé.
  Leur  - gé.

  Lais -- sez- moy re -- ve -- nir de mes fray -- eurs se -- cre -- tes ;
  j’at -- tens de vôtre a -- mour cet ef -- fort gé -- né -- reux.

  Je veux ce qui vous plaît, Cru -- el -- le que vous ê -- tes,
  vous n’a -- bu -- sez que trop d’un a -- mour mal- heu -- reux.

  Non, je vous aime en -- cor.

  Qu’el -- le froi -- deur ex -- tré -- me !
  In -- cons -- tante, est-ce ain -- si qu’on doit di -- re qu’on ai -- me ?

  C’est à tord que vous m’ac -- cu -- sez,
  vous a -- vez vû tou -- jours vos ri -- vaux mé -- pri -- sez.

  Le mal de mes ri -- vaux n’é -- ga -- le point ma pei -- ne,
  la douce il -- lu -- si -- on d’une es -- pe -- ran -- ce vai -- ne
  ne les fait point tom -- ber du faî -- te du bon- heur,
  au -- cun d’eux, com -- me moy, n’a per -- du vô -- tre cœur :
  comme eux, à vôtre hu -- meur sé -- ve -- re,
  je ne suis point ac -- coû -- tu -- mé :
  Quel tour -- ment de ces -- ser de plai -- re,
  lors qu’on a fait l’ef -- fay du plai -- sir d’être ai -- mé !
  Je ne le sens que trop, vô -- tre cœur se dé -- ta -- che,
  et je ne sçay qui me l’ar -- ra -- che !
  Je cherche en vain l’heu -- reux a -- mant
  qui me dé -- robe un bien char -- mant,
  où j’ay crû de -- voir seul pré -- ten -- dre ;
  je sen -- ti -- rois moins mon tour -- ment,
  si je trou -- vois à qui m’en pren -- dre.
  Je sen -- ti -- rois moins mon tour -- ment,
  si je trou -- vois à qui m’en pren -- dre.
  Vous fuy -- ez mes re -- gards, vous ne me dî -- tes rien.
  Il faut vous dé -- li -- vrer d’un fâ -- cheux en -- tre -- tien :
  ma pré -- sen -- ce vous blesse, & c’est trop vous con -- train -- dre.

  Ja -- loux, sombre & cha -- grin, par -- tout où je vous vois,
  vous ne ces -- sez point de vous plain -- dre ;
  je vou -- drois vous ai -- mer au -- tant que je le dois,
  et vous me for -- cez à vous crain -- dre.
  Je vou -- drois vous ai -- mer au -- tant que je le dois,
  et vous me for -- cez à vous crain -- dre.

  Non, non, il ne tient qu’à vous,
  il ne tient qu’à vous
  de ren -- dre nô -- tre sort plus doux.

  Non, non, il ne tient qu’à vous
  de ren -- dre nô -- tre sort plus doux.
  
  Non, non, il ne tient qu’à vous
  de ren -- dre mon cœur plus ten -- dre,
  de ren -- dre mon cœur plus ten -- dre.

  Non, non, il ne tient qu’à vous
  de ren -- dre nô -- tre sort plus doux.
  
  Non, non, il ne tient qu’à vous,
  il ne tient qu’à vous
  de ren -- dre mon cœur plus ten -- dre.

  Non, non, non, non, il ne tient qu’à vous
  de ren -- dre nô -- tre sort plus doux.
}
\tag #'recit2 {
  Non, non, il ne tient qu’à vous,
  il ne tient qu’à vous
  de ren -- dre nô -- tre sort plus doux.
  
  Non, non, non, non, il ne tient qu’à vous
  de ren -- dre nô -- tre sort plus doux.

  Non, non, non, non, il ne tient qu’à vous,
  il ne tient qu’à vous,
  non, non, il ne tient qu’à vous,
  non, non, il ne tient qu’à vous,
  il ne tient qu’à vous
  de ren -- dre nô -- tre sort plus doux.

  Non, non, non, non, il ne tient qu’à vous
  de ren -- dre mon cœur moins ja -- loux.
  
  Non, non, non, non, il ne tient qu’à vous,
  il ne tient qu’à vous
  de ren -- dre nô -- tre sort plus doux.
}
