\clef "dessus" R1*3 R2.*3 R1 R2.*3 R1*10 R2. R1 R2.*26 R1 R2. R1*7
R2. R1*5 R2. R1 R2. R1 R2. R1*5 R2. R1*4 R1 |
r4^"Violons" fa'' mi'' la'' |
fa''4. sib''8 \once\slurDashed sol''4.\trill( fa''16 sol'') |
la''4. la'8 la'4 si' |
do''4 do'' do'' si' |
do''4. mi''8 re''4 do'' |
si'4. la'8 la'4.\trill sold'8 |
la'1 |
r4 la' re'' do'' |
do'' do'' re''8 do'' sib' la' |
sib'2 sib'4 sib' |
la'4. fa''8 mi''4 re'' |
dod''1\trill |
r4 fa'' fa'' fa'' |
mi'' la'' la'' sol'' |
sol''4. sol''8 sol''4 fa'' |
fa'' sol'' mi''4.\trill re''8 |
re''1 |
R1*7 R2. R1*6 R2. R1*3 R2. R1*31
