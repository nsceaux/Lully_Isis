\clef "dessus" R1*3 R2.*3 R1 R2.*3 R1*10 R2. R1 R2.*26 R1 R2. R1*7
R2. R1*5 R2. R1 R2. R1 R2. R1*5 R2. R1*4 R1 |
r4 la'8 si' do''4 fa'' |
re''4. sol''8 mi''4.\trill fa''8 |
fa''4. do''8 do''4 re'' |
mi'' mi'' fa''4. sol''8 |
mi''4.\trill sol''8 fa''4 mi'' |
re''4.\trill do''8 si'4 mi'' |
dod''1 |
r4 fa'' fa'' sol'' |
la''4 la'' la''4. la''8 |
re''2 re''4 mi'' |
fa''4. la''8 sol''4 fa'' |
mi''1\trill |
r4 la' la' si' |
do''4 do'' re'' re'' |
mi''4. mi''8 mi''4 la' |
re'' mi'' dod''4.\trill re''8 |
re''1 |
R1*7 R2. R1*6 R2. R1*3 R2. R1*31
