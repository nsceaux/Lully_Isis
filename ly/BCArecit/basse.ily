\clef "basse" \once\tieDashed re,1~ |
re,2 \once\tieDashed re~ |
re1 |
sol,2. |
la, |
re2 do4 |
sib,2 si, |
do4 la,8 sib, sol,4 |
fa,2 fa4 |
dod2 re4 |
sol sol8 fa mi2 |
fa mi8 re la,4 |
re8 do \once\tieDashed sib,4~ sib,2 |
fa mi |
re1 | \allowPageTurn
do2 fad, |
sol, re8 do sib,4 |
la,2 la4 fad |
sol2 sold |
la4 la, re2 |
sol,2. |
do2. sib,4 |
la,4. sol,16 fa, do4 |
fa, fa8 sol fa mi |
re4. mi8 fa re |
la sib la sol fad4 |
sol sol,8 la, sib,4 |
sol, la,2 |
re,4. re8 do sib, |
la, sib, do4 sib,8 la, |
sol,2 sol8 fa |
mi re dod4 re |
la,8 si, dod4 la, |
re4. mi8 fa re |
mi4. fad8 sold4 |
la8 re mi4 mi, |
la,4. la,8 re do |
sib,8. la,16 sol,4. sol,8 |
do re do sib, la,4 |
sol,8 fa, mi,2 |
fa,2 fad,4 |
sol, sol8 fa mi fa |
mi re dod si, dod re |
la, sib, la, sol, fad,4 |
sol,4. la,8 sib, sol, |
la,4. si,8 dod la, |
re sol, la,2 |
re fad,4 |
re2 \once\tieDashed re'~ |
re'4 dod'2 |
re'2 re4. la8 |
sib4. sol8 la4. re8 |
la,4. la8 dod2 |
re fad, |
sol, mi, |
fa, sol, |
do2 re |
mi re4 |
do2 fa4. re8 |
mi2 fa8 re mi mi, |
la,4. la,8 fad,2 |
sol,4 sol re8. la16 sib8. sol16 |
la4. sol8 fa mi16 re do8. sib,16 |
la,4 sib, si, |
\once\tieDashed do2~ do8 do sib, la, |
sol,2 la,4 |
re2. mi4 |
<< sold,2. \\ \sugNotes { sold,2 mi,4 } >> | \allowPageTurn
la,2 sib,4 si, |
do4. la,8 re sib, do do, |
fa,4 fa2 fad4 |
sol4. fa8 mi fa re4 |
do re8 re, mi, fa, sol,4 |
la, la sol |
fa mi8 dod re sol, la,4 |
re,8 mi, fa,2. |
sib,2 la, |
sol, re8 do sib,4 |
la,4 la8 sib la sol fa mi |
<< r4 \\ \sug re >> re la fa |
sib4. sol8 do'4 do |
fa4. fa8 fa4 fa |
mi mi << { re re } \\ \sug re2 >> |
do4. do8 re4 mi |
fa4 re mi mi, |
la,2 la,8 si, dod la, |
re2 re4 mi |
fa4 fa fad4. fad8 |
sol4. la8 sib4 sol |
re'4. re'8 dod'4 re' |
la2 la8 sib la sol |
fa2 fa4 sol |
la4 la si si |
do'4. do'8 dod'4 re' |
sib sol la la, |
re2. re4 |
\tieDashed la,1~ |
la,1~ |
la,~ |
la,2 fad, | \tieSolid
sol,1 |
do2. sib,4 |
la,4. sib,8 do2 |
fa8. mi16 re2 |
la dod |
re mi fa2. mi4 |
re8 do sib,4 la,2 |
la fad |
sol4~ sol16 sol la sib do'8. fa16 do4 |
fa,2 fa4 |
mi la8 sib la4 la, |
re4 do8 sib, la, sol, fad,4 |
sol,~ sol,16 sol la sib do'8 fa do4 |
fa,2 fa4 |
mi la8 sib la4 la, |
re2. la4 | \allowPageTurn
re' re la la |
fad4. fad8 sol4 sol |
mi4. mi8 fa4 fa |
re re sol mi |
la fa sol sol, |
do do' la la |
fa4. fa8 sib4 sib |
sol sol re' re' |
dod' re' sib2 |
la4 la re re |
mi2. mi4 |
fa4. fa8 fad2 |
sol4. sol8 sold2 |
la la4. la8 |
sib2 si |
do'2 dod'4. dod'8 |
re'4. do'8 si4 si |
mi' mi' do' do' |
sold la mi2 |
la,4 la re' re |
sol4. sol8 do'4. do'8 |
la4 la sib sib |
la4. sib8 sol2 |
fa4 fa re re |
sib, sib << fad2 \\ \sugNotes { fad4. fad8 } >> |
sol4. sol8 mi4 mi |
la la fa fa |
dod re la,2 |
re, re4 mi |
\once\set Staff.whichBar = "|"
