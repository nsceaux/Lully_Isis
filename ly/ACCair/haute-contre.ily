\clef "haute-contre" sib'4. sib'8 la'4. la'8 |
sib'4. sol'8 la'4. la'8 |
re'8 sol' la' sib' do''4. do''8 |
la'4. la'8 re''4. re''8 |
sib'4. mib''8 do''4.\trill sib'8 |
sib'4. sib'8 sib'4. sib'8 |
do''4. do''8 do''4. sib'8 |
la'2~ la'8 fad' sol' la' |
sib'4. do''8 re''4 si' |
do''2 do''8 sib' la'4 |
sib'4 sib' la'4.\trill sib'8 |
sib'2 re''4. re''8 |
mib''2 re''4. re''8 |
sol'2 la'4. sib'8 |
do''4. sib'8 sib'4. do''8 |
la'2 la'4. la'8 |
sol'4. la'8 fad'4.\trill sol'8 |
sol'1 |
