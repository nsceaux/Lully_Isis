\clef "quinte" re'4. re'8 re'2 |
re'4. sib8 la4. la8 |
sol4 re' do'4. do'8 |
do'4. do'8 sib4. sib8 |
sib2 la4 fa |
fa4. sib8 re'4. re'8 |
do'4. do'8 re'4. re'8 |
re'1 |
re'2 re'4 re' |
do'2 do'4 do' |
sib sol la fa |
fa'2 sib2 |
sib4. do'8 re'4. re'8 |
do'2 do'4.\trill re'8 |
re'4. re'8 sib4. la8 |
la2.\trill re'8 re' |
sib4. la8 la4 re' |
re'1 |
