\clef "basse"
\footnoteHere #'(0 . 0) \markup { Matériel 1677 : B.C. tacet. }
sol4. sol8 re'4. do'8 |
sib4. sib8 fad2 |
sol4 sol, do8 do re mib |
fa4 mib re8 sib, do re |
mib2 fa4 fa, |
sib,2 sib |
la re'4 sol |
re2~ re8 re mi fad |
sol4. la8 si4 sol |
do'4. sib8 la4 fa |
sib mib fa fa, |
sib, sib8 la sol4. fa8 |
mib2 si,4. si,8 |
do4. sib,8 la,4. sol,8 |
fad,4 sol, mib4. do8 |
re4. mi8 fad2\trill |
sol4 do re re, |
sol,1 |
