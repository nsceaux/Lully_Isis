\clef "taille" sol'4. sol'8 <<
  \new CueVoice {
    s4^\markup\note-by-number #2 #1 #UP s^\markup\note-by-number #3 #0 #UP
  }
  { la'4 fad' | }
>>
sol'4 re'2 re'4 |
re'4. sol'8 sol'4. sol'8 |
fa'2 fa'4. fa'8 |
 <<
  \new CueVoice { s4^\markup\croche-pointee-double }
  { mib'8 fa' }
>> sol'4 fa'4. mib'8 |
re'4. fa'8 fa'4. sol'8 |
la'4. la'8 la'4. sol'8 |
fad'2~ fad'8 la' la' la' |
sol'2 sol'4. sol'8 |
sol'4. sol'8 fa'4. fa'8 |
fa'4 sol' fa'4. mib'8 |
re'4 re'2 sol'4 |
sol'2. sol'4 |
sol'8 fa' mi'4 fad'4. sol'8 |
la'4 re' mib'4. mib'8 |
re'2. re'4 |
re'4 mib' re'4. do'8 |
si1\trill |
