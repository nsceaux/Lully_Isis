\clef "dessus" sol''4. sol''8 fad''4 la'' |
re''4. re''8 do''4 re''8 la' |
sib' sib' do'' re'' mib''4. re''8 |
do''4.\trill do''8 fa''4. fa''8 |
sol''8. la''16 sol''8. la''16 la''4.\trill sib''8 |
sib''4. re''8 re''4. mi''8 |
fa''4. fa''8 fad''4.\trill sol''8 |
la''1 |
re''4. re''8 sol''4. fa''8 |
mi''4.\trill do''8 fa''4. mib''8 |
re''4 mib'' \once\slurDashed do''4.(\trill sib'16 do'') |
re''4 sib' sib''4. la''8 |
sol''4. fa''8 \once\slurDashed fa''4.( mib''16 re'') |
mib''4 do'' do'''4. sib''8 |
la''4. sib''8 sol''4. la''8 |
fad''4 mi''8 re'' do''4\trill sib'8 la' |
sib'4. do''8 la'4.\trill sol'8 |
sol'1 |
