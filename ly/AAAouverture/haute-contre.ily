\clef "haute-contre" sib'4. sib'8 sib'4. re''8 |
re''2. la'4 |
la'2. la'4 |
la'4. re''8 re''4. re''8 |
sol'4. sol'8 do''4. do''8 |
fa'4. << \sug sol'8 \\ fa'8 >> la'4. la'8 |
re'4. re'8 sol'4. sol'8 |
fa'4. la'8 la'4 sib' |
sol'4. sol'8 do''4. do''8 |
la'4. sib'8 sib'4 la' |
sib'2. r8 re'' |
re''4. re''8 re''4. re''8 |
sib'4. sib'8 do''4. do''8 |
do''4. sib'8 sib'2 |
la'1 |
la' |
R2. |
r4 sol' sol' |
la' la'8 sib' do'' la' |
sib'4 si'4. si'8 |
do''2 do''4 |
do'' sib'4. sib'8 |
do''4 do'' do'' |
re'' re'' re'' |
mib'' do''4. sib'8 |
la'4 la' re'' |
re'' do'' do'' |
do'' re''8 do'' sib' la' |
sib'4 sib' sib' |
do'' do'' do'' |
re''2 re''4 |
sol' do'' do'' |
do'' sib' sib' |
sib' do''8 re'' do'' sib' |
la'4. la'8 re''4 |
sib'2 sib'4 |
sib'4 la'4. sib'8 |
sib'2 sol'4 |
sol' fa'4. fa'8 |
fa'4 sib'4. sib'8 |
<< sib'4 \\ \sug sol' >> do'' do'' |
la'4. re''8 re''4 |
re'' do''4. do''8 |
do''4 do'' re'' |
do'' do''4. sib'8 |
la'4 sib'4. sib'8 |
sib'2 sib'4 |
la' do''4. do''8 |
sib'4. la'8 sol' fa' |
mi'4 la'4. la'8 |
fa'4 fa' sib' |
la'2 la'4 |
la' sol'4. sol'8 |
sol'4 la'4. la'8 |
la'2 fad'8 fad' |
sol'4 sol' sol' |
la' la' la' |
sol'2 re''8 re'' |
do''4 do'' do'' |
sib'8 do'' re''4. re''8 |
sol'4 sol'4. la'8 |
fad'4 la' re'' |
sib' do''4. do''8 |
do''4 re''4. re''8 |
re''2 re''4 |
sib' sib' do'' |
sib' la'4. sol'8 |
sol'2 sol'8 sol' |
la'4 la'4. la'8 |
re'4 sol'8 la' sib'4 |
do'' do''4. do''8 |
fa'4 sib'4. sib'8 |
sib'4 la'4. sol'8 |
fad'4.\trill( mi'16 fad') sol'4 |
sol' fad'4. sol'8 |
sol'2. |
R1*4 |
