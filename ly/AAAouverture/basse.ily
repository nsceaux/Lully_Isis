\clef "basse" sol,2 sol4. sol8 |
re2 re'4. re'8 |
re'2 dod'4. dod'8 |
re'4. re'8 si4. si8 |
do'4. do'8 la4. la8 |
sib4. sib8 fad4. fad8 |
sol4. sol8 mi4. mi8 |
fa4. fa8 re4. re8 |
mib4. mib8 do4. do8 |
fa2 fa,4. fa,8 |
sib,4. fa8 sib4. sib8 |
sol4. sol8 re4. re8 |
mib4. mib8 do4. do8 |
re4. re8 sol,4. sol,8 |
re,2 re4. re8 |
re,1 |
R2.*9 |
r4 re re |
mi mi mi |
\footnoteHere #'(0 . 0) \markup {
  Matériel 1677 : \italic fa♯
}
fad? fad fad |
sol sol sol |
la la la |
sib si si |
do'2. |
R2.*4 |
r4 fa fa |
sol sol sol |
la la la |
sib2. |
R2.*4 |
r4 do do |
re re re |
mi mi mi |
fa fa fa |
sol sol sol |
la la la |
sib sib sol |
la la, la, |
sib, si, si, |
do dod dod |
re re re |
mib mi mi |
fa fad fad |
sol sol sol |
la la la |
sib si si |
do' dod' dod' |
re'2. |
R2.*4 |
r4 re re |
mib mi mi |
fa fad fad |
sol sol, sol, |
la, la, la, |
sib, sib, sib, |
do do do |
re2 sib,4 |
do re re, |
sol,2.~ |
sol,2. sol4 |
do4. do16 do do4 sol, |
do sol, do sol, |
do1 |
