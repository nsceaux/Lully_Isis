\key re \minor
\midiTempo#160
\digitTime\time 2/2 s1*14 \alternatives s1 s1
\digitTime\time 3/4 \bar ".!:" \beginMarkSmall "Reprise" s2.*60 \bar ":|."
\digitTime\time 2/2 s1*4 \bar "|."
