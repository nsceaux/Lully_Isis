\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille)
   (quinte)
   (basse)
   (basse-continue)
   (timbales #:score-template "score-sug")
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
