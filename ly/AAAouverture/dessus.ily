\clef "dessus" sol''4. sol''8 re''4. sib''8 |
la''2\trill la''4. sib''8 |
sol''4.\trill sol''8 sol''4 la''8 mi'' |
fa''4. fa''8 fa''4. re''8 |
mib''4. mib''8 mib''4. do''8 |
re''4. re''8 do''4. re''8 |
sib'4.\trill sib'8 sib'4. do''8 |
la'4.\trill do''8 fa''4. fa''8 |
sib'4. sib'8 mib''4. mib''8 |
do''4. re''8 mib''4. fa''8 |
re''2.\trill r8 fa'' |
sib''4. sib''8 la''4.\trill sib''8 |
sol''4. sol''8 la''4. sib''8 |
fad''4.\trill sol''8 sol''4.\trill( fad''16 sol'') |
la''1 |
la'' |
r4 re'' re'' |
mi'' mi''8 fa'' sol''4 |
fad'' fad''8 sol'' la'' fad'' |
sol''4 re''4.\trill re''8 |
mib''4 mib''4. fa''8 |
re''4. re''8 sol''4 |
sol'' fa''4.\trill fa''8 |
fa''4 sib''4. sib''8 |
sib''4 la''4. sol''8 fad''2\trill fad''8 fad'' |
sol''4 sol''4.(\trill fad''16 sol'') |
la''2. |
re''4 sol''4. sol''8 |
sol''4 fa''4. fa''8 |
fa''4 sol''8 fa'' mib'' re'' |
mib''2 mib''8 mib'' |
fa''4 fa''4.(\trill mib''16 fa'') |
sol''4. fa''8 mib'' re'' |
do''4.\trill do''8 fa''4 |
re'' re'' mib'' |
do'' do''4.\trill sib'8 |
sib'4. do''8 re'' mib'' |
do''4.\trill do''8 fa''4 |
re'' sol''4. sol''8 |
sol''4 mi''4.\trill mi''8 |
fa''4. sol''8 la'' sib'' |
sol''4.\trill sol''8 la'' sib'' |
la''4.\trill la''8 sib''4 |
sol'' sol''4.\trill fa''8 |
fa''2 fa''8 fa'' |
sol''4 sol''4.(\trill fa''16 sol'') |
la''2~ la''8 la'' |
sib'' la'' sol'' fa'' mi'' re'' |
dod''4. re''8 mi'' fa'' |
re''4. re''8 sol''4 |
fa''8 mi'' mi''4.\trill re''8 |
re''4 sol''4. sol''8 |
mi''4. mi''8 la''4 |
fad''2\trill re''4 |
re'' do''4. do''8 |
do''2 re''8 la' |
sib'2 sib''8 sib'' |
la''4. sol''8 fa'' \footnoteHere #'(0 . 0) \markup {
  Matériel 1677 : \italic mi♭
}
mib''? |
re''4. mi''8 fa''4 |
mi'' la''4. la''8 |
la''4 fad''4.\trill fad''8 sol''4 sol''4.(\trill fad''16 sol'') |
la''4 la''4. la''8 |
sib''4. sib''8 la'' sib'' |
sol''4.\trill sol''8 la''4 |
sib'' fad''4.\trill sol''8 |
sol''2 do''8 do'' |
do''4 re''8 do'' sib' la' |
sib'4 mib''4. mib''8 |
mib''?4 fa''8 mib''! re'' do'' |
re''4. mib''8 fa'' re'' |
mib''4 do''4.\trill do''8 |
do''4 la' re'' |
do''8 sib' la'4.\trill sol'8 |
sol'2. |
R1*4 |
