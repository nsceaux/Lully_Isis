\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "dessus" >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff << \global \includeNotes "taille" >>
    \new Staff <<
      \global \includeNotes "quinte"
      \keepWithTag #'quinte \includeFigures "chiffres"
    >>
    \new Staff <<
      \global \includeNotes "basse"
      \keepWithTag #'basse \includeFigures "chiffres"
      \origLayout {
        s1*7\break s1*9\pageBreak
        s2.*10\break s2.*10\pageBreak
        s2.*11\break s2.*11\pageBreak
        s2.*10\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
