\score {
  \new ChoirStaff <<
    \new Staff \withLyricsB <<
      \global \keepWithTag #'voix1 \includeNotes "voix"
    >> \keepWithTag #'voix1-couplet1 \includeLyrics "paroles"
    \keepWithTag #'voix1-couplet2 \includeLyrics "paroles"
    \new Staff \withLyricsB <<
      \global \keepWithTag #'voix2 \includeNotes "voix"
    >> \keepWithTag #'voix2-couplet1 \includeLyrics "paroles"
    \keepWithTag #'voix2-couplet2 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2 s1*3 s2 \break s2 s1*3 s2 \bar "" \break s2 s1*4 s2 \bar "" \break
      }
    >>
  >>
  \layout { }
  \midi { }
}