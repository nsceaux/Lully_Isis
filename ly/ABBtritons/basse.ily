\clef "basse" sol2 |
sol,8 la, si, do re4. si,8 |
do re do si, la, si, la, sol, |
fad,4 sol, re re, |
sol,2 sol8 sol sol fad |
mi4. mi8 re4. re8 |
do4. do8 si,4. si,8 |
la,4. la,8 mi4 do |
re re, re re8 mi |
fa4 fa8 sol la4 la, |
mi4. mi8 si,4. si,8 |
do4. do8 la,8 si, do la, |
re4 sol, re, re |
sol8 la si do' re'4 re8 mi |
fa4 fa8 sol la4 la, |
mi4. mi8 si,4. si,8 |
do4. do8 la, si, do la, |
re4 sol, re, re |
sol,2
