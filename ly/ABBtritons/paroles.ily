\tag #'(voix1-couplet1 voix2-couplet1 basse) {
  C’est le Dieu des Eaux qui va pa -- rais -- tre,
  ran -- geons- nous près de nô -- tre Maî -- tre :
  En -- chaî -- nons les vents
  les plus ter -- ri -- bles,
  que le bruit des flots céde à nos Chants ;
  Ré -- gnez, Zé -- phirs,
  ré -- gnez, \tag #'(voix1-couplet1 basse) { Zé -- phirs, } Zé -- hirs pai -- si -- bles,
  ra -- me -- nez le doux Prin -- temps.
  Ré -- gnez, Zé -- phirs,
  ré -- gnez, \tag #'(voix1-couplet1 basse) { Zé -- phirs, } Zé -- hirs pai -- si -- bles,
  ra -- me -- nez le doux Prin -- temps.
}
\tag #'(voix1-couplet2 voix2-couplet2) {
  Fuy -- ez loin d’i -- cy, cru -- els o -- ra -- ges,
  rien ne doit trou -- bler ces Ri -- va -- ges.
}
