<<
  \tag #'(voix1 basse) {
    \clef "vpetite-haute-contre" si8 do' re' mi' |
    re'4 sol' fad'4. sol'8 |
    mi'4 mi' do'4. do'8 |
    do'4 si la si8 do' |
    si4\trill sol re'8 re' mi' fad' |
    sol'4. la'8 fad'4. sol'8 |
    mi'4 mi' re'4. mi'8 |
    do'4. re'8 si8. do'16 do'8.\trill si16 |
    la2\trill fad'4 fad'8 sol' |
    la'2 mi'4 mi'8 fad' |
    sol'4. sol'8 fa'4. sol'8 |
    mi'4\trill mi' la'4. la'8 |
    fad'4 sol' fad'4.\trill sol'8 |
    sol'2 fad'4 fad'8 sol' |
    la'2 mi'4 mi'8 fad' |
    sol'4. sol'8 fa'4. sol'8 |
    mi'4\trill mi' la'4. la'8 |
    fad'4 sol' fad'4.\trill sol'8 |
    sol'2
  }
  \tag #'voix2 {
    \clef "vtaille" sol8 la si do' |
    si4 si la4.\trill si8 |
    sol4 sol la4. la8 |
    la4 sol sol sol8 fad |
    sol4 sol si8 si dod' re' |
    re'4 do' do' si |
    si\trill la la sol |
    sol fad sol8. la16 la8.\trill sol16 |
    fad2\trill r |
    la4 la8 si do'2 |
    si4 si8 do' re'4. re'8 |
    re'4 do' do'4. do'8 |
    do'4 si la4.\trill sol8 |
    sol2 r |
    la4 la8 si do'2 |
    si4 si8 do' re'4. re'8 |
    re'4 do' do'4. do'8 |
    do'4 si la4.\trill sol8 |
    sol2
  }
>>