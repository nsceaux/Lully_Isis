\clef "quinte" r8 mi' re' |
do' do' do' |
do' fa la |
sol sol sol |
sol r r |
R4.*31 |
r8 la la |
sol r r |
r mi' mi' |
do' r r |
r la la |
sol r r |
R4.*2 |
r8 mi' re' |
do' do' do' |
do' fa la |
sol sol sol |
sol r r |
R4.*42 |
r8 do' mi' |
re' r r |
r re' re' |
do' r r |
r do' do' |
do' la re' |
re' r r |
R4.*2 |
r8 mi' re' |
do' do' do' |
do' fa la |
sol sol sol |
sol4. |
