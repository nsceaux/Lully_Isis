\clef "haute-contre" r8 sol' sol' |
la' la' sol' |
fa' la' re'' |
si' si' si' |
do'' r r |
R4.*31 |
r8 fad' fad' |
sol' r r |
r sol' sol' |
fa' r r |
r re'' re'' |
si' r r |
R4.*2 |
r8 sol' sol' |
la' la' sol' |
fa' la' re'' |
si' si' si' |
do'' r r |
R4.*42 |
r8 do'' do'' |
si' r r |
r la' si' |
do'' r r |
r do'' do'' |
la' re'' re'' |
<< re'' \\ \sug si' >> r r |
R4.*2 |
r8 sol' sol' |
la' la' sol' |
fa' la' re'' |
si' si' si' |
do''4. |
