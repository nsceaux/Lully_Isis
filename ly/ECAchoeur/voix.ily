<<
  \tag #'(vdessus basse) {
    <<
      \tag #'basse { s4.*30 s8 \ffclef "vdessus" }
      \tag #'vdessus { \clef "vdessus" R4.*30 r8 <>^\markup\character Chœurs }
    >> mi''8 mi'' |
    do''4 do''8 |
    fa'' fa'' fa'' |
    mi'' mi'' fa'' |
    re'' re'' re'' |
    re'' re'' dod'' |
    re'' r r |
    r re'' re'' |
    mi'' r r |
    r do'' do'' |
    fa'' r r |
    r re'' mi'' |
    do'' do'' fa'' |
    re'' re'' sol'' |
    mi'' <<
      \tag #'basse { s4 s4.*18 s8 \ffclef "vdessus" }
      \tag #'vdessus { r8 r R4.*18 r8 }
    >> mi''8 mi'' |
    re''4 mi''8 |
    la' la' si' |
    do''4 do''8 |
    r do'' do'' |
    la' la' re'' |
    si'\trill si' mi'' |
    la'4 re''8 |
    si'4\trill re''8 |
    mi''8. mi''16 mi''8 |
    mi''8 do'' mi'' |
    la' la' fa'' |
    re''8. re''16 re''8 |
    re''8 re'' mi'' |
    dod''4\trill dod''8 |
    r re'' re'' |
    mi''4 fa''8 |
    mi'' mi'' la'' |
    fad''4 re''8 |
    si'8. si'16 si'8 |
    si'8 si' mi'' |
    la'4 la'8 |
    r fa'' fa'' |
    re''4 sol''8 |
    mi'' fa'' re'' |
    mi'' mi'' mi'' |
    re''4 mi''8 |
    do'' re'' si' |
    do'' r r |
    r re'' mi'' |
    fa'' r r |
    r mi'' fa'' |
    sol'' r r |
    R4. |
    r8 re'' sol'' |
    mi'' mi'' fa'' |
    re'' re'' sol'' |
    mi'' r r |
    R4.*4 |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" R4.*30 |
    r8 sol' sol' |
    la'4 la'8 |
    la' la' la' |
    la' la' la' |
    fa' fa' sol' |
    mi' mi' la' |
    fad' r r |
    r sol' sol' |
    sol' r r |
    r la' la' |
    la' r r |
    r sol' sol' |
    mi' mi' la' |
    sol' sol' sol' |
    sol' r r |
    R4.*18 |
    r8 sol' sol' |
    sol'4 sol'8 |
    fa' fa' fa' |
    mi'4 mi'8 |
    r la' la' |
    fad' fad' fad' |
    sol' sol' sol' |
    sol'4 fad'8 |
    sol'4 sol'8 |
    sol'8. sol'16 sol'8 |
    sol'8 mi' sol' |
    fa' fa' la' |
    fa'8. fa'16 fa'8 |
    fa' fa' sol' |
    mi'4\trill mi'8 |
    r8 la' la' |
    sol'4 la'8 |
    la' la' la' |
    la'4 fad'8 |
    sol'8. sol'16 sol'8 |
    sol' sol' sol' |
    fa'4 fa'8 |
    r la' la' |
    sol'4 sol'8 |
    la' la' sol' |
    sol' sol' sol' |
    sol'4 sol'8 |
    mi' fa' re' |
    mi' r r |
    r sol' sol' |
    la' r r |
    r la' la' |
    sol' r r |
    R4. |
    r8 sol' sol' |
    sol' sol' la' |
    sol' sol' sol' |
    sol' r r |
    R4.*4 |
  }
  \tag #'(vtaille basse) {
    \clef "vtaille" R4.*4 |
    r8 <>^\markup\character Deux conducteurs de Chalybes mi' do' |
    re' re' si |
    do' do' fa' |
    re' re' sol' |
    mi' <<
      \tag #'basse { s4 s4.*3 \ffclef "vtaille" }
      \tag #'vtaille { r8 r | R4.*3 }
    >>
    r8 r si |
    do'4 do'8 |
    la la la |
    re' re' re' |
    si4 si8 |
    r mi' mi' |
    la4 la8 |
    si si si |
    sold mi' mi' |
    dod' <<
      \tag #'basse { s4. \ffclef "vtaille" }
      \tag #'vtaille { r8 r | r }
    >> re'8 re' |
    si <<
      \tag #'basse { s4. \ffclef "vtaille" }
      \tag #'vtaille { r8 r | r }
    >> sol8 do' |
    la4\trill la8 |
    la la re' |
    si si mi' |
    do' do' fa' |
    re' re' sol' |
    mi' <<
      \tag #'basse { s4 s4.*13 s8 \ffclef "vtaille" }
      \tag #'vtaille {
        do'8 do' |
        do'4 do'8 |
        re' re' re' |
        do' do' do' |
        sib sib sib |
        la la la |
        la r r |
        r si si |
        do' r r |
        r la la |
        re' r r |
        r si si |
        do' do' do' |
        do' do' si |
        do'
      }
    >> r8 r |
    R4.*3 |
    r8 mi' mi' |
    re'4 mi'8 |
    la la si |
    do'4 do'8 |
    r do' do' |
    la la re' |
    si si mi' |
    la4 re'8 |
    si4\trill re'8 |
    mi'8. mi'16 mi'8 |
    mi'8 mi' mi' |
    la4 la8 |
    r la re' |
    si4\trill mi'8 |
    do' re' si |
    do'
    \tag #'vtaille {
      do'8 do' |
      si4 do'8 |
      do' do' re' |
      do'4 do'8 |
      r mi' mi' |
      re' re' re' |
      sol mi' mi' |
      re'4 re'8 |
      re'4 si8 |
      do'8. do'16 do'8 |
      do' do' do' |
      do' do' do' |
      sib8. sib16 sib8 |
      sib sib sib |
      la4 la8 |
      r8 la re' |
      dod'4 re'8 |
      re' re' dod' |
      re'4 re'8 |
      re'8. re'16 re'8 |
      re'8 re' do' |
      do'4 do'8 |
      r re' re' |
      si4 mi'8 |
      do' re' si |
      do' do' do' |
      si4 si8 |
      la la sol |
      sol r r |
      r si dod' |
      re' r r |
      r do' do' |
      do' r r |
      R4. |
      r8 si si |
      do' do' do' |
      do' do' si |
      do' r r |
      R4.*4 |
    }
  }
  \tag #'(vbasse basse) {
    <<
      \tag #'basse { s4.*8 s8 \ffclef "vbasse" }
      \tag #'vbasse {
        \clef "vbasse" R4.*4 |
        r8 do' do' |
        si si sol |
        la la fa |
        sol sol sol, |
        do
      }
    >> do8 do |
    sol sol sol |
    sol4 fad8 |
    sol4 sol8 |
    <<
      \tag #'basse { s4.*9 s8 \ffclef "vbasse" }
      \tag #'vbasse {
        R4.*5 |
        r8 do do |
        fa4 fa8 |
        re re re |
        mi r r |
        r
      }
    >> la8 la |
    re <<
      \tag #'basse { s4. \ffclef "vbasse" }
      \tag #'vbasse { r8 r | r }
    >> sol8 sol |
    mi
    \tag #'vbasse {
      mi8 mi |
      fa4 fa8 |
      re re re |
      sol sol mi |
      la la fa |
      sol sol sol, |
      do do do |
      fa4 fa8 |
      re re re |
      la la fa |
      sib sib sol |
      la la la, |
      re r r |
      r sol sol |
      mi r r |
      r fa fa |
      re r r |
      r sol mi |
      la la fa |
      sol sol sol, |
      do r r |
      R4.*3 |
      r8 do do |
      sol4 mi8 |
      fa fa re |
      la4 la8 |
      r la, la, |
      re re re |
      mi mi do |
      re4 re8 |
      sol,4 sol8 |
      do'8. do'16 do'8 |
      do' do' do |
      fa4 fa8 |
      r re re |
      sol4 mi8 |
      la fa sol |
      do do do |
      sol4 mi8 |
      fa fa re |
      la4 la8 |
      r la, la, |
      re re re |
      mi mi do |
      re4 re8 |
      sol,4 sol8 |
      do'8. do'16 do'8 |
      do' do' do |
      fa fa fa |
      sib8. sib16 sib8 |
      sib sib sol |
      la4 la8 |
      r fa fa |
      mi4 re8 |
      la la la |
      re4 re8 |
      sol8. sol16 sol8 |
      sol sol mi |
      fa4 fa8 |
      r re re |
      sol4 mi8 |
      la fa sol |
      do do do |
      sol4 mi8 |
      la fa sol |
      do r r |
      r sol sol |
      re r r |
      r la la |
      mi r r |
      R4. |
      r8 sol sol |
      la la fa |
      sol sol sol, |
      do r r |
      R4.*4 |
    }
  }
>>