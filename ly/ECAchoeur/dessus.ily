\clef "dessus" r8 mi'' sol'' |
do'' do'' mi'' |
la' do'' fa'' |
re'' sol'' re'' |
mi''\trill r r |
R4.*31 |
r8 la' re'' |
si' r r |
r sol' do'' |
la' r r |
r fa'' fa'' |
re'' r r |
R4.*2 |
r8 mi'' sol'' |
do'' do'' mi'' |
la' do'' fa'' |
re'' sol'' re'' |
mi''\trill r r |
R4.*42 |
r8 mi'' do'' |
re'' r r |
r fa'' re'' |
mi'' r r |
r sol'' mi'' |
la'' fa'' la'' |
re'' r r |
R4.*2 |
r8 mi'' sol'' |
do'' do'' mi'' |
la' do'' fa'' |
re'' re'' sol'' |
mi''4.\trill |
