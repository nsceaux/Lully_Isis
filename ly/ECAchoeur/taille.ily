\clef "taille" r8 do' re' |
mi' mi' mi' |
do' do' re' |
re' re' do' |
do' r r |
R4.*31 |
r8 re' re' |
re' r r |
r do' do' |
do' r r |
r re' re' |
re' r r |
R4.*2 |
r8 do' re' |
mi' mi' mi' |
do' do' re' |
re' re' re' |
do' r r |
R4.*42 |
r8 sol' sol' |
sol' r r |
r fa' fa' |
mi' r r |
r mi' sol' |
fa' la' la' |
sol' r r |
R4.*2 |
r8 do' re' |
mi' mi' mi' |
do' do' re' |
re' re' re' |
do'4. |
