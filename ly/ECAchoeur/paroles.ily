\tag #'(vtaille vbasse basse) {
  Tôt \ru#10 tôt tôt.
}
\tag #'(vbasse basse) {
  Que cha -- cun a -- vec soin s’em -- pres -- se.
}
\tag #'(vtaille basse) {
  For -- gez, for -- gez, qu’on tra -- vail -- le sans ces -- se,
}
\tag #'(vtaille vbasse basse) {
  qu’on pré -- pa -- re tout ce qu’il faut.
}
\tag #'(vtaille basse) {
  Tôt tôt tôt.
}
\tag #'(vbasse basse) {
  Tôt tôt tôt.
}
\tag #'(vtaille basse) {
  Tôt tôt tôt ;
}
\tag #'(vbasse basse) {
  Tôt tôt tôt ;
}
\tag #'(vtaille vbasse basse) {
  Qu’on pré -- pa -- re tout ce qu’il faut,
  \ru#8 tôt tôt.
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Qu’on pré -- pa -- re tout ce qu’il faut,
  \ru#8 tôt tôt.
  Tôt tôt tôt,
  tôt tôt tôt,
  \ru#8 tôt tôt.
}
\tag #'(vtaille vbasse basse) {
  Que le feu des for -- ges s’al -- lu -- me,
  tra -- vail -- lons, tra -- vail -- lons d’un ef -- fort nou -- veau ;
  qu’on fas -- se re -- ten -- tir l’en -- clu -- me
  sous les coups pe -- sants du mar -- teau.
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Que le feu des for -- ges s’al -- lu -- me,
  tra -- vail -- lons, tra -- vail -- lons d’un ef -- fort nou -- veau ;
  qu’on fas -- se re -- ten -- tir l’en -- clu -- me,
  qu’on fas -- se re -- ten -- tir l’en -- clu -- me,
  sous les coups pe -- sants du mar -- teau,
  qu’on fas -- se re -- ten -- tir l’en -- clu -- me,
  sous les coups pe -- sants du mar -- teau,
  sous les coups pe -- sants du mar -- teau.
  Tôt tôt tôt.
  Tôt tôt tôt.
  Tôt \ru#7 tôt tôt.
}
