\clef "basse" \tag #'basse <>^"[Tous]" do8 do' si |
la la mi |
fa fa re |
sol sol sol, |
do \tag #'basse <>^"[B.C.]" do'[ do'] |
si si sol |
la la fa |
sol sol sol, |
do do do |
si, si, si, |
la, la, la, |
sol,4 sol,8 |
sol8 sol sol |
mi mi mi |
fa fa fa |
fad8. mi16 re8 |
sol4 sol,8 |
do do do |
fa4 fa8 |
re re re |
mi mi mi |
la la la |
re re re |
sol sol sol |
mi mi mi |
fa4 fa8 |
re re re |
sol sol mi |
la la fa |
sol sol sol, |
do do do |
fa4 fa8 |
re re re |
la la fa |
sib sib sol |
la la la, |
re \tag #'basse <>^"[tous]" re[ re] |
sol \tag #'basse <>^"[B.C.]" sol[ sol] |
mi \tag #'basse <>^"[tous]" mi[ mi] |
fa \tag #'basse <>^"[B.C.]" fa[ fa] |
re \tag #'basse <>^"[tous]" re[ re] |
sol \tag #'basse <>^"[B.C.]" sol[ mi] |
la la fa |
sol sol sol, |
do \tag #'basse <>^"[tous]" do'[ si] |
la la mi |
fa fa re |
sol sol sol, |
do \tag #'basse <>^"[B.C.]" do[ do] |
sol4 mi8 |
fa fa re |
la4 la8 |
la,4 la,8 |
re re re |
mi mi do |
re4 re8 |
sol,4 sol8 |
do'8. do'16 do'8 |
do' do' do |
fa4 fa8 |
re4 re8 |
sol4 mi8 |
la fa sol |
do do do |
sol4 mi8 |
fa fa re |
la4 la8 |
la,4 la,8 |
re re re |
mi mi do |
re4 re8 |
sol,4 sol8 |
do'8. do'16 do'8 |
do' do' do |
fa fa fa |
sib8. sib16 sib8 |
sib sib sol |
la4 la8 |
fa4 fa8 |
mi4 re8 |
la la la |
re4 re8 |
sol8. sol16 sol8 |
sol sol mi |
fa4 fa8 |
re4 re8 |
sol4 mi8 |
la fa sol |
do do do |
sol4 mi8 |
la fa sol |
do \tag #'basse <>^"[tous]" do'[ do] |
sol \tag #'basse <>^"[B.C.]" sol[ sol] |
re \tag #'basse <>^"[tous]" re'[ re] |
la \tag #'basse <>^"[B.C.]" la[ la] |
mi \tag #'basse <>^"[tous]" mi[ mi] |
fa fa fa |
sol \tag #'basse <>^"[B.C.]" sol[ sol] |
la la fa |
sol sol sol, |
do \tag #'basse <>^"[tous]" do'[ si] |
la la mi |
fa fa re |
sol sol sol, |
do4. |
