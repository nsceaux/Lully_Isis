\piecePartSpecs
#`((trompette #:score "score-trompette")
   (dessus #:score "score-dessus")
   (haute-contre #:score-template "score-voix")
   (taille #:score-template "score-voix"
                 #:music , #{
 s1*15 s1*4 s2. s1*6 s2.
 s1*34\break s1 s1*26\break s1*7\break s1*7\break #})
   (quinte #:score-template "score-voix")
   (basse #:score-template "score-voix")
   (basse-continue #:score-template "score-voix" #:tag-notes basse)
   (timbales #:score "score-timbales")
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
