\clef "dessus" \tag #'dessus2 \startHaraKiri
R1*19 R2. R1*6 R2. << \ru#34 { s1\allowPageTurn } R1*34 >> |
r4 mi''8 mi'' re''4 re'' |
mi'' mi''8 mi'' do''4 fa''8 fa'' |
re''4 sol''8 sol'' mi''4 fa'' |
re'' mi'' re''4.\trill do''8 |
do''4 r r2 |
R1*3 |
r4 la'8 la' re''4 re'' |
mi''4 mi''8 mi'' do''4 re''8 re'' |
si'4 mi''8 mi'' do''4 re'' |
si' do'' si'4.\trill la'8 |
la'4 r r2 |
r4 si'8 si' mi''4 mi'' |
do'' do''8 do'' re''4 re''8 re'' |
mi''4 mi''8 mi'' do''4 do'' |
la' si' la'4.\trill sol'8 |
sol'4 r r2 |
R1*4 |
r4 \twoVoices #'(dessus1 dessus2 dessus) <<
  { mi''8 fa'' sol''4 sol''8 fa'' |
    mi''4\trill mi''8 fa'' sol'' la'' sol'' fa'' |
    mi''4\trill sol''8 fa'' mi''4 re''8 do'' |
    si'4\trill re''8 mi'' mi''4.\trill re''16 do'' |
    re''8 do'' re'' mi'' mi''4.\trill re''16 do'' |
    re''4 si'8 do'' re''4 re''8 re'' |
    mi''4 }
  { \stopHaraKiri do''8 re'' mi''4 mi''8 re'' |
    do''4 do''8 re'' mi'' fa'' mi'' re'' |
    do''4 r4 r2 |
    r4 sol'8 sol' do''4. do''16 do'' |
    sol'4 sol'8 sol' do''4. do''16 do'' |
    sol'4 r r2 |
    r4
  }
>> \twoVoices #'(dessus1 dessus2 dessus) <<
  { sol''8 fa'' mi''4 mi''8 re'' |
    do''4 sol''8 fa'' mi'' fa'' mi'' re'' |
    do''4 mi''8 mi'' fa''4 fa'' |
    sol'' sol''8 sol'' do''4 fa''8 fa'' |
    re''4 sol''8 sol'' mi''4 fa'' |
    re'' mi'' re''4.\trill do''8 |
    do''4 }
  { mi''8 fa'' sol''4 sol''8 fa'' |
    mi''4 mi''8 fa'' sol'' la'' sol'' fa'' |
    mi''4 r r2 | \startHaraKiri
    R1*3 |
    r4
  }
>> \twoVoices #'(dessus1 dessus2 dessus) <<
  { mi''8 fa'' sol''4 sol''8 fa'' |
    mi''4 mi''8 fa'' sol'' la'' sol'' fa'' |
    mi''4 sol''8 fa'' mi''4 re''8 do'' |
    si'4\trill re''8 mi'' mi''4.\trill re''16 do'' |
    re''8 do'' re'' mi'' mi''4.\trill re''16 do'' |
    re''8 mi'' fa'' mi'' re''4.\trill do''8 |
    do''4 mi''8 mi'' fa''4 fa'' |
    sol'' sol''8 sol'' do''4 fa''8 fa'' |
    re''4 sol''8 sol'' mi''4 fa'' |
    re'' mi'' re''4.\trill do''8 |
    do''4 }
  { \stopHaraKiri do''8 re'' mi''4 mi''8 re'' |
    do''4 do''8 re'' mi'' fa'' mi'' re'' |
    do''2 r |
    r4 sol'8 sol' do''4. do''16 do'' |
    sol'4 sol'8 sol' do''4. do''16 do'' |
    sol'4 do'' sol'4. do''8 |
    do''2 r |
    \startHaraKiri R1*3 |
    r4
  }
>> \twoVoices #'(dessus1 dessus2 dessus) <<
  { mi''8 fa'' sol''4 fa''8 mi'' |
    re''4 sol''8 sol'' mi''4 fa'' |
    re'' mi'' re''4.\trill do''8 | }
  { \stopHaraKiri sol''8 fa'' mi''4 re''8 do'' |
    << { si'4 si'8 si' } \\ { sol'4 sol'8 sol' } >> do''4 do'' |
    sol'4 do'' sol'4. do''8 |
  }
>> 
do''1\fermata |
