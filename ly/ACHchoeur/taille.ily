\clef "taille" R1*19 R2. R1*6 R2. R1*34 |
r4 sol'8 sol' sol'4 sol' |
sol' sol'8 sol' la'4 la'8 la' |
sol'4 sol'8 sol' la'4 la' |
sol' sol' sol'4. sol'8 |
mi'4\trill r r2 |
R1*3 |
r4 la'8 la' sol'4 sol' |
sol'4 sol'8 sol' fa'4 fa'8 fa' |
mi'4 mi'8 mi' fa'4 fa' |
mi' mi' mi'4. mi'8 |
dod'4\trill r r2 |
r4 re'8 re' sol'4 sol' |
mi' fa'8 fa' fa'4 fa'8 fa' |
mi'4 mi'8 mi' mi'4 mi' |
re' re' re'4. re'8 |
si4\trill r r2 |
R1*4 |
r4 sol'8 fa' mi'4 mi'8 fa' |
sol'4 sol'8 fa' mi' re' mi' fa' |
sol'4 mi'8 fa' sol'4 fa'8 mi' |
re'4 sol'8 sol' sol'4 sol' |
sol'4. sol'16 sol' sol'4 sol' |
\footnoteHere #'(0 . 0) \markup {
  Ballard 1719 : \raise #1 \score {
    { \tinyQuote \clef "petrucci-c2"
      mi'4 mi'8 mi' mi'4 sol'8 sol' | }
    \layout { \quoteLayout }
  }
}
\sugNotes { sol'4 re'8 re' re'4 } sol'8 sol' |
sol'4 do'8 re' mi'4 mi'8 fa' |
sol'4 do'8 re' mi' re' mi' fa' |
sol'4 sol'8 sol' la'4 la' |
sol' sol'8 sol' fa'4 fa'8 fa' |
sol'4 sol'8 sol' la'4 la' |
sol' sol' sol'4. sol'8 |
mi'4 sol'8 fa' mi'4 mi'8 fa' |
sol'4 sol'8 fa' mi' re' mi' fa' |
sol'4 mi'8 fa' sol'4 fa'8 mi' |
re'4 sol'8 sol' sol'4. sol'8 |
sol'4 sol'8 sol' sol'4 sol' |
sol' sol' sol'4. fa'8 |
mi'4\trill sol'8 sol' la'4 la' |
sol' sol'8 sol' fa'4 fa'8 fa' |
sol'4 sol'8 sol' la'4 la' |
sol' sol' sol'4. sol'8 |
mi'4\trill mi'8 fa' sol'4 fa'8 mi' |
re'4 mi' mi' fa' |
sol'4 sol' sol'4. << sol'8 \\ \sug fa' >> |
mi'1\trill\fermata |
