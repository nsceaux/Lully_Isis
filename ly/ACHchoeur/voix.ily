<<
  \tag #'(vdessus basse) {
    \clef "vdessus" <>^\markup\character La Renommée
    R1 |
    r4 mi''8 re'' do''4 mi'' |
    la'4 la'8 la' re''4 re''8 re'' |
    si'4 si'8 do'' re''4. re''8 |
    mi''[ re''] do''[ si'] la'4.\trill sol'8 |
    sol'4 sol'8 sol' do''4 do'' |
    la' do''8 do'' fa''4 fa''8 fa'' |
    re''4\trill si'8 si' do''4. do''8 |
    re''4 mi'' re''4.\trill do''8 |
    do''4 %{%} do''8 do'' si'4 dod'' |
    re''4 la'8 si' do''4 re'' |
    mi'' do''8 do'' la'4 re''8 re'' |
    si'4\trill mi''8 mi'' do''4 re'' |
    si' do'' si'4.\trill do''8 |
    do''1 |
    <>^\markup\character La Renommée
    r2 r4 sol'8 sol' |
    mi'4\trill mi'8 mi' fa'4 sol' |
    la'2 la'4 la' |
    la' sol' fa'\trill( mi'8) fa' |
    mi'8\trill mi' mi' la' fad'8.\trill sol'16 |
    sol'4 sol'8 la' si'4\trill si'8 do'' |
    re''4 re''8 re'' re''4\trill re''8 dod'' |
    re''4 la'8 la' la'4\trill la'8 si' |
    do''4 do''8 re'' mi''2~ |
    mi'' mi''4 re'' |
    mi''2 mi''4 si'8 si' |
    do''8 do''16 do'' la'8\trill la'16 la' la'8 si'16 do'' |
    si'4\trill sol'8 sol' re''4 re''8 mi'' |
    fa''2. re''8 re'' |
    re''2 re''4. mi''8 |
    mi''2( re'')\trill |
    do''1 |
    r4 mi'8 fa' sol'4 la'8 si' |
    do''4 do'' la' re'' |
    si'\trill mi''8 re'' do''4 do''8 si' |
    la'4 la' mi' la' |
    fad'4.\trill re''8 re'' do'' si' la' |
    sol'4. mi'8 fa' sol' la' si' |
    do''4 la'8 fa'' fa'' mi'' re'' do'' |
    si'4.\trill si'8 do'' re'' mi'' fa'' |
    sol''4 mi'' r do''8 si' |
    la'4 sol'8 fa' mi'\trill mi' mi' fa' |
    sol' sol' la' si' do'' la' si' do'' |
    re''4. mi''8 mi''2\trill |
    re'' r4 sol'8 fa' |
    mi'4\trill fa'8 sol' la' la' si' do'' |
    si'\trill si' do'' re'' mi'' do'' re'' mi'' |
    fa''4. mi''8 mi''4( re'')\trill |
    do'' mi''8 re'' do''4 mi'' |
    la'4 la'8 la' re''4 re''8 re'' |
    si'4\trill si'8 do'' re''4. re''8 |
    mi''[ re''] do''[ si'] la'4.\trill sol'8 |
    sol'4 sol'8 sol' do''4 do'' |
    la'4\trill do''8 do'' fa''4 fa''8 fa'' |
    re''4\trill si'8 si' do''4. do''8 |
    re''4\trill mi'' re''4.\trill do''8 |
    do''4 %{%} do''8 do'' si'4 dod'' |
    re'' la'8 si' do''4 re'' |
    mi'' do''8 do'' la'4 re''8 re'' |
    si'4\trill mi''8 mi'' do''4 re'' |
    si' do'' si'4.\trill do''8 |
    do''4 <<
      \tag #'basse { r4 r2 | R1*3 | r4 }
      \tag #'vdessus {
        <>^\markup\character Chœur mi''8 mi'' re''4 re'' |
        mi'' mi''8 mi'' do''4 fa''8 fa'' |
        re''4 sol''8 sol'' mi''4 fa'' |
        re'' mi'' re''4.\trill do''8 |
        do''4
      }
    >> <>^\markup [à 3] mi''8 mi'' fa''4 fa'' |
    re''4\trill re''8 re'' mi''4 mi''8 mi'' |
    dod''4 fa''8 fa'' re''4 mi'' |
    dod'' re'' dod''4.\trill re''8 |
    re''4 <<
      \tag #'basse { r4 r2 | R1*3 | r4 }
      \tag #'vdessus {
        <>^\markup\character [Chœur] la'8 la' re''4 re'' |
        mi''4 mi''8 mi'' do''4 re''8 re'' |
        si'4 mi''8 mi'' do''4 re'' |
        si' do'' si'4.\trill la'8 |
        la'4
      }
    >> <>^\markup [à 3] dod''8 dod'' re''4 re''8 re'' |
    si'4 <<
      \tag #'basse { r4 r2 | R1*3 | r4 }
      \tag #'vdessus {
        <>^\markup\character [Chœur] si'8 si' mi''4 mi'' |
        do''4 do''8 do'' re''4 re''8 re'' |
        mi''4 mi''8 mi'' do''4 do'' |
        la' si' la'4.\trill sol'8 |
        sol'4
      }
    >> <>^\markup [à 3] si'8 dod'' re''4 si' |
    do'' do''8 re'' mi''4 mi'' |
    la' la'8 la' re''4 re''8 re'' |
    si'4\trill mi''8 mi'' do''4 re'' |
    si' do'' si'4.\trill do''8 |
    do''4 r r2 |
    R1 |
    r4 <<
      \tag #'basse { r4 r2 | R1*23 }
      \tag #'vdessus {
        <>^\markup\character [Chœur] sol''8 fa'' mi''4 re''8 do'' |
        si'4\trill r r2 |
        R1 |
        r4 si'8 do'' re''4 re''8 re'' |
        mi''4 r r2 |
        R1 |
        r4 mi''8 mi'' fa''4 fa'' |
        sol'' sol''8 sol'' do''4 fa''8 fa'' |
        re''4 sol''8 sol'' mi''4 fa'' |
        re'' mi'' re''4.\trill do''8 |
        do''4 r r2 |
        R1 |
        r4 sol''8 fa'' mi''4 re''8 do'' |
        si'4\trill r r2 |
        R1*2 |
        r4 mi''8 mi'' fa''4 fa'' |
        sol'' sol''8 sol'' do''4 fa''8 fa'' |
        re''4 sol''8 sol'' mi''4 fa'' |
        re'' mi'' re''4.\trill do''8 |
        do''4 sol''8 fa'' mi''4 re''8 do'' |
        si'4\trill sol''8 sol'' mi''4 fa'' |
        re''4 mi'' re''4.\trill do''8 |
        do''1\fermata |
      }
    >>
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" R1*9 |
    r4 <>^\markup\character Apollon mi'8 mi' re'4 mi' |
    fa' fa'8 sol' mi'4 fa' |
    sol' mi'8 mi' do'4 fa'8 fa' |
    \footnoteHere #'(0 . 0) \markup { Ballard 1719 : \italic fa }
    \sug re'4 sol'8 sol' mi'4 fa' |
    re' mi' re'4.\trill do'8 |
    do'1 |
    R1*4 R2. R1*6 R2. R1*29 |
    r4 mi'8 mi' re'4 mi' |
    fa' fa'8 sol' mi'4 fa' |
    sol' mi'8 mi' do'4 fa'8 fa' |
    re'4 sol'8 sol' mi'4 fa' |
    re' mi' re'4.\trill do'8 |
    do'4 %{%} sol'8 sol' sol'4 sol' |
    sol' sol'8 sol' la'4 la'8 la' |
    sol'4 sol'8 sol' la'4 la' |
    sol' sol' sol'4. sol'8 |
    mi'4\trill %{%} sol'8 sol' la'4 la' |
    fa'4 fa'8 fa' sol'4 sol'8 sol' |
    mi'4 la'8 la' fa'4 sol' |
    mi' fa' mi'4.\trill re'8 |
    re'4 fad'8 fad' sol'4 sol' |
    sol'4 sol'8 sol' fa'4 fa'8 fa' |
    mi'4 mi'8 mi' fa'4 fa' |
    mi' mi' mi'4. mi'8 |
    dod'4\trill mi'8 mi' fa'4 fa'8 fa' |
    re'4 re'8 re' sol'4 sol' |
    mi' la'8 la' sol'4 sol'8 sol' |
    sol'4 sol'8 sol' la'4 la' |
    fad' sol' fad'4.\trill sol'8 |
    sol'4 re'8 mi' fa'4 fa' |
    mi' mi'8 fa' sol'4 sol' |
    do' do'8 do' fa'4 fa'8 fa' |
    re'4 sol'8 sol' mi'4 fa' |
    re' mi' re'4.\trill do'8 |
    do'4 r r2 |
    R1 |
    r4 mi'8 fa' sol'4 sol'8 la' |
    sol'4 r r2 |
    R1 |
    r4 sol'8 sol' sol'4 sol'8 sol' |
    sol'4 r r2 |
    R1 |
    r4 sol'8 sol' la'4 la' |
    sol' sol'8 sol' la'4 la'8 la' |
    sol'4 sol'8 sol' la'4 la' |
    sol' sol' sol'4. sol'8 |
    mi'4 r r2 |
    R1 |
    r4 mi'8 fa' sol'4 sol'8 la' |
    sol'4 r r2 |
    R1*2 |
    r4 sol'8 sol' la'4 la' |
    sol' sol'8 sol' la'4 la'8 la' |
    sol'4 sol'8 sol' la'4 la' |
    sol' sol' sol'4. sol'8 |
    mi'4\trill mi'8 fa' sol'4 sol'8 la' |
    sol'4 sol'8 sol' la'4 la' |
    sol'4 sol' sol'4. sol'8 |
    << sol'1\fermata \new Voice { \voiceTwo \sug mi' } >> |
  }
  \tag #'vtaille {
    \clef "vtaille" R1*19 R2. R1*6 R2. R1*34 |
    r4 do'8 do' si4 si |
    do' do'8 do' la4 re'8 re' |
    si4 mi'8 mi' do'4 do' |
    si do' si4.\trill do'8 |
    do'4 r r2 |
    R1*3 |
    r4 re'8 re' re'4 re' |
    do'4 do'8 do' la4 la8 la |
    sold4 do'8 do' la4 si |
    sold la sold4.\trill la8 |
    la4 r r2 |
    r4 si8 si si4 si |
    do' fa'8 fa' re'4 re'8 re' |
    do'4 mi'8 mi' mi'4 mi' |
    re' re' re'4. re'8 |
    si4\trill r r2 |
    R1*6 |
    r4 do'8 do' do'4 mi'8 mi' |
    re'4 r r2 |
    R1 |
    r4 re'8 do' si4 si8 si |
    do'4 r r2 |
    R1 |
    r4 do'8 do' do'4 re' |
    mi' do'8 do' la4 re'8 re' |
    si4 mi'8 mi' do'4 do' |
    si do' si4.\trill do'8 |
    do'4 r r2 |
    R1 |
    r4 do'8 do' do'4 mi'8 mi' |
    re'4 r r2 |
    R1*2 |
    r4 do'8 do' do'4 re' |
    mi' do'8 do' la4 re'8 re' |
    si4 mi'8 mi' do'4 do' |
    si do' si4.\trill do'8 |
    do'4 do'8 do' do'4 mi'8 mi' |
    re'4 mi'8 mi' do'4 do' |
    si4 do' si4.\trill do'8 |
    do'1\fermata |
  }
  \tag #'vbasse {
    \clef "vbasse" R1*9 |
    r4 <>^\markup\character Neptune do8 do sol4 sol |
    re re8 re la4 la |
    mi mi8 mi fa4 re8 re |
    \footnoteHere #'(0 . 0) \markup { Ballard 1719 : \italic mi }
    \sug sol4 mi8 mi la4 fa |
    sol do sol,4. do8 |
    do1 |
    R1*4 R2. R1*6 R2. R1*29 |
    r4 do8 do sol4 sol |
    re re8 re la4 la |
    mi mi8 mi fa4 re8 re |
    sol4 mi8 mi la4 fa |
    sol do sol,4. do8 |
    do4 %{%} do8 do sol4 sol |
    mi mi8 mi fa4 re8 re |
    sol4 mi8 mi la4 fa |
    sol do sol,4. do8 |
    do4 %{%} do'8 do' la4 la |
    sib4 sib8 sib sol4 sol8 sol |
    la4 fa8 fa sib4 sol |
    la re la,4. re8 |
    re4 re'8 do' si4\trill si |
    do'4 do8 do fa4 re8 re |
    mi4 do8 do fa4 re |
    mi la, \voiceOne << mi4. \new Voice { \voiceTwo \sug mi,4. } >> \oneVoice la,8 |
    la,4 la8 la re4 re8 re |
    sol4 sol8 fa mi4 mi |
    la la8 la si4\trill si8 si |
    do'4 do'8 do' la4 la |
    re' sol re4. sol8 |
    sol4 sol8 sol re4 re |
    la4 la8 la mi4 mi |
    fa fa8 fa re4 re8 re |
    sol4 mi8 mi la4 fa |
    sol do sol,4. do8 |
    do4 r r2 |
    R1 |
    r4 do8 re mi4 mi8 fa |
    sol4 r r2 |
    R1 r4 sol8 la si4 sol8 sol |
    do'4 r r2 |
    R1 |
    r4 do'8 sib la4 la |
    mi mi8 mi fa4 re8 re |
    sol4 mi8 mi la4 fa |
    sol do sol,4. do8 |
    do4 r r2 |
    R1 |
    r4 do8 re mi4 mi8 fa |
    sol4 r r2 |
    R1*2 |
    r4 do'8 sib la4 la |
    mi mi8 mi fa4 re8 re |
    sol4 mi8 mi la4 fa |
    sol do sol,4. do8 |
    do4 do8 re mi4 mi8 fa |
    sol4 mi8 mi la4 fa |
    sol4 do sol,4. do8 |
    do1\fermata |
  }
>>