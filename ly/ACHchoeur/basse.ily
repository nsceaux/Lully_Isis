\clef "basse"
\tag #'basse <>^"[B.C.]" do1~ |
do4 do8 re mi4 do |
fa4 fa8 mi re4 re |
sol sol8 la si4. si8 |
do'2 re'4 re |
sol sol8 fa mi4 do |
fa4 fa8 mi re2 |
sol4 sol8 fa mi4 re8 do |
si,4 do sol,2 |
do4 do sol sol |
re re la la |
mi mi fa re |
sol mi la fa |
sol do sol,2 |
\once\tieDashed do1~ |
do1~ |
do4 sib, la, sol, |
fa,1 |
fa4 mi re2 |
la4 la, re |
sol,2 sol |
fa mi |
re do4. si,8 |
la,2 sol, |
fa,1 |
mi,2 mi |
la,4 re fad, |
sol, sol <<
  \new CueVoice {
    s4^\markup\note-by-number #2 #1 #UP s^\markup\note-by-number #3 #0 #UP 
  }
  { fa mi | }
>>
re2~ re4. mi8 |
fad2 sol4 do |
sol,1 |
do |
do'2 si |
la2 re'4 re |
sol do8 re mi2 |
fa dod |
re4. re8 sol4. la8 |
si4 do'8 sib la sol fa4 |
mi fa re2 |
sol4. fa8 mi re do4 |
si, do8 re mi2 |
fa4 sol la8 la sol fa |
mi re do si, la, re re do |
si, la, sol,4 do2 |
sol,4 sol8 la si la si sol |
do' sib la sol fa mi re do |
sol fa mi re do sib, la, sol, |
fa,2 sol, |
do4 do8 re mi4 do |
fa4 fa8 mi re4 re |
sol sol8 la si4. si8 |
do'2 re'4 re |
sol sol8 fa mi4 do |
fa fa8 mi re2 |
sol4 sol8 fa mi4 re8 do |
si,4 do sol,2 |
do4 do8 do sol4 sol |
re re la la |
mi mi fa re |
sol mi la fa |
sol do sol,4. do8 | \allowPageTurn
do4 \tag #'basse <>^"[Tous]" do4 sol sol |
mi mi fa re |
sol mi la fa |
sol do sol,2 |
do4 \tag #'basse <>^"[B.C.]" do'4 la la |
sib4. sib8 sol4. sol8 |
la4 fa sib sol |
la re la,2 |
re4 \tag #'basse <>^"[Tous]" re'8 do' si4 si |
do'4 do fa re |
mi do fa re |
mi la, mi,2 |
la,4 \tag #'basse <>^"[B.C.]" << \sugNotes { la8 la } \\ la4 >> re re |
sol \tag #'basse <>^"[Tous]" sol8 fa mi4 mi |
la4. la8 si4. si8 |
do'4 do' la la |
re' sol re2 |
sol \tag #'basse <>^"[B.C.]" re4 re |
la la mi2 |
fa4 fa re re |
sol mi la fa |
sol do sol,2 |
do4 \tag #'basse <>^"[Tous]" do8 do do4 do |
do do'8 do' do'4 do' |
do' do8 re mi4 mi8 fa |
sol4 sol8 sol do'4 do |
sol sol,8 sol, do4 do |
sol, sol8 la si4 sol |
do'4 do8 do do4 do |
do do'8 do' do'4 do' |
do' do'8 sib la4 la |
mi mi fa re |
sol mi la fa |
sol do sol,2 |
do4 do8 do do4 do |
do do'8 do' do'4 do' |
do' do8 re mi4 mi8 fa |
sol4 sol8 sol do'4 do |
sol sol,8 sol, do4 do |
sol, do sol,2 |
do4 do'8 sib la4 la |
mi mi fa re |
sol mi la fa |
sol do sol,2 |
do4 do8 re mi4 mi8 fa |
sol4 mi la fa |
sol do sol,2 |
do1\fermata |

