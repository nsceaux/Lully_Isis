\clef "bass" R1*19 R2. R1*6 R2. R1*56 |
r4 do8 do do4 do |
do do8 do do4 do |
do2 r |
r4 sol,8 sol, do4 do |
sol, sol,8 sol, do4 do |
sol,2 r |
r4 do8 do do4 do |
do do8 do do4 do |
do2 r |
R1*3 |
r4 do8 do do4 do |
do do8 do do4 do |
do2 r |
r4 sol,8 sol, do4 do |
sol, sol,8 sol, do4 do |
sol, do sol,2 |
do r |
R1*3 |
r4 do8 do do4 do8 do |
sol,4 sol,8 sol, do4 do |
sol, do sol,4. do8 |
do1 |
