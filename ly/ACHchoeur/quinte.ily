\clef "quinte" R1*19 R2. R1*6 R2. R1*34 |
r4 do'8 do' re'4 re' |
do' do'8 do' do'4 re'8 re' |
re'4 mi'8 mi' mi'4 re' |
re' do' re' sol |
sol r r2 |
R1*3 |
r4 re'8 re' re'4 re' |
do'4 do'8 do' do'4 << \sugNotes { si8 si } \\ { la8 si } >> |
<<
  { si4 do'8 do' do'4 \voiceTwo si |
    si la si mi' |
    mi' } \\
  \sugNotes {
    si4 la8 la la4 \voiceOne re' |
    re' do'8 re' mi'4. mi'8 |
    mi'4
  }
>> r4 r2 |
r4 si8 si si4 si |
do' la8 la re'4 re'8 re' |
do'4 do'8 do' do'4 do' |
re' re' re'4. re'8 |
re'4 r r2 |
R1*4 |
r4 mi'8 re' do'4 do'8 re' |
mi'4 mi'8 re' do'4 do'8 re' |
mi'4 do'8 do' sol4 sol8 la |
si4 sol8 re' do'4. mi'8 |
re'4 re'8 re' do'4. mi'8 |
re'4 re'8 do' si4 re'8 re' |
do'4 mi'8 re' do'4 do'8 re' |
mi'4 mi'8 re' do'4 do'8 re' |
mi'4 do'8 do' do'4 do' |
do' do'8 do' do'4 << \sugNotes { re'8 re' } \\ re'4 >> |
re'4 do'8 do' do'4 fa' |
fa' mi'8 fa' sol'4. sol'8 |
sol'4 mi'8 re' do'4 do'8 re' |
mi'4 mi'8 re' << \sugNotes { do'4 do'8 re' } \\ { do'8 si do' re' } >> |
mi'4 do'8 do' sol4 sol8 la |
si4 sol8 re' do'4. mi'8 |
re'4 re'8 re' do'4. mi'8 |
re'4 do' re' sol |
sol do'8 do' do'4 do' |
do' do'8 do' do'4 re'8 re' |
re'4 do'8 do' do'4 fa' |
fa' mi'8 fa' sol'4. sol'8 |
sol'4 do'8 do' sol4 sol8 la |
si4 do'8 do' do'4 re' |
re' do' re' sol |
sol1\fermata |
