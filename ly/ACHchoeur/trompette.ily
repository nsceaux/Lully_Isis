\clef "dessus" R1*19 R2. R1*6 R2. R1*56 |
r4 \twoVoices #'(trompette1 trompette2 trompettes) <<
  { mi''8 fa'' sol''4 sol''8 fa'' |
    mi''4 mi''8 fa'' sol'' la'' sol'' fa'' |
    mi''2 }
  { do''8 re'' mi''4 mi''8 re'' |
    do''4 do''8 re'' mi'' fa'' mi'' re'' |
    do''2 }
>> r2 |
r4 \twoVoices #'(trompette1 trompette2 trompettes) <<
  { re''8 mi'' mi''4.\trill re''16 do'' |
    re''8 do'' re'' mi'' mi''4.\trill re''16 do'' |
    re''2 }
  { sol'8 sol' do''4. do''16 do'' |
    sol'4 sol'8 sol' do''4. do''16 do'' |
    sol'2 }
>> r2 |
r4 \twoVoices #'(trompette1 trompette2 trompettes) <<
  { sol''8 fa'' mi''4 mi''8 re'' |
    do''4 sol''8 fa'' mi'' fa'' mi'' re'' |
    do''2 }
  { mi''8 fa'' sol''4 sol''8 fa'' |
    mi''4 mi''8 fa'' sol'' la'' sol'' fa'' |
    mi''2 }
>> r2 |
R1*3 |
r4 \twoVoices #'(trompette1 trompette2 trompettes) <<
  { mi''8 fa'' sol''4 sol''8 fa'' |
    mi''4 mi''8 fa'' sol'' la'' sol'' fa'' |
    mi''2 }
  { do''8 re'' mi''4 mi''8 re'' |
    do''4 do''8 re'' mi'' fa'' mi'' re'' |
    do''2 }
>> r2 |
r4 \twoVoices #'(trompette1 trompette2 trompettes) <<
  { re''8 mi'' mi''4.\trill re''16 do'' |
    re''8 do'' re'' mi'' mi''4.\trill re''16 do'' |
    re''8 mi'' fa'' mi'' re''4.\trill do''8 |
    do''2 }
  { sol'8 sol' do''4. do''16 do'' |
    sol'4 sol'8 sol' do''4. do''16 do'' |
    sol'4 do'' sol'4. do''8 |
    do''2 }
>> r2 |
R1*3 |
r4 \twoVoices #'(trompette1 trompette2 trompettes) <<
  { mi''8 fa'' sol''4 fa''8 mi'' |
    re''4 sol''8 sol'' mi''4 fa'' |
    re'' mi'' re''4.\trill do''8 |
    do''1\fermata | }
  { sol''8 fa'' mi''4 re''8 do'' |
    sol'4 sol'8 sol' do''4 do'' |
    sol' do'' sol'4. do''8 |
    do''1\fermata | }
>>
