\score {
  <<
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff <<
        { s1*19 s2. s1*6 s2. s1*34 s4 <>^"Trompettes" \noHaraKiri }
        \global \keepWithTag #'trompette1 \includeNotes "trompette"
      >>
      \new Staff <<
        { s1*19 s2. s1*6 s2. s1*34 s4 \noHaraKiri}
        \global \keepWithTag #'trompette2 \includeNotes "trompette"
      >>
    >>
    \new StaffGroup \with { \haraKiriFirst } <<
      \new GrandStaff <<
        \new Staff <<
          { s1*19 s2. s1*6 s2. s1*34 s4 \noHaraKiri <>^"Violons"  }
          <>^"Violons" \global \keepWithTag #'dessus1 \includeNotes "dessus"
        >>
        \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus" >>
      >>
      \new Staff <<
        { s1*19 s2. s1*6 s2. s1*34 s4 \noHaraKiri }
        \global \includeNotes "haute-contre"
      >>
      \new Staff <<
        { s1*19 s2. s1*6 s2. s1*34 s4 \noHaraKiri }
        \global \includeNotes "taille"
      >>
      \new Staff <<
        { s1*19 s2. s1*6 s2. s1*34 s4 \noHaraKiri }
        \global \includeNotes "quinte"
      >>
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s1*19 s2. s1*6 s2. s1*34 \noHaraKiri }
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
      \new Staff <<
        \global \keepWithTag #'basse-continue \includeNotes "basse"
        \includeFigures "chiffres"
        \origLayout {
          s1*5\break s1*4 s4 \bar "" \break s2. s1*3\break s1*2\pageBreak
          s1*4 s2.\break s1*5\break s1 s2. s1*3\break s1*6 s2 \bar "" \break
          s2 s1*3 s2 \bar "" \break s2 s1*4\pageBreak
          s1*4\break s1*4 s2 \bar "" \break
          s2 s1*2 s4 \bar "" \break s2. s1*4 s4 \bar "" \pageBreak
          s2. s1*3 s2 \bar "" \pageBreak
          s2 s1*4\pageBreak
          s1*4\pageBreak
          s1*4 s2 \bar "" \pageBreak
          s2 s1*4\pageBreak
          s1*5 s2 \bar "" \pageBreak
          s2 s1*4 s2 \bar "" \pageBreak
          s2 s1*4 s2 \bar "" \pageBreak
          s2 s1*4\pageBreak
          s1*4\pageBreak
        }
        \modVersion {
          s1*9 s1*6\break
          s1*4 s2. s1*6 s2. s1*34\break
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
