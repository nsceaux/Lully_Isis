\tag #'(vdessus basse) {
  Hâ -- tez- vous, Plai -- sirs, hâ -- tez- vous,
  hâ -- tez- vous de mon -- trer vos char -- mes les plus doux.
  Hâ -- tez- vous, Plai -- sirs, hâ -- tez- vous,
  hâ -- tez- vous de mon -- trer vos char -- mes les plus doux.
}
\tag #'(vdessus basse vhaute-contre vbasse) {
  Hâ -- tez- vous, Plai -- sirs,
  hâ -- tez- vous, Plai -- sirs, hâ -- tez- vous,
  hâ -- tez- vous de mon -- trer vos char -- mes les plus doux.
}
\tag #'(vdessus basse) {
  Il n’est pas en -- cor temps de croi -- re
  que les pai -- si -- bles Jeux ne se -- ront plus trou -- blez ;
  rien ne plaît au Hé -- ros qui les a ras -- sem -- blez
  à l’é -- gal des Ex -- ploits d’é -- ter -- nel -- le mé -- moi -- re ;
  rien ne plaît au Hé -- ros qui les a ras -- sem -- blez
  à l’é -- gal des Ex -- ploits d’é -- ter -- nel -- le mé -- moi -- re.
  En -- ne -- mis de la Paix, trem -- blez, trem -- blez,
  en -- ne -- mis de la Paix, trem -- blez, trem -- blez ;
  vous le ver -- rez bien- tôt cou -- rir à la vic -- toi -- re,
  vous le ver -- rez bien- tôt cou -- rir à la vic -- toi -- re.
  Vos ef -- forts re -- dou -- blez
  ne ser -- vi -- ront,
  ne ser -- vi -- ront qu’à re -- dou -- bler sa gloi -- re.
  Vos ef -- forts re -- dou -- blez
  ne ser -- vi -- ront,
  ne ser -- vi -- ront qu’à re -- dou -- bler sa gloi -- re.
  
  Hâ -- tez- vous, Plai -- sirs, hâ -- tez- vous,
  hâ -- tez- vous de mon -- trer vos char -- mes les plus doux.
  Hâ -- tez- vous, Plai -- sirs, hâ -- tez- vous,
  hâ -- tez- vous de mon -- trer vos char -- mes les plus doux.
}
\tag #'(vdessus basse vhaute-contre vbasse) {
  Hâ -- tez- vous, Plai -- sirs,
  hâ -- tez- vous, Plai -- sirs, hâ -- tez- vous,
  hâ -- tez- vous de mon -- trer vos char -- mes les plus doux.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Hâ -- tez- vous, Plai -- sirs, hâ -- tez- vous,
  hâ -- tez- vous de mon -- trer vos char -- mes les plus doux.
}
\tag #'(vdessus basse vhaute-contre vbasse) {
  Hâ -- tez- vous, Plai -- sirs, hâ -- tez- vous,
  hâ -- tez- vous de mon -- trer vos char -- mes les plus doux.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Hâ -- tez- vous, Plai -- sirs, hâ -- tez- vous,
  hâ -- tez- vous de mon -- trer vos char -- mes les plus doux.
}
\tag #'(vdessus basse vhaute-contre vbasse) {
  Hâ -- tez- vous, hâ -- tez- vous,
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Hâ -- tez- vous, Plai -- sirs, hâ -- tez- vous,
  hâ -- tez- vous de mon -- trer vos char -- mes les plus doux.
}
\tag #'(vdessus basse vhaute-contre vbasse) {
  Hâ -- tez- vous, Plai -- sirs,
  hâ -- tez- vous, Plai -- sirs, hâ -- tez- vous,
  hâ -- tez- vous de mon -- trer vos char -- mes les plus doux.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Hâ -- tez- vous, hâ -- tez- vous,
  Hâ -- tez- vous, hâ -- tez- vous,
  Hâ -- tez- vous, Plai -- sirs, hâ -- tez- vous,
  hâ -- tez- vous de mon -- trer vos char -- mes les plus doux.
  Hâ -- tez- vous, hâ -- tez- vous,
  Hâ -- tez- vous, Plai -- sirs, hâ -- tez- vous,
  hâ -- tez- vous de mon -- trer vos char -- mes les plus doux.
  hâ -- tez- vous,
  hâ -- tez- vous de mon -- trer vos char -- mes les plus doux. 
}
