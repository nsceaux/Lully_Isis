\clef "haute-contre" R1*19 R2. R1*6 R2. R1*34 |
r4 do''8 do'' si'4 si' |
do'' do''8 do'' la'4 re''8 re'' |
si'4 mi''8 mi'' do''4 do'' |
si' do'' si'4.\trill do''8 |
do''4 r r2 |
R1*3 |
r4 fad'8 fad' sol'4 sol' |
sol'4 do''8 do'' la'4 si'8 si' |
sold'4 do''8 do'' la'4 si' |
sold'4 la' sold'4.\trill la'8 |
la'4 r r2 |
r4 sol'8 sol' sol'4 si' |
la' la'8 la' sol'4 sol'8 sol' |
sol'4 sol'8 sol' la'4 la' |
fad' sol' fad'4.\trill sol'8 |
sol'4 r r2 |
R1*4 |
r4 do''8 do'' sol'4 do'' |
do'' do''8 do'' sol'4 do'' |
do'' do''8 do'' do''4. la'8 |
sol'4 si'8 do'' do''4.\trill si'16 la' |
si'8 la' si' do'' do''4.\trill si'16 la' |
si'4 sol'8 sol' sol'4 si'8 si' |
do''4 do''8 do'' sol'4 do'' |
do'' do''8 do'' sol'4 do'' |
do'' do''8 do'' do''4 re'' |
mi'' mi''8 mi'' la'4 la'8 la' |
si'4 mi''8 mi'' do''4 do'' |
si' do'' si'4.\trill do''8 |
do''4 do''8 do'' sol'4 do'' |
do'' do''8 do'' sol'4 do'' |
do'' do''8 do'' do''4. la'8 |
sol'4 si'8 do'' do''4.\trill si'16 la' |
si'4 si'8 do'' do''4.\trill si'16 la' |
si'8 do'' re'' do'' si'4.\trill do''8 |
do''4 do''8 do'' do''4 re'' |
mi'' mi''8 mi'' la'4 la'8 la' |
si'4 mi''8 mi'' do''4 do'' |
si' do'' si'4.\trill do''8 |
do''4 do''8 do'' do''4. la'8 |
sol'4 sol'8 sol' la'4 do'' |
si'4 do'' si'4.\trill do''8 |
do''1\fermata |
