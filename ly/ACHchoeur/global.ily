\key do \major
\digitTime\time 2/2 \midiTempo#160 s1*15
\time 2/2 \midiTempo#160 s1*4
\digitTime\time 3/4 \midiTempo#80 s2.
\time 2/2 \midiTempo#160 s1*6
\digitTime\time 3/4 \midiTempo#80 s2.
\time 2/2 \midiTempo#160 s1*34 s4 \segnoMark s2. s1*47 \bar "|."
