\clef "basse" sol4 |
sol fad sol do |
re2. sib,4 |
mib do re << re \\ \sug re, >> |
sol, sol,2 sol4 |
do4. do8 do4. do8 |
fa2. re4 |
sol4 mib fa fa, |
sib, sib,2 sib,4 |
fa fa mib4. re8 |
do4 do2 do4 |
sol fa mib2 |
re2. sol4 |
re2. re4 |
sol2. sol8 fa |
mi4 mi mi4.\trill mi8 |
fa4 fa2 fa8 mib |
re4 re re4.\trill re8 |
mib4. mib8 do4. do8 |
re4. re8 sol4 sol, |
re2. sib,4 |
mib do re re, |
sol,2. sol4 |
do re mib do |
re2. sib,4 |
mib4 do re re, |
sol, sol,8 la, sib,4 sol, |
re4 re8 mi fad4 re |
sol2. sol8 fa |
<<
  \tag #'basse { sol,1 }
  \tag #'(basse-continue bc-part) {
    sol,2. \tag #'bc-part <>_"[B.C.]" la,4 |
    \once\set Staff.whichBar = "|"
  }
>>
