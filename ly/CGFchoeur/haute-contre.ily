\clef "haute-contre" sol'4 |
sol' la' sol' la' |
fad'2. sol'4 |
sol' sol' sol' fad' |
sol' sol'2 sol'4 |
sol' do'' do''4. sib'8 |
la'2. la'4 |
sib'4 sib' sib' la' |
sib' sib'2 sib'4 |
\sugRythme { si'4. si'8 } la'4 la' la'4. si'8 |
do''4 do''2 do''4 |
sib' la' sib' sol' |
fad'2.\trill sol'4 |
fad'1\trill |
r2 r4 sol'8 sol' |
sol'4 sol' sol'4. sol'8 |
fa'4 fa'2 fa'8 fa' |
fa'4 fa' fa'4. fa'8 |
mib'4. sol'8 sol'4. la'8 |
fad'4. fad'8 sol'4. la'8 |
fad'2.\trill sol'4 |
sol' sol' fad'4.\trill sol'8 |
sol'2. sol'8 sol' |
do''4 sib' la'4.\trill sol'8 |
fad'2.\trill sol'4 |
sol'4 sol' fad'4.\trill sol'8 |
sol'1 |
R1 |
r2 r4 sol'8 sol' |
sol'1 |
