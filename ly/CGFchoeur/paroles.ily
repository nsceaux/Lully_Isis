\tag #'(vdessus1 vhaute-contre1 vtaille1 vbasse1) {
  Que ces Lieux ont d’at -- traits ;
  goû -- tons- en bien les char -- mes,
  l’A -- mour n’y fait ja -- mais
  ver -- ser de tris -- tes lar -- mes,
  les soins & les al -- lar -- mes,
  n’en trou -- blent point la paix :
  Que  paix :
  Joü -- is -- sons dans ces Re -- trai -- tes,
  des dou -- ceurs les plus par -- fai -- tes ;
  \tag #'vdessus1 { sui -- vez- nous, }
  sui -- vez- nous char -- mants Plai -- sirs,
  com -- blez tous nos de -- sirs.
  Sui -- vez- nous char -- mants Plai -- sirs,
  com -- blez tous nos de -- sirs.
  \tag #'vdessus1 {
    Joü -- is -- sons dans ces Re-
  }
  \tag #'(vhaute-contre1 vtaille1 vbasse1) { Joü -- is- }
  - sirs.
}
\tag #'(vdessus2 vhaute-contre2 vtaille2 vbasse2) {
  Voy -- ons cou -- ler ces eaux
  dans ces ri -- ants boc -- ca -- ges ;
  Chan -- tez pe -- tits oy -- seaux,
  chan -- tez sur ces feuil -- la -- ges,
  joi -- gnez vos doux ra -- ma -- ges
  à nos Con -- certs nou -- veaux :
  Voy- -veaux :
}
