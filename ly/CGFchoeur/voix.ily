<<
  \tag #'vdessus {
    \clef "vdessus" re''4 |
    sib' la' sib' do'' |
    la'2.\trill re''4 |
    sol' do'' la' re'' |
    sib' sol'2 sib'4 |
    mib'' mib'' mib''4. re''8 |
    do''2.\trill fa''4 |
    sib' mib'' do'' fa'' |
    re'' sib'2 re''4 |
    do''\trill do'' do''4. re''8 |
    mib''4 mib''2 mib''4 |
    re'' re'' re'' do'' |
    re''2. re''4 |
    re''2. re''8 do'' |
    si'4 si' si'4.\trill si'8 |
    do''4 do''2 do''8 sib' |
    la'4 la' la'4.\trill la'8 |
    sib'4 sib' sib'4. la'8 |
    sol'2\trill do''4. do''8 |
    do''4. re''8 sib'4.\trill la'8 |
    la'2.\trill re''4 |
    sol' la' la'4.\trill sol'8 |
    sol'2. re''8 re'' |
    mib''4 re'' do''4.\trill sib'8 |
    la'2.\trill re''4 |
    sol'4. la'8 la'4.\trill sol'8 |
    sol'1 |
    r2 r4 re''8 do'' |
    si'4 si' si'4.\trill si'8 |
    sol'1 |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" sol'4 |
    sol' la' sol' la' |
    fad'2.\trill sol'4 |
    sol' sol' sol' fad' |
    sol' sol'2 sol'4 |
    sol' sol' sol'4. sol'8 |
    fa'2. fa'4 |
    re'4 sol' fa' fa' |
    fa' re'2 fa'4 |
    fa' fa' fa'4. fa'8 |
    sol'4 sol'2 sol'4 |
    sol' la' sol'4. sol'8 |
    fad'2. sol'4 |
    fad'1 |
    r2 r4 sol'8 sol' |
    sol'4 sol' sol'4. sol'8 |
    fa'4 fa'2 fa'8 fa' |
    fa'4 fa' fa'4. fa'8 |
    mib'4. sol'8 sol'4. sol'8 |
    fad'4.\trill fad'8 sol'4. la'8 |
    fad'2.\trill sol'4 |
    sol' sol' fad'4.\trill sol'8 |
    sol'2. sol'8 sol' |
    sol'4 fa' mib'4. mib'8 |
    re'2. re'4 |
    sib4 mib' re' la |
    sib1 |
    R1 |
    r2 r4 sol'8 sol' |
    sib1 |
  }
  \tag #'vtaille {
    \clef "vtaille" sib4 |
    re' re' re' mib' |
    re'2. re'4 |
    mib' mib' re' re' |
    re' sib2 re'4 |
    do' do' do'4. sib8 |
    la2.\trill la4 |
    sib sib sib la |
    sib sib2 sib4 |
    la la la4. si8 |
    do'4 do'2 do'4 |
    sib la sib sol |
    la2. sib4 |
    la1 |
    r2 r4 re'8 re' |
    sol4 sol do'4. do'8 |
    do'4 do'2 do'8 do' |
    re'4 re' re'4. re'8 |
    sib4. sib8 mib'4. mib'8 |
    re'4. re'8 re'4. re'8 |
    re'2. re'4 |
    sib mib' re' la |
    sib2. sib8 sib |
    sib4 sib la4.\trill sol8 |
    fad2.\trill sol4 |
    sol4 sol fad4.\trill sol8 |
    sol1 |
    R1 |
    r2 r4 re'8 re' |
    sol1 |
  }
  \tag #'vbasse {
    \clef "vbasse" sol4 |
    sol fad sol do |
    re2. sib,4 |
    mib do re << { \voiceOne re \oneVoice } \new Voice { \voiceTwo \sug re, } >> |
    sol, sol,2 sol4 |
    do do do4. do8 |
    fa2. re4 |
    sol4 mib fa fa, |
    sib, sib,2 sib,4 |
    fa fa mib4.\trill re8 |
    do4 do2 do4 |
    sol fa mib4.\trill re8 |
    re2. sol4 |
    re1 |
    r2 r4 sol8 fa |
    mi4\trill mi mi4.\trill mi8 |
    fa4 fa2 fa8 mib |
    re4 re re4.\trill re8 |
    mib4. mib8 do4. do8 |
    re4. re8 sol4 sol, |
    re2. sib,4 |
    mib do re re, |
    sol,2. sol8 sol |
    do4 re mib4. do8 |
    re2. sib,4 |
    mib4 do re << { \voiceOne re \oneVoice } \new Voice { \voiceTwo \sug re, } >> |
    sol,1 |
    R1 |
    r2 r4 sol8 fa |
    sol,1 |
  }
>>
