\clef "taille" sib4 |
re' re' re' mib' |
re'2. re'4 |
mib' mib' re' re' |
re' sib2 re'4 |
do' sol' sol'4. sol'8 |
fa'2. fa'4 |
sol'4 sol' fa' fa' |
fa' re'2 fa'4 |
fa' fa' fa'4. fa'8 |
sol'4 sol'2 sol'4 |
sol' la' sol'4. sol'8 |
la'2. sib4 |
la'1 |
r2 r4 re'8 re' |
mi'4 mi' mi'4. mi'8 |
do'4 do'2 do'8 do' |
re'4. re'8 re'4. re'8 |
sib4. sib8 mib'4. mib'8 |
re'4. re'8 re'4. re'8 |
re'2. re'4 |
mib'4. mib'8 re'4. re'8 |
re'2. sol'8 sol' |
sol'4 fa' mib'4. mib'8 |
re'2. re'4 |
mib'4 mib' re' la |
sib1 |
R1 |
r2 r4 re'8 re' |
sib1 |
