\clef "quinte" re'4 |
sol re' sib la |
la2. sib4 |
sib la la4. la8 |
sol4 sol2 sol4 |
sol4. sol8 do'4. do'8 |
do'2. re'4 |
re' do' do'4. do'8 sib4 fa2 sib4 |
do' do'2 fa'4 |
mib' do'2 do'4 |
re' re' sol do' |
la2.\trill re'4 |
la1\trill |
r2 r4 sol8 sol |
sol4 sol sol4. sol8 |
la4 fa2 fa8 fa |
fa4 fa fa4. fa8 |
sol4. sol8 sol4 do' |
la4. la8 sol4 sol |
la2. sib4 |
sib do' la4.\trill la8 |
sib2. sib8 sib |
la2. la4 |
la2. sib4 |
sib4 do' la re' |
re'1 |
R1 |
r2 r4 sol8 sol |
re'1 |
