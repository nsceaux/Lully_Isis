\score {
  <<
    \new StaffGroup \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyricsB <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >>
      \keepWithTag #'vdessus1 \includeLyrics "paroles"
      \keepWithTag #'vdessus2 \includeLyrics "paroles"
      \new Staff \withLyricsB <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >>
      \keepWithTag #'vhaute-contre1 \includeLyrics "paroles"
      \keepWithTag #'vhaute-contre2 \includeLyrics "paroles"
      \new Staff \withLyricsB <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >>
      \keepWithTag #'vtaille1 \includeLyrics "paroles"
      \keepWithTag #'vtaille2 \includeLyrics "paroles"
      \new Staff \withLyricsB <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >>
      \keepWithTag #'vbasse1 \includeLyrics "paroles"
      \keepWithTag #'vbasse2 \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s1*6\pageBreak
        s1*7\pageBreak
        s1*5\pageBreak
        s1*6\pageBreak
      }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
