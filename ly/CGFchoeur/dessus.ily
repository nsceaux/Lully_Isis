\clef "dessus" re''4 |
sib'\trill la' sib' do'' |
la'2.\trill re''4 |
<< sib' \\ \sug sol' >> do'' la' re'' |
sib' sol'2 sib'4 |
mib'' mib'' mib''4.\trill re''8 |
do''2.\trill fa''4 |
sib' mib'' do'' fa'' |
re'' sib'2 re''4 |
\sugRythme { si'4. si'8 } do''4\trill do'' do''4. re''8 |
mib''4 mib''2 mib''4 |
re'' re'' re'' do'' |
re''2. re''4 |
re''2. re''8 do'' |
si'4 si' si'4.\trill si'8 |
do''4 do''2 do''8 sib' |
la'4 la' la'4.\trill la'8 |
sib'4 sib' sib'4.\trill la'8 |
sol'2 do''4. do''8 |
do''4. re''8 sib'4.\trill la'8 |
la'2.\trill re''4 |
\sugRythme { si'4. si'8 } sol'4 la' la'4.\trill sol'8 |
sol'2. re''8 re'' |
mib''4 re'' do''4.\trill sib'8 |
la'2.\trill re''4 |
sol'4. la'8 la'4.\trill sol'8 |
sol'1 |
r2 r4 re''8 do'' |
si'4 si' si'4.\trill si'8 |
sol'1 |
