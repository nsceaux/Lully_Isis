\clef "basse"
\footnoteHere #'(0 . 0) \markup { Matériel 1677 : B.C. tacet. }
r4 |
r r sol |
re'4. re'8 do'4 |
sib fad2 |
sol4. fa8 mib re |
do4. do8 fa mib |
re do sib,4 mib |
do re sol, |
re,2 sol,4 |
do4. re8 mib do |
sol4. sol8 fa4 |
mib re do |
sol4. sol8 do' sib |
la2 sib4 |
mib2 re4 |
mib8 re do4 sib, |
fa fa, sib,~ |
sib, si,2 |
do4 dod2 |
re re4 |
mib mi4. mi8 |
fa4 fad sol |
do4. do8 re4 |
sol,2 sol4 |
do4. do8 la,4 |
re4. re8 mib4 |
do4. do8 re4 |
sol,2. |
