\clef "dessus" re''4 |
sol''4. la''8 sib''4 |
fad''4.\trill sol''8 la''4 |
re'' do''4.\trill re''8 |
sib'4.\trill la'8 sol'4 |
mib''4. re''8 do''4 |
fa'' re'' sol'' |
la'' fad''4.\trill sol''8 |
la''2 re''4 |
mib''4. re''8 do''4 |
si'4.\trill do''8 re''4 |
mib'' fa'' sol'' |
re''2\trill do''4 |
fa''4. mib''8 re''4 |
sol''4. la''8 sib''4 |
sib' mib''4. re''8 |
do''2\trill sib'4 |
re''4. mi''8 fa''4\trill |
mi''4. \footnoteHere#'(0 . 0) \markup {
  Matériel 1677 : \italic fa♯
} fad''?8 sol''4 |
fad''2\trill re''4 |
re'' do''4.\trill do''8 |
do''4. re''8 sib'4 |
sib'4. do''8 la'4\trill |
sol'2 re''4 |
re'' do''4.\trill do''8 |
do''4. re''8 sib'4 |
sib'4. do''8 la'4\trill |
sol'2. |
