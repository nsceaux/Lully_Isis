\clef "haute-contre" r4 |
r r re'' |
re''4. re''8 la'4 |
sib' la'2\trill |
sol'2 sol'4 |
do''4. sib'8 la'4 |
sib' sib' sib' |
do'' la'4.\trill sol'8 |
fad'2\trill sol'4 |
sol'2 sol'4 |
sol'4. la'8 si'4 |
do''4 si' do'' |
si'4.\trill la'16 si' do''4 |
do''2 sib'4 |
sib'4. do''8 re''4 |
sol' la' sib' |
la'2\trill sib'4 |
sib'8 do'' re''4. re''8 |
sol'4 la'4. la'8 |
la'2 fad'4 |
sol'4. la'8 sib'4 |
la'4. sib'8 sol'4 |
sol'4. sol'8 fad'4\trill |
sol'2 sib'4 |
sib' la'4.\trill la'8 |
la'4. sib'8 sol'4 |
sol'4. sol'8 fad'4\trill |
sol'2. |
