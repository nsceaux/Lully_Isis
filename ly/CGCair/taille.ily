\clef "taille" r4 |
r r sol' |
la' fad'4.\trill fad'8 |
sol'4 la'2 |
re'4. re'8 mib'4 |
mib'?8 fa' sol'4 fa' |
fa' fa' mib'~ |
mib' re'4. re'8 |
re'2 si4 |
do'8 re' mib'2 |
re' re'4 |
sol' fa' mib' |
sol'2 sol'4 |
la'2 fa'4 |
sol'2 fa'4 |
mib'2 fa'4 |
fa'4. mib'8 re'4 |
re' re'2 |
do'4 mi'4. mi'8 |
re'2 re'4 |
sib do'4. do'8 |
do'4 re' re' |
mib'4. mib'8 re'4 |
re'2 re'4 |
mib'4 mi'4. mi'8 |
fad'4. fad'8 sol'4 |
mib'4. mib'8 re'4 |
re'2. |
