\clef "quinte" r4 |
r r re' |
re'2 re'4 |
re'2 re'4 |
re' sib sib |
do'2 do'4 |
re' re' sib |
la la sib |
la2\trill sol4 |
sol do'4. do'8 |
re'4. re'8 re'4 |
do' re' sol |
sol4. re'8 mib'4 |
do'2 re'4 |
mib'2 sib4 |
sib do' fa |
fa2 fa4 |
fa sol2 |
sol4 mi2 |
la la4 |
sol4 sol4. sol8 |
la4 la sib |
sol4. la8 la4 |
sib2 sib4 |
do'2 do'4 |
re'2 sol4 |
sol4. la8 la4\trill |
sib2. |
