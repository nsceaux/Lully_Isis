\clef "basse" r4 |
R2.*2 |
r4 r sol |
re'4. do'8 re'4 |
sol2 sol4 |
do'4. do'8 sib4 |
la4. la8 re'4 |
sib do' do |
fa4. fa8 mib4 |
re4. do8 re4 |
mib4. mib8 sib,4 |
fa4. fa8 fa,4 |
sib,4. do8 sib, la, |
sol,2. |
sib,2 sib4 |
fa4. fa8 do re |
mib2 sib,4 |
fa fad sol |
re'4. do'8 re'4 |
sol4. fad8 sol4 |
re re' fad |
sol sol sol, |
re2 re4 |
mib mi4. mi8 |
fa4 fad sol |
do4. do8 re4 |
sol,2 sol4 |
do4. sib,8 la,4 |
re4. re8 mib4 |
do4. do8 re4 |
sol,2 sib4 |
sol,2. |
