\clef "vbas-dessus" <<
  \tag #'(voix1 basse) {
    sol'4 |
    re''4. do''8 re''4 |
    sib'4. do''8 re''4 |
    do'' sib'2\trill |
    la'2\trill la'4 |
    sib'4. la'8 sib'4 |
    sol'4.\trill la'8 sib'4 |
    do''4. sib'8 la'4 |
    sib'4 sol'2\trill |
    fa'2 do''4 |
    re''4. mib''8 fa''4 |
    sib'4. do''8 re''4 |
    do''4. re''8 mib''4 |
    re''2\trill sol'4 |
    re''4. do''8 re''4 |
    re''2\trill
    \footnoteHere#'(0 . 0) \markup { Matériel 1677 : \italic { Ce Dieux vous éclaire } }
    re''4 |
    do''4.\trill re''8 mib''4 |
    sib'4. do''8 re''4 |
    do''4.\trill re''8 sib'4 |
    la'2\trill la'4 |
    sib'4. do''8 sib'4 |
    la'4.\trill sib'8 do''4 |
    sib'4.\trill la'8 sib'4 |
    la'2\trill re''4 |
    re'' do''4.\trill do''8 |
    do''4. re''8 sib'4 |
    sib'4. do''8 la'4\trill |
    sol'2 sib'4 |
    sib' la'4.\trill sol'8 |
    fad'4.\trill fad'8 sol'4 |
    sol'4. la'8 fad'4\trill |
    sol'2 re''4 |
    sol'2. |
  }
  \tag #'voix2 {
    r4 |
    r r re' |
    sol'4. la'8 sib'4 |
    la' sol'2 |
    fad'2\trill fad'4 |
    sol'4. fa'8 sol'4 |
    mi'4.\trill fa'8 sol'4 |
    la'4. sol'8 fa'4 |
    sol' mi'2\trill |
    fa' la'4 |
    sib'4. sib'8 lab'4 |
    sol'4.\trill la'8 sib'4 |
    sib'4. sib'8 la'4 |
    sib'2 r4 |
    r r re' |
    sib'2 sib'4 |
    la'4.\trill si'8 do''4 |
    sol'4. la'8 sib'4 |
    la'4.\trill sib'8 sol'4 |
    fad'2\trill fad'4 |
    sol'4. la'8 sol'4 |
    fad'4.\trill sol'8 la'4 |
    re'4. re'8 sol'4 |
    fad'2\trill fad'4 |
    sol'4. la'8 sib'4 |
    la'4.\trill sib'8 sol'4 |
    sol'4. la'8 fad'4\trill |
    sol'2 re''4 |
    re'' do''4.\trill do''8 |
    do''4. re''8 sib'4 |
    sib'4. do''8 la'4\trill |
    sol'2 sib'4 |
    sol'2. |
  }
>>
