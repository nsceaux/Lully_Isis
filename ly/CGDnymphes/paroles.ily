\tag #'(voix1-couplet1 voix2-couplet1 basse) {
  Ai -- mez, \tag #'(voix1-couplet1 basse) {  pro -- fi -- tez, } pro -- fi -- tez du temps,
  jeu -- nes -- se char -- man -- te,
  ren -- dez vos de -- sirs con -- tents.
  Tout rit, tout en -- chan -- te
  dans les plus beaux ans :
  \tag #'(voix1-couplet1 basse) { Ai -- mez, pro -- fi - }
  \tag #'(voix2-couplet1) { Ai - }
  ans :

  L’A -- mour vous é -- clai -- re,
  mar -- chez sur ses pas ;
  cher -- chez à vous fai -- re
  des nœuds pleins d’ap -- pas ;
  Que vous sert de plai -- re,
  si vous n’ai -- mez pas ?
  Que vous sert de plai -- re,
  si vous n’ai -- mez pas ?
  L’A -  pas ?
}
\tag #'(voix1-couplet2 voix2-couplet2) {
  Pour -- quoy \tag #'(voix1-couplet2) { crai -- gnez- vous, } crai -- gnez- vous d’ai -- mer,
  beau -- tez in -- hu -- mai -- nes,
  ces -- sez de vous al -- lar -- mer ;
  L’A -- mour a des pei -- nes,
  qui doi -- vent char -- mer.
  \tag #'(voix1-couplet2) { Pour -- quoy crai -- gnez -  }
  \tag #'(voix2-couplet2) { Pour - }
  - mer.
}
