\clef "quinte" sib la2 |
sol re'4 |
re' sol re' |
re'4. do'8 sib4 |
sib8 do' re'4 re' |
re' sol2 |
fa4 do' la |
la2. |
la4 re'2 |
re'4. do'8 sib4 |
re' fa'4. fa'8 |
mib'4. re'8 do'4 |
sib2 fa4 |
fa sib2 |
do'4 la la |
sol sol2 |
la4 re'2 |
re'4. do'8 sib4 |
re' fa'4. fa'8 |
mib'4. re'8 do'4 |
sib2 sib4 |
do'2 re'4 |
sol2 fa8 sol |
la4 la4.\trill sol8 |
sol4 sol'2 |
sol2. |
