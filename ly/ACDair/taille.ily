\clef "taille" sol'4 \once\slurDashed fad'4.(\trill mi'16 fad') |
sol'4 re'2 |
re'4 do'2 |
sib4. do'8 re'4 |
sol' \once\slurDashed fad'4.(\trill mi'16 fad') |
sol'4 re' mib' |
fa' mib'2 |
re'2. |
re' |
fa'4 sib'2 |
la'4.\trill sol'8 fa'4 |
sol' fa'4. fa'8 |
fa'4. mib'8 re'4 |
do' re' sib |
mib'2 re'4 |
re' re'4. re'8 |
re'2. |
fa'4 sib'2 |
la'4.\trill sol'8 fa'4 |
sol' fa'4. fa'8 |
fa'4. mib'8 re'4 |
do' fa' fa' |
mib'2 fa'4 |
mib' re'4. re'8 |
re'2. |
re' |
