\clef "haute-contre" re''4 re''2 |
re''4. do''8 sib'4 |
la'8 sib' do''4 la' |
sib'2. |
re''4 re''2 |
re''8 do'' sib'4 sib'8 sol' |
re''4 mib'' do''8 sib' |
la'2.\trill |
la'2.\trill |
sib'4. do''8 re''4 |
re'' do'' sib' |
sib' la'2 |
sib'4 fa'2 |
fa'4 fa' sol' |
la' la' fad' |
sol'4 sol'4. la'8 |
fad'2.\trill |
sib'4. do''8 re''4 |
re'' do'' sib' |
sib' la'2\trill |
sib'4 fa' sol' |
la'8 sib' do''4 sib' |
sib'4. do''8 re''4 |
do''8 sib' la'4 re'' |
si'2.\trill |
si'2.\trill |
