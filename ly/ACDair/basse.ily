\clef "basse"
\footnoteHere #'(0 . 0) \markup { Matériel 1677 : B.C. tacet. }
sol4 re2 |
sol,4. la,8 sib, do |
re4 mi fad |
sol4. la8 sib4 |
sol re' re |
sol4. fa8 mib4 |
re do2 |
re2. |
re4 re' do' |
sib4. la8 sol4 |
re' la sib |
mib fa fa, |
sib,4. do8 re mib |
fa4 re mib |
do2 re8 do |
sib, la, sol,2 |
re4 re' do' |
sib4. la8 sol4 |
re' la sib |
mib fa fa, |
sib,2 sib4 |
la2 sib4 |
mib2 sib,4 |
do re re, |
sol,2 sol8 la |
sol,2. |
