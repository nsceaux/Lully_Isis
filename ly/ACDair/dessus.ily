\clef "dessus" sol''4 \once\slurDashed la''4.(\trill sol''16 la'') |
sib''4. la''8 sol''4 |
fad''4 sol'' la'' |
re''2. |
sol''4 \once\slurDashed la''4.(\trill sol''16 la'') |
sib''4. la''8 sol''4 |
la'' la''4.\trill sol''8 |
fad''2.\trill |
fad''\trill |
re''4 sol''2 |
fa''4.\trill mib''8 re''4 |
mib'' \once\slurDashed do''4.(\trill sib'16 do'') |
re''4. do''8 sib'4 |
la' sib' sol' |
do''4. sib'8 la'4 |
sib'4 sib'4.\trill la'8 |
la'2.\trill |
re''4 sol''2 |
fa''4.\trill mib''8 re''4 |
mib''4 \once\slurDashed do''4.(\trill sib'16 do'') |
re''4. do''8 sib'4 |
fa'' fa'' re'' |
sol''4. la''8 sib''4 |
la''8 sol'' fad''4.\trill sol''8 |
sol''2. |
sol'' |
