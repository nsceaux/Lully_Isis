\clef "vbasse" <>^\markup\character Pan
r4 r8 do'16 mi fa8 fa re\trill re16 sol |
mi8.\trill mi16 r8 sol16 sol sol8 la16 si |
do'4 do'8 do' fad4\trill fad8 sol |
sol4
\ffclef "vdessus" <>^\markup\character Syrinx
si'4 r8 sol'16 sol' do''8 mi'' |
la'4 r8 re''16 re'' re''4 re''8 dod'' |
re''4 la'16 la' la' si' do''8 do''16 re'' |
mi''4 mi''8
\ffclef "vbasse" <>^\markup\character Pan
sold16 sold sold8\trill sold16 sold |
la8 la la sol sol fad |
fad4\trill r8 re'16 re' si8.\trill do'16 la8 si16 do' |
si2.\trill |
r8^"Air"  do' do'4 sol |
la4. mi8 fa4 |
re4 sol8 fa[ mi re] |
mi2\trill sol4 |
la8 si do' si[ la si] |
si\trill[ do' si] la( sol4) |
r8 do' do'4 sol |
la4. mi8 fa4 |
re4 sol8 fa[ mi re] |
mi4.\trill sol8 la[ sol] |
fa[ mi] re4.\trill do8 |
do4. mi8 mi mi |
fa4 fa fa fa |
re4\trill re8 sib sib sib |
sib4 la la la |
fad4.\trill
\ffclef "vdessus" <>^\markup\character Syrinx
re''16 re'' si'8\trill si' do'' re'' |
mi''8. mi''16 r8 do''16 mi'' la'8\trill la' si' do'' |
si'2\trill
\ffclef "vbasse" <>^\markup\character Pan
r4 mi8 fa |
sol4. sol8 la4 la8 si |
do'4 do' r sol8 sol |
la4. mi8 fa4 fa8 mi |
re4\trill re r la8 la |
sib4 la8 sol fad4.\trill sol8 |
sol2 r4 |
re'4 r8 la la re' |
si2\trill si8 sol |
do'4 do'8 si la sol |
fa4\trill re mi~ |
mi8. fa16 mi4( re8.\trill) do16 |
do4
\ffclef "vdessus" <>^\markup\character Syrinx
r8 sol' sold'\trill sold' |
la'4 la' si' do'' |
si'\trill si' r2 |
do''2^"Air" sib'8[ la'16] sib' |
la'2\trill la'8 si' |
do''4 re''4. mi''8 |
mi''4( re''4.)\trill do''8 |
do''2 mi''4 |
sold'4.\trill sold'8 la' si' |
do''2 la'8 sol' |
fa'2\trill mi'8 fa' |
mi'2\trill mi'4 |
la'4. la'8 la' sib' |
sol'2 sol'8 fad' |
sol'2. |
do''2 sib'8[ la'16] sib' |
la'2\trill fa''4 |
re''4.\trill do''8 si'4 |
do''2 si'8 la' |
sold'2\trill sold'4 |
la' mi'4.\trill fa'8 |
sol'4. la'8 si' do'' |
re''2 re''8 mi'' |
do''2\trill la'8 re'' |
si'2\trill si'4 |
do''2 sib'8[ la'16] sib' |
la'2\trill la'8 si' |
do''4 re''4. mi''8 |
mi''4( re''4.)\trill do''8 |
do''2
\ffclef "vbasse" <>^\markup\character Pan
sol4~ |
sol r fa8[ mi16] fa |
mi2\trill mi4 |
do'2 sol8 do' |
la2\trill la4 |
r8 mi mi fa fa\trill^\markup\croche-pointee-double mi |
mi4( re4.)\trill do8 |
do4 r8 mi mi mi fa sol |
la4 la8 si do'4 si8 la |
sold4\trill sold8 mi la si do' la |
re'4 la8 si sol4\trill sol8 la |
fad4\trill fad8 r16 re' re'8 la |
sib4 sib8 do' la4\trill si?8 do' |
si2.\trill |
sol2 fa8[ mi16] fa |
mi2\trill mi4 |
do'2 sol8 do' |
la2\trill la4 |
r8 mi mi fa fa8.\trill mi16 |
mi4( re4.)\trill do8 |
do2. |
