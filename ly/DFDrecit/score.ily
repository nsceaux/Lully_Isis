\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1 s2. s1 s4 \bar "" \break s2. s1 s2.\break s2.*2 s1 s2.\pageBreak
        s2.*6\break s2.*6\break s1 s2. s1 s2. \bar "" \break
        s4 s1*3\break s1*4 s2.\break s2.*5\pageBreak
        s2. s1*2 s2.*3\break s2.*7\break s2.*6\break
        s2.*7\break s2.*6\pageBreak
        s2.*4 s1\break s1*3 s2 \bar "" \break s4 s1 s2.*3\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
