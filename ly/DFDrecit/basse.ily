\clef "basse" do2. si,4 |
do2 do8 si, |
la,2 re4 re, |
sol, sol2 mi4 |
fa2 mi |
re4. do16 si, la, sol, fa,8 |
mi,2 mi8 re |
dod2. |
re4. si,8 mi do re re, |
sol,4 sol fa |
mi2. |
fa4 dod re~ |
re8 do si,2 |
do2. |
si,4 la, re |
sol,2 sol8 fa |
mi2. |
fa4 dod re~ |
re8 do si,2 |
do mi,4 |
fa, sol,2 |
do2. |
la,1 |
sib,2. |
do1 |
re2 sol8 fa mi re |
do2 re4 re, |
sol,2 do |
si,4 do fa,2 |
mi,1 |
fa,8 sol, la,4 re,2 |
sol,2 fad, |
sol,4 do re re, |
sol,2 sol4 |
fad2. |
sol2~ sol8 fa |
mi2 fa8 mi |
re do si,4 do |
fa, sol,2 |
do2 si,4 |
la,2 fad, |
sol, sol8 fa mi re |
do re mi2 |
fa2. |
mi4 re do |
sol sol,2 |
do4. si,8 la,4 |
mi4. re8 do si, |
la,2. |
si, |
do4 do'8 si la sol |
fad2~ fad8 sol |
mib do re4 re, |
sol, sol fa |
mi2. | \allowPageTurn
fa4. mi8 re4 |
sol sold2 |
la4 re2 |
mi4 mi'8 re' do' si |
la4 sol4. fa8 |
mi4. mi8 re do |
si,2~ si,8 do |
la,4 re re, |
sol, sol fa |
mi2. |
fa |
mi4 re do |
sol sol,2 |
do2. |
si, |
do4 sib,8 la, sol, fa, |
mi,2. |
fa,2~ fa,8 sol, |
la,2 fa,4 |
sol,2. |
do2~ do8 sib, la, sol, |
fa,4 fa8 re la4 re |
mi4. re8 do si, la, sol, |
fad,4. sol,8 mi,2 |
re,4 re8 mi fad4 |
sol4. do8 re4 re, |
sol,8 sol fa mi re do |
si,2. |
<< { do8 re do sib, la, sol,16 fa, } \\ \sugNotes { do8 do sib, la, sol, fa, } >> |
mi,2. |
fa,2 fa,8 sol, |
la,2 fa,4 |
sol,2. |
<< do \\ \sug do, >>  |
