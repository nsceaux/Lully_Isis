Je vous ai -- me, Nym -- phe char -- man -- te,
un a -- mant im -- mor -- tel cherche à plaire à vos yeux.

Pan est un Dieu puis -- sant, je ré -- ve -- re les Dieux ;
mais le nom d’a -- mant m’é -- pou -- van -- te.

Pour vous fai -- re trou -- ver le nom d’a -- mant plus doux,
j’y join -- dray le ti -- tre d’é -- poux.
Je n’au -- ray pas de peine
à m’en -- ga -- ger
dans une ai -- ma -- ble chaî -- ne,
je n’au -- ray pas de peine
à m’en -- ga -- ger
pour ne ja -- mais chan -- ger.
Ai -- mez un Dieu qui vous a -- do -- re,
u -- nis -- sons- nous d’un nœud char -- mant.

Un E -- poux doit être en -- co -- re
plus à crain -- dre qu’un a -- mant.

Dis -- si -- pez de vai -- nes al -- lar -- mes,
e -- prou -- vez l’a -- mour & ses char -- mes,
con -- nois -- sez ses plus doux ap -- pas :
Non, ce ne peut ê -- tre
que fau -- te de le con -- noî -- tre
qu’il ne vous plaît pas.

Les maux d’au -- truy me ren -- dront sa -- ge.
Ah ! quel mal- heur
de lais -- ser en -- ga -- ger son cœur !
Pour -- quoy faut- il pas -- ser le plus beau de son â -- ge
dans u -- ne mor -- tel -- le lan -- gueur ?
Ah ! quel mal- heur !
Pour -- quoy n’a -- voir pas le cou -- ra -- ge
de s’af -- fran -- chir de la ri -- gueur
d’un fu -- neste es -- cla -- va -- ge ?
Ah ! quel mal- heur
de lais -- ser en -- ga -- ger son cœur !

Ah ! quel dom -- ma -- ge,
ah ! quel dom -- ma -- ge,
que vous ne sa -- chiez pas ai -- mer !
Que vous sert- il d’a -- voir tant d’at -- trais en par -- ta -- ge,
si vous en né -- gli -- gez le plus grand a -- van -- ta -- ge ?
Que vous sert- il de sça -- voir tout char -- mer ?
Ah ! quel dom -- ma -- ge
ah ! quel dom -- ma -- ge
que vous ne sa -- chiez pas ai -- mer.
