\clef "vbas-dessus" <>^\markup\character Junon
r8 la' la' la' re'' fad' |
sol'4 r16 sol' sol' si' sol'8 sol' |
mi'4\trill mi' r8 sol'16 sol' do''8 do''16 re'' |
mi''8. do''16 do''8 mi'' la'8.\trill si'16 |
sol'4 sol'8 r r2 |
R1*3 |
r4 do''4 r8 sol' la' la'16 la' |
re'4 re'' r8 re'' sol' sol'16 sol' |
mi'4\trill mi''4. mi''8 sold'8\trill sold'16 la' |
si'4 si'8 do'' re''4 re''8 mi'' |
dod''4\trill dod''16 la' la' la' re''8 re'' |
si'8.\trill do''16 re''8 mi'' mi''8.\trill re''16 |
re''4 r8 sol'16 sol' mi'8\trill mi' mi' fa'16 sol' |
la'4 do''8 do''16 re'' mi''4 mi''8 mi''16 mi'' |
la'4\trill la'8 r re''4 re''8 re'' |
si'4\trill si' sold'8\trill sold' sold' si' |
mi'4 r8 si' do''4 do''8 mi' |
fa'4 fa'8 la' re' re' r re'' |
si'4\trill si'8 sol' do''4 do''8 do'' |
la'4\trill la' r do''8 fa'' |
re''4\trill re''8 la' si'4.\trill do''8 |
do''2
\clef "vbas-dessus" <>^\markup\character-text Io \line {
  poursuivie par la Furie
}
-\tag #'recit ^\markup\italic {
  La Furie sort des Enfers, elle poursuit Io, elle l’enleve,
  & Junon remonte dans le ciel.
}
r4 mi'' |
la'2. r8 fa'' |
re''4\trill r8 re'' re'' re'' mi'' fa'' |
mi''2\trill r |
