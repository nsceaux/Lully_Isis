\clef "basse" re2.\repeatTie |
si, |
\tieDashed do1~ |
do2 re4 |
sol,4~ sol,16 sol^\markup\italic [Guay] la si do'8 do'16 si do'8 do |
sol8 sol16 fa sol8 mi la la16 sol la8 fa |
sol8 sol16 fa sol8 sol, do do16 si, do8 sol, |
la,8 la,16 sol, la,8 mi, fa, re, sol,4 |
do,2 do~ |
do4 si,2. |
do2~ do8 la, mi4 |
sold,1 |
la,4 la fad |
sol8 la si sol do' do |
sol4 sol, do8 sib, la, sol, |
fa,4 fa dod2 |
re1~ |
re2 mi |
sold, la, |
fa, sol, |
sol mi |
fa1 |
fad2 sol4 sol, |
do1 |
fa |
sol2 sol, |
do1 |
