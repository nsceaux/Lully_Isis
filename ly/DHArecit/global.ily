\key do \major
\digitTime\time 3/4 \midiTempo#80 s2.*2
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*7
\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2.*2
\time 4/4 s1*2
\time 2/2 \midiTempo#160 s1*11 \bar "|."
