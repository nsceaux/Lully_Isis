\score {
  \new ChoirStaff <<
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*2 s1 s2 \bar "" \pageBreak
        s4 s1*4\break s1*4\break s2.*2 s1\break s1*4\break s1*4\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
