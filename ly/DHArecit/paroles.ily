Re -- voy le jour, Ar -- gus ; que ta fi -- gu -- re chan -- ge.

Et vous, Nymphe, ap -- pre -- nez com -- ment Ju -- non se van -- ge.
Sors, bar -- bare E -- rin -- nis, sors du fond des en -- fers,
vien, prend soin de ser -- vir ma ven -- gean -- ce fa -- ta -- le,
et d’en mon -- trer l’hor -- reur en cent cli -- mats di -- vers :
E -- pou -- van -- te tout l’u -- ni -- vers
par les tour -- ments de ma ri -- va -- le.
Vien la pu -- nir au gré de mon cou -- roux ;
re -- dou -- ble ta rage in -- fer -- na -- le,
et fay, s’il se peut, qu’elle é -- ga -- le
la fu -- reur de mon cœur ja -- loux.

O Dieux ! O Dieux ! où me re -- dui -- sez- vous ?
