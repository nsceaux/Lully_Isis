\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*3 s2.\break s1*2 s4 \bar "" \break s2. s1 s2 \bar "" \break
      }
    >>
  >>
  \layout { }
  \midi { }
}
