\clef "vbas-dessus" <>^\markup\character [Io]
r2 r4 r8 do'' |
mi'4 mi'8 r r sol' do'' do''16 do'' |
la'8.\trill la'16 r8 re'' si'8.\trill si'16 si'8 do'' |
re'' re'' do'' si' la'\trill sol' |
fad'\trill fad' r16 si' do'' re'' sol'8 la'16 si' do''8 do''16 re'' |
si'4\trill r8 sol' mi'\trill mi'16 mi' fa'8 fa'16 sol' |
la'8. la'16 r8 do''16 do'' dod''8.\trill dod''16 re''8 re''16 mi'' |
fa''8 re''16 re'' la'8\trill la'16 si' do''8 do''16 do'' sol'8 sol'16 do'' |
la'8\trill la'16 re'' si'8\trill do'' re'' mi'' mi''8.\trill re''16 |
re''2 r4 r8 si' |
sol'\trill sol' r sol'16 sol' do''8 do''16 mi' fa'8 fa'16 mi' |
mi'4\trill
