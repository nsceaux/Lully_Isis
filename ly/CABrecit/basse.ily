\clef "basse" do1~ |
do |
fa4 fad sol2 |
si,4 do dod |
re si, mi8 do re re, |
sol,2 do8 sib, la,8. sol,16 |
fa,4 fa mi8 la16 sol fa8. mi16 |
re4 re, la, mi |
fa8. re16 sol8 la si do' do4 |
sol,2 sol~ |
sol mi4 re |
\once\tieDashed do4*3/4~ \hideNotes do16
