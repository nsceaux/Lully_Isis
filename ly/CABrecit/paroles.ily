Où suis- je, d’où vient ce Nu -- a -- ge !
Les on -- des de mon Pere, & son char -- mant ri -- va -- ge,
ont dis -- pa -- ru tout à coup à mes yeux !
Où puis- je trou -- vez un pas -- sa -- ge ?
La ja -- lou -- se Rei -- ne des cieux
me fait- el -- le si tôt a -- che -- ter l’a -- van -- ta -- ge
de plaire au plus puis -- sant des Dieux ?
Que vois- je ! quel é -- clat se ré -- pand dans ces lieux !
