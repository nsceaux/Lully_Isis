\key fa \major
\digitTime\time 2/2 \midiTempo#160 s1*2
\digitTime\time 3/4 \midiTempo#80 s2.*2
\time 4/4 s1
\digitTime\time 3/4 s2.*4
\digitTime\time 2/2 \midiTempo#160 s1 \bar "||"
\digitTime\time 3/4 \midiTempo#80 s2.
\time 4/4 s1*3
\digitTime\time 2/2 \midiTempo#160 s1*2
\digitTime\time 3/4 \midiTempo#80 s2.
\time 4/4 s1
\digitTime\time 2/2 \midiTempo#160 s1*2
\digitTime\time 3/4 \midiTempo#80 s2.
\time 4/4 s1. \bar "|."
