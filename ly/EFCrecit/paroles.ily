\tag #'(recit basse) {
  C’est con -- tre moy qu’il faut tour -- ner
  vô -- tre ri -- gueur la plus fu -- nes -- te ;
  d’u -- ne vie o -- di -- euse, ar -- ra -- chez- moy le res -- te,
  hâ -- tez- vous de la ter -- mi -- ner.
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  C’est aux Par -- ques de l’or -- don -- ner,
  c’est aux Par -- ques de l’or -- don -- ner.
}
\tag #'(recit basse) {
  Fa -- vo -- ri -- sez mes vœux, Dé -- es -- ses sou -- ve -- rai -- nes,
  qui re -- glez du Des -- tin les im -- mu -- a -- bles loix ;
  fi -- nis -- sez mes jours & mes pei -- nes,
  ne me con -- dam -- nez pas à mou -- rir mil -- le fois.
  Fi -- nis -- sez mes jours & mes pei -- nes,
  ne me con -- dam -- nez pas à mou -- rir mil -- le fois.
}
