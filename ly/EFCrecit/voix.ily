<<
  \tag #'(recit basse) {
    \clef "vbas-dessus"
    \tag #'recit <>^\markup\character-text Io \line { parlant à la suite des Parques }
    \tag #'basse <>^\markup\character Io
    r8 fa' fa' sol' la'\trill la' la' fa' |
    do'' do'' do'' re'' mib'' mib'' mib'' fa'' |
    re''\trill re'' r fa''16 fa'' re''8 re''16 re'' |
    sib'8 sib' sib' re'' sib'8.\trill la'16 |
    la'8\trill la' r fa''16 fa'' re''8\trill re''16 re'' re''8. mi''16 |
    do''2 <<
      \tag #'basse { s4 s2.*3 s1 \ffclef "vbas-dessus" }
      \tag #'recit { r4 R2.*3 R1 }
    >> <>^\markup\character Io
    r8 fa' fa' fa' do''8. do''16 |
    do''4. do''8 dod''8\trill dod'' re'' mi'' |
    fa''8. fa''16 r8 la'16 la' re''4 re''8 la' |
    sib'4 sib'8 sib'16 sib' sol'4.\trill sol'8 |
    sol'2 r4 do''8 do'' |
    do''4 re'' sib'\trill sib'8 la' |
    la'\trill la' r16 fa'' fa'' fa'' re''8. re''16 |
    si'4 re''8 mib'' do''4\trill do''8 si' |
    do''2 r4 sol'8 sol' |
    la'4 do'' fa' fa'8 la' |
    re'\trill re' r16 sib' sib' sib' sol'8. sol'16 |
    mi'4\trill sol'8 la' fa'4\trill fa'8 mi' |
    fa'2
  }
  \tag #'(vdessus basse) {
    <<
      \tag #'basse { s1*2 s2.*2 s1 s2 \ffclef "vdessus" <>^\markup\character Chœur }
      \tag #'vdessus {
        \clef "vdessus" R1*2 R2.*2 R1 r4 r
        <>^\markup\character Chœur de la suite des Parques
      }
    >> do''8 do'' |
    do''4 do''8 do'' do'' do'' |
    re''2 re''8 re'' |
    re''4 re''8 re'' re'' re'' |
    do''1 |
    \tag #'vdessus { R2. R1*5 R2. R1*3 R2. R1 r2 }
  }    
  \tag #'vhaute-contre {
    \clef "vhaute-contre" R1*2 R2.*2 R1 |
    r4 r mi'8 mi' |
    fa'4 fa'8 fa' fa' fa' |
    fa'2 fa'8 fa' |
    fa'4 fa'8 fa' fa' fa' |
    fa'1 |
    R2. R1*5 R2. R1*3 R2. R1 r2
  }
  \tag #'vtaille {
    \clef "vtaille" R1*2 R2.*2 R1 |
    r4 r sol8 sol |
    la4 la8 la la la |
    la2 la8 la |
    sib4 sib8 sib sib sib |
    la1 |
    R2. R1*5 R2. R1*3 R2. R1 r2
  }
  \tag #'vbasse {
    \clef "vbasse" R1*2 R2.*2 R1 |
    r4 r do8 do |
    fa4 fa8 fa fa fa |
    re2 re8 re |
    sib,4 sib,8 sib, sib, sib, |
    fa,1 |
    R2. R1*5 R2. R1*3 R2. R1 r2
  }
>>
