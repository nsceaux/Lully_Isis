\clef "basse" \tag #'bc-part <>^"[B.C.]" fa,2 fa |
la,1 |
sib,2.~ |
sib, |
fa2 sol4 sol, |
do2 <>^\markup\italic viste do8 do |
fa4. fa8 fa fa |
re2 re8 re |
sib,4 sib,8 sib, sib, sib, |
fa,1 |
fa2. |
mi2 la8 sol fa mi |
re1 |
sol2 si, |
do1 |
fad,2 sol, |
re2. |
sol2 lab8 fa sol sol, |
do1 |
la, |
sib,2. |
do2 re8 sib, do do, |
fa,2
