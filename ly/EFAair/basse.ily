\clef "basse"
\tag #'basse-continue \footnoteHere #'(0 . 0) \markup { Matériel 1677 : B.C. tacet. }
fa8 |
fa2. fa4 |
sib2. sib4 |
do'2 do |
fa4 re8 re re4 sol |
mi mi8 mi mi4 la |
fa4 fa8 mi re4 re |
sol2 sol,4 fa, |
mi, fa, sol,2 |
do,2. r8 fa |
do,4 do do do |
sol, sol,8 la, sib, do re mi |
fa4 fa8 sol la sib do' re' |
mib'4 mib8 fa sol la sib do' |
re'4 re8 mi fa sol la sib |
do'4. do'8 la4. la8 |
re'2 fad4. fad8 |
sol2 do |
re4 sol, re,2 |
sol,~ sol,8 sol la sib |
do'4. do'8 la4~ la16 la sib do' |
re'4 re'8 do' sib la sol fa |
mi do re mi fa fa, sol, la, |
sib,2~ sib,8 sol, la, sib, |
do2. do4 |
re4 sib, do do, |
fa,4 fa8 mi fa sol la sib |
do'4 do do do |
fa,2.
