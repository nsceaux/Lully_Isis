\clef "dessus" la''8 |
la'' sib'' la'' sol'' fa'' mi'' fa'' do'' |
re'' mib'' re'' do'' sib' do'' sib' la' |
sol' la' sib' sol' do'' re'' do'' sib' |
la'4 fa''8 fa'' fa''4 re'' |
sol'' sol''8 sol'' sol''4 mi'' |
la'' la''8 sol'' fa'' mi'' re'' do'' |
si'\trill sol' la' si' do'' re'' mi'' fa'' |
sol''4 la'' re''4.\trill do''8 |
do''2. r8 la'' |
do''4 do''8 re'' mi'' fa'' sol'' la'' |
sib''4. sib''8 sib''4. do'''8 |
la''4.\trill la''8 la''4. sib''8 |
sol''4.\trill sol''8 sol''4. la''8 |
fa''4.\trill fa''8 fa''4. sol''8 |
mi''\trill fa'' sol'' mi'' la'' sib'' la'' sol'' |
fad'' sol'' fad'' mi'' re'' do'' re'' la' |
sib' do'' re'' sib' mib'' re'' do'' sib' |
la'4 sib' la'4.\trill sol'8 |
sol'4~ sol'16 re'' mi'' fa'' sol''4. sol''8 |
mi''4~ mi''16 mi'' fa'' sol'' la''4. la''8 |
fa''4~ fa''16 fa'' sol'' la'' sib''4. sib''8 |
sol''4. sol''8 la''4. la''8 |
re''4. re''8 sol''4. sol''8 |
sol'' fa'' mi'' re'' do'' re'' do'' sib' |
la'4 re'' sol'4.\trill fa'8 |
fa'1~ |
fa'4 do''8 re'' mi'' fa'' sol'' la'' |
fa'2.
