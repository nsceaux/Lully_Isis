\key fa \major \midiTempo#160
\tempo\markup\smaller\normal-text\italic [fort viste]
\digitTime\time 2/2 \partial 8 s8 \bar ".!:" s1*8 \alternatives s1 s1
\bar ".!:" s1*15 \alternatives s1*2 s2. \bar "|."
