\clef "taille" fa'8 |
fa'4 fa'8 sol' la'4 fa' |
fa'2 sol' |
sol'4 do' do'2 |
do'4 re'8 re' re'4 re' |
do' do'8 do' do'4 do' |
fa'4 fa'8 sol' la' sol' fa' mi' |
re' si do' re' mi' fa' sol' la' |
sol'4 fa' fa'4. mi'8 |
mi'2.\trill r8 fa' |
mi'4 mi'8 re' do' re' mi' fa' |
sol'4. sol'8 sol'4. sol'8 |
fa'4. fa'8 fa'4. fa'8 |
sol'4 sib' sib'4. la'8 |
la'4. la'8 la'4. sol'8 |
sol'4 mi' mi'4. mi'8 |
re'4 fad'8 sol' la'4. la'8 |
re'4 sol'8 fa' mib'4. mib'8 |
re'4. re'8 re'4 la |
sib4. sib'16 la' sol'4. sol'8 |
sol'4. sol'8 fa'4. fa'8 |
fa'4. fa'8 fa'4 sib' |
sib'2 la'4. la'8 |
la'2 sol'4. sol'8 |
sol'4. sol'8 sol'8 fa' mi'4 |
re' re' do'4. sib8 |
la1 |
r4 mi'8 re' do' re' mi' fa' |
la2.
