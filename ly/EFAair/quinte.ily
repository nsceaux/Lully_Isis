\clef "quinte" la8 |
la sol la sib do'4. do'8 |
sib la sib do' re'4. re'8 |
do'4. do'8 sol2 |
fa4 fa8 sol la4 sol |
sol sol8 sol sol4 la |
la2 re'4. re'8 |
re'2. re'4 |
mi'4 la sol4. sol8 |
sol2. r8 la |
sol4 sol sol do' |
sib4. sol8 sol4. sol8 |
la2 fa'4. fa'8 |
mib'2 mib'4. re'8 |
re'8 mib' fa'4 fa'4. << fa'8 \\ \sug re' >> |
do'4. do'8 do'4. do'8 |
re'2. re'4 |
re'8 do' sib re' do'4. do'8 |
do'4 sib8 do' re'4. re'8 |
re'2. re'4 |
do'4. do'8 do'4. << fa'8 \\ \sug do'8 >> |
re'4. re'8 re'4 sib |
do'4. do'8 do'2 |
fa4. fa8 sib4. sib8 |
sol4 do'2 do'4 |
fa sol sol do' |
\once\tieDashed do'1~ |
do'4 sol sol do' |
do'2.
