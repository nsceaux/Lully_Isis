\clef "haute-contre" do''8 |
do'' re'' do'' sib' la' sib' do'' la' |
sib' do'' sib' la' sol' la' sol' fa' |
mi'4.\trill mi'8 mi' fa' sol' mi' |
fa'4 la'8 la' re''4 si' |
mi''4 mi''8 mi'' mi''4 do'' |
do'' la'8 la' la'4. la'8 |
sol'4. sol'8 sol'4. re''8 |
do''4 do'' si'4.\trill do''8 |
do''2. r8 do'' |
do''4 do'' do'' do'' |
re''4. re''8 re''4. do''8 |
do''4. do''8 do''4.\trill sib'8 |
sib'4 mib'' re''4. do''16 sib' |
la'4 re'' do''4. sib'16 la' |
sol'4. sol'8 do'' re'' do'' sib' |
la'2 la'4. la'8 |
sol' la' sib' sol' do'' sib' la' sol' |
fad'4 sol' fad'4.\trill sol'8 |
sol'4. sol'16 la' sib'4. sib'8 |
sol'4~ sol'16 sol' la' sib' do''4. do''8 |
la'4~ la'16 la' sib' do'' re''4. re''8 |
do''2 do''4. do''8 |
sib'4. sib'8 sib' la' sol' fa' |
mi'4. mi'8 mi'4. mi'8 |
fa'4 fa' mi'4. fa'8 |
fa'1 |
r4 do'' do'' do'' |
fa'2.
