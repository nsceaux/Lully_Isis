\clef "dessus" sol''4 \once\slurDashed sol''4.\trill( fa''16 sol'') |
la''4. sib''8 la''4 |
fa'' re'' sol'' |
mi''8 fa'' mi'' re'' do''4 |
sol''4 \once\slurDashed sol''4.\trill( fa''16 sol'') |
la''4. sib''8 la''4 |
fa'' re'' sol'' |
mi''2.\trill |
re''4 \once\slurDashed re''4.(\trill do''16 re'') |
mi''4. fa''8 mi''4 |
do'' la' re'' |
si'8 do'' si' la' sol'4 re'' \once\slurDashed re''4.(\trill do''16 re'') |
mi''4. fa''8 sol''4 |
la'' re''2\trill |
do''2. |
