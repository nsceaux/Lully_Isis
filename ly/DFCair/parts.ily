\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille)
   (quinte #:system-count 2)
   (basse)
   (basse-continue)
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
