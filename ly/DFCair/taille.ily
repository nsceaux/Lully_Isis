\clef "taille" mi'4 mi'2 |
fa' fa'4 |
la' sol'2 |
sol' sol'4 |
mi'2 mi'4 |
fa'2 fa'4 |
la' sol'2 |
sol'2. |
sol'4 sol'2 |
sol'8 fa' mi'4 mi' |
mi' re' re' |
re'8 mi' re' do' si4 |
sol' sol'2 |
sol'4. fa'8 mi'4 |
re' re'2\trill |
<< \sug mi'2. \\  do'2. >> |
