\clef "quinte" do'4 do'2 |
la do'4 |
re' re'2\trill |
do' do'4 |
do' sol'2 |
do' do'4 |
re' re'2\trill |
do'2. |
re'4 re'2 |
do'2 sol4 |
la la2 |
sol re'4 |
re' re'2 |
do' do'4 |
la sol2 |
sol2. |
