\clef "haute-contre" do''4 do''2 |
do'' do''4 |
do'' \slurDashed si'4.(\trill la'16 si') |
do''2 do''4 |
do'' do''2 |
do'' do''4 |
do'' si'4.(\trill la'16 si') |
do''2. |
si'4 si'4.( la'16 si') |
do''4 sol' sol' |
sol' fad'4.(\trill mi'16 fad') |
sol'2 sol'4 |
si' si'4.( la'16 si') |
do''2 do''4 |
do'' si'4.(\trill la'16 si') |
do''2. |
