\clef "basse" \footnoteHere #'(0 . 0) \markup { Matériel 1677 : B.C. tacet. }
do'4 do2 |
fa4. sol8 fa mi |
re4 sol sol, |
do2 do4 |
do' sib2 |
la4. sol8 fa mi |
re4 sol sol, |
do2. |
sol4 fa2 |
mi4. re8 do si, |
la,4 re re, |
sol,2 sol,4 |
sol8 la si4 sol |
do' do8 re mi4 |
fa sol sol, |
do2. |
