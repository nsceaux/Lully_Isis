\clef "dessus" do''8 mi'' do'' |
sol'' mi'' la'' |
fa'' re'' sol'' |
mi'' do'' sol' |
do'' la' re'' |
si'8.\trill la'16 sol'8 |
si'8.\trill la'16 sol'8 |
re'' sol'' mi'' |
fa'' la'' fa'' |
re'' sol'' mi'' |
dod'' la' mi'' |
fa'' re'' la' |
si' sol' re'' |
mi'' do'' sol' |
la' fa'' re'' |
si' sol'' mi'' |
do'' la' fa'' |
re'' si' mi'' |
do'' re'' si' |
do''4. |
do'' |
