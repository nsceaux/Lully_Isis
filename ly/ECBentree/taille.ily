\clef "taille" mi'8 sol' sol' |
sol' sol' fa' |
la' sol' sol' |
sol' mi' re' |
do' re' re' |
re'4 re'8 |
re'4 re'8 |
sol' sol' la' |
la' fa' fa' |
fa' mi' mi' |
mi' dod' dod' |
re' re' re' |
re' si si |
do' do' do' |
do' do' fa' |
re' si mi' |
mi' fa' la |
si si si |
<<
  { la fa' fa' | mi'4. | mi' | } \\
  \sugNotes { la8 re' re' | do'4. | do' | }
>>
