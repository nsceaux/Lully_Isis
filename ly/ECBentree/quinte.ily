\clef "quinte" do'8 do' mi' |
re' mi' do' |
re' re' sol |
sol sol sol |
la la la |
sol8. la16 si8 |
sol8. la16 si8 |
re' mi' mi' |
re' re' re' |
re' sib sib |
la la la |
la la la |
sol re' re' |
do' do' do' |
la la re' |
re' re' do' |
do' do' re' |
re' re' sol |
la la sol |
sol4. |
sol |
