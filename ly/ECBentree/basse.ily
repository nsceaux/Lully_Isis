\clef "basse" \footnoteHere #'(0 . 0) \markup { Matériel 1677 : B.C. tacet. }
do4 do'8 |
si do' fa |
re sol sol, |
do do si, |
la, re re, |
sol, sol16 fa mi re |
sol,4. |
sol8 mi la |
re re re |
sib sol sol |
la la, la, |
re re re |
mi mi mi |
mi mi mi |
fa fa fa |
sol sol sol |
la fa re |
sol sol mi |
la fa sol |
do8. re16 mi fa |
do4. |
