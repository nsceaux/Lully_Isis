\clef "haute-contre" sol'8 do'' do'' |
re'' do'' do'' |
do'' \slurDashed si'(\trill la'16 si') |
do''8 sol' sol' |
sol' fad'(\trill mi'16 fad') |
sol'4 sol'8 |
sol'4 sol'8 |
si' si' dod'' |
re'' re'' la' |
sib' sib' sol' |
mi' mi' mi' |
re' fa' fa' |
sol' sol' sol' |
sol' sol' sol' |
fa' la' la' |
sol' si' do'' |
la' la' la' |
sol' sol' sol' |
mi' fa' re' |
mi'4. |
mi'4. |
