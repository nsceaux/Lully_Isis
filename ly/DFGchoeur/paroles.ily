\tag #'(recit basse) {
  Je ne puis vous quit -- ter : mon cœur s’at -- tache à vous
  par des nœuds trop forts & trop doux.
  
  Mes Com -- pa -- gnes, ve -- nez… C’est en vain que j’ap -- pel -- le.
  
  E -- coû -- tez, In -- grate, é -- cou -- tez,
  un Dieu char -- mé de vos beau -- tez,
  qui vous jure un a -- mour fi -- del -- le.
  
  Je de -- clare à l’a -- mour u -- ne guerre im -- mor -- tel -- le.
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Cru -- elle, ar -- rê -- tez.
  Ar -- rê -- tez, cru -- el -- le.
}
\tag #'(recit basse) {
  On me re -- tient de tous cô -- tez.
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Cru -- elle, ar -- rê -- tez, ar -- rê -- tez, ar -- rê -- tez.
}
\tag #'(recit basse) {
  Dieux, pro -- tec -- teurs de l’in -- no -- cen -- ce,
  Na -- ya -- des, Nym -- phes de ces eaux,
  j’im -- plore i -- cy vôtre as -- sis -- tan -- ce.

  Où vous ex -- po -- sez- vous ? Quels pro -- di -- ges nou -- veaux ?
  La Nymphe est chan -- gée en Ro -- seaux !
}
