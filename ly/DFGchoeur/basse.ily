\clef "basse" \tag #'bc-part <>^"[B.C.]" do2 si, |
la, sold, |
la, sol, |
fa,1 |
mi,2. mi4 |
la1 |
fad |
sol |
dod |
re2 re, |
la,4 re sol, |
do2 re |
mib4. do8 re re, |
sol,2 sol8 fa |
mi2 re |
do4. |
r8 r do |
fa fa re |
la r r |
r la la |
fa4 re8 |
sol4 sol8 |
\tag #'bc-part <>^"[B.C.]"
sol2 fad |
sol4 sol8 |
la la la |
si si do' |
sol sol sol, |
do4. \tag #'bc-part <>^"[B.C.]" si,8 la,2~ |
la,1 |
re |
fad,2 sol, |
sol4. fa8 mi4 re8 do |
sol,1 |
sol,2 mi,~ |
mi, fa, |
sol,1 |
la,4. fa,8 sol,2 |
do1 r2 |
