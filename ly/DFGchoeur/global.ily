\key do \major
\time 4/4 \midiTempo#80 s1*3
\digitTime\time 2/2 \midiTempo#160 s1
\time 4/4 \midiTempo#80 s1
\time 2/2 \midiTempo#160 s1
\time 4/4 \midiTempo#80 s1
\digitTime\time 2/2 \midiTempo#160 s1*3
\digitTime\time 3/4 \midiTempo#80 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.*2
\digitTime\time 2/2 \midiTempo#160 s1
\time 3/8 \midiTempo#80 s4.*7
\time 2/2 \midiTempo#160 s1
\time 3/8 \midiTempo#80 s4.*4
\time 2/2 \midiTempo#160 s1
\time 4/4 \midiTempo#80 s1*4
\time 2/2 \midiTempo#160 s1*5
\time 3/2 s1. \bar "|."
