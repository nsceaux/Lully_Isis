<<
  \tag #'(recit basse) {
    \ffclef "vbasse" <>^\markup\character Pan
    r4 sol8 sol sold4\trill sold8 sold |
    la4. la8 si si si si |
    mi4 fa8 fa fa4. mi8 |
    la2 la4 si |
    sold4\trill
    \ffclef "vdessus" <>^\markup\character Syrinx
    r8 si'16 si' mi''8. mi''16 r8 r16 mi'' |
    dod''2\trill r4 la'8 la' |
    re''4 re''8 re'' re''2 |
    re''4 r
    \ffclef "vbasse" <>^\markup\character Pan
    sol4 sol |
    sol2. la4 |
    fa2 fa4 mi |
    mi8.\trill do'16 do'8 sib16[ la] sib4~ |
    sib8 la la\trill la fad4\trill re'8 << { \voiceOne sib \oneVoice } \new Voice { \voiceTwo \sug fad } >> |
    sol4 sol8 sol sol fad |
    sol4 sol8
    \ffclef "vdessus" <>^\markup\character-text Syrinx fuyant
    si'16 si' si'8 do''16 re'' |
    mi''4 do''8 do'' fa''4 fa''8 mi'' |
    mi''4\trill mi''8 |
  }
  \tag #'vdessus \clef "vdessus"
  \tag #'vhaute-contre \clef "vhaute-contre"
  \tag #'vtaille \clef "vtaille"
  \tag #'vbasse \clef "vbasse"
  \tag #'(vdessus vhaute-contre vtaille vbasse) {
    R1*10 R2. R1 R2.*2 R1 R4.
  }
>>
<<
  \tag #'(vdessus basse) {
    \tag #'vdessus <>^\markup\character-text Troupe de Sylvains & de Satyres qui arrêtent Syrinx
    \tag #'basse { \ffclef "vdessus" <>^\markup\character Chœur }
    r8 r do'' |
    do'' do'' re'' |
    mi'' r r |
    r mi'' mi'' |
    fa''4 fa''8 |
    re''4\trill re''8 |
  }
  \tag #'vhaute-contre {
    r8 r sol' |
    fa' fa' fa' |
    mi' r r |
    r la' la' |
    la'4 la'8 |
    sol'4 sol'8 |
  }
  \tag #'vtaille {
    r8 r mi' |
    la la si |
    do' r r |
    r do' do' |
    do'4 re'8 |
    si4\trill si8 |
  }
  \tag #'vbasse {
    r8 r do |
    fa fa re |
    la r r |
    r la la |
    fa4 re8 |
    sol4 sol8 |
  }
  \tag #'recit { R4.*6 }
>>
<<
  \tag #'(recit basse) {
    \tag #'basse \ffclef "vdessus" <>^\markup\character Syrinx
    re''4 re''8 re'' la'\trill la' la' re'' |
    si'4\trill
  }
  \tag #'(vdessus vhaute-contre vtaille vbasse) { R1 r8 r }
>>
<<
  \tag #'(vdessus basse) {
    \tag #'basse { \ffclef "vdessus" <>^\markup\character Chœur }
    si'8 |
    do'' do'' do'' |
    re'' re'' mi'' |
    re''\trill re'' sol'' |
    mi''\trill r r4 r2 |
  }
  \tag #'vhaute-contre {
    sol'8 |
    << { \voiceOne sol' sol' \oneVoice } \new Voice \sugNotes { \voiceTwo mi' mi' } >> la'8 |
    sol' sol' sol' |
    sol' sol' sol' |
    sol' r r4 r2 |
  }
  \tag #'vtaille {
    re'8 |
    do' do' fa' |
    re'\trill re' do' |
    do' do' si |
    do' r r4 r2 |
  }
  \tag #'vbasse {
    sol8 |
    la la la |
    si si do' |
    sol sol sol, |
    do r r4 r2 |
  }
  \tag #'recit { r8 | R4.*3 R1 }
>>
<<
  \tag #'(recit basse) {
    \tag #'basse \ffclef "vdessus" <>^\markup\character Syrinx
    mi''4 do''8 do'' la'\trill la' la' mi' |
    fa'4 fa'8 r16 re'' la'4\trill la' |
    do''8 do'' do'' si' si'2\trill |
    r8 sol' la' si' do'' do'' re'' mi'' | \noBreak
    re''2\trill re''4*1/2
    \once\override Staff.TextScript.self-alignment-X = #RIGHT
    \tag #'recit <>^\markup\italic {
      Syrinx se jette dans les Eaux.
    } s8
    \ffclef "vbasse"
    \tag #'basse <>^\markup\character Pan
    \tag #'recit <>^\markup\character-text Pan suivant Syrinx dans le Lac où elle s’est jettée
    r8 sol | \noBreak
    sol sol sol sol do'2 |
    r4 do'8 do' fa4 fa8 la |
    re2\trill r4 r8 sol |
    mi4\trill mi8 fa re4\trill re8 sol |
    do1 r2 |
  }
  \tag #'(vdessus vhaute-contre vtaille vbasse) { R1*9 R1. }
>>
