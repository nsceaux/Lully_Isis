\clef "basse" fa,2. fa4 |
mib reb8 do sib,4 fa8 mib |
reb2 do4. do8 |
fa4. fa8 la,4. la,8 |
sib,4 r8 lab,! sol, fa, do do, |
fa,2 fa8. fa16 do8. do16 |
reb8. reb16 sib,8. sib,16 mib8 mib mib reb |
do2 reb4. reb8 |
mib4 lab, mib,2 |
lab,4 lab8 sol fa2 |
mib4 reb8 do sib,4 fa8 mib |
reb2 do4. do8 |
fa4. fa8 la,4. la,8 |
sib,4. lab,8 sol, fa, do do, |
fa,4~ fa,16 do re mi fa4~ fa16 fa sol lab |
sib4 mib16 mib fa sol lab4 lab8. lab16 |
lab4 sol8. sol16 fa8. mib16 sib8 sib, |
mib4 mib8 do lab4~ lab16 lab sol fa |
sol fa mib re do sib, lab, sol, fa,4 sol, |
do,4. do8 fa2 |
mib4 reb8 do sib,4 fa8 mib |
reb2 do4. do8 |
fa4. fa8 la,4. la,8 |
sib,4 r8 lab,! sol, fa, do do, |
fa,2.*2/3 \once\set Staff.whichBar = ""
