\piecePartSpecs
#`((dessus #:score-template "score-dessus2")
   (basse #:music ,#{ <>_"[trio]" #})
   (basse-continue #:music ,#{ <>_"[à 3]" #})
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
