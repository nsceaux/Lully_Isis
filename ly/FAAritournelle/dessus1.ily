\clef "dessus" r2 r4 lab'8 lab' |
la'4 sib'8 do'' reb''4 do''8. do''16 |
fa''4 fa''8. sol''16 mi''4.\trill do''8 |
la'8.\trill do''16 do''8 re'' mib''4 \slurDashed mib''8.( re''16) |
re''4\trill r8 re'' mi'' fa'' fa'' mi'' |
fa''2 r8 do'' mib''8. mib''16 |
lab'8. lab'16 reb''8. reb''16 sib'2\trill |
r8 mib'' mib''8. mib''16 mib''8. reb''16 reb''8. reb''16 |
reb''4 do''8. reb''16 sib'8.\trill sib'16 do''8. reb''16 |
do''2\trill r4 lab'8 lab' |
la'4 sib'8 do'' reb''4 do''8. do''16 |
fa''4 fa''8. sol''16 mi''4.\trill do''8 |
la'8.\trill do''16 do''8 re'' mib''4 mib''8.( re''16) |
re''4\trill r8 re'' mi'' fa'' fa'' mi'' |
fa''2 r8 lab'' lab''8. lab''16 |
lab''8. sol''16 sol''8. sol''16 sol''4 fa''8 sol''16 mib'' |
re''4\trill mib''8. mib''16 lab''8. sol''16 fa''8.\trill mib''16 |
mib''8. sol''16 sol''8. sol''16 sol''8. fa''16 fa''8. fa''16 |
fa''4 mib''8. mib''16 mib''8. re''16 re''8.\trill do''16 |
do''2 r4 lab'8 lab' |
la'4 sib'8 do'' reb''4 do''8. do''16 |
fa''4 fa''8. sol''16 mi''4.\trill do''8 |
la'8.\trill do''16 do''8 re'' mib''4 mib''8.( re''16) |
re''4\trill r8 re'' mi'' fa'' fa'' mi'' |
fa''2
