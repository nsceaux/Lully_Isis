\clef "dessus" r2 r4 do''8 do'' |
do''4 reb''8 lab' sib' fa'16 sol' \footnoteHere #'(0 . 0) \markup {
  Ballard 1719 : \italic la♮, Matériel 1677 : \italic la♭
} lab'8. lab'16 |
\slurDashed lab'8.( sib'16) sib'8.\trill do''16 do''4. mi'8 |
fa'8. la'16 la'8 sib' do''4. do''8 |
fa'4 r8 fa' sol' lab'! sol' lab' |
fa'2 r8 lab' do''8. do''16 |
fa'8. fa'16 sib'8. sib'16 sol'2\trill |
r8 do'' do''8. do''16 do''8. fa'16 fa'8. sib'16 |
sol'4 lab'2 lab'8 sol' |
lab'2 r4 do''8 do'' |
do''4 reb''8 la' sib' fa'16 sol' lab'?8. lab'16 |
lab'8.( sib'16) sib'8. do''16 do''4. mi'8 |
fa'8. la'16 la'8 sib' do''4. do''8 |
fa'4. fa'8 sol' lab'! sol' lab' |
fa'2 r8 do'' fa''8. do''16 |
\footnoteHere #'(0 . 0) \markup {
  Ballard 1719 : \italic ré♭, Matériel 1677 : \italic ré♮
} re''4 mib''8. mib''16 do''4 lab''8 sib''16 sol'' |
\mergeDifferentlyDottedOn
fa''4\trill sol''8. sol''16 re''8. mib''16 << { re''8.\trill mib''16 } \\ \sugNotes { re''8(\trill do''16 re'') } >> |
mib''8. sib'16 sib'8 mib'' do''8. do''16 re''8. re''16 |
si'4 do''8. do''16 do''8. re''16 << { si'8. do''16 } \\ \sugNotes { si'8\trill la'16 si' } >> |
do''2 r4 do''8 do'' |
do''4 reb''8 lab' sib' fa'16 sol' lab'8. lab'16 |
lab'8.( sib'16) sib'8.\trill do''16 do''4. mi'8 |
fa'8. la'16 la'8 sib' do''4. do''8 |
fa'4 r8 fa' sol' lab'! sol' lab' |
fa'2
