\clef "basse" R1 |
r8 fa16 sol la8 fa do' do16 re mib8 do |
sol8 sol16 la sib8 sol re' re16 mi fa8 re |
sol do sol,4 do do'8 do' |
la fa16 sol la8 fa sib sib,16 do re8 sib, |
fa8. fa16 mi8. re16 do8 do16 re mi8 do |
re8 re16 mi fa8 re mi8 mi16 fa sol8 mi |
fa re sol sol, do fa, do,4 |
fa,8 fa16 sol la8 fa sib sol16 la sib8 sol |
do'8 la16 sib do'8 la re' sib do' do |
<<
  \tag #'basse fa,1
  \tag #'basse-continue {
    \once\tieDashed fa,1*7/8~ \hideNotes fa,16
  }
>>
