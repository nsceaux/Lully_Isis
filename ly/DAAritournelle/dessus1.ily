\clef "dessus" r2 r8 do''16 re'' mi''8 do'' |
fa''4. fa''8 fa''8. fa''16 mib''8. fa''16 |
re''4.\trill re''16 mi'' fa''4. fa''8 |
fa'' mi'' \once\slurDashed re''( mi''16) fa'' mi''8 mi''16 fa'' sol''8 mi'' |
la'' la''16 sol'' fa''8 fa''16 mi'' re''4. re''8 |
do''8.\trill do''16 do''8. re''16 mi''8 sol'' sol''8. sol''16 |
fa''8. fa''16 fa''8. fa''16 mi''4.\trill mi''8 |
re''8.\trill re''16 re''8. sol''16 mi''8 fa'' mi''8.\trill fa''16 |
fa''8 fa'' mib''8. fa''16 re''4 \once\slurDashed re''8( mi''?16) fa'' |
mi''8 fa'' mi''16 re'' do'' sib' la'8 sib' sol'8.\trill fa'16 |
fa'1 |
