\score {
  \new StaffGroup <<
    \new GrandStaff <<
      \new Staff << <>^"Violons" \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s1*4\break s1*4\break }
    >>
  >>
  \layout { }
  \midi { }
}