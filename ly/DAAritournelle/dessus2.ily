\clef "dessus" r8 fa'16 sol' la'8 fa' do''8. do''16 sib'8. do''16 |
la'4. la'8 sol'4.\trill sol'16 la' |
sib'8. sib'16 sib'8. sib'16 la'8. la'16 re''8. re''16 |
si'8 do'' si'8.\trill do''16 do''8 do''16 re'' mi''8 do'' |
fa''8 la'16 sib' do''8 la' sib'4 fa'8 fa'16 sol' |
la'8. la'16 la'8. si'16 do''8 mi'' mi''8.\trill mi''16 |
mi''8. re''16 re''8. re''16 re''8. do''16 do''8. do''16 |
do''8. do''16 sib'8.\trill sib'16 sib'8 la' sol' la'16 sib' |
la'8.\trill la''16 la''8. la''16 la''4 sol''8.\trill sol''16 |
sol''8 la'' sol'' la'' fa'' sol'' mi''8.\trill fa''16 |
fa''1 |
