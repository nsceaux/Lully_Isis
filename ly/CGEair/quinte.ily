\clef "quinte" sib4 |
sib8 do' re'4 re' re' |
re' re'2 re'4 |
do'2 la4 re'8 do' |
sib4 re' << { sol'4. sol'8 } \\ \sugNotes { do'4. do'8 } >> |
do'2. do'4 |
sib2. sib4 |
sol2 do'4 fa' |
fa'2. re'4 |
do'2 re'4 do' |
do'2. fa'4 |
re'2 mi'4. mi'8 |
mi'2. la'4 |
fa'4 sol'8 mi' mi'4 la |
la2. la8 la |
sol4 << \sug re'2 \\ sol2 >> re'8 re' |
do'2 do'4. do'8 |
do'4. do'8 re'4. re'8 |
re'2. sib4 |
sib do' la re' |
re'2. re'8 re' |
do'2. la4 |
la2. sib4 |
sib do' la re' |
re'2. re'8 re' |
do'2. la4 |
la2. sib4 |
sib do' la re' |
re'2.
