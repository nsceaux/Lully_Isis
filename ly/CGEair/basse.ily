\clef "basse"
\footnoteHere #'(0 . 0) \markup { Matériel 1677 : B.C. tacet. }
sol8 la |
sib4 sol re' sol |
re re'8 do' si2 |
do' fad |
sol2. sol4 |
la2. fa4 |
sib2. sib,4 |
mib2 fa4 fa, |
sib,2. sib4 |
la2 sol4 do' |
fa2. fa4 |
sol2. sol4 |
la2. fa4 |
sib sol la la, |
re2. re4 |
sol sol2 sol4 |
do2 do'4. sib8 |
la4. la8 re'4. sol8 |
fad4. mi8 re4 sol |
mib do re re, |
sol,2. sol4 |
do4. re8 mib4 do |
re2. sib,4 |
mib do re re, |
sol,2. sol4 |
do4. re8 mib4 do |
re2. sib,4 |
mib do re re, |
sol,2.
