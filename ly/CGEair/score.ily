\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "dessus" >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff << \global \includeNotes "taille" >>
    \new Staff << \global \includeNotes "quinte" >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s4 s1*7 s2.\break s4 s1*7 s2 \bar "" \pageBreak s2 s1*7\break }
    >>
  >>
  \layout { }
  \midi { }
}
