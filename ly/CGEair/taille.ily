\clef "taille" sol'4 |
sol' sol' fad' sol' |
la' la' sol' fa' |
mib'2 re'4. re'8 |
re'4 sib sol sol' |
fa'2. fa'8 mib' |
re'2. fa'4 |
mib' sol' fa'4. mib'8 |
re'2. fa'4 |
fa' fa' fa' mi' |
fa'2. fa'4 |
sib'4. la'8 sol'4. fa'8 |
mi'4. dod'8 dod'4 re' |
re' re' dod'4.\trill re'8 |
re'2. fad'8 fad' |
sol'4 sol'2 sol'8 sol' |
sol'4 mib' sol'4. sol'8 |
la'4. la'8 la'4. sib'8 |
la'4. sol'8 fad'4 sol' |
sol' sol' fad'4.\trill sol'8 |
sol'2. sol'8 sol' |
sol'4. fa'8 mib'4. mib'8 |
re'2. re'4 |
mib' mib' re' la |
sib2. sol'8 sol' |
sol'4. fa'8 mib'4. mib'8 |
re'2. re'4 |
mib' mib' re' la |
sib2.
