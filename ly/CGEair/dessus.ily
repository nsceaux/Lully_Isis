\clef "dessus" sol''4 |
re'' sib'' la''4. sib''8 |
\once\slurDashed fad''4.(\trill mi''16 fad'') sol''4 re'' |
mib'' do'' re''8 do'' sib' la' |
sib'4 sol' mib''4. mib''8 |
mib''4 do'' fa''4. fa''8 |
fa''2. re''4 |
sol'' do'' do''4.\trill sib'8 |
sib'2. re''8 mi'' |
fa''4. fa''8 \once\tieDashed sol''4\trill~ \once\slurDashed sol''8( fa''16 sol'') |
la''4 fa''2 la''8 la'' |
sol''4.\trill fa''8 mi''4.\trill re''8 |
dod''4.\trill la''8 mi''4 fa'' |
re''4. mi''8 mi''4.\trill re''8 |
re''2. la''8 la'' |
sib''4 sol''2 re''8 re'' |
mib''4 do'' mi''4. mi''8 |
fa''4. fa''8 fad''4.\trill sol''8 |
la''2. sib''4 |
sol''4. la''8 la''4.\trill sol''8 |
sol''2. re''8 re'' |
mib''4. re''8 do''4.\trill sib'8 |
la'2.\trill re''4 |
sol'4. la'8 la'4.\trill sol'8 |
sol'2. re''8 re'' |
mib''4. re''8 do''4.\trill sib'8 |
la'2.\trill re''4 |
sol'4. la'8 la'4.\trill sol'8 |
sol'2.
