\clef "haute-contre" re''4 |
re'' re'' re''8 do'' sib'4 |
la' re'' re''2 |
sol' la'4. la'8 |
sol'4 sol' << \sugNotes { do''4. do''8  } \\ { sib'4. sib'8 } >> |
do''4 la'2 la'4 |
sib'2. sib'4 |
sib' sib' la'4.\trill sib'8 |
sib'2. fa'8 sol' |
la'4 la' sib' do'' |
do'' la'2 do''8 do'' |
sib'2. sib'4 |
la'2. la'4 |
sib' sib' la'4. sol'8 |
fad'2.\trill re''8 re'' |
re''4 sib'2 si'8 si' |
do''2 do''4. do''8 |
do''4. do''8 la'4. << la'8 \\ \sug sol' >> |
re''2. re''4 |
mib'' mib'' re''4. do''8 |
si'2. sib'8 sib' sib'4. sib'8 la'4. sol'8 |
fad'2.\trill sol'4 |
sol' sol' fad'4.\trill sol'8 |
sol'2. sib'8 sib' |
sib'4. sib'8 la'4. sol'8 |
fad'2.\trill sol'4 |
sol' sol' fad'4.\trill sol'8 |
sol'2.
