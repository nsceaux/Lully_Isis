\clef "quinte" re' re'4. re'8 |
re'2~ re'8 re' |
sol4 sib4. la8 |
la2\trill r4 |
R2.*4 |
r4 r r8 la |
sol4 sol r |
r r r8 sol |
fa4 fa r |
R2.*5 |
r4 sib4. sib8 |
re'2~ re'8 re' |
do'4 r r |
R2. |
r4 la4. la8 |
sib4 fa'4. mi'8 |
mi'4\trill r r |
R2. |
r4 la4. la8 |
sol4 la re'8 re' |
re'4 r r |
R2.*5 |
r4 sib4. do'8 |
re'4 re'4. re'8 |
do'4 r r |
R2. |
r4 la4. la8 |
sol4 la re'8 re' |
re'4 sib4. do'8 |
re'4 re'4. re'8 |
re'2. |
