\clef "basse" sol,4^\markup\whiteout [Tous] sol4. sol8 |
re'4 re4. re8 |
mib4 mib4. do8 |
re2 re'8.^\markup\whiteout [à 3.] re'16 |
sol4 do'2 |
fa4 sib2 |
sol4 la2 |
re4 la,2 |
re2~ re8 re^\markup\whiteout [Tous] |
sol2~ sol8 sol^\markup\whiteout [à 3.] |
do4 do4. do8^\markup\whiteout [Tous] |
fa2~ fa8 fa^\markup\whiteout [à 3.] |
sib,4 sib4. sib8 |
la4 sib sib, |
mib2 do4 |
fa sib,2 |
fa,2. |
sib,4 sib4.^\markup\whiteout [Tous] sib8 |
si2~ si8 si |
do'4 do'4.^\markup\whiteout [à 3.] do'8 |
fa4 do2 |
fa4 fa4.^\markup\whiteout [Tous] fa8 |
sib4 sib4. sol8 |
la4 la4.^\markup\whiteout [à 3.] la8 |
re4 la,2 |
re4 re4.^\markup\whiteout [Tous] re8 |
sol,4 re,2 |
sol,4 sol4.^\markup\whiteout [à 3.] sol8 |
re4 sol sol, |
do fa fa, |
sib, mib2 |
do4 re2 |
sol,4 re,2 |
sol,4 sol4.^\markup\whiteout [Tous] la8 |
sib4 si4. si8 |
do'4 do4.^\markup\whiteout [à 3.] re8 |
mib2 do4 |
re4 re4.^\markup\whiteout [Tous] re8 |
sol,4 re,2 |
sol,4 sol4. la8 |
sib4 si4. si8 |
<<
  \tag #'basse sol,2.
  \tag #'basse-continue { sol,2.*11/12~ \hideNotes sol,16 }
>>

