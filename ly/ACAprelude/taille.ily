\clef "taille" sol'4 sol'4. sol'8 |
fa'4 la'4. la'8 |
sol'4 mib'4. mib'8 |
re'2 r4 |
R2.*4 |
r4 r r8 re' |
re'4 re' r |
r r r8 do' |
do'4 do' r |
R2.*5 |
r4 fa'4. fa'8 |
sol'2~ sol'8 sol' |
sol'4 r r |
R2. |
r4 fa'4. fa'8 |
fa'4 fa'4. sol'8 |
mi'4\trill r r |
R2. |
r4 re'4. re'8 |
re'2 la8 la |
sib4 r r |
R2.*5 |
r4 sol'2 |
sol'~ sol'8 sol' |
sol'4 r r |
R2. |
r4 re'4. re'8 |
re'2 la4 |
sib sol'2 |
sol'~ sol'8 sol' |
sib2. |
