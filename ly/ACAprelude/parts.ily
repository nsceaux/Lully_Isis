\piecePartSpecs
#`((dessus #:score-template "score-dessus2")
   (haute-contre)
   (taille)
   (quinte)
   (basse)
   (basse-continue)
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
