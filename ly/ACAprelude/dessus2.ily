\clef "dessus" R2.*3 |
r4 r fad''8. fad''16 |
sol''8. mi''16 mi''4.(\trill re''16 mi'') |
fa''8. re''16 re''4.(\trill dod''16 re'') |
mi''8. dod''16 dod''4.(\trill si'16 dod'') |
re''8. mi''16 dod''4.\trill re''8 |
re''2 r4 |
r4 r r8 si'8 |
do''4 do''4 r |
r4 r r8 la'8 |
sib'4 re''4. re''8 |
do''4 re''4. re''8 |
re''4 do''4.\trill sib'8 |
la'4 sib'4. sib'8 |
sib'4. do''8 la'4\trill |
sib'4 r4 r |
R2. |
r4 mi''4.\trill mi''8 |
<<
  \new CueVoice {
    s8^\markup\croche-pointee-double
  }
  { fa'' sol'' }
>> mi''4.\trill fa''8 |
fa''4 r4 r |
R2. |
r4 dod''4. dod''8 |
<<
  \new CueVoice {
    s8^\markup\croche-pointee-double
  }
  { re'' mi'' }
>> dod''4.\trill re''8 |
re''4 r4 r |
R2. |
r4 sib'4. do''8 |
la'8. si'16 si'4.(\trill la'16 si') |
do''8. la'16 la'4.(\trill sol'16 la') |
sib'8. sol'16 sol'4.(\trill fad'16 sol') |
la'8. fad'?16 fad'!4.(\trill mi'16 fad') |
sol'8. la'16 fad'4.\trill sol'8 |
sol'4 r4 r |
R2. |
r4 mib''4. re''8 |
do''4\trill do''4. re''8 |
la'4\trill r r |
R2.*3 |
sol'2. |
