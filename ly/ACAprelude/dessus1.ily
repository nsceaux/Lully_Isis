\clef "dessus" <>^"[Tous]" sib''4 re''4. mi''8 |
fa''4 fad''4.\trill fad''8 |
sol''4 sol''4. la''8 |
fad''2\trill <>^"[à 3.]" la''8. la''16 |
sib''8. sol''16 sol''4.(\trill fa''16 sol'') |
la''8. fa''16 fa''4.(\trill mi''16 fa'') |
sol''8. mi''16 mi''4.(\trill re''16 mi'') |
fa''8. sol''16 mi''4.\trill re''8 |
re''2 r8 re''^"[Tous]" |
si'4\trill si'4. re''8^"[à 3.]" |
mib''4 mib''4. do''8^"[Tous]" |
la'4\trill la'4. do''8^"[à 3.]" |
re''4 fa''4. fa''8 |
fa''4 fa''4. fa''8 |
sol''4 mib''4. mib''8 |
mib''?4 re''4. mib''!8 |
do''4.\trill do''8 re'' mib'' |
re''4\trill sib''4.^"[Tous]" la''8 |
sol''4 fa''4. sol''8 |
mi''4\trill sol''4.^"[à 3.]" sol''8 |
<<
  \new CueVoice {
    s8^\markup\croche-pointee-double
  }
  { la''8 sib'' }
>> sol''4.\trill fa''8 |
fa''4 la''4.^"[Tous]" la''8 |
re''4 re''4. mi''8 |
dod''4\trill mi''4.^"[à 3.]" mi''8 |
<<
  \new CueVoice {
    s8^\markup\croche-pointee-double
  }
  { fa'' sol'' }
>> mi''4. la''8 |
fad''4\trill la''4.^"[Tous]" la''8 |
sib''4 la''4.\trill sol''8 |
sol''4 re''4.^"[à 3.]" mi''8 |
fa''8. re''16 re''4.(\trill do''16 re'') |
mib''8. do''16 do''4.(\trill sib'16 do'') |
re''8. sib'16 sib'4.(\trill la'16 sib') |
do''8. la'16 la'4.(\trill sol'16 la') |
sib'8. do''16 la'4.\trill sol'8 |
sol'4 sib''4.^"[Tous]" la''8 |
sol''4 fa'' sol''8 re'' |
mib''4 do'''4.^"[à 3.]" sib''8 |
la''4 la''4. sib''8 |
fad''4\trill re''4.^"[Tous]" la'8 |
sib'8. do''16 la'4.\trill sol'8 |
sol'4 sib''4. la''8 |
sol''4 fa'' sol''8 re'' |
sol'2. |
