\clef "haute-contre" re''4 sib'4. do''8 |
la'4 la'4. re''8 |
sib'4 sib'4. do''8 |
la'2\trill r4 |
R2.*4 |
r4 r r8 fad' |
sol'4 sol' r |
r r r8 mi' |
fa'4 fa' r |
R2.*5 |
r4 re''4. re''8 |
re''4 re''4. re''8 |
sol'4 r r |
R2. |
r4 do''4. do''8 |
sib'4 sib'4. sib'8 |
la'4\trill r r |
R2. |
r4 fad'4. fad'8 |
sol'4 fad'4.\trill sol'8 |
sol'4 r r |
R2.*5 |
r4 re''4. do''8 |
sib' do'' re''4. re''8 |
sol'4 r r |
R2. |
r4 fad'4. fad'8 |
sol'8. la'16 fad'4.\trill sol'8 |
sol'4 re''4. do''8 |
sib' do'' re''4. re''8 |
sol'2. |
