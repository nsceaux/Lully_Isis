\key fa \major
\digitTime\time 2/2 \midiTempo#120 \partial 4 s4 s1*24
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.*2
\time 4/4 s1
\digitTime\time 3/4 s2.*2
\time 4/4 s1*5
\digitTime\time 3/4 s2.*2
\digitTime\time 2/2 \midiTempo#120 s1*19
\digitTime\time 3/4 s2.*15 \bar "|."
