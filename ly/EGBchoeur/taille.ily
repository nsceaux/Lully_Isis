\clef "taille" r4 |
R1*25 R2. R1 R2.*2 R1 R2.*2 R1*5 R2.*2 R1*19 R2. |
r4 sol' sol' |
fa' fa' la' |
sol'2 sol'4 |
la' la' la' |
la' sol'2 |
mi'4 sol' la' |
fa' fa' sol' |
sol'2 la'4 |
la' re' re' |
re' do'4. sib8 |
la2 la4 |
la re' re' |
re' do'4. sib8 |
la2. |
