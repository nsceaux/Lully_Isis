\tag #'(voix1 voix2 voix3 basse) {
  Le fil de la vi -- e,
  le fil de la vi -- e
  de tous les hu -- mains,
  sui -- vant nôtre en -- vi -- e,
  tour -- ne dans nos mains ;
  \tag #'(voix1 voix2 basse) {
    le fil de la vi -- e
    de tous les hu -- mains,
    le fil de la vi -- e
  }
  \tag #'voix3 {
    tour -- ne dans nos mains,
    sui -- vant nôtre en -- vi -- e,
  }
  tour -- ne, tour -- ne dans nos mains,
  sui -- vant nôtre en -- vi -- e,
  tour -- ne dans nos mains.
}
\tag #'(recit basse) {
  Tran -- chez mon tris -- te sort d’un coup qui me dé -- li -- vre
  des tour -- mens que Ju -- non me con -- traint à souf -- frir ;
  cha -- cun vous fait de vœux pour vi -- vre,
  et je vous en fais pour mou -- rir.
  
  Ju -- pi -- ter l’a soû -- mise aux loix de son é -- pou -- se ;
  elle a ren -- du Ju -- non ja -- lou -- se ;
  l’a -- mour d’un Dieu puis -- sant a trop sçû la char -- mer.
  Elle est trop peu pu -- nie en -- co -- re.

  Est-ce un si grand cri -- me d’ai -- mer
  ce que tout l’u -- ni -- vers a -- do -- re ?
}
\tag #'(voix1 voix2 voix3 basse) {
  Nymphe, ap -- pai -- se Ju -- non, si tu veux voir la fin
  de ton sort dé -- plo -- ra -- ble ;
  \tag #'voix2 { c’est l’Ar -- rêt, }
  c’est l’Ar -- rêt du Des -- tin,
  il est ir -- re -- vo -- ca -- ble ;
  c’est l’Ar -- rêt du Des -- tin,
  il est ir -- re -- vo -- ca -- ble.
}
\tag #'(recit basse) {
  He -- las ! com -- ment flé -- chir u -- ne haine im -- pla -- ca -- ble ?
}

C’est l’Ar -- rêt du Des -- tin,
il est ir -- re -- vo -- ca -- ble ;
c’est l’Ar -- rêt du Des -- tin,
il est ir -- re -- vo -- ca -- ble,
il est ir -- re -- vo -- ca -- ble.

