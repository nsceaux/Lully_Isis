\clef "petrucci-c3/bass" \tag #'basse <>^"[B.C.]" fa4 |
do'2 do'4. do'8 |
sol2 sol4. sol8 |
re'2 re'4. re'8 |
sib2 sib4. sib8 |
si2 si4. si8 |
do'2. do'4 |
la2 la4 la |
sib sib sib4. la8 |
sol2. sol4 |
fa1 |
do'4. sib8 do'4 sib8 la |
re'4. do'8 re'4 do'8 sib |
mib'4. re'8 mib'4 re'8 do' |
re'4 re' re' re |
sol2. sol4 |
do'2 do'4 sib |
la2 la |
re'4. mib'8 re'4. do'8 |
\clef "bass" sib2 sib |
do' fa |
do2. la4 |
sib2 sol |
do' fa |
do1 |
\once\tieDashed fa~ |
fa4 re4. do8 |
sib,1 | \allowPageTurn
si,2. |
do2 sib,4 |
la,4. sib,8 do4 do, |
fa,2. |
fa4 re2 |
mib2 fa |
sol~ sol4. fa8 |
mi4. fa8 sol4 sol, |
\once\tieDashed do2~ do4 sib,8 la, |
sol,1~ |
sol,2 \once\tieDashed re4~ |
re2 sol8 sol, |
do2. do4 |
\clef "petrucci-c3/bass" fa2. fa4 |
sib2 sib4 sib |
sol2. sol4 |
do'4 do' la4. la8 |
re'2 re4 re |
\once\tieDashed sol2~ sol |
<< sol1 \\ \sug sol, >> |
do2 do'4 do' |
la2 la4 la |
re'2 r4 sib |
mib' do' re' re |
sol sol sol sol |
do'2 do'4 do' |
do'2 r4 la |
re' sib do' do |
fa1~ |
fa |
\clef "bass" mi2 re4 sol |
\once\tieDashed do2.~ |
do4 \tag #'basse <>^\markup [tous] do do |
fa fa re |
sol2 mi4 |
la la la |
fa sol2 |
do4 do' la |
sib sib sol |
do'2 la4 |
re' re' re' |
sib4 do'2 |
fa2 fa4 |
re re re |
sib, do2 |
fa,2. |
