\piecePartSpecs
#`((dessus #:score-template "score-voix")
   (haute-contre #:score-template "score-voix")
   (taille #:score-template "score-voix")
   (quinte #:score-template "score-voix")
   (basse #:score-template "score-voix"})
   (basse-continue #:score-template "score-voix" #:tag-notes basse))
