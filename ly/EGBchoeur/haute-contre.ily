\clef "haute-contre" r4 |
R1*25 R2. R1 R2.*2 R1 R2.*2 R1*5 R2.*2 R1*19 R2. |
r4 do'' do'' |
la' la' re'' |
si'2 si'4 |
do'' do'' do'' |
<< { do'' do''( si') } \\ \sugNotes { do''4 si'2 } >> |
do''4 do'' do'' |
sib' sib' sib' |
do''2 do''4 |
la' la' la' |
sib' sol'2 |
fa' fa'4 |
fa' fa' fa' |
sol' mi'2 |
fa'2. |
