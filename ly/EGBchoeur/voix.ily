<<
  \tag #'(voix1 basse) {
    \clef "vbas-dessus" <>^\markup\character Les Trois Parques la'4 |
    sol'2\trill sol'4. la'8 |
    sib'2 sib'4. sib'8 |
    la'2\trill la'4. la'8 |
    re''2 re''4. re''8 |
    re''2 re''4. re''8 |
    sol'2. sol'4 |
    do''2 do''4 do'' |
    fa' fa' sol'4. la'8 |
    sib'2.( la'8) sib' |
    la'2.\trill <>^\markup\italic { [Un peu plus viste] } la'4 |
    sol'2\trill sol'4 do'' |
    la'2\trill la'4 re'' |
    sib'2\trill sib'4 do'' |
    la'2.\trill la'4 |
    sib'2 sib'4 sib' |
    sol'2\trill sol' |
    do''4.(\melisma re''8) do''4. sib'8( |
    la'2)\melismaEnd la' |
    re''4.(\melisma do''8) sib'4. la'8( |
    sol'4.)\melismaEnd sol'8 la'4. sib'8 |
    sol'2.\trill do''4 |
    do''2 sib'4 sib' |
    sib' sib' la'4. sib'8 |
    la'2( sol'4.)\trill fa'8 |
    <<
      \tag #'basse { fa'4 s2. s2. s1 s2.*2 s1 s2.*2 s1*5 s2.*2 s1 \ffclef "vbas-dessus" }
      \tag #'voix1 {
        fa'2 r2 | R2. R1 R2.*2 R1 R2.*2 R1*5 R2.*2 R1
      }
    >> <>^\markup\character Les Parques
    do''2. do''4 |
    re''2 re''4 re'' |
    re''2. sib'4 |
    sol'\trill sol' do''4. do''8 |
    la'2\trill la'4 re'' |
    si'2\trill do''8[ si'] do''4 |
    do''2( si')\trill |
    do''2 sol'4 sol' |
    do''2 do''4 do'' |
    la'2\trill r4 re'' |
    \footnoteHere #'(0 . 0) \markup { Ballard 1719 : \italic sol }
    \sug sib' do'' la'4(\trill sol'8) la' |
    sib'4 sib' sib' sib' |
    sib'2 la'4 sib' |
    sol'2\trill r4 do'' |
    la' re'' sol' do'' |
    la'2\trill la'4
    \tag #'voix1 { r4 R1*2 R2.*15 }
  }
  \tag #'voix2 {
    \clef "vpetite-haute-contre" fa'4 |
    fa'2 mib'4( re'8) mib' |
    re'2\trill re'4. mi'8 |
    fa'2 fa'4. fa'8 |
    fa'2 fa'4. fa'8 |
    fa'2 fa'4. sol'8 |
    mi'2.\trill mi'4 |
    fa'2 mib'4( re'8) mib' |
    re'4\trill re' mi' fa' |
    fa'2. mi'4 |
    fa'2. fa'4 |
    mi'2 mi'4 la' |
    fa'2 fa'4 sib' |
    sol'2 sol'4 la' |
    fad'2.\trill fad'4 |
    sol'2 sol'4 sol' |
    mi'2\trill mi' |
    la'4.(\melisma sib'8) la'4. sol'8( |
    fa'2)\melismaEnd fa' |
    sib'4.(\melisma la'8) sol'4. fa'8( |
    mi'4.)\melismaEnd mi'8 fa'4. sol'8 |
    mi'2.\trill fa'4 |
    re'2 re'4 sol' |
    mi'\trill mi' fa'8[ mi'] fa'4 |
    fa'2( mi'4.)\trill fa'8 |
    fa'2 r2 |
    R2. R1 R2.*2 R1 R2.*2 R1*5 R2.*2 R1 |
    la'2. la'4 |
    fa'2\trill fa'4 fa' |
    sib'2. sol'4 |
    mi'\trill mi' la'4. la'8 |
    fa'2 fa'4 fa' |
    fa'2 mib'8[ re'] mib'4 |
    re'\trill re' sol' sol' |
    mi'2\trill mi'4 mi' |
    la'2 la'4 la' |
    fa'2 r4 sib' |
    sol'\trill sol' sol' fad' |
    sol' sol' re' re' |
    mi'2 fa'4 sol' |
    mi'2\trill r4 la' |
    fa' fa' fa' mi' |
    fa'2 fa'4 r |
    R1*2 R2.*15
  }
  \tag #'voix3 {
    \clef "petrucci-c3/bass"
    \footnoteHere #'(0 . 0) \markup {
      Matériel 1677 : \concat { 3 \super e } Parque dans la partie séparée de basse.
    }
    fa4 |
    do'2 do'4. do'8 |
    sol2 sol4. sol8 |
    re'2 re'4. re'8 |
    sib2 sib4. sib8 |
    si2\trill si4. si8 |
    do'2. do'4 |
    la2\trill la4 la |
    sib sib sib4. la8 |
    sol2.\trill sol4 |
    fa1 |
    do'4.(\melisma sib8) do'4 sib8[ la] |
    re'4. do'8 re'4 do'8[ sib] |
    mib'4. re'8 mib'4 re'8[ do']( |
    re'4)\melismaEnd re' re' re |
    sol2. sol4 |
    do'2 do'4 sib |
    la2\trill la |
    re'4.(\melisma mib'8) re'4.( do'8)( |
    sib2)\melismaEnd sib |
    do'4. do'8 fa4. fa8 |
    do2. la4 |
    sib2 sol4\trill sol |
    do' do' fa4. fa8 |
    do2~ do4. fa8 |
    fa2 r2 |
    R2. R1 R2.*2 R1 R2.*2 R1*5 R2.*2 R1 |
    fa2. fa4 |
    sib2 sib4 sib |
    sol2.\trill sol4 |
    do' do' la4.\trill la8 |
    re'2 re4 re |
    sol2 sol4 sol |
    sol1 |
    do2 do'4 do' |
    la2 la4 la |
    re'2 r4 sib |
    mib' do' re' re |
    sol sol sol sol |
    do'2 do'4 do' |
    do'2 r4 la |
    re' sib do' do |
    fa2 fa4 r |
    R1*2 R2.*15
  }
  \tag #'(recit basse) {
    <<
      \tag #'basse { s4 s1*24 s4 \ffclef "vbas-dessus" }
      \tag #'recit { \clef "vbas-dessus" r4 R1*24 r4 }
    >> <>^\markup\character Io
    r8 fa'' do''4 do''8 do''16 do'' |
    la'8.\trill fa'16 sib'8 sib' sib' do'' |
    re'' re'' re'' fa'' sib'4 sib'8 <<
      \new Voice { \voiceOne \sug re'' }
      { \voiceTwo sib' \oneVoice }
    >> |
    sol'4\trill sol'8 fa' fa'8\trill fa'16 mi' |
    mi'8.\trill sol'16 do'' do'' do'' do'' re''8 mi'' |
    fa''8 fa''16 r do'' do'' do'' re'' sol'4\trill la'8 sib' |
    la'4\trill
    \ffclef "vhaute-contre" <>^\markup\character La Furie
    r8 do'16 do' fa'8 do'16 do' |
    la8\trill la sib sib sib sib |
    sol\trill sol r16 mib' mib' mib' mib'8 re' re' re' |
    si4\trill si8 re' re' re' mi'8.\trill fa'16 |
    sol'4 sol'8 la' re'4\trill mi'8 fa' |
    mi'4\trill r16 do' do' do' sol8\trill sol sol la |
    sib4 sib
    \ffclef "vbas-dessus" <>^\markup\character Io
    re''8 re'' re'' re'' |
    sib'4\trill sib'8 la' la'4\trill |
    re''8. re''16 re''8 mi''16 fa'' si'8.\trill do''16 |
    do''2 do''4 r |
    <<
      \tag #'basse { s1*15 s2. \ffclef "vbas-dessus" }
      \tag #'recit { R1*15 r2 r4 }
    >> <>^\markup\character Io
    fa''4 |
    la'4\trill r8 la' sib'4. do''8 |
    sol'4\trill do''8 do'' re''4 mi''8 fa'' |
    mi''2\trill mi''4 |
    R2.*14 |
  }
  \tag #'vdessus {
    \clef "vdessus" r4 |
    R1*25 R2. R1 R2.*2 R1 R2.*2 R1*5 R2.*2 R1*19 R2. |
    <>^\markup\character\line {
      \smallCaps { Les Parques, la Furie, } et le chœur de la suite des Parques
    }
    r4 mi'' mi'' |
    do'' do'' fa'' |
    re''2\trill sol''4 |
    mi''\trill mi'' mi'' |
    fa'' re''2\trill |
    do''4 mi'' fa'' |
    re''\trill re'' \footnoteHere #'(0 . 0) \markup { Ballard 1719 : \italic fa }
    \sug sol'' |
    mi''2\trill mi''4 |
    fa'' fa'' fa''8[ mi''] |
    fa''4 fa''( mi'')\trill |
    fa''2 do''4 |
    re'' la' la' |
    sib' sol'2\trill |
    fa'2. |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" r4 |
    R1*25 R2. R1 R2.*2 R1 R2.*2 R1*5 R2.*2 R1*19 R2. |
    r4 sol' sol' |
    fa' fa' la' |
    sol'2 sol'4 |
    la' la' la' |
    la' sol'2 |
    mi'4\trill sol' la' |
    fa' fa' sib' |
    sol'2 la'4 |
    la' la' la' |
    sol'4 sol'2\trill |
    fa' fa'4 |
    fa' fa' fa' |
    sol' mi'2\trill |
    fa'2. |
  }
  \tag #'vtaille {
    \clef "vtaille" r4 |
    R1*25 R2. R1 R2.*2 R1 R2.*2 R1*5 R2.*2 R1*19 R2. |
    r4 do' do' |
    la la re' |
    si2 si4 |
    do' do' do' |
    do' do'( si) |
    do' do' do' |
    sib re' re' |
    mi'2 do'4 |
    re' re' re' |
    re' do'2 |
    la la4 |
    la re' re' |
    re' do'2 |
    la2. |
  }
  \tag #'vbasse {
    \clef "vbasse" r4 |
    R1*25 R2. R1 R2.*2 R1 R2.*2 R1*5 R2.*2 R1*19 R2. |
    r4 do do |
    fa fa re |
    sol2 mi4 |
    la la la |
    fa sol2 |
    do4 do' la |
    sib sib sol |
    do'2 la4 |
    re' re' re' |
    sib do'2 |
    fa fa4 |
    re re re |
    sib, do2 |
    fa,2. |
  }
>>
