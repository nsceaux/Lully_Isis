\clef "quinte" r4 |
R1*25 R2. R1 R2.*2 R1 R2.*2 R1*5 R2.*2 R1*19 R2. |
r4 do' do' |
do' do' re' |
re'2 mi'4 |
mi' do' do' |
re'2 sol4 |
sol do' do' |
re' re' << re' \\ \sug sib >> |
do'2 do'4 |
re' fa' fa' |
sol'2 do'4 |
do'2 do'4 |
fa fa la |
sol2 do'4 |
do'2. |
