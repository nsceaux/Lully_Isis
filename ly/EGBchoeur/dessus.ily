\clef "dessus" r4 |
R1*25 R2. R1 R2.*2 R1 R2.*2 R1*5 R2.*2 R1*19 R2. |
r4 mi'' mi'' |
do'' do'' fa'' |
re''2\trill sol''4 |
mi''\trill mi'' mi'' |
fa'' re''4.\trill( do''8) |
do''4 mi'' fa'' |
re'' re'' sol'' |
mi''2\trill mi''4 |
fa'' fa'' fa'' |
fa'' mi''4.\trill fa''8 |
fa''2 do''4 |
re'' la' la' |
sib' sol'4.\trill fa'8 |
fa'2. |
