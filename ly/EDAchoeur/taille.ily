\clef "taille" R1*2 R4.*4 R1*2 R2. R4.*4 R1*2 R2.*2 R1 R2.*2 R1
R2. R1*2 R2. R1*2 R2. R1 R2. R1 R4.*7 |
r8 re' re' |
re' r r |
r do' do' |
do' r r |
r re' re' |
re' r r |
R4.*2 |
r8 do' re' |
mi' mi' mi' |
do' do' re' |
re' re' re' |
do' r r4 r2 |
R2. R1*3 R4.*6 |
r8 re' re' |
re' r r |
r do' do' |
do' r r |
r re' re' |
re' r r |
R4.*2 |
r8 do' re' |
mi' mi' mi' |
do' do' re' |
re' re' re' |
do'4. |
