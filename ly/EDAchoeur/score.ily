\score {
  <<
    \new StaffGroup \with { \haraKiriFirst } <<
      \new Staff <<
        { s1*2 s4.*4 s1 s1 s2. s4.*4 s1*2 s2.*2 s1 s2.*2 s1 s2. s1*2
          s2. s1*2 s2. s1 s2. s1 s4. s4 \noHaraKiri s8 s4.*17 s1 \revertNoHaraKiri
          s2. s1*3 s8 \noHaraKiri }
        \global \includeNotes "dessus"
      >>
      \new Staff <<
        { s1*2 s4.*4 s1 s1 s2. s4.*4 s1*2 s2.*2 s1 s2.*2 s1 s2. s1*2
          s2. s1*2 s2. s1 s2. s1 s4. s4 \noHaraKiri s8 s4.*17 s1 \revertNoHaraKiri
          s2. s1*3 s8 \noHaraKiri }
        \global \includeNotes "haute-contre"
      >>
      \new Staff <<
        { s1*2 s4.*4 s1 s1 s2. s4.*4 s1*2 s2.*2 s1 s2.*2 s1 s2. s1*2
          s2. s1*2 s2. s1 s2. s1 s4. s4 \noHaraKiri s8 s4.*17 s1 \revertNoHaraKiri
          s2. s1*3 s8 \noHaraKiri }
        \global \includeNotes "taille"
      >>
      \new Staff <<
        { s1*2 s4.*4 s1 s1 s2. s4.*4 s1*2 s2.*2 s1 s2.*2 s1 s2. s1*2
          s2. s1*2 s2. s1 s2. s1 s4. s4 \noHaraKiri s8 s4.*17 s1 \revertNoHaraKiri
          s2. s1*3 s8 \noHaraKiri }
        \global \includeNotes "quinte"
      >>
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        { s1*2 s4.*4 s1 s1 s2. s4.*4 s1*2 s2.*2 s1 s2.*2 s1 s2. s1*2
          s2. s1*2 s2. s1 s2. s1 s4. s4 \noHaraKiri s8 s4.*17 s1 \revertNoHaraKiri
          s2. s1*3 s8 \noHaraKiri }
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s1*2 s4.*4 s1 s1 s2. s4.*4 s1*2 s2.*2 s1 s2.*2 s1 s2. s1*2
          s2. s1*2 s2. s1 s2. s1 s4. s4 \noHaraKiri s8 s4.*17 s1 \revertNoHaraKiri
          s2. s1*3 s8 \noHaraKiri }
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s1*2 s4.*4 s1 s1 s2. s4.*4 s1*2 s2.*2 s1 s2.*2 s1 s2. s1*2
          s2. s1*2 s2. s1 s2. s1 s4. s4 \noHaraKiri s8 s4.*17 s1 \revertNoHaraKiri
          s2. s1*3 s8 \noHaraKiri }
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s1*2 s4.*4 s1 s1 s2. s4.*4 s1*2 s2.*2 s1 s2.*2 s1 s2. s1*2
          s2. s1*2 s2. s1 s2. s1 s4. s4 \noHaraKiri s8 s4.*17 s1 \revertNoHaraKiri
          s2. s1*3 s8 \noHaraKiri }
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        \global \keepWithTag #'recit \includeNotes "voix"
      >> \keepWithTag #'recit \includeLyrics "paroles"
      \new Staff <<
        { s1*2 s4.*4 s1*2 s2. s4.*4 s1*2 s2.*2 s1 s2.*2 s1 s2. s1*2 s2.
          s1*2 s2. s1 s2. s1 s4.*18 s4 s16
          \once\override TextScript.self-alignment-X = #RIGHT
          <>^\markup\italic\smaller {
            Les feux des Forges redoublent & les Chalybes environnent Io
            avec des morceaux d’acier & brûlants.
          }
        }
        \global \keepWithTag #'basse-continue \includeNotes "basse"
        \includeFigures "chiffres"
        \origLayout {
          s1*2 s4.*3\break s4. s1*2 s2. s4.\pageBreak
          s4.*3 s1*2\break s2.*2 s1\break s2.*2 s1 s2.\break s1*2 s2.\pageBreak
          s1*2 s4 \bar "" \break s2 s1 s2.\break s1 s4.\break s4.*6 s8 \bar "" \pageBreak
          s4 s4.*7 s8 \bar "" \break s4 s4.*3\pageBreak
          s1 s2. s1\break s1*2 s8 \bar "" \break s4 s4.*5\pageBreak
          s4.*8 s8 \bar "" \break
        }
        \modVersion {
          s1*2 s4. s4.\noBreak s4.\noBreak s4.\noBreak s1*2
          s2. s4. s4.\noBreak s4.\noBreak s4.\noBreak s1*2 s2.*2 s1 s2.*2 s1 s2. s1*2
          s2. s1*2 s2. s1 s2. s1 s4.\pageBreak s4.*18 s1 s2. s1*3\break
        }
      >>
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
