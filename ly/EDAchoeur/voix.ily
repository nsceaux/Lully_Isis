<<
  \tag #'(recit basse) {
    \clef "vbas-dessus"
    \tag #'basse <>^\markup\character Io
    \tag #'recit <>^\markup\character-text Io \line {
      au milieu des Feux qui sortent des Forges.
    }
    r4 mi''8 sol'' do''4 do''8 do'' |
    sol'4 sol'8 sol' sib'4 sib'8 la' |
    la'4\trill la'8
    <<
      \tag #'basse { s4.*3 s2 \ffclef "vbas-dessus" }
      \tag #'recit { R4.*3 r2 }
    >> <>^\markup\character Io
    re''2 |
    si'4
    \ffclef "vhaute-contre" <>^\markup\character La Furie
    -\tag #'recit ^\markup\italic\smaller {
      Les Chalybes passent auprés d’Io avec des morceaux d’épées, de lances,
      & de haches à demy forgées.
    }
    r8 sol re' re' mi' fa' |
    mi'\trill mi' do'16 do' do' re' mi'8 mi'16 fad' |
    sol'4. |
    <<
      \tag #'basse { s4.*3 s2 }
      \tag #'recit { R4.*3 r2 }
    >>
    \ffclef "vbas-dessus" <>^\markup\character Io
    r4 sol' |
    do'' do''8 do'' la'4\trill la'8 si' |
    sold'8\trill sold' r16 si' si' si' mi''8. mi''16 |
    dod''8 r16 fa'' re''8\trill re'' re'' dod''! |
    re''8. re''16
    \ffclef "vhaute-contre" <>^\markup\character La Furie
    r8 re' si16\trill si si si do'8 re' |
    mi' sol' mi'\trill mi' fa' sol' |
    la'8. fa'16 fa'8 la' re'8.\trill mi'16 |
    do'4
    \ffclef "vbas-dessus" <>^\markup\character Io
    r8 mi'' re''4\trill r8 si' |
    do''4 do''8 do''16 mi'' do''8\trill do'' |
    la'\trill la'16 r re''8 re'' sol' sol' sol' sol'16 la' |
    fad'4\trill fad'8 r16 la' la'8 la' si' do'' |
    re''8. mi''16 la'8\trill la' si' do'' |
    si'4\trill re''16 re'' re'' mi'' fa''4 fa''8. sol''16 |
    mi''4.\trill mi''8 fa'' si' si' si' |
    sold'8. do''16 re''8 mi'' si'\trill^\markup\croche-pointee-double do'' |
    la'8\trill la'16 r mi'' mi'' mi'' fa'' sol''4 do''8. mi''16 |
    la'8\trill r16 do'' fa''8 la' la' re'' |
    si'8.\trill do''16 re''8 mi'' fa''^\markup\croche-pointee-double mi'' re''4\trill |
    do''4. |
    <<
      \tag #'basse { s4.*18 r4 \ffclef "vbas-dessus" }
      \tag #'recit { R4.*18 r4 }
    >> <>^\markup\character Io
    r8 do''16 do'' do''8\trill do''16 do'' do''8 si' |
    si'8\trill si' r re'' si'8.\trill si'16 |
    mi''4 do''8 si' la'4\trill la'8 re'' |
    sol'
    \ffclef "vhaute-contre" <>^\markup\character La Furie
    re'8 sol' si do' do' do' mi' |
    la\trill la r do' fa' fa'16 fa' re'8\trill re'16 sol' |
    mi'8\trill
    \tag #'recit { r8 r | R4.*18 }
  }
  \tag #'(vdessus basse) {
    <<
      \tag #'basse { s1*2 s4. \ffclef "vdessus" }
      \tag #'vdessus { \clef "vdessus" R1*2 R4. }
    >> <>^\markup\character Tous
    r8 fa'' fa'' |
    re'' re'' re'' |
    re'' re'' dod'' |
    re''4 r <<
      \tag #'basse { s2 s1 s2. s4. \ffclef "vdessus" }
      \tag #'vdessus { r2 R1 R2. R4. }
    >> <>^\markup\character Tous
    r8 re'' sol'' |
    mi'' mi'' fa'' |
    re'' re'' sol'' |
    mi''4 r <<
      \tag #'basse {
        s2 s1 s2.*2 s1 s2.*2 s1 s2. s1*2 s2. s1*2 s2. s1 s2. s1 s4.
        \ffclef "vdessus"
      }
      \tag #'vdessus { r2 R1 R2.*2 R1 R2.*2 R1 R2. R1*2 R2. R1*2 R2. R1 R2. R1 R4. }
    >> <>^\markup\character Chœurs
    r8 mi'' mi'' |
    do''4 do''8 |
    fa'' fa'' fa'' |
    mi'' mi'' fa'' |
    re'' re'' re'' |
    re'' re'' dod'' |
    re'' r r |
    r re'' re'' |
    mi'' r r |
    r do'' do'' |
    fa'' r r |
    r re'' mi'' |
    do'' do'' fa'' |
    re'' re'' sol'' |
    mi'' r r |
    R4.*3 |
    <<
      \tag #'basse { s1 s2. s1*3 s8 \ffclef "vdessus" }
      \tag #'vdessus { R1 R2. R1*3 r8 }
    >> <>^\markup\character Tous
    mi''8 mi'' |
    do''4 do''8 |
    fa'' fa'' fa'' |
    mi'' mi'' fa'' |
    re'' re'' re'' |
    re'' re'' dod'' |
    re'' r r |
    r re'' re'' |
    mi'' r r |
    r do'' do'' |
    fa'' r r |
    r re'' mi'' |
    do'' do'' fa'' |
    re'' re'' sol'' |
    mi''\trill r r |
    R4.*4 |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" R1*2 R4. |
    r8 la' la' |
    fa' fa' sol' |
    mi' mi' la' |
    fad'4 r r2 |
    R1 R2. R4. |
    r8 sol' sol' |
    la' la' la' |
    sol' sol' sol' |
    sol'4 r r2 |
    R1 R2.*2 R1 R2.*2 R1 R2. R1*2 R2. R1*2 R2. R1 R2. R1 R4. |
    r8 sol' sol' |
    la'4 la'8 |
    la' la' la' |
    la' la' la' |
    fa' fa' sol' |
    mi' mi' la' |
    fad' r r |
    r sol' sol' |
    sol' r r |
    r la' la' |
    la' r r |
    r sol' sol' |
    mi' mi' la' |
    sol' sol' sol' |
    sol' r r |
    R4.*3 R1 R2. R1*3 |
    r8 sol' sol' |
    la'4 la'8 |
    la' la' la' |
    la' la' la' |
    fa' fa' sol' |
    mi' mi' la' |
    fad' r r |
    r sol' sol' |
    sol' r r |
    r la' la' |
    la' r r |
    r sol' sol' |
    mi' mi' la' |
    sol' sol' sol' |
    sol' r r |
    R4.*4 |
  }
  \tag #'vtaille {
    \clef "vtaille" R1*2 R4. |
    r8 do' do' |
    sib sib sib |
    la la la |
    la4 r r2 |
    R1 R2. R4. |
    r8 si si |
    do' do' do' |
    do' do' si |
    do'4 r r2 |
    R1 R2.*2 R1 R2.*2 R1 R2. R1*2 R2. R1*2 R2. R1 R2. R1 R4. |
    r8 do' do' |
    do'4 do'8 |
    re' re' re' |
    do' do' do' |
    sib sib sib |
    la la la |
    la r r |
    r si si |
    do' r r |
    r la la |
    re' r r |
    r si si |
    do' do' do' |
    do' do' si |
    do' r r |
    R4.*3 R1 R2. R1*3 |
    r8 do' do' |
    do'4 do'8 |
    re' re' re' |
    do' do' do' |
    sib sib sib |
    la la la |
    la r r |
    r si si |
    do' r r |
    r la la |
    re' r r |
    r si si |
    do' do' do' |
    do' do' si |
    do' r r |
    R4.*4 |
  }
  \tag #'vbasse {
    \clef "vbasse" R1*2 R4. |
    r8 fa fa |
    sib sib sol |
    la la la, |
    re4 r r2 |
    R1 R2. R4. |
    r8 sol mi |
    la la fa |
    sol sol sol, |
    do4 r r2 |
    R1 R2.*2 R1 R2.*2 R1 R2. R1*2 R2. R1*2 R2. R1 R2. R1 R4. |
    r8 do do |
    fa4 fa8 |
    re re re |
    la la fa |
    sib sib sol |
    la la la, |
    re r r |
    r sol sol |
    mi r r |
    r fa fa |
    re r r |
    r sol mi |
    la la fa |
    sol sol sol, |
    do r r |
    R4.*3 R1 R2. R1*3 |
    r8 do do |
    fa4 fa8 |
    re re re |
    la la fa |
    sib sib sol |
    la la la, |
    re r r |
    r sol sol |
    mi r r |
    r fa fa |
    re r r |
    r sol mi |
    la la fa |
    sol sol sol, |
    do r r |
    R4.*4 |
  }
>>