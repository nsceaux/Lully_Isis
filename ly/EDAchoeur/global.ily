\key do \major
\time 2/2 \midiTempo#160 s1*2
\time 3/8 \midiTempo#120 s4.*4
\digitTime\time 2/2 \midiTempo#160 s1
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.
\time 3/8 \midiTempo#120 s4.*4
\digitTime\time 2/2 \midiTempo#160 s1*2
\digitTime\time 3/4 \midiTempo#80 s2.*2
\time 4/4 s1
\digitTime\time 3/4 \midiTempo#80 s2.*2
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1
\time 3/8 \midiTempo#120 s4.*19
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*3
\time 3/8 \midiTempo#120 s4.*19 \bar "|."
