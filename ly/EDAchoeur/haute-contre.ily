\clef "haute-contre" R1*2 R4.*4 R1*2 R2. R4.*4 R1*2 R2.*2 R1 R2.*2 R1
R2. R1*2 R2. R1*2 R2. R1 R2. R1 R4.*7 |
r8 fad' fad' |
sol' r r |
r sol' sol' |
fa' r r |
r re'' re'' |
si' r r |
R4.*2 |
r8 sol' sol' |
la' la' sol' |
fa' la' re'' |
si' si' si' |
do'' r r4 r2 |
R2. R1*3 R4.*6 |
r8 fad'8 fad' |
sol' r r |
r sol' sol' |
fa' r r |
r re'' re'' |
si' r r |
R4.*2 |
r8 sol' sol' |
la' la' sol' |
fa' la' re'' |
si' si' si' |
do''4. |
