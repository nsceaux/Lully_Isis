\clef "dessus" R1*2 R4.*4 R1*2 R2. R4.*4 R1*2 R2.*2 R1 R2.*2 R1
R2. R1*2 R2. R1*2 R2. R1 R2. R1 R4.*7 |
r8 la' re'' |
si' r r |
r sol' do'' |
la' r r |
r fa'' fa'' |
re'' r r |
R4.*2 |
r8 mi'' sol'' |
do'' do'' mi'' |
la' do'' fa'' |
re'' sol'' re'' |
mi''8 r r4 r2 |
R2. R1*3 R4.*6 |
r8 la' re'' |
si' r r |
r sol' do'' |
la' r r |
r fa'' fa'' |
re'' r r |
R4.*2 |
r8 mi'' sol'' |
do'' do'' mi'' |
la' do'' fa'' |
re'' sol'' re'' |
mi''4.\trill |
