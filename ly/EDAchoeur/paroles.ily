\tag #'(recit basse) {
  Quel dé -- lu -- ge de feux vient sur moy se ré -- pan -- dre ?
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Tôt \ru#7 tôt tôt.
}
\tag #'(recit basse) {
  O Ciel !
  
  Le Ciel ne peut t’en -- ten -- dre,
  tu ne te plains pas as -- sez haut.
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Tôt \ru#7 tôt tôt.
}
\tag #'(recit basse) {
  Ju -- non se -- roit moins in -- hu -- mai -- ne,
  tu me fais trop souf -- frir, tu sers trop bien sa hai -- ne.
  
  Au gré de son dé -- pit ja -- loux,
  tes maux les plus cru -- els se -- ront en -- cor trop doux.
  
  He -- las, he -- las ! quel -- le ri -- gueur ex -- trê -- me !
  C’est en vain que Ju -- pi -- ter m’ai -- me,
  la hai -- ne de Ju -- non joü -- it de mon tour -- ment ;
  Que vous ha -- ïs -- sez for -- te -- ment,
  grands Dieux ! qu’il s’en faut bien que vous m’ai -- miez de mê -- me !
  Que vous ha -- ïs -- sez for -- te -- ment,
  grands Dieux ! qu’il s’en faut bien que vous m’ai -- miez de mê -- me !
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Qu’on pré -- pa -- re tout ce qu’il faut,
  \ru#8 tôt tôt.
  Tôt tôt tôt,
  Tôt tôt tôt,
  \ru#8 tôt tôt.
}
\tag #'(recit basse) {
  Ne pour -- ray- je ces -- ser de vi -- vre ?
  Cher -- chons, cher -- chons le tré -- pas dans les flots.
  
  Par tout, ma ra -- ge te doit sui -- vre,
  n’at -- ten ny se -- cours, ny re -- pos.
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Qu’on pré -- pa -- re tout ce qu’il faut,
  \ru#8 tôt tôt.
  Tôt tôt tôt,
  Tôt tôt tôt,
  \ru#8 tôt tôt.
}
