\clef "basse" \tieDashed \tag #'(basse bc-part) <>^"[B.C.]" do1~ |
do |
fa,4.~ |
fa,8 fa fa |
sib sib sol |
la la la, |
re2. re4 |
sol2 sol, |
do4. si,8 la,4 |
\tieSolid sol,4.~ |
sol,8 sol mi |
la la fa |
sol sol sol, |
\tieDashed do1~ |
do2 fa |
mi2 mi4 |
<< la4 \\ \sugNotes { la8. fa16 } >> sib8 sol la la, |
re4. re8 sol fa mi re |
do4. sib,8 la, sol, |
fa,2 sol,4 |
do,4. do8 sol4 sold |
la2 la4 |
fa2 mi |
re2. do4 |
si,8. do16 re4 re, |
sol,4 sol8 fa16 mi re2 |
la4 la, re2 |
mi8. do16 si,8 la, mi mi, |
la,4 la8 sol16 fa mi2 |
fa4 re2 |
sol8. mi16 re8 do si, do sol,4 |
do4.~ | \allowPageTurn
do8 do[ do] |
fa4 fa8 |
re re re |
la la fa |
sib sib sol |
la la la, |
re \tag #'(basse bc-part) <>^"[tous]" re[ re] |
sol \tag #'(basse bc-part) <>^"[B.C.]" sol[ sol] |
mi \tag #'(basse bc-part) <>^"[tous]" mi[ mi] |
fa \tag #'(basse bc-part) <>^"[B.C.]" fa[ fa] |
re \tag #'(basse bc-part) <>^"[tous]" re[ re] |
sol \tag #'(basse bc-part) <>^"[B.C.]" sol[ mi] |
la la fa |
sol sol sol, |
do \tag #'(basse bc-part) <>^"[tous]" do'[ si] |
la la mi |
fa fa re |
sol sol sol, |
\tag #'(basse bc-part) <>^"[B.C.]" do2 fad, | \allowPageTurn
sol, sol4 |
do2 re |
si,8. la,16 sol,8 fa, mi,4 mi |
fa2 re4 sol |
do8 do[ do] |
fa4 fa8 |
re re re |
la la fa |
sib sib sol |
la la la, |
re \tag #'(basse bc-part) <>^"[tous]" re[ re] |
sol \tag #'(basse bc-part) <>^"[B.C.]" sol[ sol] |
mi \tag #'(basse bc-part) <>^"[tous]" mi[ mi] |
fa \tag #'(basse bc-part) <>^"[B.C.]" fa[ fa] |
re \tag #'(basse bc-part) <>^"[tous]" re[ re] |
sol \tag #'(basse bc-part) <>^"[B.C.]" sol[ mi] |
la la fa |
sol sol sol, |
do \tag #'(basse bc-part) <>^"[tous]" do'[ si] |
la la mi |
fa fa re |
sol sol sol, |
<<
  \tag #'basse { do4. }
  \tag #'(basse-continue bc-part) {
    do8. \tag #'bc-part <>^"[B.C.]" \sugNotes { sib,16[ la, sol,]
      \once\set Staff.whichBar = "|"
      \measure 4/4 \tieSolid fa,1*15/16~ \hideNotes fa,16 |
    }
  }
>>
