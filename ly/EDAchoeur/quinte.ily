\clef "quinte" R1*2 R4.*4 R1*2 R2. R4.*4 R1*2 R2.*2 R1 R2.*2 R1
R2. R1*2 R2. R1*2 R2. R1 R2. R1 R4.*7 |
r8 la la |
sol r r |
r mi' mi' |
do' r r |
r la la |
sol r r |
R4.*2 |
r8 mi' re' |
do' do' do' |
do' fa la |
sol sol sol |
sol r r4 r2 |
R2. R1*3 R4.*6 |
r8 la la |
sol r r |
r mi' mi' |
do' r r |
r la la |
sol r r |
R4.*2 |
r8 mi' re' |
do' do' do' |
do' fa la |
sol sol sol |
sol4. |
