\clef "basse" sol,1 |
\tieDashed sol2~ sol4 fad |
sol2 do |
do re4 re, |
sol,2. sol4 |
fad1 |
mi |
la2 si8 sol la la, |
re2. do8 si, |
la,2. la4 |
fad2 sol4 |
do2 dod |
re1 |
mi2 fad |
sol do~ |
do re4 re, |
sol,2 sol4 |
fad2 mi |
re1 |
la,2. |
si,2 si4. la8 |
sold2. | \allowPageTurn
la2 dod |
re1 |
si,2 la, |
sol,4 sol4. fad8 |
mi2 red4 |
mi2. |
la1 |
fad | \allowPageTurn
sol2 do'4. la8 |
si2 sol4. mi8 |
si4 si, mi4. mi8 |
dod2 re4. re8 |
sib,4. sol,8 la,2 |
re,4. re8 sol4 sol8( sol) |
do4 do sol8([ sol]) sol([ sol]) |
fad([ fad fad fad]) fa([ fa]) fa([ fa]) |
mi([ mi]) mi do re([ re]) re([ re]) |
sol,4 sol mi2 |
fa2 mi8 re |
dod1 |
re |
si,2. |
do2 dod |
re1 |
mi2 mi4. mi8 |
do1 |
re |
sol,2 sol8([ sol sol sol]) |
fad([ fad fad fad]) fa([ fa]) fa([ fa]) |
mi([ mi]) mi do re([ re]) re([ re]) |
sol,([ sol, sol, sol,] sol,4)
