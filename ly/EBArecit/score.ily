\score {
  <<
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'recit0 \includeNotes "voix"
      >> \keepWithTag #'recit0 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'recit1 \includeNotes "voix"
      >> \keepWithTag #'recit1 \includeLyrics "paroles"
      \new Staff <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
        \origLayout {
          s1*5\break s1*3\break s1*2 s2. s1\break
          s1*4 s2 \bar "" \break s4 s1*2 s2.\break s1 s2. s1\pageBreak
          s1*2 s2.*2\break s2. s1*2 s2 \bar "" \break s2 s1*2\break
          s1*2 s2 \bar "" \break s2 s1*3 s4 \bar "" \pageBreak
          s2.*2 s1\break s1 s2. s1\break s1*5\break
        }
      >>
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
