s1 s2 <2>4 <5/> s1 <6 5>2 <4>4 \new FiguredBass <3> s1
<6+>1 s1 <_+>2 <"">8\figExtOn <"">\figExtOff <4> <_+>
s4 <"">2\figExtOn <"">8\figExtOff <6+>8 s1 <6>2. s2 <5/>
s1 s2 <5/> s1 <6 5>2 <4>4 \new FiguredBass <3> s2.
<6>2 <6+> s1 s2 <6>4
<_+>2..\figExtOn <_+>8\figExtOff <6>2. <_+>2 <6>
s1 <6>2 <6> s4 <"">4.\figExtOn <"">8\figExtOff s2 <6>4
s2. <_+>1 <6> s2
s4. <6 5>8 <_+>2 <6> <4>4 <_+>2.
<5/>2 <_-> s4. <6 5>8 <_+>2
s1*2 <6>2..\figExtOn <6>8\figExtOff <7>4 <6> <4>4. <3>8
s2 <6> s <6+>4 <5/>1 s
<6>2 <5/>4 s2 <6>
s1*5
<6>2..\figExtOn <6>8\figExtOff <7>4 <6> <4> <3>
