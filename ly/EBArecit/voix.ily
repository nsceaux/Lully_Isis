<<
  \tag #'(recit1 basse) {
    \clef "vbas-dessus" <>^\markup\character Io
    r2 r4 re''8 re'' |
    sol'4. sol'8 do''4 do''8 re'' |
    si'8.\trill si'16 r8 re'' mi'' mi'' r mi''16 sol'' |
    do''4 do''8. mi''16 la'4\trill la'8. si'16 |
    sol'4 r re'' r8 re'' |
    red''4\trill red'' red'' red''8 fad'' |
    si'4\trill si'8 mi'' si' si' si' mi'' |
    dod''4\trill mi''8 fad'' re''4 re''8 dod'' |
    re''4
    \ffclef "vhaute-contre" <>^\markup\character La Furie
    r8 re' la8.\trill la16 r8 si |
    do'2 mi'4 mi'8 r |
    la'4 la'16 r re' re' si8\trill do'16 re' |
    mi'4 mi'8 mi' mi'4\trill mi'8 la' |
    fad'2\trill
    \ffclef "vbas-dessus" <>^\markup\character Io
    r4 re''8 re'' |
    sol'4 r8 sol' do''4 do''8. re''16 |
    si'8\trill si' r re'' mi'' mi'' r mi''16 sol'' |
    do''4 do''8. mi''16 la'4\trill la'8. si'16 |
    sol'4 r8 sol''16 sol'' si'8\trill si'16 do'' |
    re''4 r8 re'' mi'' mi'' mi'' mi'' |
    la'4\trill la' r la'8 si' |
    do''8 do''16 re'' mi''8 mi''16 mi'' fad''8 fad''16 fad'' |
    red''4\trill red'' r8 si' si' si' |
    mi'' si' si' dod'' re''8. mi''16 |
    dod''4\trill dod''8 mi'' la' la' mi'\trill mi'16 la' |
    fad'4\trill fad'8 la' la' la' si' do'' |
    re''4 sol'8 sol' do''4 do''8 si' |
    si'4\trill
    \ffclef "vhaute-contre" <>^\markup\character La Furie
    r8 re'16 re' re'8 mi'16 fad' |
    sol'4 si8 si16 si si8 fad |
    sol4 sol8 si16 si mi'8 mi'16 mi' |
    dod'4\trill mi'8 mi' la4 si8 dod'! |
    re'4 fad'8 fad'16 la' re'8 do' do' si |
    si\trill si r sol'16 sol' mi'4\trill mi'16 mi' mi' fad' |
    red'8\trill red' r si mi'4 mi'8 sol' |
    mi'4\trill mi'8 red' mi'4
    \tag #'recit1 {
      r8 sol'16 sol' |
      sol'4 sol'16 sol' sol' fa'32[ mi'] fa'4 fa'8 fa' |
      fa'?4\trill fa'8 sol' mi'4\trill mi'8 fa' |
      re'2 r |
      R1*4 R2. R1*2 R2. R1*8 r2 r4
    }
  }
  \tag #'(recit0 basse) {
    <<
      \tag #'basse {
        s1*10 s2. s1*5 s2. s1*2 s2. s1 s2. s1*3 s2.*3 s1*4 s2.
        \ffclef "vbas-dessus"
      }
      \tag #'recit0 {
        \clef "vbas-dessus" R1*10 R2. R1*5 R2. R1*2 R2. R1 R2. R1*3
        R2.*3 R1*4 r2 r4
      }
    >> <>^\markup\character Io
    r8 mi''16 mi'' |
    mi''4 mi''16 mi'' mi'' mi'' la'4\trill la'8 la' |
    re''4 re''8 mi'' dod''4\trill dod''8 re'' |
    re''2 <<
      \tag #'basse { s2 s1*3 s4 \ffclef "vbas-dessus" }
      \tag #'recit0 { r2 | R1*3 | r4 }
    >> <>^\markup\character Io
    si'4 do'' do''8 do'' |
    la'4\trill la'8 do''16 do'' dod''8\trill dod''16 re'' |
    mi''4 mi''8 mi'' la'4 la'8 la' |
    fad'4\trill re'' la'8 la'16 la' fad'8 fad'16 la' |
    re'8 re'
    \ffclef "vhaute-contre" <>^\markup\character La Furie
    r8 sol16 sol re'8 mi'16 fa' |
    mi'4\trill mi'8 mi' la'4 la'8 la' |
    fad'2\trill
    \tag #'recit0 { r2 R1*6 r2 r4 }
  }
  \tag #'(vdessus basse) {
    <<
      \tag #'basse {
        s1*10 s2. s1*5 s2. s1*2 s2. s1 s2. s1*3 s2.*3 s1*12 s2. s1*2
        s2. s1 s2 \ffclef "vbas-dessus"
      }
      \tag #'vdessus {
        \clef "vbas-dessus" R1*10 R2. R1*5 R2. R1*2 R2. R1 R2. R1*3
        R2.*3 R1*12 R2. R1*2 R2. R1 r2
      }
    >> <>^\markup\character Io
    re''4 re''8 re'' |
    sol'4 sol' <<
      \tag #'basse { s1 \ffclef "vdessus" <>^\markup\character Io }
      \tag #'vdessus { r2 | r2 }
    >> mi''4 mi''8 mi'' |
    la'4 la'
    \tag #'vdessus { r2 R1*3 | r2 r4 }
  }
  \tag #'(vhaute-contre basse) {
    <<
      \tag #'basse {
        s1*10 s2. s1*5 s2. s1*2 s2. s1 s2. s1*3 s2.*3 s1*7 s2
        \ffclef "vhaute-contre" <>^\markup\character Chœur
      }
      \tag #'vhaute-contre {
        \clef "vhaute-contre" R1*10 R2. R1*5 R2. R1*2 R2. R1 R2. R1*3
        R2.*3 R1*7 r2
        <>^\markup\character-text Chœur de Peuples des Climats glacez
      }
    >> re'4 re'8 re' |
    mi'4 mi' re'8([ re']) re'([ re']) |
    re'([ re' re' re']) re'([ re']) re'([ re']) |
    re'([ re']) do' do' do'([ do']) la re' |
    si4\trill <<
      \tag #'basse { s2. s2. s1*2 s2. s1*2 s2 \ffclef "vhaute-contre" }
      \tag #'vhaute-contre { r4 r2 | R2. R1*2 R2. R1*2 | r2 }
    >> <>^\markup\character Chœur si4 si8 si |
    mi'4 mi' <<
      \tag #'basse { s1 \ffclef "vhaute-contre" <>^\markup\character Chœur }
      \tag #'vhaute-contre { r2 | r2 }
    >> la4 la8 re' |
    si4\trill si re'8([ re']) re'([ re']) |
    re'([ re' re' re']) re'([ re']) re'([ re']) |
    re'([ re']) do' do' do'([ do']) la re' |
    si([\trill si si si] si4)
  }
  \tag #'vtaille {
    \clef "vtaille" R1*10 R2. R1*5 R2. R1*2 R2. R1 R2. R1*3 R2.*3 R1*7
    r2 si4 si8 si |
    do'4 do' si8([ si]) si([ si]) |
    la([ la la la]) la([ la]) la([ la]) |
    sol([ sol]) sol sol sol([ sol]) sol fad |
    sol4 r r2 |
    R2. R1*2 R2. R1*2 |
    r2 sol4 sol8 sol |
    sol4 sol r2 |
    r fad4 fad8 fad |
    sol4 sol si8([ si]) si([ si]) |
    la([ la la la]) la([ la]) la([ la]) |
    sol([ sol]) sol sol sol([ sol]) sol fad |
    sol([ sol sol sol] sol4)
  }
  \tag #'vbasse {
    \clef "vbasse" R1*10 R2. R1*5 R2. R1*2 R2. R1 R2. R1*3 R2.*3 R1*7
    r2 sol4 sol8 sol |
    do4 do sol8([ sol]) sol([ sol]) |
    fad([ fad fad fad]) fa([ fa]) fa([ fa]) |
    mi([ mi]) mi do re([ re]) re re |
    sol,4 r r2 |
    R2. R1*2 R2. R1*2 |
    r2 mi4 mi8 mi |
    do4 do r2 |
    r re4 re8 re |
    sol,4 sol, sol8([ sol]) sol([ sol]) |
    fad([ fad fad fad]) fa([ fa]) fa([ fa]) |
    mi([ mi]) mi do re([ re]) re re |
    sol,([ sol, sol, sol,] sol,4)
  }
>>