\tag #'(recit1 basse) {
  Lais -- sez- moy, cru -- el -- le Fu -- ri -- e,
  cru -- el -- le, lais -- sez- moy res -- pi -- rer un mo -- ment.
  Ah ! Bar -- ba -- re, plus je te pri -- e,
  et plus tu prens plai -- sir d’aug -- men -- ter mon tour -- ment !
  
  Soû -- pi -- re, gé -- my, pleu -- re, cri -- e ;
  je me fais de ta peine un spec -- ta -- cle char -- mant.
  
  Lais -- sez- moy, cru -- el -- le Fu -- ri -- e,
  cru -- el -- le, lais -- sez- moy res -- pi -- rer un mo -- ment.
  Quel hor -- ri -- ble sé -- jour ! Quel froid in -- su -- por -- ta -- ble !
  Tes Ser -- penrs a -- ni -- mez par ta rage im -- pla -- ca -- ble,
  ne sont- ils par d’as -- sez cru -- els bour -- reaux ?
  Pour pu -- nir un cœur mi -- se -- ra -- ble,
  viens- tu cher -- cher si loin des sup -- pli -- ces nou -- veaux ?
  
  Mal -- heu -- reux Ha -- bi -- tants d’u -- ne de -- meure af -- freu -- se,
  con -- nois -- sez de Ju -- non le fu -- nes -- te cour -- roux :
  par sa ven -- gean -- ce ri -- gou -- reu -- se,
  vous voy -- ez u -- ne mal -- heu -- reu -- se
  qui souf -- fre cent fois plus que vous.
}
\tag #'(recit0 recit1 basse) {
  Vous voy -- ez u -- ne mal -- heu -- reu -- se
  qui souf -- fre cent fois plus que vous.
}
\tag #'(vhaute-contre vtaille vbasse basse) {
  Ah ! quel -- le pei -- ne
  de trem -- bler, de lan -- guir dans l’hor -- reur des fri -- mats !
}
\tag #'(recit0 basse) {
  Ah ! Ah ! quel -- le pei -- ne
  d’é -- prou -- ver tant de maux, sans trou -- ver le tré -- pas !
  Ah ! quel -- le ven -- geance in -- hu -- mai -- ne !
  
  Vien chan -- ger de tour -- ments, passe en d’au -- tres cli -- mats.
}

\tag #'(vdessus basse) {
  Ah ! quel -- le pei -- ne !
}
\tag #'(vhaute-contre vtaille vbasse basse) {
  Ah ! quel -- le pei -- ne !
}
\tag #'(vdessus basse) {
  Ah ! quel -- le pei -- ne !
}
\tag #'(vhaute-contre vtaille vbasse basse) {
  Ah ! quel -- le pei -- ne
  de trem -- bler, de lan -- guir dans l’hor -- reur des fri -- mats !
}
