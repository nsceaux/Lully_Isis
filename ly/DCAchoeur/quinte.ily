\clef "quinte" R4.*18 R2. R4. |
do'4 do'8 |
do'4 do'8 |
re'4 re'8 |
sol4 r r |
R4. |
re'4 re'8 |
do'4 do'8 |
re'8. re'16 do'8 |
do'4 r r2 |
R1 R4.*5 R1 R4.*7 R1*2 R4.*8 r8
