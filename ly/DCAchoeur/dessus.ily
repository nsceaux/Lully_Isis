\clef "dessus" R4.*18 R2. R4. |
<>^"[Violons]" fa'16 mi' fa' sol' la' sib' |
do'' sib' do'' re'' mi'' do'' |
fa'' sol'' fa'' mi'' re'' mi'' |
mi''4\trill r r |
R4. |
sol''16 la'' sib'' la'' sol'' fa'' |
mi'' fa'' mi'' re'' do''8 |
fa''8. sol''16 mi''8\trill |
fa''4 r r2 |
R1 R4.*5 R1 R4.*7 R1*2 R4.*8 r8
