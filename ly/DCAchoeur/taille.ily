\clef "taille" R4.*18 R2. R4. |
fa'8 la'4 |
sol' sol'8 |
la'16 sib' la' sol' fa'8 |
sol'4 r r |
R4. |
sol'4 sol'8 |
sol'4 la'8 |
la'8. sol'16 sol'8\trill |
fa'4 r r2 |
R1 R4.*5 R1 R4.*7 R1*2 R4.*8 r8
