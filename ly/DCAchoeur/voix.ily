<<
  \tag #'(hierax basse) {
    <<
      \tag #'basse { s4.*18 s2 \ffclef "vbasse-taille" }
      \tag #'hierax { \clef "vbasse-taille" R4.*18 r4 r }
    >> <>^\markup\character Hierax
    do'8 do' |
    la4 la8 |
    R4.*3 |
    r4 r sol8 la |
    sib4 sib8 |
    R4.*3 |
    r4 r8 do'16 do' re'8 re' r re' |
    sib4 r8 sib si8.\trill si16 si8 do' |
    do'4 <<
      \tag #'basse { s8 s4.*4 s2 \ffclef "vbasse-taille" }
      \tag #'hierax { r8 | R4.*4 | r2 }
    >> <>^\markup\character Hierax sib4 sib8 sib |
    sol4 r8 |
    <<
      \tag #'basse { s4.*6 s2 \ffclef "vbasse-taille" }
      \tag #'hierax { R4.*6 | r2 }
    >> <>^\markup\character Hierax sol4 sol8 sol |
    la4. do'8 la la la la |
    fa4 fa8 |
    \tag #'hierax { R4.*7 r8 }
  }
  \tag #'(argus basse) {
    \clef "vbasse-taille" <>^\markup\character Argus
    r8 fa fa |
    mi8.\trill re16 do8 |
    fa8. sol16 la8 |
    sib sol do' |
    la \tag #'argus {
      r8 r |
      R4.*13 |
      r4 r la8 la |
      fa4 fa8 |
      R4.*3 |
      r4 r mi8 fad |
      sol4 sol8 |
      R4.*3 |
      r4 r8 la16 la sib8 sib r sib |
      sol4\trill r8 sol fa fa fa mi |
      mi4\trill r8 |
      R4.*4 |
      r2 <>^\markup\character Argus sol4 sol8 sol |
      mi4 r8 |
      R4.*6 |
      r2 <>^\markup\character Argus mi4 mi8 mi |
      fa4. la8 fa8 fa fa fa |
      re4 re8 |
      R4.*7 |
      r8
    }
  }
  \tag #'(vdessus basse) {
    <<
      \tag #'basse { s4.*6 s8 \ffclef "vdessus" }
      \tag #'vdessus { \clef "vdessus" R4.*6 | r8 }
      { s4.*4 s8 <>^\markup\character Syrinx & les Nymphes }
    >> fa''8 fa'' |
    re''8. re''16 sol''8 |
    mi''8.\trill mi''16 fa''8 |
    re''8. mi''16 fa''8 |
    mi''4.\trill |
    r8 re'' mi'' |
    fa''4. |
    r8 fa'' mib'' |
    re'' re'' re'' |
    mi'' mi'' mi'' |
    fa''8. fa''16 fa''8 |
    fa''8. fa''16 mi''8 |
    fa''2 <<
      \tag #'basse {
        s4 s4.*4 s2. s4.*4 s1*2 s4
        \ffclef "vdessus" <>^\markup\character Syrinx & les Nymphes
      }
      \tag #'vdessus {
        r4 | R4.*4 R2. R4.*4 R1*2 | r8 r
      }
    >> do''8 |
    la' la' re'' |
    sib'4 do''8 |
    la'\trill la' re'' |
    do''16[ sib'] la'8.\trill sol'16 |
    sol'2 <<
      \tag #'basse {
        s2 | s4. <>^\markup\character Syrinx & les Nymphes
        s4.*2 s8 \ffclef "vdessus"
      }
      \tag #'vdessus { r2 | R4.*3 | r8 }
    >> fa''8 fa'' |
    re''8. re''16 sol''8 |
    mi''8.\trill mi''16 fa''8 |
    re''8. mi''16 fa''8 |
    mi''2\trill <<
      \tag #'basse {
        s2 s1 s4.
        \ffclef "vdessus" <>^\markup\character Syrinx & les Nymphes
      }
      \tag #'vdessus { r2 | R1 R4. }
    >>
    r8 r re'' |
    sib' sib' sib' |
    mib''4 mib''8 |
    do''\trill do'' fa'' |
    mib''16[ re''] do''8.\trill sib'16 |
    sib'8 re'' re'' |
    do''8. do''16 si'8 |
    do''8
  }
  \tag #'(vbas-dessus basse) {
    <<
      \tag #'basse {
        s4.*5 s8
        \ffclef "vbas-dessus" do''8 do'' |
        la'8 s4 |
        s4.*11 s2. s4.*4 s2. s4.*4 s1*2 s4.*5 s1 s4.*2 |
        s8 \ffclef "vbas-dessus" do''8 sib' |
        la'
      }
      \tag #'vbas-dessus {
        \clef "vbas-dessus" R4.*5 |
        r8 do''8 do'' |
        la'8.\trill la'16 re''8 |
        si'8.\trill si'16 mi''8 |
        do''8. do''16 do''8 |
        do''8. do''16 si'8 |
        do''8 sol' la' |
        sib'8 sib' sib' |
        la' la' sib' |
        do'' do'' do'' |
        fa'8. fa'16 sib'8 |
        sol'8. sol'16 do''8 |
        la'8. la'16 re''8 |
        sol'8. la'16 sib'8 |
        la'2 r4 |
        R4.*4 R2. R4.*4 R1*2 |
        r8 r mi' |
        fad' fad' fad' |
        sol'4 la'8 |
        fad'\trill fad' sib' |
        la'16[ sol'] fad'8.\trill sol'16 |
        sol'2 r |
        R4.*2 |
        r8 do''\trill << \new Voice { \voiceOne \sug do'' } { \voiceTwo sib' \oneVoice } >> |
        la'8. la'16 re''8 |
        si'8.\trill si'16 mi''8 |
        do''8. do''16 do''8 |
        do''8. do''16 si'8 |
        do''2 r |
        R1 R4. |
        r8 r sib' |
        sol' sol' sol' |
        do''4 do''8 |
        la' la' re'' |
        do''16[ sib'] la'8.\trill sib'16 |
        sib'8 fa' sol' |
        la'8. sol'16 fa'8 |
        mi'8\trill
      }
    >>
  }
  \tag #'(vhaute-contre basse) {
    <<
      \tag #'basse {
        s4.*4 s8
        \ffclef "vhaute-contre" fa'8 fa' |
        mi' s4 |
        s4.*12 s2. s4.*4 s2. s4.*4 s1*2 s4.*5 s1 s4. |
        \ffclef "vhaute-contre" r8 fa' fa' |
        mi'
      }
      \tag #'vhaute-contre {
        \clef "vhaute-contre" R4.*4 |
        r8 fa' fa' |
        mi'8._\trill re'16 do'8 |
        fa'8 re' re' |
        sol'8. sol'16 mi'8 |
        la'8. la'16 fa'8 |
        sol'8. fa'16 sol'8 |
        do'4. |
        r8 sol' sol' |
        re' re' re' |
        la la la |
        sib sib sib |
        do' do' do' |
        re'8. do'16 sib8 |
        do'8. sib16 do'8 |
        fa2 r4 |
        R4.*4 R2. R4.*4 R1*2 |
        r8 r do' |
        re' re' re' |
        mib'4 do'8 |
        re' re' sib |
        do' re'16[ do'] re'8 |
        sol2 r |
        R4. |
        r8 fa' fa' |
        mi'8. re'16 do'8 |
        fa' re' re' |
        sol'8. sol'16 mi'8 |
        la'8. sol'16 fa'8 |
        sol'8. fa'16 sol'8 |
        do'2 r |
        R1 R4. |
        r8 r sib |
        mib' mib' mib' |
        do'4 do'8 |
        fa' fa' re' |
        mib' fa'16[ mib'] fa'8 |
        sib sib sib |
        fa'8. mi'16 re'8 |
        do'
      }
    >>
  }
>>