\clef "basse" \tag #'basse <>^"[B.C.]" fa4 fa,8 |
do8. sib,16 la,8 |
re8. mi16 fa8 |
sib, do do, |
fa, <<
  \tag #'conducteur { r8 r | R4.*13 | }
  \tag #'(basse basse-continue) {
    \clef "alto" fa'8[ fa'] |
    mi'8. re'16 do'8 |
    fa'8 re' re' |
    sol'8. sol'16 mi'8 |
    la'8. la'16 fa'8 |
    sol'8. fa'16 sol'8 |
    do'4. |
    r8 sol' sol' |
    re' re' re' |
    la la la |
    sib sib sib |
    do' do' do' |
    re'8. do'16 sib8 |
    do'8. sib16 do'8 | \clef "bass"
  }
>>
fa,2. |
fa4. |
\tag #'basse <>^"[Tous]" fa4. |
mi |
re |
<< \tag #'basse { s4 <>^"[B.C.]" } do2. >> |
sol,4. |
\tag #'basse <>^"[Tous]" sol |
do'4 la8 |
re'8. sib16 do'8 |
<< \tag #'basse { s4 <>^"[B.C.]" } fa2 >> re |
mib re |
do4 <<
  \tag #'conducteur { r8 | R4.*4 | }
  \tag #'(basse basse-continue) {
    \clef "alto" do'8 | \noBreak
    re' re' re' |
    mib'4 do'8 |
    re' re' sib |
    do' re'16 do' re'8 | \clef "bass"
  }
>>
sol,1 |
do8. sib,16 la, sol, |
<<
  \tag #'conducteur { fa,4. | R4.*5 | }
  \tag #'(basse basse-continue) {
    fa,8 \clef "alto" fa'[ fa'] |
    mi'8. re'16 do'8 |
    fa' re' re' |
    sol'8. sol'16 mi'8 |
    la'8. sol'16 fa'8 |
    sol'8. fa'16 sol'8 | \clef "bass"
  }
>>
do1 |
la, |
\once\tieDashed sib,4.~ |
sib,4 <<
  \tag #'conducteur { r8 | R4.*6 | r8 }
  \tag #'(basse basse-continue) {
    \clef "alto" sib8 |
    mib' mib' mib' |
    do'4 do'8 |
    fa' fa' re' |
    mib' fa'16 mib' fa'8 |
    sib sib sib |
    fa'8. mi'16 re'8 |
    do'
  }
>>
