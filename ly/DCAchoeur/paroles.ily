\tag #'(argus basse) {
  Li -- ber -- té, li -- ber -- té, li -- ber -- té, li -- ber -- té.
}
\tag #'vdessus {
  Li -- ber -- té, li -- ber -- té, li -- ber -- té, li -- ber -- té.
  Li -- ber -- té, li -- ber -- té, li -- ber -- té, li -- ber -- té,
  li -- ber -- té, li -- ber -- té.
}
\tag #'vbas-dessus {
  Li -- ber -- té, li -- ber -- té, li -- ber -- té, li -- ber -- té,
  li -- ber -- té, li -- ber -- té, li -- ber -- té, li -- ber -- té,
  li -- ber -- té, li -- ber -- té, li -- ber -- té, li -- ber -- té,
  li -- ber -- té.
}
\tag #'vhaute-contre {
  Li -- ber -- té, li -- ber -- té, li -- ber -- té, li -- ber -- té,
  li -- ber -- té, li -- ber -- té.
  Li -- ber -- té, li -- ber -- té,
  li -- ber -- té, li -- ber -- té, li -- ber -- té, li -- ber -- té,
  li -- ber -- té.
}
\tag #'basse {
  Li -- ber -- té,
  Li -- ber -- té,
  Li -- ber -- té, li -- ber -- té, li -- ber -- té, li -- ber -- té.
  Li -- ber -- té, li -- ber -- té, li -- ber -- té, li -- ber -- té,
  li -- ber -- té, li -- ber -- té.
}
\tag #'(hierax argus basse) {
  Quel -- les Dan -- ses !
  Quel -- les Dan -- ses !
  Quel -- les Dan -- ses ! quels chants ! & quel -- le nou -- veau -- té !
}
\tag #'(vdessus vbas-dessus vhaute-contre basse) {
  S’il est quel -- que bien au mon -- de,
  c’est la li -- ber -- té.
}
\tag #'(hierax argus basse) {
  Que vou -- lez- vous ?
}
\tag #'(vdessus vbas-dessus vhaute-contre basse) {
  Li -- ber -- té, li -- ber -- té, li -- ber -- té,
  \tag #'(vhaute-contre basse) { li -- ber -- té, li -- ber -- té, }
  \tag #'vbas-dessus { li -- ber -- té, }
  li -- ber -- té.
}
\tag #'(hierax argus basse) {
  Que vou -- lez- vous ? il faut qu’on nous ré -- pon -- de.
}
\tag #'(vdessus vbas-dessus vhaute-contre basse) {
  S’il est quel -- que bien au mon -- de,
  c’est la li -- ber -- té, li -- ber -- té, li -- ber -- té.
}
