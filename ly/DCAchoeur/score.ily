\score {
  <<
    \new StaffGroup \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbas-dessus \includeNotes "voix"
      >> \keepWithTag #'vbas-dessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
        %\keepWithTag #'hc \includeFiguresInStaff "chiffres"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'hierax \includeNotes "voix"
      >> \keepWithTag #'hierax \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'argus \includeNotes "voix"
      >> \keepWithTag #'argus \includeLyrics "paroles"
      \new Staff <<
        \global \keepWithTag #'basse-continue \includeNotes "basse"
        \keepWithTag #'bc \includeFigures "chiffres"
        \origLayout {
          s4.*4 s8\bar "" \break s4 s4.*8\break s4.*5 \pageBreak
          s2. s4.*4 s2. s4.\break s4.*3 s1*2 s4 \bar "" \pageBreak
          s8 s4.*4 s1 s4.*2\break s4.*5 s1\pageBreak
        }
        \modVersion { s4.*18\break }
      >>
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
