\clef "haute-contre" R4.*18 R2. R4. |
fa'8 do' re' |
mi'4 do''8 |
do''8. do''16 si'8 |
do''4 r r |
R4. |
sib'16 do'' re'' do'' sib' la' |
sol'4 fa'8 |
fa'8. sib'16 sol'8 |
la'4 r r2 |
R1 R4.*5 R1 R4.*7 R1*2 R4.*8 r8
