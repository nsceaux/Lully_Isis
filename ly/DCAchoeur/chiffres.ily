\simultaneous {
  { s4. <"">4\figExtOn <"">8\figExtOff s4. <6 5>8 <4> <3> s4.*14
    s2. s4.*2 <6>4. <7>4 <6+>8 s2. s4.*3 s4 <3>8 s2 <6> s <6+> <_!>4.
    s4.*4 s1 <"">4\figExtOn <"">8\figExtOff s4.*6 s1 <6> }
  { %\bassFigureStaffAlignmentUp
    s4.*5 <6>4\figExtOn <6>8\figExtOff s4. <_+>4. s4 <6 5>8 <4>4 <_+>8 s4 <6>8 s4 <6>8 s4 <6>8
    s4 <5/>8 s4 <6 5>8 s4. <3>8.\figExtOn <3>\figExtOff <4>4 <3>8
    s2. s4.*4 s2. s4.*4 s1*2 s4.
    <_+>4. <"">4\figExtOn <"">8\figExtOff <_+>4 <6>8 <6 5> <_+>4
    s1 s4.*2 <6>4\figExtOn <6>8\figExtOff s4. <_+> s4 <6 5>8 <4>4 <_+>8
    s1*2 s4.*3 <_->4. s4 <6>8 <6 5> <4> <3> s4. s4 <6+>8
  }
}

