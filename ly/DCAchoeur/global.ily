\key do \major
\time 3/8 \midiTempo#80 s4.*4 \key fa \major s4.*14
\digitTime\time 3/4 s2.
\time 3/8 s4.*4
\digitTime\time 3/4 s2.
\time 3/8 s4.*4
\time 4/4 s1*2
\time 3/8 s4.*5
\digitTime\time 2/2 \midiTempo#160 s1
\time 3/8 \midiTempo#80 s4.*7
\digitTime\time 2/2 \midiTempo#160 s1*2
\time 3/8 \midiTempo#80 s4.*8 s8 \bar "|."
