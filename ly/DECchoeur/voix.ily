<<
  \tag #'(vdessus1 basse) {
    <>^\markup\character Chœur 
    <<
      \tag #'basse {
        \clef "vhaute-contre" fa'8 fa' |
        mi'8 \ffclef "vbas-dessus" do''8 do'' |
        la' \clef "vdessus" \ffclef "vdessus"
      }
      \tag #'vdessus1 { \clef "vdessus" r8 r | R4. | r8 }
    >> fa''8 fa'' |
    re''8.\trill re''16 sol''8 |
    mi''8.\trill mi''16 fa''8 |
    re''8.\trill mi''16 fa''8 |
    mi''4\trill do''8 |
    la'\trill la' re'' |
    sib'4 do''8 |
    la'\trill la' re'' |
    do''16[ sib'] la'8.\trill sol'16 |
    sol'8 re'' mi'' |
    fa''4. |
    r8 fa'' mib'' |
    re'' sib' la' |
    sol'8. sol'16 do''8 |
    la'8. la'16 re''8 |
    sol'8. la'16 sib'8 |
    la'4.
  }
  \tag #'vdessus2 {
    \clef "vbas-dessus" r8 r |
    r8 do'' do'' |
    la'8.\trill la'16 re''8 |
    si'8.\trill si'16 mi''8 |
    do''8. do''16 do''8 |
    do''8. do''16 si'8 |
    do''4 mi'8 |
    fad' fad' fad' |
    sol'4 la'8 |
    fad'8\trill fad' sib' |
    la'16[ sol'] fad'8.\trill sol'16 |
    sol'8 sib' sib' |
    la' la' sib'  |
    do'' do'' do'' |
    fa' sol' fa' |
    mi'8. mi'16 la'8 |
    fa'8. fa'16 fa'8 |
    fa'8. fa'16 mi'8\trill |
    fa'4.
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" fa'8 fa' |
    mi'8. re'16 do'8 |
    fa'8 fa' re' |
    sol'8. sol'16 mi'8 |
    la'8. la'16 fa'8 |
    sol'8. fa'16 sol'8 |
    do'4 do'8 |
    re' re' re' |
    mib'4 do'8 |
    re' re' sib |
    do' re'16[ do'] re'8 |
    sol8 sol' sol' |
    re' re' re' |
    la la la |
    sib sib sib |
    do' do' do' |
    re'8. do'16 sib8 |
    do'8. sib16 do'8 |
    fa4.
  }
>>
\omit Staff.TimeSignature
