\clef "alto" fa'8 fa' |
mi'8. re'16 do'8 |
fa'8 fa' re' |
sol'8. sol'16 mi'8 |
la'8. la'16 fa'8 |
sol'8. fa'16 sol'8 |
do'4 do'8 |
re' re' re' |
mib'4 do'8 |
re' re' sib |
do' re'16[ do'] re'8 |
sol8 sol' sol' |
re' re' re' |
la la la |
sib sib sib |
do' do' do' |
re'8. do'16 sib8 |
do'8. sib16 do'8 |
fa4.
\tag #'basse-continue {
  \clef "bass"
  \omit Score.BarNumber
  \digitTime\time 3/4
  fa,4. fa8 mi re |
  \digitTime\time 2/2
  do1~ |
  do2
}
