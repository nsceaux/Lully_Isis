\tag #'(vdessus1 vdessus2 vhaute-contre basse) {
  Li -- ber -- té, li -- ber -- té, li -- ber -- té,
  \tag #'(vhaute-contre basse) { li -- ber -- té, li -- ber -- té, }
  \tag #'vdessus2 { li -- ber -- té, }
  li -- ber -- té.
  S’il est quel -- que bien au mon -- de,
  c’est la li -- ber -- té.
  Li -- ber -- té, li -- ber -- té, li -- ber -- té, li -- ber -- té, li -- ber -- té,
  \tag #'(vdessus2 vhaute-contre) { li -- ber -- té, }
  li -- ber -- té.
}
