\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'vdessus1 \includeNotes "voix"
    >> \keepWithTag #'vdessus1 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'vdessus2 \includeNotes "voix"
    >> \keepWithTag #'vdessus2 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'vhaute-contre \includeNotes "voix"
    >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
    \new Staff \with { \haraKiriFirst } <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFiguresInStaff "chiffres"
      { s4 s4.*18 \break }
    >>
  >>
  \layout { ragged-last = ##t }
  \midi { }
}
