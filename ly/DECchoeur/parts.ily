\piecePartSpecs
#`((basse-continue #:score-template "score-voix"
                   #:clef "alto"
                   #:ragged #t
                   #:music ,#{ s4 s4.*18\break #})
   (basse #:score-template "score-voix"
          #:clef "alto"))
