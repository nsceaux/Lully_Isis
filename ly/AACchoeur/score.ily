\score {
  <<
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff <<
        { s1*8 s2.*7
          s2.*4 \noHaraKiri <>^"Trompettes" s2.*22
          \revertNoHaraKiri s1*8 s2.
          \noHaraKiri s2.*4 <>^"Trompettes" s2.*24
          \revertNoHaraKiri s2.*14
          \noHaraKiri }
        \global \keepWithTag #'trompette1 \includeNotes "trompette"
      >>
      \new Staff <<
        { s1*8 s2.*7
          s2.*4 \noHaraKiri s2.*22
          \revertNoHaraKiri s1*8 s2.
          \noHaraKiri s2.*4 s2.*24
          \revertNoHaraKiri s2.*14
          \noHaraKiri }
        \global \keepWithTag #'trompette2 \includeNotes "trompette"
      >>
    >>
    \new StaffGroup \with { \haraKiriFirst } <<
      \new GrandStaff <<
        \new Staff <<
          <>^"Violons" \global \keepWithTag #'dessus1 \includeNotes "dessus"
        >>
        \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus" >>
      >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \with { \haraKiri } \withLyrics <<
        \global \keepWithTag #'renommee \includeNotes "voix"
      >> \keepWithTag #'renommee \includeLyrics "paroles"
      \new Staff <<
        \global \keepWithTag #'basse-continue \includeNotes "basse"
        \includeFigures "chiffres"
        \origLayout {
          s1*4 s2 \bar "" \break s2 s1*3 s2.\break s2.*6\pageBreak
          s2.*9\pageBreak
          s2.*9\pageBreak
          s2.*8 s1\pageBreak
          %% 5
          s1*3\break s1*3\break s1 s2.\pageBreak
          s2.*9\pageBreak
          s2.*9\pageBreak
          s2.*9\break s2.*8\break s2.*6\pageBreak
          s2.*8\pageBreak
          %% 10
          s2.*9\pageBreak
          s2.*9\pageBreak
          s2.*9\pageBreak
          s2.*8\pageBreak
          s2.*9\pageBreak
          %% 15
          s2.*9\pageBreak
        }
        \modVersion {
          s1*8 s2.*7\break
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
