\key do \major
\midiTempo#80
\time 4/4 s1
\time 2/2 s1*7
\digitTime\time 3/4 \midiTempo#160 s2.*33
\time 2/2 \midiTempo#80 s1*8
\digitTime\time 3/4 \midiTempo#160 s2.*108 \bar "|."
\digitTime\time 2/2 \once\override Staff.TimeSignature.stencil = ##f