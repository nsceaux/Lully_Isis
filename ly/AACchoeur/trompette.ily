\clef "dessus" R1*8 R2.*7 |
\setMusic #'choeur {
  R2.*4 |
  \twoVoices #'(trompette1 trompette2 trompettes) <<
    { re''4 mi'' fa'' |
      sol''8 la'' sol'' fa'' mi''4 |
      la'' re''4.\trill do''8 |
      \sug do''2. }
    { sol'4 do'' re'' |
      mi''8 fa'' mi'' re'' do''4 |
      do''4 sol'4. do''8 |
      \sug do''2. }
  >>
  R2.*4 |
  \twoVoices #'(trompette1 trompette2 trompettes) <<
    { mi''8 fa'' sol'' la'' sol''4 |
      sol''4.\trill fa''8 mi''4 |
      mi''8 fa'' sol'' la'' sol''4 |
      sol''4.\trill fa''8 mi''4 | }
    { do''8 re'' mi'' fa'' mi''4 |
      mi''4.\trill re''8 do''4 |
      do''8 re'' mi'' fa'' mi''4 |
      mi''4.\trill re''8 do''4 | }
  >>
  R2.*3 | \allowPageTurn
  \twoVoices #'(trompette1 trompette2 trompettes) <<
    { mi''8 fa'' sol'' la'' sol''4 |
      sol''4.\trill fa''8 mi''4 |
      mi''8 fa'' sol'' la'' sol''4 |
      sol''4.\trill fa''8 mi''4 |
      mi''4 do'' fa'' |
      mi''4.\trill fa''8 mi'' fa'' |
      sol''4 <<
        \new CueVoice { s4^\markup\croche-pointee-double }
        { fa''8 mi'' re''8 mi'' | }
      >>
    }
    { do''8 re'' mi'' fa'' mi''4 |
      mi''4.\trill re''8 do''4 |
      do''8 re'' mi'' fa'' mi''4 |
      mi''4.\trill re''8 do''4 |
      do'' do'' re'' do''4. re''8 do'' re'' |
      mi''4 do'' sol' | }
  >>
}
\keepWithTag #'(trompette1 trompette2 trompettes) \choeur
\twoVoices #'(trompette1 trompette2 trompettes) <<
  { mi''2\trill }
  { do''2 }
>> r2 |
R1*7 R2. |
\choeur
\twoVoices #'(trompette1 trompette2 trompettes) <<
  { mi''2.\trill }
  { do''2. }
>>
R2.*46 |
\twoVoices #'(trompette1 trompette2 trompettes) <<
  { re''4 mi'' fa'' |
    sol''8 la'' sol'' fa'' mi''4 |
    la'' re''4.\trill do''8 |
    do''2. | }
  { sol'4 do'' re'' |
    mi''8 fa'' mi'' re'' do''4 |
    do''4 sol'4. do''8 |
    do''2. | }
>>
R2.*15 |
\twoVoices #'(trompette1 trompette2 trompettes) <<
  { mi''8 fa'' sol'' la'' sol''4 |
    sol''4.\trill fa''8 mi''4 |
    mi''8 fa'' sol'' la'' sol''4 |
    sol''4.\trill fa''8 mi''4 | }
  { do''8 re'' mi'' fa'' mi''4 |
    mi''4.\trill re''8 do''4 |
    do''8 re'' mi'' fa'' mi''4 |
    mi''4.\trill re''8 do''4 | }
>>
R2.*3 |
\twoVoices #'(trompette1 trompette2 trompettes) <<
  { mi''8\trill fa'' sol'' la'' sol''4 |
    sol''4.\trill fa''8 mi''4 |
    mi''8 fa'' sol'' la'' sol''4 |
    sol''4.\trill fa''8 mi''4 |
    mi'' do'' fa'' |
    mi''4.\trill fa''8 mi'' fa'' |
    sol''4 <<
      \new CueVoice { s4^\markup\croche-pointee-double }
      { fa''8 mi'' re''8 mi'' | }
    >>
    mi''2.\trill | }
  { do''8 re'' mi'' fa'' mi''4 |
    mi''4.\trill re''8 do''4 |
    do''8 re'' mi'' fa'' mi''4 |
    mi''4.\trill re''8 do''4 |
    do'' do'' re'' |
    do''4. re''8 do'' re'' |
    mi''4 do'' sol' |
    do''2. | }
>>
