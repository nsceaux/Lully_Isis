\clef "dessus" R1*8 R2.*7 |
\setMusic #'choeur {
  mi''4 mi'' mi'' |
  re''2\trill re''4 |
  re'' <<
    \new CueVoice { s4^\markup\croche-pointee-double }
    { do''8 si' la'8 si' | }
  >>
  si'2.\trill |
  \twoVoices #'(dessus1 dessus2 dessus) <<
    { re''4 mi'' fa'' |
      sol''8 la'' sol'' fa'' mi''4 |
      la'' re''4.\trill do''8 |
      << mi''4 \\ \sug do''4 >> }
    { \stopHaraKiri sol'4 do'' re'' |
      mi''8 fa'' mi'' re'' do''4 |
      <<
        \sugNotes { la''4 re''4.\trill mi''8 | } \\
        { do''4 sol'4. do''8 | }
      >> mi''4 \startHaraKiri }
  >> mi''4 mi'' |
  re''2\trill re''4 |
  sol'' do'' fa'' |
  mi''2\trill mi''4 |
  sol'' <<
    \new CueVoice { s4^\markup\croche-pointee-double }
    { fa''8 mi'' re''8 mi'' | }
  >>
  \twoVoices #'(dessus1 dessus2 dessus) <<
    { mi''8\trill fa'' sol'' la'' sol''4 |
      sol''4.\trill fa''8 mi''4 |
      mi''8 fa'' sol'' la'' sol''4 |
      sol''4.\trill fa''8 mi''4 | }
    { \stopHaraKiri << \sug mi''8 do'' >> re'' mi'' fa'' mi''4 |
      mi''4.\trill re''8 do''4 |
      do''8 re'' mi'' fa'' mi''4 |
      mi''4.\trill re''8 do''4 | \stopHaraKiri }
  >>
  mi''4 do'' fa'' |
  mi''2\trill mi''4 |
  sol''4 <<
    \new CueVoice { s4^\markup\croche-pointee-double }
    { fa''8 mi'' re''8 mi'' | }
  >>
  \twoVoices #'(dessus1 dessus2 dessus) <<
    { mi''8 fa'' sol'' la'' sol''4 |
      sol''4.\trill fa''8 mi''4 |
      mi''8 fa'' sol'' la'' sol''4 |
      sol''4.\trill fa''8 mi''4 |
      mi''4 do'' fa'' |
      mi''4.\trill fa''8 mi'' fa'' |
      sol''4 <<
        \new CueVoice { s4^\markup\croche-pointee-double }
        { fa''8 mi'' re''8 mi'' | }
      >>
    }
    { \stopHaraKiri << \sug mi''8 do'' >> re'' mi'' fa'' mi''4 |
      mi''4.\trill re''8 do''4 |
      do''8 re'' mi'' fa'' mi''4 |
      mi''4.\trill re''8 do''4 |
      do'' do'' re'' do''4. re''8 do'' re'' |
      <<
        \sugNotes { mi'' sol'' fa''8.*2/3 mi''16*2 re''8 mi'' } \\
        { mi''4 do'' sol' | }
      >> \startHaraKiri }
  >>
}
\keepWithTag #'(dessus1 dessus2 dessus) \choeur
<< \sug mi''2\trill \\ do'' >> r |
R1*7 R2. |
\choeur
mi''2.\trill |
R2.*14 |
mi''4 do''4. mi''8 |
la'2\trill la'4 |
re'' la' re'' |
si'2.\trill |
mi''4 do'' fa'' |
mi''2\trill mi''4 |
sol'' <<
  \new CueVoice { s4^\markup\croche-pointee-double }
  { fa''8 mi'' re''8 mi'' | }
>>
mi''2.\trill |
R2.*4 |
mi''4 mi'' mi'' |
do''2 do''4 |
re'' si' mi'' |
dod''2.\trill |
mi''4 mi'' fa'' |
re''2\trill re''4 |
re'' re'' dod''\trill |
re''2. |
R2.*4 |
mi''4 do'' fa'' |
mi''2\trill mi''4 |
sol''4 <<
  \new CueVoice { s4^\markup\croche-pointee-double }
  { fa''8. mi''16 re''8 mi'' | }
>>
mi''2.\trill |
R2.*4 |
\twoVoices #'(dessus1 dessus2 dessus) <<
  { re''4 mi'' fa'' |
    sol''8 la'' sol'' fa'' mi''4 |
    la'' re''4.\trill do''8 |
    do''4 }
  { \stopHaraKiri sol'4 do'' re'' |
    mi''8 fa'' mi'' re'' do''4 |
    <<
      \sugNotes {
        la''4 re''4.\trill mi''8 | mi''4
      } \\
      { do''4 sol'4. do''8 | do''4 }
    >> \startHaraKiri }
>> do''4. mi''8 |
la'2\trill la'4 |
re'' la' re'' |
si'2.\trill |    
R2.*5 | \allowPageTurn
mi''4 do''4. mi''8 |
la'2\trill la'4 |
re'' la' re'' |
si'2.\trill |
mi''4 do'' fa'' |
mi''2\trill mi''4 |
sol'' <<
  \new CueVoice { s4^\markup\croche-pointee-double }
  { fa''8 mi'' re''8 mi'' | }
>>
\twoVoices #'(dessus1 dessus2 dessus) <<
  { mi''8 fa'' sol'' la'' sol''4 |
    sol''4.\trill fa''8 mi''4 |
    mi''8 fa'' sol'' la'' sol''4 |
    sol''4.\trill fa''8 mi''4 | }
  { \stopHaraKiri << \sug mi''8 do'' >> re'' mi'' fa'' mi''4 |
    mi''4.\trill re''8 do''4 |
    do''8 re'' mi'' fa'' mi''4 |
    mi''4.\trill re''8 do''4 | \startHaraKiri }
>>
mi''4 do'' fa'' |
mi''2\trill mi''4 |
sol'' <<
  \new CueVoice { s4^\markup\croche-pointee-double }
  { fa''8 mi'' re''8 mi'' | }
>>
\twoVoices #'(dessus1 dessus2 dessus) <<
  { mi''8\trill fa'' sol'' la'' sol''4 |
    sol''4.\trill fa''8 mi''4 |
    mi''8 fa'' sol'' la'' sol''4 |
    sol''4.\trill fa''8 mi''4 |
    mi'' do'' fa'' |
    mi''4.\trill fa''8 mi'' fa'' |
    sol''4 <<
      \new CueVoice { s4^\markup\croche-pointee-double }
      { fa''8 mi'' re''8 mi'' | }
    >>
    mi''2.\trill | }
  { \stopHaraKiri << \sug mi''8 do'' >> re'' mi'' fa'' mi''4 |
    mi''4.\trill re''8 do''4 |
    do''8 re'' mi'' fa'' mi''4 |
    mi''4.\trill re''8 do''4 |
    do'' do'' re'' |
    do''4. re''8 do'' re'' |
    <<
      \sugNotes {
        mi''8 sol'' fa''8. mi''16 re''8 mi'' |
        mi''2.\trill |
      } \\
      { mi''4 do'' sol' | do''2. | }
    >>
  }
>>
