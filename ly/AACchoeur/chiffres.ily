s1 s2 <7>4 <6+> s2 <"">4\figExtOn <"">\figExtOff <6>4. <6>8 s2
s <6> s4 <6> <7> <6+> s1 s2 <_+> s2.
s4 <6> <5/> s2. s4 <6> <6+> s2.*3
\setMusic #'choeur {
<"">2\figExtOn <"">4\figExtOff s2. <6>4 <6 5> <_+> s2. s4 <6 4> <6> <6>2. <6 5>4 <4> <3> s2.*2
<6>2. s <6>4 <6 5>2 s2.*6
<6>4 <6 5>2 s2.*6 <6>4 <6 5>2
}
\keepWithTag #'() \choeur s1
s2 <5/> <_+>2.\figExtOn <_+>8\figExtOff <6+> s2 <7>4 <6>
<_+>1 <_+>2. <4>8 <_+> s2. <6>4
s2 <4>4 <3> s2.
\choeur s2.

s2 <6>4 s2. <"">4.\figExtOn <"">8\figExtOff s4 s2. <"">4.\figExtOn <"">8\figExtOff s4 <_+>2. s4 <6 5> <_+> <_+>2\figExtOn <_+>4\figExtOff
<6>2. s s4 <6 4> <6+> s2.*3

s2.*4 <6>2. s <6>4 <6 5>2 s2.
<"">2\figExtOn <"">4\figExtOff s2. <"">8*5\figExtOn <"">8\figExtOff <_+>8*5\figExtOn <_+>8\figExtOff <6>2. s <6 5>4 <4> <_+> <_+>2. s2 <6>4
s2. <6 5 _->4 <4> <_+> <_+>2. <_+>2\figExtOn <_+>4\figExtOff s2. <6>4 <7>2 s2.*3
<6>4 <6 5>2 s2.*2 <_+>2. s4 <6 5> <_+> s2. s4 <6 4> <6> <6>2. <6 5>4 <4> <3>
s2.*4 s4 <6> <5> <"">8*5\figExtOn <"">8\figExtOff s4 <4> <6+> s2.
s2.*5 <6>2. s <6>4 <6 5>2 s2.
s2.*5 <6>4 <6 5>2 s2.*3
s2.*3 <6>4 <6>2 s2.
