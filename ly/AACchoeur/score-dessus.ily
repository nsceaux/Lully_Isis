\score {
  \new GrandStaff <<
    \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus" >>
    \new Staff <<
      \global \keepWithTag #'dessus2 \includeNotes "dessus"
      { s1*8 s2.*11\break s2.*9\break s2.*9\break s2.*4 s1*8 s2.*3\break s2.*10\pageBreak
        s2.*8\break s2.*22\break s2.*13\break s2.*13\break s2.*17\pageBreak
        s2.*8\break s2.*7\break
      }
    >>
  >>
  \layout {
    \context {
      \Score
      \override NonMusicalPaperColumn #'line-break-permission = ##f
      \override NonMusicalPaperColumn #'page-break-permission = ##f
    }
  }
}
