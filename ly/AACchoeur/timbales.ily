\clef "bass" R1*8 R2.*11 |
sol2 sol4 |
do2 do4 |
do sol sol |
do4 r r |
R2.*4 |
do4. do16 do do4 |
do4. do8 do4 |
do4. do16 do do4 |
do4. do16 do do4 |
R2.*3 |
do4. do16 do do4 |
do4. do16 do do4 |
do4. do16 do do4 |
do4. do8 do4 |
R2.*3 |
R1*8 |
R2.*5 |
sol2 sol4 |
do2 do4 |
do sol sol |
do4 r r |
R2.*4 |
do4. do16 do do4 |
do4. do8 do4 |
do4. do16 do do4 |
do4. do16 do do4 |
R2.*3 |
do4. do16 do do4 |
do4. do16 do do4 |
do4. do16 do do4 |
do4. do8 do4 |
R2.*50 |
sol2 sol4 |
do2 do4 |
do sol sol |
do4 r r |
R2.*15 |
do4. do16 do do4 |
do4. do8 do4 |
do4. do16 do do4 |
do4. do8 do4 |
R2.*3 |
do4. do16 do do4 |
do4. do8 do4 |
do4. do16 do do4 |
do4. do8 do4 |
R2.*4 |
