\clef "quinte" R1*8 R2.*7 |
\setMusic #'choeur {
  do'4 do' do' |
  re'2 re'4 |
  re' la la |
  sol2. |
  re'4 do' fa' |
  do'2 do'4 |
  re' re' sol |
  sol do' do' |
  re'2 re'4 |
  do'4 do' re' |
  mi'2 mi'4 |
  do' la re' |
  do' sol do' |
  do' sol do' |
  do' sol'4. sol'8 |
  mi'4.\trill fa'8 sol'4 |
  do' do' re' |
  mi'2 mi'4 |
  do'4 la re' |
  do' sol do' |
  do' sol do' |
  do' sol' sol' |
  mi'4.\trill fa'8 sol'4 |
  sol4 la << si \\ \sug la >> |
  la2 do'4 |
  do' la re' |
}
\keepWithTag #'() \choeur
do'2 r |
R1*7 R2. |
\choeur
do'2. |
R2.*14 |
do'4 do'4. do'8 |
do'2 do'4 |
fa fa fa |
sol2. |
do'4 do' re' |
mi'2 mi'4 |
do' la re' |
do'2. |
R2.*4 |
do'4 do' do' |
do'2 do'4 |
si si4.\trill la8 |
la2. |
la4 la la |
sib2 sib4 |
sib? la4. la8 |
la2. |
R2.*4 |
do'4 do' re' |
mi'2 mi'4 |
do'4 la re' |
do'2. |
R2.*4 |
re'4 do' fa' |
do'2 do'4 |
re' re' sol |
sol do'4. do'8 |
do'2 do'4 |
fa fa fa |
sol2. |
R2.*5 |
do'4 do'4. do'8 |
do'2 do'4 |
fa fa fa |
sol2 r4 |
do'4 do' re' |
mi'2 mi'4 |
do' la re' |
do' sol do' |
do' sol do' |
do' sol' sol' |
mi'4. fa'8 sol'4 |
do' do' re' |
mi'2 mi'4 |
do' la re' |
do' sol do' |
do' sol do' |
<< \sugNotes { do'4 sol' sol' } \\ { do'4. fa'8 sol'4 | } >>
<<
  \new CueVoice {
    s4^\markup\note-by-number #2 #1 #UP s^\markup\note-by-number #3 #0 #UP
  }
  { mi'4 fa' sol' | }
>>
sol la la |
la2 do'4 |
do' la re' |
do'2. |
