\clef "taille" R1*8 R2.*7 |
\setMusic #'choeur {
  sol'4 sol' sol' |
  sol'2 sol'4 |
  sol' mi'\trill re' |
  re'2. |
  sol'4 mi' re' |
  sol'2 la'4 |
  la' sol'4. fa'8 |
  mi'4\trill sol' sol' |
  sol'2 sol'4 |
  sol'4 fa' la' |
  la'2 la'4 |
  sol' la' sol' |
  sol'8 fa' mi' re' mi'4 |
  do'8 re' mi' fa' sol'4 |
  sol'8 fa' mi' re' mi'4 |
  do'4. re'8 mi'4 |
  sol' fa' la' |
  la'2 la'4 |
  sol'4 la' sol' |
  sol'8 fa' mi' re' mi'4 |
  do'8 re' mi' fa' sol'4 |
  sol'8 fa' mi' re' mi'4 |
  do'4. re'8 mi'4 |
  sol' fa' la' |
  la'2 la'4 |
  sol' la' sol' |
}
\keepWithTag #'() \choeur
sol'2 r |
R1*7 R2. |
\choeur
sol'2. |
R2.*14 |
mi'4 mi'4. mi'8 |
fa'2 fa'4 |
fa' fa' fa' |
re'2. |
sol'4 fa' la' |
la'2 la'4 |
sol' la' sol' |
sol'2. |
R2.*4 |
mi'4 mi' mi' |
fa'2 fa'4 |
fa' mi'4. mi'8 |
mi'2. |
dod'4 dod' re' |
re'2 fa'4 |
mi' mi'4.\trill re'8 |
re'2. |
R2.*4 |
sol'4 fa' la' |
la'2 la'4 |
sol' la' sol' |
sol'2. |
R2.*4 |
sol'4 mi' re' |
sol'2 la'4 |
la' sol'4. fa'8 |
mi'4\trill mi'4. mi'8 |
fa'2 fa'4 |
fa' fa' fa' |
re'2. |
R2.*5 |
mi'4 mi'4. mi'8 |
fa'2 fa'4 |
fa' fa' fa' |
re'2\trill r4 |
sol'4 fa' la' |
la'2 la'4 |
sol' la' sol' |
sol'8 fa' mi' re' mi'4 |
do'8 re' mi' fa' sol'4 |
sol'8 fa' mi' re' mi'4 |
do'4. re'8 mi'4 |
sol' fa' la' |
la'2 la'4 |
sol' la' sol' |
sol'8 fa' mi' re' mi'4 |
do'8 re' mi' fa' sol'4 |
sol'8 fa' mi' re' mi'4 |
do'4. re'8 mi'4 |
sol' fa' la' |
la'2 la'4 |
sol' la' sol' |
sol'2. |

