\clef "basse"
<<
  \tag #'basse { R1*8 R2.*7 }
  \tag #'(basse-continue bc-part) {
    \tag #'bc-part <>^"[B.C.]"
    do1 |
    do4. si,8 la,2 |
    sol, sol4 fa |
    mi4. re8 do2 |
    sol mi |
    fa mi |
    re la, |
    mi4. do8 re4 re, |
    sol,2. |
    sol4 la si |
    do' do8 re mi do |
    fa2 mi4 |
    re2 sol4 |
    do sol,2 |
    do2. |
  }
>>
\setMusic #'choeur {
  <>^"[Tous]"
  do4 mi do |
  sol2 sol4 |
  si, do re |
  sol,2. |
  sol2 fa4 |
  mi2 la4 |
  fa sol sol, |
  do do' do |
  sol2 sol4 |
  mi fa re |
  la2 la4 |
  mi fa sol |
  do4. do16 do do4 |
  do4. do8 do4 |
  do'4. do'16 do' do'4 |
  <<
    \sugNotes { do'4. do'8 do'4 } \\
    { do'4. do'16 do' do'4 | }
  >>
  do fa re |
  la2 la4 |
  mi fa sol |
  do4. do16 do do4 |
  <<
    \sugNotes {
      do4. do8 do4 |
      do'4. do'8 do'4 |
      do'4. do'16 do' do'4 | } \\
    { do4. do16 do do4 |
      do'4. do'16 do' do'4 |
      do'4. do'8 do'4 | }
  >>
  do fa re |
  la2 la4 |
  mi fa sol |
}
\keepWithTag #'(basse basse-continue bc-part) \choeur
<<
  \tag #'basse { do2 r | }
  \tag #'basse-continue { do1~ | }
  \tag #'bc-part <<
    \new Voice { \voiceTwo do2 r | }
    { \voiceOne do1^"[B.C.]"~ | \oneVoice }
  >>
>>
<<
  \tag #'basse {
    R1*7 R2. |
  }
  \tag #'(basse-continue bc-part) {
    do2 dod |
    re2. do8 si, |
    la,4. sol,8 fa,2 |
    mi,2 mi4 sold, |
    la, la sib8 sol la la, |
    re2 sol4 mi |
    fa2 sol4 sol, |
    do2. |
  }
>>
\keepWithTag #'() \choeur
do2. |
<<
  \tag #'basse { R2.*14 }
  \tag #'(basse-continue bc-part) {
    \tag #'bc-part <>^"[B.C.]"
    do4. re8 mi4 |
    fa4. sol8 fa mi |
    re4. mi8 fad re |
    sol4. la8 sol fa |
    mi4. fa8 sol mi |
    la2 la4 |
    sib sol la |
    re mi fad |
    sol la si |
    do' do8 re mi do |
    fa2 mi4 |
    re2 sol4 |
    do sol,2 |
    do2. |
  }
>>
\tag #'bc-part <>^"[Tous]"
do'4 do'4. do8 |
fa2 fa4 |
re re re |
sol2. |
mi4 fa re |
la2 la4 |
mi fa sol |
do2. |
<<
  \tag #'basse { R2.*4 }
  \tag #'(basse-continue bc-part) {
    \tag #'bc-part <>^"[B.C.]"
    do4. re8 mi4 |
    fa4. sol8 fa mi |
    re4. mi8 fa re |
    mi4. fa8 mi re |
  }
>>
\tag #'bc-part <>^"[Tous]" do2 do4 |
fa2 fa4 |
re mi mi, |
la,2. |
la4 la fa |
sib2 sib4 |
sol la la, |
re2. | \allowPageTurn
<<
  \tag #'basse { R2.*4 }
  \tag #'(basse-continue bc-part) {
    \tag #'bc-part <>^"[B.C.]"
    re4. mi8 fad4 |
    sol4. la8 sol fa |
    mi4 re sol |
    do2. |
  }
>>
\tag #'bc-part <>^"[Tous]" do4 fa re |
la2 la4 |
mi fa sol |
do2. |
<<
  \tag #'basse { R2.*4 }
  \tag #'(basse-continue bc-part) {
    \tag #'bc-part <>^"[B.C.]"
    do'4 do'8 si la4 |
    re'2. |
    mi'4 do' re' |
    sol2. |
  }
>>
\tag #'bc-part <>^"[Tous]" sol2 fa4 |
mi4. mi8 la4 |
fa sol sol, |
do4 do' do |
fa2. |
re4 re re |
sol2. |
<<
  \tag #'basse { R2.*5 }
  \tag #'(basse-continue bc-part) {
    \tag #'bc-part <>^"[B.C.]"
    sol4 la si |
    do' do8 re mi do |
    fa2 mi4 |
    re2 sol4 |
    do4 sol,2 |
  }
>>
\tag #'bc-part <>^"[Tous]" do4 << \sug do'4. \\ do >> do8 |
fa2 fa4 |
re re re |
sol2. |
mi4 fa re |
la2 la4 |
mi fa sol |
do4. do16 do do4 |
do4. do8 do4 |
do'4. do'16 do' do'4 |
do'4. do'8 do'4 |
do4 fa re |
la2 la4 |
mi fa sol |
do4. do16 do do4 |
do4. do8 do4 |
do'4. do'16 do' do'4 |
do'4. do'8 do'4 |
do4 fa re |
la2 la4 |
mi fa sol |
<<
  \tag #'basse {
    do2. |
  }
  \tag #'(basse-continue bc-part) {
    do4. \tag #'bc-part <>^"[B.C.]" do8 si, la, |
    \once\set Staff.whichBar = "|"
    \override Staff.TimeSignature.stencil = #ly:time-signature::print
    sol,1~ |
    sol,2
  }
>>
