\tag #'(renommee basse) {
  C’est luy dont les Dieux ont fait choix
  pour com -- bler le bon- heur de l’Em -- pi -- re Fran -- çois ;
  en vain, pour le trou -- bler, tout s’u -- nit, tout cons -- pi -- re,
  c’est en vain que l’En -- vie a li -- gué tant de Rois.
  Heu -- reux l’Em -- pi -- re,
  heu -- reux l’Em -- pi -- re
  qui suit ses Loix !
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Heu -- reux l’Em -- pi -- re
  qui suit ses Loix !
  Heu -- reux l’Em -- pi -- re,
  heu -- reux l’Em -- pi -- re
  qui suit ses Loix !
  Heu -- reux l’Em -- pi -- re
  qui suit ses Loix !
  Heu -- reux l’Em -- pi -- re
  qui suit ses Loix !
}
\tag #'(renommee basse) {
  Il faut que par tout on l’ad -- mi -- re :
  par -- lons de ses ver -- tus, ra -- con -- tons ses ex -- ploits ;
  à peine y pour -- rons- nous suf -- fi -- re,
  a -- vec tou -- tes nos voix.
  A peine y pour -- rons- nous suf -- fi -- re,
  a -- vec tou -- tes nos voix.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Heu -- reux l’Em -- pi -- re
  qui suit ses Loix !
  Heu -- reux l’Em -- pi -- re,
  heu -- reux l’Em -- pi -- re
  qui suit ses Loix !
  Heu -- reux l’Em -- pi -- re
  qui suit ses Loix !
  Heu -- reux l’Em -- pi -- re
  qui suit ses Loix !
}
\tag #'(renommee basse) {
  Il faut le di -- re
  cent & cent fois,
  il faut le di -- re
  cent & cent fois :
  Heu -- reux l’Em -- pi -- re,
  heu -- reux l’Em -- pi -- re
  qui suit ses Loix !
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Il faut le di -- re
  cent & cent fois :
  Heu -- reux l’Em -- pi -- re
  qui suit ses Loix !
}
\tag #'(renommee basse) {
  Il faut le di -- re
  cent & cent fois :
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Heu -- reux l’Em -- pi -- re
  qui suit ses Loix !
  Heu -- reux l’Em -- pi -- re
  qui suit ses Loix !
}
\tag #'(renommee basse) {
  Il faut le di -- re
  cent & cent fois :
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Heu -- reux l’Em -- pi -- re
  qui suit ses Loix !
}
\tag #'(renommee basse) {
  Il faut le di -- re
  cent & cent fois :
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Il faut le di -- re
  cent & cent fois :
}
\tag #'(renommee basse) {
  Heu -- reux l’Em -- pi -- re,
  heu -- reux l’Em -- pi -- re
  qui suit ses Loix !
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Il faut le di -- re
  cent & cent fois :
  Heu -- reux l’Em -- pi -- re
  qui suit ses Loix !
  Heu -- reux l’Em -- pi -- re
  qui suit ses Loix !
  Heu -- reux l’Em -- pi -- re
  qui suit ses Loix !
}
