\clef "haute-contre" R1*8 R2.*7 |
\setMusic #'choeur {
  do''4 do'' do'' |
  si'2\trill sol'4 |
  sol' sol' fad'\trill |
  sol'2. |
  si'4 do'' la' |
  sol'2 do''4 |
  do''4 si'4.\trill do''8 |
  do''4 do'' do'' |
  si'2\trill si'4 |
  do'' la' re'' |
  do''2 do''4 |
  mi'' re'' si' |
  do''4. do''16 do'' sol'4 |
  do''4. do''8 do''4 |
  do''4. do''16 do'' sol'4 |
  do''4. do''8 do''4 |
  do''4 la' re'' |
  do''2 do''4 |
  mi''4 re'' si' |
  do''4. do''16 do'' sol'4 |
  do''4. do''8 do''4 |
  do''4. do''16 do'' sol'4 |
  do''4. do''8 do''4 |
  do''4 la' la'8 si' |
  do''2 do''4 |
  do'' do'' si'\trill |
}
\keepWithTag #'() \choeur
do''2 r |
R1*7 R2. |
\choeur
do''2. |
R2.*14 |
sol'4 sol'4. sol'8 |
fa'2 fa'4 |
la' la' la' |
sol'2. |
do''4 la' re'' |
do''2 do''4 |
mi'' re'' si' |
do''2. |
R2.*4 |
sol'4 sol' sol' |
la'2 la'4 |
la' la' sold'\trill |
la'2. |
la'4 la' la' |
fa'2 fa'4 |
sol' mi' la' |
fad'2.\trill |
R2.*4 |
do''4 la' re'' |
do''2 do''4 |
mi''4 re'' si' |
do''2. |
R2.*4 |
si'4 do'' la' |
sol'2 do''4 |
do'' si'4.\trill do''8 |
do''4 sol'4. sol'8 |
<< { la'2 la'4 } \\ \sugNotes { fa'2 fa'4 } >> |
la' la' la' |
sol'2. |
R2.*5 |
sol'4 sol'4. sol'8 |
fa'2 fa'4 |
la' la' la' |
sol'2 r4 |
do''4 la' re'' |
do''2 do''4 |
mi'' re'' si' |
do''4. do''16 do'' sol'4 |
do''4. do''8 do''4 |
do''4. do''16 do'' sol'4 |
do''4. do''8 do''4 |
do'' la' re'' |
do''2 do''4 |
mi'' re'' si' |
do''4. do''16 do'' sol'4 |
do''4. do''8 do''4 |
do''4. do''16 do'' sol'4 |
do''4. do''8 do''4 |
do'' la' re'' |
do''2 do''4 |
mi'' re'' si' |
do''2. |
