%% La Renommée
<<
  \tag #'(renommee basse) {
    \clef "vdessus" <>^\markup\character La Renommée
    r2 r4 do''4 |
    sol' sol'8 sol' do''4 do''8 si' |
    si'4\trill si'8 re'' sol'4 la'8 si' |
    do''4 do''8 re'' mi''4 mi''8 fa'' |
    re''4.\trill sol'8 do''4 do''8 do''16 sol' |
    la'4 re''8 re'' re''4 dod''8 mi'' |
    la'8 la' r la'16 si' do''4 do''8 re'' |
    si'4\trill si'8 mi'' la'4\trill la'8 si' |
    sol'2. |
    si'4 do'' re'' |
    mi''2 mi''4 |
    do'' re'' mi'' |
    fa''2 re''4 |
    mi'' re''4.\trill do''8 |
    do''2. |
  }
  \tag #'vdessus { \clef "vdessus" R1*8 R2.*7 | <>^\markup\character Chœur }
  \tag #'vhaute-contre { \clef "vhaute-contre" R1*8 R2.*7 | }
  \tag #'vtaille { \clef "vtaille" R1*8 R2.*7 | }
  \tag #'vbasse { \clef "vbasse" R1*8 R2.*7 | }
>>
%% Chœur
\setMusic #'choeur <<
  \tag #'(renommee basse) { \tag #'basse <>^\markup\character Chœur R2.*26 | }
  \tag #'vdessus {
    mi''4 mi'' mi'' |
    re''2\trill re''4 |
    re'' <<
      \new CueVoice {
        s8^\markup\note-by-number #3 #1 #UP s8^\markup\note-by-number #4 #0 #UP
      }
      { do''8 si'_[ la'8 si'] | }
    >>
    si'2.\trill |
    R2.*3 |
    mi''4 mi'' mi'' |
    re''2\trill re''4 |
    sol''4 do'' fa'' |
    mi''2\trill mi''4 |
    sol'' <<
      \new CueVoice {
        s8^\markup\note-by-number #3 #1 #UP s8^\markup\note-by-number #4 #0 #UP
      }
      { fa''8 mi''_[ re''8 mi''] | }
    >>
    mi''2.\trill |
    R2.*3 |
    mi''4 do'' fa'' |
    mi''2\trill mi''4 |
    sol''4 <<
      \new CueVoice {
        s8^\markup\note-by-number #3 #1 #UP s8^\markup\note-by-number #4 #0 #UP
      }
      { fa''8 mi''_[ re''8 mi''] | }
    >>
    mi''2.\trill |
    R2.*3 |
    mi''4 do'' fa'' |
    mi''2\trill mi''4 |
    sol'' <<
      \new CueVoice {
        s8^\markup\note-by-number #3 #1 #UP s8^\markup\note-by-number #4 #0 #UP
      }
      { fa''8 mi''[ re''8 mi''] | }
    >>
  }
  \tag #'vhaute-contre {
    sol'4 sol' sol' |
    sol'2 sol'4 |
    sol' sol' fad'\trill |
    sol'2. |
    R2.*3 |
    sol'4 sol' sol' |
    sol'2 sol'4 |
    sol'4 la' la' |
    la'2 la'4 |
    sol' la' sol' |
    sol'2. |
    R2.*3 |
    sol'4 la' la' |
    la'2 la'4 |
    sol'4 la' sol' |
    sol'2. |
    R2.*3 |
    sol'4 la' la' |
    la'2 la'4 |
    sol' la' sol' |
  }
  \tag #'vtaille {
    do'4 do' do' |
    si2\trill si4 |
    re' mi' re' |
    re'2. |
    R2.*3 |
    do'4 do' do' |
    si2\trill si4 |
    do'4 la re' |
    do'2 do'4 |
    mi' re' si |
    do'2. |
    R2.*3 |
    do'4 la re' |
    do'2 do'4 |
    mi'4 re'\trill si |
    do'2. |
    R2.*3 |
    do'4 la re' |
    do'2 do'4 |
    mi' re' si |
  }
  \tag #'vbasse {
    do4 mi do |
    sol2 sol4 |
    si, do re |
    sol,2. |
    R2.*3 |
    do'4 do' do |
    sol2 sol4 |
    mi fa re |
    la2 la4 |
    mi fa sol |
    do2. |
    R2.*3 |
    do4 fa re |
    la2 la4 |
    mi4 fa sol |
    do2. |
    R2.*3 |
    do4 fa re |
    la2 la4 |
    mi fa sol |
  }
>>
\keepWithTag #'(renommee basse vdessus vhaute-contre vtaille vbasse) \choeur
%% La Renommée
<<
  \tag #'(renommee basse) {
    r2 r4 <>^\markup\character La Renommée r8 do'' |
    mi'4 mi'8 mi' la'4 la'8 si' |
    fad'8.\trill fad'16 r8 re'' la' la' la' si' |
    do''4 do''8 do'' do''4\trill do''8 re'' |
    mi''4. mi'8 si'16\trill si' si' si' mi''8. mi''16 |
    dod''8\trill dod'' r dod''16 dod'' re''4 re''8 dod'' |
    re''4. re''8 si'16\trill si' si' si' do''8. mi''16 |
    la'8\trill la' r do''16 fa'' re''4\trill re''8 mi'' |
    do''2.
  }
  \tag #'vdessus { mi''2\trill r | R1*7 | R2. }
  \tag #'vhaute-contre { sol'2 r | R1*7 | R2. }
  \tag #'vtaille { do'2 r | R1*7 | R2. }
  \tag #'vbasse { do2 r | R1*7 | R2. }
>>
%% Chœur
\choeur
%% La Renommée
<<
  \tag #'(renommee basse) {
    R2. |
    <>^\markup\character La Renommée do''4 sol'4. do''8 |
    la'2\trill la'4 |
    re'' la' re'' |
    si'2.\trill |
    mi''4 si'4. mi''8 |
    dod''2\trill dod''4 |
    re''4. mi''8 dod''4\trill |
    re''2. |
    si'4 do'' re'' |
    mi''2 mi''4 |
    do'' re'' mi'' |
    fa''2 re''4 |
    mi'' re''4.\trill do''8 |
    do''2. |
  }
  \tag #'vdessus { mi''2.\trill | R2.*14 }
  \tag #'vhaute-contre { sol'2. | R2.*14 }
  \tag #'vtaille { do'2. | R2.*14 }
  \tag #'vbasse { do2. | R2.*14 }
>>
%% Chœur
<<
  \tag #'(renommee basse) { \tag #'basse <>^\markup\character Chœur R2.*8 }
  \tag #'vdessus {
    mi''4 do''4. mi''8 |
    la'2 la'4 |
    re''4 la' re'' |
    si'2.\trill |
    mi''4 do'' fa'' |
    mi''2\trill mi''4 |
    sol'' <<
      \new CueVoice {
        s8^\markup\note-by-number #3 #1 #UP s8^\markup\note-by-number #4 #0 #UP
      }
      { fa''8 mi''[ re''8 mi''] | }
    >>
    mi''2.\trill |
  }
  \tag #'vhaute-contre {
    sol'4 mi'4. sol'8 |
    fa'2 do'4 |
    fa' fa' fa' |
    re'2.\trill |
    sol'4 la' la' |
    la'2 la'4 |
    sol' la' sol' |
    sol'2. |
  }
  \tag #'vtaille {
    do'4 do'4. do'8 |
    do'2 la4 |
    la la la |
    sol2. |
    do'4 la re' |
    do'2 do'4 |
    mi' re' si |
    do'2. |
  }
  \tag #'vbasse {
    do'4 do'4. do8 |
    fa2 fa4 |
    re re re |
    sol2. |
    mi4 fa re |
    la2 la4 |
    mi fa sol |
    do2. |
  }
>>
%% La Renommée
<<
  \tag #'(renommee basse) {
    <>^\markup\character La Renommée
    mi''4 do''4. mi''8 |
    la'2\trill la'4 |
    re'' re'' si' |
    sold'2.\trill |
  }
  \tag #'(vdessus vhaute-contre vtaille vbasse) { R2.*4 }
>>
%% Chœur
<<
  \tag #'(renommee basse) { \tag #'basse <>^\markup\character Chœur R2.*8 }
  \tag #'vdessus {
    mi''4 mi'' mi'' |
    do''2 do''4 |
    re'' si' mi'' |
    dod''2.\trill |
    mi''4 mi'' fa'' |
    re''2\trill re''4 |
    re'' re'' dod''\trill |
    re''2. |
  }
  \tag #'vhaute-contre {
    sol'4 sol' sol' |
    la'2 fa'4 |
    fa' mi' mi' |
    mi'2. |
    la'4 la' la' |
    fa'2 fa'4 |
    sol' mi' la' |
    fad'2.\trill |
  }
  \tag #'vtaille {
    do'4 do' do' |
    la2 la4 |
    la la sold\trill |
    la2. |
    dod'4 dod' re' |
    re'2 sib4 |
    sib la la |
    la2. |
  }
  \tag #'vbasse {
    do4 do do |
    fa2 fa4 |
    re mi mi |
    la,2. |
    la4 la fa |
    sib2 sib4 |
    sol la la, |
    re2. |
  }
>>
%% La Renommée
<<
  \tag #'(renommee basse) {
    <>^\markup\character La Renommée
    re''4 la'4. re''8 |
    si'2\trill si'4 |
    do'' re''8[ do''] re''4 |
    mi''2. |
  }
  \tag #'(vdessus vhaute-contre vtaille vbasse) { R2.*4 }
>>
%% Chœur
<<
  \tag #'(renommee basse) { \tag #'basse <>^\markup\character Chœur R2.*4 }
  \tag #'vdessus {
    mi''4 do'' fa'' |
    mi''2\trill mi''4 |
    sol''4 <<
      \new CueVoice {
        s8^\markup\note-by-number #3 #1 #UP s8^\markup\note-by-number #4 #0 #UP
      }
      { fa''8 mi''[ re''8 mi''] | }
    >>
    mi''2.\trill |
  }
  \tag #'vhaute-contre {
    sol'4 la' la' |
    la'2 la'4 |
    sol'4 la' sol' |
    sol'2. |
  }
  \tag #'vtaille {
    do'4 la re' |
    do'2 do'4 |
    mi'4 re' si |
    do'2. |
  }
  \tag #'vbasse {
    do4 fa re |
    la2 la4 |
    mi4 fa sol |
    do2. |
  }
>>
%% La Renommée
<<
  \tag #'(renommee basse) {
    <>^\markup\character La Renommée
    do''4 mi'4. la'8 |
    fad'2 fad'4 |
    sol'4. la'8 fad'4 |
    sol'2. |
    R2.*3
  }
  \tag #'(vdessus vhaute-contre vtaille vbasse) { R2.*7 }
>>
%% Chœur
<<
  \tag #'(renommee basse) { \tag #'basse <>^\markup\character Chœur R2.*4 }
  \tag #'vdessus {
    mi''4 do''4. mi''8 |
    la'2\trill la'4 |
    re'' la' re'' |
    si'2.\trill |
  }
  \tag #'vhaute-contre {
    sol'4 mi'4. sol'8 |
    fa'2 do'4 |
    fa' fa' fa' |
    re'2. |
  }
  \tag #'vtaille {
    do'4 do'4. do'8 |
    do'2 la4 |
    la la la |
    sol2. |
  }
  \tag #'vbasse {
    do'4 do'4. do8 |
    fa2 fa4 |
    re re re |
    sol2. |
  }
>>
%% La Renommée
<<
  \tag #'(renommee basse) {
    <>^\markup\character La Renommée
    si'4 do'' re'' |
    mi''2 mi''4 |
    do'' re'' mi'' |
    fa''2 re''4 |
    mi'' re''4.\trill do''8 |
  }
  \tag #'(vdessus vhaute-contre vtaille vbasse) { R2.*5 }
>>
%% Chœur
<<
  \tag #'(renommee basse) { \tag #'basse <>^\markup\character Chœur do''2. | R2.*21 }
  \tag #'vdessus {
    mi''4 do''4. mi''8 |
    la'2\trill la'4 |
    re'' la' re'' |
    si'2.\trill |
    mi''4 do'' fa'' |
    mi''2\trill mi''4 |
    sol'' <<
      \new CueVoice {
        s8^\markup\note-by-number #3 #1 #UP s8^\markup\note-by-number #4 #0 #UP
      }
      { fa''8 mi''[ re''8 mi''] | }
    >>
    mi''2.\trill |
    R2.*3 |
    mi''4 do'' fa'' |
    mi''2\trill mi''4 |
    sol'' <<
      \new CueVoice {
        s8^\markup\note-by-number #3 #1 #UP s8^\markup\note-by-number #4 #0 #UP
      }
      { fa''8 mi''[ re''8 mi''] | }
    >>
    mi''2.\trill |
    R2.*3 |
    mi''4 do'' fa'' |
    mi''2\trill mi''4 |
    sol'' <<
      \new CueVoice {
        s8^\markup\note-by-number #3 #1 #UP s8^\markup\note-by-number #4 #0 #UP
      }
      { fa''8 mi''[ re''8 mi''] | }
    >>
    mi''2.\trill |
  }
  \tag #'vhaute-contre {
    sol'4 mi'4. sol'8 |
    fa'2 do'4 |
    fa' fa' fa' |
    re'2.\trill |
    sol'4 la' la' |
    la'2 la'4 |
    sol' la' sol' |
    sol'2. |
    R2.*3 |
    sol'4 la' la' |
    la'2 la'4 |
    sol' la' sol' |
    sol'2. |
    R2.*3 |
    sol'4 la' la' |
    la'2 la'4 |
    sol' la' sol' |
    sol'2. |
  }
  \tag #'vtaille {
    do'4 do'4. do'8 |
    do'2 la4 |
    la la la |
    sol2. |
    do'4 la re' |
    do'2 do'4 |
    mi' re' si |
    do'2. |
    R2.*3 |
    do'4 la re' |
    do'2 do'4 |
    mi' re' si |
    do'2. |
    R2.*3 |
    do'4 la re' |
    do'2 do'4 |
    mi' re' si |
    do'2. |
  }
  \tag #'vbasse {
    do'4 do'4. do8 |
    fa2 fa4 |
    re re re |
    sol2. |
    mi4 fa re |
    la2 la4 |
    mi fa sol |
    do2. |
    R2.*3 |
    do4 fa re |
    la2 la4 |
    mi fa sol |
    do2. |
    R2.*3 |
    do4 fa re |
    la2 la4 |
    mi fa sol |
    do2. |
  }
>>
