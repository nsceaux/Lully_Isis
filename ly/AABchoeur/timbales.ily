\clef "basse" do4. do16 do do4 sol, |
do4. do16 do do4 sol, |
do sol, do sol, |
do4. do16 do do4 do |
do4. do16 do do4 do |
do4. do16 do do4 do |
sol,4. sol,16 sol, sol,4 do |
sol,4. sol,16 sol, sol,4 do |
sol, do sol,2 |
do1 |
R1*14 |
do4. do16 do do4 sol, |
do4. do16 do do4 sol, |
do sol, do sol, |
do4. do16 do do4 do |
do4. do16 do do4 do |
do4. do16 do do4 do |
sol,1 |
R1*19 |
do4. do16 do do4 sol, |
do4. do16 do do4 sol, |
do4 sol, do sol, |
do4. do16 do do4 do |
do4. do16 do do4 do |
do4. do16 do do4 do |
sol,4. sol,16 sol, sol,4 do |
sol,4. sol,16 sol, sol,4 do |
sol, do sol,2 |
do1 |
