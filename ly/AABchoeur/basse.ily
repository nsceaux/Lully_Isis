\clef "basse" do4. do16 do do4 do |
do4. do16 do do4 do |
do do do do |
do'4. do'16 do' do'4 do' |
do'4. do'16 do' do'4 do' |
do4. do16 do do4 do |
sol4. sol16 sol sol4 do |
sol4. sol16 sol sol4 do |
sol do sol,2 |
do,2. do8 do |
sol2 sol4 sol |
mi2 mi4 mi |
fa2 fa4. re8 |
la2 la4. fa8 |
do'2 do'4 do |
sol2 sol4 sol8 sol |
do2 do4 do |
fa2 fa4 re |
la2 la4. fa8 |
do'2 do'4. la8 |
re'2 re'4 re |
sol4. sol8 sol4 mi |
la2 la4 fa |
sol1 |
do4. do16 do do4 do |
do4. do16 do do4 do |
do do do do |
do'4. do'16 do' do'4 do' |
do'4. do'16 do' do'4 do' |
do4. do16 do do4 do |
sol2 sol4 sol |
re2 re4 re |
la2 la4 la |
fa2 fa4 fa |
do'2 do4 do |
sol2 sol4 mi |
la2 la4 fa |
do'2 do'4 la |
re'4. re'8 re'4 si |
mi'2 mi'4 do' |
re'1 |
sol2 sol4 sol |
re2 re4 re |
la2 la4 la |
fa2 fa4 fa |
do'2 do4 do |
sol2 sol4 mi |
la2 la4 la |
si2 si4 do' |
sol2 sol, |
do4. do16 do do4 do |
do4. do16 do do4 do |
do4 do do do |
do'4. do'16 do' do'4 do' |
do'4. do'16 do' do'4 do' |
do4. do16 do do4 do |
sol4. sol16 sol sol4 do |
sol4. sol16 sol sol4 do |
sol do sol,2 |
do,1 |
