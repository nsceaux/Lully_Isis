\clef "quinte" sol4. sol16 sol << \sug do'4 \\ sol4 >> << do' \\ \sug sol >> |
sol4. sol16 sol << \sug do'4 \\ sol4 >> << do' \\ \sug sol >> |
sol4. sol16 sol sol4 do' |
do'4. do'16 do' do'4 do' |
do'4. do'16 do' do'4 do' |
do'4. do'16 do' do'4 sol |
sol4. re'16 re' re'4 do' |
re'4. re'16 re' re'4 do' |
re' do' re' sol |
sol2. do'8 do' |
re'2 re'4 re' |
do'2 sol4 sol |
la2 do'4. re'8 |
mi'2 mi'4. fa'8 |
sol'2 do'4 do' |
re'2 re'4 re'8 re' |
do'2 mi'4 mi' |
fa'2 do'4 re' |
mi'2 mi'4. fa'8 |
sol'2 do'4. do'8 |
re'2 re'4 re' |
re'4. sol8 sol4 sol |
la2 mi'4 re' |
re'2 sol |
sol4. sol16 sol do'4 sol |
sol4. sol16 sol << \sug do'4 \\ sol4 >> << do' \\ \sug sol >> |
sol4. sol16 sol sol4 do' |
do'4. do'16 do' do'4 do' |
do'4. do'16 do' do'4 do' |
do'4. do'16 do' do'4 sol |
sol2 sol4 sol |
fa2 la4 re' |
do'2 do'4 la |
do'2 do'4 do' |
do'2 mi'4 do' |
re'2 re'4 sol |
la2 mi'4 fa' |
sol'2 do'4 do' |
re'4. re'8 re'4 re' |
mi'2 mi'4 mi' |
re'2~ re'4. re'8 |
re'2 sol4 sol |
fa2 la4 re' |
do'2 do'4 la |
la2 do'4 do' |
do'2 do'4 do' |
re'2 si4 si |
do'2 la4 la |
re'2 re'4 mi'8 fa' |
sol'2~ sol'4. sol'8 |
sol'2 do'4 sol |
sol4. sol16 sol do'4 sol |
sol4. sol16 sol sol4 do' |
do'4. do'16 do' do'4 do' |
do'4. do'16 do' do'4 do' |
do'4. do'16 do' do'4 sol |
sol4. re'16 re' re'4 do' |
re'4. re'16 re' re'4 do' |
re'4 do' re' sol |
sol1 |
