<<
  \tag #'vdessus {
    \clef "vdessus" R1*9 |
    r2 r4 <>^\markup\character Chœurs mi''8 mi'' |
    re''2 re''4 si' |
    do''2 do''4 mi'' |
    la'2 la'4. si'8 |
    do''2 do''4. re''8 |
    mi''2 mi''4. fa''8 |
    re''2\trill re''4 re''8 re'' |
    mi''2 mi''4 mi'' |
    la'2 la'4 si' |
    do''2 do''4. re''8 |
    mi''2 mi''4. do''8 |
    la'2 la'4 re'' |
    si'4.\trill si'8 si'4. mi''8 |
    do''2 do''4 fa'' |
    re''1\trill |
    do'' |
    R1*5 |
    r2 si'4 dod'' |
    re''2 la'4 si' |
    do''2 do''4 mi'' |
    la'2 la'4 fa'' |
    mi''2\trill mi''4 mi'' |
    re''2 re''4 mi'' |
    do''2 do''4 re'' |
    mi''2 mi''4 do'' |
    la'4.\trill la'8 la'4 re'' |
    si'2 si'4 do'' |
    la'1\trill |
    sol'2 si'4 dod'' |
    re''2 la'4 si' |
    do''2 do''4 mi'' |
    la'2 la'4 fa'' |
    mi''2 mi''4 mi'' |
    re''2\trill re''4 mi'' |
    do''2 fa''4 fa'' |
    fa''2 fa''4 mi'' |
    re''1\trill |
    do'' |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" R1*9 |
    r2 r4 sol'8 sol' |
    sol'2 sol'4 sol' |
    sol'2 sol'4 sol' |
    fa'2 fa'4. fa'8 |
    mi'2 mi'4. fa'8 |
    sol'2 sol'4 sol' |
    sol'2 sol'4 sol'8 sol' |
    sol'2 sol'4 sol' |
    fa'2 fa'4 fa' |
    mi'2 mi'4. fa'8 |
    sol'2 sol'4. la'8 |
    fad'2\trill fad'4 fad' |
    sol'4. sol'8 sol'4. sol'8 |
    mi'2 mi'4 la' |
    sol'1 |
    mi'\trill |
    R1*5 |
    r2 re'4 mi' |
    fa'2 fa'4 fa' |
    mi'2 mi'4 mi' |
    fa'2 fa'4 la' |
    sol'2 sol'4 sol' |
    sol'2 sol'4 sol' |
    mi'2\trill mi'4 la' |
    sol'2 sol'4 la' |
    fad'4. fad'8 fad'4 fad' |
    sol'2 sol'4 sol' |
    sol'2( fad')\trill |
    sol'2 re'4 mi' |
    fa'2 fa'4 fa' |
    mi'2 mi'4 mi' |
    fa'2 fa'4 fa' |
    sol'2 sol'4 sol' |
    sol'2 sol'4 sol' |
    mi'2 la'4 la' |
    sol'2 sol'4 sol' |
    sol'1 |
    mi'\trill |
  }
  \tag #'vtaille {
    \clef "vtaille" R1*9 |
    r2 r4 do'8 do' |
    si2 si4 si |
    mi'2 mi'4 do' |
    do'2 do'4. re'8 |
    do'2 do'4. do'8 |
    do'2 do'4. re'8 |
    si2\trill si4 si8 si |
    do'2 do'4 do' |
    do'2 do'4 re' |
    do'2 do'4. fa'8 |
    mi'2\trill do'4. do'8 |
    re'2 re'4 re' |
    re'4. re'8 re'4 sol |
    la2 do'4 do' |
    do'2( si\trill) |
    do'1 |
    R1*5 |
    r2 sol4 sol |
    la2 re'4 re' |
    do'2 do'4 do' |
    do'2 do'4 do' |
    do'2 do'4 do' |
    si2 si4 si |
    la2 do'4 fa' |
    mi'2 do'4 mi' |
    re'4. re'8 re'4 re' |
    mi'2 mi'4 mi' |
    re'1 |
    si2\trill sol4 sol |
    la2 re'4 re' |
    do'2 do'4 do' |
    do'2 do'4 do' |
    do'2 do'4 do' |
    si2 si4 si |
    la2 do'4 do' |
    re'2 re'4 do' |
    do'2( si)\trill |
    do'1 |
  }
  \tag #'vbasse {
    \clef "vbasse" R1*9 |
    r2 r4 do8 do |
    sol2 sol4 sol |
    mi2 mi4 mi |
    fa2 fa4. re8 |
    la2 la4. fa8 |
    do'2 do'4 do |
    sol2 sol4 sol8 sol |
    do2 do4 do |
    fa2 fa4 re |
    la2 la4. fa8 |
    do'2 do'4. la8 |
    re'2 re'4 re |
    sol4. sol8 sol4 mi |
    la2 la4 fa |
    sol1 |
    do |
    R1*5 |
    r2 sol4 sol |
    re2 re4 re |
    la2 la4 la |
    fa2 fa4 fa |
    do'2 do4 do |
    sol2 sol4 mi |
    la2 la4 fa |
    do'2 do'4 la |
    re'4. re'8 re'4 si |
    mi'2 mi'4 do' |
    re'1 |
    sol2 sol4 sol |
    re2 re4 re |
    la2 la4 la |
    fa2 fa4 fa |
    do'2 do4 do |
    sol2 sol4 mi |
    la2 la4 la |
    si2 si4 do' |
    sol2( sol,) |
    do1 |
  }
>>
R1*9 |
