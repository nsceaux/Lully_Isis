\clef "taille" do'4. do'16 do' << \sugNotes { mi'4 do' } \\ { do'4 sol } >> |
do'4. do'16 do' << \sugNotes { mi'4 do' } \\ { do'4 sol } >> |
<< { do'4 do' mi' do' } \\ \sugNotes { do'4. do'16 do' do'4 do' } >> |
mi' do' sol' sol' |
sol'4. sol'16 sol' sol'4 sol' |
sol'4. sol'16 sol' sol'4 sol' |
sol'4. sol'16 sol' sol'4 sol' |
sol'4. sol'16 sol' sol'4 sol' |
sol' sol' sol'4. fa'8 |
mi'2. sol'8 sol' |
sol'2 re'4 re' |
mi'2 mi'4 do' |
do'2 la4. re'8 |
do'2 do'4. fa'8 |
mi'2\trill sol'4 sol' |
sol'2 sol'4 sol'8 sol' |
sol'2 do'4 do' |
do'2 la4 re' |
do'2 do'4. fa'8 |
mi'2 mi'4. mi'8 |
re'2 re'4 re' |
re'4. re'8 re'4 mi' |
mi'2 do'4 do' |
do'2 si4.\trill do'8 |
do'4. do'16 do' mi'4 do' |
do'4. do'16 do' mi'4 do' |
do'4. do'16 do' do'4 do' |
mi' do' sol' sol' |
sol'4. sol'16 sol' sol'4 sol' |
sol'4. sol'16 sol' sol'4 sol' |
sol'2 re'4 mi' |
fa'2 re'4 re' |
mi'2 do'4 do' |
do'2 do'4 do'8 re' |
mi'2 do'4 do' |
si2 si4 si |
do'2 do'4 fa' |
mi'2 mi'4 mi' |
re'4. re'8 la'4 si' |
sol'2 sol'4 \once\tieDashed la'~ |
la' re' re'4. do'8 |
si2 re'4 mi' |
fa'2 re'4 re' |
mi'2 do'4 do' |
do'2 do'4 do'8 re' |
mi'2 sol'4 sol' |
sol'2 sol'4 sol' |
mi'2 la'4 la' |
sol'2 sol'4 sol' |
sol'2~ sol'4. sol'8 |
mi'4.\trill mi'16 mi' mi'4 do' |
do'4. do'16 do' mi'4 do' |
do'4. do'16 do' do'4 do' |
mi' do' sol' sol' |
sol'4. sol'16 sol' sol'4 sol' |
sol'4. sol'16 sol' sol'4 sol' |
sol'4. sol'16 sol' sol'4 sol' |
sol'4. sol'16 sol' sol'4 sol' |
sol'4 sol' sol'4. fa'8 |
mi'1\trill |
