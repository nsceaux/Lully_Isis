\clef "haute-contre" sol'4. sol'16 sol' sol'4 mi' |
sol'4. sol'16 sol' sol'4 mi' |
sol' mi' sol' mi' |
sol' do'' do'' do'' |
do''4. do''16 do'' do''4 do'' |
do''2 do''4 do'' |
si'4. si'16 si' si'4 do'' |
si'4. si'16 si' si'4 do'' |
si'4 do'' si'4.\trill do''8 |
do''2. do''8 do'' |
si'2 si'4 sol' |
sol'2 sol'4 sol' |
fa'2 fa'4. fa'8 |
mi'2 la'4. la'8 |
sol'2 do''4. re''8 |
si'2\trill si'4 si'8 si' |
do''2 do''4 sol' |
fa'2 fa'4 fa' |
mi'2 mi'4. la'8 |
sol'2 sol'4. la'8 |
fad'2\trill fad'4 fad' |
sol'4. sol'8 sol'4 sol' |
mi'2 mi'4 la' |
\once\tieDashed sol'2~ sol'4. sol'8 |
mi'4. sol'16 sol' sol'4 mi' |
sol'4. sol'16 sol' sol'4 mi' |
sol' mi' sol' mi' |
sol' do'' do'' do'' |
do''4. do''16 do'' do''4 do'' |
do''2 do''4 do'' |
<< \sug si'2 \\ sol'2 >> sol'4 sol' |
la'2 fa'4 fa' |
mi'2 mi'4 mi' |
fa'2 fa'4 la' |
sol'2 sol'4 sol' |
sol'2 sol'4 sol' |
mi'2 mi'4 la' |
sol'2 sol'4 la' |
fad'4. fad'8 fad'4 fad' |
sol'2 sol'4 sol' |
sol'2( fad'4.)\trill sol'8 |
sol'2 sol'4 sol' |
la'2 fa'4 fa' |
mi'2 mi'4 mi' |
fa'2 fa'4 la' |
sol'2 do''4 do'' |
si'2\trill si'4 si' |
la'2 do''4 do'' |
re''2 re''4 do'' |
do''2( si'4.)\trill do''8 |
do''4. sol'16 sol' sol'4 mi' |
sol'4. sol'16 sol' sol'4 mi' |
sol'4 mi' sol' mi' |
sol' do'' do'' do'' |
do''4. do''16 do'' do''4 do'' |
do''2 do''4 do'' |
si'4.\trill si'16 si' si'4 do'' |
si'4.\trill si'16 si' si'4 do'' |
si' do'' si'4.\trill do''8 |
do''1 |
