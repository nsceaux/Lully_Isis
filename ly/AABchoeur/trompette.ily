\clef "dessus"
\twoVoices #'(trompette1 trompette2 trompettes) <<
  { mi''4. mi''16 mi'' mi''4 sol'' |
    mi''4. mi''16 mi'' mi''4 sol'' |
    mi'' sol'' mi'' sol'' |
    mi''8 fa'' sol'' la'' sol'' la'' sol'' la'' |
    sol'' la'' sol'' la'' sol'' la'' sol'' fa'' |
    mi'' fa'' mi'' fa'' sol''4. fa''16 mi'' |
    re''4. re''16 re'' re''4 mi'' |
    re''4. re''16 re'' re''4 mi'' |
    re''4 mi'' re''4.\trill do''8 |
    do''1 }
  { do''4. do''16 do'' do''4 mi'' |
    do''4. do''16 do'' do''4 mi'' |
    do'' mi'' do'' mi'' |
    do''8 re'' mi'' fa'' mi'' fa'' mi'' fa'' |
    mi'' fa'' mi'' fa'' mi'' fa'' mi'' re'' |
    do'' re'' do'' re'' mi''4 re''8 do'' |
    sol'4. sol'16 sol' sol'4 do'' |
    sol'4. sol'16 sol' sol'4 do'' |
    sol' do'' sol'2 |
    do'1 }
>>
R1*14 \allowPageTurn
\twoVoices #'(trompette1 trompette2 trompettes) <<
  { mi''4. mi''16 mi'' mi''4 sol'' |
    mi''4. mi''16 mi'' mi''4 sol'' |
    mi'' sol'' mi'' sol'' |
    mi''8 fa'' sol'' la'' sol'' la'' sol'' la'' |
    sol'' la'' sol'' la'' sol'' la'' sol'' fa'' |
    mi'' fa'' mi'' fa'' sol''4 fa''8 mi'' |
    re''2 }
  { \stopHaraKiri do''4. do''16 do'' do''4 mi'' |
    do''4. do''16 do'' do''4 mi'' |
    do'' mi'' do'' mi'' |
    do''8 re'' mi'' fa'' mi'' fa'' mi'' fa'' |
    mi'' fa'' mi'' fa'' mi'' fa'' mi'' re'' |
    do'' re'' do'' re'' mi''4 re''8 do'' |
    sol'2 \startHaraKiri }
>> r2 | \allowPageTurn
R1*19 |
\twoVoices #'(trompette1 trompette2 trompettes) <<
  { mi''4. mi''16 mi'' mi''4 sol'' |
    mi''4. mi''16 mi'' mi''4 sol'' |
    mi''4 sol'' mi'' sol'' |
    mi''8 fa'' sol'' la'' sol'' la'' sol'' la'' sol'' la'' sol'' la'' sol'' la'' sol'' fa'' |
    mi'' fa'' mi'' fa'' sol''4 fa''8 mi'' |
    re''4.\trill re''16 re'' re''4 mi'' |
    re''4.\trill re''16 re'' re''4 mi'' |
    re'' mi'' re''2\trill |
    do''1 | }
  { \stopHaraKiri do''4. do''16 do'' do''4 mi'' |
    do''4. do''16 do'' do''4 mi'' |
    do''4 mi'' do'' mi'' |
    do''8 re'' mi'' fa'' mi'' fa'' mi'' fa'' |
    mi'' fa'' mi'' fa'' mi'' fa'' mi'' re'' |
    do'' re'' do'' re'' mi''4 re''8 do'' |
    sol'4. sol'16 sol' sol'4 do'' |
    sol'4. sol'16 sol' sol'4 do'' |
    sol' do'' sol'2 |
    do'1 | }
>>
