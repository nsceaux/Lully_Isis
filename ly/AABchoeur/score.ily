\score {
  <<
    \new GrandStaff <<
      \new Staff <<
        <>^"Trompettes" \global \keepWithTag #'trompette1 \includeNotes "trompette"
      >>
      \new Staff << \global \keepWithTag #'trompette2 \includeNotes "trompette" >>
    >>
    \new StaffGroup <<
      \new GrandStaff <<
        \new Staff <<
          <>^"Violons" \global \keepWithTag #'dessus1 \includeNotes "dessus"
        >>
        \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus" >>
      >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*6 s2 \bar "" \pageBreak
        s2 s1*8\pageBreak
        s1*7\pageBreak
        s1*7\pageBreak
        s1*7 s2 \bar "" \pageBreak
        s2 s1*7\pageBreak
        s1*8\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
