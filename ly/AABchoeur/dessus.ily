\clef "dessus"
\twoVoices #'(dessus1 dessus2 dessus) <<
  { mi''4. mi''16 mi'' mi''4 sol'' |
    mi''4. mi''16 mi'' mi''4 sol'' |
    mi'' sol'' mi'' sol'' |
    mi''8 fa'' sol'' la'' sol'' la'' sol'' la'' |
    sol'' la'' sol'' la'' sol'' la'' sol'' fa'' |
    mi'' fa'' mi'' fa'' sol''4. fa''16 mi'' |
    re''4. re''16 re'' re''4 mi'' |
    re''4. re''16 re'' re''4 mi'' |
    re''4 mi'' re''4.\trill do''8 |
    do''2.
  }
  { do''4. do''16 do'' do''4 mi'' |
    do''4. do''16 do'' do''4 mi'' |
    do'' mi'' do'' mi'' |
    do''8 re'' mi'' fa'' mi'' fa'' mi'' fa'' |
    mi'' fa'' mi'' fa'' mi'' fa'' mi'' re'' |
    do'' re'' do'' re'' mi''4 re''8 do'' |
    <<
      \sugNotes {
        re''4. re''16 re'' re''4 mi'' |
        re''4. re''16 re'' re''4 mi'' |
        re''4 mi'' re''4.\trill do''8 |
      } \\
      { sol'4. sol'16 sol' sol'4 do'' |
        sol'4. sol'16 sol' sol'4 do'' |
        sol' do'' sol'2 | }
    >> do''2. \startHaraKiri }
>> mi''8 mi'' |
re''2 re''4 si' |
do''2 do''4 mi''4 |
la'2 << \new CueVoice {
    s4_\markup\note-by-number #2 #1 #UP s_\markup\note-by-number #3 #0 #UP
  }
  { la'4 si' } >> |
do''2 do''4. re''8 |
mi''2 mi''4. fa''8 |
re''2\trill re''4 re''8 re'' |
mi''2 mi''4 mi'' |
la'2 la'4 si' |
do''2 do''4. re''8 |
mi''2 mi''4. do''8 |
la'2 la'4 re'' |
si'4.\trill si'8 si'4. mi''8 |
do''2 do''4 fa'' |
re''2\trill~ re''4.
\twoVoices #'(dessus1 dessus2 dessus) <<
  { << \sug mi''8 \\ do'' >> |
    mi''4. mi''16 mi'' mi''4 sol'' |
    mi''4. mi''16 mi'' mi''4 sol'' |
    mi'' sol'' mi'' sol'' |
    mi''8 fa'' sol'' la'' sol'' la'' sol'' la'' |
    sol'' la'' sol'' la'' sol'' la'' sol'' fa'' |
    mi'' fa'' mi'' fa'' sol''4 fa''8 mi'' |
    re''2 }
  { \stopHaraKiri do''8 |
    do''4. do''16 do'' do''4 mi'' |
    do''4. do''16 do'' do''4 mi'' |
    do'' mi'' do'' mi'' |
    do''8 re'' mi'' fa'' mi'' fa'' mi'' fa'' |
    mi'' fa'' mi'' fa'' mi'' fa'' mi'' re'' |
    do'' re'' do'' re'' mi''4 re''8 do'' |
    << \sug re''2 \\ sol' >> \startHaraKiri }
>> si'4 dod'' |
re''2 la'4 si' |
do''2 do''4 mi'' |
la'2\trill la'4 fa'' |
mi''2\trill mi''4 mi'' |
re''2\trill re''4 mi'' |
do''2 do''4 re'' |
mi''2 mi''4 do'' |
la'4.\trill la'8 la'4 re'' |
si'2\trill si'4 do'' |
la'2\trill~ la'4. sol'8 |
sol'2 si'4 dod'' |
re''2 la'4 si' |
do''2 do''4 mi'' |
la'2 la'4 fa'' |
mi''2 mi''4 mi'' |
re''2\trill re''4 mi'' |
do''2 fa''4 fa'' |
fa''2 fa''4 mi'' |
re''2\trill~ re''4. \twoVoices #'(dessus1 dessus2 dessus) <<
  { mi''8 |
    mi''4. mi''16 mi'' mi''4 sol'' |
    mi''4. mi''16 mi'' mi''4 sol'' |
    mi''4 sol'' mi'' sol'' |
    mi''8 fa'' sol'' la'' sol'' la'' sol'' la'' sol'' la'' sol'' la'' sol'' la'' sol'' fa'' |
    mi'' fa'' mi'' fa'' sol''4 fa''8 mi'' |
    re''4.\trill re''16 re'' re''4 mi'' |
    re''4.\trill re''16 re'' re''4 mi'' |
    re'' mi'' re''2\trill |
    do''1 | }
  { \stopHaraKiri do''8 |
    do''4. do''16 do'' do''4 mi'' |
    do''4. do''16 do'' do''4 mi'' |
    do''4 mi'' do'' mi'' |
    do''8 re'' mi'' fa'' mi'' fa'' mi'' fa'' |
    mi'' fa'' mi'' fa'' mi'' fa'' mi'' re'' |
    do'' re'' do'' re'' mi''4 re''8 do'' |
    <<
      \sugNotes {
        re''4.\trill re''16 re'' re''4 mi'' |
        re''4.\trill re''16 re'' re''4 mi'' |
        re'' mi'' re''2\trill |
        do''1 |
      } \\
      { sol'4. sol'16 sol' sol'4 do'' |
        sol'4. sol'16 sol' sol'4 do'' |
        sol' do'' sol'2 |
        do'1 | }
    >>
  }
>>
