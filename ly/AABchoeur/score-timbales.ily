\score {
  <<
    \new Staff \with {
      \remove "Page_turn_engraver"
      \tinyStaff
      \haraKiriFirst
    } \withLyrics <<
      \global \keepWithTag #'vdessus \includeNotes "voix"
    >> \keepWithTag #'vdessus { \set fontSize = #-2 \includeLyrics "paroles" }
    \new Staff \with {
      \haraKiriFirst
      \tinyStaff
      instrumentName = "Trompette"
    } <<
      \global \keepWithTag #'trompette1 \includeNotes "trompette"
    >>
    \new Staff \with { \haraKiriFirst } <<
      \keepWithTag #(*tag-global*) \global
      \keepWithTag #(*tag-notes*) \includeNotes #(*note-filename*)
      \clef #(*clef*)
      $(if (*instrument-name*)
                      (make-music 'ContextSpeccedMusic
                        'context-type 'Staff
                        'element (make-music 'PropertySet
                                   'value (make-large-markup (*instrument-name*))
                                   'symbol 'instrumentName))
                      (make-music 'Music))
      $(or (*score-extra-music*) (make-music 'Music))
    >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = \largeindent
    ragged-last = #(*score-ragged*)
  }
}
