\key do \major
\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2.
\time 4/4 s1
\time 2/2 \midiTempo#160 s1
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.
\time 3/8 s4.*23
\beginMarkSmall "Symph." s4.*16
\beginMarkSmall "Symph." s4.*35 \bar "||"
\time 2/2 \midiTempo#160 s1*2
\digitTime\time 3/4 \midiTempo#80 s2.
\digitTime\time 2/2 \midiTempo#160 s1
\time 3/8 \midiTempo#80 s4.*65 \bar "|."
