\tag #'(recit basse) {
  Faut- il qu’en vains dis -- cours un si beau jour se pas -- se ?
  Mes Com -- pa -- gnes cou -- rons dans le fond des fo -- rêts,
  voy -- ons qui d’en -- tre- nous se sert mieux de ses traits.
  Cou -- rons __ à la chasse, à la chas -- se.
  Cou -- rons __ à la chasse, à la chas -- se.
}
\tag #'(vdessus basse vdessus2 vhaute-contre1) {
  Cou -- rons __
  \tag #'(vdessus basse vhaute-contre1) { cou -- rons }
  \tag #'vdessus2 { à la chasse, }
  à la chasse, à la chasse, à la chas -- se.
}
\tag #'(vdessus basse vdessus2 vhaute-contre1 vhaute-contre2 vtaille vbasse) {
  \tag #'(vdessus basse vdessus2 vbasse vhaute-contre2) { Cou -- rons __ }
  \tag #'vhaute-contre1 { Cou -- rons à la chasse, à la chasse, }
  \tag #'vhaute-contre2 { à la chasse, à la chasse, }
  \tag #'vtaille { Cou -- rons à la chasse, à la chasse, à la chasse, }
  à la chasse, à la chasse, à la chas -- se.
}

\tag #'(vdessus basse vhaute-contre1 vtaille vbasse) {
  \tag #'(vdessus basse vhaute-contre1 vbasse) { Cou -- rons __ }
  \tag #'vtaille { Cou -- rons à la chasse, à la chasse, }
  \tag #'(vdessus basse) { à la chasse, }
  à la chasse, à la chasse, à la chas -- se.

  \tag #'(vdessus basse vhaute-contre1 vbasse) { Cou -- rons __ }
  \tag #'vtaille { Cou -- rons à la chasse, à la chasse, à la chasse, }
  \tag #'vhaute-contre1 { à la chasse, à la chasse, }
  à la chasse, à la chasse, à la chas -- se.

  À la chasse, à la chasse,
  \tag #'(vhaute-contre1 vtaille) { à la chasse, }
  à la chas -- se.
}
\tag #'(recit basse) {
  Pour -- quoy me sui -- vre de si prés ?
  
  Pour -- quoy fuïr que vous ai -- me ?
  
  Un a -- mant m’em -- ba -- ras -- se.
}
\tag #'(vdessus basse vdessus2 vhaute-contre1) {
  Cou -- rons __
  \tag #'(vdessus basse vhaute-contre1) { cou -- rons }
  \tag #'vdessus2 { à la chasse, }
  à la chasse, à la chasse, à la chas -- se.
}
\tag #'(vdessus basse vdessus2 vhaute-contre1 vhaute-contre2 vtaille vbasse) {
  \tag #'(vdessus basse vdessus2 vbasse vhaute-contre2) { Cou -- rons __ }
  \tag #'vhaute-contre1 { Cou -- rons à la chasse, à la chasse, }
  \tag #'vhaute-contre2 { à la chasse, à la chasse, }
  \tag #'vtaille { Cou -- rons à la chasse, à la chasse, à la chasse, }
  à la chasse, à la chasse, à la chas -- se.
}

\tag #'(vdessus basse vhaute-contre1 vtaille vbasse) {
  \tag #'(vdessus basse vhaute-contre1 vbasse) { Cou -- rons __ }
  \tag #'vtaille { Cou -- rons à la chasse, à la chasse, }
  \tag #'(vdessus basse) { à la chasse, }
  à la chasse, à la chasse, à la chas -- se.

  \tag #'(vdessus basse vhaute-contre1 vbasse) { Cou -- rons __ }
  \tag #'vtaille { Cou -- rons à la chasse, à la chasse, à la chasse, }
  \tag #'vhaute-contre1 { à la chasse, à la chasse, }
  à la chasse, à la chasse, à la chas -- se.

  À la chasse, à la chasse,
  \tag #'(vhaute-contre1 vtaille) { à la chasse, }
  à la chas -- se.
}
\tag #'(recit basse) {
  Je ne puis vous quit -- ter, mon cœur s’at -- tache à vous
  par des nœuds trop forts & trop doux…
  
  Mes com -- pa -- gnes ? Ve -- nez ?… C’est en vain que j’ap -- pel -- le.
  
  E -- cou -- tez, in -- grate, é -- cou -- tez,
  un Dieu char -- mé de vos beau -- tez,
  qui vous jure une a -- mour fi -- del -- le.
  
  Je de -- clare à l’A -- mour u -- ne guerre im -- mor -- tel -- le.
}
