\clef "dessus" R1 R2. R1*4 R2. R4.*9 \allowPageTurn
\setMusic #'choeur {
  R4.*14 |
  r16 sol'' sol'' sol'' mi'' mi'' |
  do'' sol'' sol'' sol'' mi'' mi'' |
  do'' do'' mi'' mi'' sol'' sol'' |
  mi'' mi'' do'' do'' sol' sol' |
  do'' do'' mi'' mi'' sol'' sol'' |
  mi'' mi'' do'' do'' sol' sol' |
  do'' do'' do'' do'' do'' do'' |
  do''4. |
  <>^\markup\italic [Echo] do''16 do'' do'' do'' do'' do'' |
  do''4 r8 |
  R4.*6 | \allowPageTurn
  r16 re'' re'' re'' si' si' |
  sol' re'' re'' re'' si' si' |
  sol' sol'' sol'' sol'' re'' re'' |
  si' sol'' sol'' sol'' re'' re'' |
  si' si' sol' sol' re'' re'' |
  re''4. |
  <>^\markup\italic [Echo] si'16 si' sol' sol' re'' re'' |
  re''4 r8 |
  R4.*7 | \allowPageTurn
  r16 sol' sol' sol' do'' do'' |
  mi'' mi'' do'' do'' sol' sol' |
  do'' do'' mi'' mi'' do'' do'' |
  sol'' sol'' mi'' mi'' do'' do'' |
  sol' sol' sol' sol' sol' sol' |
  sol'4. |
  sol'16 sol' sol' sol' sol' sol' |
  sol'8 sol'16 sol' do'' do'' |
  mi'' mi'' do'' do'' sol'' sol'' |
  mi'' mi'' do'' do'' sol'' sol'' |
  mi'' mi'' do'' do'' sol' sol' |
  do'' do'' do'' do'' do'' do'' |
  do''4 <>^\markup\italic [Echo] sol'16 sol' |
  do'' do'' do'' do'' do'' do'' |
  do''8 r r |
  R4.*5 | \allowPageTurn
}
\keepWithTag #'() \choeur
R1*2 R2. R1 |
\choeur
