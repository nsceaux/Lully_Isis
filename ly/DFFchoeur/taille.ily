\clef "taille" R1 R2. R1*4 R2. R4.*9 \allowPageTurn
\setMusic #'choeur {
  R4.*14 |
  r16 sol' sol' sol' sol' sol' |
  sol' sol' sol' sol' sol' sol' |
  sol' sol' sol' sol' mi' mi' |
  do' sol' sol' sol' mi' mi' |
  do' sol' sol' sol' mi' mi' |
  do' sol' sol' sol' mi' mi' |
  do' do' do' do' do' do' |
  do'4. |
  <>^\markup\italic [Echo] do'16 do' do' do' do' do' |
  do'4 r8 |
  R4.*6 | \allowPageTurn
  r16 sol' sol' sol' re' re' |
  \sug si sol' sol' sol' re' re' |
  si si si si si si |
  si re' re' re' re' re' |
  re' re' si si si si |
  si4. |
  <>^\markup\italic [Echo] re'16 re' si si si si |
  si4 r8 |
  R4.*7 | \allowPageTurn
  r16 sol' sol' sol' mi' mi' |
  do' sol' sol' sol' mi' mi' |
  do' do' sol' sol' mi' mi' |
  <<
    \sugNotes {
      do' do' sol' sol' mi' mi' |
      do' mi' mi' mi' do' do' |
    } \\
    { do' mi' mi' mi' do' do' |
      sol mi' mi' mi' do' do' | }
  >>
  sol mi' mi' mi' do' do' |
  sol mi' mi' mi' do' do' |
  sol mi' mi' mi' do' do' |
  sol sol' sol' sol' mi' mi' |
  do' sol' sol' sol' mi' mi' |
  do' sol' sol' sol' mi' mi' |
  do' sol' sol' sol' sol' sol' |
  sol'4 <>^\markup\whiteout\italic [Echo] sol'16 sol' |
  sol' sol' sol' sol' sol' sol' |
  sol'8 r r |
  R4.*5 | \allowPageTurn
}
\keepWithTag #'() \choeur
R1*2 R2. R1 |
\choeur
