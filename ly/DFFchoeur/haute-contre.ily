\clef "haute-contre" R1 R2. R1*4 R2. R4.*9 \allowPageTurn
\setMusic #'choeur {
  R4.*14 |
  r16 mi'' mi'' mi'' do'' do'' |
  sol' mi'' mi'' mi'' do'' do'' |
  sol' sol' sol' sol' sol' sol' |
  sol' sol' sol' sol' sol' sol' |
  sol'4 r16 sol' |
  sol' sol' sol' sol' sol' sol' |
  mi' mi' mi' mi' mi' mi' |
  mi'4. |
  <>^\markup\italic [Echo] mi'16 mi' mi' mi' mi' mi' |
  mi'4 r8 |
  R4.*6 | \allowPageTurn
  r16 sol' sol' sol' sol' sol' |
  sol'4 r16 sol' |
  sol' sol' re'' re'' si' si' |
  sol' sol' re'' re'' si' si' |
  sol' sol' sol' sol' sol' sol' |
  sol'4. |
  <>^\markup\italic [Echo] sol'16 sol' sol' sol' sol' sol' |
  sol'4 r8 |
  R4.*7 | \allowPageTurn
  r16 mi' mi' mi' mi' mi' |
  mi' mi' mi' mi' mi' mi' |
  mi' sol' sol' sol' sol' sol' |
  sol' do'' do'' do'' sol' sol' |
  mi' mi' mi' mi' mi' mi' |
  mi'4 r16 mi' |
  mi' mi' mi' mi' mi' mi' |
  mi'4 r16 sol' |
  sol' sol' sol' sol' sol' sol' |
  sol' sol' mi' mi' do' do' |
  sol' sol' mi' mi' mi' mi' |
  mi' mi' mi' mi' mi' mi' |
  mi'4 <>^\markup\whiteout\italic [Echo] mi'16 mi' |
  mi' mi' mi' mi' mi' mi' |
  mi'8 r r |
  R4.*5 | \allowPageTurn
}
\keepWithTag #'() \choeur
R1*2 R2. R1 |
\choeur
