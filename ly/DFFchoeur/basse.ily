\clef "basse"
<<
  \tag #'basse {
    R1 R2. R1*4 R2. R4.*15 r8 r
  }
  \tag #'(basse-continue bc-part conducteur) {
    \tag #'bc-part <>^"[B.C.]" do1 |
    si,2 la,4 |
    sol, sol do2 |
    fa4. mi8 re2 |
    la, la |
    fad sol4. do8 |
    sol,2. | \allowPageTurn
    sol4. |
    re8 sol mi |
    la fad re |
    sol4. |
    sol, |
    sol4 sol8 |
    re4 re8 |
    la si do' |
    <<
      \new Voice { \voiceTwo fa8 sol sol, | do4. }
      { \voiceOne s4 r16 \clef "alto" sol'16 |
        do'16 si do' re' mi' fa' | \oneVoice }
    >>
    sol'4 r16 sol' |
    re' do' re' mi' fa' sol' |
    la'8 la' fa' |
    sol' sol' mi' |
    fa' fa' re' |
    sol'4 \clef "bass"
  }
>>
\setMusic #'choeur {
  r16 sol16 |
  do si, do re mi fa |
  sol fa sol la si sol |
  do'8 do' do' |
  do' do' do' |
  do' do' do' |
  \once\tieDashed do'4.~ |
  do' | \allowPageTurn
  do'16 do do do do do |
  do do do do do do |
  do4 r16 do |
  do do do do do do |
  do do do do do do |
  do do do do do do |
  do do do do do do |
  do4. |
  <>^\markup\italic [Echo] do16 do do do do do |
  do4 r16 do |
  do si, do re mi fa |
  sol8 sol sol |
  sol sol sol |
  sol sol sol |
  \once\tieDashed sol4.~ |
  sol |
  sol16 sol sol sol sol sol |
  sol sol sol sol sol sol |
  sol4 r16 sol, |
  sol, sol, sol, sol, sol, sol, |
  sol, sol, sol, sol, sol, sol, |
  sol,4. |
  <>^\markup\italic [Echo] sol,16 sol, sol, sol, sol, sol, |
  sol,4 r16 sol |
  do si, do re mi fa |
  sol fa sol la si sol |
  do'8 do' do' |
  do' do' do' |
  do' do' do' |
  do'4.~ |
  do' |
  do'16 do do do do do |
  do do do do do do |
  do do' do' do' do' do' |
  do' do' do' do' do' do' |
  do' sol sol sol mi mi |
  do sol sol sol mi mi |
  do sol sol sol mi mi |
  do sol sol sol mi mi |
  do do do do do do |
  do4 r16 do |
  do do do do do do |
  do do do do do do |
  do4 <>^\markup\italic [Echo] do16 do |
  do do do do do do |
  do8 do' do' |
  do' do' do' |
  do' do' do' |
  do'4.~ |
  do' |
  do'8 r r |
}
\tag #'bc-part <>^"[Tous]" \keepWithTag #'() \choeur
<<
  \tag #'(basse-continue bc-part conducteur) {
    \tag #'bc-part <>^"[B.C.]"
    do2. si,4 |
    la,1 |
    re2. |
    sol,1*7/8 \clef "alto" sol'8
    do'16 si do' re' mi' fa' |
    sol'4 r16 sol' |
    re' do' re' mi' fa' sol' |
    la'8 la' fa' |
    sol' sol' mi' |
    fa' fa' re' |
    sol'4 \clef "bass"
  }
  \tag #'basse { R1*2 R2. R1 R4.*6 | r8 r }
>>
\tag #'bc-part <>^"[Tous]" \choeur
