\score {
  <<
    \new StaffGroup \with { \haraKiriFirst } <<
      \new Staff << 
        { s1 s2. s1*4 s2. s4.*15 s8 \noHaraKiri }
        \global \includeNotes "dessus"
      >>
      \new Staff <<
        { s1 s2. s1*4 s2. s4.*15 s8 \noHaraKiri }
        \global \includeNotes "haute-contre"
      >>
      \new Staff <<
        { s1 s2. s1*4 s2. s4.*15 s8 \noHaraKiri }
        \global \includeNotes "taille"
      >>
      \new Staff <<
        { s1 s2. s1*4 s2. s4.*15 s8 \noHaraKiri }
        \global \includeNotes "quinte"
      >>
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        { s1 s2. s1*4 s2. s4.*8 s8 \noHaraKiri }
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus2 \includeNotes "voix"
      >> \keepWithTag #'vdessus2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s1 s2. s1*4 s2. s4.*8 s8 \noHaraKiri }
        %\keepWithTag #'hc \includeFiguresInStaff "chiffres"
        \global \keepWithTag #'vhaute-contre1 \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre2 \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s1 s2. s1*4 s2. s4.*15 s8 \noHaraKiri }
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s1 s2. s1*4 s2. s4.*15 s8 \noHaraKiri }
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'recit \includeNotes "voix"
      >> \keepWithTag #'recit \includeLyrics "paroles"
      \new Staff <<
        \global \keepWithTag #'conducteur \includeNotes "basse"
        \keepWithTag #'bc \includeFigures "chiffres"
        \origLayout {
          s1 s2. s1\break s1*3\pageBreak
          s2. s4.*8\break s4.*7\break s4.*8 s8 \bar "" \pageBreak
          s4 s4.*6\break s4.*9 s8 \bar "" \pageBreak
          s4 s4.*7\break s4.*7\pageBreak
          s4.*7\break s4.*7 s8 \bar "" \pageBreak
          s4 s4.*5\break s1*2 s2.\pageBreak
          s1 s4.*6\break s4.*8 s8 \bar "" \pageBreak
          s4 s4.*6\break s4.*9 s8 \bar "" \pageBreak
          s4 s4.*7\break s4.*7\pageBreak
          s4.*7\break s4.*7 s8 \bar "" \pageBreak
        }
        \modVersion {
          s1 s2. s1*4 s2. s4.*61\pageBreak <>^"pb1"
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
