\setMusic #'choeur <<
  \tag #'(vdessus basse) {
    <<
      \tag #'basse { s4 }
      \tag #'vdessus { r8 r }
    >> r16 mi''16 |
    re''[ do'' si' do'' re'' mi'']( |
    fa''4) r16 fa'' |
    mi''8 mi'' fa'' |
    re''8\trill re'' mi'' |
    do'' do'' fa'' |
    re''4.\trill |
    do''4 <>^\markup\right-align\character Tous r16 
    \tag #'vdessus \footnoteHere #'(0 . 0) \markup { Ballard 1719 : \italic mi }
    \sug do''16 |
    si'[ la' si' do'' re'' si']( |
    mi''8) do'' do'' |
    do'' do'' do'' |
    do'' do'' do'' |
    do''4.~ |
    do'' |
    do''8 r r |
    R4.*9 |
    r16 sol'' fa''[ mi'' re'' do'']( |
    si'8) re'' re'' |
    mi'' mi'' mi'' |
    re'' re'' re'' |
    mi'' mi'' mi'' |
    re''4. |
    re''8 r r |
    R4.*7 |
    r8 r r16 do'' |
    si'[ la' si' do'' re'' si']( |
    mi''8) do'' do'' |
    do'' do'' do'' |
    do'' do'' do'' |
    do''4.~ |
    do'' |
    do''8 r r |
    R4.*13 |
    r8 <>^\markup\italic [Echo] do'' do'' |
    do'' do'' do'' |
    do'' do'' do'' |
    do''4.~ |
    do'' |
    do''8 r r |
  }
  \tag #'vdessus2 {
    r8 r r16 do''16 |
    si'[ la' sol' la' si' do'']( |
    re''8) la'8. si'16 |
    do''8 do'' re'' |
    si'\trill si' do'' |
    la' la' re'' |
    si'4.\trill |
    do''4 \sugNotes {
      r16 do'' |
      si'[ la' si' do'' re'' si']( |
      mi''8) do'' do'' |
      do'' do'' do'' |
      do'' do'' do'' |
      do''4.~ |
      do'' |
      do''8
    } r8 r |
    R4.*50 |
  }
  \tag #'vhaute-contre1 {
    do'16[ si do' re' mi' fa']( |
    sol'4) r16 sol' |
    re'[ do' re' mi' fa' sol']( |
    la'8) la' fa' |
    sol' sol' mi' |
    fa' fa' re' |
    sol'4. |
    sol'4
    \footnoteHere #'(0 . 0) \markup {
      Ballard 1719 : \raise#1 \score {
        \new Staff \withLyrics {
          \tinyQuote \clef "petrucci-c3" \time 3/8 \partial 8
          sol'8 | sol' sol' sol' | la' la' la' | sol' sol' sol' |
          sol' sol' sol' | la' la' la' | sol'4. | sol'8
        } \lyrics {
          Cou -- rons à la chasse, à la chasse, 
          à la chasse, à la chasse, à la chas -- se.
        }
        \layout { \quoteLayout }
      }
    } sol'8 |
    sol' sol' sol' |
    \sugNotes {
      sol'8 sol' sol' |
      la' la' la' |
    }
    sol' sol' sol' |
    la' la' la' |
    sol'4. |
    sol'8 r r |
    R4.*8 |
    r8 r r16 sol |
    do'[ re' mi' fa' sol' la']( |
    sol'8) sol' sol' |
    sol' sol' sol' |
    sol' sol' sol' |
    sol'4.~ |
    sol' |
    sol'8 r r |
    R4.*6 |
    r8 r r16 si |
    do'[ re' mi' fa' sol' la']( |
    sol'8) sol' sol' |
    sol' sol' sol' |
    la' la' la' |
    sol' sol' sol' |
    \footnoteHere #'(0 . 0) \markup {
      Ballard 1719 : \raise#1 \score {
        \new Staff \withLyrics {
          \tinyQuote \clef "petrucci-c3" \time 3/8
          sol'4.~ | sol' | sol'8
        } \lyrics { chas -- se. }
        \layout { \quoteLayout }
      }
    }
    \sugNotes { la'8 la' la' | }
    sol'4. |
    sol'8 r r |
    R4.*13 |
    r8 sol' sol' |
    \footnoteHere #'(0 . 0) \markup { Ballard 1719 : \italic sol }
    \sugNotes { la' la' la' } |
    sol' sol' sol' |
    la' la' la' |
    sol'4. |
    sol'8 r r |
  }
  \tag #'vhaute-contre2 {
    R4.*6 |
    r8 r <>^\markup\character Tous \sugNotes {
      r16 si |
      do'[ re' mi' fa' sol' la']( |
      sol'8) sol' sol' |
      sol' sol' sol' |
      la' la' la' |
      sol' sol' sol' |
      la' la' la' |
      sol'4. |
      sol'8
    } r r |
    R4.*50 |
  }
  \tag #'vtaille {
    R4.*6 |
    r8 r r16 re' |
    mi'8 mi' mi' |
    re' re' re' |
    << { \voiceOne mi' \oneVoice } \new Voice { \voiceTwo \sug do' } >> mi' mi' |
    fa' fa' fa' |
    mi' mi' mi' |
    fa' fa' fa' |
    mi'4. |
    mi'8 r r |
    R4.*8 |
    r8 r r16 mi' |
    mi'8 mi' mi' |
    re' si si |
    do' do' do' |
    si si si |
    do' do' do' |
    \footnoteHere #'(0 . 0) \markup { Ballard 1719 : \italic do }
    \sugNotes { si4. | si8 } r r |
    R4.*6 |
    r8 r r16 re' |
    mi'8 mi' mi' |
    re' re' re' |
    << { \voiceOne mi' \oneVoice } \new Voice { \voiceTwo \sug do' } >> mi' mi' |
    fa' fa' fa' |
    mi' mi' mi' |
    fa' fa' fa' |
    mi'4. |
    mi'8 r r |
    R4.*13 |
    r8 mi' mi' |
    \footnoteHere #'(0 . 0) \markup { Ballard 1719 : \italic mi }
    \sugNotes { fa' fa' fa' } |
    mi' mi' mi' |
    fa' fa' fa' |
    mi'4. |
    mi'8 r r |
  }
  \tag #'vbasse {
    R4.*6 |
    r8 r r16 sol16 |
    do[\melisma si, do re mi fa] |
    sol[ fa sol la si sol]( |
    do'8)\melismaEnd do' do' |
    do' do' do' |
    do' do' do' |
    do'4.~ |
    do' |
    do'8 r r |
    R4.*8 |
    r8 r r16 do |
    do[ si, do re mi fa]( |
    sol8) sol sol |
    sol sol sol |
    sol sol sol |
    sol4.~ |
    sol |
    sol8 r r |
    R4.*6 |
    r8 r r16 sol |
    do16[\melisma si, do re mi fa] |
    sol[ fa sol la si sol]( |
    do'8)\melismaEnd do' do' |
    do' do' do' |
    do' do' do' |
    do'4.~ |
    do' |
    do'8 r r |
    R4.*13 |
    r8 do' do' |
    do' do' do' |
    do' do' do' |
    do'4.~ |
    do' |
    do'8 r r |
  }
>>

<<
  \tag #'(recit basse) {
    \clef "vdessus" <>^\markup\character Syrinx
    r4 r8 do'' mi'\trill mi' mi' fa' |
    sol'4 sol'8 la'16 si' do''8. re''16 |
    si'4\trill si'16 r re'' re'' mi''4 mi''8 r16 do'' |
    la'4\trill la'8 sol' fa'4\trill fa'8 mi' |
    mi'4\trill r8 mi'' do''\trill do'' do'' mi'' |
    la'4\trill la'8 re'' si'4\trill si'8 do'' |
    re''2 r8 sol' |
    sol'16[ la' si' do'' re'' mi'']( |
    fa''8) re'' mi'' |
    do'' la' re'' |
    si'4\trill sol'8 |
    r r r16 sol' |
    sol'[ la' si' do'' re'' mi'']( |
    fa''8) la' si' |
    do'' re'' mi'' |
    mi''( re''4)\trill |
    do''4 <<
      \tag #'basse { s8 s4.*64 }
      \tag #'recit { r8 | R4.*64 | }
    >>
    \tag #'recit <>^\markup\character-text Syrinx
    \raise#1 \override #'(baseline-skip . 0) \column\italic {
      revenant sur le Théatre suivie de Pan
    }
    \tag #'basse <>^\markup\character Syrinx
    r8 mi' mi' fa' sol' sol' la' si' |
    do''4
    \ffclef "vbasse" <>^\markup\character Pan
    mi8 mi la4 la8 la |
    fad4\trill fad8
    \ffclef "vdessus" <>^\markup\character Syrinx
    la'16 la' re''8 re''16 re'' |
    si'2\trill si'4 r |
    \tag #'recit R4.*65 |
  }
  \tag #'vdessus \clef "vdessus" 
  \tag #'vdessus2 \clef "vbas-dessus"
  \tag #'vhaute-contre1 \clef "vhaute-contre"
  \tag #'vhaute-contre2 \clef "vhaute-contre"
  \tag #'vtaille \clef "vtaille"
  \tag #'vbasse \clef "vbasse"
  \tag #'basse {
    s1 s2. s1*4 s2. s4.*9
    <<
      { s4 <>^\markup\character Chœur}
      \keepWithTag #'(vdessus basse vdessus2 vhaute-contre1 vhaute-contre2 vtaille vbasse) \choeur
    >>
    s1*2 s2. s1
    <<
      { <>^\markup\character Chœur r8 r }
      \choeur
    >>
  }
  \tag #'(vdessus vdessus2 vhaute-contre1 vhaute-contre2 vtaille vbasse) {
    R1 R2. R1*4 R2. R4.*8
    <<
      \tag #'vdessus { s4 <>^\markup\character Chœur des Nymphes }
      \tag #'(vdessus vdessus2 vhaute-contre2 vtaille vbasse ) R4.
      \tag #'vhaute-contre1 { r8 r r16 sol' | }
    >>
    \choeur
    R1*2 R2.
    <<
      \tag #'vdessus { s2. <>^\markup\character Chœur des Nymphes }
      \tag #'(vdessus vdessus2 vhaute-contre2 vtaille vbasse ) R1
      \tag #'vhaute-contre1 { r2 r4 r8 sol' | }
    >>
    \choeur
  }
>>
