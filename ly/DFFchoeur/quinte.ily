\clef "quinte" R1 R2. R1*4 R2. R4.*9 \allowPageTurn
\setMusic #'choeur {
  R4.*14 |
  r16 do' do' do' sol sol |
  mi do' do' do' sol sol |
  mi mi' mi' mi' do' do' |
  sol mi' mi' mi' do' do' |
  sol mi' mi' mi' do' do' |
  sol mi' mi' mi' do' do' |
  sol sol sol sol sol sol |
  sol4. |
  <>^\markup\italic [Echo] sol16 sol sol sol sol sol |
  sol4 r8 |
  R4.*6 | \allowPageTurn
  r16 si si si sol sol |
  re' si si si sol sol |
  re' re' re' re' re' re' |
  re' si si si si si |
  sol re' re' re' re' re' |
  re'4. |
  <>^\markup\italic [Echo] re'16 re' re' re' re' re' |
  re'4 r8 |
  R4.*7 | \allowPageTurn
  r16 do' do' do' sol sol |
  mi mi sol sol do' do' |
  mi' mi' do' do' sol' sol' |
  mi' mi' do' do' do' do' |
  do' do' do' do' sol sol |
  mi do' do' do' do' do' |
  do' do' do' do' do' do' |
  do' do' do' do' do' do' |
  do' mi' mi' mi' mi' mi' |
  mi'8. sol'16 sol' sol' |
  sol'8. sol16 sol sol |
  sol sol sol sol sol sol |
  sol4 <>^\markup\whiteout\italic [Echo] sol16 sol |
  sol sol sol sol sol sol |
  sol8 r r |
  R4.*5 | \allowPageTurn
}
\keepWithTag #'() \choeur
R1*2 R2. R1 |
\choeur
