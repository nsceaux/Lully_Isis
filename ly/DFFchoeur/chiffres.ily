\simultaneous {
  { s1 <6>2 <6+>4 s1
    s4. <6>8 <7>4 <6> s1 <5/>
    s2. s4.*2 s8 <5/>4 s4.*3 s4 <6>8 s <5/>4
    <7>8 <4> <3> s4.*6
    s4.*2 <"">16*5\figExtOn <"">16\figExtOff s4.*5
    s4.*16
    s4.*15
    s4.*14
    s4.*6
    %%
    <"">2.\figExtOn <"">4\figExtOff s1 <_+>2. s1 }
  { %\bassFigureStaffAlignmentUp
    s1 s2. s1*4 s2. s4.*12 s4 <6 5>8 s4 <6>8 }
}
