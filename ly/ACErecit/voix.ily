\clef "vhaute-contre" <>^\markup\character-text Apollon à la Renommée
R2.*3 |
r4 r8
\footnoteHere #'(0 . 0) \markup {
  Matériel 1677 : \column\italic {
    \line { Ne parlez pas toujours de la Guerre cruelle, }
    \line { Parlons, parlons des plaisirs, & des jeux. }
  }
}
sol sol la si do' |
re'4 re'8 re' mi'4 fad'8 sol' |
fad'4\trill fad'8 re' si4.\trill si8 |
mi'4 do'8 si la4\trill la8 si |
sol4 r8 sol' si\trill si si si |
do'4 do'8 re'16 mi' fa'8 sol' |
mi'2\trill mi'4 mi' |
fa'8 mi' re' do' si4.\trill la8 |
la2 r8 do' do' mi' |
do'4\trill do'8 do' do'4\trill do'8 si |
si4\trill si r8 re' re' re' |
red'\trill red' mi' fad' sol'4 sol' |
mi'4 mi'8 mi' dod'4.\trill re'8 |
re'4. sol8 sol la si do' |
re'4 re'8 re' mi'4 fad'8 sol' |
fad'4\trill fad'8 re' si4.\trill si8 |
mi'4 do'8 si la4\trill la8 si |
sol2. |
