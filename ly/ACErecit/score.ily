\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*3 s1*2\break s1*3 s2 \bar "" \break
        s4 s1*4 s2 \bar "" \break s2 s1*3\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
