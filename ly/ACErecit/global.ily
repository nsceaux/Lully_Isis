\key sol \major
\digitTime\time 3/4 \midiTempo#160 s2.*3
\time 2/2 s1*4
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.
\digitTime\time 2/2 \midiTempo#160 s1*11
\digitTime\time 3/4 s2.
