Ne par -- lons pas tou -- jours de la Guer -- re cru -- el -- le,
par -- lons, par -- lons des plai -- sirs, & des jeux.
Les Mu -- ses, & les Arts vont si -- gna -- ler leur ze -- le,
je vais fa -- vo -- ri -- ser leurs vœux ;
nous pré -- pa -- rons u -- ne fê -- te nou -- vel -- le,
pour le Hé -- ros qui les ap -- pel -- le
dans cet a -- zile heu -- reux :
Ne par -- lons pas tou -- jours de la guer -- re cru -- el -- le,
par -- lons, par -- lons des plai -- sirs, & des jeux.
