\clef "basse" sol4. sol8 re4 |
mi4. mi8 do4 |
re re re, |
sol,2 sol |
fad mi |
re mi |
do re4 re, |
sol,2 sol4 sol8. fa16 |
mi2 si,4 |
do1 |
re2 mi4 mi, |
la,1 |
fad, |
sol,2 sol |
fad mi~ |
mi4. mi8 la4 la, |
re4 sol2 sol4 |
fad2 mi |
re mi |
do re4 re, |
sol,2. |
