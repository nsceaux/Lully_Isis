\tag #'(mercure basse) {
  Le Dieu puis -- sant qui lan -- ce le ton -- ner -- re,
  et qui des cieux tient le Sceptre en ses mains,
  a re -- so -- lu de ve -- nir sur la ter -- re
  chas -- ser les maux qui trou -- blent les hu -- mains :
  Que la terre a -- vec soin à cet hon -- neur ré -- pon -- de ;
  E -- chos, re -- ten -- tis -- sez dans ces lieux pleins d’ap -- pas ;
  an -- non -- cez qu’au -- jour -- d’huy pour le bon- heur du mon -- de,
  Ju -- pi -- ter des -- cend i -- cy bas,
  Ju -- pi -- ter des -- cend i -- cy bas :
  E -- chos, re -- ten -- tis -- sez dans ces lieux pleins d’ap -- pas,
  an -- non -- cez qu’au -- jour -- d’huy pour le bon- heur du mon -- de,
  Ju -- pi -- ter des -- cend i -- cy bas,
  Ju -- pi -- ter des -- cend i -- cy bas.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  E -- chos, re -- ten -- tis -- sez,
  E -- chos, re -- ten -- tis -- sez dans ces lieux pleins d’ap -- pas,
  dans ces lieux pleins d’ap -- pas.
  An -- non -- cez qu’au -- jour -- d’huy pour le bon- heur du mon -- de,
  pour le bon- heur du mon -- de,
  Ju -- pi -- ter des -- cend i -- cy bas,
  \tag #'vdessus { i -- cy bas, }
  Ju -- pi -- ter des -- cend i -- cy bas,
  i -- cy bas,
  Ju -- pi -- ter des -- cend i -- cy bas,
  Ju -- pi -- ter des -- cend i -- cy bas,
  i -- cy bas.
  E -- chos, re -- ten -- tis -- sez dans ces lieux pleins d’ap -- pas,
  dans ces lieux pleins d’ap -- pas,
  an -- non -- cez qu’au -- jour -- d’huy pour le bon- heur du mon -- de,
  pour le bon- heur du mon -- de,
  Ju -- pi -- ter des -- cend i -- cy bas,
  \tag #'vhaute-contre { Ju -- pi -- ter, }
  Ju -- pi -- ter des -- cend i -- cy bas,
  i -- cy bas,
  \tag #'vdessus { Ju -- pi -- ter, }
  Ju -- pi -- ter des -- cend i -- cy bas,
  \tag #'vdessus { i -- cy bas, }
  Ju -- pi -- ter des -- cend i -- cy bas,
  i -- cy bas.
}