\score {
  <<
    \new StaffGroup \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \with { \haraKiri } \withLyrics <<
        \global \keepWithTag #'mercure \includeNotes "voix"
      >> \keepWithTag #'mercure \includeLyrics "paroles"
      \new Staff <<
        \global \keepWithTag #'basse-continue \includeNotes "basse"
        \includeFigures "chiffres"
        \origLayout {
          s1*2 s2. s1\break s1 s2.*2\pageBreak
          s1 s2. s1 s2 \bar "" \break s2 s1 s2. s1\break
          s1*4\break s1 s2. s1\break s1*2\pageBreak
          s1*3 s2 \bar "" \pageBreak
          s2 s1 s2. s1\pageBreak
          s1*4\pageBreak
          s1*3 s2 \bar "" \pageBreak
          s2 s1*2 s2 \bar "" \pageBreak
          s2 s2. s1*2\pageBreak
          s1*3 s2 \bar "" \pageBreak
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
