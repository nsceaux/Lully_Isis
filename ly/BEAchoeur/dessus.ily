\clef "dessus" R1*2 R2. R1*2 R2.*2 R1 R2. R1*3 R2. R1*6 R2. R1*3 |
r4 r8 do'' re'' re'' re'' sol'' |
mi''4\trill r r2 |
r4 do''8 do'' do''4 si'8.\trill do''16 |
do''4 r r2 |
r4 mi''8 mi'' fa''4 fa''8 fa'' |
re''4 re''8 re''16 mib'' fa''8 mib''16 re'' |
do''8 la'16 sib' do''8 do''16 re'' mib''8 mib''16 fa'' sol''8 fa''16 mib'' |
re''8 sib'16 do'' re''8 do''16 sib' la'8 la' sib'8 fad' |
sol'4 la'8 sib' la'4\trill r |
R1 |
r2 r4 re''8 do'' |
sib'8 la' sol'2 sol'8 fad' |
sol'4 r r2 |
R1 |
r4 r8 sib' sol' sol' sol' do'' |
la'4\trill do''8 do'' fa''4 fa''8 mib'' |
re''4 r r2 |
r4 re''8 re'' do''4 do''8 fa'' |
fa''4 re''8 re''16 mib'' fa''8 re'' |
mib''8 sol'16 la' sib'8 sib'16 do'' re''8 re''16 do'' sib'8^\markup\croche-pointee-double sib' |
la' la'16 sib' do''8 sib'16 la' sol'8 sol' do'' mi' |
fa'4 sol'8 la' sol'4 r |
R1 |
r2 r4 do''8 sib' |
la' do''16 do'' fa''8 la' sib'4 sol'8 do'' |
la'4\trill r r2 |
R1*3 |
