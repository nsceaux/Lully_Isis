\clef "haute-contre" R1*2 R2. R1*2 R2.*2 R1 R2. R1*3 R2. R1*6 R2. R1*3 |
r4 r8 la'8 sib' sib' sib' sib' |
sol'4 r r2 |
r4 sol'8 sol' la'4 sol'8 sol' |
sol'4 r r2 |
r4 do''8 do'' do''4 do''8 la' |
sib'4 sib'8 sib'16 sib' sib'8 sib' |
<< \sugNotes { la'8 la'16 la' } \\ { la'8 fa'16 sol' } >> la'8 la'16 si'? do''4 do''8 do'' |
sib'8 sib'16 la' sol'8 sol' fad'8\trill fad' sol' la' |
re'4 re'8 sol' fad'4\trill r |
R1*2 |
sol'4 sol'8 fa' mib'4 re'8 re' |
re'4 r r2 |
R1 |
r4 r8 sol' mi' mi' mi' mi' |
fa'4 la'8 la' sib'4 sib'8 sib' |
sib'4 r r2 |
r4 << \sugNotes { fa'8 sol' } \\ { fa'8 fa' } >> la'4 la'8 la' |
sib'4 sib'8 sib'16 sib' sib'8 sib' |
<< \sugNotes { sol'8 sol'16 sol' } \\ sol'4 >> sol'8 sol'16 la' sib'4 fa'8 fa' |
fa'8 fa'16 fa' fa'8 fa' mi' mi' fa' sol' do'4 do'8 fa' mi'4 r |
R1*2 |
fa'4 fa' fa'4 fa'8 mi' |
fa'4 r r2 |
R1*3 |
