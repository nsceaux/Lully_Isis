\clef "quinte" R1*2 R2. R1*2 R2.*2 R1 R2. R1*3 R2. R1*6 R2. R1*3 |
r4 r8 la sib re' re' re' |
do'4 r r2 |
r4 mi'8 mi' re'4 re'8.\trill do'16 |
do'4 r r2 |
r4 do'8 do' do'4 do'8 do' |
sib4 sib8 sib16 sib sib8 sib |
do'8 do'16 do' do'8 do' do'4 do'8 do' |
re'8 re'16 re' re'8 re' re'4. re'8 |
re'4 re'8 re' re'4 r |
R1*2 |
re'4 sol8 sol la4 la8 la |
sol4 r r2 |
R1 |
r4 r8 sib do' do' do' do' |
do'4 do'8 do' sib4 sib8 sib |
sib4 r r2 |
r4 sib8 sib do'4 do'8 do' |
sib4 re'8 re'16 do' sib8 sib |
sib8 sib16 sib sib8 sib sib4 sib8 sib |
do'4 do'8 do' do' do' do' sib |
la4 sol8 fa sol4 r |
R1*2 |
la8 la sib do' sol4 sol8 sol |
fa4 r r2 |
R1*3 |
