<<
  \tag #'(mercure basse) {
    \clef "vhaute-contre" <>^\markup\character Mercure
    r2 r8 fa la\trill sib |
    do'4. do'8 fa' fa' re'\trill fa' |
    sib4 sib8 re' mi'\trill fa' |
    sol'4 do'8 re' sib4\trill sib8 la |
    la4\trill do'8 do'16 fa' re'8\trill re'16 re' sib8 sib16 re' |
    sol4\trill sol8 sol' re' mi' |
    si\trill sol re' mi' re'8.\trill do'16 |
    do'4 do'8 fa' la4\trill la8 fa |
    do'4 do'8 re'16 mi' fa'8 re' |
    dod'4\trill la8 fa' re'\trill re' la sib |
    do'4 do'8 re' sib4 sib8 la |
    sol4\trill sol8 la sib4 sib8 sol |
    re'4 re'8 re'16 re' mib'8 do' |
    si4\trill si sol'8 fa' mib' re' |
    do'4 si8 do' sol2 |
    sol'8 fa' mib' re' do'4 do'8 si |
    do'4. do'8 fa' fa' fa' la |
    sib4 sib8 do' re'4 do'8 sib |
    la4\trill la8 sib do'4 re'8 mi' |
    fa'4 re'8 re'16 re' sol'8 fa' |
    mi'4\trill do' fa'8 mib' re' do' |
    sib4 sib8 la sol2\trill |
    do'8 re' sib\trill la sol4\trill sol8 la |
    fa4.
  }
  \tag #'vdessus { \clef "vdessus" }
  \tag #'vhaute-contre { \clef "vhaute-contre" }
  \tag #'vtaille { \clef "vtaille" }
  \tag #'vbasse { \clef "vbasse" }
  \tag #'(vdessus vhaute-contre vtaille vbasse) {
    R1*2 R2. R1*2 R2.*2 R1 R2. R1*3 R2. R1*6 R2. R1*3 |
    r4 r8
  }
>>
<<
  \tag #'vdessus {
    <>^\markup\italic [Tous] ^\markup\character-text Chœur Les chants répetez font l’Echo.
    do''8 re'' re'' re'' sol'' |
    mi''4.\trill do''8^\markup\italic [Echo] re'' re'' re'' sol'' |
    mi''4\trill do''8^\markup\italic [Tous] do'' do''4 si'8.\trill do''16 |
    do''4 do''8^\markup\italic [Echo] do'' do''4 si'8.\trill do''16 |
    do''4 mi''8^\markup\italic [Tous] mi'' fa''4 fa''8 fa'' |
    re''4\trill re''8 re''16 mib'' fa''8 re'' |
    do''4\trill do'' mib''8 mib''16 fa'' sol''8 mib'' |
    re''4\trill re'' la'8 la' re'' fad' |
    sol'4 la'8 sib' la'4\trill la'8^\markup\italic [Echo] sol' |
    fad'8 la'16 la' re''8 fad' sol'4 la'8 sib' |
    la'4\trill la'8^\markup\italic [Doux] sib' la'4\trill re''8^\markup\italic [Tous] do'' |
    sib'8 la' sol'2 sol'8 fad' |
    sol'4 re''8^\markup\italic [Echo] do'' sib' la' sol'4~ |
    sol' sol'8 fad' sol'4 sol'8^\markup\italic [Doux] fad' |
    sol'4. sib'8^\markup\italic [Tous] sol'8 sol' sol' do'' |
    la'4\trill do''8 do'' fa''4 fa''8 mib'' |
    re''4\trill do''8^\markup\italic [Echo] do'' fa''4 fa''8 mib'' |
    re''4\trill re''8^\markup\italic [Tous] re'' do''4\trill do''8 fa'' |
    fa''4 re''8 re''16 mib'' fa''8 re'' |
    mib''4 mib'' re''8 re''16 do'' sib'8 sib' |
    la'4\trill la' sol'8 sol' do'' mi' |
    fa'4 sol'8 la' sol'4 r^\markup\italic [Echo] |
    sol'8 sol' do'' mi' fa'4 sol'8 la' |
    sol'4\trill sol'8^\markup\italic [Doux] la' sol'4\trill do''8^\markup\italic [Tous] sib' |
    la' do''16 do'' fa''8 la' sib'4 sol'8 do'' |
    la'4\trill do''8^\markup\italic [Echo] sib' la'\trill do''16 do'' fa''8 la' |
    sib'4 sol'8 do'' la'4\trill sol'8^\markup\italic [Doux] do'' |
    la'4\trill r r2 |
    R1 |
  }
  \tag #'vhaute-contre {
    fa'8 fa' fa' fa' sib' |
    sol'4. fa'8 fa' fa' fa' sib' |
    sol'4 sol'8 sol' fa'4 re'8 sol' |
    mi'4\trill sol'8 sol' fa'4 re'8 sol' |
    mi'4\trill sol'8 sol' la'4 la'8 la' |
    fa'4 fa'8 fa'16 mib' re'8\trill fa' |
    fa'4 fa' sol'8 sol'16 fa' mib'8 sol' |
    sol'4 sol' fad'8 fad' sol' la' |
    re'4 re'8 re' re'4 r |
    r8 fad'16 fad' sol'8 la' re'4 re'8 re' |
    re'4 re'8 re' re'4 r |
    re'8 re' mib' fa' mib'4 re'8 re' |
    re'4 r re'8 re' mib' fa' |
    mib'4\trill re'8 re' re'4 re'8 re' |
    re'4. sol'8 mi'8\trill mi' mi' mi' |
    fa'4 fa'8 do' re'4 fa'8 sol' |
    fa'4 fa'8 do' re'4 fa'8 sol' |
    fa'4 fa'8 fa' fa'4 fa'8 fa' |
    re'4\trill fa'8 fa'16 mib' re'8\trill re' |
    sol'4 sol' fa'8 fa'16 mib' re'8 fa' |
    fa'4 fa' mi'8 mi' fa' sol' |
    do'4 do'8 do' do'4 sol'8 fa' |
    mi'8\trill mi'16 mi' fa'8 sol' do'4 do'8 do' |
    do'4 do'8 do' do'4 r |
    do'8 do' re' mib' re'4 do'8 do' |
    do'4 r do'8 do' re' mib' |
    re'4 do'8 do' do'4 do'8 do' |
    do'4 r r2 |
    R1 |
  }
  \tag #'vtaille {
    la8 sib re' re' re' |
    do'4. la8 sib re' re' re' |
    do'4 do'8 do' la4 sol8 sol |
    sol4 do'8 do' la4 sol8 sol |
    sol4 do'8 do' do'4 do'8 do' |
    sib4 sib8 sib16 sib sib8 sib |
    <<
      { \voiceOne do'4 do' \oneVoice }
      \new Voice \sugNotes { \voiceTwo la4\trill la }
    >> do'8 do'16 do' do'8 do' |
    sib4 sib re'8 re' re' do' |
    sib4 la8 sol fad4\trill r |
    r8 re'16 re' re'8 do' sib4 la8 sol |
    fad4\trill la8 sol fad4\trill r |
    sib8 sib do' re' la4 la8 la |
    sib4 r sib8 sib do' re' |
    la4 la8 la sib4 la8 la |
    sib4. re'8 do'8 do' do' do' |
    do'4 la8 la sib4 sib8 sib |
    sib4 la8 la sib4 sib8 sib |
    sib4 sib8 sib do'4 do'8 do' |
    sib4 sib8 sib16 sib sib8 sib |
    sib4 sib sib8 sib16 sib sib8 re' |
    do'4 do' do'8 do' do' sib |
    la4 sol8 fa mi4\trill r |
    do'8 do' do' sib la4 sol8 fa |
    mi4\trill sol8 fa mi4\trill r |
    la8 la sib do' fa4 fa8 mi |
    fa4 r la8 la sib do' |
    fa4 fa8 mi fa4 fa8 mi |
    fa4 r r2 |
    R1 |
  }
  \tag #'vbasse {
    fa8 sib sib sib sol |
    do'4. fa8 sib sib sib sol |
    do'4 mi8 mi fa4 sol8 sol |
    do4 mi8 mi fa4 sol8 sol |
    do4 do'8 do' la4\trill la8 fa |
    sib4 sib,8 sib,16 do re8 sib, |
    fa4 fa do8 do16 re mib8 do |
    sol4 sol re'8 do' sib la |
    sol4 fad8 sol re4 r |
    re'8 do' sib la sol4 fad8 sol |
    re4 fad8 sol re4 r |
    sol8 fa mib re do4 re8 re |
    sol,4 r sol8 fa mib re |
    do4 re8 re sol,4 re8 re |
    sol,4. sol8 do' do' do' do |
    fa4 fa8 fa re4\trill re8 mib |
    sib,4 fa8 fa re4\trill re8 mib |
    sib,4 sib8 sib la4\trill la8 fa |
    sib4 sib,8 sib,16 do re8 sib, |
    mib4 mib sib,8 sib,16 do re8 sib, |
    fa4 fa do'8 sib la sol |
    fa4 mi8 fa do4 r |
    do'8 sib la sol fa4 mi8 fa |
    do4 mi8 fa do4 r |
    fa8 mib re do sib,4 do8 do |
    fa,4 r fa8 mib re do |
    sib,4 do8 do fa,4 do8 do fa,4 r r2 |
    R1 |
  }
  \tag #'(mercure basse) {
    \tag #'basse <>^\markup\character Chœur r8 r2 |
    R1*4 R2. R1*12 R2. R1*10
  }
>>
