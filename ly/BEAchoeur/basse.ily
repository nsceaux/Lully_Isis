\clef "basse" <<
  \tag #'basse {
    R1*2 R2. R1*2 R2.*2 R1 R2. R1*3 R2. R1*6 R2. R1*3 |
    r4 r8
  }
  \tag #'(basse-continue bc-part) {
    \tag #'bc-part <>^"[B.C.]"
    fa,2. fa4 |
    mi fa la, sib, sol,4. sol8 do'4 |
    sib la sol sol, |
    re la, sib,2 |
    do4. do'8 si do' |
    sol4 si,8 do sol,4 |
    do4 la, fa8. mi16 re4 |
    la,8 sol, fa,8. mi,16 re,4 |
    la, re8. do16 sib,4 fa |
    mi fad sol sol, |
    do8 re16 do sib,8 la, sol,4 sol |
    si,2 do4 |
    sol,4. sol16 la si8 sol do' sib? |
    lab fa sol do si, do sol, sol16 fa |
    mib8 re do sib, lab, fa, sol,4 |
    do8 sol do' sib la sol fa mib |
    re mib re do sib, do16 sib, la,8 sol, |
    re mib16 re do8 sib, la, la16 sol fa8 mi? |
    re8. re16 sol8 fa mi re |
    do8. do16 fa8 mib re do sib, la, |
    sol,8 fa, mi, fa, do, sol, do sib, |
    la, sib, sol, fa, do sib, do do, |
    fa,4.
  }
>> \tag #'bc-part <>^"[Tous]" fa8 sib sib sib sol |
do'4 r r2 |
r4 mi8 mi fa4 sol8 sol |
do4 r r2 |
r4 do'8 do' la4 la8 fa |
sib4 sib,8 sib,16 do re8 sib, |
fa8 fa16 sol la8 fa do' do16 re mib8 do |
sol8 sol16 la sib8 sol re' do' sib la |
sol4 fad8 sol re4 r |
R1*2 |
sol8 fa mib re do4 re8 re |
sol,4 r r2 |
R1 |
r4 r8 sol do' do' do' do |
fa4 fa8 fa re4 re8 mib |
sib,4 r r2 |
r4 sib8 sib la4 la8 fa |
sib4 sib,8 sib,16 do re8 sib, |
mib8 mib16 fa sol8 mib sib sib,16 do re8 sib, |
fa8 fa16 sol la8 fa do' sib la sol |
fa4 mi8 fa do4 r |
R1*2 |
fa8 mib re do sib,4 do8 sib,16 do |
fa,4 r r2 |
<<
  \tag #'basse { R1*3 }
  \tag #'(basse-continue bc-part) {
    R1 |
    r4 \tag #'bc-part <>^"[B.C.]" fa4. mib8 re do |
    sib,1~ |
    \once\set Staff.whichBar = "|"
    \custosNote sib,16
  }
>>
