\clef "taille" R1*2 R2. R1*2 R2.*2 R1 R2. R1*3 R2. R1*6 R2. R1*3 |
r4 r8 fa' fa' fa' fa' sol' |
sol'4 r r2 |
r4 sol'8 sol' fa'4 re'8 sol' |
mi'4\trill r r2 |
r4 sol'8 sol' la'4 fa'8 fa' |
fa'4 fa'8 fa'16 fa' fa'8 fa' |
fa' fa'16 fa' fa'8 fa' sol'4 sol'8 sol' |
sol'4 re'8 re' re' re' re' do' |
sib4 la8 sol la4 r |
R1*2 |
sib8 sib do' si do'4 << { do'8 do' } \\ \sugNotes { la8 la } >> |
sib4 r r2 |
R1 |
r4 r8 re' << { do' do' do' do' } \\ \sugNotes { do'4. do'8  } >> |
do'4 do'8 do' re'4 fa'8 sol' |
fa'4 r r2 |
r4 re'8 re' fa'4 fa'8 fa' |
re'4 fa'8 fa'16 mib' re'8 fa' |
mib'8 mib'16 mib' mib'8 mib' fa' fa'16 mib' re'8 re' |
do'8 do'16 sib la8 do' do'4 do'8 do' |
do'4 do'8 do' do'4 r |
R1*2 |
do'8 do' re' mib' re'4 do'8 do' |
do'4 r r2 |
R1*3 |
