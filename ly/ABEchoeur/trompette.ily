\clef "dessus" r2 |
R1*40 |
r2 r4 \twoVoices #'(trompette1 trompette2 trompette) <<
  { \sug sol''4 |
    mi''8 fa'' mi'' fa'' sol'' la'' sol'' fa'' |
    mi'' fa'' mi'' fa'' sol'' la'' sol'' fa'' |
    mi'' fa'' mi'' fa'' sol''4 fa''8 mi'' |
    re''8 do'' re'' mi'' re''2 | }
  { \sug sol'4 |
    do''8 re'' do'' re'' mi'' fa'' mi'' re'' |
    do'' re'' do'' re'' mi'' fa'' mi'' re'' |
    do'' re'' do'' re'' mi''4 re''8 do'' |
    sol'4 sol'8 sol' sol'2 | }
>>
R1*5 |
r2 r4 \twoVoices #'(trompette1 trompette2 trompette) <<
  { sol''4 |
    mi''8 fa'' mi'' fa'' sol'' la'' sol'' fa'' |
    mi'' fa'' mi'' fa'' sol'' la'' sol'' fa'' |
    mi'' fa'' mi'' fa'' sol''4 fa''8 mi'' |
    re'' mi'' fa'' mi'' re''4.\trill do''8 |
    do''1 | }
  { sol'4 |
    do''8 re'' do'' re'' mi'' fa'' mi'' re'' |
    do'' re'' do'' re'' mi'' fa'' mi'' re'' |
    do'' re'' do'' re'' mi''4 \sugNotes { re''8 do'' } |
    sol'4 do'' sol'4. do''8 |
    do''1 | }
>>
R1*8 |
