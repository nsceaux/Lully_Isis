\clef "basse" do4 do |
fa2 fa4 re |
la2 la4 la |
fa2 fa4 re |
sol2 sol4 sol8 sol |
do'2 do'4 do' |
la2 la4 la |
fa2 fa4 fa |
do2 do4 do |
fa fa fa re |
mi2. mi4 |
la la la4. la8 |
re2. re4 |
sol sol sol fa |
mi2. mi4 |
re do sol4. sol8 |
do2. do4 |
sol8 fa sol la si la si sol |
do'4 do' do' la |
re'2. re'4 |
<< \sugNotes { sol2. sol4 } \\ { sol2 sol4 sol } >> |
do'2. do'4 |
do'1~ |
do'4. do'8 do'4. do'8 |
fa2. fa4 |
sib8 do' sib la sol la sib sol |
la sol la sib la sol fa mi |
re2. do4 |
sib,1 |
la,2. la4 |
re'2. re'4 |
re'1~ |
re'4. re'8 re'4. re'8 |
sol2. sol4 |
do8 si, do re mi re mi do |
fa sol fa mi re mi fa re |
sib4 sib sib sol |
la2. la4 |
<< \sugNotes { re2. re4 } \\ { re2 re4 re } >> |
sol2. sol4 |
sol1~ |
sol4. sol8 sol4. sol8 |
do'4. do'16 do' do'4 do' |
do2 do4 do |
do4. do16 do do4 do |
sol4. sol16 sol sol4 sol |
re8 do re mi fa mi fa re |
la2 la4 la |
fa8 mi fa sol la sol la fa |
do'4 do' do' la |
re'2. re'4 |
sol2 sol4 sol |
do4. do16 do do4 do |
do2 do4 do' |
do'4. do'16 do' do'4 do' |
sol4 do' sol2 |
do8 si, do re mi re mi do |
fa sol fa mi re mi fa re |
sib2 sib4 sol |
la2. la4 |
re2 re4 re |
sol2. sol4 |
sol1~ |
sol4. sol8 sol4. sol8 |
do'1 |
\tag #'basse-continue {
  \digitTime\time 3/4
  \override Staff.TimeSignature.stencil = #ly:time-signature::print
  <>^"Prelude" sol4. sol8 fad sol |
  mib4. mib8 re do |
  re4 re,2 |
  sol,2. |
}
