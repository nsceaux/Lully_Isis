\score {
  <<
    \new Staff \with {
      \remove "Page_turn_engraver"
      \tinyStaff
      \haraKiriFirst
    } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff << \global \keepWithTag #'trompette1 \includeNotes "trompette" >>
      \new Staff << \global \keepWithTag #'trompette2 \includeNotes "trompette" >>
    >>
  >>
  \layout { }
}
