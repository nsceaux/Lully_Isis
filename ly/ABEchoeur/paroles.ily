Ce -- lé -- brons son grand Nom sur la terre & sur l’on -- de,
ce -- lé -- brons son grand Nom sur la terre & sur l’on -- de ;
qu’il ne soit pas bor -- né par les plus vas -- tes mers :
qu’il ne soit pas bor -- né par les plus vas -- tes mers :
\tag #'(vhaute-contre vtaille) { qu’il vo -- le, }
qu’il vo -- le jus -- qu’au bout du mon -- de,
\tag #'(vdessus basse vhaute-contre vtaille) { qu’il du -- re, }
qu’il dure au -- tant que l’u -- ni -- vers.
\tag #'(vdessus basse vhaute-contre vbasse) { Qu’il vo -- le }
\tag #'vtaille { Qu’il vo -- le, qu’il vo -- le }
jus -- qu’au bout du mon -- de,
\tag #'(vdessus basse vhaute-contre) { jus -- qu’au bout du mon -- de, }
\tag #'(vdessus basse vhaute-contre vtaille) { qu’il du -- re, }
qu’il dure au -- tant que l’u -- ni -- vers.
\tag #'(vhaute-contre vbasse) { Qu’il vo -- le }
\tag #'(vdessus basse) { Qu’il vo -- le, qu’il vo -- le }
\tag #'vtaille { Qu’il vo -- le, qu’il vo -- le, qu’il vo -- le }
jus -- qu’au bout du mon -- de,
\tag #'(vdessus basse vhaute-contre vtaille) { qu’il du -- re, }
qu’il dure au -- tant que l’u -- ni -- vers.

Qu’il vo -- le,
\tag #'(vdessus basse vhaute-contre vtaille) { qu’il vo -- le, }
qu’il vo -- le jus -- qu’au bout du mon -- de.

\tag #'(vhaute-contre vbasse) { Qu’il vo -- le }
\tag #'(vdessus basse) { Qu’il vo -- le, qu’il vo -- le }
\tag #'vtaille { Qu’il vo -- le, qu’il vo -- le, qu’il vo -- le }
jus -- qu’au bout du mon -- de ;
\tag #'(vdessus basse vhaute-contre vtaille) { qu’il du -- re, }
qu’il dure au -- tant que l’u -- ni -- vers.
