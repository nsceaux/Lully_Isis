\clef "quinte" sol sol |
fa2 la4 la |
la2 la4 la |
la2 la4 la |
sol2 re'4 re'8 re' |
do'2 do'4 do' |
do'2 do'4 la la2 fa4 fa |
sol2 sol4 mi |
fa do' do' si |
si2. si4 |
la la <<
  \new CueVoice {
    s4^\markup\note-by-number #2 #1 #UP s4^\markup\note-by-number #3 #0 #UP
  }
  { la4 la | }
>>
la2. la4 |
sol sol sol4. sol8 |
sol2. sol4 |
fa mi sol4. sol8 |
sol2. do'4 |
re'2 re'4 sol' |
mi' mi' do' do' |
re'2. re'4 |
re'2 re'4 re' |
do'2. do'4 |
\once\tieDashed do'1~ |
do'4 do' do'4. do'8 |
do'2. do'4 |
re'8 mi' fa' re' mi' fa' mi' re' |
dod'2. dod'4 |
re' la la la |
re'2. sol'4 |
mi'2. mi'4 |
re'2. re'4 |
re'1~ |
re'4. re'8 re'4. re'8 |
re'2. re'4 |
do'4 sol2 sol4 |
la2 la4 la |
sib2 sib4 sib |
la2. la4 |
la2 la4 la |
sol2 sol'4 mi' |
re'2. sol'4 |
sol' sol' sol'4. sol'8 |
sol'4. sol'16 sol' sol'4 sol' |
sol'4. do'16 do' do'4 do' |
do'4. do'16 do' sol4 sol |
sol4. re'16 re' re'4 re' |
re'2. la4 |
la la8 si do'4 do' |
do'2. do'4 do' do' do' do' |
re'2. re'4 |
re'2 re'4 sol |
sol4. do'16 do' do'4 do' |
do'2 do'4 do' |
do'4. do'16 do' do'8 re' mi' << \sug fa' sol' >> |
sol'4 do' re' sol |
sol sol2 sol4 |
la2 la4 la |
sib sib sib sib |
la2. la4 |
la2 la4 la |
sol2 sol'4 mi' |
re'2. sol'4 |
sol' sol' sol'4. sol'8 |
sol'1 |
