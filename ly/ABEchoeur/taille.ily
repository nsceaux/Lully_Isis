\clef "taille" sol' sol' |
la'2 la'4 la' |
la'2 mi'4 do' |
do'2 do'4 fa' |
re'2 re'4 sol'8 sol' |
sol'2 mi'4 mi' |
mi'2 do'4 do' |
do'2 do'4 re' |
mi'2 mi'4 sol' |
fa' fa' fa' fa' |
mi'2. mi'4 |
mi' fa' mi'4. mi'8 |
re'2. re'4 |
re' re' re' re' |
mi'2. mi'4 |
fa' sol' sol'4. sol'8 |
sol'2. mi'4 |
re'8 mi' re' do' si4 si8 re' |
do'4 do' mi' mi' |
re'2. fad'4 |
sol'2 sol'4 sol' |
sol'2 sol'4 fa' |
mi'2.\trill sol'4 |
la'4. sol'8 sol'4.\trill fa'8 |
fa'4 la' la'8 sib' la' sol' |
fa'4 fa' mi' mi' |
mi'2. la'8 sol' |
fa'4 re' re' mi' |
fa'2. mi'4 |
mi'2 mi'4 mi' |
fad'2 fad'4 sol' |
la'2. la'4 |
sol' re' re'4. re'8 |
re'2. sol'4 |
sol'8 la' sol' fa' mi' fa' mi' re' |
do' si do' la fa' sol' fa' mi' |
re'4 re' fa' mi' |
mi'2. mi'4 |
re'2 re'4 fad' |
sol'2 sol4 do' |
si2.\trill re'4 |
do' do' sol'4. sol'8 |
sol'4 sol' do'8 si do' re' |
mi' re' mi' re' do'4 do'8 re' |
mi' re' mi' re' do'4 sol' |
sol'4. sol'16 sol' sol'4 sol' |
la'2 la'4 la' |
la' la2 do'4 |
do'8 re' do' si la si do' re' |
mi'4 mi' mi' mi' |
re'2. re'8 do' |
si2 si4 re' |
do'4 sol do'8 si do' re' |
mi'4 sol' do'8 si do' re' |
mi'4 sol'8 fa' mi'4 sol' |
sol'4. sol'16 sol' sol'4 sol' |
sol'8 la' sol' fa' mi' fa' mi' re' |
do' si do' la fa' sol' fa' mi' |
re'4 re' fa' mi' |
mi'2. mi'4 |
re'2 re'4 fad' |
sol'2 sol4 do' |
si2. re'4 |
do' do' sol'4. sol'8 |
sol'1 |
