\clef "haute-contre" do'' do'' |
do''2 do''4 re'' |
do''2 la'4 mi' |
fa'2 fa'4 fa' |
sol'2 sol'4 si'8 si' |
do''2 sol'4 sol' |
la'2 mi'4 mi' |
fa'2 fa'4 fa' |
mi'2 mi'4 do'' |
la' la' la' si' |
sold'2.\trill sold'4 |
la' la' la'4. la'8 |
fad'2.\trill fad'4 |
sol' sol' sol' sol' |
sol'2. sol'8 la' |
si'4 do'' si'4.\trill do''8 |
do''2. sol'4 |
sol'2 sol'4 sol' |
sol' sol' sol' la' |
fad'2.\trill la'4 |
sol'2 sol'4 si' |
do''2 sol'4 la' |
sib'2. sib'4 |
la'4. sib'8 do''4. do''8 |
la'2. do''4 |
sib'2. sib'4 |
la'2. la'4 |
la'2 la' |
la' la'4 sol' |
la'2 la'4 la' |
la'2 la'4 sol' |
fad'2. fad'4 |
sol'4. la'8 fad'4.\trill sol'8 |
sol'4 si' si'8 do'' re'' si' |
do''2 do''4 sol' |
fa'2 fa'4 fa' |
fa'2 fa'4 sol' |
mi'2. la'4 |
fad'2\trill fad'4 la' |
si'2 si'4 do'' |
re''2. si'4 |
do''4. re''8 si'4.\trill do''8 |
do''4 sol'8 fa' mi' re' mi' fa' |
sol'4 sol'8 fa' mi' re' mi' fa' |
sol'4 do'' do'' do'' |
si'8 la' si' do'' si'4 si' |
re''2 re''4 re'' |
do'' do'8 re' mi' re' mi' do' |
fa'2 fa'4 la' |
sol' sol' sol' do'' |
la'2. la'4 |
sol'2 sol'4 sol' |
sol'8 la' sol' fa' mi' re' mi' fa' |
sol' la' sol' fa' mi' re' mi' fa' |
sol'4 do'' do'' do'' |
si' do'' si'8 do'' re'' si' |
do''2 do''4 sol' |
fa'2 fa'4 fa' |
fa'2 fa'4 sol' |
mi'2.\trill la'4 |
fad'2\trill fad'4 la' |
si'2 si'4 do'' |
re''2. si'4 |
do''4. re''8 si'4.\trill do''8 |
do''1 |
