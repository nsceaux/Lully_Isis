<<
  \tag #'(vdessus basse) {
    \clef "vdessus" mi''4 mi'' |
    fa''2 fa''4 fa'' |
    mi''2 do''4 mi'' |
    la'2 la'4 re'' |
    si'2\trill si'4 re''8 re'' |
    mi''2 mi''4 mi'' |
    do''2 do''4 mi'' |
    la'2\trill la'4 si' |
    do''2 do''4 mi'' |
    do'' do'' do'' re'' |
    si'2.\trill mi''4 |
    dod'' re'' re'' dod'' |
    re''2. re''4 |
    si' si' si' si' |
    do''2. do''4 |
    re'' mi'' re''4. sol''8 |
    mi''2.\trill do''4 |
    si'8[\melisma la' si' do''] re''[ do'' re'' si']( |
    mi''4)\melismaEnd mi'' mi'' do'' |
    la'2. re''4 |
    si'2\trill si'4 re'' |
    mi''2 mi''4 fa'' |
    sol''2. mi''4 |
    fa''4. sol''8 mi''4.\trill fa''8 |
    fa''4 do'' fa''8[ sol'' fa'' mi'']( |
    re''4) re'' sol'' sol'' |
    mi''2.\trill mi''4 |
    fa'' fa'' fa'' mi'' |
    re''2. mi''4 |
    dod''2\trill dod''4 dod'' |
    re''2 la'4 sib' |
    do''2. la'4 |
    sib'4. do''8 la'4. re''8 |
    si'4\trill re'' sol''8[\melisma la'' sol'' fa''] |
    mi''[ fa'' mi'' re''] do''[ re'' do'' sib'?]( |
    la'2)\melismaEnd la'4 fa'' |
    fa''4 fa'' re'' mi'' |
    dod''2.\trill dod''4 |
    re''2 re''4 re'' |
    re''2 re''4 mi'' |
    fa''2. re''4 |
    mi''4. fa''8 re''4.\trill sol''8 |
    mi''1\trill |
    R1*2 |
    r2 r4 re'' |
    fa''2 fa''4 fa'' |
    mi''8[\melisma fa'' mi'' re''] do''[ re'' do'' si']( |
    la'2)\melismaEnd la'4 fa'' |
    mi'' mi'' <<
      \new CueVoice {
        s4^\markup\note-by-number #2 #1 #UP s4^\markup\note-by-number #3 #0 #UP
      }
      { mi'' mi'' | }
    >>
    fad''2. fad''4 |
    sol''2 sol''4 r |
    R1*3 |
    r4 sol'' sol''8[\melisma la'' sol'' fa''] |
    mi''[ fa'' mi'' re''] do''[ re'' do'' sib']( |
    la'2)\melismaEnd la'4 fa'' |
    <<
      \new CueVoice {
        s4^\markup\note-by-number #2 #1 #UP s4^\markup\note-by-number #3 #0 #UP
      }
      { fa''4 fa'' }
    >> re'' mi'' |
    dod''2.\trill dod''4 |
    re''2 re''4 re'' |
    re''2 re''4 mi'' |
    fa''2. re''4 |
    mi''4. fa''8 re''4. sol''8 |
    mi''1\trill |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" sol'4 sol' |
    la'2 la'4 la' |
    la'2 mi'4 mi' |
    fa'2 fa'4 fa' |
    re'2\trill re'4 sol'8 sol' |
    sol'2 sol'4 sol' |
    la'2 mi'4 mi' |
    fa'2 do'4 re' |
    mi'2 mi'4 sol' |
    fa' fa' fa' fa' |
    mi'2. mi'4 |
    mi' fa' mi'4. la'8 |
    fad'2. fad'4 |
    sol' sol' sol' sol' |
    sol'2. sol'4 |
    fa' sol' sol'4. sol'8 |
    sol'2. sol'4 |
    sol'2 sol'4 sol' |
    sol' sol' sol' la' |
    fad'2.\trill fad'4 |
    sol'2 sol'4 sol' |
    sol'2 sol'4 la' |
    sib'2. sol'4 |
    la'4. sib'8 sol'4.\trill la'8 |
    la'4 la' la'8[ sib' la' sol']( |
    fa'4) fa' sib' sib' |
    la'2. la'4 |
    la' la' la' la' |
    la'2. sol'4 |
    la'2 la'4 la' |
    fad'2 fad'4 << \new Voice { \voiceOne \sug sol' } { \voiceTwo fad' \oneVoice } >> |
    la'2. fad'4 |
    sol'4. la'8 fad'4.\trill sol'8 |
    sol'2. sol'4 |
    sol'8[\melisma la' sol' fa'] mi'[ fa' mi' re'] |
    do'[ si do' la] fa'[ sol' fa' mi']( |
    re'4)\melismaEnd re' fa' sol' |
    mi'2. la'4 |
    fad'2\trill fad'4 fad' |
    sol'2 sol'4 mi' |
    si2. sol'4 |
    sol' sol' sol'4. sol'8 |
    sol'1 |
    R1*2 |
    r2 r4 sol' |
    la'2 la'4 re' |
    do'8[\melisma si do' re'] mi'[ re' mi' do']( |
    fa'2)\melismaEnd fa'4 la' |
    sol' sol' sol' la' |
    la'2. re'4 |
    re'2 re'4 r |
    R1*3 |
    r2 r4 sol' |
    sol'8[\melisma la' sol' fa'] mi'[ fa' mi' re'] |
    do'[ si do' la] fa'[ sol' fa' mi']( |
    re'4)\melismaEnd re' fa' sol' |
    mi'2. mi'4 |
    fad'2 fad'4 fad' |
    sol'2 sol'4 mi' |
    si2. sol'4 |
    sol' sol' sol'4. sol'8 |
    sol'1 |
  }
  \tag #'vtaille {
    \clef "vtaille" do'4 do' |
    do'2 do'4 re' |
    do'2 do'4 do' |
    do'2 la4 la |
    sol2 sol4 si8 si |
    do'2 do'4 do' |
    do'2 do'4 do' |
    do'2 la4 fa |
    sol2 sol4 do' |
    la la la si |
    sold2.\trill sold4 |
    la la la4. la8 |
    la2. re'4 |
    re' re' re' re' |
    mi'2. mi'4 |
    si do' si4.\trill do'8 |
    do'2. mi'4 |
    re'8[ mi' re' do']( si4) si8 mi' |
    do'4 do' mi' mi' |
    re'2. re'4 |
    re'2 re'4 si |
    do'2 do'4 fa' |
    mi'2. sol'4 |
    fa'4 do' do'4. do'8 |
    do'2. do'4 |
    re'8[\melisma mi' fa' re'] mi'[ fa' mi' re']( |
    dod'2)\melismaEnd dod'4 dod' |
    re' re' re' mi' |
    fa'2. mi'4 |
    mi'2 mi'4 mi' |
    re'2 re'4 re' |
    re'2. re'4 |
    re'4. re'8 re'4. re'8 |
    re'4 si si8[ do' re' si]( |
    do'2) do'4 sol |
    fa2 fa4 la |
    sib sib sib sib |
    la2. la4 |
    la2 la4 la |
    si2 si4 do' |
    re'2. si4 |
    do'4. re'8 si4.\trill do'8 |
    do'1 |
    R1*2 |
    r2 r4 si |
    re'2 re'4 re' |
    <<
      \new Voice \sugNotes { \voiceTwo mi'4 do' r }
      { \voiceOne mi'2 do'4 \oneVoice }
    >> do'4 |
    do'8[\melisma re' do' si] la[ si do' re']( |
    mi'4)\melismaEnd <<
      { \voiceOne mi' \oneVoice }
      \new Voice { \voiceTwo \sug do' }
    >> do' do' |
    re'2. re'4 |
    si2\trill si4 r |
    R1*3 |
    r4 do' si8[ do' re' si]( |
    do'2) do'4 sol |
    fa2 fa4 la |
    sib sib sib sib |
    la2. la4 |
    la2 la4 la |
    si2 si4 do' |
    re'2. si4 |
    do'4. re'8 si4.\trill do'8 |
    do'1 |
  }
  \tag #'vbasse {
    \clef "vbasse" do4 do |
    fa2 fa4 re |
    la2 la4 la |
    fa2 fa4 re |
    sol2 sol4 sol8 sol |
    do'2 do'4 do' |
    la2 la4 la |
    fa2 fa4 fa |
    do2 do4 do |
    fa fa fa re |
    mi2. mi4 |
    la la la4. la8 |
    re2. re4 |
    sol sol sol fa |
    mi2. mi4 |
    re do sol4. sol8 |
    do2. do4 |
    sol8[\melisma fa sol la] si[ la si sol]( |
    do'4)\melismaEnd do' do' la |
    re'2. re'4 |
    sol2 sol4 sol |
    do'2. do'4 |
    do'1~ |
    do'4. do'8 do'4. do'8 |
    fa2. fa4 |
    sib8[\melisma do' sib la] sol[ la sib sol] |
    la[ sol la sib] la[ sol fa mi]( |
    re4)\melismaEnd re re do |
    sib,2. sib,4 |
    la,2 la,4 la |
    re'2. re'4 |
    re'1~ |
    re'4. re'8 re'4. re'8 |
    sol2. sol4 |
    do8[\melisma si, do re] mi[ re mi do] |
    fa[ sol fa mi] re[ mi fa re]( |
    sib4)\melismaEnd sib sib sol |
    la2. la4 |
    re2 re4 re |
    sol2. sol4 |
    sol1~ |
    sol4. sol8 sol4. sol8 |
    do'1 |
    R1*2 |
    r2 r4 sol |
    re8[\melisma do re mi] fa[ mi fa re]( |
    la2)\melismaEnd la4 la |
    fa8[\melisma mi fa sol] la[ sol la fa]( |
    do'4)\melismaEnd do' do' la |
    re'2. re'4 |
    sol2 sol4 r |
    R1*3 |
    r2 r4 sol |
    do8[\melisma si, do re] mi[ re mi do] |
    fa[ sol fa mi] re[ mi fa re]( |
    sib4)\melismaEnd sib sib sol |
    la2. la4 |
    re2 re4 re |
    sol2. sol4 |
    sol1~ |
    sol4. sol8 sol4. sol8 |
    do'1 |
  }
>>