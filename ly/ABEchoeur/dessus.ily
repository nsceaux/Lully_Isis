\clef "dessus" \tag #'dessus2 \startHaraKiri mi''4 mi'' |
fa''2 fa''4 fa'' |
mi''2 do''4 mi'' |
la'2\trill la'4 re'' |
si'2\trill si'4 re''8 re'' |
mi''2 mi''4 mi'' |
do''2 do''4 mi'' |
la'2 la'4 si' |
do''2 do''4 mi'' |
do'' do'' do'' re'' |
si'2.\trill mi''4 |
dod'' re'' re'' dod'' |
re''2. re''4 |
si' si' si' si' |
do''2. do''4 |
re'' mi'' re''4. sol''8 |
mi''2.\trill do''4 |
si'8 la' si' do'' re'' do'' re'' si' |
mi''4 mi'' mi'' do'' |
la'2. re''4 |
si'2\trill si'4 re'' |
mi''2 mi''4 fa'' |
sol''2. mi''4 |
fa''4. sol''8 mi''4.\trill fa''8 |
fa''4 do'' fa''8 sol'' fa'' mi'' |
re''4 re'' sol'' sol'' |
mi''2.\trill mi''4 |
fa'' fa'' fa'' mi'' |
re''2. mi''4 |
dod''2 dod''4 dod'' |
re''2 la'4 sib' |
do''2. la'4 |
sib'4. do''8 la'4. re''8 |
si'4\trill re'' sol''8 la'' sol'' fa'' |
mi'' fa'' mi'' re'' do'' re'' do''
\tag #'dessus1 \footnoteHere #'(0 . 0) \markup { Matériel 1677 : \italic si♭ }
sib'?8 |
la'2 la'4 fa'' |
fa'' fa'' re'' mi'' |
dod''2.\trill dod''4 |
re''2 re''4 re'' |
re''2 re''4 mi'' |
fa''2. re''4 |
mi''4. fa''8 re''4. sol''8 |
\twoVoices #'(dessus1 dessus2 dessus) <<
  { mi''8 fa'' mi'' fa'' sol'' la'' sol'' fa'' |
    mi'' fa'' mi'' fa'' sol'' la'' sol'' fa'' |
    mi'' fa'' mi'' fa'' sol''4 fa''8 mi'' |
    re'' do'' re'' mi'' re''4 re'' | }
  { \stopHaraKiri do''8 re'' do'' re'' mi'' fa'' mi'' re'' |
    do'' re'' do'' re'' mi'' fa'' mi'' re'' |
    do'' re'' do'' re'' mi''4 re''8 do'' |
    <<
      \sugNotes { re''8 do'' re'' mi'' re''4 re'' } \\
      { sol'4 sol'8 sol' sol'2 | }
    >>
  }
>>
fa''2 fa''4 fa'' |
mi''8 fa'' mi'' re'' do'' re'' do'' si' |
la'2 la'4 fa'' |
mi'' mi'' mi'' mi'' |
fad''2.\trill fad''4 |
sol''2. \twoVoices #'(dessus1 dessus2 dessus) <<
  { sol''4 |
    mi''8 fa'' mi'' fa'' sol'' la'' sol'' fa'' |
    mi'' fa'' mi'' fa'' sol'' la'' sol'' fa'' |
    mi''8 fa'' mi'' fa'' sol''4 fa''8 mi'' |
    re'' mi'' fa'' mi'' re''4.\trill do''8 |
    do''2 do''8 re'' do'' sib' | }
  { << \sug si'4 \\ sol'4 >> |
    do''8 re'' do'' re'' mi'' fa'' mi'' re'' |
    do'' re'' do'' re'' mi'' fa'' mi'' re'' |
    do'' re'' do'' re'' mi''4 \once\tieDashed sol''~ |
    <<
      { \voiceOne sol''4 sol'' sol''8 la'' sol'' fa'' |
        mi'' fa'' mi'' re'' << \sugNotes { do''8 re'' do'' sib' } \\ do''2 >> | }
      \new Voice {
        \voiceTwo sol'4 do'' sol'4. do''8 |
        do''1 |
      }
    >>
  }
>>
la'2 la'4 fa'' |
fa'' fa'' re'' mi'' |
dod''2.\trill dod''4 |
re''2 re''4 re'' |
re''2 re''4 mi'' |
fa''2. re''4 |
mi''4. fa''8 re''4.\trill do''8 |
do''1 |
