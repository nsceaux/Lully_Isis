\clef "bass" r2 |
R1*40 |
r2 r4 sol, |
do4. do16 do do4 do |
do2 do4 do |
do4. do16 do do4 do |
sol,4. sol,16 sol, sol,2 |
R1*5 |
r2 r4 sol, |
do4. do16 do do4 do |
do2 do4 do |
do4. do16 do do4 do |
sol, do sol,2 |
do1 |
R1*4 |
sol,2. sol,4 |
sol,1~ |
sol,4. sol,8 sol,4. sol,8 |
do1 |
