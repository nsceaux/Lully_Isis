<<
  \tag #'vdessus1 {
    \clef "vdessus" <>^\markup\character Chœur do''8 do'' |
    la'8. la'16 re''8 |
    si'8.\trill si'16 si'8 |
    do''8. do''16 do''8 |
    do''8. do''16 si'8\trill |
    do''8 sol' la' |
    sib' re'' mi'' |
    fa'' fa'' fa'' |
    fa''8. fa''16 mib''8 |
    re''8. do''16 sib'8 |
    sol'8. la'16 sib'8 |
    la'4\trill do''8 |
    re'' re'' re'' |
    sib'4 mib''8 |
    do''\trill do'' fa'' |
    mib''16[ re''] do''8.\trill sib'16 |
    sib'8 re'' re'' |
    mi''4 do''8 |
    la' la' re'' |
    sib'4 do''8 |
    la'\trill la' re'' |
    do''16[ sib'] la'8.\trill sol'16 |
    sol'8 si' si' |
    do''4 mi''8 |
    fa'' fa'' fa'' |
    re''4 sol''8 |
    mi''8\trill mi'' fa'' |
    fa'' mi''8.\trill fa''16 |
    fa''8 fa'' fa'' |
    fa'' mi'' mi'' |
    mi'' dod'' dod'' |
    re''8. re''16 re''8 |
    re''8. re''16 dod''8 |
    re''8 la' la' |
    sib' sib' sib' |
    sol' sol' sol' |
    la' do'' re'' |
    sib' sib' do'' |
    la'8.\trill la'16 re''8 |
    sol'8. la'16 sib'8 |
    la' do'' re'' |
    sib'8. sib'16 do''8 |
    la'8. la'16 sib'8 |
    sol'8.\trill la'16 sib'8 |
    la'2 r |
  }
  \tag #'vdessus2 {
    \clef "vbas-dessus" do''8 do'' |
    la'8. la'16 la'8 |
    sol'8 sol' sol' |
    la'8. la'16 la'8 |
    sol' sol' sol' |
    sol' \sugNotes { sol'8 la' | sib' } sib' sib' |
    la' \sugNotes {
      fa''8 fa'' |
      fa''8. fa''16 mib''8 |
      re''8. do''16 sib'8 |
      sol'8. la'16 sib'8 |
      la'4\trill
    } la'8 |
    sib' sib' sib' |
    sol'4 do''8 |
    la'\trill la' re'' |
    do''16[ sib'] la'8.\trill sib'16 |
    sib'8 \sugNotes {
      re'' re'' |
      mi''4 
    } mi'8 |
    fad' fad' fad' |
    sol'4 la'8 |
    fad'\trill fad' sib' |
    la'16[ sol'] fad'8.\trill sol'16 |
    sol'8 \sugNotes {
      si'8 si' |
      do''4 mi''8 |
      fa'' fa'' fa'' |
      re''4 sol''8 |
      mi''8\trill mi'' fa'' |
      fa'' mi''8.\trill fa''16 |
      fa''8
    } la'8 la' |
    si' si' si' |
    dod'' \sugNotes {
      dod''8 dod'' |
      re''8. re''16 re''8 |
      re''8. re''16 dod''8 |
      re''8 
    } fad' fad' |
    sol' sol' sol' |
    mi'\trill \sugNotes {
      sol' sol' |
      la'
    } la'8 sib' |
    sol' sol' la' |
    fa'8. fa'16 fa'8 |
    \footnoteHere #'(0 . 0) \markup {
      Ballard 1719 : \raise#1 \score {
        \new Staff \withLyrics {
          \tinyQuote \time 3/8 \clef "petrucci-c1" \key fa \major
          mi'8. fa'16 fa'8 | fa'
        } \lyrics { té. Li -- ber -- té. }
        \layout { \quoteLayout }
      }
    }
    \sug fa'8. fa'16 \sug mi'8 |
    fa' \sugNotes {
      do''8 re'' |
      sib'8. sib'16 do''8 |
      la'8. la'16 sib'8 |
      sol'8.\trill la'16 sib'8 |
      la'2 r |
    }
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" mi'8 mi' |
    fa' fa' fa' |
    sol' sol' sol' |
    mi'8. mi'16 fa'8 |
    re'8. mi'16 fa'8 |
    mi'\trill mi' fad' |
    sol' sol' sol' |
    re' la' la' |
    la'8. la'16 la'8 |
    fa'8. fa'16 sol'8 |
    mi'8.\trill re'16 mi'8 |
    fa'4 fa'8 |
    re' re' re' |
    mib'?4 do'8 |
    fa' fa' re' |
    mib' fa' fa' |
    fa'8. fa'16 sol'8 |
    sol'4 do'8 |
    re' re' re' |
    mib'4 do'8 |
    \footnoteHere #'(0 . 0) \markup {
      Ballard 1719 : \raise#1 \score {
        \new Staff \withLyrics {
          \tinyQuote \time 3/8 \clef "petrucci-c3" \partial 8 \key fa \major
          do'8 | fa' fa' sib
        } \lyrics { au mon -- de, c’est }
        \layout { \quoteLayout }
      }
    }
    \sugNotes { re' re' } sib |
    do' re' re' |
    re' sol' sol' |
    sol'4 sol'8 |
    la' la' la' |
    fa'4 sib'8 |
    sol'\trill sol' la' |
    sol' sol'8.\trill fa'16 |
    fa'8 fa' fa' |
    sol' sol' sol' |
    la' la' la' |
    fa' fa' sol' |
    mi'8. fad'16 sol'8 |
    fad' re' re' |
    sol sol sol |
    do' mi' mi' |
    fa' fa' re' |
    mib' mib' do' |
    re'8. do'16 sib8 |
    do'8. do'16 do'8 |
    do' fa' fa' |
    sol'8. sol'16 la'8 |
    fa'8. fa'16 fa'8 |
    fa'8. fa'16 mi'8\trill |
    fa'2 r |
  }
  \tag #'vtaille {
    \clef "vtaille" do'8 do' |
    do'8. do'16 re'8 |
    re'8. re'16 mi'8 |
    do'8. si16 la8 |
    sol8. sol16 sol8 |
    sol8 do' do' |
    re' r r |
    r re' re' |
    do' do' do' |
    sib8. do'16 re'8 |
    do'8. do'16 do'8 |
    do'4 r8 |
    R4.*3 |
    r8 la la |
    sib8. sib16 sib8 |
    do'4 r8 |
    R4.*3 |
    r8 re' re' |
    re' re' re' |
    mi'4 do'8 |
    do' do' do' |
    re'4 re'8 |
    do' do' do' |
    re' do'8. sib16 |
    la8\trill r r |
    R4. |
    r8 mi' mi' |
    re' sib sib |
    la8. la16 la8 |
    la r r |
    R4. |
    r8 do' do' |
    do' r r |
    R4.*2 |
    r8 do'8 do' |
    do' do' sib |
    sib8. sib16 la8 |
    la re' re' |
    do'8. do'16 do'8 |
    do'2 r |
  }
  \tag #'vbasse {
    \clef "vbasse" do8 do |
    fa8. mi16 re8 |
    sol8. fa16 mi8 |
    la8. sol16 fa8 |
    sol8. fa16 sol8 |
    do8 do' do' |
    sol r r |
    r re' re' |
    la8. la16 la8 |
    sib8. la16 sol8 |
    do'8. sib16 do'8 |
    fa4 r8 |
    R4.*3 |
    r8 fa fa |
    sib sib sol |
    do'4 r8 |
    R4.*3 |
    r8 re re |
    sol sol sol |
    do4 do'8 |
    la la la |
    sib4 sol8 |
    do' do' la |
    sib do'16[ sib] do'8 |
    fa r r |
    R4. |
    r8 la la |
    sib sib sol |
    la8. sol16 la8 |
    re r r |
    R4. |
    r8 do do |
    fa r r |
    R4.*2 |
    r8 do do |
    fa fa re |
    mib8. re16 do8 |
    re8. do16 sib,8 |
    do8. sib,16 do8 |
    fa,2 r |
  }
>>
