\clef "quinte" do'8 do' |
do'8. do'16 re'8 |
re'8. re'16 mi'8 |
do'4 re'8 |
re'4 re'8 |
do' do' do' |
sib r r |
r re' re' |
do' do' do' |
sib8. sib16 sib8 |
do'8. do'16 do'8 |
do'4 r8 |
R4.*3 |
r8 do' do' |
re' sib sib |
do'4 r8 |
R4.*3 |
r8 re' re' |
re' re' re' |
do'4 do'8 |
do' do' do' |
re'4 re'8 |
do' do' do' |
re'8 do'8.\trill sib16 |
la8\trill r r |
R4. |
r8 la la |
re' re' sib |
la8. la16 la8 |
la r r |
R4. |
r8 do' do' |
do' r r |
R4.*2 |
r8 do' do' |
do' do' sib |
sib8. sib16 la8 |
la8. la16 sol8 |
sol8. sol16 sol8 |
fa2 r |
