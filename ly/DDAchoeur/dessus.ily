\clef "dessus" <>^"[Violons]" sol''8 sol'' |
la''8. sol''16 fa'' mi'' re''8 si' si' |
do''8. do''16 do''8 |
do''8. do''16 si'8 |
do'' sol' la' |
sib' r r |
r fa'' fa'' |
fa''8. fa''16 mib''8 |
re''8. re''16 sol''8 |
mi''8.\trill re''16 mi''8 |
fa''4 r8 |
R4.*3 |
r8 fa'' fa'' |
re'' re'' sol'' |
mi''4\trill r8 |
R4.*3 |
r8 la'' la'' |
sol'' sol''16 la'' sol'' fa'' |
mi''4 mi''8 |
fa'' fa'' fa'' |
re''4 sol''8 |
mi'' mi'' fa'' |
fa'' mi''8.\trill fa''16 |
fa''8 r r |
R4. |
r8 dod'' dod'' |
re'' re'' re'' |
re''8. re''16 dod''8 |
re'' r r |
R4. |
r8 sol'' sol'' |
fa'' r r |
R4.*2 |
r8 mi'' mi'' |
fa'' do'' re'' |
sib'8. sib'16 do''8 |
la'8. la'16 sib'8 |
sol'8. la'16 sib'8 |
la'2 r |
