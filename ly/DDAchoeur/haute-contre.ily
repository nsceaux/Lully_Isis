\clef "haute-contre" do''8 do'' |
la'8. la'16 la'8 |
sol'8 sol' sol' |
la'8. la'16 la'8 |
\sugRythme { si'8.[ si'16 si'8] } sol'8 sol' sol' |
sol' mi' fad' |
sol' r r |
r la' la' |
do'' do'' do'' |
fa'8. fa'16 sib'8 |
sol'8. la'16 sib'8 |
la'4\trill r8 |
R4.*3 |
r8 do'' do'' |
sib' re'' re'' |
do''4 r8 |
R4.*3 |
r8 re'' re'' |
si' si' si' |
do''4 do''8 |
do'' do'' do'' |
re''4 re''8 |
do'' do'' do'' |
sib' sol' do'' |
do'' r r |
R4. |
r8 \footnoteHere #'(0 . 0) \markup {
  Ballard 1719 : \raise#1 \score {
    { \tinyQuote \clef "petrucci-c1" \time 3/8 \key fa \major
      r8 do'' do'' | fa' fa' sol' }
    \layout { \quoteLayout }
  }
} \sugNotes { la' la' } |
fa' fa' sol' |
mi'8. fad'?16 sol'8 |
fad'8\trill r r |
R4. |
r8 do'' do'' |
la' r r |
R4.*2 |
r8 do'' do'' |
do'' la' sib' |
sol'8. sol'16 la'8 |
fa'8. fa'16 fa'8 fa'8. fa'16 mi'8 |
fa'2 r |
