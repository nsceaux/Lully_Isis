\clef "basse" \tag #'(basse bc-part) <>^"[Tous]" do8 do |
fa8. mi16 re8 |
sol8. fa16 mi8 |
la8. sol16 fa8 |
sol8. fa16 sol8 |
do do' do' |
sol <<
  \tag #'conducteur { r8 r | r }
  \tag #'(basse basse-continue bc-part) {
    \tag #'(basse bc-part) <>^"[B.C.]" \clef "alto" sol'8[ sol'] | re' \clef "bass"
  }
>> \tag #'(basse bc-part) <>^"[Tous]" re'8[ re'] |
la la la |
sib8. la16 sol8 |
do'8. sib16 do'8 |
fa4 <<
  \tag #'conducteur { r8 | R4.*3 | r8 }
  \tag #'(basse basse-continue bc-part) {
    \tag #'(basse bc-part) <>^"[B.C.]" \clef "alto" fa'8 |
    re' re' re' |
    mi'4 do'8 |
    fa' fa' re' |
    mib' \clef "bass"
  }
>> \tag #'(basse bc-part) <>^"[Tous]" fa8[ fa] |
sib sib sol |
do'4 <<
  \tag #'conducteur { r8 | R4.*3 | r8 }
  \tag #'(basse basse-continue bc-part) {
    \tag #'(basse bc-part) <>^"[B.C.]" \clef "alto" do'8 |
    re' re' re' |
    mib'4 do'8 |
    \sugNotes { re' re' } sib |
    do' \clef "bass"
  }
>> \tag #'(basse bc-part) <>^"[Tous]" re8[ re] |
sol sol sol |
do4 do'8 |
la la la |
sib4 sol8 |
do' do' la |
sib do' do |
fa <<
  \tag #'conducteur { r8 r | R4. | r8 }
  \tag #'(basse basse-continue bc-part) {
    \tag #'(basse bc-part) <>^"[B.C.]" \clef "alto" fa'8[ fa'] |
    sol' sol' sol' |
    la' \clef "bass"
  }
>> \tag #'(basse bc-part) <>^"[Tous]" la8[ la] |
sib sib sol |
la8. sol16 la8 |
re <<
  \tag #'conducteur { r8 r | R4. | r8 }
  \tag #'(basse basse-continue bc-part) {
    \tag #'(basse bc-part) <>^"[B.C.]" \clef "alto" re'8[ re'] |
    sol sol sol |
    do' \clef "bass"
  }
>> \tag #'(basse bc-part) <>^"[Tous]" do8[ do] |
fa <<
  \tag #'conducteur { r8 r | R4.*2 | r8 }
  \tag #'(basse basse-continue bc-part) {
    \tag #'(basse bc-part) <>^"[B.C.]" \clef "alto" fa'8[ re'] |
    mib' mib' do' |
    re'8. do'16 sib8 |
    do'8 \clef "bass"
  }
>> \tag #'(basse bc-part) <>^"[Tous]" do8[ do] |
fa fa re |
mib8. re16 do8 |
re8. do16 sib,8 |
do8. sib,16 do8 |
<<
  \tag #'basse { fa2 r }
  \tag #'(basse-continue bc-part conducteur) {
    fa2. \tag #'bc-part <>^"[B.C]" mi4 |
    \once\set Staff.whichBar = "|"
  }
>>
