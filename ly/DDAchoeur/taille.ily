\clef "taille" sol'8 sol' |
fa'8. fa'16 fa'8 |
sol'8 sol' sol' |
mi'8 mi' fa' |
re'8. mi'16 fa'8 |
mi' do' do' |
re' r r |
r fa' fa' |
fa' do'4 |
re'8. re'16 re'8 |
do'8. do'16 do'8 |
do'4 r8 |
R4.*3 |
r8 fa' fa' |
fa' fa' sib' |
sol'4\trill r8 |
R4.*3 |
r8 fad' fad' |
sol' sol' sol' |
sol'4 sol'8 |
la' la' la' |
fa'4 sib'8 |
sol' sol' la' |
sol' sol'8.\trill fa'16 |
fa'8 r r |
R4. |
r8 mi' mi' |
re' re' mi' |
mi'8. mi'16 mi'8 |
re' r r |
R4. |
r8 mi' mi' |
fa' r r |
R4.*2 |
r8 sol' sol' |
fa' fa' fa' |
mib'8. mib'16 mib'8 |
re'8. re'16 re'8 |
do'8. do'16 do'8 |
do'2 r |
