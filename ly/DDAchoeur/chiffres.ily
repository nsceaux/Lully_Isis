\simultaneous {
  { s4 s4. <_+>4\figExtOn <_+>8\figExtOff s4 <6>8 <4>4 <_+>8 s4
    <6+>8 s4.*2 <6>4 <5/>8 <"">4\figExtOn <"">8\figExtOff <3>4\figExtOn <3>8\figExtOff s4.*10
    s8 <_+>4 <_+>4. s <6> s s4 <6>8 <6 5> <3>4 s4.*2
    s8 s4 s <6>8 <4>4 <_+>8 <_+>4. s4.*5
    s8 <3>4 s <6>8 s4 <6>8 s4 <6 5>8 <4>4 <3>8 }
  { %\bassFigureStaffAlignmentUp
    s4 s4.*19
    s4 <6>8 <6 5 _-> s4 s4.*7
    <7>8 <6>4 <_+>4. s4.*2 s8 s4 s4.*2 s4 <6>8 s4 <6>8 <3>4. }
}
