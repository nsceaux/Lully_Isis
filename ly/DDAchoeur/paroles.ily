Li -- ber -- té, li -- ber -- té, li -- ber -- té, li -- ber -- té,
li -- ber -- té, li -- ber -- té,
\tag #'(vdessus1 vdessus2 vhaute-contre) { li -- ber -- té, }
li -- ber -- té,
li -- ber -- té, li -- ber -- té, li -- ber -- té.
\tag #'(vdessus1 vdessus2 vhaute-contre) {
  S’il est quel -- que bien au mon -- de,
  c’est la li -- ber -- té, li -- ber -- té.
}
\tag #'(vtaille vbasse) { Li -- ber -- té, li -- ber -- té. }
\tag #'(vdessus1 vdessus2 vhaute-contre) {
  S’il est quel -- que bien au mon -- de,
  c’est la li -- ber -- té, li -- ber -- té.
}
\tag #'(vtaille vbasse) { Li -- ber -- té, li -- ber -- té. }
S’il est quel -- que bien au mon -- de,
c’est la li -- ber -- té.
\tag #'(vdessus1 vdessus2 vhaute-contre) {
  Li -- ber -- té, li -- ber -- té.
}
Li -- ber -- té, li -- ber -- té, li -- ber -- té.
\tag #'(vdessus1 vdessus2 vhaute-contre) {
  Li -- ber -- té, li -- ber -- té.
}
Li -- ber -- té.
\tag #'(vdessus1 vdessus2 vhaute-contre) {
  Li -- ber -- té, li -- ber -- té, li -- ber -- té,
}
Li -- ber -- té,
li -- ber -- té, li -- ber -- té, li -- ber -- té, li -- ber -- té.

