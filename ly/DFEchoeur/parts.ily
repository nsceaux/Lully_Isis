\piecePartSpecs
#`((dessus #:score "score-voix")
   (haute-contre #:score "score-voix")
   (taille #:score "score-voix")
   (quinte #:score "score-voix")
   (basse #:score "score-voix")
   (basse-continue #:score "score-voix" #:tag-notes bc-part))
