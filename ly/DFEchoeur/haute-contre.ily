\clef "haute-contre"
\setMusic #'choeur {
  do'' do'' sol' |
  la'2. |
  R2.*2 |
  do''4 do'' re'' |
  si'2.\trill |
  R2. |
  r4 r sol' |
  la'2 la'4 |
  fa' fa' fa' |
  sol' sol' sol' |
  mi'2\trill mi'4 |
  fa' fa' fa' |
  sol'2 fa'4 |
  mi'2 la'4 |
  fad'2\trill fad'4 |
  R2.*3 |
  si'4 do'' re'' |
  sol'2. |
  R2.*2 |
  fa'4 fa' fa' |
  mi'2. |
  R2. |
  r4 r do'' |
  do''2 si'4 |
  sold' sold' sold' |
  la' la' si' |
  sold'2\trill sold'4 |
  do'' do'' do'' |
  la'2 la'4 |
  la'2 sold'4 |
  la'2. |
  R2.*3 |
  mi'4 mi' fad' |
  sol'2. |
  R2. |
  fa'4 fa' sol' |
  la'2. |
  R2. |
  do''4 do'' do'' |
  si'2.\trill |
  R2.*2 |
}
\keepWithTag #'() \choeur
R2.*11 |
mi'4 mi' fad' |
sol'2. |
R2. |
fa'4 fa' sol' |
la'2. |
R2. |
do''4 do'' do'' |
si'2.\trill |
R2.*2 |
R2.*15 |
\choeur
