\setMusic #'choeur <<
  \tag #'(choeur1-d1 basse) {
    <<
      \tag #'basse { s2.*2 \ffclef "vdessus" }
      \tag #'choeur1-d1 { \clef "vdessus" <>^\markup\character Chœur de Nymphes R2.*2 }
    >>
    do''4 do'' fa'' |
    mi''2.\trill |    
    <<
      \tag #'basse { s2.*2 \ffclef "vdessus" }
      \tag #'choeur1-d1 R2.*2
    >>
    sol''4 fa''\trill re'' |
    mi''2 <<
      \tag #'basse { s4 s2.*8 \ffclef "vdessus" }
      \tag #'choeur1-d1 { r4 R2.*8 }
    >>
    la'4 la' re'' |
    si'2.\trill |
    mi''4 la'4. re''8 |
    <<
      \tag #'basse { s2.*2 \ffclef "vdessus" }
      \tag #'choeur1-d1 { si'2.\trill R2. }
    >>
    mi''4 fa'' sol'' |
    do''2. |
    <<
      \tag #'basse { s2.*2 \ffclef "vdessus" }
      \tag #'choeur1-d1 R2.*2
    >>
    mi''4 fa'' re'' |
    mi''2 <<
      \tag #'basse { s4 s2.*8 \ffclef "vdessus" }
      \tag #'choeur1-d1 { r4 R2.*8 }
    >>
    mi''4 fa'' fa'' |
    re''2.\trill |
    sol''4 fa''\trill re'' |
    <<
      \tag #'basse { s2.*2 \ffclef "vdessus" }
      \tag #'choeur1-d1 { mi''2. R2. }
    >>
    re''4 re'' mi'' |
    <<
      \tag #'basse { s2.*2 \ffclef "vdessus" }
      \tag #'choeur1-d1 { fa''2 r4 R2. }
    >>
    mi''4 fa'' re'' |
    <<
      \tag #'basse { s2.*2 \ffclef "vdessus" }
      \tag #'choeur1-d1 { mi''2. R2. }
    >>
    sol''4 fa''\trill re'' |
    mi''2. |
  }
  \tag #'choeur1-d2 {
    \clef "vbas-dessus" R2.*2 |
    la'4 la' si' |
    do''2. |
    R2.*2 |
    mi''4 re''\trill si' |
    do''2 r4 |
    R2.*8 |
    fad'4 fad' fad' |
    sol'2. |
    sol'4 sol'4. fad'8 |
    sol'2. |
    R2. |
    do''4 sib'8[ la'] sib'4 |
    la'2.\trill |
    R2.*2 |
    do''4 re'' si'\trill |
    do''2 r4 |
    R2.*8 |
    do''4 re'' re'' |
    si'2.\trill |
    mi''4 re''\trill si' |
    do''2. |
    R2. |
    si'4 si' dod'' |
    re''2 r4 |
    R2. |
    do''4 re'' si' |
    do''2 r4 |
    R2. |
    mi''4 re''\trill si' |
    do''2. |
  }
  \tag #'choeur1-hc {
    \clef "vhaute-contre" R2.*2 |
    fa'4 fa' re' |
    la'2. |
    R2.*2 |
    mi'4 fa' sol' |
    do'2. |
    R2.*8 |
    re'4 re' si |
    mi'2. |
    do'4 re'8[ do'] re'4 |
    sol2. |
    R2. |
    do'4 re' mi' |
    fa'2. |
    R2.*2 |
    la'4 fa' sol' |
    do'2 r4 |
    R2.*8 |
    la'4 fa' re' |
    sol'2. |
    mi'4 fa' sol' |
    do'2. |
    R2. |
    sol'4 sol' sol' |
    re'2 r4 |
    R2. |
    la'4 fa' sol' |
    do'2 r4 |
    R2. |
    mi'4 fa' sol' |
    do'2. |
  }
  \tag #'(choeur2-d basse) {
    \clef "vdessus"
    \tag #'choeur2-d <>^\markup\character Chœur de Sylvains, de Satyres, & de Bergers
    do''4 do'' sol' |
    la'2 la'4 |
    <<
      \tag #'basse { s2.*2 \ffclef "vdessus" }
      \tag #'choeur2-d R2.*2
    >>
    mi''4 fa'' fa'' |
    re''2\trill re''4 |
    <<
      \tag #'basse { s2. s2 \ffclef "vdessus" }
      \tag #'choeur2-d { R2. | r4 r }
    >> mi''4 |
    fa''2 fa''4 |
    re'' re'' re'' |
    mi'' mi'' mi'' |
    dod''2\trill dod''4 |
    re'' re'' re'' |
    mi''2 re''4 |
    re''( dod''4\trill si'8) dod'' |
    re''2 re''4 |
    <<
      \tag #'basse { s2.*3 \ffclef "vdessus" }
      \tag #'choeur2-d R2.*3
    >>
    re''4 do'' si' |
    mi''2 mi''4 |
    <<
      \tag #'basse { s2.*2 \ffclef "vdessus" }
      \tag #'choeur2-d R2.*2
    >>
    la'4 la' si' |
    do''2 do''4 |
    <<
      \tag #'basse { s2. s2 \ffclef "vdessus" }
      \tag #'choeur2-d { R2. r4 r }
    >> mi''4 |
    mi''2 re''4 |
    re'' si' mi'' |
    do'' do'' re'' |
    si'2\trill si'4 |
    mi'' mi'' mi'' |
    do''2 re''4 |
    si'2 mi''4 |
    dod''2\trill dod''4 |
    <<
      \tag #'basse { s2.*3 \ffclef "vdessus" }
      \tag #'choeur2-d R2.*3
    >>
    sol'4 sol' la' |
    si'2 si'4 |
    <<
      \tag #'basse { s2. \ffclef "vdessus" }
      \tag #'choeur2-d R2.
    >>
    la'4 la' si' |
    do''2 do''4 |
    <<
      \tag #'basse { s2. \ffclef "vdessus" }
      \tag #'choeur2-d R2.
    >>
    mi''4 mi'' mi'' |
    re''2\trill re''4 |
    \tag #'choeur2-d R2.*2
  }
  \tag #'choeur2-hc {
    \clef "vhaute-contre" mi'4 mi' mi' |
    fa'2 fa'4 |
    R2.*2 |
    la'4 la' la' |
    sol'2 sol'4 |
    R2. |
    r4 r sol' |
    la'2 la'4 |
    fa' fa' fa' |
    sol' sol' sol' |
    mi'2\trill mi'4 |
    fa' fa' fa' |
    sol'2 fa'4 |
    mi'2 la'4 |
    fad'2\trill fad'4 |
    R2.*3 |
    sol'4 sol' sol' |
    sol'2 sol'4 |
    R2.*2 |
    fa'4 fa' fa' |
    mi'2\trill mi'4 |
    R2. |
    r4 r sol' |
    fa'2 fa'4 |
    mi' mi' mi' |
    la' fa' fa' |
    mi'2\trill mi'4 |
    mi' mi' mi' |
    fa'2 fa'4 |
    mi'2\trill mi'4 |
    mi'2 mi'4 |
    R2.*3 |
    mi'4 mi' fad' |
    sol'2 sol'4 |
    R2. |
    fa'4 fa' fa' |
    mi'2\trill mi'4 |
    R2. |
    sol'4 sol' sol' |
    sol'2 sol'4 |
    R2.*2 |
  }
  \tag #'choeur2-t {
    \clef "vtaille" sol4 sol do' |
    do'2 do'4 |
    R2.*2 |
    do'4 do' re' |
    si2\trill si4 |
    R2. |
    r4 r do' |
    do'2 do'4 |
    sib sib sib |
    sib sib sib |
    la2 la4 |
    la la la |
    la2 la4 |
    la2 la4 |
    la2 la4 |
    R2.*3 |
    si4 do' re' |
    do'2 do'4 |
    R2.*2 |
    do'4 do' re' |
    do'2 do'4 |
    R2. |
    r4 r do' |
    do'2 si4 |
    sold sold sold |
    la la si |
    sold2\trill sold4 |
    la la la |
    la2 la4 |
    la2 sold4 |
    la2 la4 |
    R2.*3 |
    do'4 do' do' |
    re'2 re'4 |
    R2. |
    re'4 re' re' |
    do'2 do'4 |
    R2. |
    do'4 do' do' |
    si2\trill si4 |
    R2.*2 |
  }
  \tag #'choeur2-b {
    \clef "vbasse" do4 do do |
    fa2 fa4 |
    R2.*2 |
    la4 fa re |
    sol2 sol4 |
    R2. |
    r4 r do' |
    la2\trill la4 |
    sib sib sib |
    sol\trill sol sol |
    la2 la4 |
    fa fa fa |
    dod2\trill re4 |
    la,2 la,4 |
    re2 re4 |
    R2.*3 |
    sol4 la si |
    do'2 do'4 |
    R2.*2 |
    fa4 fa re |
    la2 la4 |
    R2. |
    r4 r do |
    re2 re4 |
    mi mi mi |
    fa fa re |
    mi2 mi4 |
    do do do |
    fa2 re4 |
    mi2 mi4 |
    la,2 la,4 |
    R2.*3 |
    do'4 do' << { \voiceTwo do \oneVoice } \new Voice { \voiceOne \sug do' } >> |
    sol2 sol4 |
    R2. |
    re'4 re' re' |
    la2 la4 |
    R2. |
    do'4 do' do |
    sol2 sol4 |
    R2.*2 |
  }
  \tag #'voix {
    \clef "vdessus" R2.*48
  }
>>
\keepWithTag #'(basse choeur1-d1 choeur1-d2 choeur1-hc
                      choeur2-d choeur2-hc choeur2-t choeur2-b
                      voix) \choeur
<<
  \tag #'(voix basse) {
    \tag #'basse \ffclef "vdessus" <>^\markup\character Syrinx
    r4 do'' do'' |
    la' la' si' |
    sold'\trill la' si' |
    do'' re''8[ do''] re''[ si'] |
    do''2 la'4 |
    \ffclef "vbasse" <>^\markup\character Pan
    r4 mi fa |
    sol la8[ sol] fa[ mi] |
    re2\trill re4 |
    sol4. sol8 fa mi |
    << { \voiceTwo mi4 mi re \oneVoice }
      \new Voice \sugNotes { \voiceOne re4\trill mi fa } >> |
    mi2.\trill |
  }
  \tag #'(choeur1-d1 choeur1-d2 choeur1-hc
                     choeur2-d choeur2-hc choeur2-t choeur2-b) R2.*11
>>
<<
  \tag #'(choeur1-d1 basse) {
    <<
      \tag #'basse { s2.*2 \ffclef "vdessus" }
      \tag #'choeur1-d1 { R2.*2 }
    >>
    re''4 re'' mi'' |
    <<
      \tag #'basse { s2.*2 \ffclef "vdessus" }
      \tag #'choeur1-d1 { fa''2 r4 | R2. }
    >>
    mi''4 fa'' re'' |
    <<
      \tag #'basse { s2.*2 \ffclef "vdessus" }
      \tag #'choeur1-d1 { mi''2 r4 | R2. }
    >>
    sol''4 fa''\trill re'' |
    mi''2. |
  }
  \tag #'choeur1-d2 {
    R2.*2 |
    si'4 si' dod'' |
    re''2 r4 |
    R2. |
    do''4 re'' si' |
    do''2 r4 |
    R2. |
    mi''4 re''\trill si' |
    do''2. |
  }
  \tag #'choeur1-hc {
    R2.*2 |
    sol'4 sol' sol' |
    re'2 r4 |
    R2. |
    la'4 fa' sol' |
    do'2 r4 |
    R2. |
    mi'4 fa' sol' |
    do'2. |
  }
  \tag #'(choeur2-d basse) {
    \tag #'basse \ffclef "vdessus"
    \tag #'choeur2-d <>^\markup\character Les Satyres
    sol'4 sol' la' |
    si'2\trill si'4 |
    <<
      \tag #'basse { s2. \ffclef "vdessus" }
      \tag #'choeur2-d R2.
    >>
    la'4 la' si' |
    do''2 do''4 |
    <<
      \tag #'basse { s2. \ffclef "vdessus" }
      \tag #'choeur2-d R2.
    >>
    mi''4 mi'' mi'' |
    re''2\trill re''4 |
    \tag #'choeur2-d R2.*2
  }
  \tag #'choeur2-hc {
    mi'4 mi' fad' |
    sol'2 sol'4 |
    R2. |
    fa'4 fa' fa' |
    mi'2\trill mi'4 |
    R2. |
    sol'4 sol' sol' |
    sol'2 sol'4 |
    R2.*2 |
  }
  \tag #'choeur2-t {
    do'4 do' do' |
    re'2 re'4 |
    R2. |
    re'4 re' re' |
    do'2 do'4 |
    R2. |
    do'4 do' do' |
    si2\trill si4 |
    R2.*2 |
  }
  \tag #'choeur2-b {
    do'4 do' do' |
    sol2 sol4 |
    R2. |
    re'4 re' re' |
    la2 la4 |
    R2. |
    do'4 do' do |
    sol2 sol4 |
    R2.*2 |
  }
  \tag #'voix { R2.*10 }
>>
<<
  \tag #'(voix basse) {
    \ffclef "vdessus" <>^\markup\character Syrinx
    r4 do'' do'' |
    do''2 si'4 |
    la' si' do'' |
    si'2\trill si'4 |
    \ffclef "vbasse" <>^\markup\character Pan
    r4 r sol |
    sol4. sol8 fa4 |
    mi4 fad sol |
    fad\trill
    \ffclef "vdessus" <>^\markup\character Syrinx
    re'' re'' |
    re''2 do''4 |
    do'' si' do'' |
    si'2\trill si'4 |
    \ffclef "vbasse" <>^\markup\character Pan
    r4 r sol |
    sol4. fa8 mi4 |
    re\trill mi fa |
    mi2.\trill |
  }
  \tag #'(choeur1-d1 choeur1-d2 choeur1-hc
                     choeur2-d choeur2-hc choeur2-t choeur2-b) R2.*15
>>
\choeur
