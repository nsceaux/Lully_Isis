\clef "quinte"
\setMusic #'choeur {
  sol4 sol do' |
  do'2. |
  R2.*2 |
  la4 la re' |
  re'2. |
  R2. |
  r4 r do' |
  do'2 do'4 |
  sib sib sib |
  sib sib sib |
  la2 la4 |
  la la la |
  la2 la4 |
  la2 la4 |
  la2. |
  R2.*3 |
  re'2 re'4 |
  do'2. |
  R2.*2 |
  la4 la re' |
  do'2. |
  R2. |
  r4 r do' |
  la2 re'4 |
  si si si |
  la do' si |
  si2 si4 |
  la la la |
  la2 si4 |
  si2 si4 |
  la2. |
  R2.*3 |
  do'4 do' do' |
  si2. |
  R2. |
  re'4 re' re' |
  do'2. |
  R2. |
  do'4 do' do' |
  re'2. |
  R2.*2 |
}
\keepWithTag #'() \choeur
R2.*11 |
do'4 do' do' |
si2. |
R2. |
re'4 re' re' |
do'2. |
R2. |
do'4 do' do' |
re'2. |
R2.*2 |
R2.*15 |
\choeur
