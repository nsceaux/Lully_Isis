\clef "dessus"
\setMusic #'choeur {
  sol''4 sol'' sol'' |
  fa''2 fa''4 |
  R2.*2 |
  mi''4 fa'' fa'' |
  re''2.\trill |
  R2. |
  r4 r mi'' |
  fa''2 fa''4 |
  re'' re'' re'' |
  mi'' mi'' mi'' |
  dod''2\trill dod''4 |
  re'' re'' re'' |
  mi''2 re''4 |
  dod''4.\trill si'8 dod''4 |
  re''2 re''4 |
  R2.*3 |
  sol''4 sol'' fa'' |
  mi''2.\trill |
  R2.*2 |
  la'4 la' si' |
  do''2. |
  R2. |
  r4 r mi'' |
  mi''2 re''4 |
  re'' si' mi'' |
  do'' do'' re'' |
  si'2\trill si'4 |
  mi'' mi'' mi'' |
  do''2 re''4 |
  si'2 mi''4 |
  dod''2.\trill |
  R2.*3 |
  do''4 do'' la' |
  si'2. |
  R2. |
  la'4 la' si' |
  do''2. |
  R2. |
  mi''4 mi'' mi'' |
  re''2.\trill |
  R2.*2 |
}
\keepWithTag #'() \choeur
R2.*11 |
do''4 do'' la' |
si'2. |
R2. |
la'4 la' si' |
do''2. |
R2. |
mi''4 mi'' mi'' |
\sug re''2. |
R2.*2 |
R2.*15 |
\choeur
