\clef "taille"
\setMusic #'choeur {
  mi'4 mi' mi' |
  fa'2. |
  R2.*2 |
  la'4 la' la' |
  sol'2. |
  R2. |
  r4 r sol' |
  fa'2 fa'4 |
  fa' fa' fa' |
  mi' mi' mi' |
  mi'2 mi'4 |
  re' re' fa' |
  mi'2 fa'8 sol' |
  la'2 la'4 |
  la'2. |
  R2.*3 |
  sol'4 sol' sol' |
  sol'2. |
  R2.*2 |
  do'4 do' re' |
  mi'2. |
  R2. |
  r4 r sol' |
  fa'2 fa'4 |
  mi' mi' mi' |
  la' fa' fa' |
  mi'2 mi'4 |
  la' la' la' |
  la'2 fa'4 |
  mi'2 mi'4 |
  mi'2. |
  R2.*3 |
  do'4 do' do' |
  re'2. |
  R2. |
  fa'4 fa' fa' |
  mi'2. |
  R2. |
  sol'4 sol' sol' |
  sol'2. |
  R2.*2 |
}
\keepWithTag #'() \choeur
R2.*11 |
do'4 do' do' |
re'2. |
R2. |
fa'4 fa' fa' |
mi'2. |
R2. |
sol'4 sol' sol' |
sol'2. |
R2.*2 |
R2.*15 |
\choeur
