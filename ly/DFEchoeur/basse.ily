\clef "basse" 
\setMusic #'choeur {
  \tag #'bc-part <>^"[Tous]"
  do4 do do |
  fa2. |
  <<
    \tag #'basse { R2.*2 }
    \tag #'(basse-continue bc-part) {
      \tag #'bc-part <>^"[B.C.]"
      \clef "alto" fa'4 fa' re' |
      la'2. |
      \clef "bass"
    }
  >>
  \tag #'bc-part <>^"[Tous]"
  la4 fa re |
  sol2 sol4 |
  <<
    \tag #'basse { R2. r4 r }
    \tag #'(basse-continue bc-part) {
      \tag #'bc-part <>^"[B.C.]"
      \clef "alto" mi'4 fa' sol' |
      do'2 \clef "bass"
    }
  >> \tag #'bc-part <>^"[Tous]" do'4 |
  la2 la4 |
  sib sib sib |
  sol sol sol |
  la2 la4 |
  fa fa fa |
  dod2 re4 |
  la,2 la,4 |
  re2. |
  <<
    \tag #'basse { R2.*3 }
    \tag #'(basse-continue bc-part) {
      \tag #'bc-part <>^"[B.C.]"
      \clef "alto" re'4 re' si |
      mi'2. |
      do'4 re'8 do' re'4 |
      \clef "bass"
    }
  >> \tag #'bc-part <>^"[Tous]" sol4 la si |
  do'2. |
  <<
    \tag #'basse { R2.*2 }
    \tag #'(basse-continue bc-part) {
      \tag #'bc-part <>^"[B.C.]"
      \clef "alto" do'4 re' mi' |
      fa'2. |
      \clef "bass"
    }
  >>
  \tag #'bc-part <>^"[Tous]" fa4 fa re |
  la2. |
  <<
    \tag #'basse { R2. r4 r }
    \tag #'(basse-continue bc-part) {
      \tag #'bc-part <>^"[B.C.]"
      \clef "alto" la'4 fa' sol' |
      do'2 \clef "bass"
    }
  >> \tag #'bc-part <>^"[Tous]" do4 |
  re2 re4 |
  mi mi mi |
  fa fa re |
  mi2 mi4 |
  do do do |
  fa2 re4 |
  mi2 mi,4 |
  la,2. |
  <<
    \tag #'basse { R2.*3 }
    \tag #'(basse-continue bc-part) {
      \tag #'bc-part <>^"[B.C.]"
      \clef "alto" la'4 fa' re' |
      sol'2. |
      mi'4 fa' sol' |
      \clef "bass"
    }
  >> \tag #'bc-part <>^"[Tous]" do'4 do' << \sug do' \\ do >> |
  sol2. |
  <<
    \tag #'basse { R2. }
    \tag #'(basse-continue bc-part) {
      \tag #'bc-part <>^"[B.C.]"
      \clef "alto" sol'4 sol' sol' |
      \clef "bass"
    }
  >>
  \tag #'bc-part <>^"[Tous]" re'4 re' re' |
  la2. |
  <<
    \tag #'basse { R2. }
    \tag #'(basse-continue bc-part) {
      \tag #'bc-part <>^"[B.C.]"
      \clef "alto" la'4 fa' sol' |
    }
  >>
  \tag #'bc-part <>^"[Tous]" \clef "bass" do' do' do |
  sol2. |
  <<
    \tag #'basse { R2.*2 }
    \tag #'(basse-continue bc-part) {
      \tag #'bc-part <>^"[B.C.]"
      \clef "alto" mi'4 fa' sol' |
      do'2. | \clef "bass"
    }
  >>
}
\keepWithTag #'(basse basse-continue bc-part) \choeur
<<
  \tag #'basse R2.*11
  \tag #'(basse-continue bc-part) {
    \tag #'bc-part <>^"[B.C.]"
    do2. |
    fa2 re4 |
    mi do si, |
    la, sold,2 |
    la,2. |
    la,4 sol, fa, |
    mi,2 fa,4 |
    sol,2. |
    mi,2 fa,4 |
    sol, sol,2 |
    do,2. | \allowPageTurn
  }
>>
\tag #'bc-part <>^"[Tous]" do'4 do' do' |
sol2. |
<<
  \tag #'basse { R2. }
  \tag #'(basse-continue bc-part) {
    \tag #'bc-part <>^"[B.C.]"
    \clef "alto" sol'4 sol' sol' | \clef "bass"
  }
>>
\tag #'bc-part <>^"[Tous]" re'4 re' re' |
la2. |
<<
  \tag #'basse { R2. }
  \tag #'(basse-continue bc-part) {
    \tag #'bc-part <>^"[B.C.]"
    \clef "alto" la'4 fa' sol' | \clef "bass"
  }
>>
\tag #'bc-part <>^"[Tous]" do'4 do' do |
sol2. |
<<
  \tag #'basse { R2.*2 | }
  \tag #'(basse-continue bc-part) {
    \tag #'bc-part <>^"[B.C.]"
    \clef "alto" mi'4 fa' sol' |
    do'2. | \clef "bass"
  }
>>
<<
  \tag #'basse { R2.*15 }
  \tag #'(basse-continue bc-part) {
    \tag #'bc-part <>^"[B.C.]"
    do'4. si8 la sol |
    fad2 sol4 |
    do re re, |
    sol,2 << sol,4 \\ \sug fa, >> |
    mi, mi re |
    dod2 re4 |
    sol, la,2 |
    re2. |
    mi |
    fad2 fad4 |
    sol2 fa4 |
    mi re do |
    si,2\trill do4 |
    fa, sol,2 |
    do,2. |
  }
>>
\choeur
