\score {
  <<
    \new StaffGroup \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'choeur2-d \includeNotes "voix"
      >> \keepWithTag #'choeur2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'choeur2-hc \includeNotes "voix"
      >> \keepWithTag #'choeur2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'choeur2-t \includeNotes "voix"
      >> \keepWithTag #'choeur2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'choeur2-b \includeNotes "voix"
      >> \keepWithTag #'choeur2 \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'choeur1-d1 \includeNotes "voix"
      >> \keepWithTag #'choeur1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'choeur1-d2 \includeNotes "voix"
      >> \keepWithTag #'choeur1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'choeur1-hc \includeNotes "voix"
      >> \keepWithTag #'choeur1 \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix \includeNotes "voix"
      >> \keepWithTag #'voix \includeLyrics "paroles"
      \new Staff <<
        \global \keepWithTag #'basse-continue \includeNotes "basse"
        \includeFigures "chiffres"
        \origLayout {
          s2.*8\pageBreak
          s2.*8\pageBreak
          s2.*9\pageBreak
          s2.*9\pageBreak
          s2.*7\pageBreak
          s2.*9\pageBreak
          s2.*9\break s2.*8\pageBreak
          s2.*7\break s2.*6 s2 \bar "" \break s4 s2.*4\pageBreak
          s2.*7\pageBreak
          s2.*8\pageBreak
          s2.*9\pageBreak
          s2.*9\pageBreak
          s2.*7\pageBreak
        }
        \modVersion { s2.*48\break s2.*11\break s2.*25\break }
      >>
    >>
  >>
  \layout { indent = 0 }
  \midi { }
}
