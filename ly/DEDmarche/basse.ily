\clef "basse" do'4 si |
do' do fa2 |
do4 do fa2 |
do4 do'2 si4 |
do' do sol fad |
sol4. fa8 mi4 re |
do do' fa2 |
do4 do fa2 |
do4 do'2 si4 |
do' fa sol sol, |
do do' si la |
sol sol, do2 |
sol,4 sol, do2 |
sol,4 sol2 fad4 |
sol sol fa mi |
re4. re8 la4 la |
mi2. do4 |
fa re sol sol, |
do do'
\sug do2
