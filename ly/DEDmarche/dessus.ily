\clef "dessus" <>^"Violons, Musettes et Hautbois" do''4 re'' |
mi''2 fa''4\trill~ fa''8( mi''16 fa'') |
sol''4 \slurDashed fa''8( mi'') la''4 sol''8( fa'') |
sol''4 fa''8( mi'') fa''4 mi''8 re'' | \slurSolid
mi''4 fa''8 mi'' re'' mi'' do'' re'' |
si'4\trill sol' do'' re'' |
mi''2 fa''4\trill~ fa''8( mi''16 fa'') |
sol''4 \slurDashed fa''8( mi'') la''4 sol''8( fa'') |
sol''4 fa''8( mi'') fa''4 mi''8( re'') | \slurSolid
mi''4. fa''8 re''4.\trill do''8 |
do''2 sol'4 la' |
si'2 do''4\trill~ do''8( si'16 do'') |
re''4 \slurDashed do''8( si') mi''4 re''8( do'') |
re''4 do''8( si') do''4 si'8 la' |
si'4 la'8( sol') re''4 mi'' |
fa'' sol''8 fa'' mi''4.\trill fa''8 |
sol''4 sol' do'' do''8 si' |
la'4 fa''8 mi'' re''4.\trill do''8 |
do''2
do'' |
