\clef "haute-contre" sol'4 sol' |
sol' sol' la'2 |
sol'4 do'' do''2 |
do''4. do''8 re''4. re''8 |
sol'4. do''8 si' do'' la'4 |
sol'2. sol'4 |
sol' sol' la'2 |
sol'4 do'' do''2 |
do''4. do''8 re''4. re''8 |
sol'4 la' sol'4. fa'8 |
mi'2\trill sol'4 fad' |
sol'2 sol'4 sol' |
sol' la'8 si' do''4 si'8 la' |
si'4 la'8 sol' la'4. la'8 |
sol'4 sol' << \sug la' \\ sol' >> la' |
la' la'8 si' do''4 la' |
sol' sol' mi' mi' |
fa' la' sol'4.\trill fa'8 |
mi'2\trill
mi'\trill |
