\clef "taille" sol'4 sol' |
sol' sol' fa'2 |
mi'4 fa'8 sol' fa'4 sol'8 la' |
sol'2. sol'4 |
sol' sol' sol' la' |
re'8 do' si4 do' si |
do' sol' fa'2 |
mi'4 fa'8 sol' fa'4 sol'8 la' |
sol'2. sol'8 fa' |
mi'4 re' re' sol' |
sol'2 si4 do' |
re'2 mi'4. re'16 do' |
si4 do'8 re' do'4 re'8 mi' |
re'2. re'4 |
re'4 re' re' dod' |
re'4. re'8 do'4 do'4 |
do'2. do'4 |
do' re' re' sol' |
sol'2
sol' |
