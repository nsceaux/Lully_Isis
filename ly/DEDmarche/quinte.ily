\clef "quinte" mi' re' |
do' do' do'2 |
do'4 do' la la |
mi' mi' re'4. re'8 |
do'4 do' re' re' |
re' re' mi' fa' |
mi' do' do'2 |
do'4 do' la la |
mi' mi' re'4. re'8 |
do'4 do' si4. do'8 |
do'2 re'4 do' |
si re' do' sol |
sol sol sol2 |
sol4 la8 si la4 re' |
re' do'8 si la4 sol |
fa4 la la8 si do'4 |
sol2. sol4 |
la la si4. do'8 |
do'2
do' |
