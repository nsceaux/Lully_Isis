\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille)
   (quinte #:system-count 3)
   (basse #:system-count 3)
   (basse-continue #:system-count 3)
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
