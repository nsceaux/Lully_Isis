\clef "vbasse" <>^\markup\character Jupiter
la8 la16 la la8 do' fa fa16 fa do8 re16 mib |
re8\trill re r16 re re mi fa8 fa fa sol |
la4 la8 la fa fa fa fa |
fa4 mi8 fa16 sol re8.\trill do16 |
do4 r8 sol sol\trill sol sol la |
sib4 sib8 sib16 sib re8 mi |
fa4 fa16 fa fa sol la8 sib |
do'4 do'8 do' do'4 re'8 mib' |
re'4\trill re'8 sib sib sib sol sol |
mi4\trill la8 sib16 do' sol8.\trill fa16 |
fa8
\ffclef "vbas-dessus" <>^\markup\character Io
fa''4 do''16 do'' dod''8 dod'' dod'' re'' |
re''16 re'' fa'' fa'' re''8 la'16 la' fa'8 fa'' si'16\trill si' si' re'' |
sol'8\trill sol'16 mib'' mib''8. re''16 re''8. re''16 |
si'8\trill re'' re'' mi'' fa'' sol'' |
mi''\trill
\ffclef "vbasse" <>^\markup\character Jupiter
do'16 do' mi8\trill mi16 mi <<
  \new Voice \with { autoBeaming = ##f } \sugNotes {
    \voiceOne fa8 la16 do' fa8 fa16 la |
  }
  { \voiceTwo fa8 fa16 la fa8 fa16 fa | \oneVoice }
>>
re4\trill re8 sib16 sib fad8\trill fad16 sol |
la4 la8 la16 la re'8 la |
sib8 sib16 sib sol8 sol16 sol mi8\trill mi16 fa re8\trill re16 sol |
do4 do8
\ffclef "vbas-dessus" <>^\markup\character Io
do''8 sol' sol' sol' la' |
sib' sib' sib'\trill sib' sib' la' |
la'4\trill la'8
\ffclef "vbasse" <>^\markup\character Jupiter
do8 fa fa fa sol |
la fa fa fa sol la |
sib r16 re' sib sib sib fa sol8 mib16 sol do8\trill do16 fa |
sib,8 sib,
\ffclef "vbas-dessus" <>^\markup\character Io
re''8 re'' sol' sol'16 fa' fa'8 mi' |
mi'\trill mi'16 do'' la'8\trill la' sib' do'' |
re''8 re'' mi'' fa'' sol'8.\trill fa'16 |
fa'2 r |
