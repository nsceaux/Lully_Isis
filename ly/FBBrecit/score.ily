\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2\break s1 s2. s1\break s2.*2 s1\break
        s1 s2. s2 \bar "" \break s2 s1 s4 \bar "" \break s2 s2. s2 \bar "" \pageBreak
        s2 s2.*2\break s1*2\break s2. s1 s2.\break s1*2\break
      }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
