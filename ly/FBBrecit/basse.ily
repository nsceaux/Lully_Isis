\clef "basse" fa,1\repeatTie |
sib,2 la,4. sol,8 |
fa,1 |
sol,4 do8 fa, sol,4 |
do,2 do4 sib,8 la, |
sol,2. |
re2 do8 sib, |
la,1 | \allowPageTurn
sib,2. si,4 |
do la,8 sol,16 fa, do8 do, |
fa,4. fa8 mi2 |
re2. sol8^\markup\croche-pointee-double fa |
mib4 fa2 |
sol si,4 |
do4. sib,8 la,2 |
sib, la,8. sol,16 |
fad,2. |
sol,2 << \sugNotes { do8[ fa,] } \\ { la,8[ fa,] } >> sol,4 |
do1 |
mi,2. |
fa,1 |
\once\tieDashed fa4~ fa mib |
re re, mib, fa, |
sib,2 si, |
do4 fa8 mib re do |
sib, la, sol, fa, do do, |
fa,2. mi,4 |
\once\set Staff.whichBar = "|"
re,1*15/16~ \hideNotes re,16 |
