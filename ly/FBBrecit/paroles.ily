Il ne m’est pas per -- mis de fi -- nir vos -- tre pei -- ne,
et ma puis -- san -- ce sou -- ve -- rai -- ne,
doit sui -- vre du des -- tin l’ir -- re -- vo -- ca -- ble loy :
c’est tout ce que je puis par un a -- mour ex -- tré -- me,
que de quit -- ter le ciel & ma gloi -- re su -- prê -- me,
pour pren -- dre part aux maux que vous souf -- frez pour moy.

Ah ! mon sup -- plice aug -- mente en -- co -- re !
Tous le feu des En -- fers me brûle & me dé -- vo -- re ;
mour -- ray- je tant de fois, sans voir fi -- nir mon sort ?

Ma ten -- dres -- se pour vous, rend Ju -- non in -- flé -- xi -- ble :
el -- le voit mon a -- mour, il luy pa -- roit trop fort ;
son cour -- roux se re -- double & de -- vient in -- vin -- ci -- ble.

N’im -- porte, en ma fa -- veur, soy -- ez toû -- jours sen -- si -- ble.

C’est trop vous ex -- po -- ser à son ja -- loux trans -- port ;
j’ir -- rite en vous ai -- mant, sa ven -- gean -- ce ter -- ri -- ble.

Ai -- mez- moy, s’il vous est pos -- si -- ble,
as -- sez pour la for -- cer à me don -- ner la mort.
