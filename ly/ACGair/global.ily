\key do \major \midiTempo#160
\digitTime\time 2/2 \partial 2 s2 s1*3 s2 \bar ":|.|:"
s2 s1*7 \alternatives { \measure 1/2 s2 } { \measure 2/2 s1 \bar "|." }
