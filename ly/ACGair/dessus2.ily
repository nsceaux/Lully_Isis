\clef "dessus"
<>^\markup\whiteout\italic { [Second Dessus pour les trompettes seulement] }
mi''4 mi'' |
do''2 do''4 do'' |
sol' do'' mi'' mi'' |
do''2 do''4 do'' |
sol'2 sol'4 sol' |
sol'2 sol'4 sol' |
sol' do''8 re'' mi'' fa'' mi'' re'' |
do''4 do''8 re'' mi''4 re''8 do'' |
sol'4 do'' sol' sol' |
sol'2 sol'4 sol' |
sol' do''8 re'' mi'' fa'' mi'' re'' |
do''4 do'' sol'4. do''8 |
do''2 |
do''1 |
