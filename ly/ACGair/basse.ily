\clef "basse" do'4 do' |
do'2 do'4 do |
sol do do' do' |
do'2 do'4 do |
sol2 sol,4 sol, |
sol,2 sol4 sol |
sol do' do' sol |
do'4. do'8 do'4 do |
sol do sol, sol, |
sol,2 sol4 sol |
sol do' do' sol |
do' do sol sol, |
do2 |
do1 |
