\clef "dessus"
<>^\markup\whiteout\italic { [Premier Dessus pour les trompettes & violons] }
sol''4 sol'' |
mi''\trill do''8 re'' mi'' re'' mi'' fa'' |
re''4\trill do'' sol'' sol'' |
mi''\trill do''8 re'' mi'' re'' mi'' fa'' |
re''2\trill re''8 do'' re'' mi'' |
re''4 sol' re''8 do'' re'' mi'' |
re''4 mi''8 fa'' sol'' la'' sol'' fa'' |
mi''\trill re'' mi'' fa'' sol''4 fa''8 mi'' |
re''4\trill do'' re''8 do'' re'' mi'' |
re''4 sol' re''8 do'' re'' mi'' |
re''4 mi''8 fa'' sol'' la'' sol'' fa'' |
mi'' re'' mi'' fa'' re''4.\trill do''8 |
do''2
do''1 |

