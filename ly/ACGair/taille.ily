\clef "taille" do'8 re' mi' fa' |
sol'4 mi'8 fa' sol'4 sol' |
sol' mi' do'8 re' mi' fa' |
sol'4 mi'8 fa' sol'4 sol' |
sol'2\trill re'8 mi' re' do' |
re'4 si re'8 mi' re' do' |
re'4 do' do' si |
do' sol'8 fa' mi'4 sol' |
sol' mi' re'8 mi' re' do' |
re'4 si re'8 mi' re' do' |
re'4 do' do' re' |
mi'8 fa' sol'4 sol'4.\trill fa'8 |
mi'2 |
mi'1 |
