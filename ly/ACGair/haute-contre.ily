\clef "haute-contre" mi''4 mi'' |
do''2 do''8 si' do'' re'' |
si'4\trill do'' mi'' mi'' |
do''2 do''8 si' do'' re'' |
si'2\trill si'8 la' si' do'' |
si'4 sol' si'8 la' si' do'' |
si'4 do''8 re'' mi''4 re'' |
do''8 si' do'' re'' mi''4 re''8 do'' |
si'4 do'' si'8 la' si' do'' |
si'4 sol' si'8 la' si' do'' |
si'4 do''8 re'' mi''4 re'' |
do''8 si' do'' re'' si'4.\trill do''8 |
do''2 |
do''1 |
