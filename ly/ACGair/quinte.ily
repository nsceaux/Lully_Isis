\clef "quinte" mi'4 do'8 re' |
mi' fa' sol' fa' mi'4 << \sug sol' \\ do' >> |
sol' sol' mi' do'8 re' |
mi' fa' sol' fa' mi'4 do' |
re'2\trill si8 do' si la |
si4 sol si8 do' si la |
si4 sol' sol' sol' |
sol'8 fa' mi' re' do'4 sol |
sol sol sol la |
si re' si sol |
sol'8 fa' mi' re' do'4 si |
do' sol sol4. sol8 |
sol2 |
sol1 |
