\score {
  \new StaffGroup <<
    \new GrandStaff <<
      \new Staff <<
        \global \keepWithTag #'trompette1 \includeNotes "dessus1"
      >>
      \new Staff <<
        \global \keepWithTag #'trompette2 \includeNotes "dessus2"
      >>
    >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff << \global \includeNotes "taille" >>
    \new Staff << \global \includeNotes "quinte" >>
    \new Staff <<
      \global \includeNotes "basse"
      \origLayout {
        s2 s1*6\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
