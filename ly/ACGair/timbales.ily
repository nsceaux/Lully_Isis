\clef "basse" do4 do |
do2 do4 do |
sol, do do do |
do2 do4 do |
sol,2 sol,4 sol, |
sol,2 sol,4 sol, |
sol, do do sol, |
do4. do8 do4 do |
sol, do sol, sol, |
sol,2 sol,4 sol, |
sol, do do sol, |
do do sol, sol, |
do2 |
do1 |
