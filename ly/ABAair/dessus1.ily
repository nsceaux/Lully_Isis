\clef "dessus" si'8 do'' re'' mi'' |
re''4 sol'' fad''4. sol''8 |
mi''4 mi'' do''4. do''8 |
do''4 si' la' si'8 do'' |
si'4\trill sol' re''8 re'' mi'' fad'' |
sol''4. la''8 fad''4. sol''8 |
mi''4 mi'' re''4. mi''8 |
do''4. re''8 <<
  \new CueVoice { s4^\markup\croche-pointee-double }
  { si'8 do'' }
>> do''8.\trill si'16 |
la'2\trill fad''4 fad''8 sol'' |
la''2 mi''4 mi''8 fad'' |
sol''4. sol''8 fa''4. sol''8 |
mi''4\trill mi'' la''4. la''8 |
fad''4 sol'' fad''4.\trill sol''8 |
sol''2 fad''4 fad''8 sol'' |
la''2 mi''4 mi''8 fad'' |
sol''4. sol''8 fa''4. sol''8 |
mi''4 mi'' la''4. la''8 |
fad''4 sol'' fad''4.\trill sol''8 |
sol''2
