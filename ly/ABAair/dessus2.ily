\clef "dessus" sol'8 la' si' do'' |
si'4 si' la'4.\trill si'8 |
sol'4 sol' la'4. la'8 |
la'4 sol' sol' sol'8 fad' |
sol'4 sol' si'8 si' dod'' re'' |
re''4 do'' do'' si' |
si' la' la' sol' |
sol' fad'  <<
  \new CueVoice {
    s4_\markup\croche-pointee-double
    s4_\markup\croche-pointee-double
  }
  { sol'8 la' la'8*3/2\trill sol'8*1/2 | }
>>
fad'2\trill r |
la'4 la'8 si' do''2 si'4 si'8 do'' re''4. re''8 |
re''4 do'' do''4. do''8 |
do''4 si' la'4.\trill sol'8 |
sol'2 r |
la'4 la'8 si' do''2 |
si'4 si'8 do'' re''4. re''8 |
re''4 do'' do''4. do''8 |
do''4 si' la'4.\trill sol'8 |
sol'2
