\piecePartSpecs
#`((dessus #:score-template "score-dessus2" #:system-count 3)
   (basse)
   (basse-continue)
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
