\tag #'(recit1 basse) {
  Vous voy -- ez Ju -- pi -- ter ; que rien ne vous é -- ton -- ne.
  C’est pour trom -- per Ju -- non & ses re -- gards ja -- loux
  qu’un Nu -- a -- ge vous en -- vi -- ron -- ne,
  bel -- le Nym -- phe, ras -- su -- rez- vous.
  Je vous aime & pour vous le di -- re,
  je sors a -- vec plai -- sir de mon su -- prême Em -- pi -- re.
  La foudre est dans mes mains, les Dieux me font la cour,
  je tiens tout l’U -- ni -- vers sous mon o -- bé -- ïs -- san -- ce ;
  mais, si je pré -- tens en ce jour
  en -- ga -- ger vô -- tre cœur à m’ai -- mer à son tour,
  je fon -- de moins mon es -- pe -- ran -- ce
  sur la gran -- deur de ma puis -- san -- ce,
  que sur l’ex -- cés de mon a -- mour.
  
  Que sert- il qu’i -- cy- bas vôtre a -- mour me choi -- sis -- se ?
  L’hon -- neur me vient trop tard, j’ay for -- mé d’au -- tres nœuds,
  il fal -- loit que ce bien pour com -- bler tous mes vœux,
  ne me coû -- tât point d’in -- jus -- ti -- ce,
  et ne fit point de mal- heu -- reux.
  
  C’est une as -- sez gran -- de gloi -- re
  pour vô -- tre pre -- mier Vain -- queur,
  d’être en -- cor dans vô -- tre mé -- moi -- re,
  et de me dis -- pu -- ter si long- temps vô -- tre Cœur.
  
  La gloi -- re doit for -- cer mon cœur à se dé -- fen -- dre.
  Si vous sor -- tez du Ciel pour cher -- cher les dou -- ceurs
  d’une a -- mour ten -- dre,
  vous pour -- rez ai -- sé -- ment at -- ta -- quer d’au -- tres cœurs,
  qui fe -- ront gloi -- re de se ren -- dre.
  
  Il n’est rien dans les cieux, il n’est rien i -- cy- bas,
  de si char -- mant que vos ap -- pas ;
  rien ne peut me tou -- cher d’u -- ne fla -- me si for -- te ;
  bel -- le Nym -- phe, vous l’em -- por -- tez
  sur les au -- tres beau -- tez,
  au -- tant que Ju -- pi -- ter l’em -- por -- te
  sur les au -- tres Di -- vi -- ni -- tez.
  Ver -- rez- vous tant d’a -- mour a -- vec in -- dif -- fe -- ren -- ce ?
  Quel trou -- ble vous sai -- sit ? où tour -- nez- vous vos pas ?
  
  Mon Cœur en vô -- tre pre -- sen -- ce
  fait trop peu de re -- sis -- tan -- ce ;
  con -- ten -- tez- vous, he -- las !
  d’é -- ton -- ner ma cons -- tan -- ce,
  et n’en tri -- om -- phez pas.
  Con -- ten -- tez- vous, he -- las !
  d’é -- ton -- ner ma cons -- tan -- ce,
  et n’en tri -- om -- phez pas.
  
  Et pour -- quoy crai -- gnez- vous Ju -- pi -- ter qui vous ai -- me ?
  
  Je crains tout, je me crains moy- mê -- me.
  
  Quoy, vou -- lez- vous me fuïr ?
  
  C’est mon der -- nier es -- poir.
  
  E -- coû -- tez mon a -- mour.
  
  E -- coû -- tez mon de -- voir.
  
  Vous a -- vez un cœur libre, & qui peut se dé -- fen -- dre.
  
  Non, vous ne lais -- sez pas mon cœur en mon pou -- voir.
  
  Quoy, vous ne vou -- lez pas m’en -- ten -- dre ?
  
  Je n’ay que trop de peine à ne le pas vou -- loir.
  Lais -- sez- moy.
  
  Quoy, si tôt ?
  
  Je de -- vois moins at -- ten -- dre ;
  que ne fuy -- ois-je, he -- las ! a -- vant que de vous voir !
    
  L’A -- mour pour moy vous sol -- li -- ci -- te,
  et je vois que vous me quit -- tez.

  Le de -- voir veut que je vous quit -- te,
  et je sens que vous m’ar -- res -- tez.
  Vous m’ar -- res -- tez.
  Vous m’ar -- res -- tez.
  Vous m’ar -- res -- tez.
}
\tag #'recit2 {
  Vous me quit -- tez.
  Vous me quit -- tez.
  Vous me quit -- tez.
}
