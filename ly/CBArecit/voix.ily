<<
  \tag #'(recit1 basse) {
    \ffclef "vbasse" <>^\markup\character Jupiter
    r8 sol16 sol mi8\trill mi16 sol |
    do4 r8 mi la la la la |
    fad\trill fad16 r re8 re16 re sol8. la16 |
    si4 sol8 sol16 sol la8. si16 |
    do'4 sol8. la16 sib8 sib16 sib sib[ la] sib[ sol] |
    la8 la r fa16 la re8\trill re r16 re re mi |
    do4 r8 do'16 mi fa8. fa16 fa8 fa16 mi |
    mi8.\trill mi16 r8 do sol sol sold8.\trill la16 |
    si8. si16 si8 do' re' si |
    do'4 do'8 r16 do' mi8\trill mi fa16[ mi] re[ do] |
    sol4 r8 sol la8. la16 si8 do' |
    si4\trill r8 sol do'4 do'8 do'16 do' |
    la4\trill fa8 fa16 fa fa8 fa |
    re4\trill re8 r re'4 |
    r8 si si do' la4\trill la8 si |
    sold8\trill mi16 mi la8 la16 la fad8\trill fad16 la re8 mi16 fad |
    sol8 r16 sol mi8.\trill do16 fa8 fa fa sol |
    la la16 la la8 re' si8.\trill si16 si8 do' |
    re'4 re'8 r16 sol sol8 do' |
    fa4. mi8 re4.\trill do8 |
    do4
    \ffclef "vbas-dessus" <>^\markup\character Io
    do''8 mi'' sold'4\trill sold'8 si' |
    mi'4 si'8 si' do''4 do''8 do'' |
    la'4\trill la'8 re'' si'\trill si' si' re'' |
    sol'4 do''8 re'' mi''4 mi''8 fa'' |
    re''4\trill si'8 si' do''4 do''8 do'' |
    la'4 la'8 do'' fa'4 fa'8 mi' |
    mi'4\trill do''16 do'' re'' mi'' la'4\trill la'8 si' |
    sold'\trill sold'16 r si'8 si'16 do'' la'8\trill la' la' sold' |
    la'4
    \ffclef "vbasse" <>^\markup\character Jupiter
    do'8. do'16 do' do' \footnoteHere #'(0 . 0) \markup {
      Matériel 1677 : \score {
        \new Staff \withLyrics <<
          { \tinyQuote \clef "basse" \measure 3/4
            r8 do'8. do'16 do' do' mi8\trill fa | }
        >> \lyrics { C’est une assez gran -- de }
        \layout { \quoteLayout }
      }
    }
    mi16\trill fa |
    sol4 sol8 la si si16 do' la8\trill sol |
    re'4 fad8 fad sol sol sol la16 si |
    mi8\trill mi16 r la4 la16 la si do' |
    fad4\trill sol8 do re4 re8 re |
    sol,4
    \ffclef "vbas-dessus" <>^\markup\character Io
    r8 sol' do''8. do''16 do''8 << { \voiceOne sol' \oneVoice } \new Voice { \voiceTwo \sug mi' } >> |
    la'8 la' sol'4\trill sol'8 sol'16 fa' |
    mi'4\trill mi'8 r16 la' si'8 do'' si'8.\trill la'16 |
    mi''4 sold'8 sold' la'4 la'8 la' |
    fad'4.\trill fad'8 fad'4 sol' |
    sol' sol'16 r sol' la' si'8\trill si'16 do'' |
    re''8 si'16 si' do''8 re''16 mi'' la'8\trill do'' re'' mi'' |
    fa''8 re'' si' do'' do''[^\markup\croche-pointee-double si'] |
    do''4
    \ffclef "vbasse" <>^\markup\character Jupiter
    mi8 fa sol4 la8 si |
    do'4 fa8 fa fa4
    \footnoteHere #'(0 . 0) \markup {
      Ballard 1719 : \score {
        \new Staff \withLyrics <<
          { \tinyQuote \clef "basse" \measure 5/4
            do'4 fa8 fa fa4 fa( mi8) fa | }
        >> \lyrics { cieux, il n’est rien i -- cy }
        \layout { \quoteLayout }
      }
    }
    mi8 fa |
    mi8\trill sol sold sold la8. la16 si8. do'16 |
    sold4\trill mi8 mi la4 la8 la |
    fad4.\trill la16 si do'4 do'8 re' |
    si4 sol16 r re mi fa8 fa16 sol la8 si |
    do'4 mi8 fa sol4 sol8 la |
    re\trill r16 re' si\trill la sol fa mi8 re16[ do] |
    sol4 mi8\trill fa16 sol la8 si16 do' si8.\trill do'16 |
    do'1 |
    r4 mi8 fad\trill sol4 sol8 la |
    fad fad fad fad sol la |
    si8. si16 r8 re' sol sol sol sol |
    re4\trill r8 re re mi fa sol |
    mi4\trill
    \ffclef "vbas-dessus" <>^\markup\character Io
    r8 do'' do'' si' si'\trill si'16 si' |
    sold'8.\trill sold'16 r8 si'16 mi'' dod''8.\trill dod''16 dod''8 re'' |
    re'' re'' r16 la' si' do'' si'4\trill re'' |
    sol'4 do''8 mi'' do''4\trill do''8 sol' |
    la'4 la'8 do'' re'' mi'' fa'' mi'' |
    re''\trill r16 si' si'8.\trill si'16 do''4 mi'' |
    la' do''8 fa'' re''4\trill re''8 la' |
    si'4\trill sol'8 do'' re'' mi'' re''8.\trill do''16 |
    do''4
    \ffclef "vbasse" <>^\markup\character Jupiter
    do8 do sol4 la8 si |
    do'4 do'8 mi' la4 la8 si |
    sold8. sold16
    \ffclef "vbas-dessus" <>^\markup\character Io
    r8 mi''16 mi'' la' r la' la' mi'8. la'16 |
    fad'8.\trill fad'16
    \ffclef "vbasse" <>^\markup\character Jupiter
    re'4 r8 la16 la si8 do' |
    si4\trill
    \ffclef "vbas-dessus" <>^\markup\character Io
    sol'8 sol'16 sol' la'8 si' |
    do''4
    \ffclef "vbasse" <>^\markup\character Jupiter
    mi8 fa sol4 la8 sib |
    la4\trill
    \ffclef "vbas-dessus" <>^\markup\character Io
    r8 do''16 do'' fa''4 fa''8. fa''16 |
    re''4
    \ffclef "vbasse" <>^\markup\character Jupiter
    r8 sol16 sol re4\trill re8 mi |
    fa4 fa8 la fa4\trill fa8 mi |
    mi\trill mi
    \ffclef "vbas-dessus" <>^\markup\character Io
    mi''4 r8 do''16 do'' re''8 mi'' |
    fa'' re'' si'\trill si' si'8. do''16 |
    la'4\trill
    \ffclef "vbasse" <>^\markup\character Jupiter
    do'4 mi16 mi mi mi fa8 sol |
    la4 la8
    \ffclef "vbas-dessus" <>^\markup\character Io
    do''8 do'' do'' dod''8.\trill dod''16 |
    re''8 re'' mi'' fa'' mi''8.\trill re''16 |
    re''4 r8 la'16 la' fad'4\trill
    \ffclef "vbasse" <>^\markup\character Jupiter
    re'8. la16 |
    sib4
    \ffclef "vbas-dessus" <>^\markup\character Io
    r8 re''16 re''
    \footnoteHere #'(0 . 0) \markup {
      Ballard 1719 : \score {
        \new Staff \withLyrics <<
          { \tinyQuote \clef "petrucci-c1" \measure 2/4
            r8 re''16 re'' sib'8 sib'16 sib' | sol'8 sol' }
        >> \lyrics { Je de -- vois moins at -- ten -- dre ; }
        \layout { \quoteLayout }
      }
    }
    sib'4\trill sib'8 sib' |
    sol'8\trill sol' r16 sib' do'' re'' mib''8 la' |
    fad'8.\trill sib'16 sol'8\trill sol' sol' fad' |
    sol'
    \ffclef "vbasse" <>^\markup\character Jupiter
    sol8 mi do sol sol sol do' |
    la\trill la r mi16 fa sol8. sol16 sol8 sol16 la |
    re4\trill
    \ffclef "vbas-dessus" <>^\markup\character Io
    r16 re'' re'' si' do''8. do''16 do''8 re'' |
    mi''8. mi''16 r8 do''16 fa'' re''4\trill re''16 re'' re'' mi'' |
    do''8\trill do''8 do'' mi'' la'2\trill |
    r8 la' la' re'' si'\trill re'' re'' sol'' |
    mi''2\trill r |
  }
  \tag #'recit2 {
    \ffclef "vbasse" r4 r |
    R1 R2.*2 R1*4 R2. R1*3 R2.*2 R1*4 R2. R1 R1*8 R2. R1*2 R2.
    R1 R1 R2. R1*2 R1 R2. R1 R2. R1*6 R1 R2. R1 R1 R1 R2. R1*14 R2.
    R1*5 R2. R1*2 R2. R1*2 R2.*2 R1*3 |
    r2 <>^\markup\character Jupiter r8 sol sol sol |
    mi2\trill r8 fa fa fa |
    re2 r8 sol sol sol |
    do'2 r |
  }
>>
