\clef "basse" do4~ do8. si,16 |
la,1 |
re4~ re8 do si,8. la,16 |
sol,2 fa,4 |
mi,1 |
fa,2 sol, |
do2. si,4 |
do2 si,4 mi8 la, |
sold,2. |
la,1 | \allowPageTurn
mi,4. mi8 fa4 fad |
sol4 sol,8. fa,16 mi,2 |
fa, re,4 |
sol,2. |
sol4. mi8 fa4. re8 |
mi4 dod re do? |
si, do la,4. mi,8 |
fa,4 fa8 re sol4. do8 |
sol,4. sol8 mi4 |
re4. do8 sol,2 |
do4 la, mi2 |
sold, la, |
re sol4. fa8 |
mi4. re8 do4. fa,8 |
sol,4 sol mi2 |
fa4. mi8 re2 |
la4. sol8 fa4. re8 |
mi4 re do8 la, mi mi, |
la,2~ la,8 sol,16 fa, |
mi,4. fad,8 sol, sol fad^\markup\croche-pointee-double mi |
re4. do8 si,2 |
do4 la,2 |
re4 mi8 do re4 re, |
sol, sol mi2 |
fa4 sol2 |
la4. la8 sold la fa4 |
mi2 dod |
re1 |
si,8. do16 si,8. la,16 sol,4 |
sol8 fa mi re16 do fa8. sol16 fa8 mi |
re4 sol8 do sol,4 |
do2 si, |
la,4. fa,8 sol,2 |
do4 si, la, sold,8. la,16 |
mi,4 mi dod2 |
re fad, |
sol, re4 re, |
la,4 sol,8 fa, mi,4. fa,8 |
sol,2 la,4 |
si,4 do16 sib, la, sol, fa, mi, re, do, sol,4 |
do,1 | \allowPageTurn
do2 dod |
re2 do4 |
si,1~ |
si, |
do2 re |
mi4 mi, la,4. sol,8 |
fad,2 sol,4 fa,? |
mi,1 |
fa,4 fa4. mi8 re4 |
sol fa mi2 |
fa fad |
sol4. mi8 re do sol,4 |
\once\tieDashed do2~ do4. si,8 |
la,2 fa |
mi dod |
re fad, |
sol,4 sol fa |
mi2 mi, |
fa, re, |
sol,1 |
re2 re, |
la, la |
re4 mi mi, |
la,2. sol,4 |
fa, \once\tieDashed fa~ fa mi |
re sol, la, |
re,2 re |
sol,1 |
mib4. re8 do4 |
re8. sib,16 mib8 do re re, |
sol,4 do4 mi,2 |
fa,8. sol,16 la,4 mi,4. fa,8 |
sol,4 sol mi4. re8 |
do4. fa,8 sol,2 |
do2 fa |
re sol4 sol, |
\once\tieDashed do2~ do8 do si, la, |
\once\set Staff.whichBar = "|"
\custosNote sol,8
