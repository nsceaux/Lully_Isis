\piecePartSpecs
#`((basse-continue #:score-template "score-voix")
   (silence #:music , #{

 s2\allowPageTurn
 s1\allowPageTurn
 s2.*2\allowPageTurn
 s1*4\allowPageTurn
 s2.\allowPageTurn
 s1*3\allowPageTurn
 s2.*2\allowPageTurn
 s1*4\allowPageTurn
 s2.\allowPageTurn
 s1\allowPageTurn
 s1*8\allowPageTurn
 s2.\allowPageTurn
 s1*2\allowPageTurn
 s2.\allowPageTurn
 s1\allowPageTurn
 s1\allowPageTurn
 s2.\allowPageTurn
 s1*2\allowPageTurn
 s1\allowPageTurn
 s2.\allowPageTurn
 s1\allowPageTurn
 s2.\allowPageTurn
 s1*6\allowPageTurn
 s1\allowPageTurn
 s2.\allowPageTurn
 s1\allowPageTurn
 s1\allowPageTurn
 s1\allowPageTurn
 s2.\allowPageTurn
 s1*14\allowPageTurn
 s2.\allowPageTurn
 s1*5\allowPageTurn
 s2.\allowPageTurn
 s1*2\allowPageTurn
 s2.\allowPageTurn
 s1*2\allowPageTurn
 s2.*2\allowPageTurn
 s1*4\allowPageTurn

#}))
