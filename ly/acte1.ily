\bookpart {
  \act "Acte Premier"
  \sceneDescription\markup\wordwrap-center {
    Le Théatre représente des Prairies agréables,
    où le Fleuve Inachus serpente.
  }
  \scene "Scene Premiere" "Scene I"
  \sceneDescription\markup\wordwrap-center { Hierax }
  %% 1-1
  \pieceToc "Ritournelle"
  \includeScore "BAAritournelle"
  %% 1-2
  \pieceToc\markup\wordwrap {
    Hierax : \italic { Cessons d’aimer une Infidelle }
  }
  \includeScore "BABrecit"
}
\bookpart {
  \scene "Scene II" "Scene II"
  \sceneDescription\markup\wordwrap-center { Pirante, Hierax }
  %% 1-3
  \pieceToc\markup\wordwrap {
    Pirante, Hierax : \italic { C’est trop entretenir vos tristes resveries }
  }
  \includeScore "BBArecit"
}
\bookpart {
  \scene "Scene III" "Scene III"
  \sceneDescription\markup\wordwrap-center {
    La Nymphe Io, Mycene, Hierax, Pirante
  }
  %% 1-4
  \pieceToc\markup\wordwrap {
    Io, Hierax : \italic { M’aimez-vous ? puis-je m’en flater ? }
  }
  \includeScore "BCArecit"
}
\bookpart {
  \scene "Scene IV" "Scene IV"
  \sceneDescription\markup\wordwrap-center {
    Io, Micene.
  }
  %% 1-5
  \pieceToc\markup\wordwrap {
    Micene, Io :
    \italic { Ce Prince trop longtemps dans ses chagrins s’obstine. }
  }
  \includeScore "BDArecit"
}
\bookpart {
  \paper { min-systems-per-page = 2 }
  \scene "Scene V" "Scene V"
  \sceneDescription\markup\wordwrap-center {
    Mercure, Io, chœurs des Divinitez de La Terre,
    et des Echos.
  }
  %% 1-6
  \pieceToc\markup\wordwrap {
    Mercure, chœur :
    \italic { Le Dieu puissant qui lance le Tonnerre }
  }
  \includeScore "BEAchoeur"
}
\bookpart {
  %% 1-7
  \pieceToc\markup\wordwrap {
    Mercure, Io :
    \italic { C'est ainsi que Mercure }
  }
  \includeScore "BEBrecit"
}
\bookpart {
  \paper { systems-per-page = 1 }
  \scene "Scene VI" "Scene VI"
  \sceneDescription\markup\justify {
    Les Divinitez de la Terre, des Eaux, & des Richesses soûterraines,
    viennent magnifiquement parées, pour recevoir Jupiter, & pour luy
    rendre hommage.
  }
  %% 1-8
  \pieceToc\markup\wordwrap {
    Chœur : \italic { Que tout l’Univers se pare }
  }
  \includeScore "BFAchoeur"
}
\bookpart {
  %% 1-9
  \pieceToc\markup\wordwrap {
    Premier air [pour l’Entrée des Divinitez de la Terre]
  }
  \includeScore "BFBair"
}
\bookpart {
  %% 1-10
  \pieceToc\markup Deuxième air
  \includeScore "BFCair"
}
\bookpart {
  %% 1-11
  \pieceToc\markup\wordwrap {
    Jupiter, chœur :
    \italic { Les armes que je tiens protegent l’innocence }
  }
  \includeScore "BFDchoeur"
  \actEnd FIN DU PREMIER ACTE
}
\bookpart {
  %% 1-12
  \pieceToc\markup Entr’acte
  \markup\italic {
    On reprend le second Air des Divinitez de la Terre, pour Entr’Acte.
  }
  \includeScore "BFEair"
}
